﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using Contract.Domain;
using Contract.Exception;
using Contract.Services;
using DataAccess;

namespace BusinessLogic
{
    public class ReportService : IReportService
    {
        private readonly IConfiguration _configuration;

        public ReportService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public List<ICustomerReport> GetCustomerReport(int seasonId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                return data.Tickets.AsNoTracking()
                    .Where(t => t.IsActive && t.SeasonId == seasonId)
                    .Join(data.Materials,
                       t => t.OrderTable.MaterialId, m => m.MaterialID,
                       (t, m) => new
                       {
                           t,
                           Material = m.Description,
                           EntityId = t.OrderTable.Bid.Customer.ReportingEntityID
                       })
                    .Join(data.Customers,
                       t => t.EntityId, c => c.CustomerID,
                       (t, c) => new CustomerReportData
                       {
                           TicketNumber = t.t.TicketNumber.ToString(),
                           TicketDateTime = t.t.CreateTime,
                           ConfNumber = t.t.OrderTable.ConfirmationNumber == null ? "" : t.t.OrderTable.ConfirmationNumber.ToString(),OrderNumber = t.t.OrderTable.OrderNumber == null ? "" : t.t.OrderTable.OrderNumber.ToString(),
                           OrderCompleted = t.t.OrderTable.isComplete,
                           PoNumber = t.t.OrderTable == null ? "" : t.t.OrderTable.POOverride,
                           CustomerId = t.t.OrderTable.Bid.Customer.ShortName,
                           CustomerName = t.t.OrderTable.Bid.Customer.DispatcherName ?? "",
                           MaterialRate = t.t.CashPurchase.quote ?? 0,
                           Quantity = t.t.Quantity,
                           CashRate = t.t.CashPurchase == null ? 0 : (t.t.CashPurchase.quote ?? 0) * (t.t.CashPurchase.TaxRate ?? 0),
                           PaymentType = t.t.CashPurchase == null ? "" : t.t.CashPurchase.paymentForm,
                           TruckId = t.t.Truck != null ? t.t.Truck.ShortName : string.Empty,
                           VoidDescription = t.t.VoidReason,
                           Organization = t.t.OrderTable == null ? "" : t.t.OrderTable.Bid.Customer.CustomerContacts.Where(cc => cc.Contact.IsActive).Select(cc => cc.Contact).FirstOrDefault().Organization,
                           Season = t.t.SeasonId,
                           IsVoid = t.t.isVoid,
                           Material = t.Material,
                           Entity = c.DispatcherName
                       })
                   .Select(CustomerReportData.TransformReport).ToList();
            }
        }

        public List<ITruckReport> GetTruckReport(int seasonId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                return data.Tickets.AsNoTracking()
                    .Where(t => t.IsActive && t.SeasonId == seasonId)
                    .Join(data.Materials,
                       t => t.OrderTable.MaterialId, m => m.MaterialID,
                       (t, m) => new
                       {
                           t,
                           Material = m.Description,
                           EntityId = t.OrderTable.Bid.Customer.ReportingEntityID
                       })
                    .Join(data.Customers,
                       t => t.EntityId, c => c.CustomerID,
                       (t, c) => new TruckReportData
                       {
                           TicketNumber = t.t.TicketNumber.ToString(),
                           TicketDateTime = t.t.CreateTime,
                           ConfNumber = t.t.OrderTable.ConfirmationNumber == null ? "" : t.t.OrderTable.ConfirmationNumber.ToString(),
                           OrderNumber = t.t.OrderTable.OrderNumber == null ? "" : t.t.OrderTable.OrderNumber.ToString(),
                           PoNumber = t.t.OrderTable == null ? "" : t.t.OrderTable.POOverride,
                           CustomerId = t.t.OrderTable.Bid.Customer.ShortName,
                           CustomerName = t.t.OrderTable.Bid.Customer.DispatcherName ?? "",
                           MaterialRate = t.t.OrderTable.Bid.Customer.CashMaterialRate ?? 0,
                           TareTons = (t.t.CashPurchase.tare / 2000m) ?? 0,
                           Quote = t.t.CashPurchase.quote,
                           Quantity = t.t.Quantity,
                           FuelSurcharge = t.t.FuelSurcharge ?? 0,
                           TruckId = t.t.Truck != null ? t.t.Truck.ShortName : string.Empty,
                           VoidDescription = t.t.VoidReason,
                           Organization = t.t.OrderTable == null ? "" : t.t.OrderTable.Bid.Customer.CustomerContacts.Where(cc => cc.Contact.IsActive).Select(cc => cc.Contact).FirstOrDefault().Organization,
                           Season = t.t.SeasonId,
                           IsVoid = t.t.isVoid,
                           Material = t.Material,
                           Entity = c.DispatcherName
                       })
                    .Select(TruckReportData.TransformReport).ToList();
            }
        }

        public List<IMunicipalReport> GetMunicipalReport(int seasonId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                return data.Tickets.AsNoTracking()
                    .Where(t => t.IsActive && t.SeasonId == seasonId)
                    .Join(data.Materials,
                       t => t.OrderTable.MaterialId, m => m.MaterialID,
                       (t, m) => new
                       {
                           t,
                           Material = m.Description,
                           EntityId = t.OrderTable.Bid.Customer.ReportingEntityID
                       })
                    .Join(data.Customers,
                       t => t.EntityId, c => c.CustomerID,
                       (t, c) => new MunicipalReportData
                       {
                           TicketNumber = t.t.TicketNumber.ToString(),
                           TicketDateTime = t.t.CreateTime,
                           ConfNumber = t.t.OrderTable.ConfirmationNumber == null ? "" : t.t.OrderTable.ConfirmationNumber.ToString(),
                           OrderNumber = t.t.OrderTable.OrderNumber == null ? "" : t.t.OrderTable.OrderNumber.ToString(),
                           OrderCompleted = t.t.OrderTable.isComplete,
                           PoNumber = t.t.OrderTable == null ? "" : t.t.OrderTable.POOverride,
                           CustomerId = t.t.OrderTable.Bid.Customer.ShortName,
                           CustomerName = t.t.OrderTable.Bid.Customer.DispatcherName ?? "",
                           Quantity = t.t.Quantity,
                           TruckId = t.t.Truck != null ? t.t.Truck.ShortName : string.Empty,
                           Organization = t.t.OrderTable == null ? "" : t.t.OrderTable.Bid.Customer.CustomerContacts.Where(cc => cc.Contact.IsActive).Select(cc => cc.Contact).FirstOrDefault().Organization,
                           Season = t.t.SeasonId,
                           IsVoid = t.t.isVoid,
                           Material = t.Material,
                           Entity = c.DispatcherName
                       })
                    .Select(MunicipalReportData.TransformReport).ToList();
            }
        }

        public List<IReportConfiguration> GetReportConfigurationList(int seasonId, int siteId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var reportConfigurationList = new List<IReportConfiguration> { new ReportConfigurationData() };
                var reportConfigurationDbList = data.ReportConfigurations
                    .AsNoTracking()
                    .Where(t => t.SeasonId == seasonId && t.SiteId == siteId)
                    .Select(ReportConfigurationData.Transform)
                    .ToList();
                reportConfigurationList.AddRange(reportConfigurationDbList);

                return reportConfigurationList;
            }
        }

        public void SaveReportConfiguration(IReportConfiguration selectedReportConfiguration)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                if (selectedReportConfiguration.Id == 0)
                {
                    if (data.ReportConfigurations.Any(t =>
                        t.Name == selectedReportConfiguration.Name
                        && t.SeasonId == selectedReportConfiguration.SeasonId
                        && t.SiteId == selectedReportConfiguration.SiteId))
                    {
                        throw new UniqueConstraintException("Report Configuration Name should be unique.");
                    }
                }

                var reportConfiguration = data.ReportConfigurations.Find(selectedReportConfiguration.Id) ?? new ReportConfiguration();

                reportConfiguration.ID = selectedReportConfiguration.Id;
                reportConfiguration.Name = selectedReportConfiguration.Name;
                reportConfiguration.Configuration = selectedReportConfiguration.Configuration;
                reportConfiguration.SiteId = selectedReportConfiguration.SiteId;
                reportConfiguration.SeasonId = selectedReportConfiguration.SeasonId;

                data.ReportConfigurations.AddOrUpdate(reportConfiguration);
                data.SaveChanges();
            }
        }

        public bool DeleteReport(int reportConfigId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var reports = data.ReportConfigurations.Find(reportConfigId);
                if (reports == null)
                {
                    return false;
                }

                data.ReportConfigurations.Remove(reports);
                data.SaveChanges();
                return true;
            }
        }

        public List<ICustomerExportData> GetCustomerExportData(int seasonId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                return data.CustomerContacts.AsNoTracking()
                    .Where(cc => cc.Customer.IsActive && cc.Customer.SeasonId == seasonId)
                    .Select(CustomerExportData.Transform).OrderBy(c=>c.CustomerId)
                    .ToList();
            }
        }


        #region DTO

        internal class CustomerReportData : ICustomerReport
        {

            public static readonly Expression<Func<Ticket, ICustomerReport>> Transform = e => new CustomerReportData
            {
                TicketNumber = e.TicketNumber.ToString(),
                TicketDateTime = e.CreateTime,
                OrderNumber = e.OrderTable.OrderNumber == null ? "" : e.OrderTable.OrderNumber.ToString(),
                ConfNumber = e.OrderTable.ConfirmationNumber == null ? "" : e.OrderTable.ConfirmationNumber.ToString(),
                OrderCompleted = e.OrderTable.isComplete,
                PoNumber = e.OrderTable == null ? "" : e.OrderTable.POOverride,
                CustomerId = e.OrderTable.Bid.Customer.ShortName,
                CustomerName = e.OrderTable.Bid.Customer.DispatcherName ?? "",
                MaterialRate = e.CashPurchase.quote ?? 0,
                Quantity = e.Quantity,
                CashRate = e.CashPurchase == null ? 0 : (e.CashPurchase.quote ?? 0) * (e.CashPurchase.TaxRate ?? 0),
                PaymentType = e.CashPurchase == null ? "" : e.CashPurchase.paymentForm,
                TruckId = e.Truck != null ? e.Truck.ShortName : string.Empty,
                VoidDescription = e.VoidReason,
                Organization = e.OrderTable == null ? "" : e.OrderTable.Bid.Customer.CustomerContacts.Where(cc => cc.Contact.IsActive).Select(cc => cc.Contact).FirstOrDefault().Organization,
                Season = e.SeasonId,
                IsVoid = e.isVoid
            };

            public static readonly Expression<Func<CustomerReportData, ICustomerReport>> TransformReport = e => new CustomerReportData
            {
                TicketNumber = e.TicketNumber,
                TicketDateTime = e.TicketDateTime,
                ConfNumber = e.ConfNumber,
                OrderNumber = e.OrderNumber,
                OrderCompleted = e.OrderCompleted,
                PoNumber = e.PoNumber,
                CustomerId = e.CustomerId,
                CustomerName = e.CustomerName,
                MaterialRate = e.MaterialRate,
                Quantity = e.Quantity,
                CashRate = e.CashRate,
                PaymentType = e.PaymentType,
                TruckId = e.TruckId,
                VoidDescription = e.VoidDescription,
                Organization = e.Organization,
                Season = e.Season,
                IsVoid = e.IsVoid,
                Material = e.Material,
                Entity = e.Entity
            };

            public CustomerReportData()
            {

            }

            public string TicketNumber { get; set; }

            public DateTime TicketDateTime { get; set; }
            public string ConfNumber { get; set; }
            public string OrderNumber { get; set; }

            public bool? OrderCompleted { get; set; }

            public string OrderCompletedText
            {
                get { return OrderCompleted != null ? "Completed" : "Not Completed"; }
            }

            public string PoNumber { get; set; }

            public string CustomerId { get; set; }

            public string CustomerName { get; set; }

            public string TruckId { get; set; }

            public decimal MaterialRate { get; set; }

            public decimal NetTons
            {
                get { return (decimal) Quantity / 2000m; }
            }

            public decimal Tax
            {
                get { return NetTons * CashRate; }
            }

            public decimal Total
            {
                get { return (MaterialRate * NetTons) + Tax; }
            }

            public string PaymentType { get; set; }

            public string VoidDescription { get; set; }
            public string Organization { get; set; }
            public int? EntityId { get; set; }
            public int? MaterialId { get; set; }
            public string Entity { get; set; }
            public string Material { get; set; }
            public int Season { get; set; }
            public bool IsVoid { get; set; }
            public float Quantity { get; set; }
            public decimal CashRate { get; set; }
        }

        internal class CustomerExportData : ICustomerExportData
        {
            public static readonly Expression<Func<CustomerContact, ICustomerExportData>> Transform =
                e => new CustomerExportData
                {
                    CustomerId = e.Customer.ShortName,
                    DispatcherName = e.Customer.DispatcherName,
                    Name = e.Contact.Name,
                    Organization = e.Contact.Organization,
                    Address1 = e.Contact.Address.AddressLineOne,
                    Address2 = e.Contact.Address.AddressLineTwo,
                    City = e.Contact.Address.City,
                    State = e.Contact.Address.State,
                    Zip = e.Contact.Address.Zip,
                    Phone = e.Contact.PhoneNumbers.Where(p => p.IsActive && p.isPrimary).Select(p => p.PhoneNumber1).FirstOrDefault(),
                };

            public string State { get; set; }

            public string Zip { get; set; }

            public string Phone { get; set; }

            public string City { get; set; }

            public string Address2 { get; set; }

            public string Address1 { get; set; }

            public string Organization { get; set; }

            public string Name { get; set; }

            public string DispatcherName { get; set; }

            public string CustomerId { get; set; }

            public CustomerExportData()
            {

            }
        }

        public class TruckReportData : ITruckReport
        {

            public static readonly Expression<Func<Ticket, ITruckReport>> Transform = e => new TruckReportData
            {                
                TicketNumber = e.TicketNumber.ToString(),
                TicketDateTime = e.CreateTime,
                ConfNumber = e.OrderTable.ConfirmationNumber == null ? "" : e.OrderTable.ConfirmationNumber.ToString(),
                OrderNumber = e.OrderTable.OrderNumber == null ? "" : e.OrderTable.OrderNumber.ToString(),
                PoNumber = e.OrderTable == null ? "" : e.OrderTable.POOverride,
                CustomerId = e.OrderTable.Bid.Customer.ShortName,
                CustomerName = e.OrderTable.Bid.Customer.DispatcherName ?? "",
                MaterialRate = e.OrderTable.Bid.Customer.CashMaterialRate ?? 0,
                TareTons = (e.CashPurchase.tare / 2000m) ?? 0,
                Quote = e.CashPurchase.quote,
                Quantity = e.Quantity,
                FuelSurcharge = e.FuelSurcharge ?? 0,
                TruckId = e.Truck != null ? e.Truck.ShortName : string.Empty,
                VoidDescription = e.VoidReason,
                Organization = e.OrderTable == null ? "" : e.OrderTable.Bid.Customer.CustomerContacts.Where(cc => cc.Contact.IsActive).Select(cc => cc.Contact).FirstOrDefault().Organization,
                Season = e.SeasonId,
                IsVoid = e.isVoid
            };

            public static readonly Expression<Func<TruckReportData, ITruckReport>> TransformReport = e => new TruckReportData
            {
                TicketNumber = e.TicketNumber,
                TicketDateTime = e.TicketDateTime,
                ConfNumber = e.ConfNumber,
                OrderNumber = e.OrderNumber,
                PoNumber = e.PoNumber,
                CustomerId = e.CustomerId,
                CustomerName = e.CustomerName,
                MaterialRate = e.MaterialRate,
                TareTons = e.TareTons,
                Quote = e.Quote,
                Quantity = e.Quantity,
                FuelSurcharge = e.FuelSurcharge,
                TruckId = e.TruckId,
                VoidDescription = e.VoidDescription,
                Organization = e.Organization,
                Season = e.Season,
                IsVoid = e.IsVoid,
                Material = e.Material,
                Entity = e.Entity
            };

            public TruckReportData()
            {

            }

            public string TicketNumber { get; set; }

            public DateTime TicketDateTime { get; set; }

            public string ConfNumber { get; set; }
            public string OrderNumber { get; set; }

            public string PoNumber { get; set; }

            public string CustomerId { get; set; }

            public string CustomerName { get; set; }

            public string TruckId { get; set; }

            public decimal GrossTons
            {
                get { return TareTons + NetTons; }
            }
            
            public decimal TareTons { get; set; }

            public decimal MaterialRate { get; set; }

            public decimal NetTons
            {
                get { return (decimal)Quantity / 2000m; }
            }
            
            public decimal TruckingRate
            {
                get { return Quote ?? 0; }
            }
            
            public decimal SubTotal
            {
                get { return TruckingRate * NetTons; }
            }
            
            public decimal FuelSurcharge { get; set; }

            public decimal Total
            {
                get { return (SubTotal * FuelSurcharge); }
            }

            public string VoidDescription { get; set; }
            public string Organization { get; set; }
            public string Entity { get; set; }
            public string Material { get; set; }
            public int Season { get; set; }
            public bool IsVoid { get; set; }
            public float Quantity { get; set; }
            public decimal? Quote { get; set; }
        }

        internal class MunicipalReportData : IMunicipalReport
        {

            public static readonly Expression<Func<Ticket, IMunicipalReport>> Transform = e => new MunicipalReportData
            {
                TicketNumber = e.TicketNumber.ToString(),
                TicketDateTime = e.CreateTime,
                ConfNumber = e.OrderTable.ConfirmationNumber == null ? "" : e.OrderTable.ConfirmationNumber.ToString(),
                OrderNumber = e.OrderTable.OrderNumber == null ? "" : e.OrderTable.OrderNumber.ToString(),
                OrderCompleted = e.OrderTable.isComplete,
                PoNumber = e.OrderTable == null ? "" : e.OrderTable.POOverride,
                CustomerId = e.OrderTable.Bid.Customer.ShortName,
                CustomerName = e.OrderTable.Bid.Customer.DispatcherName ?? "",
                Quantity = e.Quantity,
                TruckId = e.Truck != null ? e.Truck.ShortName : string.Empty,
                Organization = e.OrderTable == null ? "" : e.OrderTable.Bid.Customer.CustomerContacts.Where(cc => cc.Contact.IsActive).Select(cc => cc.Contact).FirstOrDefault().Organization,
                Season = e.SeasonId,
                IsVoid = e.isVoid,
            };

            public static readonly Expression<Func<MunicipalReportData, IMunicipalReport>> TransformReport = e => new MunicipalReportData
            {
                TicketNumber = e.TicketNumber,
                TicketDateTime = e.TicketDateTime,
                ConfNumber = e.ConfNumber,
                OrderNumber = e.OrderNumber,
                OrderCompleted = e.OrderCompleted,
                PoNumber = e.PoNumber,
                CustomerId = e.CustomerId,
                CustomerName = e.CustomerName,
                Quantity = e.Quantity,
                TruckId = e.TruckId,
                Organization = e.Organization,
                Season = e.Season,
                IsVoid = e.IsVoid,
                Material = e.Material,
                Entity = e.Entity
            };

            public MunicipalReportData()
            {

            }

            public string TicketNumber { get; set; }

            public DateTime TicketDateTime { get; set; }

            public string ConfNumber { get; set; }
            public string OrderNumber { get; set; }

            public bool? OrderCompleted { get; set; }

            public string OrderCompletedText
            {
                get { return OrderCompleted != null ? "Completed" : "Not Completed"; }
            }

            public string PoNumber { get; set; }

            public string CustomerId { get; set; }

            public string CustomerName { get; set; }

            public string TruckId { get; set; }

            public decimal NetTons
            {
                get { return (decimal)Quantity / 2000m; }
            }
            public string Organization { get; set; }
            public string Entity { get; set; }
            public string Material { get; set; }
            public int Season { get; set; }
            public bool IsVoid { get; set; }
            public float Quantity { get; set; }
        }

     
        internal class ReportConfigurationData : IReportConfiguration
        {
            public static readonly Expression<Func<ReportConfiguration, IReportConfiguration>> Transform = e => new ReportConfigurationData
            {
                Id = e.ID,
                Name = e.Name,
                Configuration = e.Configuration,
                SiteId = e.SiteId,
                SeasonId = e.SeasonId
            };

            public ReportConfigurationData()
            {
            }

            public int Id { get; set; }
            public string Name { get; set; }
            public string Configuration { get; set; }
            public int SiteId { get; set; }
            public int SeasonId { get; set; }
        }

        
        #endregion

    }

    public class ReportFilter : IReportFilter
    {
        public string ReportFileName { get; set; }
        public string ReportTitle { get; set; }
        public DateTime BeginDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public DetailSummaryEnum DetailSummary { get; set; }
        public bool IsVoidsIncluded { get; set; }
        public ReportDefintionEnum ReportDefinitionEnumValue { get; set; }
        public CustomerTypeEnum CustomerType { get; set; }
        public int SiteId { get; set; }
        public ReportSortByEnum SortBy { get; set; }
        public List<ReportFilterGroupApplied> AppliedFilters { get; set; }
        public ReportFilter()
        {

        }
    }
}