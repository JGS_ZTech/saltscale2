﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Contract.Domain;
using Contract.Services;
using DataAccess;

namespace BusinessLogic
{
    public class ScaleService : IScaleService
    {
        private readonly IConfiguration _configuration;

        public ScaleService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public IReadOnlyList<IScaleInfo> GetAvailableScales(string site)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                return data.Scales.AsNoTracking().Where(s => s.Site == site).Select(ScaleInfo.Transform).ToList();
            }
        }


        public void UpdateScales(IEnumerable<IScaleInfo> scales)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                foreach (IScaleInfo scaleInfo in scales)
                {
                    Scale scale = data.Scales.First(s => s.ID == scaleInfo.Id);
                    scale.IsScaleEnabled = scaleInfo.IsScaleEnabled;
                    scale.SerialComPort = scaleInfo.SerialComPort;
                }

                data.SaveChanges();
            }
        }

        #region DTO

        public class ScaleData : IScaleData
        {
            public static readonly Expression<Func<Scale, IScaleData>> Transform = e => new ScaleData
            {
                Id = e.ID,
                Site = e.Site,
                ScaleName = e.ScaleName,
                IsScaleEnabled = e.IsScaleEnabled,
                SerialComPort = e.SerialComPort,
                SerialBaud = e.SerialBaud,
                SerialDataBits = e.SerialDataBits,
                SerialParity = e.SerialParity,
                SerialStopBits = e.SerialStopBits,
                IsIndicatorWeightNegative = e.IsIndicatorWeightNegative,
                IsIndicatorWeightOverload = e.IsIndicatorWeightOverload,
                IsIndicatorWeightUnderload = e.IsIndicatorWeightUnderload,
                IsMotionDetectionEnabled = e.IsMotionDetectionEnabled,
                MotionDetectionCharacter = e.MotionDetectionCharacter,
                OutputStreamFormat = e.IndicatorBrand,
                IndicatorModel = e.IndicatorModel,
                ReWeighTimerMS = e.ReWeighTimerMS,
                SerialPortDataReceivedDelayMS = e.SerialPortDataReceivedDelayMS
            };

            public int Id { get; set; }
            public string Site { get; set; }
            public string ScaleName { get; set; }
            public bool IsScaleEnabled { get; set; }
            public int SerialComPort { get; set; }
            public int SerialBaud { get; set; }
            public string SerialParity { get; set; }
            public int SerialDataBits { get; set; }
            public int SerialStopBits { get; set; }
            public int SerialPortDataReceivedDelayMS { get; set; }
            public bool IsIndicatorWeightNegative { get; set; }
            public bool IsIndicatorWeightOverload { get; set; }
            public bool IsIndicatorWeightUnderload { get; set; }
            public bool IsMotionDetectionEnabled { get; set; }
            public string MotionDetectionCharacter { get; set; }
            public string OutputStreamFormat { get; set; }
            public string IndicatorModel { get; set; }
            public int ReWeighTimerMS { get; set; }
        }

        private class ScaleInfo : IScaleInfo
        {
            public static readonly Expression<Func<Scale, IScaleInfo>> Transform = e => new ScaleInfo
            {
                Id = e.ID,
                ScaleName = e.ScaleName,
                IsScaleEnabled = e.IsScaleEnabled,
                SerialComPort = e.SerialComPort
            };

            public int Id { get; set; }
            public string ScaleName { get; set; }
            public bool IsScaleEnabled { get; set; }
            public int SerialComPort { get; set; }
        }

        #endregion
    }
}