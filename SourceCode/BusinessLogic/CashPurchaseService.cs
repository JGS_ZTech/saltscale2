﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Contract.Domain;
using Contract.Exception;
using Contract.Services;
using DataAccess;

namespace BusinessLogic
{
    public class CashPurchaseService : ICashPurchaseService
    {
        private readonly IConfiguration _configuration;

        public CashPurchaseService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public ICashPurchase GetCashPurchase(int id, bool isComplete)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                // 2018-Jul-22, JGS - Get the currently active season
                var currentSeason = data.Seasons.FirstOrDefault(s => s.IsActive == true);

                var cashPurchase = data.CashPurchases.AsNoTracking().FirstOrDefault(cp => cp.IsActive && cp.isComplete == isComplete && cp.cashPurchaseID == id && cp.SeasonId == currentSeason.SeasonId);
                return cashPurchase == null ? null : new CashPurchaseData(cashPurchase);
            }
        }

        public List<ICashPurchase> GetCheckInList(int seasonId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                return data.CashPurchases.AsNoTracking().Where(cp => cp.IsActive && cp.isComplete == false && cp.SeasonId == seasonId && cp.Tickets.Count == 0).Select(CashPurchaseData.Transform).ToList();
            }
        }

        public List<ICashPurchase> GetReCheckInList(int seasonId, string siteName)
        {
            DateTime weekAgo = DateTime.Now.AddDays(-7);
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                return data.Tickets.AsNoTracking().Where(t => t.CashPurchase != null && t.OrderTable == null && t.CashPurchase.isComplete && t.CashPurchase.IsActive && t.CashPurchase.SeasonId == seasonId && t.CashPurchase.UpdateTime >= weekAgo).Select(t => t.CashPurchase).Select(CashPurchaseData.Transform).ToList();
            }
        }

        public ICashPurchase GetNewCashPurchase(int seasonId, string siteName)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var materialInfo = data.Materials.FirstOrDefault(m => m.IsActive && m.SeasonId == seasonId);  // 2018-Jul-17, JGS - Make sure the material matches for the current season
                if (materialInfo == null)
                    throw new EntityNotFoundException("Material, defined in materials, not found in database.", typeof(Material), "default material");

                var site = data.Sites.AsNoTracking().FirstOrDefault(s => s.SeasonId == seasonId && s.SiteName == siteName);

                if (site == null)
                    throw new EntityNotFoundException("No site found.", typeof(Site), siteName);

                var companyInfo = site.DefaultCompanyId.HasValue ? site.Company : data.Companies.FirstOrDefault();

                if (companyInfo == null)
                    throw new EntityNotFoundException("Company, defined in companies, not found in database.", typeof(Material), "default company");

                var cashPurchase = new CashPurchase
                {
                    SeasonId = seasonId,
                    isComplete = false,
                    truckDescription = String.Empty,
                    quote = 0m,
                    tare = 0,
                    paymentForm = String.Empty,
                    Company = companyInfo,
                    companyID = companyInfo.CompanyId,
                    Material = materialInfo,
                    materialID = materialInfo.MaterialID,
                    locationID = site.SiteId,
                    taxExempt = false,
                    checkNumber = String.Empty,
                    CreateTime = DateTime.Now,
                    UpdateTime = DateTime.Now,
                };

                return new CashPurchaseData(cashPurchase);
            }
        }

        public void SaveCashPurchase(ICashPurchase cashPurchase)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var cashPurchaseInfo = data.CashPurchases.Create();
                cashPurchaseInfo.CreateTime = DateTime.Now;
                data.CashPurchases.Add(cashPurchaseInfo);

                cashPurchaseInfo.truckDescription = cashPurchase.TruckDescription;
                cashPurchaseInfo.materialID = cashPurchase.Material.Id;
                cashPurchaseInfo.quote = cashPurchase.Quote;
                cashPurchaseInfo.isComplete = cashPurchase.IsComplete;
                cashPurchaseInfo.companyID = cashPurchase.Company.Id;
                cashPurchaseInfo.locationID = cashPurchase.LocationId;
                cashPurchaseInfo.tare = cashPurchase.Tare;
                cashPurchaseInfo.paymentForm = cashPurchase.PaymentForm;
                cashPurchaseInfo.checkNumber = cashPurchase.CheckNumber;
                cashPurchaseInfo.taxExempt = cashPurchase.TaxExempt;
                cashPurchaseInfo.SeasonId = cashPurchase.SeasonId;
                cashPurchaseInfo.UpdateTime = DateTime.Now;
                cashPurchaseInfo.IsActive = true;

                data.SaveChanges();
            }
        }

        public bool DeleteCashPurchase(int cashPurchaseId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var cashPurchase = data.CashPurchases.Find(cashPurchaseId);
                if (cashPurchase == null)
                {
                    return false;
                }

                cashPurchase.UpdateTime = DateTime.Now;
                cashPurchase.IsActive = false;
                data.SaveChanges();
                return true;
            }
        }

        #region DTO

        internal class CashPurchaseData : ICashPurchase
        {
            public static readonly Expression<Func<CashPurchase, ICashPurchase>> Transform = e => new CashPurchaseData
            {
                CashPurchaseId = e.cashPurchaseID,
                TruckDescription = e.truckDescription,
                MaterialId = e.materialID,
                Quote = e.quote,
                CompanyId = e.companyID,
                LocationId = e.locationID,
                Tare = e.tare,
                PaymentForm = e.paymentForm,
                CheckNumber = e.checkNumber,
                TaxExempt = e.taxExempt,
                IsComplete = e.isComplete,
                CreateTime = e.CreateTime,
                UpdateTime = e.UpdateTime,
                IsActive = e.IsActive,
                SeasonId = e.SeasonId,
                DataA = e.DataA,
                DataB = e.DataB,
                DataC = e.DataC,
                CheckOutTime = e.Tickets.OrderByDescending(t=>t.UpdateTime).Select(t=>t.UpdateTime).FirstOrDefault()
            };

            public DateTime? CheckOutTime { get; set; }

            public CashPurchaseData()
            {

            }

            public CashPurchaseData(CashPurchase cashPurchase)
            {
                CashPurchaseId = cashPurchase.cashPurchaseID;
                TruckDescription = cashPurchase.truckDescription;
                MaterialId = cashPurchase.materialID;
                Material = new MaterialInfo(cashPurchase.Material);
                Quote = cashPurchase.quote;
                CompanyId = cashPurchase.companyID;
                Company = new CommonService.CompanyInfo(cashPurchase.Company);
                LocationId = cashPurchase.locationID;
                Tare = cashPurchase.tare;
                PaymentForm = cashPurchase.paymentForm;
                CheckNumber = cashPurchase.checkNumber;
                TaxExempt = cashPurchase.taxExempt;
                IsComplete = cashPurchase.isComplete;
                CreateTime = cashPurchase.CreateTime;
                UpdateTime = cashPurchase.UpdateTime;
                IsActive = cashPurchase.IsActive;
                Comment = cashPurchase.Comment;
                SeasonId = cashPurchase.SeasonId;
                DataA = cashPurchase.DataA;
                DataB = cashPurchase.DataB;
                DataC = cashPurchase.DataC;
            }

            public int CashPurchaseId { get; set; }
            public string TruckDescription { get; set; }
            public int? MaterialId { get; set; }
            public decimal? Quote { get; set; }
            public int? CompanyId { get; set; }
            public int? LocationId { get; set; }
            public int? Tare { get; set; }
            public string PaymentForm { get; set; }
            public string CheckNumber { get; set; }
            public bool TaxExempt { get; set; }
            public bool IsComplete { get; set; }
            public DateTime CreateTime { get; set; }
            public DateTime? UpdateTime { get; set; }
            public bool IsActive { get; set; }
            public string Comment { get; set; }
            public string DataA { get; set; }
            public string DataB { get; set; }
            public string DataC { get; set; }
            public int SeasonId { get; set; }
            public ISeason Season { get; set; }
            public ICompanyInfo Company { get; set; }
            public IMaterialInfo Material { get; set; }
        }

        #endregion
    }
}