﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Caliburn.Micro;
using Contract.Domain;
using Contract.Events;
using Contract.Exception;
using Contract.Services;
using DataAccess;
using System.Collections.ObjectModel;

namespace BusinessLogic
{
    public class CommonService : ICommonService
    {
        private readonly IConfiguration _configuration;
        private readonly IEventAggregator _eventAggregator;
        private readonly ILog _log = LogManager.GetLog(typeof (CommonService));

        public CommonService(IConfiguration configuration, IEventAggregator eventAggregator)
        {
            _configuration = configuration;
            _eventAggregator = eventAggregator;
        }

        public IReadOnlyList<ISeason> GetSeasons()
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                return data.Seasons.AsNoTracking().Select(Season.Transform).OrderByDescending(s => s.SeasonId).ToList();
            }
        }

        public IReadOnlyList<ICompanyInfo> GetCompanies()
        {
            return GetCompanies(_configuration.ConnectionString);
        }

        public IReadOnlyList<ICompanyData> GetCompaniesData()
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var companiesData = data.Companies.AsNoTracking().Select(CompanyData.Transform).ToList();
                foreach (var company in companiesData)
                {
                    GetCompanyData(data, company);
                }
                return companiesData;
            }
        }

        public ICompanyData GetCompanyData(int id)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var company =
                    data.Companies.AsNoTracking()
                        .Where(c => c.CompanyId == id)
                        .Select(CompanyData.Transform)
                        .FirstOrDefault();
                if (company == null)
                    throw new EntityNotFoundException("No company found.", typeof (Company), id);
                    //ToDo Log Error

                GetCompanyData(data, company);
                return company;
            }
        }

        public void SaveCompany(ICompanyData companyData)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var company = data.Companies.FirstOrDefault(c => c.CompanyId == companyData.Id);

                if (company == null)
                    throw new EntityNotFoundException("No company found.", typeof (Company), companyData.Id);

                company.PrinterPort = companyData.PrinterTicketName;

                UpdateTicketPropItem(companyData.Name, "FirstTicket", companyData.FirstTicket, data);
                UpdateTicketPropItem(companyData.Name, "LastTicket", companyData.LastTicket, data);
                UpdateTicketPropItem(companyData.Name, "NextTicket", companyData.NextTicket, data);

                data.SaveChanges();
            }
        }

        public void SaveSite(ISiteInfo siteData)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var site = data.Sites.FirstOrDefault(c => c.SiteId == siteData.Id);

                if (site == null)
                    throw new EntityNotFoundException("No site found.", typeof (Company), siteData.Id);

                site.MaterialCost = siteData.DefaultMaterialCost;
                site.TaxRate = siteData.TaxRate;
                site.WeightInterval = siteData.WeightInterval;
                site.DefaultCompanyId = siteData.DefaultCompanyId;
                site.FuelSurcharge = siteData.FuelSurcharge;
                site.ConfNumberFirst = siteData.ConfNumberFirst;
                site.ConfNumberLast = siteData.ConfNumberLast;
                site.ConfNumberNext = siteData.ConfNumberNext;
                site.POOverLimit = siteData.POOverLimit;

                data.SaveChanges();
            }
        }

        private static void GetCompanyData(SaltScaleEntities data, CompanyData company)
        {
            var props = data.ticketProps.AsNoTracking().Where(p =>
                p.Name == ("FirstTicket " + company.Name)
                || p.Name == ("LastTicket " + company.Name)
                || p.Name == ("NextTicket " + company.Name)
                ).ToList();

            var firstTicket = props.FirstOrDefault(p => p.Name == ("FirstTicket " + company.Name));
            var lastTicket = props.FirstOrDefault(p => p.Name == ("LastTicket " + company.Name));
            var nextTicket = props.FirstOrDefault(p => p.Name == ("NextTicket " + company.Name));

            company.FirstTicket = 0;
            if (firstTicket != null && firstTicket.ValueNumber.HasValue)
                company.FirstTicket = firstTicket.ValueNumber.Value;

            company.LastTicket = 0;
            if (lastTicket != null && lastTicket.ValueNumber.HasValue)
                company.LastTicket = lastTicket.ValueNumber.Value;

            company.NextTicket = 0;
            if (nextTicket != null && nextTicket.ValueNumber.HasValue)
                company.NextTicket = nextTicket.ValueNumber.Value;

            company.ExportPath = GetConfigItem(company.Name, "companypath", data);
            company.BackupPath = GetConfigItem(company.Name, "backuppath", data);
            company.ExportFile = GetConfigItem(company.Name, "companyfile", data);
        }

        public IReadOnlyList<ISiteInfo> GetSites(int seasonId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                return
                    data.Sites.AsNoTracking()
                        .Where(s => s.SeasonId == seasonId)
                        .Select(SiteInfo.Transform)
                        .OrderBy(s => s.Name)
                        .ToList();
            }
        }

        public ISiteInfo GetSite(int siteId, int seasonId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var site = data.Sites.AsNoTracking().FirstOrDefault(s => s.SeasonId == seasonId && s.SiteId == siteId);

                if (site == null)
                {
                    throw new EntityNotFoundException("No site found.", typeof(Site), siteId);
                }

                return new SiteInfo(site);
            }
        }

        public ISiteInfo GetSite(string siteName, int seasonId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var site = data.Sites.AsNoTracking().FirstOrDefault(s => s.SeasonId == seasonId && s.SiteName == siteName);

                if (site == null)
                {
                    throw new EntityNotFoundException("No site found.", typeof(Site), siteName);
                }

                return new SiteInfo(site);
            }
        }


        public bool IsSeasonExist(string seasonName)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                return data.Seasons.AsNoTracking().Any(s => s.Prefix == seasonName);
            }
        }

        public ISeason GetActiveSeason()
        {
            return GetActiveSeason(_configuration.ConnectionString);
        }

        public IReadOnlyList<ICompanyInfo> GetCompanies(string connectionString)
        {
            using (var data = new SaltScaleEntities(connectionString))
            {
                return data.Companies.AsNoTracking().Select(CompanyInfo.Transform).OrderBy(c => c.Name).ToList();
            }
        }

        public ISeason GetActiveSeason(string connectionString)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                return data.Seasons.AsNoTracking().Where(s => s.IsActive).Select(Season.Transform).FirstOrDefault();
            }
        }

        public void MoveToNewSeason(ISeason selectedSeason, string seasonName)
        {
            try
            {
                _eventAggregator.PublishOnUIThreadAsync(new NewSeasonProgressArgs(SeasonStep.Start, "Started"));
                _log.Info("Move to new season process started.");
                var errors = new List<string>();
                using (var data = new SaltScaleEntities(_configuration.ConnectionString))
                {
                    data.sp_MoveToNewSeason(selectedSeason.SeasonId, seasonName);

                    var materialExceptions = data.Bids.AsNoTracking()
                        .Where(b => b.IsActive && b.Season.Prefix == seasonName && !b.Material.IsActive)
                        .Select(b => b.Customer.DispatcherName)
                        .ToList();

                    var customerExceptions = data.Customers.AsNoTracking()
                        .Where(
                            c =>
                                c.CustomerContacts.Count(cc => cc.Contact.IsActive && cc.Contact.isPrimary) > 1 &&
                                c.Season.Prefix == seasonName && c.IsActive)
                        .Select(c => c.DispatcherName)
                        .ToList();

                    var bidExceptions =
                        data.Customers.AsNoTracking()
                            .Where(
                                c =>
                                    c.Season.Prefix == seasonName && c.IsActive && !c.Bids.Any())
                            .Select(c => c.DispatcherName)
                            .ToList();

                    if (materialExceptions.Any())
                    {
                        errors.Add("--------------------------------------------");
                        errors.Add(string.Format("Next customers have BIDs associated with deleted material:{0} - {1}",
                            Environment.NewLine, string.Join(Environment.NewLine + " - ", materialExceptions)));
                    }
                    if (customerExceptions.Any())
                    {
                        errors.Add("--------------------------------------------");
                        errors.Add(string.Format("Next customers have more than 1 primary contact:{0} - {1}",
                            Environment.NewLine, string.Join(Environment.NewLine + " - ", customerExceptions)));
                    }
                    if (bidExceptions.Any())
                    {
                        errors.Add("--------------------------------------------");
                        errors.Add(string.Format("Next customers have no BIDs:{0} - {1}",
                            Environment.NewLine, string.Join(Environment.NewLine + " - ", bidExceptions)));
                    }

                    if (errors.Any())
                    {
                        errors.Add("--------------------------------------------");
                        var message = string.Format("Move to new season process done with warnings:{0}{1}",
                            Environment.NewLine, string.Join(Environment.NewLine, errors));

                        _log.Info(message);
                        _eventAggregator.PublishOnUIThreadAsync(new NewSeasonProgressArgs(SeasonStep.Warning,
                            message));
                    }
                    else
                    {
                        _log.Info("Move to new season process completed.");
                        _eventAggregator.PublishOnUIThreadAsync(new NewSeasonProgressArgs(SeasonStep.End, "Done"));
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                _eventAggregator.PublishOnUIThreadAsync(new NewSeasonProgressArgs(SeasonStep.FatalException,
                    "Fatal Exception: " + ex.Message + ". Check log file for exception " + DateTime.Now));
            }
        }


        public ObservableCollection<IReportingEntity> GetReportingEntities(int season)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                return data.Customers.AsNoTracking()
                    .Where(customer => customer.IsActive
                                       && !customer.isShippingLocation && customer.SeasonId == season)
                    .OrderBy(customer => customer.DispatcherName)
                    .Select(ReportingEntity.Transform).ToObservableList<IReportingEntity>();
            }
        }

        public ObservableCollection<IReportingEntityL2> GetReportingEntitiesL2(int season)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                return data.Customers.AsNoTracking()
                    .Where(customer => customer.IsActive
                                       && !customer.isShippingLocation && customer.SeasonId == season)
                    .OrderBy(customer => customer.DispatcherName)
                    .Select(ReportingEntityL2.Transform).ToObservableList<IReportingEntityL2>();
            }
        }
        public ObservableCollection<IReportingEntityL3> GetReportingEntitiesL3(int season)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                return data.Customers.AsNoTracking()
                    .Where(customer => customer.IsActive
                                       && !customer.isShippingLocation && customer.SeasonId == season)
                    .OrderBy(customer => customer.DispatcherName)
                    .Select(ReportingEntityL3.Transform).ToObservableList<IReportingEntityL3>();
            }
        }



        public ISeason GetSeasonById(int seasonId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                return
                    data.Seasons.AsNoTracking()
                        .Where(s => s.SeasonId == seasonId)
                        .Select(Season.Transform)
                        .FirstOrDefault();
            }
        }

        public string GetConfigItem(string companyName, string itemName)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                return GetConfigItem(companyName, itemName, data);
            }
        }

        private static string GetConfigItem(string companyName, string itemName, SaltScaleEntities data)
        {
            string key = string.Format("{0} {1}", itemName, companyName);
            var configuration = data.configurations.AsNoTracking().FirstOrDefault(c => c.name == key);

            return configuration != null ? configuration.valuestring : string.Empty;
        }

        private static void UpdateTicketPropItem(string companyName, string itemName, int? item, SaltScaleEntities data)
        {
            string key = string.Format("{0} {1}", itemName, companyName);
            var configuration = data.ticketProps.FirstOrDefault(c => c.Name == key);

            if (configuration == null)
            {
                configuration = data.ticketProps.Create();
                data.ticketProps.Add(configuration);
            }

            configuration.Name = key;
            configuration.ValueNumber = item;
        }

        public bool AuthorizeUser(string password)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                password = password.Trim();

                return data.SecurityKeys.Any(s => s.SecurityKey1 == password);
            }
        }

        #region DTO

        private class Season : ISeason
        {
            public static readonly Expression<Func<DataAccess.Season, ISeason>> Transform = e => new Season
            {
                SeasonId = e.SeasonId,
                StartDate = e.StartDate,
                EndDate = e.EndDate,
                Prefix = e.Prefix,
                IsActive = e.IsActive
            };

            public bool IsActive { get; set; }

            public string Prefix { get; set; }

            public DateTime EndDate { get; set; }

            public DateTime StartDate { get; set; }

            public int SeasonId { get; set; }
        }

        internal class ReportingEntity : IReportingEntity
        {
            public static readonly Expression<Func<Customer, IReportingEntity>> Transform = e => new ReportingEntity
            {
                Id = e.CustomerID,
                Name = e.DispatcherName,
                ShortName = e.ShortName,
                SeasonMaterialEstimateTon = e.SeasonMaterialEstimateTon,
            };

            public int Id { get; set; }
            public string Name { get; set; }
            public string ShortName { get; set; }
            public decimal? SeasonMaterialEstimateTon { get; set; }
        }

        internal class ReportingEntityL2 : IReportingEntityL2
        {
            public static readonly Expression<Func<Customer, IReportingEntityL2>> Transform = e => new ReportingEntityL2
            {
                Id = e.CustomerID,
                Name = e.DispatcherName,
                ShortName = e.ShortName,
                SeasonMaterialEstimateTon = e.SeasonMaterialEstimateTon,
            };

            public int Id { get; set; }
            public string Name { get; set; }
            public string ShortName { get; set; }
            public decimal? SeasonMaterialEstimateTon { get; set; }
        }

        internal class ReportingEntityL3 : IReportingEntityL3
        {
            public static readonly Expression<Func<Customer, IReportingEntityL3>> Transform = e => new ReportingEntityL3
            {
                Id = e.CustomerID,
                Name = e.DispatcherName,
                ShortName = e.ShortName,
                SeasonMaterialEstimateTon = e.SeasonMaterialEstimateTon,
            };

            public int Id { get; set; }
            public string Name { get; set; }
            public string ShortName { get; set; }
            public decimal? SeasonMaterialEstimateTon { get; set; }
        }

        internal class CompanyInfo : ICompanyInfo
        {
            public static readonly Expression<Func<Company, ICompanyInfo>> Transform = e => new CompanyInfo
            {
                Id = e.CompanyId,
                Name = e.FullName,
                PrinterTicketName = e.PrinterPort,
                AddressLineOne = e.AddressLineOne,
                AddressLineTwo = e.AddressLineTwo,
                City = e.City,
                State = e.State,
                Zip = e.Zip,
                PhoneNumberOne = e.PhoneNumberOne,
                PhoneNumberTwo = e.PhoneNumberTwo,
                FaxNumber = e.FaxNumber
            };

            public CompanyInfo()
            {

            }

            public CompanyInfo(Company item)
            {
                Id = item.CompanyId;
                Name = item.FullName;
                PrinterTicketName = item.PrinterPort;
            }

            public string PrinterTicketName { get; set; }

            public string Name { get; set; }

            public int Id { get; set; }
            public string AddressLineOne { get; set; }
            public string AddressLineTwo { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public string Zip { get; set; }
            public string PhoneNumberOne { get; set; }
            public string PhoneNumberTwo { get; set; }
            public string FaxNumber { get; set; }
        }

        internal class CompanyData : ICompanyData
        {
            public static readonly Expression<Func<Company, CompanyData>> Transform = e => new CompanyData
            {
                Id = e.CompanyId,
                Name = e.FullName,
                PrinterTicketName = e.PrinterPort,
                AddressLineOne = e.AddressLineOne,
                AddressLineTwo = e.AddressLineTwo,
                City = e.City,
                State = e.State,
                Zip = e.Zip,
                PhoneNumberOne = e.PhoneNumberOne,
                PhoneNumberTwo = e.PhoneNumberTwo,
                FaxNumber = e.FaxNumber
            };

            public string PrinterTicketName { get; set; }
            public string Name { get; set; }
            public int Id { get; set; }
            public string AddressLineOne { get; set; }
            public string AddressLineTwo { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public string Zip { get; set; }
            public string PhoneNumberOne { get; set; }
            public string PhoneNumberTwo { get; set; }
            public string FaxNumber { get; set; }
            public int FirstTicket { get; set; }
            public int LastTicket { get; set; }
            public int NextTicket { get; set; }
            public string ExportPath { get; set; }
            public string BackupPath { get; set; }
            public string ExportFile { get; set; }
        }

        #endregion
    }

    internal class SiteInfo : ISiteInfo
    {
        public static readonly Expression<Func<Site, ISiteInfo>> Transform = e => new SiteInfo
        {
            Id = e.SiteId,
            Name = e.SiteName,
            IsOperatingFromThisLocation = e.IsOperatingFromThisLocation,
            SeasonId = e.SeasonId,
            DefaultMaterialCost = e.MaterialCost,
            WeightInterval = e.WeightInterval,
            TaxRate = e.TaxRate,
            DefaultCompanyId = e.DefaultCompanyId,
            DefaultCashCustomerCompanyId = e.DefaultCashCustomerCompanyId,
            FuelSurcharge = e.FuelSurcharge,
            ConfNumberFirst = e.ConfNumberFirst,
            ConfNumberLast = e.ConfNumberLast,
            ConfNumberNext = e.ConfNumberNext,
            POOverLimit = e.POOverLimit
        };

        public decimal FuelSurcharge { get; set; }

        public int? DefaultCompanyId { get; set; }

        public int? DefaultCashCustomerCompanyId { get; set; }

        public SiteInfo()
        {

        }

        public SiteInfo(Site site)
        {
            Id = site.SiteId;
            Name = site.SiteName;
            IsOperatingFromThisLocation = site.IsOperatingFromThisLocation;
            SeasonId = site.SeasonId;
            DefaultMaterialCost = site.MaterialCost;
            WeightInterval = site.WeightInterval;
            TaxRate = site.TaxRate;
            DefaultCompanyId = site.DefaultCompanyId;
            DefaultCashCustomerCompanyId = site.DefaultCashCustomerCompanyId;
            ConfNumberFirst = site.ConfNumberFirst;
            ConfNumberLast = site.ConfNumberLast;
            ConfNumberNext = site.ConfNumberNext;
            POOverLimit = site.POOverLimit;
        }

        public int ConfNumberNext { get; set; }

        public int ConfNumberLast { get; set; }

        public int ConfNumberFirst { get; set; }

        public decimal POOverLimit { get; set; }

        public decimal TaxRate { get; set; }

        public int WeightInterval { get; set; }

        public decimal DefaultMaterialCost { get; set; }

        public int SeasonId { get; set; }

        public bool IsOperatingFromThisLocation { get; set; }

        public string Name { get; set; }

        public int Id { get; set; }
    }

    public static class ExtensionMethods
    {
        public static ObservableCollection<T> ToObservableList<T>(this IEnumerable<T> data)
        {
            ObservableCollection<T> dataToReturn = new ObservableCollection<T>();

            foreach (T t in data)
                dataToReturn.Add(t);

            return dataToReturn;
        }
    }
}