﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Contract.Domain;
using Contract.Exception;
using Contract.Services;
using DataAccess;

namespace BusinessLogic
{
    public class OrderService : IOrderService
    {
        private readonly IConfiguration _configuration;

        public OrderService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public List<IOrderSummary> GetFilteredOrders(string filter, int seasonId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var query = data.OrderTables.AsNoTracking()
                    .Where(
                        order =>
                            order.SeasonId == seasonId
                            && order.Material.MaterialID == order.MaterialId
                            && !order.IsVoid
                            && order.IsActive
                     );

                if (!string.IsNullOrWhiteSpace(filter))
                {
                    filter = filter.Trim();
                    query = query.Where(
                        order =>
                            (order.Bid.Customer.ShortName.Contains(filter)
                             || order.Bid.Customer.DispatcherName.Contains(filter)
                             || order.POOverride.Contains(filter)
                             || order.OrderNumber.ToString().Contains(filter)
                             || order.EffectiveTime.ToString().Contains(filter)
                             ) && !order.IsVoid);
                }

                return query.Select(OrderSummary.Transform).OrderBy(o => o.CustomerName).ToList();
            }
        }

        // Variation with filters for CustomerID
        public List<IOrderSummary> GetFilteredOrders(string filter, int customerId, int seasonId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                DateTime filterDateTime;
                DateTime.TryParse(filter, out filterDateTime);
                return data.OrderTables.AsNoTracking()
                    .Where(order => order.IsActive
                                    && order.SeasonId == seasonId
                                    && !order.IsVoid
                                    && (order.OrderNumber.ToString().Contains(filter)
                                        ||
                                        (order.Bid.Customer.CustomerID == customerId &&
                                         (filterDateTime.TimeOfDay.TotalMinutes == 0
                                             ? DbFunctions.DiffDays(order.EffectiveTime, filterDateTime) == 0
                                             : DbFunctions.DiffMinutes(order.EffectiveTime, filterDateTime) == 0))
                                        || order.POOverride.Contains(filter)))
                .Select(OrderSummary.Transform).OrderBy(o => o.CustomerName).ToList();
            }
        }

        // Variation with filters for status
        public List<IOrderSummary> GetFilteredOrders(string filter, int customerId, int seasonId, string status)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                if (status == "open")
                {
                    IQueryable<OrderTable> sql = data.OrderTables.AsNoTracking()
                        .Where(
                            order =>
                                order.IsActive
                                && order.SeasonId == seasonId
                                && (order.Bid.Customer.CustomerID == customerId));
                    return sql.Select(OrderSummary.Transform).OrderBy(o => o.CustomerName).ToList();
                }
                else if (status == "completed")
                {
                    IQueryable<OrderTable> sql = data.OrderTables.AsNoTracking()
                        .Where(
                            order =>
                                order.isComplete
                                && order.SeasonId == seasonId
                                && (order.Bid.Customer.CustomerID == customerId));
                    return sql.Select(OrderSummary.Transform).OrderBy(o => o.CustomerName).ToList();
                }
                else if (status == "closed")
                {
                    IQueryable<OrderTable> sql = data.OrderTables.AsNoTracking()
                        .Where(
                            order =>
                                (order.isComplete && !order.IsActive)
                                && order.SeasonId == seasonId
                                && (order.Bid.Customer.CustomerID == customerId));
                    return sql.Select(OrderSummary.Transform).OrderBy(o => o.CustomerName).ToList();
                }
                else if (status == "deleted")
                {
                    IQueryable<OrderTable> sql = data.OrderTables.AsNoTracking()
                        .Where(
                            order =>
                                order.IsVoid
                                && order.SeasonId == seasonId
                                && (order.Bid.Customer.CustomerID == customerId));
                    return sql.Select(OrderSummary.Transform).OrderBy(o => o.CustomerName).ToList();
                }
                else
                {
                    // All orders by default
                    IQueryable<OrderTable> sql = data.OrderTables.AsNoTracking()
                        .Where(
                            order =>
                                order.SeasonId == seasonId
                                && (order.Bid.Customer.CustomerID == customerId));
                    return sql.Select(OrderSummary.Transform).OrderBy(o => o.CustomerName).ToList();
                }
            }
        }

        public List<IOrderSummary> GetOrders(int customerId, int seasonId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                IQueryable<OrderTable> sql = data.OrderTables.AsNoTracking()
                    .Where(
                        order =>
                            //order.IsActive && 
                            order.SeasonId == seasonId
                               //&& (!order.isComplete || (order.HeldUntilDate >= DateTime.Today))
                            && (order.Bid.Customer.CustomerID == customerId));
                return sql.Select(OrderSummary.Transform).OrderBy(o => o.CustomerName).ToList();
            }
        }

        public IOrder GetNewOrder(int seasonId, int customerId, string siteName)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                Site site = data.Sites.FirstOrDefault(s => s.SiteName == siteName && s.SeasonId==seasonId);

                if (site == null)
                    throw new EntityNotFoundException("Site, defined in configuration, not found in database.",
                        typeof (Site), siteName);

                // 2018-JUl-17, JGS - Make explicit use of the SeasonID
                //Bid bid = data.Bids.FirstOrDefault(b => b.IsActive && b.CustomerID == customerId);
                Bid bid = data.Bids.FirstOrDefault(b => b.IsActive && b.CustomerID == customerId && b.SeasonId ==seasonId);

                if (bid == null)
                    throw new EntityNotFoundException("There are no Bid for this customer.", typeof (Bid), customerId);

                Company company = data.Companies.AsNoTracking().FirstOrDefault(c => c.CompanyId == bid.Customer.CompanyId);
                if (company == null)
                    throw new EntityNotFoundException("There are no companies defined in database.", typeof (Company),
                        "default company");

                OrderTable order = data.OrderTables.Create();
                order.Bid = bid;
                order.Bid.BidID = bid.BidID;
                order.SeasonId = seasonId;
                order.IsActive = false;
                order.EffectiveTime = DateTime.Now;
                order.Site = site;
                order.SiteID = site.SiteId;
                order.CreateTime = DateTime.Now;
                order.UpdateTime = DateTime.Now;
                order.MaterialId = bid.MaterialID;
                order.IsVoid = false;

                order.ConfirmationNumber = site.ConfNumberNext;
                site.ConfNumberNext = site.ConfNumberNext + 1;

                
                if (!string.IsNullOrWhiteSpace(bid.POOverride))
                {
                    var purchaseOrder =
                        data.PurchaseOrders.AsNoTracking().FirstOrDefault(p => p.PurchaseOrderNumber == bid.POOverride
                                                                               && p.IsActive 
                                                                               && p.CustomerID == bid.CustomerID
                                                                               && (!p.isComplete || p.isHeld));
                    if (purchaseOrder != null)
                        order.MaterialId = purchaseOrder.MaterialID;
                }

                data.OrderTables.Add(order);

                data.SaveChanges();

                string key = string.Format("NextTicket {0}", company.FullName);

                int? nextTicketId =
                    data.ticketProps.AsNoTracking().Where(tp => tp.Name == key).Select(tp => tp.ValueNumber)
                        .FirstOrDefault();

                order.OrderNumber = order.OrderID + nextTicketId;
                data.SaveChanges();

                return new OrderData(order);
            }
        }

        public void SaveOrder(IOrder order)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                OrderTable orderTable = data.OrderTables.FirstOrDefault(o => o.OrderID == order.OrderId);
                if (orderTable == null)
                    //TODO Log
                    throw new NullReferenceException("Not able to Save Order.  OrderService:SaveOrder");

                DateTime updateTime = order.UpdateTime.HasValue
                    ? order.UpdateTime.Value
                    : DateTime.Now;

                orderTable.BidID = order.BidId;
                orderTable.UpdateTime = updateTime;
                orderTable.Comment = order.Comment;
                orderTable.POOverride = order.POOverride;
                orderTable.EffectiveTime = order.EffectiveTime;
                orderTable.HeldUntilDate = order.HeldUntilDate;
                orderTable.OrderedBy = order.OrderedBy;
                orderTable.Priority = order.Priority;
                orderTable.Promise = order.Promise;
                orderTable.PromiseExpiresTime = order.PromiseExpiresTime;
                orderTable.SpecialInstructionOverride = order.SpecialInstructionOverride;
                orderTable.IsActive = order.IsActive;
                orderTable.SeasonId = order.SeasonId;  // 2018-JUl-17, JGS - Make explicit use of the SeasonID
                orderTable.MaterialId = order.Material.Id;
                orderTable.ConfirmationNumber = order.ConfirmationNumber;
                orderTable.IsVoid = order.IsVoid;

                // 20180627, JGS - Assign "complete" status
                orderTable.isComplete = order.IsComplete;
                //if (order.IsComplete != orderTable.isComplete)
                //{
                //    orderTable.isComplete = order.IsComplete;
                //    //orderTable.CompleteDate = DateTime.Now;
                //}

                var quantity = orderTable.OrderUpdates.Where(ou => ou.IsActive).Sum(o => o.Quantity);

                if (quantity != order.Quantity)
                {
                    OrderUpdate newOrderUpdate = data.OrderUpdates.Create();
                    newOrderUpdate.IsActive = true;

                    newOrderUpdate.CreateTime = updateTime;
                    newOrderUpdate.Quantity = order.Quantity*2000 - quantity;
                    newOrderUpdate.UpdateTime = updateTime;

                    orderTable.OrderUpdates.Add(newOrderUpdate);
                }
                data.SaveChanges();
            }
        }

        public void SaveOrder_Reopen(IOrder order)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                OrderTable orderTable = data.OrderTables.FirstOrDefault(o => o.OrderID == order.OrderId);
                if (orderTable == null)
                    //TODO Log
                    throw new NullReferenceException("Not able to Save Order.  OrderService:SaveOrder");

                DateTime updateTime = order.UpdateTime.HasValue
                    ? order.UpdateTime.Value
                    : DateTime.Now;

                orderTable.BidID = order.BidId;
                orderTable.UpdateTime = updateTime;
                orderTable.Comment = order.Comment;
                orderTable.POOverride = order.POOverride;
                orderTable.EffectiveTime = order.EffectiveTime;
                orderTable.HeldUntilDate = order.HeldUntilDate;
                orderTable.OrderedBy = order.OrderedBy;
                orderTable.Priority = order.Priority;
                orderTable.Promise = order.Promise;
                orderTable.PromiseExpiresTime = order.PromiseExpiresTime;
                orderTable.SpecialInstructionOverride = order.SpecialInstructionOverride;
                orderTable.SeasonId = order.SeasonId;   // 2018-JUl-17, JGS - Make explicit use of the SeasonID
                orderTable.MaterialId = order.Material.Id;
                orderTable.ConfirmationNumber = order.ConfirmationNumber;
                orderTable.IsActive = order.IsActive;
                orderTable.IsVoid = order.IsVoid;
                orderTable.isComplete = order.IsComplete;
                orderTable.CompleteDate = null;

                var quantity = orderTable.OrderUpdates.Where(ou => ou.IsActive).Sum(o => o.Quantity);

                // Make sure the default quantity is valid
                //if (quantity > order.Quantity)
                //{
                //    // Reset order quality
                //    order.Quantity = (int)((1.5 * quantity) / 2000);

                //    OrderUpdate newOrderUpdate = data.OrderUpdates.Create();
                //    newOrderUpdate.IsActive = true;

                //    newOrderUpdate.CreateTime = updateTime;
                //    newOrderUpdate.Quantity = ((order.Quantity * 2000) - quantity);
                //    newOrderUpdate.UpdateTime = updateTime;

                //    orderTable.OrderUpdates.Add(newOrderUpdate);
                //}

                if (quantity != order.Quantity)
                {
                    OrderUpdate newOrderUpdate = data.OrderUpdates.Create();
                    newOrderUpdate.IsActive = true;

                    newOrderUpdate.CreateTime = updateTime;
                    newOrderUpdate.Quantity = order.Quantity * 2000 - quantity;
                    newOrderUpdate.UpdateTime = updateTime;

                    orderTable.OrderUpdates.Add(newOrderUpdate);
                }
                data.SaveChanges();
            }
        }

        public IOrder GetOrder(int seasonId, int orderId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                //OrderTable order = data.OrderTables.FirstOrDefault(o => o.IsActive && o.SeasonId == seasonId && o.OrderID == orderId);
                OrderTable order = data.OrderTables.FirstOrDefault(o => o.SeasonId == seasonId && o.OrderID == orderId);
                if (order == null)
                {
                    throw new Exception("No Order found");
                }

                return new OrderData(order);
            }
        }

        public bool IsPurchaseOrderQuantityAcceptable(int purchaseOrderId, string poNumber, int materialId, int orderId, int newOrderQuantity, int customerId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                // 2018-Jul-22, JGS - Get the currently active season
                var currentSeason = data.Seasons.FirstOrDefault(s => s.IsActive == true);

                var purchaseOrder = data.PurchaseOrders.FirstOrDefault(p => p.PurchaseOrderID == purchaseOrderId && p.IsActive);
                if (purchaseOrder == null)
                    return true;

                // 2018-Jul-22, JGS - Get the currently active customer
                var order = data.OrderTables.FirstOrDefault(o => o.OrderID == orderId);

                // Get the POOverLimit value for the current site
                var site = data.Sites.FirstOrDefault(s => s.SiteId == order.SiteID);
                decimal poOverLimit = Convert.ToDecimal(site.POOverLimit);

                //-- Looks at all orders associated with the PO
                //-- Determines if there is a difference in quantity of the order from the db to current screen value
                var quantitiesOfOtherOrdersWithThisPO = data.OrderUpdates.Where(ou => ou.OrderID != orderId
                                                    && ou.OrderTable.Bid.CustomerID == customerId
                                                    && ou.OrderTable.Bid.MaterialID == materialId
                                                    && ou.OrderTable.POOverride == poNumber
                                                    && ou.OrderTable.IsActive).Select(p => p.Quantity).ToList();

                var isEditOrderHaveSamePOValueInTable = data.OrderUpdates.Where(o => o.OrderID == orderId
                                                                            && o.OrderTable.POOverride == poNumber
                                                                            && o.OrderTable.MaterialId == materialId
                                                                            && o.IsActive).Any();

                var quantityOfCurrentOrder = data.OrderUpdates.Where(o => o.OrderID == orderId
                                                                            && o.OrderTable.MaterialId == materialId
                                                                            && o.IsActive).Select(p => p.Quantity).ToList();

                var sum = 0;

                if (isEditOrderHaveSamePOValueInTable)
                {
                    if (newOrderQuantity != (quantityOfCurrentOrder.Sum() / 2000))
                    {
                        sum = quantitiesOfOtherOrdersWithThisPO.Sum() / 2000 + newOrderQuantity;
                    }
                    else
                    {
                        sum = quantitiesOfOtherOrdersWithThisPO.Sum() / 2000 + (quantityOfCurrentOrder.Sum() / 2000);
                    }
                }
                else
                {
                    sum = (quantitiesOfOtherOrdersWithThisPO.Sum() / 2000) + newOrderQuantity;
                }

                // 2019-Mar-17, JGS - Ignore the OVERLIMIT Restrictiion
                //// 2018-Jul-22, JGS - Allow the sum to go over by PO Limit by as much as (100 * (poOverlimit - 1.00)) percent
                ////if (sum > purchaseOrder.Quantity)
                //if (sum > (poOverLimit * purchaseOrder.Quantity))
                //    return false;

                // If you get to this point, return True
                return true;
            }
        }

        public double PurchaseOrderOverageValue(int purchaseOrderId, string poNumber, int materialId, int orderId, int newOrderQuantity, int customerId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                // Get the currently active season
                var currentSeason = data.Seasons.FirstOrDefault(s => s.IsActive == true);

                var purchaseOrder = data.PurchaseOrders.FirstOrDefault(p => p.PurchaseOrderID == purchaseOrderId && p.IsActive);
                if (purchaseOrder == null)
                    return 0;

                // Get the currently active customer
                var order = data.OrderTables.FirstOrDefault(o => o.OrderID == orderId);
                double poQuanity = purchaseOrder.Quantity;

                // Get the POOverLimit value for the current site
                var site = data.Sites.FirstOrDefault(s => s.SiteId == order.SiteID);
                double poOverLimit = Convert.ToDouble(site.POOverLimit);


                // Return the calculated value
                return (poOverLimit * poQuanity);
            }
        }

        public double PurchaseOrderInOverage(int purchaseOrderId, string poNumber, int materialId, int orderId, int newOrderQuantity, int customerId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                // 2018-Jul-22, JGS - Get the currently active season
                var currentSeason = data.Seasons.FirstOrDefault(s => s.IsActive == true);

                var purchaseOrder = data.PurchaseOrders.FirstOrDefault(p => p.PurchaseOrderID == purchaseOrderId && p.IsActive);
                if (purchaseOrder == null)
                    return 0;

                // 2018-Jul-22, JGS - Get the currently active customer
                var order = data.OrderTables.FirstOrDefault(o => o.OrderID == orderId);

                // Get the POOverLimit value for the current site
                var site = data.Sites.FirstOrDefault(s => s.SiteId == order.SiteID);
                decimal poOverLimit = Convert.ToDecimal(site.POOverLimit);

                //-- Looks at all orders associated with the PO
                //-- Determines if there is a difference in quantity of the order from the db to current screen value
                var quantitiesOfOtherOrdersWithThisPO = data.OrderUpdates.Where(ou => ou.OrderID != orderId
                                                    && ou.OrderTable.Bid.CustomerID == customerId
                                                    && ou.OrderTable.Bid.MaterialID == materialId
                                                    && ou.OrderTable.POOverride == poNumber
                                                    && ou.OrderTable.IsActive).Select(p => p.Quantity).ToList();

                var isEditOrderHaveSamePOValueInTable = data.OrderUpdates.Where(o => o.OrderID == orderId
                                                                            && o.OrderTable.POOverride == poNumber
                                                                            && o.OrderTable.MaterialId == materialId
                                                                            && o.IsActive).Any();

                var quantityOfCurrentOrder = data.OrderUpdates.Where(o => o.OrderID == orderId
                                                                            && o.OrderTable.MaterialId == materialId
                                                                            && o.IsActive).Select(p => p.Quantity).ToList();

                var sum = 0;

                if (isEditOrderHaveSamePOValueInTable)
                {
                    if (newOrderQuantity != (quantityOfCurrentOrder.Sum() / 2000))
                    {
                        sum = quantitiesOfOtherOrdersWithThisPO.Sum() / 2000 + newOrderQuantity;
                    }
                    else
                    {
                        sum = quantitiesOfOtherOrdersWithThisPO.Sum() / 2000 + (quantityOfCurrentOrder.Sum() / 2000);
                    }
                }
                else
                {
                    sum = (quantitiesOfOtherOrdersWithThisPO.Sum() / 2000) + newOrderQuantity;
                }

                // 2018-Oct-03, JGS - Determine if the value is in the "overage" range
                if ((sum > purchaseOrder.Quantity) && (sum <= (poOverLimit * purchaseOrder.Quantity)))
                    return sum;

                // If you get to this point, return false
                return 0;
            }
        }

        public bool IsPOValid(string poNumber, int materialId, int orderId, int customerId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                return data.PurchaseOrders.Any(p => p.PurchaseOrderNumber == poNumber
                                                    && p.MaterialID == materialId 
                                                    && p.IsActive 
                                                    && p.CustomerID == customerId
                                               );
            }
        }

        public bool DeleteOrder(int orderId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                OrderTable order = data.OrderTables.Find(orderId);
                if (order == null)
                {
                    return false;
                }

                //Deactivate the Order
                order.IsActive = false;
                order.IsVoid = true;
                order.UpdateTime = DateTime.Now;
                data.SaveChanges();
                return true;
            }
        }

        public void CloseCompletedOrders(int seasonId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var orderTables = data.OrderTables.Where(
                    o =>
                        o.isComplete 
                        && o.IsActive 
                        && o.SeasonId == seasonId  // 2018-JUl-17, JGS - Make explicit use of the SeasonID
                        && !o.CompleteDate.HasValue
                ).ToList();

                foreach (var order in orderTables)
                {
                    order.isComplete = true;
                    order.IsActive = false;
                    order.UpdateTime = DateTime.Now;
                    order.CompleteDate = DateTime.Now;
                }

                data.SaveChanges();
            }
        }

        public void CloseRemainingOrders(float tons, int seasonId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var orderTables = data.OrderTables.Where(
                    o =>
                        o.IsActive && o.SeasonId == seasonId &&  // 2018-JUl-17, JGS - Make explicit use of the SeasonID
                        (o.OrderUpdates.Where(u => u.IsActive).Sum(u => u.Quantity) -
                         o.Tickets.Where(t => t.IsActive && !t.isVoid).Sum(t => t.Quantity))/2000 <= tons)
                .ToList();

                foreach (var order in orderTables)
                {
                    order.isComplete = true;
                    order.IsActive = false;
                    order.UpdateTime = DateTime.Now;
                    order.CompleteDate = DateTime.Now;
                }

                data.SaveChanges();
            }
        }

        public float GetOrderBalance(int orderId, int seasonId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                // 2018-JUl-17, JGS - Make explicit use of the SeasonID
                var order = data.OrderTables.AsNoTracking().FirstOrDefault(o => o.IsActive && o.SeasonId == seasonId && o.OrderID == orderId);

                if (order == null)
                    throw new Exception("No Order found");

                var requested = order.OrderUpdates.Where(u => u.IsActive).Sum(u => u.Quantity);
                var delivered = order.Tickets.Where(t => t.IsActive && !t.isVoid).Sum(t => t.Quantity);
                if (delivered < 0) delivered = 0;

                return (requested - delivered)/2000;
            }
        }

        // 2018-Jun-13, JGS - Added to provide a mechanism for determine order by pass
        public float GetOrderRequestedTotal(int orderId, int seasonId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                // 2018-JUl-17, JGS - Make explicit use of the SeasonID
                var order = data.OrderTables.AsNoTracking().FirstOrDefault(o => o.IsActive && o.SeasonId == seasonId && o.OrderID == orderId);

                if (order == null)
                    throw new Exception("No Order found");

                var requested = order.OrderUpdates.Where(u => u.IsActive).Sum(u => u.Quantity);

                return (requested) / 2000;
            }
        }

        // 2018-Jun-15, JGS - Added to provide a mechanism for determine order by pass
        public float GetSitePOOverLimit(int seasonId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                // 2018-JUl-17, JGS - Make explicit use of the SeasonID
                var pooverlimit = data.Sites.AsNoTracking().FirstOrDefault(s => s.SiteName == _configuration.Site.Name && s.SeasonId == seasonId);

                if (pooverlimit == null)
                    throw new Exception("Site Not found");

                var requested = pooverlimit.POOverLimit;

                return ((float)requested);
            }
        }

        public List<int?> GetOrderNumbersForConfNumber(int confNumber, int orderId, int seasonId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                return data.OrderTables.Where(
                    o => o.IsActive && o.SeasonId == seasonId && o.OrderID != orderId && o.ConfirmationNumber == confNumber).Select(o=>o.OrderNumber).ToList();
            }
        }
    }

    #region DTO

    internal class OrderSummary : IOrderSummary
    {
        public static readonly Expression<Func<OrderTable, IOrderSummary>> Transform = e => new OrderSummary
        {
            OrderId = e.OrderID,
            SpecialInstructionOverride = e.SpecialInstructionOverride,
            Priority = e.Priority,
            BidId = e.BidID,
            CompleteDate = e.CompleteDate,
            EffectiveTime = e.EffectiveTime,
            HeldUntilDate = e.HeldUntilDate,
            IsComplete = e.isComplete,
            OrderNumber = e.OrderNumber,
            OrderedBy = e.OrderedBy,
            PoOverride = e.POOverride,
            Promise = e.Promise,
            PromiseExpiresTime = e.PromiseExpiresTime,
            CreateTime = e.CreateTime,
            IsActive = e.IsActive,
            SiteId = e.SiteID,
            UpdateTime = e.UpdateTime,
            _quantity = e.OrderUpdates.Where(o => o.IsActive).Sum(o => o.Quantity),
            Tickets = e.Tickets.Where(t => t.IsActive && !t.isVoid),
            Material = e.Material.ShortName,
            CustomerId = e.Bid.CustomerID,
            CustId = e.Bid.Customer.ShortName,
            CustomerName = e.Bid.Customer.DispatcherName,
            //CustomerCity = e.Bid.Customer.CustomerContacts.Where(cc => cc.Contact.isPrimary).Select(s => s.Contact.Address.City).FirstOrDefault(),
            //CustomerState = e.Bid.Customer.CustomerContacts.Where(cc => cc.Contact.isPrimary).Select(s => s.Contact.Address.State).FirstOrDefault(),
            CustomerCity = e.Bid.Customer.CustomerContacts.Where(cc => cc.Contact.IsActive).Select(s => s.Contact.Address.City).FirstOrDefault(),
            CustomerState = e.Bid.Customer.CustomerContacts.Where(cc => cc.Contact.IsActive).Select(s => s.Contact.Address.State).FirstOrDefault(),

            Comment = e.Comment,
            CompanyId = (int)e.Bid.Customer.CompanyId,
            CompanyName = e.Bid.Customer.Company.FullName,
            ConfirmationNumber = e.ConfirmationNumber,
            IsVoid = e.IsVoid,
            IsClosed = e.isComplete && !e.IsActive,
        };

        public int? ConfirmationNumber { get; set; }
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }

        private int? _quantity;
        
        public IEnumerable<Ticket> Tickets { get; set; }
        public string Comment { get; set; }
        public int CustomerId { get; set; }

        public string CustomerName { get; set; }
        public string CustomerCity { get; set; }
        public string CustomerState { get; set; }
        public string CustId { get; set; }

        public string Material { get; set; }

        public DateTime? LastTicketDate
        {
            get
            {
                List<DateTime> lastTicketDate = Tickets
                    .OrderByDescending(t => t.CreateTime)
                    .Select(t => t.CreateTime).ToList();

                return lastTicketDate.Any() ? lastTicketDate.First() : (DateTime?) null;
            }
        }

        public float Balance
        {
            get
            {
                float delivered = Tickets.Sum(t => t.Quantity)/2000;
                return Quantity - delivered;
            }
        }

        public float TodayTons
        {
            get
            {
                return
                    Tickets.Where(t => t.CreateTime.Date == DateTime.Today)
                        .Sum(t => t.Quantity)/2000;
            }
        }

        public int NumberOfTicket
        {
            get { return Tickets.Count(); }
        }
        public int Quantity
        {
            get
            {
                return _quantity.HasValue ? _quantity.Value/2000 : 0;
            }
        }

        public int OrderId { get; set; }
        public int SiteId { get; set; }
        public int BidId { get; set; }
        public int? OrderNumber { get; set; }
        public string OrderedBy { get; set; }
        public bool IsComplete { get; set; }
        public DateTime? CompleteDate { get; set; }
        public string Promise { get; set; }
        public DateTime? PromiseExpiresTime { get; set; }
        public string PoOverride { get; set; }
        public string SpecialInstructionOverride { get; set; }
        public DateTime EffectiveTime { get; set; }
        public DateTime? HeldUntilDate { get; set; }
        public string Priority { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        public bool IsActive { get; set; }
        public bool IsVoid { get; set; }
        public bool IsClosed { get; set; }
    }

    internal class OrderData : IOrder
    {
        public OrderData()
        {
        }

        public OrderData(OrderTable data)
        {
            OrderId = data.OrderID;
            BidId = data.BidID;
            SiteId = data.SiteID;
            OrderNumber = data.OrderNumber;
            ConfirmationNumber = data.ConfirmationNumber;
            OrderedBy = data.OrderedBy;
            IsComplete = data.isComplete;
            CompleteDate = data.CompleteDate;
            Promise = data.Promise;
            PromiseExpiresTime = data.PromiseExpiresTime;
            POOverride = data.POOverride;
            SpecialInstructionOverride = data.SpecialInstructionOverride;
            EffectiveTime = data.EffectiveTime;
            HeldUntilDate = data.HeldUntilDate;
            Priority = data.Priority;
            CreateTime = data.CreateTime;
            UpdateTime = data.UpdateTime;
            IsActive = data.IsActive;
            SeasonId = data.SeasonId;
            Comment = data.Comment;
            Customer = new CustomerAddress(data.Bid.Customer);
            Material = new MaterialInfo(data.Material);
            Bid = new BidData(data.Bid);
            Quantity = 0;
            if (data.OrderUpdates != null && data.OrderUpdates.Any(o => o.IsActive))
            {
                Quantity = data.OrderUpdates.Where(o => o.IsActive).Sum(o=>o.Quantity)/2000;
            }
            if (string.IsNullOrWhiteSpace(SpecialInstructionOverride))
            {
                SpecialInstructionOverride = Customer.SpecialInstruction;
            }
            IsVoid = data.IsVoid;
        }

        public int? ConfirmationNumber { get; set; }

        public IMaterialInfo Material { get; set; }
        public int Quantity { get; set; }
        public int OrderId { get; set; }
        public int BidId { get; set; }
        public int SiteId { get; set; }
        public int? OrderNumber { get; set; }
        public string OrderedBy { get; set; }
        public bool IsComplete { get; set; }
        public DateTime? CompleteDate { get; set; }
        public string Promise { get; set; }
        public DateTime? PromiseExpiresTime { get; set; }
        public string POOverride { get; set; }
        public string SpecialInstructionOverride { get; set; }
        public DateTime EffectiveTime { get; set; }
        public DateTime? HeldUntilDate { get; set; }
        public string Priority { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        public bool IsActive { get; set; }
        public int SeasonId { get; set; }
        public string Comment { get; set; }
        public ICustomerAddress Customer { get; private set; }
        public IBid Bid { get; set; }
        public bool IsVoid { get; set; }
    }

    #endregion
}