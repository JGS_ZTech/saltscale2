﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using Caliburn.Micro;
using Contract.Domain;
using Contract.Events;
using Contract.Exception;
using Contract.Services;
using DataAccess;
using Microsoft.Win32.SafeHandles;
using System.Runtime.InteropServices;
using System.ComponentModel;  // 2018-Nov-23, JGS - For .Net Framework 4.7,2
using System.IO;

namespace BusinessLogic
{
    public class ScaleManager : IScaleManager, IDisposable
    {
        private const int STREAM_BYTE_START_INT_AVERYWEIGHTRONIX = 2;
        private const int STREAM_BYTE_MAX_LENGTH_AVERYWEIGHTRONIX = 13;
        private const int STREAM_BYTE_START_INT_AVERYWEIGHTRONIX_ZM405 = 1;
        private const int STREAM_BYTE_MAX_LENGTH_AVERYWEIGHTRONIX_ZM405 = 12;
        private const int STREAM_BYTE_START_INT_IQ355 = 2; 
        private const int STREAM_BYTE_MAX_LENGTH_IQ355 = 14;
        private const int STREAM_BYTE_START_INT_FAIRBANKS = 2;
        private const int STREAM_BYTE_MAX_LENGTH_FAIRBANKS = 14;
        private const int STREAM_BYTE_START_INT_CARDINAL = 2;
        private const int STREAM_BYTE_MAX_LENGTH_CARDINAL = 17;

        private const int STREAM_BYTE_START_INT_CARDINAL_SB200 = 1;
        private const int STREAM_BYTE_MAX_LENGTH_CARDINAL_SB200 = 17;

        private const int STREAM_BYTE_START_INT_SIGNWEIGHT = 1;
        private const int STREAM_BYTE_MAX_LENGTH_SIGNWEIGHT = 9;
        private const int STREAM_BYTE_START_INT_GSE660 = 1;
        private const int STREAM_BYTE_MAX_LENGTH_GSE660 = 11;

        private const int STREAM_BYTE_START_INT_FAIRBANKS_TOLEDO_CR = 3;
        private const int STREAM_BYTE_MAX_LENGTH_FAIRBANKS_TOLEDO_CR = 17;

        private const int STREAM_BYTE_START_INT_FAIRBANKS_TOLEDO_CRLF = 3;
        private const int STREAM_BYTE_MAX_LENGTH_FAIRBANKS_TOLEDO_CRLF = 18;

        public const short FILE_ATTRIBUTE_NORMAL = 0x80;
        public const short INVALID_HANDLE_VALUE = -1;
        public const uint GENERIC_READ = 0x80000000;
        public const uint GENERIC_WRITE = 0x40000000;
        public const uint CREATE_NEW = 1;
        public const uint CREATE_ALWAYS = 2;
        public const uint OPEN_EXISTING = 3;


        private readonly IConfiguration _configuration;
        private readonly IEventAggregator _eventAggregator;
        private readonly ILog _log = LogManager.GetLog(typeof (ScaleManager));
        private IScaleData _scaleData;
        private ScaleIndicatorOutputFormat _outputStreamFormat;
        private SerialPort _serialPort;
        private SerialPort _senderSerialPort;
        private byte[] _comBuffer;

        private object locker = new object();

        bool _textStart = false;
        bool _textEnd = false;

        int _iChar = 0;
        private StringBuilder _textWeight = new StringBuilder();
        private ScaleResult _result;


        public ScaleManager(IEventAggregator eventAggregator, IConfiguration configuration)
        {
            _eventAggregator = eventAggregator;
            _configuration = configuration;
        }

        public List<int> GetAvailableComPorts()
        {
            var serialPortNumbers = new List<int>();
            foreach (string serialCommPortNamePortNumber in SerialPort.GetPortNames())
            {
                serialPortNumbers.Add(Int32.Parse(serialCommPortNamePortNumber.Last().ToString()));
            }

            return serialPortNumbers;
        }

        /// <summary>
        ///     Determines if the ComPort exists on the computer
        /// </summary>
        /// <param name="comPort">Serial Com Port</param>
        /// <returns>true|false</returns>
        private bool IsComPortExistOnComputer(int comPort)
        {
            return GetAvailableComPorts().IndexOf(comPort) != -1;
        }

        /// <summary>
        ///     Determines if the ComPort set for the scale indicator exists on the computer
        /// </summary>
        /// <param name="comPort">Serial Com Port of conencted Scale Indicator</param>
        /// <returns>true|false</returns>
        public bool IsSelectedScaleComPortExistOnComputer()
        {
            return GetAvailableComPorts().IndexOf(_scaleData.SerialComPort) != -1;
        }

        public void StartScaling(IScaleInfo scaleInfo)
        {
            //SerialPortClose();

            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                _scaleData =
                    data.Scales.AsNoTracking()
                        .Where(s => s.ID == scaleInfo.Id)
                        .Select(ScaleService.ScaleData.Transform)
                        .FirstOrDefault();

                if (_scaleData == null)
                {
                    var exEntityNotFound = new EntityNotFoundException("Scale not found.", typeof(Scale), scaleInfo.Id);
                    _log.Error(exEntityNotFound);
                    throw exEntityNotFound;
                }
                    
            }

            if (scaleInfo.SerialComPort != _scaleData.SerialComPort)
                _scaleData.SerialComPort = scaleInfo.SerialComPort;

            _outputStreamFormat =
                (ScaleIndicatorOutputFormat)Enum.Parse(typeof(ScaleIndicatorOutputFormat), _scaleData.OutputStreamFormat);

            InitScale();
        }

        private void InitScale()
        {
            try
            {
                _log.Info($"ScaleManager:InitScale - Begin  {_scaleData.ScaleName}");

                if (!Enum.IsDefined(typeof(ScaleIndicatorOutputFormat), _scaleData.OutputStreamFormat))
                {
                    var exScaleOutputStreamFormatNotConfigured =
                        new Exception("Output Stream is not configured: '" + _scaleData.OutputStreamFormat +
                                      "'  The program needs to be updated or expected output for this scale changed.");
                    _log.Error(exScaleOutputStreamFormatNotConfigured);
                    Debug.WriteLine(exScaleOutputStreamFormatNotConfigured.Message);
                    throw exScaleOutputStreamFormatNotConfigured;
                }

                if (!IsSelectedScaleComPortExistOnComputer())
                {
                    string message = $"ComPort set for scale of '{_scaleData.SerialComPort}' not used by this computer";
                    _log.Warn(message);
                    throw new Exception(message);
                }
                else
                {
                    _serialPort = new SerialPort("COM" + _scaleData.SerialComPort
                        , _scaleData.SerialBaud
                        , (Parity)Enum.Parse(typeof(Parity), _scaleData.SerialParity, true)
                        , _scaleData.SerialDataBits
                        , (StopBits)Enum.Parse(typeof(StopBits), _scaleData.SerialStopBits.ToString(), true)
                        );

                    _serialPort.Encoding = Encoding.ASCII;
                    _serialPort.Handshake = Handshake.None;
                    _serialPort.RtsEnable = true;
                    _serialPort.DtrEnable = true;
                    _serialPort.DiscardNull = true;


                    if (IsScaleComPortAccessibleForReading(_scaleData.SerialComPort))
                    {
                        SerialPortFixer.Execute(_serialPort.PortName);
                        _serialPort.Open();
                        _log.Info("Serial Port Opened - First Try", _serialPort);
                    }
                    else
                    {
                        // 2019-Jan-07, JGS - If the port isn't available, then try again
                        _log.Info("Serial Port Did NOT Open - Retry #1", _serialPort);
                        _serialPort.DiscardOutBuffer();
                        _serialPort.DiscardInBuffer();
                        _serialPort.Close();
                        _serialPort.DataReceived -= SerialPort_DataReceived;
                        _serialPort.Dispose();
                        _serialPort = null;
                        Thread.Sleep(500);
                        _serialPort = new SerialPort("COM" + _scaleData.SerialComPort
                            , _scaleData.SerialBaud
                            , (Parity)Enum.Parse(typeof(Parity), _scaleData.SerialParity, true)
                            , _scaleData.SerialDataBits
                            , (StopBits)Enum.Parse(typeof(StopBits), _scaleData.SerialStopBits.ToString(), true)
                            );

                        _serialPort.Encoding = Encoding.ASCII;
                        _serialPort.Handshake = Handshake.None;
                        _serialPort.RtsEnable = true;
                        _serialPort.DtrEnable = true;
                        _serialPort.DiscardNull = true;

                        if (IsScaleComPortAccessibleForReading(_scaleData.SerialComPort))
                        {
                            SerialPortFixer.Execute(_serialPort.PortName);
                            _serialPort.Open();
                            _log.Info("Serial Port Opened - Second Try", _serialPort);
                        }
                        else
                        {
                            // 2019-Jan-07, JGS - If the port isn't available, then try again
                            _log.Info("Serial Port Did NOT Open - Retry #2", _serialPort);
                            _serialPort.DiscardOutBuffer();
                            _serialPort.DiscardInBuffer();
                            _serialPort.Close();
                            _serialPort.DataReceived -= SerialPort_DataReceived;
                            _serialPort.Dispose();
                            _serialPort = null;
                            Thread.Sleep(500);
                            _serialPort = new SerialPort("COM" + _scaleData.SerialComPort
                                , _scaleData.SerialBaud
                                , (Parity)Enum.Parse(typeof(Parity), _scaleData.SerialParity, true)
                                , _scaleData.SerialDataBits
                                , (StopBits)Enum.Parse(typeof(StopBits), _scaleData.SerialStopBits.ToString(), true)
                                );

                            _serialPort.Encoding = Encoding.ASCII;
                            _serialPort.Handshake = Handshake.None;
                            _serialPort.RtsEnable = true;
                            _serialPort.DtrEnable = true;
                            _serialPort.DiscardNull = true;

                            if (IsScaleComPortAccessibleForReading(_scaleData.SerialComPort))
                            {
                                SerialPortFixer.Execute(_serialPort.PortName);
                                _serialPort.Open();
                                _log.Info("Serial Port Opened - Third Try", _serialPort);
                            }
                        }
                    }

                    _serialPort.DataReceived += SerialPort_DataReceived;
                    _serialPort.ErrorReceived += SerialPort_ErrorReceived;
                    _serialPort.PinChanged += _serialPort_PinChanged;

                    _log.Info($"ScaleManager:InitScale - End {_scaleData.ScaleName}");
                }
            }
            catch (Exception ex)
            {
                var exInnerMessageBoxText = string.Empty;
                _log.Error(ex);
                if (ex.InnerException != null)
                {
                    _log.Error(ex.InnerException);
                    exInnerMessageBoxText = $" and inner exception '{ex.InnerException.Message}'{Environment.NewLine} ";
                }

                //SerialPortClose();
                throw ex;
            }
        }

        public void StopScaling()
        {
            Thread closeSerial = new Thread(new ThreadStart(SerialPortClose));
            closeSerial.Start();
            _log.Info($"ScaleManager:StopScaling");
        }

        public void Dispose()
        {
            //SerialPortClose();

            if (_serialPort != null)
            {
                //_serialPort.Dispose();
                _serialPort = null;
            }
        }

        //public const short FILE_ATTRIBUTE_NORMAL = 0x80;
        //public const short INVALID_HANDLE_VALUE = -1;
        //public const uint GENERIC_READ = 0x80000000;
        //public const uint GENERIC_WRITE = 0x40000000;
        //public const uint CREATE_NEW = 1;
        //public const uint CREATE_ALWAYS = 2;
        //public const uint OPEN_EXISTING = 3;

        //private const int GENERIC_READ = unchecked((int)0x80000000);
        //private const int FILE_SHARE_READ = 1;
        //private const int FILE_SHARE_WRITE = 2;
        //private const int OPEN_EXISTING = 3;
        //private const int IOCTL_DISK_GET_DRIVE_LAYOUT_EX = unchecked((int)0x00070050);
        //private const int ERROR_INSUFFICIENT_BUFFER = 122;
        //NativeMethods.CreateFile("\\\\.\\PHYSICALDRIVE" + PhysicalDrive, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, IntPtr.Zero, OPEN_EXISTING, 0, IntPtr.Zero))


        [System.Runtime.InteropServices.DllImport("kernel32.dll", CharSet = System.Runtime.InteropServices.CharSet.Auto, SetLastError = true)]
        //// 2018-Nov-23, JGS - Reset scale access to open the scale handle with defaults
        //internal static extern Microsoft.Win32.SafeHandles.SafeFileHandle CreateFile(string lpFileName, int dwDesiredAccess, int dwShareMode, IntPtr securityAttrs, int dwCreationDisposition, int dwFlagsAndAttributes, IntPtr hTemplateFile);
        internal static extern SafeFileHandle CreateFile(string lpFileName, uint dwDesiredAccess, 
                                                            uint dwShareMode, IntPtr securityAttrs, uint dwCreationDisposition, 
                                                            uint dwFlagsAndAttributes, IntPtr hTemplateFile);

        // Other options
        // internal static extern SafeFileHandle Handle = CreateFile(port, GenericRead | GenericWrite, 0, IntPtr.Zero, OpenExisting, 0, IntPtr.Zero);


        public bool IsScaleComPortAccessibleForReading(int scaleSerialComPort)
        {
            // 2018-Nov-23, JGS - Reset scale access to open the scale handle with defaults
            uint dwFlagsAndAttributes = 0x40000000;

            //// 2018-Nov-23, JGS - Reset scale access to open the scale handle with defaults
            //Microsoft.Win32.SafeHandles.SafeFileHandle safeFileHandleComPort = CreateFile(@"\\.\COM" + scaleSerialComPort, -1073741824, 0, IntPtr.Zero, 3, dwFlagsAndAttributes, IntPtr.Zero);
            //SafeFileHandle safeFileHandleComPort = CreateFile(@"\\.\COM" + scaleSerialComPort, GENERIC_READ | GENERIC_WRITE, 0, IntPtr.Zero, OPEN_EXISTING, dwFlagsAndAttributes, IntPtr.Zero);
            SafeFileHandle safeFileHandleComPort = CreateFile(@"\\.\COM" + scaleSerialComPort, GENERIC_WRITE, 0, IntPtr.Zero, OPEN_EXISTING, dwFlagsAndAttributes, IntPtr.Zero);

            if (safeFileHandleComPort.IsInvalid)
            {
                // 2019-Jan-08, JGS - Try to Close the COM port first and then open it for processing
                _log.Info($"ScaleManager:IsComPortInUse - COM Port not accessible and in use - Failed ttempt 1 of 2: {scaleSerialComPort}");
                _log.Info($"Delaying for 2sec");
                safeFileHandleComPort.Close();
                safeFileHandleComPort.Dispose();
                safeFileHandleComPort = null;
                Thread.Sleep(2000);
                safeFileHandleComPort = CreateFile(@"\\.\COM" + scaleSerialComPort, GENERIC_WRITE, 0, IntPtr.Zero, OPEN_EXISTING, dwFlagsAndAttributes, IntPtr.Zero);

                if (safeFileHandleComPort.IsInvalid)
                {
                    // 2019-Jan-08, JGS - Try to Close the COM port first and then open it for processing
                    _log.Info($"ScaleManager:IsComPortInUse - COM Port not accessible and in use - Failed attempt 2 of 2: {scaleSerialComPort}");
                    _log.Info($"Delaying for 2sec");
                    safeFileHandleComPort.Close();
                    safeFileHandleComPort.Dispose();
                    safeFileHandleComPort = null;
                    Thread.Sleep(2000);
                    safeFileHandleComPort = CreateFile(@"\\.\COM" + scaleSerialComPort, GENERIC_WRITE, 0, IntPtr.Zero, OPEN_EXISTING, dwFlagsAndAttributes, IntPtr.Zero);

                    if (safeFileHandleComPort.IsInvalid)
                    {
                        _log.Info($"ScaleManager:IsComPortInUse - COM Port not accessible and in use: {scaleSerialComPort}");
                        return false;
                    }
                }
            }

            safeFileHandleComPort.Close();
            return true;
        }

        #region DTO

        private class ScaleResult : IScaleResult
        {
            public int Id { get; set; }
            public ScaleIndicatorStreamPolarity StreamPolarity { get; set; }

            public int? StreamWeight { get; set; }

            public ScaleIndicatorStreamUnitOfWeight StreamUnitOfWeight { get; set; }

            public ScaleIndicatorStreamWeightGrossNet StreamWeightGrossNet { get; set; }

            public ScaleIndicatorStreamMotionStatus StreamMotionStatus { get; set; }
        }

        #endregion

        private void _serialPort_PinChanged(object sender, SerialPinChangedEventArgs e)
        {
            //throw new NotImplementedException();
            //-- While, yes, this isn't implimented a Pin Changed event isn't necessarily a problem
            //-- https://msdn.microsoft.com/en-us/library/system.io.ports.serialport.pinchanged(v=vs.110).aspx
            //-- https://msdn.microsoft.com/en-us/library/system.io.ports.serialport.breakstate(v=vs.110).aspx
            
            //ToDo Log warning...maybe in different file for info to not spam
        }

        private void SerialPort_ErrorReceived(object sender, SerialErrorReceivedEventArgs e)
        {
            var exception = new ScaleException("SerialPort ErrorReceived Event", e.EventType);
            _log.Error(exception);

            switch (e.EventType)
            {
                case SerialError.RXParity:
                    Debug.WriteLine("SerialPort ErrorReceived Event: " + SerialError.RXParity);
                    break;

                case SerialError.Frame:
                    Debug.WriteLine("SerialPort ErrorReceived Event: " + SerialError.Frame);
                    break;

                case SerialError.Overrun:
                    Debug.WriteLine("SerialPort ErrorReceived Event: " + SerialError.Overrun);
                    break;

                case SerialError.RXOver:
                    Debug.WriteLine("SerialPort ErrorReceived Event: " + SerialError.RXOver);
                    break;

                case SerialError.TXFull:
                    Debug.WriteLine("SerialPort ErrorReceived Event: " + SerialError.TXFull);
                    break;
            }

            _log.Warn($"SerialPort_ErrorRecieved: SerialErrorReceivedEventArgs {e.EventType}");
            //CloseConn();  //Unsure if this should be used because the com port will be continuously read and one read error shouldn't prevent the next read which could be a non-error
        }

        /*
        private void DataReceivedHandler(object sender, SerialDataReceivedEventArgs e)
        {
            BeginInvoke(new EventHandler(delegate
            {

            }));
        }
        */
        private void SerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                Thread.Sleep(_scaleData.SerialPortDataReceivedDelayMS);

                _senderSerialPort = (SerialPort)sender;

                if (_serialPort.IsOpen)
                {
                    int btr = _senderSerialPort.BytesToRead;
                    _comBuffer = new byte[btr];

                    lock (locker)
                    {
                    }
                    _senderSerialPort.Read(_comBuffer, 0, btr);


                    //ThreadPool.QueueUserWorkItem(handleReceivedBytes);
                    string serialPortDataRecieved;
                    Match match;

                    #region Parse obtained serial line from scale indicator
                    
                    _result = new ScaleResult();

                    _result.Id = _scaleData.Id;

                    switch (_outputStreamFormat)
                    {
                        case ScaleIndicatorOutputFormat.AVERYWEIGHTRONIX:

                            #region Avery Weigh-Tronix

                            match = Regex.Match(Encoding.ASCII.GetString(_comBuffer), "(\\u0002)(.*?)(\\r\\n)", RegexOptions.Compiled);
                            serialPortDataRecieved = match.ToString();
                            _serialPort.DiscardInBuffer();

                            if (IsScaleDataRecievedByteLengthMatch(serialPortDataRecieved.Length, STREAM_BYTE_MAX_LENGTH_AVERYWEIGHTRONIX, _senderSerialPort.PortName))
                            {
                                foreach (char charOfScaleReadString in serialPortDataRecieved)
                                {
                                    //-- \u0002 = Unicode Start-of-Text
                                    //-- Having this character at the beginning of the line is standard for all RiceLake and FairBanks data formats
                                    if (charOfScaleReadString == '\u0002')
                                    {
                                        _textStart = true;
                                        _textEnd = false;
                                        _textWeight.Clear();
                                    }

                                    if (_textStart && !_textEnd)
                                    {

                                    }
                                }
                            }

                            #endregion

                            break;


                        case ScaleIndicatorOutputFormat.CARDINAL:

                            #region Cardinal
                            //-- https://www.regex101.com/r/InLFMx/1

                            //-- Regex (\s)([\s0-9]{6})(\s)([A-Z]{1,2})(\s)([A-Z])(\s)([A-Z]{2})(\s)(\n)
                            //-- (\s)([\s0-9]{6})(\s)([A-Z]{1,2})(\s)([A-Z])(\s)([A-Z|\s\s]{2})(\s)(\n) -- Portsmouth

                            //match = Regex.Match(Encoding.ASCII.GetString(comBuffer), @"(\s)([\s0-9]{6})(\s)([A-Z]{1,2})(\s)([A-Z])(\s)([A-Z]{2}|\s\s)(\s)", RegexOptions.Compiled);
                            match = Regex.Match(Encoding.ASCII.GetString(_comBuffer), @"[\s|-]([\s0-9]{6})(\s)([A-Z]{1,2})(\s)([A-Z])(\s)([A-Z|\s\s]{2})(\s)(\r)", RegexOptions.Compiled);
                            serialPortDataRecieved = match.ToString();
                            //_serialPort.DiscardInBuffer();

                            if (IsScaleDataRecievedByteLengthMatch(serialPortDataRecieved.Length, STREAM_BYTE_MAX_LENGTH_CARDINAL, _senderSerialPort.PortName))
                            {

                                //if (match.Groups[1].Value == " " || match.Groups[1].Value == "-")
                                //{
                                _textStart = true;
                                _textEnd = false;
                                _textWeight.Clear();

                                _iChar = 0;
                                //}

                                foreach (char charOfScaleReadString in serialPortDataRecieved)
                                {
                                    if (_textStart && !_textEnd)
                                    {

                                        #region Parse Indicactor Stream

                                        switch (_iChar)
                                        {

                                            case 0:
                                                /*-- Character Interpritations: Polarity of weight
                                                    <space> = positive weight
                                                    - = negative weight
                                                    ^ = overload
                                                    ] = underrange
                                                */

                                                if (Enum.IsDefined(typeof(ScaleIndicatorStreamPolarity),
                                                    (int)charOfScaleReadString))
                                                {
                                                    _result.StreamPolarity = (ScaleIndicatorStreamPolarity)charOfScaleReadString;
                                                }
                                                else
                                                {
                                                    _log.Error(
                                                        new Exception(
                                                            "Scale Indicator Stream Enum.IsDefined(typeof(ScaleIndicatorStreamPolarity) with value " +
                                                            charOfScaleReadString));
                                                }

                                                break;

                                            /*-- Character Interpritations: Weight number
                                                Weight is 7 digits in length
                                                Leading 0's are surpressed
                                                Right justified

                                                If overloaded or underrange the character will carry into weight field but is being ignored for these cases because it was already determined
                                            */
                                            case 1:
                                            case 2:
                                            case 3:
                                            case 4:
                                            case 5:
                                            case 6:
                                                _textWeight.Append(charOfScaleReadString);

                                                if (_iChar == 6)
                                                {
                                                    int weightConvertStringToInt;
                                                    if (int.TryParse(_textWeight.ToString(), out weightConvertStringToInt))
                                                    {
                                                        _result.StreamWeight = (_result.StreamPolarity ==
                                                                               ScaleIndicatorStreamPolarity.Negative)
                                                            ? weightConvertStringToInt * -1
                                                            : weightConvertStringToInt;
                                                    }
                                                    else
                                                    {
                                                        _log.Warn("StreamWeight could not be paresed as an int: " +
                                                                  _result.StreamWeight + "   Full string: " +
                                                                  serialPortDataRecieved);
                                                    }
                                                }
                                                break;

                                            case 7:
                                                //-- Space after Units
                                                break;

                                            case 8:
                                                //case 9:
                                                //-- 9 commented as it is the "B" of "LB" and program is not coded for two characters
                                                if (Enum.IsDefined(typeof(ScaleIndicatorStreamUnitOfWeight),
                                                    (int)charOfScaleReadString))
                                                {
                                                    _result.StreamUnitOfWeight =
                                                        (ScaleIndicatorStreamUnitOfWeight)
                                                            Enum.Parse(typeof(ScaleIndicatorStreamUnitOfWeight),
                                                                charOfScaleReadString.ToString());
                                                }
                                                else
                                                {
                                                    _log.Error(
                                                        new Exception(
                                                            "Scale Indicator Stream Enum.IsDefined(typeof(ScaleIndicatorStreamUnitOfWeight) with value " + charOfScaleReadString));
                                                }
                                                break;

                                            case 10:
                                                //-- Space after Units
                                                break;

                                            case 11:
                                                if (Enum.IsDefined(typeof(ScaleIndicatorStreamWeightGrossNet),
                                                    (int)charOfScaleReadString))
                                                {
                                                    _result.StreamWeightGrossNet =
                                                        (ScaleIndicatorStreamWeightGrossNet)
                                                            Enum.Parse(typeof(ScaleIndicatorStreamWeightGrossNet),
                                                                charOfScaleReadString.ToString());
                                                }
                                                else
                                                {
                                                    _log.Error(
                                                        new Exception("Scale Indicator Stream Enum.IsDefined(typeof(ScaleIndicatorStreamWeightGrossNet) with value " + charOfScaleReadString));
                                                }
                                                break;

                                            case 12:
                                                //-- Space after Weight Mode
                                                break;

                                            case 13:
                                                //case 14:
                                                //-- 14 commented out as it is the second number of the weight status that the program is not coded for
                                                if (_scaleData.IsMotionDetectionEnabled)
                                                {
                                                    /*
                                                     OC = overcap 
                                                     CZ = center of zero 
                                                     MO = motion 
                                                     ee = weight not currently being displayed
                                                     BZ = Below Zero
                                                    */

                                                    if (charOfScaleReadString == 'e')
                                                    {
                                                        _log.Warn($"Scale Reader: Weight Status is not programmed for character {charOfScaleReadString}");
                                                    }
                                                    else
                                                    {
                                                        if (charOfScaleReadString == ' ')
                                                        {
                                                            _log.Info($"Scale Reader: No motion or Center of Zero");
                                                        }

                                                        if (Enum.IsDefined(typeof(ScaleIndicatorStreamMotionStatus),
                                                                            (int)charOfScaleReadString))
                                                        {
                                                            _result.StreamMotionStatus = (ScaleIndicatorStreamMotionStatus)charOfScaleReadString;
                                                        }
                                                        else
                                                        {
                                                            _log.Error(
                                                                new Exception("Scale Indicator Stream Enum.TryParse() typeof(ScaleIndicatorStreamMotionStatus) with value " + charOfScaleReadString));
                                                        }
                                                    }
                                                }
                                                break;
                                            case 15:
                                                //-- Space after Motion/Center of Zero detection
                                                break;

                                            case 16:
                                                _textEnd = true;
                                                break;
                                        } //-- switch (iChar)

                                        #endregion
                                    }

                                    EndOfStreamOrNextChar();

                                    //-- if (textStart && !textEnd)
                                } //-- foreach (char charOfScaleReadString in serialPortDataRecieved)
                            }

                            #endregion

                            break;


                        case ScaleIndicatorOutputFormat.SB200:

                            #region SB-200
                            //-- Regex (\s|-)([0-9]{6})\.([a-z]|\s)\s([a-z]{2})\s([a-z])\s\s\x03
                            //-- https://regex101.com/r/Yr9Bym/1

                            match = Regex.Match(Encoding.ASCII.GetString(_comBuffer), @"(\s|-)([0-9]{6})\.([a-z]|\s)\s([a-z]{2})\s([a-z])\s\s\x03", RegexOptions.Compiled);
                            serialPortDataRecieved = match.ToString();
                            //_serialPort.DiscardInBuffer();

                            if (IsScaleDataRecievedByteLengthMatch(serialPortDataRecieved.Length, STREAM_BYTE_MAX_LENGTH_CARDINAL_SB200, _senderSerialPort.PortName))
                            {

                                //if (match.Groups[1].Value == " " || match.Groups[1].Value == "-")
                                //{
                                _textStart = true;
                                _textEnd = false;
                                _textWeight.Clear();

                                _iChar = 0;
                                //}

                                foreach (char charOfScaleReadString in serialPortDataRecieved)
                                {
                                    if (_textStart && !_textEnd)
                                    {

                                        #region Parse Indicactor Stream

                                        switch (_iChar)
                                        {

                                            //case 1:
                                            //case 2:
                                            //-- Do nothing, these char's are part of the beginning of the ascii string
                                            //    break;

                                            /*-- Character Interpritations: Polarity of weight
                                                <space> = positive weight
                                                - = negative weight
                                                ^ = overload
                                                ] = underrange
                                            */
                                            case 0:
                                                if (Enum.IsDefined(typeof(ScaleIndicatorStreamPolarity),
                                                    (int)charOfScaleReadString))
                                                {
                                                    _result.StreamPolarity = (ScaleIndicatorStreamPolarity)charOfScaleReadString;
                                                }
                                                else
                                                {
                                                    _log.Error(
                                                        new Exception(
                                                            "Scale Indicator Stream Enum.IsDefined(typeof(ScaleIndicatorStreamPolarity) with value " +
                                                            charOfScaleReadString));
                                                }

                                                break;

                                            /*-- Character Interpritations: Weight number
                                                Weight is 7 digits in length
                                                Leading 0's are surpressed
                                                Right justified

                                                If overloaded or underrange the character will carry into weight field but is being ignored for these cases because it was already determined
                                            */
                                            case 1:
                                            case 2:
                                            case 3:
                                            case 4:
                                            case 5:
                                            case 6:
                                                _textWeight.Append(charOfScaleReadString);

                                                if (_iChar == 6)
                                                {
                                                    int weightConvertStringToInt;
                                                    if (int.TryParse(_textWeight.ToString(), out weightConvertStringToInt))
                                                    {
                                                        _result.StreamWeight = (_result.StreamPolarity ==
                                                                               ScaleIndicatorStreamPolarity.Negative)
                                                            ? weightConvertStringToInt * -1
                                                            : weightConvertStringToInt;
                                                    }
                                                    else
                                                    {
                                                        _log.Warn("StreamWeight could not be paresed as an int: " +
                                                                  _result.StreamWeight + "   Full string: " +
                                                                  serialPortDataRecieved);
                                                    }
                                                }
                                                break;

                                            case 7:
                                                //-- decimal point added by output format.  not used.
                                                Debug.WriteLine($"Case {_iChar} character: {charOfScaleReadString}");
                                                break;

                                            case 8:
                                                if (_scaleData.IsMotionDetectionEnabled)
                                                {
                                                    /*
                                                     m = Motion
                                                     o = Overcap 
                                                     e = weight not currently being displayed
                                                    */

                                                    if (charOfScaleReadString == 'e')
                                                    {
                                                        _log.Warn($"Scale Reader: Weight Status is not programmed for character {charOfScaleReadString}");
                                                    }
                                                    else
                                                    {
                                                        if (charOfScaleReadString == ' ')
                                                        {
                                                            _log.Info($"Scale Reader: No motion");
                                                        }

                                                        if (Enum.IsDefined(typeof(ScaleIndicatorStreamMotionStatus),
                                                            (int)charOfScaleReadString))
                                                        {
                                                            _result.StreamMotionStatus = (ScaleIndicatorStreamMotionStatus)charOfScaleReadString;

                                                            //result.StreamMotionStatus =
                                                            //    (ScaleIndicatorStreamMotionStatus)
                                                            //    Enum.Parse(typeof(ScaleIndicatorStreamMotionStatus),
                                                            //        charOfScaleReadString.ToString());

                                                        }
                                                        else
                                                        {
                                                            _log.Error(
                                                                new Exception(
                                                                    "Scale Indicator Stream Enum.TryParse() typeof(ScaleIndicatorStreamMotionStatus) with value " +
                                                                    charOfScaleReadString));
                                                        }
                                                    }
                                                }
                                                break;
                                            case 9:
                                                //-- Space in the output
                                                break;

                                            case 10:
                                                //case 11:
                                                //--11 commented as it is the "b" of "lb" and program is not coded for two characters
                                                if (Enum.IsDefined(typeof(ScaleIndicatorStreamUnitOfWeight),
                                                    (int)charOfScaleReadString))
                                                {
                                                    _result.StreamUnitOfWeight =
                                                        (ScaleIndicatorStreamUnitOfWeight)
                                                            Enum.Parse(typeof(ScaleIndicatorStreamUnitOfWeight),
                                                                charOfScaleReadString.ToString());
                                                }
                                                else
                                                {
                                                    _log.Error(
                                                        new Exception(
                                                            "Scale Indicator Stream Enum.IsDefined(typeof(ScaleIndicatorStreamUnitOfWeight) with value " +
                                                            charOfScaleReadString));
                                                }
                                                break;

                                            case 12:
                                                //-- Space after Units
                                                Debug.WriteLine($"Case {_iChar} character: {charOfScaleReadString}");
                                                break;

                                            case 13:
                                                if (Enum.IsDefined(typeof(ScaleIndicatorStreamWeightGrossNet),
                                                    (int)charOfScaleReadString))
                                                {
                                                    _result.StreamWeightGrossNet =
                                                        (ScaleIndicatorStreamWeightGrossNet)
                                                            Enum.Parse(typeof(ScaleIndicatorStreamWeightGrossNet),
                                                                charOfScaleReadString.ToString());
                                                }
                                                else
                                                {
                                                    _log.Error(
                                                        new Exception(
                                                            "Scale Indicator Stream Enum.IsDefined(typeof(ScaleIndicatorStreamWeightGrossNet) with value " +
                                                            charOfScaleReadString));
                                                }
                                                break;

                                            case 14:
                                            case 15:
                                                //-- Space after Weight Mode
                                                Debug.WriteLine($"Case {_iChar} character: {charOfScaleReadString}");
                                                break;

                                            case 16:
                                                _textEnd = true;
                                                break;
                                        } //-- switch (iChar)

                                        #endregion
                                    }

                                    EndOfStreamOrNextChar();

                                    //-- if (textStart && !textEnd)
                                } //-- foreach (char charOfScaleReadString in serialPortDataRecieved)
                            }

                            #endregion

                            break;

                        case ScaleIndicatorOutputFormat.FAIRBANKS:

                            #region FairBanks

                            match = Regex.Match(Encoding.ASCII.GetString(_comBuffer), "(\\u0002)(.*?)(\\r\\n)", RegexOptions.Compiled);
                            serialPortDataRecieved = match.ToString();
                            _serialPort.DiscardInBuffer();

                            if (IsScaleDataRecievedByteLengthMatch(serialPortDataRecieved.Length, STREAM_BYTE_MAX_LENGTH_IQ355, _senderSerialPort.PortName))
                            {
                                foreach (char charOfScaleReadString in serialPortDataRecieved)
                                {
                                    //-- \u0002 = Unicode Start-of-Text
                                    //-- Having this character at the beginning of the line is standard for all RiceLake and FairBanks data formats
                                    if (charOfScaleReadString == '\u0002')
                                    {
                                        _textStart = true;
                                        _textEnd = false;
                                        _textWeight.Clear();
                                    }

                                    if (_textStart && !_textEnd)
                                    {

                                    }
                                }
                            }

                            #endregion

                            break;

                        case ScaleIndicatorOutputFormat.FAIRBANKS_TOLEDO_CR:

                            #region Fairbanks/Toledo

                            match = Regex.Match(Encoding.ASCII.GetString(_comBuffer), @"(\u0002)(.*?)(\r)", RegexOptions.Compiled);
                            serialPortDataRecieved = match.ToString();

                            if (IsScaleDataRecievedByteLengthMatch(serialPortDataRecieved.Length, STREAM_BYTE_MAX_LENGTH_FAIRBANKS_TOLEDO_CR, _senderSerialPort.PortName))
                            {

                                _iChar = 0;

                                foreach (char charOfScaleReadString in serialPortDataRecieved)
                                {
                                    //-- \u0002 = Unicode Start-of-Text
                                    //-- Having this character at the beginning of the line is standard for all RiceLake and FairBanks data formats
                                    if (charOfScaleReadString == '\u0002')
                                    {
                                        _textStart = true;
                                        _textEnd = false;
                                        _textWeight.Clear();
                                    }

                                    if (_textStart && !_textEnd)
                                    {
                                        #region Parse Indicator Stream

                                        switch (_iChar)
                                        {
                                            case 1:
                                            case 2:
                                            case 3:
                                                //-- Status words which indicate something but not sure what.  Not needed though.
                                                break;
                                            case 4:
                                            case 5:
                                            case 6:
                                            case 7:
                                            case 8:
                                            case 9:
                                                _textWeight.Append(charOfScaleReadString);

                                                if (_iChar == 9)
                                                {
                                                    int weightConvertStringToInt;
                                                    if (int.TryParse(_textWeight.ToString(), out weightConvertStringToInt))
                                                    {
                                                        _result.StreamWeight = (_result.StreamPolarity ==
                                                                               ScaleIndicatorStreamPolarity.Negative)
                                                            ? weightConvertStringToInt * -1
                                                            : weightConvertStringToInt;
                                                    }
                                                    else
                                                    {
                                                        _log.Warn("StreamWeight could not be paresed as an int: " +
                                                                  _result.StreamWeight + "   Full string: " +
                                                                  serialPortDataRecieved);
                                                    }
                                                }
                                                break;

                                            case 16:
                                                _textEnd = true;

                                                _result.StreamMotionStatus = ScaleIndicatorStreamMotionStatus.Undefined;
                                                if (_result.StreamWeight.Value.ToString().Contains('-'))
                                                {
                                                    _result.StreamPolarity = ScaleIndicatorStreamPolarity.Negative;
                                                }
                                                else
                                                {
                                                    _result.StreamPolarity = ScaleIndicatorStreamPolarity.Positive;
                                                }
                                                _result.StreamUnitOfWeight = ScaleIndicatorStreamUnitOfWeight.L;
                                                _result.StreamWeightGrossNet = ScaleIndicatorStreamWeightGrossNet.G;
                                                break;

                                        }

                                        #endregion
                                    }

                                    EndOfStreamOrNextChar();
                                }
                            }

                            #endregion

                            break;

                        case ScaleIndicatorOutputFormat.FAIRBANKS_TOLEDO_CRLF:

                            #region Fairbanks/Toledo

                            match = Regex.Match(Encoding.ASCII.GetString(_comBuffer), @"(\\u0002)(.*?)(\\r\\n)", RegexOptions.Compiled);
                            serialPortDataRecieved = match.ToString();

                            if (IsScaleDataRecievedByteLengthMatch(serialPortDataRecieved.Length, STREAM_BYTE_MAX_LENGTH_FAIRBANKS_TOLEDO_CRLF, _senderSerialPort.PortName))
                            {

                                _iChar = 0;

                                foreach (char charOfScaleReadString in serialPortDataRecieved)
                                {
                                    //-- \u0002 = Unicode Start-of-Text
                                    //-- Having this character at the beginning of the line is standard for all RiceLake and FairBanks data formats
                                    if (charOfScaleReadString == '\u0002')
                                    {
                                        _textStart = true;
                                        _textEnd = false;
                                        _textWeight.Clear();
                                    }

                                    if (_textStart && !_textEnd)
                                    {
                                        #region Parse Indicator Stream

                                        switch (_iChar)
                                        {
                                            case 1:
                                            case 2:
                                            case 3:
                                                //-- Status words which indicate something but not sure what.  Not needed though.
                                                break;
                                            case 4:
                                            case 5:
                                            case 6:
                                            case 7:
                                            case 8:
                                            case 9:
                                                _textWeight.Append(charOfScaleReadString);

                                                if (_iChar == 9)
                                                {
                                                    int weightConvertStringToInt;
                                                    if (int.TryParse(_textWeight.ToString(), out weightConvertStringToInt))
                                                    {
                                                        _result.StreamWeight = (_result.StreamPolarity ==
                                                                               ScaleIndicatorStreamPolarity.Negative)
                                                            ? weightConvertStringToInt * -1
                                                            : weightConvertStringToInt;
                                                    }
                                                    else
                                                    {
                                                        _log.Warn("StreamWeight could not be paresed as an int: " +
                                                                  _result.StreamWeight + "   Full string: " +
                                                                  serialPortDataRecieved);
                                                    }
                                                }
                                                break;

                                            case 17:
                                                _textEnd = true;

                                                _result.StreamMotionStatus = ScaleIndicatorStreamMotionStatus.Undefined;
                                                if (_result.StreamWeight.Value.ToString().Contains('-'))
                                                {
                                                    _result.StreamPolarity = ScaleIndicatorStreamPolarity.Negative;
                                                }
                                                else
                                                {
                                                    _result.StreamPolarity = ScaleIndicatorStreamPolarity.Positive;
                                                }
                                                _result.StreamUnitOfWeight = ScaleIndicatorStreamUnitOfWeight.L;
                                                _result.StreamWeightGrossNet = ScaleIndicatorStreamWeightGrossNet.G;
                                                break;

                                        }

                                        #endregion
                                    }

                                    EndOfStreamOrNextChar();
                                }
                            }

                            #endregion

                            break;

                        case ScaleIndicatorOutputFormat.IQ355:

                            #region IQ355

                            match = Regex.Match(Encoding.ASCII.GetString(_comBuffer), "(\\u0002)(.*?)(\\r\\n)", RegexOptions.Compiled);
                            serialPortDataRecieved = match.ToString();
                            //_serialPort.DiscardInBuffer();

                            if (IsScaleDataRecievedByteLengthMatch(serialPortDataRecieved.Length, STREAM_BYTE_MAX_LENGTH_IQ355, _senderSerialPort.PortName))
                            {

                                _iChar = 0;

                                foreach (char charOfScaleReadString in serialPortDataRecieved)
                                {
                                    //-- \u0002 = Unicode Start-of-Text
                                    //-- Having this character at the beginning of the line is standard for all RiceLake and FairBanks data formats
                                    if (charOfScaleReadString == '\u0002')
                                    {
                                        _textStart = true;
                                        _textEnd = false;
                                        _textWeight.Clear();
                                    }

                                    if (_textStart && !_textEnd)
                                    {
                                        #region Parse Indicator Stream

                                        switch (_iChar)
                                        {

                                            //case 1:
                                            //case 2:
                                            //-- Do nothing, these char's are part of the beginning of the ascii string
                                            //     break;

                                            /*-- Character Interpritations: Polarity of weight
                                                <space> = positive weight
                                                - = negative weight
                                                ^ = overload
                                                ] = underrange
                                            */
                                            case 1:
                                                if (Enum.IsDefined(typeof(ScaleIndicatorStreamPolarity),
                                                    (int)charOfScaleReadString))
                                                {
                                                    _result.StreamPolarity = (ScaleIndicatorStreamPolarity)charOfScaleReadString;
                                                }
                                                else
                                                {
                                                    _log.Error(
                                                        new Exception(
                                                            "Scale Indicator Stream Enum.IsDefined(typeof(ScaleIndicatorStreamPolarity) with value " +
                                                            charOfScaleReadString));
                                                }

                                                break;

                                            /*-- Character Interpritations: Weight number
                                                Weight is 7 digits in length
                                                Leading 0's are surpressed
                                                Right justified

                                                If overloaded or underrange the character will carry into weight field but is being ignored for these cases because it was already determined
                                            */
                                            case 2:
                                            case 3:
                                            case 4:
                                            case 5:
                                            case 6:
                                            case 7:
                                            case 8:
                                                _textWeight.Append(charOfScaleReadString);

                                                if (_iChar == 8)
                                                {
                                                    int weightConvertStringToInt;
                                                    if (int.TryParse(_textWeight.ToString(), out weightConvertStringToInt))
                                                    {
                                                        _result.StreamWeight = (_result.StreamPolarity ==
                                                                               ScaleIndicatorStreamPolarity.Negative)
                                                            ? weightConvertStringToInt * -1
                                                            : weightConvertStringToInt;
                                                    }
                                                    else
                                                    {
                                                        _log.Warn("StreamWeight could not be paresed as an int: " +
                                                                  _result.StreamWeight + "   Full string: " +
                                                                  serialPortDataRecieved);
                                                    }
                                                }
                                                break;


                                            case 9:
                                                if (Enum.IsDefined(typeof(ScaleIndicatorStreamUnitOfWeight),
                                                    (int)charOfScaleReadString))
                                                {
                                                    _result.StreamUnitOfWeight =
                                                        (ScaleIndicatorStreamUnitOfWeight)
                                                            Enum.Parse(typeof(ScaleIndicatorStreamUnitOfWeight),
                                                                charOfScaleReadString.ToString());
                                                }
                                                else
                                                {
                                                    _log.Error(
                                                        new Exception(
                                                            "Scale Indicator Stream Enum.IsDefined(typeof(ScaleIndicatorStreamUnitOfWeight) with value " +
                                                            charOfScaleReadString));
                                                }
                                                break;

                                            case 10:
                                                if (Enum.IsDefined(typeof(ScaleIndicatorStreamWeightGrossNet),
                                                    (int)charOfScaleReadString))
                                                {
                                                    _result.StreamWeightGrossNet =
                                                        (ScaleIndicatorStreamWeightGrossNet)
                                                            Enum.Parse(typeof(ScaleIndicatorStreamWeightGrossNet),
                                                                charOfScaleReadString.ToString());
                                                }
                                                else
                                                {
                                                    _log.Error(
                                                        new Exception(
                                                            "Scale Indicator Stream Enum.IsDefined(typeof(ScaleIndicatorStreamWeightGrossNet) with value " +
                                                            charOfScaleReadString));
                                                }
                                                break;

                                            case 11:
                                                if (_scaleData.IsMotionDetectionEnabled)
                                                {
                                                    if (Enum.IsDefined(typeof(ScaleIndicatorStreamMotionStatus),
                                                        (int)charOfScaleReadString))
                                                    {
                                                        _result.StreamMotionStatus = (ScaleIndicatorStreamMotionStatus)charOfScaleReadString;
                                                    }
                                                    else
                                                    {
                                                        _log.Error(
                                                            new Exception(
                                                                "Scale Indicator Stream Enum.TryParse() typeof(ScaleIndicatorStreamMotionStatus) with value " +
                                                                charOfScaleReadString));
                                                    }
                                                }
                                                break;

                                            case 12:
                                                _textEnd = true;
                                                break;

                                        }

                                        #endregion
                                    }

                                    EndOfStreamOrNextChar();
                                }
                            }

                            #endregion

                            break;

                        case ScaleIndicatorOutputFormat.SignWeight:

                            #region SignWeight

                            _result.StreamPolarity = ScaleIndicatorStreamPolarity.Positive;
                            _result.StreamUnitOfWeight = ScaleIndicatorStreamUnitOfWeight.L;
                            _result.StreamWeightGrossNet = ScaleIndicatorStreamWeightGrossNet.G;

                            //-- https://www.regex101.com/r/GT3Kxn/1
                            //-- Delete: https://www.regex101.com/delete/GYSNMlFrdwkM80wj9lF7Pnl6

                            match = Regex.Match(Encoding.ASCII.GetString(_comBuffer), @"^[-|\s].*\n", RegexOptions.Compiled);   
                            serialPortDataRecieved = match.ToString();

                            if (IsScaleDataRecievedByteLengthMatch(serialPortDataRecieved.Length, STREAM_BYTE_MAX_LENGTH_SIGNWEIGHT, _senderSerialPort.PortName))
                            {
                                _iChar = 0;

                                string polarity = string.Empty;
                                _textWeight.Clear();

                                foreach (char charOfScaleReadString in serialPortDataRecieved)
                                {
                                    if (charOfScaleReadString == '-' || charOfScaleReadString == ' ')
                                    {
                                        //polarty = charOfScaleReadString;
                                    }
                                    else if (charOfScaleReadString.ToString() == "\n" || charOfScaleReadString.ToString() == "\r")
                                    {
                                        int weightConvertStringToInt;
                                        if (int.TryParse(_textWeight.ToString(), out weightConvertStringToInt))
                                        {
                                            _result.StreamWeight = (_result.StreamPolarity ==
                                                                   ScaleIndicatorStreamPolarity.Negative)
                                                ? weightConvertStringToInt * -1
                                                : weightConvertStringToInt;
                                        }
                                        else
                                        {
                                            _log.Warn("StreamWeight could not be paresed as an int: " +
                                                      _result.StreamWeight + "   Full string: " +
                                                      serialPortDataRecieved);
                                        }

                                        break;
                                    }
                                    else
                                    {
                                        _textWeight.Append(charOfScaleReadString);
                                    }
                                }
                            }

                            #endregion

                            break;

                        case ScaleIndicatorOutputFormat.GSE660:

                            #region GSE660

                            _result.StreamPolarity = ScaleIndicatorStreamPolarity.Positive;
                            _result.StreamUnitOfWeight = ScaleIndicatorStreamUnitOfWeight.l;
                            _result.StreamWeightGrossNet = ScaleIndicatorStreamWeightGrossNet.G;

                            //-- https://www.regex101.com/r/GT3Kxn/1
                            //-- Delete: https://www.regex101.com/delete/GYSNMlFrdwkM80wj9lF7Pnl6

                            //match = Regex.Match(Encoding.ASCII.GetString(_comBuffer), @"[\s|-]([\s0-9]{7})(\r)", RegexOptions.Singleline);
                            match = Regex.Match(Encoding.ASCII.GetString(_comBuffer), "(\\u0002)(.*?)(\\r)", RegexOptions.Singleline);
                            serialPortDataRecieved = match.ToString();

                            if (IsScaleDataRecievedByteLengthMatch(serialPortDataRecieved.Length, STREAM_BYTE_MAX_LENGTH_GSE660, _senderSerialPort.PortName))
                            {
                                _iChar = 0;

                                string polarity = string.Empty;
                                _textWeight.Clear();

                                _iChar = 0;

                                foreach (char charOfScaleReadString in serialPortDataRecieved)
                                {
                                    //-- \u0002 = Unicode Start-of-Text
                                    //-- Having this character at the beginning of the line is standard for all RiceLake and FairBanks data formats
                                    if (charOfScaleReadString == '\u0002')
                                    {
                                        _textStart = true;
                                        _textEnd = false;
                                        _textWeight.Clear();
                                    }

                                    if (_textStart && !_textEnd)
                                    {
                                        #region Parse Indicator Stream

                                        switch (_iChar)
                                        {

                                            //case 1:
                                            //case 2:
                                            //-- Do nothing, these char's are part of the beginning of the ascii string
                                            //     break;

                                            /*-- Character Interpritations: Polarity of weight
                                                <space> = positive weight
                                                - = negative weight
                                                ^ = overload
                                                ] = underrange
                                            */
                                            case 1:
                                                if (Enum.IsDefined(typeof(ScaleIndicatorStreamPolarity),
                                                    (int)charOfScaleReadString))
                                                {
                                                    _result.StreamPolarity = (ScaleIndicatorStreamPolarity)charOfScaleReadString;
                                                }
                                                else
                                                {
                                                    _log.Error(
                                                        new Exception(
                                                            "Scale Indicator Stream Enum.IsDefined(typeof(ScaleIndicatorStreamPolarity) with value " +
                                                            charOfScaleReadString));
                                                }

                                                break;

                                            /*-- Character Interpritations: Weight number
                                                Weight is 7 digits in length
                                                Leading 0's are surpressed
                                                Right justified

                                                If overloaded or underrange the character will carry into weight field but is being ignored for these cases because it was already determined
                                            */
                                            case 2:
                                            case 3:
                                            case 4:
                                            case 5:
                                            case 6:
                                            case 7:
                                            case 8:
                                                _textWeight.Append(charOfScaleReadString);

                                                if (_iChar == 8)
                                                {
                                                    int weightConvertStringToInt;
                                                    if (int.TryParse(_textWeight.ToString().Substring(0, _textWeight.Length-1), out weightConvertStringToInt))
                                                    {
                                                        _result.StreamWeight = (_result.StreamPolarity ==
                                                                               ScaleIndicatorStreamPolarity.Negative)
                                                            ? weightConvertStringToInt * -1
                                                            : weightConvertStringToInt;
                                                    }
                                                    else
                                                    {
                                                        _log.Warn("StreamWeight could not be paresed as an int: " +
                                                                  _result.StreamWeight + "   Full string: " +
                                                                  serialPortDataRecieved);
                                                    }
                                                }
                                                break;


                                            case 9:
                                                if (Enum.IsDefined(typeof(ScaleIndicatorStreamUnitOfWeight),
                                                    (int)charOfScaleReadString))
                                                {
                                                    _result.StreamUnitOfWeight =
                                                        (ScaleIndicatorStreamUnitOfWeight)
                                                            Enum.Parse(typeof(ScaleIndicatorStreamUnitOfWeight),
                                                                charOfScaleReadString.ToString());
                                                }
                                                else
                                                {
                                                    _log.Error(
                                                        new Exception(
                                                            "Scale Indicator Stream Enum.IsDefined(typeof(ScaleIndicatorStreamUnitOfWeight) with value " +
                                                            charOfScaleReadString));
                                                }
                                                break;

                                            case 10:
                                                if (Enum.IsDefined(typeof(ScaleIndicatorStreamWeightGrossNet),
                                                    (int)charOfScaleReadString))
                                                {
                                                    _result.StreamWeightGrossNet =
                                                        (ScaleIndicatorStreamWeightGrossNet)
                                                            Enum.Parse(typeof(ScaleIndicatorStreamWeightGrossNet),
                                                                charOfScaleReadString.ToString());
                                                }
                                                else
                                                {
                                                    _log.Error(
                                                        new Exception(
                                                            "Scale Indicator Stream Enum.IsDefined(typeof(ScaleIndicatorStreamWeightGrossNet) with value " +
                                                            charOfScaleReadString));
                                                }
                                                break;

                                            case 11:
                                                if (_scaleData.IsMotionDetectionEnabled)
                                                {
                                                    if (Enum.IsDefined(typeof(ScaleIndicatorStreamMotionStatus),
                                                        (int)charOfScaleReadString))
                                                    {
                                                        _result.StreamMotionStatus = (ScaleIndicatorStreamMotionStatus)charOfScaleReadString;
                                                    }
                                                    else
                                                    {
                                                        _log.Error(
                                                            new Exception(
                                                                "Scale Indicator Stream Enum.TryParse() typeof(ScaleIndicatorStreamMotionStatus) with value " +
                                                                charOfScaleReadString));
                                                    }
                                                }
                                                break;

                                            case 12:
                                                _textEnd = true;
                                                break;

                                        }

                                        #endregion
                                    }

                                    EndOfStreamOrNextChar();
                                }
                            }

                            #endregion

                            break;

                        case ScaleIndicatorOutputFormat.AVERYWEIGHTRONIX_ZM405:

                            #region AVERYWEIGHTRONIX_ZM405

                            _result.StreamPolarity = ScaleIndicatorStreamPolarity.Positive;
                            _result.StreamUnitOfWeight = ScaleIndicatorStreamUnitOfWeight.l;
                            _result.StreamWeightGrossNet = ScaleIndicatorStreamWeightGrossNet.G;

                            //-- https://www.regex101.com/r/GT3Kxn/1
                            //-- Delete: https://www.regex101.com/delete/GYSNMlFrdwkM80wj9lF7Pnl6

                            //match = Regex.Match(Encoding.ASCII.GetString(_comBuffer), @"[\s|-]([\s0-9]{7})(\r)", RegexOptions.Singleline);
                            match = Regex.Match(Encoding.ASCII.GetString(_comBuffer), "(\\u0002)(.*?)(\\r)", RegexOptions.Singleline);
                            serialPortDataRecieved = match.ToString();

                            if (IsScaleDataRecievedByteLengthMatch(serialPortDataRecieved.Length, STREAM_BYTE_MAX_LENGTH_AVERYWEIGHTRONIX_ZM405, _senderSerialPort.PortName))
                            {
                                _iChar = 0;

                                string polarity = string.Empty;
                                _textWeight.Clear();

                                _iChar = 0;

                                foreach (char charOfScaleReadString in serialPortDataRecieved)
                                {
                                    //-- \u0002 = Unicode Start-of-Text
                                    //-- Having this character at the beginning of the line is standard for all RiceLake and FairBanks data formats
                                    if (charOfScaleReadString == '\u0002')
                                    {
                                        _textStart = true;
                                        _textEnd = false;
                                        _textWeight.Clear();
                                    }

                                    if (_textStart && !_textEnd)
                                    {
                                        #region Parse Indicator Stream

                                        switch (_iChar)
                                        {

                                            //case 1:
                                            //case 2:
                                            //-- Do nothing, these char's are part of the beginning of the ascii string
                                            //     break;

                                            /*-- Character Interpritations: Polarity of weight
                                                <space> = positive weight
                                                - = negative weight
                                                ^ = overload
                                                ] = underrange
                                            */
                                            case 1:
                                                if (Enum.IsDefined(typeof(ScaleIndicatorStreamPolarity),
                                                    (int)charOfScaleReadString))
                                                {
                                                    _result.StreamPolarity = (ScaleIndicatorStreamPolarity)charOfScaleReadString;
                                                }
                                                else
                                                {
                                                    _log.Error(
                                                        new Exception(
                                                            "Scale Indicator Stream Enum.IsDefined(typeof(ScaleIndicatorStreamPolarity) with value " +
                                                            charOfScaleReadString));
                                                }

                                                break;

                                            /*-- Character Interpritations: Weight number
                                                Weight is 7 digits in length
                                                Leading 0's are surpressed
                                                Right justified

                                                If overloaded or underrange the character will carry into weight field but is being ignored for these cases because it was already determined
                                            */
                                            case 2:
                                            case 3:
                                            case 4:
                                            case 5:
                                            case 6:
                                            case 7:
                                            case 8:
                                                _textWeight.Append(charOfScaleReadString);

                                                if (_iChar == 8)
                                                {
                                                    int weightConvertStringToInt;
                                                    if (int.TryParse(_textWeight.ToString().Substring(0, _textWeight.Length - 1), out weightConvertStringToInt))
                                                    {
                                                        _result.StreamWeight = (_result.StreamPolarity ==
                                                                               ScaleIndicatorStreamPolarity.Negative)
                                                            ? weightConvertStringToInt * -1
                                                            : weightConvertStringToInt;
                                                    }
                                                    else
                                                    {
                                                        _log.Warn("StreamWeight could not be paresed as an int: " +
                                                                  _result.StreamWeight + "   Full string: " +
                                                                  serialPortDataRecieved);
                                                    }
                                                }
                                                break;


                                            case 10:
                                                if (Enum.IsDefined(typeof(ScaleIndicatorStreamUnitOfWeight),
                                                    (int)charOfScaleReadString))
                                                {
                                                    _result.StreamUnitOfWeight =
                                                        (ScaleIndicatorStreamUnitOfWeight)
                                                            Enum.Parse(typeof(ScaleIndicatorStreamUnitOfWeight),
                                                                charOfScaleReadString.ToString());
                                                }
                                                else
                                                {
                                                    _log.Error(
                                                        new Exception(
                                                            "Scale Indicator Stream Enum.IsDefined(typeof(ScaleIndicatorStreamUnitOfWeight) with value " +
                                                            charOfScaleReadString));
                                                }
                                                break;

                                            case 9:
                                                if (Enum.IsDefined(typeof(ScaleIndicatorStreamWeightGrossNet),   
                                                    (int)charOfScaleReadString))
                                                {
                                                    _result.StreamWeightGrossNet =
                                                        (ScaleIndicatorStreamWeightGrossNet)
                                                            Enum.Parse(typeof(ScaleIndicatorStreamWeightGrossNet),
                                                                charOfScaleReadString.ToString());
                                                }
                                                else
                                                {
                                                    _log.Error(
                                                        new Exception(
                                                            "Scale Indicator Stream Enum.IsDefined(typeof(ScaleIndicatorStreamWeightGrossNet) with value " +
                                                            charOfScaleReadString));
                                                }
                                                break;

                                            case 11:
                                                if (_scaleData.IsMotionDetectionEnabled)
                                                {
                                                    if (Enum.IsDefined(typeof(ScaleIndicatorStreamMotionStatus),
                                                        (int)charOfScaleReadString))
                                                    {
                                                        _result.StreamMotionStatus = (ScaleIndicatorStreamMotionStatus)charOfScaleReadString;
                                                    }
                                                    else
                                                    {
                                                        _log.Error(
                                                            new Exception(
                                                                "Scale Indicator Stream Enum.TryParse() typeof(ScaleIndicatorStreamMotionStatus) with value " +
                                                                charOfScaleReadString));
                                                    }
                                                }
                                                break;

                                            case 12:
                                                _textEnd = true;
                                                break;

                                        }

                                        #endregion
                                    }

                                    EndOfStreamOrNextChar();
                                }
                            }

                            #endregion

                            break;
                    }

                    #endregion
                }
                else
                {
                    _log.Info($"Serial port closed: {_scaleData.ScaleName} {_serialPort.PortName}");
                    Debug.WriteLine($"Serial port closed: {_scaleData.ScaleName} {_serialPort.PortName}");

                    _result = null;
                }
                

                if (_result != null)
                {
                    Debug.WriteLine("Polarity: " + _result.StreamPolarity);
                    Debug.WriteLine("Weight: " + _result.StreamWeight);
                    Debug.WriteLine("Unit of Weight: " + _result.StreamUnitOfWeight);
                    Debug.WriteLine("Weight Type: " + _result.StreamWeightGrossNet);
                    if (_scaleData.IsMotionDetectionEnabled) {

                        Debug.WriteLine("Motion Status: " + _result.StreamMotionStatus);
                    }
                    else
                    {
                        Debug.WriteLine("Motion Status: Not Set"); 
                    }
                    Debug.WriteLine(string.Empty);

                    _log.Info($"Polarity: {_result.StreamPolarity}  Weight: {_result.StreamWeight}  Unit of Weight: {_result.StreamUnitOfWeight}  Weight Type: {_result.StreamWeightGrossNet}  Motion Status: {_result.StreamMotionStatus}");

                    _eventAggregator.PublishOnCurrentThread(new ScaleResultsArgs(_result));
                }
                else
                {
                    _log.Warn($"SerialPort_DataReceived: Scale result was null and stream wasn't able to be parsed");
                }
                
                
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                _eventAggregator.PublishOnCurrentThread(new ScaleExceptionsArgs(_scaleData.Id, ex));
            }
        }

        private void handleReceivedBytes(object state)
        {

        }

        private bool IsScaleDataRecievedByteLengthMatch(int actualLength, int expectedLength, string indicatorPortName)
        {
            
            if (actualLength == expectedLength)
            {
                string message = $"Scale Reading: Byte length matched expected indicator byte length of '{actualLength}'.  ComPort '{indicatorPortName}' with output stream '{_outputStreamFormat}'.";
                Debug.WriteLine(message);
                _log.Info(message);

                return true;
            }
            else
            {
                string message = $"Scale Reading Byte Lengh mismatch{Environment.NewLine}Actual byte length is '{actualLength}' and expected length is '{STREAM_BYTE_MAX_LENGTH_IQ355}'.  ComPort '{indicatorPortName}' with output stream '{_outputStreamFormat}'";
                Debug.WriteLine(message);
                _log.Warn(message);

                return false;
            }
        }

        private void EndOfStreamOrNextChar()
        {
            if (_textStart && _textEnd)
            {
                Debug.WriteLine("Return weight");
                _textStart = false;
                _textEnd = false;
                int parseResult;
                if (int.TryParse(_textWeight.ToString(), out parseResult))
                    _result.StreamWeight = parseResult;
                _textWeight.Clear();
            }
            else
            {
                _iChar++;
            }
        }

        private void SerialPortClose()
        {
            if (_serialPort != null && _serialPort.IsOpen)
            {
                _log.Info("SerialPortClose - serialPort is open and not null");

                _serialPort.DataReceived -= SerialPort_DataReceived;
                _log.Info("SerialPortClose - Detach event of DataReceived");

                if (_serialPort.IsOpen)
                {
                    _log.Info($"SerialPortClose - Check serialPort is open #1  Status: {_serialPort.IsOpen}");

                    while (_serialPort.BytesToRead != 0)
                    {
                        _serialPort.DiscardInBuffer();
                        _log.Info($"SerialPortClose - serialPort DiscardInBuffer  Buffer Value: TBD");
                    }
                    try
                    {
                        if (_serialPort.IsOpen)
                        {
                            _log.Info($"SerialPortClose - Check serialPort is open #2  Status: {_serialPort.IsOpen}");

                            if (_serialPort.IsOpen)
                            {
                                _log.Info($"SerialPortClose - Check serialPort is open #3  Status: {_serialPort.IsOpen}");

                                _serialPort.Close();
                                _log.Info("SerialPortClose - serialPort is now closed");
                            }

                        }
                    }catch(Exception ex)
                    {
                        _log.Warn($"SerialPortClose Exception handler - {ex.Message}");
                    }
                }
            }
        }

        private enum ScaleIndicatorOutputFormat
        {
            AVERYWEIGHTRONIX,
            AVERYWEIGHTRONIX_ZM405,
            CARDINAL,
            SB200,
            FAIRBANKS,
            IQ355,
            SignWeight,
            GSE660,
            FAIRBANKS_TOLEDO_CR,
            FAIRBANKS_TOLEDO_CRLF,
        }
    }

    public class SerialPortFixer : IDisposable
    {
        //-- Reference: http://zachsaw.blogspot.com/2010/07/net-serialport-woes.html
        
        public static void Execute(string portName)
        {
            using (new SerialPortFixer(portName))
            {
            }
        }
        #region IDisposable Members

        public void Dispose()
        {
            if (m_Handle != null)
            {
                m_Handle.Close();
                m_Handle = null;
            }
        }

        #endregion

        #region Implementation

        private const int DcbFlagAbortOnError = 14;
        private const int CommStateRetries = 10;
        private SafeFileHandle m_Handle;

        private SerialPortFixer(string portName)
        {
            const uint dwFlagsAndAttributes = 0x40000000;
            const uint dwAccess = unchecked((uint)0xC0000000);

            if ((portName == null) || !portName.StartsWith("COM", StringComparison.OrdinalIgnoreCase))
            {
                throw new ArgumentException("Invalid Serial Port", "portName");
            }
            SafeFileHandle hFile = CreateFile(@"\\.\" + portName, dwAccess, 0, IntPtr.Zero, 3, dwFlagsAndAttributes,
                                              IntPtr.Zero);
            if (hFile.IsInvalid)
            {
                WinIoError();
            }
            try
            {
                int fileType = GetFileType(hFile);
                if ((fileType != 2) && (fileType != 0))
                {
                    throw new ArgumentException("Invalid Serial Port", "portName");
                }
                m_Handle = hFile;
                InitializeDcb();
            }
            catch
            {
                hFile.Close();
                m_Handle = null;
                throw;
            }
        }

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int FormatMessage(int dwFlags, HandleRef lpSource, int dwMessageId, int dwLanguageId,
                                                StringBuilder lpBuffer, int nSize, IntPtr arguments);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern bool GetCommState(SafeFileHandle hFile, ref Dcb lpDcb);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern bool SetCommState(SafeFileHandle hFile, ref Dcb lpDcb);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern bool ClearCommError(SafeFileHandle hFile, ref int lpErrors, ref Comstat lpStat);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern SafeFileHandle CreateFile(string lpFileName, uint dwDesiredAccess, uint dwShareMode,
                                                        IntPtr securityAttrs, uint dwCreationDisposition,
                                                        uint dwFlagsAndAttributes, IntPtr hTemplateFile);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern int GetFileType(SafeFileHandle hFile);

        private void InitializeDcb()
        {
            Dcb dcb = new Dcb();
            GetCommStateNative(ref dcb);
            dcb.Flags &= ~(1u << DcbFlagAbortOnError);
            SetCommStateNative(ref dcb);
        }

        private static string GetMessage(int errorCode)
        {
            StringBuilder lpBuffer = new StringBuilder(0x200);
            if (
                FormatMessage(0x3200, new HandleRef(null, IntPtr.Zero), errorCode, 0, lpBuffer, lpBuffer.Capacity,
                              IntPtr.Zero) != 0)
            {
                return lpBuffer.ToString();
            }
            return "Unknown Error";
        }

        private static int MakeHrFromErrorCode(int errorCode)
        {
            return (int)(0x80070000 | (uint)errorCode);
        }

        private static void WinIoError()
        {
            int errorCode = Marshal.GetLastWin32Error();
            throw new IOException(GetMessage(errorCode), MakeHrFromErrorCode(errorCode));
        }

        private void GetCommStateNative(ref Dcb lpDcb)
        {
            int commErrors = 0;
            Comstat comStat = new Comstat();

            for (int i = 0; i < CommStateRetries; i++)
            {
                if (!ClearCommError(m_Handle, ref commErrors, ref comStat))
                {
                    WinIoError();
                }
                if (GetCommState(m_Handle, ref lpDcb))
                {
                    break;
                }
                if (i == CommStateRetries - 1)
                {
                    WinIoError();
                }
            }
        }

        private void SetCommStateNative(ref Dcb lpDcb)
        {
            int commErrors = 0;
            Comstat comStat = new Comstat();

            for (int i = 0; i < CommStateRetries; i++)
            {
                if (!ClearCommError(m_Handle, ref commErrors, ref comStat))
                {
                    WinIoError();
                }
                if (SetCommState(m_Handle, ref lpDcb))
                {
                    break;
                }
                if (i == CommStateRetries - 1)
                {
                    WinIoError();
                }
            }
        }

        #region Nested type: COMSTAT

        [StructLayout(LayoutKind.Sequential)]
        private struct Comstat
        {
            public readonly uint Flags;
            public readonly uint cbInQue;
            public readonly uint cbOutQue;
        }

        #endregion

        #region Nested type: DCB

        [StructLayout(LayoutKind.Sequential)]
        private struct Dcb
        {
            public readonly uint DCBlength;
            public readonly uint BaudRate;
            public uint Flags;
            public readonly ushort wReserved;
            public readonly ushort XonLim;
            public readonly ushort XoffLim;
            public readonly byte ByteSize;
            public readonly byte Parity;
            public readonly byte StopBits;
            public readonly byte XonChar;
            public readonly byte XoffChar;
            public readonly byte ErrorChar;
            public readonly byte EofChar;
            public readonly byte EvtChar;
            public readonly ushort wReserved1;
        }

        #endregion

        #endregion
    }


    public static class Retry
    {
        public static void Do(
            Action action,
            TimeSpan retryInterval,
            int retryCount = 3)
        {
            Do<object>(() =>
            {
                action();
                return null;
            }, retryInterval, retryCount);
        }

        public static T Do<T>(
            Func<T> action,
            TimeSpan retryInterval,
            int retryCount = 3)
        {
            var exceptions = new List<Exception>();

            for (int retry = 0; retry < retryCount; retry++)
            {
                try
                {
                    if (retry > 0)
                        Thread.Sleep(retryInterval);
                    return action();
                }
                catch (Exception ex)
                {
                    exceptions.Add(ex);
                }
            }

            throw new AggregateException(exceptions);
        }
    }
}