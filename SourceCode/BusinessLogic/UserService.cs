﻿using Contract.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;

namespace BusinessLogic
{
    public class UserService : IUserService
    {
        private readonly IConfiguration _configuration;

        public UserService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public bool Authenticate(string userName, string password)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var user = data.Users.FirstOrDefault(x => x.UserName == userName && x.Password == password);

                if (user != null)
                {
                    UserAuthorization.UserAuthorizationLoggedIn = user;
                    //UserAuthorization.UserAuthorizationLoggedIn.is
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public ICollection<User> GetAll()
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var query = data.Users;
                return query.ToList();
            }
        }

        public User GetByUsername(string userName)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var user = data.Users.FirstOrDefault(x => x.UserName == userName);
                return user;
            }
        }

        public void DefaultUser()
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var user = data.Users.Where(x => x.UserName == "Default").FirstOrDefault();
                UserAuthorization.UserAuthorizationLoggedIn = user;
            }
        }


    }

}
