﻿using System;
using System.Linq;
using System.Threading;
using Caliburn.Micro;
using Contract.Events;
using Contract.Services;
using DataAccess;

namespace BusinessLogic
{
    public class OrderTrackingService : IOrderTrackingService, IDisposable
    {
        private readonly IConfiguration _configuration;
        private readonly IEventAggregator _eventAggregator;
        private Timer _dispatcherTimer;
        private readonly ILog _log = LogManager.GetLog(typeof (ScaleManager));

        public OrderTrackingService(IConfiguration configuration, IEventAggregator eventAggregator)
        {
            _configuration = configuration;
            _eventAggregator = eventAggregator;
        }

        private void CheckOrders(object state)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var season = _configuration.Season;
                var lastUpdate = _configuration.LastOrderUpdate;

                var result = data.OrderTables.AsNoTracking().Any(o => o.SeasonId == season && (o.CreateTime > lastUpdate || o.UpdateTime > lastUpdate));

                if (result)
                {
                    _eventAggregator.PublishOnUIThreadAsync(new OrderModifiedArgs(null));
                }

                //-- For TonsToday
                _eventAggregator.PublishOnCurrentThread(new TicketAddedVoidedArgs(null));
            }
        }

        public void Dispose()
        {
            _dispatcherTimer.Dispose();
        }

        public void StartTracking()
        {
            if (_dispatcherTimer != null)
                _dispatcherTimer.Dispose();

            var interval = _configuration.OrdersRefreshTimeout;
            if (interval <= 0)
                interval = 180;

            _dispatcherTimer = new Timer(CheckOrders, null, new TimeSpan(0, 0, 3, 0), new TimeSpan(0, 0, 0, interval));
        }

        public void StopTracking()
        {
            if (_dispatcherTimer != null)
            {
                _dispatcherTimer.Change(Timeout.Infinite, Timeout.Infinite);
                _dispatcherTimer.Dispose();
            }
        }
    }
}