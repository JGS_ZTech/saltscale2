﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Contract.Domain;
using DataAccess;

namespace BusinessLogic
{
    public class ContactManager
    {
        private readonly SaltScaleEntities _data;

        public ContactManager(SaltScaleEntities data)
        {
            _data = data;
        }

        public IContactData GetContactData(int contactId, int seasonId)
        {
            Contact contact =
                _data.Contacts.AsNoTracking()
                    .Where(c => c.IsActive && c.ContactID == contactId && c.SeasonId == seasonId)
                    .OrderByDescending(cc => cc.ContactID)
                    .FirstOrDefault();

            return new ContactData(contact);
        }

        public int SaveContact(IContactData contactData, int seasonId)
        {
            if(contactData == null) 
                throw  new ArgumentNullException("contactData");

            Contact contact;

            if (!contactData.ContactId.HasValue)
            {
                contact = _data.Contacts.Create();
                contact.IsActive = true;
                contact.CreateTime = DateTime.Now;
                contact.SeasonId = seasonId;
                _data.Contacts.Add(contact);
            }
            else
            {
                // 2018-JUl-17, JGS - Make explicit use of the SeasonID
                //contact = _data.Contacts.First(c => c.ContactID == contactData.ContactId.Value);
                contact = _data.Contacts.First(c => c.ContactID == contactData.ContactId.Value && c.SeasonId == seasonId);
            }
            contact.Organization = contactData.Organization;
            contact.ContactComment = contactData.ContactComment;
            contact.Name = contactData.Name;
            contact.UpdateTime = contactData.UpdateTime;

            contact.isPrimary = contactData.IsPrimary;

            if (!contactData.Address.AddressId.HasValue)
            {
                contact.Address = new Address();
                contact.Address.CreateTime = DateTime.Now;
                contact.Address.IsActive = true;
                contact.Address.SeasonId = seasonId;
            }
            contact.Address.City = contactData.Address.City;
            contact.Address.State = contactData.Address.State;
            contact.Address.Zip = contactData.Address.Zip;
            contact.Address.UpdateTime = contactData.Address.UpdateTime;
            contact.Address.AddressLineOne = contactData.Address.AddressLineOne;
            contact.Address.AddressLineTwo = contactData.Address.AddressLineTwo;
            
            
            foreach (var phoneData in contactData.Phones)
            {
                PhoneNumber phone;

                if (!phoneData.PhoneNumberId.HasValue)
                {
                    phone = _data.PhoneNumbers.Create();
                    phone.CreateTime = DateTime.Now;
                    phone.SeasonId = seasonId;
                    contact.PhoneNumbers.Add(phone);
                }
                else
                {
                    // 2018-JUl-17, JGS - Make explicit use of the SeasonID
                    //phone = contact.PhoneNumbers.First(p => p.PhoneNumberID == phoneData.PhoneNumberId.Value);
                    phone = contact.PhoneNumbers.First(p => p.PhoneNumberID == phoneData.PhoneNumberId.Value && p.SeasonId == seasonId);
                }
                phone.Comment = phoneData.Comment;
                phone.NumberType = phoneData.NumberType;
                phone.PhoneNumber1 = phoneData.Number;
                phone.isPrimary = phoneData.IsPrimary;
                phone.IsActive = phoneData.IsActive;
                phone.SeasonId = phoneData.SeasonId;
                phone.UpdateTime = phoneData.UpdateTime;
            }
            _data.SaveChanges();

            return contact.ContactID;
        }

        public void InsertOwnerContact(int contactId, int ownerId)
        {
                var truckOwnerContact = _data.TruckOwnerContacts.Create();

                truckOwnerContact.ContactID = contactId;
                truckOwnerContact.TruckOwnerID = ownerId;
                
                _data.TruckOwnerContacts.Add(truckOwnerContact);
                _data.SaveChanges();
        }

        public void InsertCustomerContact(int contactId, int customerId)
        {
                var customerContact = _data.CustomerContacts.Create();

                customerContact.ContactID = contactId;
                customerContact.CustomerID = customerId;

                _data.CustomerContacts.Add(customerContact);
                _data.SaveChanges();
        }

        public void DeleteContact(int contactId, int seasonId)
        {
            // 2018-JUl-17, JGS - Make explicit use of the SeasonID
            //var contact = _data.Contacts.FirstOrDefault(c => c.ContactID == contactId);
            var contact = _data.Contacts.FirstOrDefault(c => c.ContactID == contactId && c.SeasonId == seasonId);
            if (contact == null)
                return;
            
            contact.IsActive = false;
            contact.UpdateTime = DateTime.Now;

            _data.SaveChanges();
        }

        public IReadOnlyList<IContactInfo> GetContactList(int seasonId)
        {
            return _data.Contacts.Where(c => c.IsActive && c.SeasonId == seasonId).Select(ContactInfo.Transform).OrderBy(c=>c.Organization).ToList();
        }

        public IReadOnlyList<IContactInfo> GetFilteredContacts(string filter, int seasonId)
        {
            return
                _data.Contacts.Where(
                    c =>
                        c.IsActive && c.SeasonId == seasonId &&
                        (c.Name.Contains(filter) || c.Organization.Contains(filter)))
                    .Select(ContactInfo.Transform).OrderBy(
                        c => c.Organization).ToList();
        }

        public IReadOnlyList<IContactInfo> GetFilteredContacts(string filter, bool hideCustomers, bool hideTruckOwners, bool hideOthers, int seasonId)
        {
            var query = _data.Contacts.Where(c =>c.IsActive && c.SeasonId == seasonId);
            
            if (!string.IsNullOrWhiteSpace(filter))
            {
                filter = filter.Trim();
                query = query.Where(c => (c.Name.Contains(filter) || c.Organization.Contains(filter)));
            }
            if (hideCustomers)
            {
                query = query.Where(c => !c.CustomerContacts.Any());
            }
            if (hideTruckOwners)
            {
                query = query.Where(c => !c.TruckOwnerContacts.Any());
            }
            if (hideOthers)
            {
                query = query.Where(c => (c.CustomerContacts.Any() || c.TruckOwnerContacts.Any()));
            }
            return query.Select(ContactInfo.Transform).OrderBy(
                       c => c.Organization).ToList();
        }

        public bool CanDeleteContact(int contactId, int seasonId)
        {
            var customerContact = _data.CustomerContacts.FirstOrDefault(c => c.ContactID == contactId);
            if (customerContact!=null)
            {
                return customerContact.Customer.CustomerContacts.Any(c => c.ContactID != contactId && c.Contact.IsActive);
            }

            var tructOwnerContact = _data.TruckOwnerContacts.FirstOrDefault(c => c.ContactID == contactId);
            if (tructOwnerContact != null)
            {
                return tructOwnerContact.TruckOwner.TruckOwnerContacts.Any(c => c.ContactID != contactId && c.Contact.IsActive);
            }

            return true;
        }
    }

    #region DTO

    internal class ContactData : IContactData
    {
        public ContactData()
        {
            Address= new AddressData();
            Phones = new List<IPhoneData>();
        }

        public ContactData(Contact contact)
        {
            Comment = contact.Comment;
            ContactComment = contact.ContactComment;
            Name = contact.Name;
            Organization = contact.Organization;
            ContactId = contact.ContactID;
            IsActive = contact.IsActive;
            Address = new AddressData(contact.Address);
            Phones = contact.PhoneNumbers.Select(p => new PhoneData(p)).Cast<IPhoneData>().ToList();
            CreateTime = contact.CreateTime;
            UpdateTime = contact.UpdateTime;
            IsPrimary = contact.isPrimary;
            SeasonId = contact.SeasonId;
        }

        public int? ContactId { get; private set; }
        public string Organization { get; set; }
        public string Name { get; set; }
        public string ContactComment { get; set; }
        public string Comment { get; set; }
        public IAddressData Address { get;  set; }
        public List<IPhoneData> Phones { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        public bool IsActive { get; set; }
        public bool IsPrimary { get; set; }
        public int SeasonId { get; set; }
    }

    internal class AddressData : IAddressData
    {
        public AddressData()
        {
            
        }
        public AddressData(Address address)
        {
            AddressLineOne = address.AddressLineOne;
            AddressLineTwo = address.AddressLineTwo;
            City = address.City;
            Comment = address.Comment;
            State = address.State;
            Zip = address.Zip;
            AddressId = address.AddressID;
            IsActive = address.IsActive;
            CreateTime = address.CreateTime;
            UpdateTime = address.UpdateTime;
            SeasonId = address.SeasonId;
        }

        public string AddressLineOne { get; set; }
        public string AddressLineTwo { get; set; }
        public string City { get; set; }
        public string Comment { get; set; }
        public string State { get; set; }
        public int? AddressId { get; set; }
        public string Zip { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        public bool IsActive { get; set; }
        public int SeasonId { get; set; }
    }

    internal class PhoneData : IPhoneData
    {
        public PhoneData()
        {
        }

        public PhoneData(PhoneNumber phoneNumber)
        {
            Comment = phoneNumber.Comment;
            IsActive = phoneNumber.IsActive;
            NumberType = phoneNumber.NumberType;
            Number = phoneNumber.PhoneNumber1;
            PhoneNumberId = phoneNumber.PhoneNumberID;
            IsPrimary = phoneNumber.isPrimary;
            CreateTime = phoneNumber.CreateTime;
            UpdateTime = phoneNumber.UpdateTime;
            SeasonId = phoneNumber.SeasonId;
        }

        public string Comment { get; set; }
        public bool IsActive { get; set; }
        public string NumberType { get; set; }
        public string Number { get; set; }
        public int? PhoneNumberId { get; set; }
        public bool IsPrimary { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        public int SeasonId { get; set; }
    }

    internal class ContactInfo : IContactInfo
    {
        public static readonly Expression<Func<Contact, IContactInfo>> Transform = e => new ContactInfo
        {
            ContactId = e.ContactID,
            Organization = e.Organization,
            Name = e.Name,
            PrimaryPhone =
                e.PhoneNumbers.OrderByDescending(p => p.PhoneNumberID).FirstOrDefault(
                    p => p.isPrimary && p.IsActive && !string.IsNullOrEmpty(p.PhoneNumber1)),
        };

        private PhoneNumber PrimaryPhone { get; set; }
        public int ContactId { get; private set; }
        public string Organization { get; set; }
        public string Name { get; set; }

        public string PhoneNumber
        {
            get { return PrimaryPhone != null ? PrimaryPhone.PhoneNumber1 : string.Empty; }
        }
    }
    #endregion
}