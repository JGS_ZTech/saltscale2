﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Transactions;
using Contract.Domain;
using Contract.Exception;
using Contract.Services;
using DataAccess;
using System.ComponentModel;
using System.Collections.ObjectModel;
using Syncfusion.Windows.Shared;
using Caliburn.Micro;

namespace BusinessLogic
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<T> SelectRecursive<T>(this IEnumerable<T> source, Func<T, IEnumerable<T>> selector)
        {
            foreach (var parent in source)
            {
                yield return parent;

                var children = selector(parent);
                foreach (var child in SelectRecursive(children, selector))
                    yield return child;
            }
        }
    }

    public class CustomerService : ICustomerService
    {
        private readonly IConfiguration _configuration;

        public CustomerService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public ICustomerAddress GetCustomerAddress(int customerId, int seasonId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var item = data.Customers.AsNoTracking().FirstOrDefault(c => c.SeasonId == seasonId 
                                                                        && c.CustomerID == customerId);

                if (item == null)
                    throw new EntityNotFoundException("There are no customer found.", typeof(Customer), customerId);

                var customer = new CustomerAddress(item);

                var contact = item.CustomerContacts.Select(c => c.Contact).Where(c => c.IsActive).OrderByDescending(c => c.ContactID)
                    .FirstOrDefault();
                if(contact!=null)
                    customer.Contact = new ContactData(contact);
               
                var bid = item.Bids.Where(b => b.IsActive).OrderByDescending(b => b.BidID).FirstOrDefault();
                if(bid !=null)
                    customer.Bid = new BidData(bid);

                return customer;
            }
        }

        public ICustomerInfo GetCustomerInfo(int customerId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var item =
                    data.Customers.AsNoTracking().FirstOrDefault(c => c.CustomerID == customerId);

                if (item == null)
                    throw new EntityNotFoundException("No customer found.", typeof(Customer), customerId);

                var customer = new CustomerInfo(item);

                return customer;
            }
        }

        public List<ICustomerLine> GetCustomerList(bool shippingCustomers, bool entityCustomers, int seasonId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                /*
                var customerService = data.Customers.AsNoTracking()
                   .Where(c => c.IsActive
                        && c.SeasonId == seasonId
                        && (shippingCustomers == false && entityCustomers == false
                            || (shippingCustomers && c.isShippingLocation == shippingCustomers
                            || entityCustomers && c.isShippingLocation != entityCustomers))
                        )
                   .Join(data.Bids,
                   c => c.CustomerID, b => b.CustomerID,
                   (c, b) => new CustomerLine
                   {
                       Id = c.CustomerID,
                       Name = c.DispatcherName,
                       ShortName = c.ShortName,
                       CashMaterialRate = c.CashMaterialRate,
                       TruckingRate = b.TruckingRate,
                       Toll = b.Toll,
                       EntityName = data.Customers.FirstOrDefault(r => r.CustomerID == c.ReportingEntityID).DispatcherName,
                       AddressLine1 = data.CustomerContacts.FirstOrDefault(g => g.CustomerID == c.CustomerID).Contact.Address.AddressLineOne,
                       City = data.CustomerContacts.FirstOrDefault(g => g.CustomerID == c.CustomerID).Contact.Address.City,
                       State = data.CustomerContacts.FirstOrDefault(g => g.CustomerID == c.CustomerID).Contact.Address.State,
                       Company = data.Companies.Where(x => x.CompanyId == c.CompanyId).FirstOrDefault()
                   })
                   .Select(CustomerLine.TransformLine).ToList();
                */


                var customerService = (from cust in data.Customers.AsNoTracking()
                                           //join b in data.Bids.AsNoTracking() on cust.CustomerID equals b.CustomerID into _customerBids
                                           //from b in _customerBids.DefaultIfEmpty()
                                       where cust.IsActive
                                              && cust.SeasonId == seasonId
                                              && (shippingCustomers == false && entityCustomers == false
                                                  || (shippingCustomers && cust.isShippingLocation == shippingCustomers
                                                  || entityCustomers && cust.isShippingLocation != entityCustomers))

                                       select new CustomerLine
                                       {
                                           Id = cust.CustomerID,
                                           Name = cust.DispatcherName,
                                           ShortName = cust.ShortName,
                                           CashMaterialRate = cust.CashMaterialRate,
                                           //TruckingRate = b.TruckingRate,
                                           TruckingRate = 0,
                                           //Toll = b.Toll,
                                           Toll = 0,
                                           EntityName = data.Customers.FirstOrDefault(r => r.CustomerID == cust.ReportingEntityID).DispatcherName,
                                           SeasonName = data.Seasons.FirstOrDefault(r => r.SeasonId == cust.SeasonId).Prefix,
                                           AddressLine1 = data.CustomerContacts.FirstOrDefault(g => g.CustomerID == cust.CustomerID).Contact.Address.AddressLineOne,
                                           City = data.CustomerContacts.FirstOrDefault(g => g.CustomerID == cust.CustomerID).Contact.Address.City,
                                           State = data.CustomerContacts.FirstOrDefault(g => g.CustomerID == cust.CustomerID).Contact.Address.State,
                                           Company = data.Companies.Where(x => x.CompanyId == cust.CompanyId).FirstOrDefault()
                                      })
                                      .Select(CustomerLine.TransformLine);


                return customerService.OrderBy(x => x.ShortName).ThenBy(x => x.Name).ToList();
            }
        }



        public ObservableCollection<ICustomerSeasonEstimate> GetEntityHierarchySeasonEstimate(int seasonId)
        {
            //using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            //{
            //    var query = (from entity1 in data.Customers.AsNoTracking()
            //                 from entity2 in data.Customers.AsNoTracking()
            //                    .Where(e2 => e2.ReportingEntityID == entity1.CustomerID).DefaultIfEmpty()
            //                 from entity3 in data.Customers.AsNoTracking()
            //                    .Where(e3 => e3.ReportingEntityID == entity2.CustomerID).DefaultIfEmpty()
            //                 where entity1.IsActive        
            //                && entity1.SeasonId == seasonId
            //                select new CustomerSeasonEstimate
            //                {
            //                    CustomerId = entity1.CustomerID,
            //                    ReportingEntityId = entity1.ReportingEntityID,
            //                    CustomerDispatcherName = entity1.DispatcherName,
            //                    CustomerShortName = entity1.ShortName,

            //                });               

            //    var returnCollection = new ObservableCollection<ICustomerSeasonEstimate>();

            //    /*
            //    var entity1 = new ObservableCollection<ICustomerSeasonEstimate>();
            //    entity1 = GetCustomerSeasonEstimate(seasonId, entity1.CustomerId);

            //    var entity2 = new ObservableCollection<ICustomerSeasonEstimate>();

            //    var entity3 = new ObservableCollection<ICustomerSeasonEstimate>();


            //    //-- Get the list of entities in tier 1
            //    var entity1Records = query;
            //    foreach (var entity1 in entity1Records)
            //    {


            //        entity1.CustomerSeasonEstimateHierarchy = GetCustomerSeasonEstimate(seasonId, entity1.CustomerId);
            //        if (entity1.CustomerSeasonEstimateHierarchy != null)
            //        {
            //            entity1.Estimate += entity1.Estimate;
            //            entity1.Actual += entity1.Actual;
            //            //-- Get Summary info from lower entities
            //            foreach (var entity1Summary in entity1.CustomerSeasonEstimateHierarchy)
            //            {

            //                //-- BEGIN Entity2
            //                entity1Summary.CustomerSeasonEstimateHierarchy = GetCustomerSeasonEstimate(seasonId, entity1.CustomerId);
            //                if (entity2.CustomerSeasonEstimateHierarchy != null)
            //                {
            //                    foreach (var customerSeasonEstimate2 in entity2.CustomerSeasonEstimateHierarchy)
            //                    {
            //                        entity2.Estimate += customerSeasonEstimate2.Estimate;
            //                        entity2.Actual += customerSeasonEstimate2.Actual;

            //                    }
            //                }
            //                returnCollection.Add(entity1Summary);

            //                //-- END Entity2
            //            }
            //        }

            //        returnCollection.Add(entity1);
            //    }
            //    */


            //    return query.ToObservableList<ICustomerSeasonEstimate>();
            //}

            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                //Just get the data we need. We'll group and sort later.
                var query = from c in data.Customers.AsNoTracking()
                            join b in data.Bids on c.CustomerID equals b.CustomerID
                            join o in data.OrderTables on b.BidID equals o.BidID
                            join t in data.Tickets on o.OrderID equals t.OrderId
                            join m in data.Materials on b.MaterialID equals m.MaterialID
                            join s in data.Seasons on c.SeasonId equals s.SeasonId
                            //join e in data.Customers on c.ReportingEntityID equals e.CustomerID 
                            where c.isShippingLocation && c.SeasonId == seasonId && !t.isVoid
                            select new CustomerSeasonEstimate
                            {
                                CustomerId = c.CustomerID,
                                ReportingEntityId = c.ReportingEntityID,
                                EntityName = c.ReportingEntityID.ToString(),
                                SeasonName = s.Prefix,
                                BidId = b.BidID,
                                MaterialId = m.MaterialID,
                                MaterialShortName = m.ShortName,
                                CustomerDispatcherName = c.DispatcherName,
                                Estimate = b.SeasonMaterialEstimateTon ?? 0,
                                Actual = (int)(t.Quantity * 100) / 100
                            };

                var rawData = query.ToList();

                //Group and Sort the data set, and also convert to Interface
                var groupedData = rawData.GroupBy(x => new { x.CustomerId, x.MaterialId })
                    .Select(x => new CustomerSeasonEstimate()
                    {
                        CustomerId = x.Key.CustomerId,
                        ReportingEntityId = x.Max(y => y.ReportingEntityId),
                        EntityName = x.Max(y => y.EntityName),
                        SeasonName = x.Max(y => y.SeasonName),
                        MaterialId = x.Key.MaterialId,
                        BidId = x.Max(y => y.BidId),
                        MaterialShortName = x.Max(y => y.MaterialShortName),
                        CustomerDispatcherName = x.Max(y => y.CustomerDispatcherName),
                        Estimate = x.Max(y => y.Estimate),
                        Actual = x.Sum(y => y.Actual) / 2000
                    } as ICustomerSeasonEstimate).OrderBy(x => x.CustomerDispatcherName).ThenBy(x => x.MaterialShortName);

                return new ObservableCollection<ICustomerSeasonEstimate>(groupedData);
            }
        }

        public ObservableCollection<ICustomerSeasonEstimate> GetCustomerSeasonEstimate(int seasonId, int? reportingId = null)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                //Just get the data we need. We'll group and sort later.
                var query = from c in data.Customers.AsNoTracking()
                            join b in data.Bids on c.CustomerID equals b.CustomerID
                            join o in data.OrderTables on b.BidID equals o.BidID
                            join t in data.Tickets on o.OrderID equals t.OrderId
                            join m in data.Materials on b.MaterialID equals m.MaterialID
                            join s in data.Seasons on c.SeasonId equals s.SeasonId
                            join e in data.Customers on c.ReportingEntityID equals e.CustomerID
                            where c.isShippingLocation
                            && c.SeasonId == seasonId
                            && !t.isVoid
                            //&& c.ReportingEntityID == reportingId
                            select new CustomerSeasonEstimate
                            {
                                CustomerId = c.CustomerID,
                                BidId = b.BidID,
                                MaterialId = m.MaterialID,
                                MaterialShortName = m.ShortName,
                                CustomerDispatcherName = c.DispatcherName,
                                Estimate = b.SeasonMaterialEstimateTon ?? 0,
                                Actual = (int)t.Quantity,
                                ReportingEntityId = c.ReportingEntityID,
                                EntityName = e.ShortName,  //c.ReportingEntityID.ToString(),
                                SeasonName = s.Prefix
                            };

                var rawData = query.ToList();

                //Group and Sort the data set, and also convert to Interface
                var groupedData = rawData.GroupBy(x => new { x.CustomerId, x.MaterialId })
                    .Select(x => new CustomerSeasonEstimate()
                    {
                        CustomerId = x.Key.CustomerId,
                        MaterialId = x.Key.MaterialId,
                        BidId = x.Max(y => y.BidId),
                        MaterialShortName = x.Max(y => y.MaterialShortName),
                        CustomerDispatcherName = x.Max(y => y.CustomerDispatcherName),
                        Estimate = x.Max(y => y.Estimate),
                        Actual = x.Sum(y => y.Actual) / 2000,
                        ReportingEntityId = x.Max(e => e.ReportingEntityId),
                        EntityName = x.Max(e => e.EntityName),
                        SeasonName = x.Max(e => e.SeasonName)
                    } as ICustomerSeasonEstimate).OrderBy(x => x.CustomerDispatcherName).ThenBy(x => x.MaterialShortName);

                return new ObservableCollection<ICustomerSeasonEstimate>(groupedData);
            }
        }

        public void UpdateSeasonEstimate(ICustomerSeasonEstimate seasonEstimate, int seasonId, string connectionString)
        {
            using (var data = new SaltScaleEntities(connectionString))
            {
                if (seasonEstimate == null)
                    throw new ArgumentNullException("seasonEstimate");

                // 2018-Jul-17, JGS - Make seasonID filter explicit
                //Bid bid = data.Bids.First(c => c.BidID == seasonEstimate.BidId);
                Bid bid = data.Bids.First(c => c.BidID == seasonEstimate.BidId && c.SeasonId == seasonId);

                if (bid != null)
                {
                    bid.SeasonMaterialEstimateTon = seasonEstimate.Estimate;
                    bid.UpdateTime = DateTime.Now;
                    data.SaveChanges();
                }
            }
        }


        private ObservableCollection<ICustomerSeasonEstimate> GetHierarchyChildren(int seasonId, int? parentId = 0, ObservableCollection<ICustomerSeasonEstimate> custSeasonEst = null)
        {
            return custSeasonEst.Where(w => w.CustomerId == parentId).Select(c => new CustomerSeasonEstimate
            {
                ReportingEntityId = c.ReportingEntityId,
                CustomerId = c.CustomerId,
                Estimate = c.Estimate,
                CustomerSeasonEstimateHierarchy = GetHierarchyChildren(seasonId, c.ReportingEntityId, custSeasonEst)
            }).ToObservableList<ICustomerSeasonEstimate>();
        }

        public List<IOrderKey> GetCustomerPendingOrders(int customerId, int seasonId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                return data.OrderTables.AsNoTracking()
                    .Where(c => c.IsActive && c.Bid.CustomerID == customerId && c.SeasonId == seasonId && c.OrderNumber.HasValue)
                    .Select(OrderKey.Transform).ToList();
            }
        }

        public List<IPurchaseOrderKey> GetCustomerPendingPurchaseOrders(int customerId, int seasonId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                return data.PurchaseOrders.AsNoTracking()
                    .Where(c => c.IsActive && c.CustomerID == customerId && c.SeasonId == seasonId)
                    .Select(PurchaseOrderKey.Transform).ToList();
            }
        }

        public List<ICustomerLine> GetCustomersForSelector(string filter, int seasonId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var query = data.Customers.AsNoTracking()
                    .Where(c => c.IsActive && c.SeasonId == seasonId && c.isShippingLocation);

                if (!string.IsNullOrWhiteSpace(filter))
                {
                    filter = filter.Trim();
                    query = query.Where(c => c.ShortName.Contains(filter));
                }

                return query.Select(CustomerLine.Transform).OrderBy(o => o.ShortName).ToList();
            }
        }

        public List<IAccountingInfo> GetAccountingCustomerCodes(string customerCode, string connectionString)
        {
            using (var data = new SaltScaleEntities(connectionString))
            {
                return data.GreatPlains.Where(gp => gp.CustomerCode == customerCode.Trim()).Select(AccountingInfo.Transform).OrderBy(ob => ob.AccountingCustomerCode).ToList();
            }
        }

        public List<IGpCustomer> GetGpCustomers(string connectionString, int seasonId, bool allCustomers = true, bool gpCustomers = false, bool nonGpCustomers = false)
        {
            using (var data = new SaltScaleEntities(connectionString))
            {
                var query = from cust in data.Customers
                            from entity in data.Customers.Where(e => e.CustomerID == cust.ReportingEntityID).DefaultIfEmpty()

                            from greatPlains in data.GreatPlains.Where(gp => gp.GreatPlainID == cust.GPID).DefaultIfEmpty()
                            where cust.SeasonId == seasonId
                            select new GPCustomerInfo
                            {
                                //Entity = entity,
                                EntityDispatcherName = entity.DispatcherName,
                                //Customer = cust,
                                CustomerId = cust.CustomerID,
                                CustomerDispatcherName = cust.DispatcherName,
                                AccountingCustomerId = cust.GPID,
                                AccountingCustomerCode = greatPlains.CustomerCode
                                //EntityName = e.DispatcherName,
                                //CustomerId = c.CustomerID,
                                //CustomerShortName = c.ShortName,
                                //CustomerName = c.DispatcherName,
                                //AccountingCustomerCode = _entityCustomer.Select(ec => ec.CustomerCode).FirstOrDefault()
                            };

                /*
                var query = data.GPCustomersViews.AsNoTracking().Where(c => c.SeasonId == seasonId);
                */
                if (gpCustomers)
                {
                    query = query.Where(c => !string.IsNullOrWhiteSpace(c.AccountingCustomerCode));
                }
                else if (nonGpCustomers)
                {
                    query = query.Where(c => string.IsNullOrWhiteSpace(c.AccountingCustomerCode));
                }

                //return query.OrderBy(c => c.Entity.DispatcherName).ThenBy(c => c.Customer.DispatcherName).Select(GpCustomer.Transform).ToList();
                return query.OrderBy(c => c.EntityDispatcherName).ThenBy(c => c.CustomerDispatcherName).Select(GpCustomer.Transform).ToList();

            }
        }

        public int AddNewAccountingInfo(string newAccountingCustomerCode, string connectionString)
        {
            using (var data = new SaltScaleEntities(connectionString))
            {
                GreatPlain greatPlain;

                greatPlain = data.GreatPlains.Create();
                greatPlain.CustomerCode = newAccountingCustomerCode;
                data.GreatPlains.Add(greatPlain);
                data.SaveChanges();

                return greatPlain.GreatPlainID;
            }
        }

        public void UpdateGpId(IGpCustomer gpCustomer, int seasonId, string connectionString)
        {
            using (var data = new SaltScaleEntities(connectionString))
            {
                // 2018-Jul-17, Add in explicit use of SeasonID
                //var customer = data.Customers.FirstOrDefault(c => c.GPID == gpCustomer.AccountingCustomerId );
                var customer = data.Customers.FirstOrDefault(c => c.GPID == gpCustomer.AccountingCustomerId && c.SeasonId == seasonId);

                if (customer != null)
                {
                    customer.GPID = gpCustomer.AccountingCustomerId;
                    data.SaveChanges();
                }
            }
        }

        public void UpdateGpId(int customerId, int greatPlainId, int seasonId, string connectionString)
        {
            using (var data = new SaltScaleEntities(connectionString))
            {
                // 2018-Jul-17, Add in explicit use of SeasonID
                //var customer = data.Customers.FirstOrDefault(c => c.CustomerID == customerId);
                var customer = data.Customers.FirstOrDefault(c => c.CustomerID == customerId && c.SeasonId == seasonId);

                if (customer != null)
                {
                    if (customer.GPID != greatPlainId)
                    {
                        customer.GPID = greatPlainId;
                        data.SaveChanges();
                    }
                }
            }
        }

        public List<ICustomerLine> GetFilteredCustomers(string filter, bool shippingCustomers, bool entityCustomers, int seasonId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var customerService = (from cust in data.Customers.AsNoTracking()
                                       //join b in data.Bids.AsNoTracking() on cust.CustomerID equals b.CustomerID into _customerBids
                                       //from b in _customerBids.DefaultIfEmpty()
                                       where cust.IsActive
                                              && cust.SeasonId == seasonId
                                              && ((cust.ShortName.Contains(filter)) || (cust.DispatcherName.Contains(filter)))
                                              && (shippingCustomers == false && entityCustomers == false
                                                  || (shippingCustomers && cust.isShippingLocation == shippingCustomers
                                                  || entityCustomers && cust.isShippingLocation != entityCustomers))

                                       select new CustomerLine
                                       {
                                           Id = cust.CustomerID,
                                           Name = cust.DispatcherName,
                                           ShortName = cust.ShortName,
                                           CashMaterialRate = cust.CashMaterialRate,
                                           //TruckingRate = b.TruckingRate,
                                           TruckingRate = 0,
                                           //Toll = b.Toll,
                                           Toll = 0,
                                           EntityName = data.Customers.FirstOrDefault(r => r.CustomerID == cust.ReportingEntityID).DispatcherName,
                                           SeasonName = data.Seasons.FirstOrDefault(r => r.SeasonId == cust.SeasonId).Prefix,
                                           AddressLine1 = data.CustomerContacts.FirstOrDefault(g => g.CustomerID == cust.CustomerID).Contact.Address.AddressLineOne,
                                           City = data.CustomerContacts.FirstOrDefault(g => g.CustomerID == cust.CustomerID).Contact.Address.City,
                                           State = data.CustomerContacts.FirstOrDefault(g => g.CustomerID == cust.CustomerID).Contact.Address.State,
                                           Company = data.Companies.Where(x => x.CompanyId == cust.CompanyId).FirstOrDefault()
                                       })
                                       .Select(CustomerLine.TransformLine);


                return customerService.OrderBy(x => x.ShortName).ThenBy(x => x.Name).ToList();

            }

            /*
            //-- Prior to multiple materials.  The inner join to Bids causes additional customer lines to show on the grid
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var customerService = data.Customers.AsNoTracking()
                    .Where(c => c.IsActive && c.SeasonId == seasonId 
                        && ((c.ShortName.Contains(filter)) || (c.DispatcherName.Contains(filter)))
                        && (shippingCustomers == false && entityCustomers == false 
                                || shippingCustomers && c.isShippingLocation == shippingCustomers
                                || entityCustomers && c.isShippingLocation != entityCustomers)
                        )
                    .Join(data.Bids,
                    c => c.CustomerID, b => b.CustomerID,
                    (c, b) => new CustomerLine
                    {
                        Id = c.CustomerID,
                        Name = c.DispatcherName,
                        ShortName = c.ShortName,
                        CashMaterialRate = c.CashMaterialRate,
                        TruckingRate = b.TruckingRate,
                        Toll = b.Toll,
                        EntityName = data.Customers.FirstOrDefault(r => r.CustomerID == c.ReportingEntityID).DispatcherName,
                        AddressLine1 = data.CustomerContacts.FirstOrDefault(g => g.CustomerID == c.CustomerID).Contact.Address.AddressLineOne,
                        City = data.CustomerContacts.FirstOrDefault(g => g.CustomerID == c.CustomerID).Contact.Address.City,
                        State = data.CustomerContacts.FirstOrDefault(g => g.CustomerID == c.CustomerID).Contact.Address.State,
                        CompanyName = b.Company.FullName

            }).Select(CustomerLine.TransformLine).ToList();
                return customerService.OrderBy(x => x.ShortName).ThenBy(x => x.Name).ToList();
            }
            */
        }

        public bool DeleteCustomer(int customerId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var customer = data.Customers.Find(customerId);
                var contacts = data.Contacts.Where(con => con.CustomerContacts.Any(cc => cc.CustomerID == customerId));
                if (customer == null)
                {
                    return false;
                }

                //Deactivate the Bids
                customer.Bids.Where(x => x.CustomerID == customerId)
                                   .ToList()
                                   .ForEach(x => { x.IsActive = false;
                                                   x.UpdateTime = DateTime.Now;
                                   });
                //Deactivate the Customer Contacts
                contacts.ToList().ForEach(x => { x.IsActive = false;
                                                 x.UpdateTime = DateTime.Now;
                                   });
                //Deactivate the Purchase Orders
                customer.PurchaseOrders.Where(x => x.CustomerID == customerId)
                                  .ToList()
                                  .ForEach(x =>
                                  {
                                      x.IsActive = false;
                                      x.UpdateTime = DateTime.Now;
                                  });
                //Deactivate the Customer
                customer.IsActive = false;
                customer.UpdateTime = DateTime.Now;
                data.SaveChanges();
                return true;
            }
        }

        public ICustomerAddress GetNewCustomer(int seasonId, string selectedSiteName)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var customer = data.Customers.Create();
                customer.SeasonId = seasonId;
                customer.IsActive = false;
                customer.ShortName = "";
                customer.CreateTime = DateTime.Now;
                customer.UpdateTime = DateTime.Now;
                customer.isShippingLocation = true;
                customer.HideDollarAmounts = false;
                customer.IsTaxExempt = false;

                data.Customers.Add(customer);
                data.SaveChanges();

                var customerAddress = new CustomerAddress(customer);

                // Company
                var company = data.Companies.FirstOrDefault();
                if (company == null)
                    throw new EntityNotFoundException("There are no companies defined in database.", 
                        typeof (Company), "default company");

                //customerAddress.Bid.Company = new CommonService.CompanyInfo(company);

                // Default Material
                var material = data.Materials.AsNoTracking()
                    .Where(m => m.IsActive && m.SeasonId == seasonId)
                    .Select(MaterialInfo.Transform)
                    .FirstOrDefault();
                if (material == null)
                    throw new EntityNotFoundException("Material, defined in materials, not found in database.",
                        typeof (Material), "default material");
                customerAddress.Bid.Material = material;

                // Site
                var site = !string.IsNullOrWhiteSpace(selectedSiteName)
                    ? data.Sites.FirstOrDefault(s => s.SeasonId == seasonId && s.SiteName == selectedSiteName)
                    : data.Sites.FirstOrDefault(s => s.SeasonId == seasonId);
                if (site == null)
                    throw new EntityNotFoundException("Site, defined in configuration, not found in database.",
                        typeof (Site), "default location");
                customerAddress.Bid.Site = new SiteInfo(site);

                return customerAddress;
            }
        }

        public void SaveCustomer(ICustomerAddress customer, int seasonId)
        {
            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                using (var data = new SaltScaleEntities(_configuration.ConnectionString))
                {
                    // 2018-JUl-17, JGS - Make explicit use of the SeasonID
                    //var customerInfo = data.Customers.FirstOrDefault(o => o.CustomerID == customer.Id);
                    var customerInfo = data.Customers.FirstOrDefault(o => o.CustomerID == customer.Id && o.SeasonId == seasonId);
                    if (customerInfo == null)
                    {
                        //TODO logging
                        throw new Exception("Impossible to save customer.");
                    }
                
                    if (String.CompareOrdinal(customerInfo.ShortName, customer.ShortName) != 0
                        && data.Customers.Any(c => c.ShortName.Equals(customer.ShortName))
                        )
                        throw new UniqueConstraintException(String.Format("You already have a customer called {0}.", customer.ShortName));


                    customerInfo.CompanyId = customer.Company.Id;
                    customerInfo.ShortName = customer.ShortName;
                    customerInfo.DispatcherName = customer.Name;
                    customerInfo.isShippingLocation = customer.IsShippingLocation;
                    customerInfo.specialInstructions = customer.SpecialInstruction;
                    customerInfo.UpdateTime = DateTime.Now;
                    customerInfo.Comment = customer.Comment;
                    customerInfo.IsActive = true;
                    customerInfo.ReportingEntityID = customer.ReportingEntityId;
                    customerInfo.HideDollarAmounts = customer.HideDollarAmounts;
                    customerInfo.IsTaxExempt = customer.IsTaxExempt;
                    customerInfo.CashMaterialRate = customer.CashMaterialRate;
                    customerInfo.SeasonId = seasonId;
                    data.SaveChanges();

                    var customerId = customerInfo.CustomerID;
                    var contactManager = new ContactManager(data);
                    var contactId = contactManager.SaveContact(customer.Contact, customerInfo.SeasonId);
                    contactManager.InsertCustomerContact(contactId, customerId);

                    scope.Complete();
                }
            }
        }

        public List<int> GetReportingBranches(int customerId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var reportingBranches = new List<int>();
                var reportingBranchesForCustomer = GetReportingBranches(customerId, data);

                foreach (var reportingBranch in reportingBranchesForCustomer)
                {
                    reportingBranches.Add(reportingBranch);
                    reportingBranches.AddRange(GetReportingBranches(reportingBranch, data));
                }

                return reportingBranches;
            }
        }

        private static IEnumerable<int> GetReportingBranches(int customerId, SaltScaleEntities data)
        {
            return data.Customers.AsNoTracking()
                .Where(customer => customer.IsActive
                                   && !customer.isShippingLocation
                                   && customer.ReportingEntityID == customerId)
                .OrderBy(customer => customer.DispatcherName)
                .Select(customer => customer.CustomerID);
        }

        /// <summary>
        /// Call this method if the Customer is not shipping location, if not, use the same Customer as the unique shipping location
        /// </summary>
        /// <param name="season"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public List<int> GetShippingLocations(int season, int customerId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var reportingBranches = GetReportingBranches(customerId);
                return data.Customers.AsNoTracking()
                        .Where(customer => customer.IsActive
                                            && customer.SeasonId == season  // 2018-JUl-17, JGS - Make explicit use of the SeasonID
                                            && customer.isShippingLocation
                                            && reportingBranches.Any(rb => rb == customer.ReportingEntityID))
                        .OrderBy(customer => customer.DispatcherName)
                        .Select(c=>c.CustomerID).ToList();
            }
        }
        
        public List<IConfirmationNumberKey> GetCustomerConfirmationNumbers(int customerId, int seasonId)
        {
            throw new NotImplementedException();
        }

    }

    #region DTO

    internal class CustomerAddress : ICustomerAddress
    {
        public static readonly Expression<Func<Customer, ICustomerAddress>> Transform = e => new CustomerAddress
        {
            Id = e.CustomerID,
            Name = e.DispatcherName,
            ShortName = e.ShortName,
            SpecialInstruction = e.specialInstructions,
            IsShippingLocation = e.isShippingLocation,
            PrimaryContact = e.CustomerContacts
                .Where(cc => cc.Contact.IsActive)
                .OrderByDescending(cc => cc.CustomerID)
                .Take(1)
                .Select(cc => cc.Contact)
                .FirstOrDefault(),
            PrimaryAddress = e.CustomerContacts
                .Where(cc => cc.Contact.IsActive)
                .OrderByDescending(cc => cc.CustomerID)
                .Take(1)
                .Select(cc => cc.Contact.Address)
                .FirstOrDefault(),
            Comment = e.Comment,
            ReportingEntityId = e.ReportingEntityID,
            UpdateTime = e.UpdateTime,
            CreateTime = e.CreateTime,
            DataC=e.DataC,
            HideDollarAmounts = e.HideDollarAmounts,
            IsTaxExempt = e.IsTaxExempt,
            CashMaterialRate = e.CashMaterialRate,
            Company = null,
        };

        public string DataC { get; set; }

        public CustomerAddress()
        {
            
        }

        public CustomerAddress(Customer customer)
        {
            Id = customer.CustomerID;
            Name = customer.DispatcherName;
            ShortName = customer.ShortName;
            SpecialInstruction = customer.specialInstructions;
            IsShippingLocation = customer.isShippingLocation;
            PrimaryContact = customer.CustomerContacts.Where(cc => cc.Contact.IsActive)
                .Select(cc => cc.Contact).FirstOrDefault();
            PrimaryAddress = PrimaryContact == null ? null : PrimaryContact.Address;
            PrimaryPhoneNumber = PrimaryContact == null
                ? null
                : PrimaryContact.PhoneNumbers.OrderByDescending(p => p.PhoneNumberID).FirstOrDefault(p => p.IsActive && p.isPrimary);
            Comment = customer.Comment;
            Contact = new ContactData();
            CreateTime = customer.CreateTime;
            UpdateTime = customer.UpdateTime;
            if (Bid == null) Bid = new BidData();
            Bid.CustomerId = Id;
            DataC = customer.DataC;
            HideDollarAmounts = customer.HideDollarAmounts;
            IsTaxExempt = customer.IsTaxExempt;
            CashMaterialRate = customer.CashMaterialRate;
            ReportingEntityId = customer.ReportingEntityID;
            Company = new CommonService.CompanyInfo(customer?.Company);
        }

        public string Comment { get; set; }

        public bool IsShippingLocation { get; set; }

        public Address PrimaryAddress { get; set; }

        public Contact PrimaryContact { get; set; }

        public PhoneNumber PrimaryPhoneNumber { get; set; }

        public string Address
        {
            get
            {
                if (PrimaryAddress == null)
                    return null;

                var address = string.Empty;

                if (!string.IsNullOrWhiteSpace(PrimaryAddress.AddressLineOne))
                    address = PrimaryAddress.AddressLineOne.Trim() + " ";

                if (!string.IsNullOrWhiteSpace(PrimaryAddress.AddressLineTwo))
                    address = address + PrimaryAddress.AddressLineTwo.Trim();

                return address.Trim();
            }
        }

        public string FullAddress
        {
            get
            {
                if (PrimaryAddress == null)
                    return null;

                var address = string.Empty;

                if (!string.IsNullOrWhiteSpace(PrimaryAddress.AddressLineOne))
                    address = PrimaryAddress.AddressLineOne.Trim() + Environment.NewLine;

                if (!string.IsNullOrWhiteSpace(PrimaryAddress.AddressLineTwo))
                    address = address + PrimaryAddress.AddressLineTwo.Trim() + Environment.NewLine;

                string[] addressInfo =
                {
                    PrimaryAddress.City == null ? String.Empty : PrimaryAddress.City.Trim(), 
                    PrimaryAddress.State == null ? String.Empty : PrimaryAddress.State.Trim(),
                    PrimaryAddress.Zip == null ? String.Empty : PrimaryAddress.Zip.Trim()
                };

                address = address + string.Join(", ", addressInfo.Where(s => !string.IsNullOrWhiteSpace(s)));
                return address.Trim();
            }
        }

        public string SpecialInstruction { get; set; }

        public string ShortName { get; set; }

        public string Name { get; set; }

        public int Id { get; set; }

        public string PrimaryContactOrganization
        {
            get { return PrimaryContact == null ? string.Empty : PrimaryContact.Organization; }
        }

        public string PrimaryContactInfo
        {
            get
            {
                if (PrimaryContact == null)
                {
                    return null;
                }

                var primaryContactInfo = PrimaryContact.Name;
                if (PrimaryPhoneNumber == null)
                {
                    return primaryContactInfo;
                }
                    
                primaryContactInfo += String.Format("{0}{1}", Environment.NewLine, PrimaryPhoneNumber.PhoneNumber1);

                return primaryContactInfo;
            }
        }

        public int? ReportingEntityId { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime? UpdateTime { get; set; }

        public int SeasonId { get; set; }

        public bool HideDollarAmounts { get; set; }

        public decimal? CashMaterialRate { get; set; }

        public bool IsTaxExempt { get; set; }

        public IBid Bid { get; set; }

        public IContactData Contact { get; set; }
        public ICompanyInfo Company { get; set; }
    }

    internal class CustomerLine : ICustomerLine
    {
        public static readonly Expression<Func<Customer, ICustomerLine>> Transform = e => new CustomerLine
        {
            Id = e.CustomerID,
            Name = e.DispatcherName,
            ShortName = e.ShortName,
            PrimaryAddress = e.CustomerContacts
                .Where(cc => cc.Contact.IsActive)
                .OrderByDescending(cc => cc.CustomerID)
                .Take(1)
                .Select(cc => cc.Contact.Address)
                .FirstOrDefault(),
            //CompanyName = e.Bids.Where(b => b.CustomerID == e.CustomerID).Select(bb => bb.Company.FullName).FirstOrDefault()
            Company = e.Company
        };

        public static readonly Expression<Func<CustomerLine, ICustomerLine>> TransformLine = e => new CustomerLine
        {
            Id = e.Id,
            Name = e.Name,
            ShortName = e.ShortName,
            CashMaterialRate = e.CashMaterialRate,
            TruckingRate = e.TruckingRate,
            Toll = e.Toll,
            EntityName = e.EntityName,
            SeasonName = e.SeasonName,
            AddressLine1 = e.AddressLine1,
            City = e.City,
            State = e.State,
            Company = e.Company
        };

        public CustomerLine()
        {

        }

        public Address PrimaryAddress { get; set; }

        public string Address
        {
            get
            {
                if (PrimaryAddress == null)
                    return null;

                var address = string.Empty;

                if (!string.IsNullOrWhiteSpace(PrimaryAddress.AddressLineOne))
                    address = PrimaryAddress.AddressLineOne.Trim();

                if (!string.IsNullOrWhiteSpace(PrimaryAddress.AddressLineTwo))
                    address = address + " " + PrimaryAddress.AddressLineTwo.Trim();

                return address.Trim();
            }
        }

        public string ShortName { get; set; }

        public string Name { get; set; }

        public int Id { get; set; }

        public int? ReportingEntityId { get; set; }

        public string EntityName { get; set; }

        public int SeasonId { get; set; }

        public string SeasonName { get; set; }

        public decimal? CashMaterialRate { get; set; }

        public decimal? TruckingRate { get; set; }

        public decimal? Toll { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public Company Company { get; set; }
        ICompanyInfo ICustomerLine.Company
        {
            get
            {
                return new CommonService.CompanyInfo(Company);
            }
        }
    }

    internal class CustomerInfo : ICustomerInfo
    {
        public CustomerInfo()
        {
            
        }

        public CustomerInfo(Customer customer)
        {
            Id = customer.CustomerID;
            Name = customer.DispatcherName;
            ShortName = customer.ShortName;
            SpecialInstruction = customer.specialInstructions;
            Comment = customer.Comment;

            var contact =
                customer.CustomerContacts.Select(c => c.Contact).Where(c => c.IsActive).OrderByDescending(
                    c => c.ContactID)
                    .FirstOrDefault();
            if (contact != null)
            {
                OrganizationName = contact.Organization;

                if (contact.Address != null)
                {
                    var address = string.Empty;

                    if (!string.IsNullOrWhiteSpace(contact.Address.AddressLineOne))
                        address = contact.Address.AddressLineOne.Trim();

                    if (!string.IsNullOrWhiteSpace(contact.Address.AddressLineTwo))
                        address = address + " " + contact.Address.AddressLineTwo.Trim();
                    Address = address.Trim();

                    string[] addressInfo =
                    {
                        contact.Address.City == null ? String.Empty : contact.Address.City.Trim(),
                        contact.Address.State == null ? String.Empty : contact.Address.State.Trim(),
                        contact.Address.Zip == null ? String.Empty : contact.Address.Zip.Trim()
                    };

                    address = address + string.Join(", ", addressInfo.Where(s => !string.IsNullOrWhiteSpace(s)));
                    FullAddress = address.Trim();
                }

                ContactInfo = contact.Name;

                var phone =
                    contact.PhoneNumbers.Where(p => p.isPrimary && p.isPrimary && !string.IsNullOrEmpty(p.PhoneNumber1))
                        .OrderByDescending(p => p.PhoneNumberID).FirstOrDefault();
                if (phone != null)
                {
                    Phone = phone.PhoneNumber1.Trim();
                    ContactInfo += String.Format("{0}Phone: {1}", Environment.NewLine, Phone);
                }

                Company = customer.Company;
            }
        }

        public string Comment { get; set; }

        public string Address { get; set; }
        
        public string Phone { get; set; }

        public string FullAddress { get; set; }

        public string SpecialInstruction { get; set; }

        public string ShortName { get; set; }

        public string Name { get; set; }

        public int Id { get; set; }

        public string OrganizationName { get; set; }

        public string ContactInfo { get; set; }
        public Company Company { get; set; }
        ICompanyInfo ICustomerInfo.Company { get; set; }
        //public ICompanyInfo Company { get; set; }
    }

    internal class OrderKey : IOrderKey
    {
        public static readonly Expression<Func<OrderTable, IOrderKey>> Transform = e => new OrderKey
        {
            Id = e.OrderID,
            OrderNumber = e.OrderNumber.Value,
        };

        public int OrderNumber { get; set; }

        public int Id { get; set; }
    }

    internal class PurchaseOrderKey : IPurchaseOrderKey
    {
        public static readonly Expression<Func<PurchaseOrder, IPurchaseOrderKey>> Transform = e => new PurchaseOrderKey
        {
            Id = e.PurchaseOrderID,
            PurchaseOrderNumber = e.PurchaseOrderNumber,
        };

        public string PurchaseOrderNumber { get; set; }

        public int Id { get; set; }
    }

    internal class AccountingInfo : IAccountingInfo
    {
        public static readonly Expression<Func<GreatPlain, IAccountingInfo>> Transform = e => new AccountingInfo
        {
            AccountingCustomerId = e.GreatPlainID,
            AccountingCustomerCode = e.CustomerCode,
        };

        public int AccountingCustomerId { get; set; }
        public string AccountingCustomerCode { get; set; }
    }

    internal class GPCustomerInfo : IGpCustomer
    {
        public GPCustomerInfo()
        { }

        public string EntityDispatcherName { get; set; }
        public int CustomerId { get; set; }
        public string CustomerDispatcherName { get; set; }
        public int AccountingCustomerId { get; set; }
        public string AccountingCustomerCode { get; set; }

    }

    internal class GpCustomer : IGpCustomer
    {

        public static readonly Expression<Func<GPCustomerInfo, IGpCustomer>> Transform = e => new GpCustomer
        {
            //Customer = e.Customer,
            CustomerDispatcherName = e.CustomerDispatcherName,
            CustomerId = e.CustomerId,
            //Entity = e.Entity,
            EntityDispatcherName = e.EntityDispatcherName,
            AccountingCustomerCode = e.AccountingCustomerCode
        };

        public string EntityDispatcherName { get; set; }
        public int CustomerId { get; set; }
        public string CustomerDispatcherName { get; set; }
        public int AccountingCustomerId { get; set; }
        public string AccountingCustomerCode { get; set; }
    }

    
    internal class EntitySeasonEstimate : NotificationObject, IEntitySeasonEstimate
    {
        public static readonly Expression<Func<EntitySeasonEstimate, IEntitySeasonEstimate>> Transform = e => new EntitySeasonEstimate
        {
            CustomerId = e.CustomerId,
            ReportingEntityId = e.ReportingEntityId,
            EntityDispatcherName = e.EntityDispatcherName,
            EntityShortName = e.EntityShortName,
            EntitySeasonMaterialEstimateTon = e.EntitySeasonMaterialEstimateTon,
            EntitySeasonMaterialActualTon = e.EntitySeasonMaterialActualTon,
            CustomerSeasonEstimateCollection = e.CustomerSeasonEstimateCollection,
        };

        public EntitySeasonEstimate()
        {

        }

        public int CustomerId { get; set; }
        public int? ReportingEntityId { get; set; }
        public string EntityDispatcherName { get; set; }
        public string EntityShortName { get; set; }
        public decimal EntitySeasonMaterialEstimateTon { get; set; }
        public decimal EntitySeasonMaterialActualTon { get; set; }
        public ObservableCollection<ICustomerSeasonEstimate> CustomerSeasonEstimateCollection { get; set; }
    }
    

    internal class CustomerSeasonEstimate : NotificationObject, IEditableObject, ICustomerSeasonEstimate
    {
        public static readonly Expression<Func<CustomerSeasonEstimate, ICustomerSeasonEstimate>> Transform = e => new CustomerSeasonEstimate
        {
            CustomerId = e.CustomerId,
            BidId = e.BidId,
            MaterialId = e.MaterialId,
            MaterialShortName = e.MaterialShortName,
            CustomerDispatcherName = e.CustomerDispatcherName,
            Estimate = e.Estimate,
            Actual = e.Actual,
            CustomerSeasonEstimateHierarchy = e.CustomerSeasonEstimateHierarchy,
            EntityName = e.EntityName,
            SeasonName = e.SeasonName,
        };

        public CustomerSeasonEstimate()
        {

        }

        private decimal _estimate;

        public ICustomerSeasonEstimate TempValues { get; set; }

        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        public ObservableCollection<ICustomerSeasonEstimate> CustomerSeasonEstimateHierarchy { get; set; }

        public int CustomerId { get; set; }
        public int? ReportingEntityId { get; set; }
        public string EntityName { get; set; }
        public int SeasonId { get; set; }
        public string SeasonName { get; set; }

        public bool IsEntity { get; set; }
        public string CustomerDispatcherName { get; set; }
        public string CustomerShortName { get; set; }

        public int BidId { get; set; }

        public int MaterialId { get; set; }

        public string MaterialShortName { get; set; }

        public decimal Estimate
        {
            get
            {
                return _estimate;
            }
            set
            {
                if (Equals(value, _estimate)) return;

                _estimate = value;
            }
        }

        public decimal Actual { get; set; }



        // 2018-05-29 - JGS
        public string EntityDispatcherName { get; set; }
        public string EntityShortName { get; set; }
        public string EstimateSeasonId { get; set; }
        // 2018-05-29 - JGS



        public decimal? Difference {
            get
            {
                return Estimate - (decimal?)Actual;
            }
        }

        public decimal? Percentage
        {
            get
            {
                if (Estimate == 0)   // || Actual == null)
                    return null;

                //Calculate the percentage and round
                decimal p = (((decimal?)Actual / Estimate) * 100).GetValueOrDefault();
                return Math.Round(p, 0);
            }
        }

        public void BeginEdit()
        {
            TempValues = new CustomerSeasonEstimate()
            {
                ReportingEntityId = ReportingEntityId,
                CustomerId = CustomerId,
                BidId = BidId,
                MaterialId = MaterialId,
                MaterialShortName = MaterialShortName,
                CustomerDispatcherName = CustomerDispatcherName,
                Estimate = Estimate,
                Actual = Actual
            };
        }

        public void CancelEdit()
        {
            if (TempValues != null)
            {
                ReportingEntityId = TempValues.ReportingEntityId;
                CustomerId = TempValues.CustomerId;
                BidId = TempValues.BidId;
                MaterialId = TempValues.MaterialId;
                MaterialShortName = TempValues.MaterialShortName;
                CustomerDispatcherName = TempValues.CustomerDispatcherName;
                Estimate = TempValues.Estimate;
                Actual = TempValues.Actual;
            }
        }


        public void EndEdit()
        {
            if (TempValues != null)
            {
                TempValues = null;
            }

            PropertyChanged(this, new PropertyChangedEventArgs("Estimate"));
            PropertyChanged(this, new PropertyChangedEventArgs("Difference"));
            PropertyChanged(this, new PropertyChangedEventArgs("Percentage"));
        }
    }
    #endregion
}