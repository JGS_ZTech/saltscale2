﻿using System.Collections.Generic;
using Contract.Domain;
using Contract.Services;
using DataAccess;

namespace BusinessLogic
{
    public class ContactService : IContactService
    {
        private readonly IConfiguration _configuration;

        public ContactService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public IContactData GetContactData(int contactId, int seasonId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                return new ContactManager(data).GetContactData(contactId, seasonId);
            }
        }

        public int SaveContact(IContactData contactData, int seasonId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                return new ContactManager(data).SaveContact(contactData, seasonId);
            }
        }

        public bool CanDeleteContact(int contactId, int seasonId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                return new ContactManager(data).CanDeleteContact(contactId, seasonId);
            }
        }

        public void DeleteContact(int contactId, int seasonId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                new ContactManager(data).DeleteContact(contactId, seasonId);
            }
        }

        public IContactData GetNewContact(int seasonId)
        {
            return new ContactData {SeasonId = seasonId};
        }

        public IReadOnlyList<IContactInfo> GetFilteredContacts(string filter, int seasonId, bool hideCustomers, bool hideTruckOwners, bool hideOthers)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                return new ContactManager(data).GetFilteredContacts(filter, hideCustomers,hideTruckOwners, hideOthers, seasonId);
            }
        }
    }
}