﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Contract.Domain;
using Contract.Exception;
using Contract.Services;
using DataAccess;

namespace BusinessLogic
{
    public class PurchaseOrderService : IPurchaseOrderService
    {
        private readonly IConfiguration _configuration;

        public PurchaseOrderService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public IPurchaseOrder GetPurchaseOrder(int id)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                return data.PurchaseOrders.AsNoTracking().Where(po => po.IsActive && po.PurchaseOrderID == id)
                    .Select(PurchaseOrderData.Transform).FirstOrDefault();
            }
        }

        public IPurchaseOrder GetPurchaseOrder(string PONumber, int CustomerId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                // 2018-Jul-22, JGS - Get the currently active season
                var currentSeason = data.Seasons.FirstOrDefault(s => s.IsActive == true);

                // 2018-Jul-22, JGS - Return one or more records matching the PO Number for the current season
                return data.PurchaseOrders.AsNoTracking().Where(po => po.PurchaseOrderNumber == PONumber && po.CustomerID == CustomerId && po.SeasonId == currentSeason.SeasonId)
                    .Select(PurchaseOrderData.Transform).FirstOrDefault();
            }
        }

        // 20180624, JGS - Added "OrderByDescending(po => po.PurchaseOrderNumber)" to display from highest to lowest (reverse order)
        public List<IPurchaseOrder> GetPurchaseOrderList(int customerId, int seasonId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                return data.PurchaseOrders.AsNoTracking()
                    .Where(po => po.IsActive 
                            && po.SeasonId == seasonId 
                            && po.CustomerID == customerId)
                    .Select(PurchaseOrderData.Transform).OrderByDescending(po => po.PurchaseOrderNumber).ToList();
            }
        }

        // 20180624, JGS - Added "OrderByDescending(po => po.PurchaseOrderNumber)"
        public List<IPurchaseOrder> GetFilteredPurchaseOrders(string filter, int customerId, int seasonId)
        {
            DateTime filterDateTime;
            DateTime.TryParse(filter, out filterDateTime);
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                return data.PurchaseOrders.AsNoTracking()
                    .Where(po => po.IsActive && po.SeasonId == seasonId && po.CustomerID == customerId
                        && (po.PurchaseOrderNumber.Contains(filter)
                            || (filterDateTime.TimeOfDay.TotalMinutes == 0 ?
                                DbFunctions.DiffDays(po.effectiveDate, filterDateTime) == 0 :
                                DbFunctions.DiffMinutes(po.effectiveDate, filterDateTime) == 0)))
                    .Select(PurchaseOrderData.Transform).OrderByDescending(po => po.PurchaseOrderNumber).ToList();
            }
        }

        // 20180627, JGS - Added Filtered overload
        public List<IPurchaseOrder> GetFilteredPurchaseOrders(string filter, int customerId, int seasonId, string status)
        {
            DateTime filterDateTime;
            DateTime.TryParse(filter, out filterDateTime);
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                if (status == "open")
                {
                    return data.PurchaseOrders.AsNoTracking()
                        .Where(po => 
                                po.IsActive
                                && !po.isComplete
                                && po.SeasonId == seasonId
                                && po.CustomerID == customerId)
                        .Select(PurchaseOrderData.Transform).OrderByDescending(po => po.PurchaseOrderNumber).ToList();
                }
                else if (status == "completed")
                { 
                    // Return Closed
                    return data.PurchaseOrders.AsNoTracking()
                        .Where(po => 
                                po.isComplete
                                && po.SeasonId == seasonId
                                && po.CustomerID == customerId)
                        .Select(PurchaseOrderData.Transform).OrderByDescending(po => po.PurchaseOrderNumber).ToList();
                }
                else
                {
                    // Return all
                    return data.PurchaseOrders.AsNoTracking()
                        .Where(po => 
                                po.SeasonId == seasonId
                                && po.CustomerID == customerId)
                        .Select(PurchaseOrderData.Transform).OrderByDescending(po => po.PurchaseOrderNumber).ToList();
                }
            }
        }

        public IPurchaseOrder GetNewPurchaseOrder(int seasonId, int customerId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                // 2018-JUl-17, JGS - Make explicit use of the SeasonID
                //var bidInfo = data.Bids.FirstOrDefault(b => b.IsActive && b.CustomerID == customerId);
                var bidInfo = data.Bids.FirstOrDefault(b => b.IsActive && b.CustomerID == customerId && b.SeasonId  == seasonId);
                var materialInfo = bidInfo != null && bidInfo.Material != null
                    ? bidInfo.Material
                    : data.Materials.FirstOrDefault(m => m.IsActive && m.SeasonId == seasonId);

                if(materialInfo == null)
                    throw new EntityNotFoundException("Material, defined in materials, not found in database.", typeof(Material), "default material");

                var purchaseOrder = new PurchaseOrder
                {
                    SeasonId = seasonId,
                    effectiveDate = DateTime.Now,
                    isComplete = false,
                    isHeld = false,
                    Quantity = 0,
                    CreateTime = DateTime.Now,
                    UpdateTime = DateTime.Now,
                    Material = materialInfo,
                    MaterialID = materialInfo.MaterialID
                };
                
                return new PurchaseOrderData(purchaseOrder);
            }
        }

        public void SavePurchaseOrder(IPurchaseOrder purchaseOrder, int customerId)
        {
            var insert = false;
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var purchaseOrderInfo = data.PurchaseOrders.FirstOrDefault(b => b.PurchaseOrderID == purchaseOrder.Id);
                if (purchaseOrderInfo == null)
                {
                    purchaseOrderInfo = data.PurchaseOrders.Create();
                    purchaseOrderInfo.CreateTime = DateTime.Now;
                    insert = true;
                }

                if (String.CompareOrdinal(purchaseOrderInfo.PurchaseOrderNumber, purchaseOrder.PurchaseOrderNumber) != 0 && data.PurchaseOrders.Any(c => c.PurchaseOrderNumber.Equals(purchaseOrder.PurchaseOrderNumber) && c.CustomerID == customerId))
                    throw new UniqueConstraintException(String.Format("You already have a Purchase Order Number called {0}.", purchaseOrder.PurchaseOrderNumber));

                var materialInfo = data.Materials.FirstOrDefault(m => m.MaterialID == purchaseOrder.MaterialId);
                if (materialInfo == null)
                    throw new Exception("Impossible to save material.");

                purchaseOrderInfo.CustomerID = customerId;
                purchaseOrderInfo.PurchaseOrderNumber = purchaseOrder.PurchaseOrderNumber;
                purchaseOrderInfo.Quantity = purchaseOrder.QuantityOfPO.HasValue ? purchaseOrder.QuantityOfPO.Value : 0;
                purchaseOrderInfo.Material = materialInfo;
                purchaseOrderInfo.MaterialID = purchaseOrder.MaterialId;
                purchaseOrderInfo.isComplete = purchaseOrder.IsComplete;
                purchaseOrderInfo.isHeld = purchaseOrder.IsHeld;
                purchaseOrderInfo.effectiveDate =purchaseOrder.EffectiveDate.HasValue ? purchaseOrder.EffectiveDate.Value : DateTime.Now;
                purchaseOrderInfo.Comment = purchaseOrder.Comment;
                purchaseOrderInfo.DataA = purchaseOrder.DataA;
                purchaseOrderInfo.DataB = purchaseOrder.DataB;
                purchaseOrderInfo.DataC = purchaseOrder.DataC;
                purchaseOrderInfo.SeasonId = purchaseOrder.SeasonId;
                purchaseOrderInfo.UpdateTime = DateTime.Now;
                purchaseOrderInfo.IsActive = true;

                if (insert)
                {
                    data.PurchaseOrders.Add(purchaseOrderInfo);
                }

                //var bidInfo = data.Bids.FirstOrDefault(b => b.CustomerID == customerId);

                //if (bidInfo != null) bidInfo.POOverride = purchaseOrder.PurchaseOrderNumber;

                data.SaveChanges();
                purchaseOrder.Id = purchaseOrderInfo.PurchaseOrderID;
            }
        }

        public bool DeletePurchaseOrder(int purchaseOrderId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var purchaseOrder = data.PurchaseOrders.Find(purchaseOrderId);
                if (purchaseOrder == null)
                {
                    return false;
                }

                purchaseOrder.UpdateTime = DateTime.Now;
                purchaseOrder.IsActive = false;
                data.SaveChanges();
                return true;
            }
        }

        public decimal GetPurchasedAmount(string purchaseOrderNumber, List<int> shippingLocationIdList)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                // 2018-Jul-22, JGS - Get the currently active season
                var currentSeason = data.Seasons.FirstOrDefault(s => s.IsActive == true);

                // 2018-Jul-22, JGS - Make sure orders are for the current season
                var orders = data.OrderTables.AsNoTracking()
                    .Where(order => order.POOverride == purchaseOrderNumber && order.SeasonId == currentSeason.SeasonId && shippingLocationIdList.Any(sl => sl == order.Bid.CustomerID))
                    .Select(OrderSummary.Transform)
                    .ToList();
                return Convert.ToDecimal(orders.Sum(order => order.Quantity - order.Balance));
            }
        }

        public decimal GetBalanceOfAllPurchaseOrders(int seasonId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var pobalance = from bids in data.Bids.AsNoTracking()
                                join companies in data.Companies.AsNoTracking() on bids.Customer.CompanyId equals companies.CompanyId
                                join ordertables in data.OrderTables.AsNoTracking() on bids.BidID equals ordertables.BidID
                                join purchaseorder in data.PurchaseOrders.AsNoTracking() on ordertables.POOverride equals purchaseorder.PurchaseOrderNumber
                                into pos
                                from poordertables in pos.DefaultIfEmpty()
                                join orderupdate in data.OrderUpdates.AsNoTracking() on ordertables.OrderID equals orderupdate.OrderID
                                into otablesupdates
                                from orderTablesUpdates in otablesupdates.DefaultIfEmpty()
                                where bids.IsActive == true
                                    && ordertables.IsActive == true
                                    && poordertables.IsActive == true
                                    && poordertables.isComplete == false
                                    && orderTablesUpdates.IsActive == true
                                    && bids.SeasonId == seasonId  // 2018-JUl-17, JGS - Make explicit use of the SeasonID
                                group new { companies, poordertables, orderTablesUpdates } by new
                                {
                                    CompanyFullname = companies.FullName,
                                }
                                into purchaseOrdersByCompany
                                select new
                                {
                                    CompanyFullname = purchaseOrdersByCompany.Key.CompanyFullname,
                                    POQuantityTons = purchaseOrdersByCompany.Sum(x => x.poordertables.Quantity),
                                    OrderedQuantityTons = purchaseOrdersByCompany.Sum(x => x.orderTablesUpdates.Quantity / 2000),
                                    POBalanceTons_POQuntityMinusOrderedQuantity = purchaseOrdersByCompany.Sum(x => x.poordertables.Quantity - (x.orderTablesUpdates.Quantity / 2000)),
                                };

                return Convert.ToDecimal(0);


                /*
                var balance = data.PurchaseOrders.AsNoTracking()
                        .Where(po => po.IsActive && po.SeasonId == seasonId)
                        .Select(PurchaseOrderData.Transform)
                        .GroupBy(s => new { s.CompanyId, s.Balance })
                        .Select(g => new
                        {
                            CompanyId = g.Key.CompanyId,
                            Balance = g.Sum(gs => gs.Balance)
                        }
                        );
                        
                
                return Convert.ToDecimal(balance);
                */
            }

        }

        #region DTO

        internal class PurchaseOrderData : IPurchaseOrder
        {
            private float? _quantityDispatched;
            private float? _quantityOfPOOnOrder;
            private float? _quantityOfPORemaining;
            private float? _balancePerOrder;

            public static readonly Expression<Func<PurchaseOrder, IPurchaseOrder>> Transform = e => new PurchaseOrderData
            {
                Id = e.PurchaseOrderID,
                PurchaseOrderNumber = e.PurchaseOrderNumber,
                CustomerId = e.CustomerID,
                QuantityOfPO = e.Quantity,
                MaterialId = e.MaterialID,
                MaterialDescription = e.Material.Description,
                //Material = e.Material,
                IsComplete = e.isComplete,
                IsHeld = e.isHeld,
                EffectiveDate = e.effectiveDate,
                CreateTime = e.CreateTime,
                UpdateTime = e.UpdateTime,
                IsActive = e.IsActive,
                Comment = e.Comment,
                
                _quantityDispatched = e.Customer.Bids.Where(b => b.IsActive)
                                        .Sum(b => b.OrderTables.Where(o => o.IsActive && o.POOverride == e.PurchaseOrderNumber)
                                            .Sum(o => o.Tickets.Where(t => t.IsActive && !t.isVoid)
                                                .Sum(t => t.Quantity / 2000))),
                /*
                _quantityDispatched = (from b in e.Customer.Bids.Where
                                      join ot in b.OrderTables on b.BidID equals ot.BidID
                                      join t in Tickets on ot.OrderID equals t.OrderId into _orderTablesTickets
                                      from t in _orderTablesTickets.DefaultIfEmpty()
                                      where ot.SeasonId == 1
                                   && ot.POOverride == "TEST100"
                                   && ot.IsActive
                                   && t.IsActive
                                   && !t.IsVoid
                                      select t.Quantity / 2000
		                        ).AsEnumerable().Sum(),
         */
            _quantityOfPOOnOrder = e.Customer.Bids.Where(b => b.IsActive)
                                        .Sum(b => b.OrderTables.Where(o => o.IsActive && o.POOverride == e.PurchaseOrderNumber)
                                            .Sum(o => o.OrderUpdates.Where(t => t.IsActive)
                                                .Sum(t => t.Quantity / 2000))),

                _balancePerOrder = e.Customer.Bids.Where(b => b.IsActive)
                                        .Sum(b => b.OrderTables.Where(o => o.IsActive && o.POOverride == e.PurchaseOrderNumber)
                                            .Sum(o => o.OrderUpdates.Where(t => t.IsActive)
                                                .Sum(t => t.Quantity / 2000))),

                SeasonId = e.SeasonId,
                CompanyId = (int)e.Customer.CompanyId,
                DataA = e.DataA,
                DataB = e.DataB,
                DataC = e.DataC
            };

            public PurchaseOrderData()
            {

            }

            public PurchaseOrderData(PurchaseOrder purchaseOrder)
            {
                Id = purchaseOrder.PurchaseOrderID;
                PurchaseOrderNumber = purchaseOrder.PurchaseOrderNumber;
                CustomerId = purchaseOrder.CustomerID;
                QuantityOfPO = purchaseOrder.Quantity;
                MaterialId = purchaseOrder.MaterialID;
                MaterialDescription = purchaseOrder.Material.Description;
                //Material = purchaseOrder.Material;
                IsComplete = purchaseOrder.isComplete;
                IsHeld = purchaseOrder.isHeld;
                EffectiveDate = purchaseOrder.effectiveDate;
                CreateTime = purchaseOrder.CreateTime;
                UpdateTime = purchaseOrder.UpdateTime;
                IsActive = purchaseOrder.IsActive;
                Comment = purchaseOrder.Comment;
                SeasonId = purchaseOrder.SeasonId;
                DataA = purchaseOrder.DataA;
                DataB = purchaseOrder.DataB;
                DataC = purchaseOrder.DataC;
            }

            public int Id { get; set; }
            public string PurchaseOrderNumber { get; set; }
            public int CustomerId { get; set; }
            public int? QuantityOfPO { get; set; }
            public float? QuantityDispatched
            {
                get
                {
                    return _quantityDispatched.HasValue ? _quantityDispatched : 0;
                }
                set { _quantityDispatched = value; }
            }
            public float? QuantityOfPOOnOrder
            {
                get
                {
                    return _quantityOfPOOnOrder.HasValue ? _balancePerOrder : 0;
                }
                set { _quantityOfPOOnOrder = value; }
            }
            public float? BalancePerOrder
            {
                get
                {
                    return _balancePerOrder.HasValue ? _balancePerOrder : 0;
                }
                set { _balancePerOrder = value; }
            }
            public float? QuantityOfPORemaining
            {
                get
                {
                    return QuantityOfPO.HasValue ? QuantityOfPO - (_quantityOfPOOnOrder ?? 0) : 0;
                }
                set { _quantityOfPORemaining = value; }
            }
            public IMaterialInfo Material { get; set; }
            public int MaterialId { get; set; }
            public string MaterialDescription { get; set; }
            public bool IsComplete { get; set; }
            public bool IsHeld { get; set; }
            public DateTime? EffectiveDate { get; set; }
            public DateTime CreateTime { get; set; }
            public DateTime? UpdateTime { get; set; }
            public bool IsActive { get; set; }
            public string Comment { get; set; }
            public int SeasonId { get; set; }
            public int CompanyId { get; set; }
            public string DataA { get; set; }
            public string DataB { get; set; }
            public string DataC { get; set; }
        }

        #endregion
    }
}