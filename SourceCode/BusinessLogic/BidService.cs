﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Contract.Domain;
using Contract.Exception;
using Contract.Services;
using DataAccess;
using System.Collections.ObjectModel;
using BusinessLogic;

namespace BusinessLogic
{
    public class BidService : IBidService
    {
        private readonly IConfiguration _configuration;

        public BidService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public IBid GetBidData(int bidId, int seasonId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var bid = data.Bids.AsNoTracking()
                .Where(c => c.IsActive && c.BidID == bidId && c.SeasonId == seasonId)
                .OrderByDescending(cc => cc.BidID)
                .FirstOrDefault();
                return bid == null ? null : new BidData(bid);
            }
        }

        public IBidInfo GetCustomerBidInformation(int seasonId, int customerId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var bid = data.Bids.AsNoTracking()
                .Where(c => c.IsActive && c.SeasonId == seasonId && c.CustomerID == customerId)
                .OrderByDescending(cc => cc.BidID)
                .FirstOrDefault();
                return bid == null ? null : new BidInfo(bid);
            }
        }


        public ObservableCollection<IBidInfo> GetCustomerBidInformationList(int seasonId, int customerId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var bids = data.Bids.AsNoTracking()
                    .Where(c => c.SeasonId == seasonId
                            && c.CustomerID == customerId
                            && c.IsActive == true)
                    .Select(BidInfo.Transform)
                    //.OrderByDescending(cc => cc.Material.ShortName)
                    .ToList();
                //return bids == null ? null : bids;
                return new ObservableCollection<IBidInfo>(bids);
            }
        }

        

        public IBidInfo AddEmptyRow(int companyId, string companyFullName, int siteId, string siteName, int seasonId, int customerId)
        {
            return new BidInfo(){
                CompanyId = companyId,
                CompanyFullName = companyFullName,
                SiteId = siteId,
                SiteName = siteName,
                SeasonId = seasonId,
                TruckingRate = 0,
                PricePerTon = 0,
                Toll = 0,
                CustomerId = customerId,
            };
        }

        
        public bool IsBidMaterialOrdersComplete(IBidInfo bidInfo)
        {
            if (bidInfo == null)
                throw new ArgumentNullException("BidService:IsBidMaterialOrdersComplete bidInfo");

            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {

                var openOrders = from b in data.Bids.AsNoTracking()
                                 join ot in data.OrderTables.AsNoTracking() on b.BidID equals ot.BidID
                                 where (b.BidID == bidInfo.BidId)
                                 && (b.SeasonId == bidInfo.SeasonId)
                                 && (b.CustomerID == bidInfo.CustomerId)

                                 && (ot.IsActive == true)
                                 && (ot.isComplete == false)
                                 select new { ot.Material.ShortName, ot.OrderNumber };

                return openOrders.Any() ? false : true;
            }
        }

        public bool IsBidMaterialPurchaseOrdersComplete(IBidInfo bidInfo)
        {
            if (bidInfo == null)
                throw new ArgumentNullException("BidService:IsBidMaterialPurchaseOrdersComplete bidInfo");

            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {

                var openPurchaseOrders = from b in data.Bids.AsNoTracking()
                                         join po in data.PurchaseOrders.AsNoTracking() on b.CustomerID equals po.CustomerID
                                         where (b.SeasonId == bidInfo.SeasonId)
                                         && (po.MaterialID == bidInfo.MaterialId)
                                         && (po.IsActive == true)
                                         && (po.isComplete == false)
                                         select new { b.Material.ShortName, po.PurchaseOrderNumber };

                return openPurchaseOrders.Any();
            }
        }

        public bool IsBidMaterialPurchaseOrdersAndOrderComplete(IBidInfo bidInfo)
        {

            return !IsBidMaterialPurchaseOrdersComplete(bidInfo) || !IsBidMaterialOrdersComplete(bidInfo);
        }


        public bool DeleteRow(IBidInfo bidInfo)
        {
            if (bidInfo == null)
                throw new ArgumentNullException("BidService:DeleteRow bidInfo");

            if (bidInfo.BidId == null)
            {
                return true;
            }

            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                Bid bid = data.Bids.First(c => c.BidID == bidInfo.BidId.Value);

                var openOrdersToBid = bid.OrderTables.Where(o => o.BidID == bidInfo.BidId
                                            && o.SeasonId == bidInfo.SeasonId   // 17-Jul-2018, JGS - Add in constraint to ensure the season matches
                                            && !o.isComplete).Any();

                if (openOrdersToBid)
                {
                    return false;
                }
                else
                {
                    bid.UpdateTime = DateTime.Now;
                    bid.IsActive = false;
                    data.SaveChanges();

                    return true;
                }
            }
        }

        public string VerifyBidBeforeSave(ObservableCollection<IBidInfo> bidList, int seasonId)
        {
            string returnStringOfErrorText = string.Empty;
            var duplicateCompanyAndMaterial = bidList.GroupBy(b => new { b.MaterialShortName })
                .Where(g => g.Count() > 1).ToList();
            foreach (var item in duplicateCompanyAndMaterial)
            {
                returnStringOfErrorText += string.Format($"This material already has a bid: {item.Key.MaterialShortName}" );
            }

            return returnStringOfErrorText;
        }

        public int SaveBid(ObservableCollection<IBidInfo> bidData, int seasonId)
        {
            if (bidData == null)
                throw new ArgumentNullException("BidService:SaveBid bidData");

            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                Bid bid;

                foreach (var bidInfo in bidData)
                {

                    if (!bidInfo.BidId.HasValue)
                    {
                        bid = data.Bids.Create();
                        bid.IsActive = true;
                        bid.CreateTime = DateTime.Now;
                        bid.SeasonId = seasonId;
                        data.Bids.Add(bid);
                    }
                    else
                    {
                        bid = data.Bids.First(c => c.BidID == bidInfo.BidId.Value);
                    }

                    bid.CustomerID = bidInfo.CustomerId;
                    //bid.CompanyID = bidInfo.CompanyId;
                    bid.SiteId = bidInfo.SiteId;
                    bid.MaterialID = bidInfo.MaterialId;
                    bid.PricePerTon = bidInfo.PricePerTon;
                    bid.PricePerYard = 0;
                    bid.TruckingRate = bidInfo.TruckingRate;
                    bid.POOverride = string.Empty;
                    bid.UpdateTime = DateTime.Now;
                    bid.Toll = bidInfo.Toll;
                    bid.SeasonMaterialEstimateTon = bidInfo.SeasonMaterialEstimateTon;
                    bid.Comment = string.Empty;

                    data.SaveChanges();
                }

                //return bid.BidID;
                return 1;
            }
        }

    }

    #region DTO

    internal class BidInfo : IBidInfo
    {
        private decimal? _seasonMaterialEstimateTon;

        public static readonly Expression<Func<Bid, IBidInfo>> Transform = e => new BidInfo
        {
            BidId = e.BidID,
            CustomerId = e.CustomerID,
            CompanyId = e.Customer.Company.CompanyId,
            CompanyFullName = e.Customer.Company.FullName,
            SiteId = e.SiteId,
            SiteName = e.Site.SiteName,
            MaterialId = e.MaterialID,
            MaterialShortName = e.Material.ShortName,
            MaterialDescription = e.Material.Description,
            PricePerTon = e.PricePerTon,
            TruckingRate = e.TruckingRate,
            IsActive = e.IsActive,
            SeasonId = e.SeasonId,
            Toll = e.Toll,
            SeasonMaterialEstimateTon = e.SeasonMaterialEstimateTon,
        };

        public BidInfo()
        {
        }

        public BidInfo(Bid bid)
        {
            BidId = bid.BidID;
            CustomerId = bid.CustomerID;
            CompanyId = bid.Customer.Company.CompanyId;
            CompanyFullName = bid.Customer.Company.FullName;
            SiteId = bid.SiteId;
            SiteName = bid.Site.SiteName;
            MaterialId = bid.MaterialID;
            MaterialShortName = bid.Material.ShortName;
            MaterialDescription = bid.Material.Description;
            PricePerTon = bid.PricePerTon;
            TruckingRate = bid.TruckingRate;
            IsActive = bid.IsActive;
            SeasonId = bid.SeasonId;
            Toll = bid.Toll;
            SeasonMaterialEstimateTon = bid.SeasonMaterialEstimateTon;
        }

        public int? BidId { get; set; }
        public int CustomerId { get; set; }
        public int CompanyId { get; set; }
        public string CompanyFullName { get; set; }
        public int SiteId { get; set; }
        public string SiteName { get; set;}
        public int MaterialId { get; set; }
        public string MaterialShortName { get; set; }
        public string MaterialDescription { get; set; }
        public decimal? PricePerTon { get; set; }
        public decimal? TruckingRate { get; set; }
        public bool IsActive { get; set; }
        public int SeasonId { get; set; }
        public decimal? Toll { get; set; }
        public decimal? SeasonMaterialEstimateTon
        {
            get
            {
                return _seasonMaterialEstimateTon.HasValue ? _seasonMaterialEstimateTon : 0;
            }
            set { _seasonMaterialEstimateTon = value; }
        }
        
    }

    internal class BidData : IBid
    {
        private decimal? _seasonMaterialEstimateTon;

        public static readonly Expression<Func<Bid, IBid>> Transform = e => new BidData
        {
            BidId = e.BidID,
            CustomerId = e.CustomerID,
            CompanyId = e.Customer.Company.CompanyId,
            //Company = new CommonService.CompanyInfo(e.Company),
            SiteId = e.SiteId,
            //Site = new SiteInfo(e.Site),
            MaterialId = e.MaterialID,
            //Material = new MaterialInfo(e.Material),
            MaterialShortName = e.Material.ShortName,
            PricePerTon = e.PricePerTon,
            PricePerYard = e.PricePerYard,
            TruckingRate = e.TruckingRate,
            POOverride = e.POOverride,
            CreateTime = e.CreateTime,
            UpdateTime = e.UpdateTime,
            Comment = e.Comment,
            IsActive = e.IsActive,
            SeasonId = e.SeasonId,
            Toll = e.Toll,
            SeasonMaterialEstimateTon = e.SeasonMaterialEstimateTon,
        };

        public BidData()
        {
        }

        public BidData(Bid bid)
        {
            BidId = bid.BidID;
            CustomerId = bid.CustomerID;
            CompanyId = bid.Customer.Company.CompanyId;
            //Company = new CommonService.CompanyInfo(bid.Company);
            Company = new CommonService.CompanyInfo(bid.Customer.Company);
            SiteId = bid.SiteId;
            Site = new SiteInfo(bid.Site);
            MaterialId = bid.MaterialID;
            Material = new MaterialInfo(bid.Material);
            PricePerTon = bid.PricePerTon;
            PricePerYard = bid.PricePerYard;
            TruckingRate = bid.TruckingRate;
            Toll = bid.Toll;
            POOverride = bid.POOverride;
            CreateTime = bid.CreateTime;
            UpdateTime = bid.UpdateTime;
            IsActive = bid.IsActive;
            Comment = bid.Comment;
            SeasonMaterialEstimateTon = bid.SeasonMaterialEstimateTon;
        }

        public IMaterialInfo Material { get; set; }
        public int? BidId { get; set; }
        public int CustomerId { get; set; }
        public int CompanyId { get; set; }
        public ICompanyInfo Company { get; set; }
        public int SiteId { get; set; }
        public ISiteInfo Site { get; set; }
        public int MaterialId { get; set; }
        public decimal? PricePerTon { get; set; }
        public decimal? PricePerYard { get; set; }
        public decimal? TruckingRate { get; set; }
        public string POOverride { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        public bool IsActive { get; set; }
        public string Comment { get; set; }
        public int SeasonId { get; set; }
        public decimal? Toll { get; set; }
        public decimal? SeasonMaterialEstimateTon
        {
            get
            {
                return _seasonMaterialEstimateTon.HasValue ? _seasonMaterialEstimateTon : 0;
            }
            set
            {
                _seasonMaterialEstimateTon = value;
            }
        }

        public string CompanyFullName { get; set; }
        public string MaterialShortName { get; set; }

    }

    #endregion
}