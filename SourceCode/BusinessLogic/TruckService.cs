using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Transactions;
using Contract.Domain;
using Contract.Exception;
using Contract.Services;
using DataAccess;

namespace BusinessLogic
{
    public class TruckService : ITruckService
    {
        private readonly IConfiguration _configuration;

        public TruckService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public List<ITruckInfo> GetFilteredTrucks(string filter, int seasonId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var query = data.Trucks.AsNoTracking()
                    .Where(
                        truck => truck.IsActive && truck.SeasonId == seasonId);
                if (!string.IsNullOrWhiteSpace(filter))
                {
                    filter = filter.Trim();
                    query = query.Where(truck => (truck.ShortName.Contains(filter)
                                                  || truck.Tag.Contains(filter)
                                                  || truck.Sticker.Contains(filter)));
                }
                
                return query.Select(TruckInfo.Transform).OrderBy(o => o.Name).ToList();
            }
        }

        public List<ITruckOwnerInfo> GetFilteredTruckOwners(string filter, int seasonId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var query = data.TruckOwners.Where(to => to.IsActive && to.SeasonId == seasonId);

                if (!string.IsNullOrWhiteSpace(filter))
                {
                    filter = filter.Trim();
                    query = query.Where(to => to.ShortName.Contains(filter));
                }

                return query.Select(TruckOwnerInfo.Transform).OrderBy(to => to.ShortName).ToList();
            }
        }

        public ITruckData GetNewTruck(int seasonId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                Truck truck = data.Trucks.Create();
                truck.IsActive = false;
                truck.ShortName = "";
                truck.CreateTime = DateTime.Now;
                truck.UpdateTime = DateTime.Now;
                truck.SeasonId = seasonId;

                data.Trucks.Add(truck);

                data.SaveChanges();

                return new TruckData(truck);
            }
        }

        public void SaveTruck(ITruckData truck)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                if(data.Trucks.Any(t => t.ShortName == truck.ShortName && t.TruckID != truck.Id && t.IsActive && t.SeasonId == truck.SeasonId))
                    throw new UniqueConstraintException("Truck Id should be unique.");

                var truckObject = data.Trucks.FirstOrDefault(t => t.TruckID == truck.Id);

                if (truckObject == null)
                    throw new Exception("Impossible to save truck.");

                var updateTime = truck.UpdateTime.HasValue
                    ? truck.UpdateTime.Value
                    : DateTime.Now;
                               
                truckObject.UpdateTime = updateTime;
                truckObject.IsActive = true;
                truckObject.ShortName = truck.ShortName;
                truckObject.Comment = truck.Comment;
                truckObject.CubicYards = truck.CubicYards;
                truckObject.DataA = truck.PhoneNumber;
                truckObject.Description = truck.Description;
                if (truck.NeedsTareOverride)
                {
                    truckObject.TareInterval = truck.TareInterval;
                }
                truckObject.NeedsTareOverride = truck.NeedsTareOverride;

                truckObject.Sticker = truck.Sticker;
                truckObject.Tag = truck.Tag;
                
                truckObject.TruckOwnerID = truck.TruckOwnerId;

                var tares = truckObject.TruckWeightTares.Where(t => t.IsActive).OrderByDescending(ou => ou.UpdateTime).ToList();
                var weightTare = tares.FirstOrDefault();

                if (weightTare == null || weightTare.TareWeight != truck.Weight )
                {
                    foreach (var truckWeightTare in tares)
                    {
                        truckWeightTare.IsActive = false;
                        truckWeightTare.UpdateTime = updateTime;
                    }

                    var tare = data.TruckWeightTares.Create();
                    tare.IsActive = true;
                    tare.DateTimeOfWeight = DateTime.Now;
                    tare.TareWeight = truck.Weight;
                    tare.CreateTime = updateTime;
                    tare.UpdateTime = updateTime;

                    truckObject.TruckWeightTares.Add(tare);
                }
                data.SaveChanges();
            }
        }

        public ITruckData GetTruck(int truckId, int seasonId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var truck = data.Trucks.AsNoTracking().FirstOrDefault(t => t.TruckID == truckId && t.SeasonId == seasonId);

                return new TruckData(truck);
            }
        }

        public void DeleteTruck(int truckId)
        {
            using (var data= new SaltScaleEntities(_configuration.ConnectionString))
            {
                var truckToDelete = data.Trucks.FirstOrDefault(t => t.TruckID == truckId);
                if (truckToDelete == null) return;
                truckToDelete.IsActive = false;
                truckToDelete.UpdateTime = DateTime.Now;

                data.SaveChanges();
            }
        }

        public ITruckOwnerData GetTruckOwner(int ownerId, int seasonId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var owner = data.TruckOwners.AsNoTracking().FirstOrDefault(o => o.TruckOwnerID == ownerId && o.SeasonId==seasonId);

                if (owner == null)
                    throw new Exception("No Truck Owners found");
                return new TruckOwnerData(owner);
            }
        }
        
        public int SaveTruckOwner(ITruckOwnerData owner, int seasonId)
        {
            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                using (var data = new SaltScaleEntities(_configuration.ConnectionString))
                {
                    if (
                        data.TruckOwners.Any(
                            m => m.TruckOwnerID != owner.Id 
                                && m.IsActive 
                                && m.ShortName == owner.ShortName 
                                && m.SeasonId == owner.SeasonId
                                )
                            )
                    {
                        throw new UniqueConstraintException("Owner Id must be unique.");
                    }

                    TruckOwner truckOwner;

                    if (!owner.Id.HasValue)
                    {
                        truckOwner = data.TruckOwners.Create();
                        data.TruckOwners.Add(truckOwner);
                    }
                    else
                    {
                        truckOwner = data.TruckOwners.First(o => o.TruckOwnerID == owner.Id.Value);
                    }

                    DateTime updateTime = owner.UpdateTime.HasValue
                        ? owner.UpdateTime.Value
                        : DateTime.Now;

                    truckOwner.DataA = owner.DataA;
                    truckOwner.DataB = owner.DataB;
                    truckOwner.DataC = owner.DataC;
                    truckOwner.IsActive = owner.IsActive;
                    truckOwner.SeasonId = owner.SeasonId;
                    truckOwner.ShortName = owner.ShortName;
                    truckOwner.CreateTime = owner.CreateTime;
                    truckOwner.UpdateTime = updateTime;
                    

                    data.SaveChanges();
                    var ownerId = truckOwner.TruckOwnerID;
                    
                    var contactManager = new ContactManager(data);
                    
                    var contactId = contactManager.SaveContact(owner.Contact, seasonId);

                    if (!owner.Contact.ContactId.HasValue)
                    {
                        contactManager.InsertOwnerContact(contactId, ownerId);
                    }

                    scope.Complete();
                    
                    return ownerId;
                }
            }
        }

        public void DeleteTruckOwner(int ownerId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var truckOwnerToDelete = data.TruckOwners.FirstOrDefault(t => t.TruckOwnerID == ownerId);
                if (truckOwnerToDelete == null) return;
                truckOwnerToDelete.IsActive = false;
                truckOwnerToDelete.UpdateTime = DateTime.Now;

                if (truckOwnerToDelete.TruckOwnerContacts.Any(t=>t.Contact.IsActive))
                {
                    foreach (var item in truckOwnerToDelete.TruckOwnerContacts)
                    {
                        if (item.Contact!=null && item.Contact.IsActive)
                        {
                            item.Contact.IsActive = false;
                            item.Contact.UpdateTime = DateTime.Now;
                        }
                    }
                }

                if (truckOwnerToDelete.Trucks.Any(t => t.IsActive))
                {
                    foreach (var item in truckOwnerToDelete.Trucks)
                    {
                        if (item.IsActive)
                        {
                            item.TruckOwnerID = null;
                            item.UpdateTime = DateTime.Now;
                        }
                    }
                }

                data.SaveChanges();
            }
        }

        public ITruckOwnerData GetNewTruckOwner(int seasonId)
        {

            var owner = new TruckOwnerData();
            owner.ShortName = string.Empty;
            owner.SeasonId = seasonId;
            owner.IsActive = true;
            owner.CreateTime = DateTime.Now;
            owner.UpdateTime = DateTime.Now;
            owner.Contact = new ContactData();

            return owner;
        }

        #region DTO

        private class TruckData : ITruckData
        {
            public TruckData()
            {
            }

            public TruckData(Truck truck)
            {
                Id = truck.TruckID;
                TruckOwnerId = truck.TruckOwnerID;
                ShortName = truck.ShortName !=null ? truck.ShortName.ToUpper() : string.Empty;
                Tag = truck.Tag != null ? truck.Tag.ToUpper() : string.Empty;
                Description = truck.Description;
                TareInterval = truck.TareInterval;
                NeedsTareOverride = truck.NeedsTareOverride;
                CubicYards = truck.CubicYards;
                Sticker = truck.Sticker != null ? truck.Sticker.ToUpper() : string.Empty;
                CreateTime = truck.CreateTime;
                UpdateTime = truck.UpdateTime;
                IsActive = truck.IsActive;
                Comment = truck.Comment;
                PhoneNumber = truck.DataA;
                SeasonId = truck.SeasonId;
                var tare = truck.TruckWeightTares.OrderByDescending(t => t.UpdateTime).FirstOrDefault(t => t.IsActive);
                if (tare != null)
                {
                    Weight = tare.TareWeight;
                    DateTimeOfWeight = tare.DateTimeOfWeight;
                }
            }


            public int Id { get; set; }
            public int? TruckOwnerId { get; set; }
            public string ShortName { get; set; }
            public string Tag { get; set; }
            public string Description { get; set; }
            public int? TareInterval { get; set; }
            public bool NeedsTareOverride { get; set; }
            public int? CubicYards { get; set; }
            public string Sticker { get; set; }
            public DateTime CreateTime { get; set; }
            public DateTime? UpdateTime { get; set; }
            public bool IsActive { get; set; }
            public string Comment { get; set; }
            public string PhoneNumber { get; set; }
            public int SeasonId { get; set; }
            public int Weight { get; set; }
            public DateTime? DateTimeOfWeight { get; set; }
        }

        private class TruckInfo : ITruckInfo
        {
            public static readonly Expression<Func<Truck, ITruckInfo>> Transform = e => new TruckInfo
            {
                Id = e.TruckID,
                Name = e.ShortName !=null ? e.ShortName.ToUpper() : string.Empty,
                Description = e.Description,
                Tag = e.Tag != null ? e.Tag.ToUpper() : string.Empty,
                Sticker = e.Sticker != null ? e.Sticker.ToUpper() : string.Empty,
                OwnerId = e.TruckOwnerID,
                OwnerName = e.TruckOwner.ShortName,
                Phone = e.DataA
            };

            public string OwnerName { get; set; }

            public int? OwnerId { get; set; }

            public string Sticker { get; set; }

            public string Tag { get; private set; }

            public string Description { get; set; }

            public string Phone { get; set; }

            public string Name { get; set; }

            public int Id { get; set; }
        }

        private class TruckOwnerInfo : ITruckOwnerInfo
        {
            public static readonly Expression<Func<TruckOwner, ITruckOwnerInfo>> Transform = e => new TruckOwnerInfo
            {
                Id = e.TruckOwnerID,
                ShortName = e.ShortName,
            };

            public int? Id { get; set; }

            public string ShortName { get; set; }
        }

        private class TruckOwnerData : ITruckOwnerData
        {
            public TruckOwnerData()
            {
                
            }

            public TruckOwnerData(TruckOwner owner)
            {
                Id = owner.TruckOwnerID;
                ShortName = owner.ShortName;
                DataA = owner.DataA;
                DataB = owner.DataB;
                DataC = owner.DataC;
                IsActive = owner.IsActive;
                SeasonId = owner.SeasonId;
                CreateTime = owner.CreateTime;
                UpdateTime = owner.UpdateTime;

                var truckOwnerContact =
                    owner.TruckOwnerContacts.Where(c => c.Contact.IsActive)
                        .Select(c => c.Contact)
                        .OrderByDescending(cc => cc.ContactID)
                        .FirstOrDefault();
                Contact = truckOwnerContact != null ? new ContactData(truckOwnerContact) : new ContactData();
            }

            public int? Id { get; set; }
            public string ShortName { get; set; }
            public DateTime CreateTime { get; set; }
            public DateTime? UpdateTime { get; set; }
            public bool IsActive { get; set; }
            public string DataA { get; set; }
            public string DataB { get; set; }
            public string DataC { get; set; }
            public int SeasonId { get; set; }

            public IContactData Contact { get; set; }

        }

        #endregion
    }
}