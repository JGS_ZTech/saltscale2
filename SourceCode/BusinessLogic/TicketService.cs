﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Contract.Domain;
using Contract.Exception;
using Contract.Services;
using DataAccess;
using Caliburn.Micro;

namespace BusinessLogic
{
    public class TicketService : ITicketService
    {
        private readonly IConfiguration _configuration;
        private readonly ILog _log = LogManager.GetLog(typeof(TicketService));

        public TicketService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public IReadOnlyList<ITicketInfo> GetTicketsInfo(int seasonId, int? customerId = null, int? orderId = null, bool? cashTransactionTickets = null)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var tickets = data.Tickets.AsNoTracking()
                    .Where(t => t.IsActive && t.isVoid == false
                                && t.SeasonId == seasonId
                                && (customerId == null || t.OrderTable.Bid.CustomerID == customerId)
                                && (orderId == null || t.OrderId == orderId));
                if (cashTransactionTickets.HasValue && cashTransactionTickets.Value)
                {
                    tickets = tickets.Where(t => t.CashPurchaseId.HasValue && t.isVoid == false && !t.OrderId.HasValue);
                }

                return tickets.OrderByDescending(t => t.CreateTime)
                        .Select(TicketInfo.Transform)
                        .ToList();
            }
        }

        public IReadOnlyList<ICashTicketInfo> GetCashTicketsInfo(int seasonId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var tickets = data.Tickets.AsNoTracking()
                    .Where(t => t.IsActive 
                        && t.isVoid == false
                        && t.CashPurchaseId.HasValue
                        && t.SeasonId == seasonId
                        && !t.OrderId.HasValue);

                return tickets.OrderByDescending(t => t.CreateTime)
                        .Select(CashTicketInfo.Transform)
                        .ToList();
            }
        }

        public void VoidTicket(ITicketInfo ticket)
        {
            VoidTicket(ticket.Id, ticket.VoidReason);
        }        
        public void VoidTicket(ICashTicketInfo ticket)
        {
            VoidTicket(ticket.Id, ticket.VoidReason);
        }

        private void VoidTicket(int id, string reason)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                Ticket ticketInfo = data.Tickets.FirstOrDefault(o => o.TicketID == id);
                if (ticketInfo == null)
                    throw new Exception("Impossible to save void reason.");

                ticketInfo.VoidReason = reason;
                ticketInfo.isVoid = true;
                ticketInfo.UpdateTime = DateTime.Now;

                data.SaveChanges();
            }
        }

        // 2018-Jun-26, JGS - Setup mechanism to remove void status
        public void UnVoidTicket(ITicketInfo ticket)
        {
            UnVoidTicket(ticket.Id, ticket.VoidReason);
        }

        // 2018-Jun-26, JGS - Setup mechanism to remove void status
        private void UnVoidTicket(int id, string reason)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                Ticket ticketInfo = data.Tickets.FirstOrDefault(o => o.TicketID == id);
                if (ticketInfo == null)
                    throw new Exception("Impossible to unvoid ticket.");

                ticketInfo.VoidReason = "";
                ticketInfo.isVoid = false;
                ticketInfo.UpdateTime = DateTime.Now;

                data.SaveChanges();
            }
        }

        public ITicketData GetNewOrderTicket(int? orderId, int truckId, int seasonId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                // 2018-JUl-17, JGS - Make explicit use of the SeasonID
                //OrderTable order = data.OrderTables.AsNoTracking().Include("Bid").FirstOrDefault(o => o.OrderID == orderId);
                OrderTable order = data.OrderTables.AsNoTracking().Include("Bid").FirstOrDefault(o => o.OrderID == orderId && o.SeasonId == seasonId);

                if (order == null)
                    throw new EntityNotFoundException("There are no Orders with this Order ID.", typeof (OrderTable), orderId);

                if (order.Bid == null)
                    throw new EntityNotFoundException("There are no Orders with this Bid ID.", typeof (OrderTable), orderId);

                if (order.Bid.Customer.Company == null)
                    throw new EntityNotFoundException("There are no companies defined in database.", typeof (Company), "default company");

                TruckWeightTare tare =
                    data.TruckWeightTares.AsNoTracking().Where(t => t.IsActive && t.TruckId == truckId)
                        .OrderByDescending(t => t.UpdateTime)
                        .FirstOrDefault();
                if (tare == null)
                    throw new EntityNotFoundException("There are no truck weight defined in database.",
                        typeof (TruckWeightTare), truckId);

                var isTareExpired = false;

                if (tare.Truck.NeedsTareOverride)
                {
                    var interval = tare.Truck.TareInterval.HasValue ? tare.Truck.TareInterval.Value : 0;
                    var date = (tare.DateTimeOfWeight.HasValue ? tare.DateTimeOfWeight.Value.AddDays(interval) : DateTime.Today).Date;

                    if (date <= DateTime.Today.Date)
                    {
                        isTareExpired = true;
                    }
                }

                //TODO Rework for multiple materials
                //decimal? quote = order.Bid.Customer.CashMaterialRate;
                decimal? quote = order.Bid.PricePerTon;

                decimal? taxRate = order.Bid.Site.TaxRate;

                string instr = GetSpecialInstruction(order);

                Contact primaryContact =
                    order.Bid.Customer.CustomerContacts.Where(cc => cc.Contact.IsActive)
                        .Select(cc => cc.Contact).OrderByDescending(c => c.ContactID).FirstOrDefault();
                
                Address primaryAddress = primaryContact == null ? null : primaryContact.Address;

                string customerOrganization = primaryContact.Organization;
                string customerDispatcherName = order.Bid.Customer.DispatcherName; //primaryContact != null ? primaryContact.Organization : string.Empty;
                string customerContactName = primaryContact != null ? primaryContact.Name : string.Empty;
                string customerShortName = order.Bid.Customer.ShortName; // primaryContact != null ? primaryContact.Name : string.Empty;
                bool taxExempt =  order.Bid.Customer.IsTaxExempt;
                string phone = GetPhone(primaryContact);

                string address = GetCustomerAddress(primaryAddress, customerContactName);

                var ticket = new TicketData
                {
                    CreateTime = DateTime.Now,
                    UpdateTime = DateTime.Now,
                    CompanyId = order.Bid.Customer.CompanyId,
                    IsActive = true,
                    SeasonId = seasonId,
                    OrderId = orderId,
                    TruckId = truckId,
                    Quantity = 0,
                    TruckWeight = tare.TareWeight,
                    TruckShortName = tare.Truck.ShortName,
                    TruckTag = tare.Truck.Tag,
                    TruckSticker = tare.Truck.Sticker,
                    TruckDescription = tare.Truck.Description,
                    SpecialInstruction = instr,
                    CustomerAddress = address,
                    HideDollarAmount = order.Bid.Customer.HideDollarAmounts,
                    Address = primaryAddress != null ? new AddressData(primaryAddress) : null,
                    CustomerOrganization = customerOrganization,
                    CustomerName = customerDispatcherName,
                    CustomerContactName = customerContactName,
                    MaterialId=order.MaterialId,
                    PO = order.POOverride,
                    CustomerPhone = phone,
                    CustomerShortName = customerShortName,
                    TruckRate = order.Bid.TruckingRate,
                    OrderNumber = order.OrderNumber,
                    IsTareExpired =isTareExpired,
                    FuelSurcharge=order.Bid.Site.FuelSurcharge,
                    ConfirmationNumber = order.ConfirmationNumber
                };

                if (quote.HasValue && quote.Value > 0)
                {
                    ticket.CashData = new CashPurchaseData
                    {
                        CompanyId = order.Bid.Customer.CompanyId,
                        CreateTime = DateTime.Now,
                        UpdateTime = DateTime.Now,
                        IsActive = true,
                        MaterialId = order.MaterialId,
                        SeasonId = seasonId,
                        Quote = quote,
                        TruckDescription = tare.Truck.Description,
                        TaxRate = taxRate,
                        LocationId = order.SiteID,
                        TaxExempt = taxExempt,
                    };
                }

                return ticket;
            }
        }

        public ITicketData GetNewCashTicket(int seasonId, string defaultSite, ICashPurchase cashPurchase)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var site = !string.IsNullOrWhiteSpace(defaultSite)
                   ? data.Sites.FirstOrDefault(s => s.SeasonId == seasonId && s.SiteName == defaultSite)
                   : data.Sites.FirstOrDefault(s => s.SeasonId == seasonId);

                if (site == null)
                {
                    throw new EntityNotFoundException("Site, defined in configuration, not found in database.",
                        typeof(Site), "default location");
                }

                //var company = site.DefaultCompanyId.HasValue ? site.Company : data.Companies.AsNoTracking().FirstOrDefault();
                var company = cashPurchase.Company;

                if (company == null)
                {
                    throw new EntityNotFoundException("There are no companies defined in database.", typeof(Company), "default company");
                }

                var key = $"NextTicket {company.Name}";

                var currentTicketId = data.ticketProps.FirstOrDefault(tp => tp.Name == key);

                if (currentTicketId == null)
                {
                    throw new EntityNotFoundException("There are no ticket number configuration defined in database.",
                        typeof(ticketProp), key);
                }

                //currentTicketId.ValueNumber++;

                data.SaveChanges();

                var ticket = new TicketData
                {
                    TicketNumber = currentTicketId.ValueNumber.Value,
                    CreateTime = DateTime.Now,
                    UpdateTime = DateTime.Now,
                    CompanyId = company.Id,
                    IsActive = true,
                    SeasonId = seasonId,
                    Quantity = 0,
                    FuelSurcharge = 0,
                    CashData = new CashPurchaseData
                    {
                        CompanyId = company.Id,
                        CreateTime = DateTime.Now,
                        UpdateTime = DateTime.Now,
                        IsActive = true,
                        SeasonId = seasonId,
                        TaxRate = site.TaxRate
                    },
                };

                return ticket;
            }
        }

        private string GetPhone(Contact contact)
        {
            if (contact == null || !contact.PhoneNumbers.Any(p => p.isPrimary && p.IsActive))
                return string.Empty;
            var number = contact.PhoneNumbers.OrderByDescending(p => p.PhoneNumberID).FirstOrDefault(
                p => p.isPrimary && p.IsActive && !string.IsNullOrWhiteSpace(p.PhoneNumber1));

            return number != null ? number.PhoneNumber1 : string.Empty;
        }

        public bool IsTicketExist(int ticketNumber, int? ticketId, int seasonId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                if (ticketId.HasValue)
                {
                    return
                        data.Tickets.AsNoTracking()
                            .Any(
                                t =>
                                    t.SeasonId == seasonId && t.TicketNumber == ticketNumber &&
                                    t.TicketID != ticketId.Value);
                }
                else
                {
                    return data.Tickets.AsNoTracking().Any(t => t.SeasonId == seasonId && t.TicketNumber == ticketNumber);
                }
            }
        }

        public int GetNextTicketNumber(int companyId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                Company company = data.Companies.AsNoTracking().FirstOrDefault(c => c.CompanyId == companyId);
                if (company == null)
                    throw new EntityNotFoundException("There are no companies defined in database.", typeof (Company),
                        "default company");

                string key = string.Format($"NextTicket {company.FullName}");

                ticketProp currentTicketId = data.ticketProps.FirstOrDefault(tp => tp.Name == key);

                if (currentTicketId == null || !currentTicketId.ValueNumber.HasValue)
                    throw new EntityNotFoundException("There are no ticket number configuration defined in database.", typeof (ticketProp), key);

                return currentTicketId.ValueNumber.Value;
            }
        }

        public int SaveOrderTicket(ITicketData ticket)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                TicketSavePreprocessing(ticket, data);

                var newItem = data.Tickets.Create();
                data.Tickets.Add(newItem);
                newItem.CompanyID = ticket.CompanyId;
                newItem.CreateTime = ticket.CreateTime;
                newItem.IsActive = ticket.IsActive;
                newItem.OrderId = ticket.OrderId;
                newItem.Quantity = ticket.Quantity;
                newItem.SeasonId = ticket.SeasonId;
                newItem.TicketNumber = ticket.TicketNumber;
                newItem.TruckID = ticket.TruckId;
                newItem.UpdateTime = ticket.UpdateTime;
                newItem.isManual = ticket.IsManual;
                newItem.FuelSurcharge = ticket.FuelSurcharge;

                //add cash transaction
                if (ticket.CashData != null && ticket.CashData.Quote.HasValue && ticket.CashData.Quote.Value != 0)
                {
                    if (ticket.CashData.PaymentForm != null)
                    {
                        newItem.CashPurchase = new CashPurchase();
                        newItem.CashPurchase = data.CashPurchases.Create();

                        newItem.CashPurchase.companyID = ticket.CompanyId;
                        newItem.CashPurchase.CreateTime = ticket.CashData.CreateTime;
                        newItem.CashPurchase.IsActive = ticket.CashData.IsActive;
                        newItem.CashPurchase.materialID = ticket.CashData.MaterialId;
                        newItem.CashPurchase.SeasonId = ticket.CashData.SeasonId;
                        newItem.CashPurchase.UpdateTime = ticket.UpdateTime;
                        newItem.CashPurchase.checkNumber = ticket.CashData.CheckNumber;
                        newItem.CashPurchase.locationID = ticket.CashData.LocationId;
                        newItem.CashPurchase.paymentForm = ticket.CashData.PaymentForm;
                        newItem.CashPurchase.quote = ticket.CashData.Quote.Value;
                        newItem.CashPurchase.tare = ticket.CashData.Tare;
                        newItem.CashPurchase.taxExempt = ticket.CashData.TaxExempt;
                        newItem.CashPurchase.truckDescription = ticket.CashData.TruckDescription;
                        newItem.CashPurchase.TaxRate = ticket.CashData.TaxRate;
                        newItem.CashPurchase.isComplete = true;
                    }
                }
                
                data.SaveChanges();

                return newItem.TicketID;
            }
        }

        private void TicketSavePreprocessing(ITicketData ticket, SaltScaleEntities data)
        {
            Company company = data.Companies.AsNoTracking().FirstOrDefault(c => c.CompanyId == ticket.CompanyId);
            if (company == null)
            {
                var ex = new EntityNotFoundException("There are no companies defined in database.", typeof(Company), "default company");
                _log.Error(ex);
                throw ex;
            }

            string key = string.Format($"NextTicket {company.FullName}");

            ticketProp currentTicketId = data.ticketProps.FirstOrDefault(tp => tp.Name == key);
            _log.Info($"Get the next ticket number - Company: {company.FullName}  Ticket: {currentTicketId.ValueNumber}");

            if (currentTicketId == null || !currentTicketId.ValueNumber.HasValue)
            {
                var ex = new EntityNotFoundException("There are no ticket number configuration defined in database.", typeof(ticketProp), key);
                _log.Error(ex);
                throw ex;
            }

            if (IsTicketExist(ticket.TicketNumber, ticket.TicketId, ticket.SeasonId))
            {
                var ex = new Exception($"The ticket number already exists. Please select a new range. The '{currentTicketId.ValueNumber.Value}' ticket number can be used.");
                _log.Error(ex);
                throw ex;
            }

            string allowRandomTicketKey = ($"allowRandomTicket {company.FullName}");
            var configuration = data.configurations.AsNoTracking().FirstOrDefault(c => c.name == allowRandomTicketKey);

            //Resequence tickets
            if (ticket.TicketNumber > currentTicketId.ValueNumber.Value
                && (configuration == null || configuration.valuestring != "Y")
                )
            {
                _log.Info($"Resequencing Tickets Begin - Because of a manual entry ticket number.  From '{currentTicketId.ValueNumber.Value}' to '{ticket.TicketNumber}'");

                for (int i = currentTicketId.ValueNumber.Value; i < ticket.TicketNumber; i++)
                {
                    var voidedItem = data.Tickets.Create();
                    data.Tickets.Add(voidedItem);
                    voidedItem.isVoid = true;
                    voidedItem.IsActive = true;
                    voidedItem.CompanyID = ticket.CompanyId;
                    voidedItem.CreateTime = ticket.CreateTime;
                    voidedItem.OrderId = ticket.OrderId;
                    voidedItem.Quantity = 0;
                    voidedItem.SeasonId = ticket.SeasonId;
                    voidedItem.TicketNumber = i;
                    voidedItem.TruckID = null;
                    voidedItem.UpdateTime = ticket.UpdateTime;
                    voidedItem.isManual = ticket.IsManual;
                    voidedItem.VoidReason = "Resequenced Tickets";
                }

                _log.Info($"Resequencing Tickets End");
            }

            if (ticket.TicketId.HasValue && ticket.TicketId != 0)
            {
                _log.Info($"  ---- Debug ----  Would have logged ticket 'Reprinted as # {ticket.TicketNumber}");
            }

                /*
                if (ticket.TicketId.HasValue && ticket.TicketId != 0)
                {
                    var toVoid = data.Tickets.FirstOrDefault(t => t.TicketID == ticket.TicketId.Value);
                    if (toVoid != null)
                    {
                        toVoid.isVoid = true;
                        toVoid.VoidReason = string.Format($"Reprinted as # {ticket.TicketNumber}");
                        toVoid.UpdateTime = DateTime.Now;
                    }
                }
                */
                //-- This increments the ticket number in table ticketProps
                currentTicketId.ValueNumber = ticket.TicketNumber + 1;
            
        }

        public int SaveCashTicket(ITicketData ticket, ICashPurchase cashPurchase)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                TicketSavePreprocessing(ticket, data);

                Ticket item = data.Tickets.Create();
                data.Tickets.Add(item);
                
                item.CompanyID = ticket.CompanyId;
                item.CreateTime = ticket.CreateTime;
                item.IsActive = ticket.IsActive;
                item.OrderId = ticket.OrderId;
                item.Quantity = ticket.Quantity;
                item.SeasonId = ticket.SeasonId;
                item.TicketNumber = ticket.TicketNumber;
                item.TruckID = ticket.TruckId;
                item.UpdateTime = ticket.UpdateTime;
                item.isManual = ticket.IsManual;
                item.FuelSurcharge = ticket.FuelSurcharge;

                if (ticket.CashData != null)
                {
                    item.CashPurchase =
                        data.CashPurchases.FirstOrDefault(t => t.cashPurchaseID == cashPurchase.CashPurchaseId) ??
                        new CashPurchase();

                    item.CashPurchase.companyID = cashPurchase.CompanyId;
                    item.CashPurchase.CreateTime = cashPurchase.CreateTime;
                    item.CashPurchase.IsActive = cashPurchase.IsActive;
                    item.CashPurchase.materialID = cashPurchase.MaterialId;
                    item.CashPurchase.SeasonId = cashPurchase.SeasonId;
                    item.CashPurchase.UpdateTime = ticket.UpdateTime;
                    item.CashPurchase.checkNumber = ticket.CashData.CheckNumber;
                    item.CashPurchase.locationID = cashPurchase.LocationId;
                    item.CashPurchase.paymentForm = ticket.CashData.PaymentForm;
                    item.CashPurchase.quote = ticket.CashData.Quote;
                    item.CashPurchase.tare = cashPurchase.Tare;
                    item.CashPurchase.taxExempt = ticket.CashData.TaxExempt;
                    item.CashPurchase.truckDescription = cashPurchase.TruckDescription;
                    item.CashPurchase.isComplete = true;
                    item.CashPurchase.TaxRate = ticket.CashData.TaxRate;
                }

                data.SaveChanges();
                

                return item.TicketID;
            }
        }

        private static string GetCustomerAddress(Address primaryAddress, string customerContactName)
        {
            string address = string.Empty;

            if (primaryAddress != null)
            {
                if (!string.IsNullOrWhiteSpace(customerContactName))
                    address = customerContactName.Trim() + Environment.NewLine;

                if (!string.IsNullOrWhiteSpace(primaryAddress.AddressLineOne))
                    address = address + primaryAddress.AddressLineOne.Trim() + Environment.NewLine;

                if (!string.IsNullOrWhiteSpace(primaryAddress.AddressLineTwo))
                    address = address + primaryAddress.AddressLineTwo.Trim() + Environment.NewLine;

                string[] addressInfo =
                {
                    primaryAddress.City,
                    primaryAddress.State,
                    primaryAddress.Zip
                };

                address = address +
                          string.Join(", ", addressInfo.Where(s => !string.IsNullOrWhiteSpace(s)).Select(s=>s.Trim()));

                primaryAddress.AddressLineOne = primaryAddress.AddressLineOne ?? string.Empty;
                primaryAddress.AddressLineTwo = primaryAddress.AddressLineTwo ?? string.Empty;
                primaryAddress.City = primaryAddress.City ?? string.Empty;
                primaryAddress.State = primaryAddress.State ?? string.Empty;
                primaryAddress.Zip = primaryAddress.Zip ?? string.Empty;
            }
            return address;
        }

        private static string GetSpecialInstruction(OrderTable order)
        {
            string instr = order.SpecialInstructionOverride;
            if (string.IsNullOrWhiteSpace(instr))
                instr = order.Bid.Customer.specialInstructions;
            return instr;
        }

        public List<ITicketExportData> GetDataForExport(string companyName, DateTime fromDate, DateTime toDate, TicketExportCondition exportCondition, string connectionString)
        {
            using (var data = new SaltScaleEntities(connectionString))
            {
                // 2018-Jul-22, JGS - Get the currently active season
                var currentSeason = data.Seasons.FirstOrDefault(s => s.IsActive == true);

                // 2018-Jul-22, JGS - Get the tickets for the current season that are in the date range
                var query = data.Tickets.AsNoTracking().Where(t => t.CreateTime>=fromDate && t.CreateTime<=toDate && t.SeasonId == currentSeason.SeasonId);
                //var query = from gp in data.GreatPlains.AsNoTracking()
                //            join c in data.Customers.AsNoTracking() on gp.GreatPlainID equals c.GPID into _AccountingCustomers
                //            from a in _AccountingCustomers.DefaultIfEmpty()
                            

                if (!string.IsNullOrWhiteSpace(companyName) && companyName != "All")
                    query = query.Where(q => q.Company.FullName == companyName);

                switch (exportCondition)
                {  
                    case TicketExportCondition.NotExported:
                        query = query.Where(q => !q.hasBeenExported.HasValue || !q.hasBeenExported.Value);
                        break;
                    case TicketExportCondition.Exported:
                        query = query.Where(q => q.hasBeenExported.HasValue && q.hasBeenExported.Value);
                        break;
                    case TicketExportCondition.ExportedAndVoided:
                        query = query.Where(q => (q.hasBeenExported.HasValue && q.hasBeenExported.Value) && q.isVoid);
                        break;
                }

                return query.Select(TicketExportData.Transform).ToList();
            }
        }

        public void MarkAsExported(IEnumerable<int> tickets , string connectionString)
        {
            using (var data = new SaltScaleEntities(connectionString))
            {
                // 2018-Jul-22, JGS - Get the currently active season
                var currentSeason = data.Seasons.FirstOrDefault(s => s.IsActive == true);

                // 2018-Jul-22, JGS - Loop through the tickets for the current season and set them to exported
                foreach (var ticket in tickets)
                {
                    var item = data.Tickets.FirstOrDefault(t => t.TicketNumber == ticket && t.SeasonId == currentSeason.SeasonId);
                    if (item != null)
                    {
                        item.hasBeenExported = true;
                        item.DateExported = DateTime.Now;
                    }
                }

                // Write the changes to each record in the database
                data.SaveChanges();
            }
        }

        public float GetTonsShippedToday(int seasonId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var query = data.Tickets
                    .Where(t => t.IsActive
                        && t.isVoid == false
                        && t.SeasonId == seasonId
                        && t.CreateTime >= DateTime.Today);//EF doesn't seem to have any way to pull out the Date part...

                return query.Select(TicketInfo.Transform).ToList().Sum(x => x.Tons);
            }
        }
    }

    #region DTO

    internal class TicketExportData : ITicketExportData
    {
        public static string GetAccountingValuesOfCashTickets(int companyId, int siteId)
        {   /*
            
            CASH ACCOUNTS:
             2    EM = CASHAT01
             3    ES = CASHAT01
             10   OP = CASHAT01
             1    AS SI = CASHSA01
             1    AS NJ = CASHSA02

            */

            var result = string.Empty;
            switch(siteId)
            {
                //-- Atlantic Salt
                case 1:
                    switch (siteId)
                    {
                        //-- SI
                        case 8:
                            result = "CASHSA01";
                            break;

                        //-- Newark
                        case 4:
                            result = "CASHSA02";
                            break;
                    }
                    
                    break;

                //-- Eastern Minerals, Inc.
                case 2:

                //-- Eastern Salt Co. Inc.
                case 3:
                
                //-- Granite State
                case 8:
                
                //-- Oceanport
                case 10:
                    result = "CASHAT01";
                    break;
            }

            return result;
        }

        public static readonly Expression<Func<Ticket, ITicketExportData>> Transform = e => new TicketExportData
        {
            Exported = e.hasBeenExported != null && e.hasBeenExported.Value,
            DateExported = e.DateExported,
            Voided = e.isVoid,
            VoidDate = e.isVoid ? e.UpdateTime : null,
            VoidReason = e.VoidReason,
            TicketNumber = e.TicketNumber,
            SeasonPrefix = e.Season.Prefix,
            PurchaseOrder = e.CashPurchaseId == null && e.OrderTable.POOverride != null && e.OrderTable.POOverride != string.Empty ? e.OrderTable.POOverride : "NONE",
            ShipDate = e.CreateTime,
            Company = e.Company.FullName.Replace(", Inc.", "").Replace(" Co. Inc.", "").Replace(", LLC", ""),
            CustomerId = e.OrderTable != null ? e.OrderTable.Bid.Customer.CustomerID : (int?)null,
            CustomerShortName = e.isVoid ? "VOID1" : e.OrderId != null ? e.OrderTable.Bid.Customer.ShortName
                                                        : "CASH",

            AccountingCustomerId = e.OrderId != null ? e.OrderTable.Bid.Customer.GPID
                                                        : 0,

            //AccountingCustomerCode = e.isVoid ? "VOID1" : e.CashPurchaseId == null ? e.OrderTable.Bid.Customer.GreatPlain.CustomerCode
            //: GetAccountingValuesOfCashTickets(e.CompanyID.GetValueOrDefault(0), e.CashPurchase.locationID.GetValueOrDefault(0)),

            /*
                        AccountingCustomerCode = e.isVoid ? "VOID1" :
                                                            e.OrderId != null ? e.OrderTable.Bid.Customer.GreatPlain.CustomerCode
                                                                                      : e.CompanyID == 1 && e.CashPurchase.locationID == 8 ? "CASHSA01"
                                                                                                        : e.CompanyID == 2 && e.CashPurchase.locationID == 4 ? "CASHSA02"
                                                                                                : e.CompanyID == 2 || e.CompanyID == 3 || e.CompanyID == 10 ? "CASHAT01" : e.CompanyID == 8 ? "CASHGS01" : string.Empty,
             */
            AccountingCustomerCode = e.isVoid ? "VOID1" :
                                                e.OrderId != null ? e.OrderTable.Bid.Customer.GreatPlain.CustomerCode
                                                                            : e.CompanyID == 1 && e.CashPurchase.locationID == 8 ? "CASHSA01"
                                                                            : e.CompanyID == 1 && e.CashPurchase.locationID == 4 ? "CASHSA02"
                                                                            : e.CompanyID == 2 || e.CompanyID == 3 || e.CompanyID == 8 || e.CompanyID == 10 ? "CASHAT01" 
                                                                            : string.Empty,
            //, e.CashPurchase.locationID.GetValueOrDefault(0)),

            Quantity = e.Quantity / 2000,
            IsManual = e.isManual,
            Material = e.OrderTable != null ? e.OrderTable.Material.ShortName : null,
            CashMaterial = e.CashPurchase != null ? e.CashPurchase.Material.ShortName : null,
            TruckId = e.Truck != null ? e.Truck.ShortName : null,
            TruckDescription = e.CashPurchase != null ? e.CashPurchase.truckDescription : null,
            TruckingCost = e.OrderTable != null ? e.OrderTable.Bid.TruckingRate : null,
            CashTicket = e.CashPurchase != null,
            TaxRate = e.CashPurchase != null ? e.CashPurchase.TaxRate : (decimal?)null,
            TaxExempt = e.CashPurchase != null ? e.CashPurchase.taxExempt : false,
            Quote = e.CashPurchase != null ? e.CashPurchase.quote : (decimal?)null
        };

        public string TruckDescription { get; set; }

        public string CashMaterial { get; set; }

        public string CustomerShortName { get; set; }           

        public decimal? MaterialCost
        {
            get
            {
                if (CashTicket && Quote.HasValue)
                {
                    return Quote.Value*(decimal) (Quantity/2000);
                }
                return 0;
            }
        } 
        
        public decimal? SalesTax
        {
            get
            {
                if (CashTicket && !TaxExempt && TaxRate.HasValue && MaterialCost.HasValue)
                {
                    return MaterialCost.Value * TaxRate.Value;
                }
                return 0;
            }
        }

        public decimal? Quote { get; set; }

        public bool TaxExempt { get; set; }

        public decimal? TaxRate { get; set; }

        public bool CashTicket { get; set; }

        public decimal? TruckingCost { get; set; }

        public string TruckId { get; set; }

        public string Location { get; set; }

        public string Material { get; set; }

        public bool IsManual { get; set; }

        public float Quantity { get; set; }

        public int? AccountingCustomerId { get; set; }
        public string AccountingCustomerCode { get; set; }

        public int? CustomerId { get; set; }

        public string Company { get; set; }

        public DateTime ShipDate { get; set; }

        public string PurchaseOrder { get; set; }

        public int TicketNumber { get; set; }
        public string SeasonPrefix { get; set; }

        public string VoidReason { get; set; }

        public DateTime? VoidDate { get; set; }

        public bool Voided { get; set; }

        public DateTime? DateExported { get; set; }

        public bool? Exported { get; set; }

        public TicketExportData()
        {
            
        }
    }

    internal class TicketInfo : ITicketInfo
    {
        public static readonly Expression<Func<Ticket, ITicketInfo>> Transform = e => new TicketInfo
        {
            Id = e.TicketID,
            CreateTime = e.CreateTime,
            OrderNumber = e.OrderTable.OrderNumber,
            TicketNumber = e.TicketNumber,
            TruckId = e.Truck.ShortName,
            OrderId = e.OrderId,
            CustomerId = e.OrderTable == null ? 0 : e.OrderTable.Bid.CustomerID,
            Tons = e.Quantity/2000,
            VoidReason = e.VoidReason,
            IsVoid = e.isVoid,
            SeasonId = e.SeasonId,
            ConfirmationNumber = e.OrderTable.ConfirmationNumber
        };

        public TicketInfo()
        {
        }

        public TicketInfo(Ticket ticket)
        {
            Id = ticket.TicketID;
            CreateTime = ticket.CreateTime;
            OrderNumber = ticket.OrderTable.OrderNumber;
            TicketNumber = ticket.TicketNumber;
            TruckId = ticket.Truck.ShortName;
            OrderId = ticket.OrderId;
            CustomerId = ticket.OrderTable.Bid.CustomerID;
            Tons = ticket.Quantity/2000;
            VoidReason = ticket.VoidReason;
            IsVoid = ticket.isVoid;
            SeasonId = ticket.SeasonId;
        }

        public int SeasonId { get; set; }

        public int Id { get; set; }

        public int TicketNumber { get; set; }
        
        public DateTime CreateTime { get; set; }
        
        public float Tons { get; set; }
        
        public string TruckId { get; set; }
        
        public int? OrderNumber { get; set; }

        public int CustomerId { get; set; }

        public int? OrderId { get; set; }

        public int PurchaseOrderId { get; set; }

        public string VoidReason { get; set; }

        public bool IsVoid { get; set; }

        public int TicketCountPerDay { get; set; }

        public int? ConfirmationNumber { get; set; }
    }

    internal class CashTicketInfo : ICashTicketInfo
    {
        public static readonly Expression<Func<Ticket, ICashTicketInfo>> Transform = e => new CashTicketInfo
        {
            Id = e.TicketID,
            CreateTime = e.CreateTime,
            TicketNumber = e.TicketNumber,
            TruckDescription = e.CashPurchase.truckDescription,
            Tons = e.Quantity/2000,
            SeasonId = e.SeasonId,
            IsVoid = e.isVoid,
            VoidReason = e.VoidReason
        };

        public CashTicketInfo()
        {
        }

        public CashTicketInfo(Ticket ticket)
        {
            Id = ticket.TicketID;
            CreateTime = ticket.CreateTime;
            TicketNumber = ticket.TicketNumber;
            TruckDescription = ticket.CashPurchase.truckDescription;
            Tons = ticket.Quantity/2000;
            SeasonId = ticket.SeasonId;
            IsVoid = ticket.isVoid;
            VoidReason = ticket.VoidReason;
        }

        public int SeasonId { get; set; }

        public int Id { get; set; }

        public int TicketNumber { get; set; }
        
        public DateTime CreateTime { get; set; }
        
        public float Tons { get; set; }
        
        public string TruckDescription { get; set; }
        public string VoidReason { get; set; }
        public bool IsVoid { get; set; }
    }
    
    internal class TicketData : ITicketData
    {
        public TicketData()
        {
        }

        public TicketData(Ticket cashTicket)
        {
            TicketId = cashTicket.TicketID;
            TicketNumber = cashTicket.TicketNumber;
            Quantity = cashTicket.Quantity;
            IsManual = cashTicket.isManual;
            TruckId = cashTicket.TruckID;
            IsVoid = cashTicket.isVoid;
            VoidReason = cashTicket.VoidReason;
            IsReconciled = cashTicket.isReconciled;
            HasBeenExported = cashTicket.hasBeenExported;
            CompanyId = cashTicket.CompanyID;
            CreateTime = cashTicket.CreateTime;
            UpdateTime = cashTicket.UpdateTime;
            IsActive = cashTicket.IsActive;
            OrderId = cashTicket.OrderId;
            Comment = cashTicket.Comment;
            DataA = cashTicket.DataA;
            DataB = cashTicket.DataB;
            DataC = cashTicket.DataC;
        }

        public string SpecialInstruction { get; set; }
        public string CustomerAddress { get; set; }
        public string CustomerOrganization { get; set; }
        public string CustomerName { get; set; }
        public int? TicketId { get; set; }
        public int TicketNumber { get; set; }
        public float Quantity { get; set; }
        public bool IsManual { get; set; }
        public int? TruckId { get; set; }
        public bool IsVoid { get; set; }
        public string VoidReason { get; set; }
        public bool? IsReconciled { get; set; }
        public bool? HasBeenExported { get; set; }
        public int? CompanyId { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        public bool IsActive { get; set; }
        public int? OrderId { get; set; }
        public string Comment { get; set; }
        public string DataA { get; set; }
        public string DataB { get; set; }
        public string DataC { get; set; }
        public int SeasonId { get; set; }
        public int TruckWeight { get; set; }
        public string TruckShortName { get; set; }
        public string TruckTag { get; set; }
        public string TruckSticker { get; set; }
        public string TruckDescription { get; set; }
        public ICashPurchaseData CashData { get; set; }
        public int MaterialId { get; set; }
        public IAddressData Address  { get; set; }
        public string PO { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerShortName { get; set; }
        public decimal? TruckRate { get; set; }
        public int? OrderNumber { get; set; }
        public bool HideDollarAmount { get; set; }
        public bool IsTareExpired { get; set; }
        public decimal FuelSurcharge { get; set; }
        public int? ConfirmationNumber { get; set; }
        public string CustomerContactName { get; set; }
    }

    internal class CashPurchaseData : ICashPurchaseData
    {
        public int? CashPurchaseId { get; set; }
        public string TruckDescription { get; set; }
        public int? MaterialId { get; set; }
        public decimal? Quote { get; set; }
        public int? CompanyId { get; set; }
        public int? LocationId { get; set; }
        public int? Tare { get; set; }
        public string PaymentForm { get; set; }
        public string CheckNumber { get; set; }
        public bool TaxExempt { get; set; }
        public bool IsComplete { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        public bool IsActive { get; set; }
        public string Comment { get; set; }
        public string DataA { get; set; }
        public string DataB { get; set; }
        public string DataC { get; set; }
        public int SeasonId { get; set; }
        public decimal? TaxRate { get; set; }
    }

    #endregion
}