﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Contract.Domain;
using Contract.Exception;
using Contract.Services;
using DataAccess;

namespace BusinessLogic
{
    public class MaterialService : IMaterialService
    {
        private readonly IConfiguration _configuration;

        public MaterialService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public List<IMaterialInfo> GetMaterials(int seasonId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                return data.Materials.AsNoTracking()
                        .Where(m => m.IsActive && m.SeasonId == seasonId)
                        .Select(MaterialInfo.Transform)
                        .OrderBy(m => m.Name).ToList();
            }
        }

        public List<IMaterialInfo> GetMaterialsOfCustomer(int seasonId, int customerId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var x = from b in data.Bids.AsNoTracking()
                        where (b.MaterialID == b.Material.MaterialID)
                        && (b.CustomerID == customerId)
                        && (b.SeasonId == seasonId)
                        orderby b.Material.ShortName
                        select new MaterialInfo()
                        {
                            Id = b.MaterialID,
                            Name = b.Material.ShortName,
                            Description = b.Material.Description,
                            PricePerTon = b.Material.PricePerTon,
                            PriceEach = b.Material.PriceEach 
                        };

                return new List<IMaterialInfo>(x);
            }
        }

        public IReadOnlyList<IMaterialData> GetMaterialList(int seasonId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                return
                    data.Materials.AsNoTracking()
                        .Where(m => m.IsActive && m.SeasonId == seasonId)
                        .Select(MaterialData.Transform)
                        .OrderBy(m => m.Name)
                        .ToList();
            }
        }

        public IMaterialData GetMaterial(int seasonId, int? materialId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                return data.Materials.AsNoTracking()
                    .Where(m => m.IsActive && m.SeasonId == seasonId && m.MaterialID == materialId)
                    .Select(MaterialData.Transform).FirstOrDefault();
            }
        }

        public IMaterialInfo GetMaterial(int seasonId, int materialId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                return data.Materials.AsNoTracking()
                    .Where(m => m.IsActive && m.SeasonId == seasonId && m.MaterialID == materialId)
                    .Select(MaterialInfo.Transform).FirstOrDefault();
            }
        }

        public IMaterialInfo GetInactiveMaterial(int? materialId)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var material = data.Materials.AsNoTracking().Where(m => m.MaterialID == materialId)
                    .Select(MaterialInfo.Transform).FirstOrDefault();
                if (material != null)
                    material.Description = string.Format("{0} - deleted", material.Description);
                return material;
            }
        }

        public void DeleteMaterial(IMaterialData material)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                var result = data.Materials.FirstOrDefault(m => m.MaterialID == material.Id);

                if (result == null) return;

                DateTime updateTime = material.UpdateTime.HasValue
                    ? material.UpdateTime.Value
                    : DateTime.Now;
                result.IsActive = false;
                result.UpdateTime = updateTime;
                data.SaveChanges();
            }
        }

        public int SaveMaterial(IMaterialData material)
        {
            using (var data = new SaltScaleEntities(_configuration.ConnectionString))
            {
                if (data.Materials.Any(m => m.MaterialID != material.Id && m.IsActive && m.ShortName == material.Name && m.SeasonId == material.SeasonId))
                    throw new UniqueConstraintException("Material Id must be unique.");

                Material result;
                if (material.Id.HasValue)
                {
                    result = data.Materials.First(m => m.MaterialID == material.Id);
                }
                else
                {
                    result = data.Materials.Create();
                    data.Materials.Add(result);
                }

                DateTime updateTime = material.UpdateTime.HasValue
                    ? material.UpdateTime.Value
                    : DateTime.Now;

                result.ShortName = material.Name;
                result.Description = material.Description;
                result.PricePerTon = material.PricePerTon;
                result.PricePerYard = material.PricePerYard;
                result.PriceEach = material.PriceEach;
                result.MinimumCharge = material.MinimumCharge;
                result.CreateTime = material.CreateTime;
                result.UpdateTime = updateTime;
                result.IsActive = material.IsActive;
                result.SeasonId = material.SeasonId;

                data.SaveChanges();

                return result.MaterialID;
            }
        }
    }

    #region DTO

    internal class MaterialInfo : IMaterialInfo
    {
        public static readonly Expression<Func<Material, IMaterialInfo>> Transform = e => new MaterialInfo
        {
            Id = e.MaterialID,
            Name = e.ShortName,
            Description = e.Description,
            PricePerTon = e.PricePerTon,
            PriceEach = e.PriceEach 
        };

        public MaterialInfo()
        {
        }

        public MaterialInfo(Material material)
        {
            Id = material.MaterialID;
            Name = material.ShortName;
            Description = material.Description;
            PricePerTon = material.PricePerTon;
            PriceEach = material.PriceEach;
        }


        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal? PricePerTon { get; set; }
        public decimal? PriceEach { get; set; }
    }
    
    internal class MaterialData : IMaterialData
    {
        public static readonly Expression<Func<Material, IMaterialData>> Transform = e => new MaterialData
        {
            Id = e.MaterialID,
            Name = e.ShortName,
            Description = e.Description,
            PricePerTon = e.PricePerTon,
            PricePerYard = e.PricePerYard,
            PriceEach = e.PriceEach,
            MinimumCharge = e.MinimumCharge,
            CreateTime = e.CreateTime,
            UpdateTime = e.UpdateTime,
            IsActive = e.IsActive,
            SeasonId = e.SeasonId
        };

        public MaterialData()
        {
        }

        public MaterialData(Material material)
        {
            Id = material.MaterialID;
            Name = material.ShortName;
            Description = material.Description;
            PricePerTon = material.PricePerTon;
            PricePerYard = material.PricePerYard;
            PriceEach = material.PriceEach;
            MinimumCharge = material.MinimumCharge;
            CreateTime = material.CreateTime;
            UpdateTime = material.UpdateTime;
            IsActive = material.IsActive;
            SeasonId = material.SeasonId;
        }

        public int? Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal? PricePerTon { get; set; }
        public decimal? PricePerYard { get; set; }
        public decimal? PriceEach { get; set; }
        public int? MinimumCharge { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        public bool IsActive { get; set; }
        public int SeasonId { get; set; }
    }

    #endregion
}
