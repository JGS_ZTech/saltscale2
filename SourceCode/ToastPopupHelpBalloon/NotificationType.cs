﻿namespace ZTech.Controls
{
    public enum NotificationType
    {
        Information = 0,

        Warning = 1,

        Error = 2
    }
}
