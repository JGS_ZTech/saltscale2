﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using Application = System.Windows.Application;
using Brush = System.Windows.Media.Brush;

namespace ZTech.Controls
{
    public partial class ToastPopUp: IDisposable
    {
        #region Members

        private readonly string _name = typeof (ToastPopUp).Name;
        
        #endregion Members

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="ToastPopUp" /> class.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="text">The text.</param>
        /// <param name="notificationType">Type of the notification.</param>
        public ToastPopUp(string title, string text, NotificationType notificationType)
            : this(title, notificationType)
        {
            TextBoxShortDescription.Text = text;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ToastPopUp" /> class.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="text">The text.</param>
        /// <param name="hyperlinkText">The hyperlink text.</param>
        /// <param name="notificationType">Type of the notification.</param>
        /// <param name="hyperlinkObjectForRaisedEvent">The hyperlink object for raised event.</param>
        public ToastPopUp(string title, string text, string hyperlinkText, NotificationType notificationType,
            object hyperlinkObjectForRaisedEvent = null)
            : this(title, text, notificationType)
        {
            HyperlinkObjectForRaisedEvent = hyperlinkObjectForRaisedEvent;
            SetHyperLinkButton(hyperlinkText);
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ToastPopUp" /> class.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="textInlines">The inlines.</param>
        /// <param name="hyperlinkText">The hyperlink text.</param>
        /// <param name="imageSource">The image source.</param>
        /// <param name="hyperlinkObjectForRaisedEvent">The hyperlink object for raised event.</param>
        public ToastPopUp(string title, List<Inline> textInlines, string hyperlinkText, ImageSource imageSource,
            object hyperlinkObjectForRaisedEvent = null)
            : this(title)
        {
            imageLeft.Source = imageSource;
            TextBoxShortDescription.Inlines.AddRange(textInlines);
            SetHyperLinkButton(hyperlinkText);
            HyperlinkObjectForRaisedEvent = hyperlinkObjectForRaisedEvent;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ToastPopUp" /> class.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="textInlines">The inlines.</param>
        /// <param name="hyperlinkText">The hyperlink text.</param>
        /// <param name="imageSource">The image source.</param>
        /// <param name="hyperlinkObjectForRaisedEvent">The hyperlink object for raised event.</param>
        public ToastPopUp(string title, List<Inline> textInlines, string hyperlinkText, Bitmap imageSource,
            object hyperlinkObjectForRaisedEvent = null)
            : this(title, textInlines, hyperlinkText, imageSource.ToBitmapImage(), hyperlinkObjectForRaisedEvent)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ToastPopUp" /> class.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="textInlines">The text inlines.</param>
        /// <param name="hyperlinkText">The hyperlink text.</param>
        /// <param name="notificationType">Type of the notification.</param>
        /// <param name="hyperlinkObjectForRaisedEvent">The hyperlink object for raised event.</param>
        public ToastPopUp(string title, List<Inline> textInlines, string hyperlinkText,
            NotificationType notificationType, object hyperlinkObjectForRaisedEvent = null)
            : this(title, notificationType)
        {
            HyperlinkObjectForRaisedEvent = hyperlinkObjectForRaisedEvent;
            TextBoxShortDescription.Inlines.AddRange(textInlines);
            SetHyperLinkButton(hyperlinkText);
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ToastPopUp" /> class.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="text">The text.</param>
        /// <param name="hyperlinkText">The hyperlink text.</param>
        /// <param name="imageSource">The image source.</param>
        /// <param name="hyperlinkObjectForRaisedEvent">The hyperlink object for raised event.</param>
        public ToastPopUp(string title, string text, string hyperlinkText, Bitmap imageSource,
            object hyperlinkObjectForRaisedEvent = null)
            : this(title)
        {
            TextBoxShortDescription.Text = text;
            SetHyperLinkButton(hyperlinkText);
            imageLeft.Source = imageSource.ToBitmapImage();
            HyperlinkObjectForRaisedEvent = hyperlinkObjectForRaisedEvent;
        }


        /// <summary>
        ///     Initializes a new instance of the <see cref="ToastPopUp" /> class.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="text">The text.</param>
        /// <param name="imageSource">The image source.</param>
        public ToastPopUp(string title, string text, ImageSource imageSource)
            : this(title)
        {
            TextBoxShortDescription.Text = text;
            imageLeft.Source = imageSource;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ToastPopUp" /> class.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="text">The text.</param>
        /// <param name="imageSource">The image source.</param>
        public ToastPopUp(string title, string text, Bitmap imageSource)
            : this(title, text, imageSource.ToBitmapImage())
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ToastPopUp" /> class.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="text">The text.</param>
        /// <param name="hyperlinkText">The hyperlink text.</param>
        /// <param name="imageSource">The image source.</param>
        /// <param name="hyperlinkObjectForRaisedEvent">The hyperlink object for raised event.</param>
        public ToastPopUp(string title, string text, string hyperlinkText, ImageSource imageSource,
            object hyperlinkObjectForRaisedEvent = null)
            : this(title)
        {
            TextBoxShortDescription.Text = text;
            HyperlinkObjectForRaisedEvent = hyperlinkObjectForRaisedEvent;
            SetHyperLinkButton(hyperlinkText);
            imageLeft.Source = imageSource;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ToastPopUp" /> class.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="notificationType">Type of the notification.</param>
        /// <exception cref="System.ArgumentOutOfRangeException">notificationType</exception>
        private ToastPopUp(string title, NotificationType notificationType)
            : this(title)
        {
            switch (notificationType)
            {
                case NotificationType.Error:
                    imageLeft.Source = Properties.Resources.Error.ToBitmapImage();
                    break;

                case NotificationType.Information:
                    imageLeft.Source = Properties.Resources.Information.ToBitmapImage();
                    break;

                case NotificationType.Warning:
                    imageLeft.Source = Properties.Resources.Warning.ToBitmapImage();
                    break;

                default:
                    throw new ArgumentOutOfRangeException("notificationType");
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ToastPopUp" /> class.
        /// </summary>
        /// <param name="title">The title.</param>
        private ToastPopUp(string title)
        {
            InitializeComponent();
            Application.Current.MainWindow.Closing += MainWindowClosing;

            TextBoxTitle.Text = title;
        }

        #endregion Constructors

        #region Public Properties

        /// <summary>
        ///     Gets or sets the color of the font.
        /// </summary>
        /// <value>
        ///     The color of the font.
        /// </value>
        public Brush FontColor
        {
            get { return TextBoxTitle.Foreground; }

            set
            {
                TextBoxTitle.Foreground = value;
                TextBoxShortDescription.Foreground = value;
            }
        }

        /// <summary>
        ///     Gets or sets a brush that describes the border background of a control.
        /// </summary>
        public new Brush BorderBrush
        {
            get { return borderBackground.BorderBrush; }

            set { borderBackground.BorderBrush = value; }
        }

        /// <summary>
        ///     Gets or sets a brush that describes the background of a control.
        /// </summary>
        public new Brush Background
        {
            get { return borderBackground.Background; }

            set { borderBackground.Background = value; }
        }

        /// <summary>
        ///     Gets or sets the hyperlink object for raised event.  This object will be passed back when
        ///     the control raises the HyperlinkClicked event.
        /// </summary>
        /// <value>
        ///     The hyperlink object for raised event.
        /// </value>
        public object HyperlinkObjectForRaisedEvent { get; set; }

        #endregion Public Properties

        #region Events

        /// <summary>
        ///     Occurs when [closed by user].
        /// </summary>
        public event EventHandler<EventArgs> ClosedByUser;

        /// <summary>
        ///     Occurs when [hyperlink clicked].
        /// </summary>
        public event EventHandler<HyperLinkEventArgs> HyperlinkClicked;

        #endregion Events

        #region Public Methods

        /// <summary>
        ///     Opens a window and returns without waiting for the newly opened window to close.
        /// </summary>
        public new void Show()
        {
            IInputElement focusedElement = Keyboard.FocusedElement;

            Topmost = true;

            foreach (Window window in Application.Current.Windows)
            {
                string windowName = window.GetType().Name;

                if (
                    !windowName.Equals(_name) 
                    && !windowName.Equals(typeof (Balloon).Name) 
                    && !windowName.Equals(typeof (HelpBalloon).Name)
                    && !windowName.Equals(typeof (ToastNotification).Name)
                    )
                {
                    Owner = window;
                }
            }

            WindowStartupLocation = WindowStartupLocation.CenterOwner;

            base.ShowDialog();

            if (focusedElement != null)
            {
                // Restore keyboard focus to the original element that had focus. That way if someone
                // was typing into a control we don't steal keyboard focus away from that control.
                focusedElement.Focusable = true;
                Keyboard.Focus(focusedElement);
            }
        }

        /// <summary>
        ///     Shows the dialog.
        /// </summary>
        /// <exception cref="System.NotImplementedException">ShowDialog() is not supported.  Use Show() instead.</exception>
        public new void ShowDialog()
        {
            Show();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        ///     Raises the <see cref="E:ClosedByUser" /> event.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        protected virtual void OnClosedByUser(EventArgs e)
        {
            if (ClosedByUser != null)
            {
                ClosedByUser(this, e);
            }
        }

        /// <summary>
        ///     Raises the <see cref="E:HyperlinkClicked" /> event.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        protected virtual void OnHyperlinkClicked(HyperLinkEventArgs e)
        {
            if (HyperlinkClicked != null)
            {
                HyperlinkClicked(this, e);
            }
        }

        /// <summary>
        ///     Buttons the view click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs" /> instance containing the event data.</param>
        private void ButtonViewClick(object sender, RoutedEventArgs e)
        {
            OnHyperlinkClicked(new HyperLinkEventArgs {HyperlinkObjectForRaisedEvent = HyperlinkObjectForRaisedEvent});
        }

        /// <summary>
        ///     Doubles the animation completed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void DoubleAnimationCompleted(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        ///     Images the mouse up.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="MouseButtonEventArgs" /> instance containing the event data.</param>
        private void ImageMouseUp(object sender, MouseButtonEventArgs e)
        {
            OnClosedByUser(new EventArgs());
            Close();
        }

        /// <summary>
        ///     Mains the window closing.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.ComponentModel.CancelEventArgs" /> instance containing the event data.</param>
        private void MainWindowClosing(object sender, CancelEventArgs e)
        {
            CloseAll();
        }

        private void CloseAll()
        {
            foreach (Window window in Application.Current.Windows)
            {
                string windowType = window.GetType().Name;
                if (windowType.Equals(_name))
                {
                    window.Close();
                }
            }
        }

        #endregion Event Handlers

        #region Private Methods
       /// <summary>
        ///     Sets the hyperlink button.
        /// </summary>
        /// <param name="hyperlinkText">The hyperlink text.</param>
        private void SetHyperLinkButton(string hyperlinkText)
        {
            if (!string.IsNullOrWhiteSpace(hyperlinkText))
            {
                buttonView.Content = hyperlinkText;
                buttonView.Visibility = Visibility.Visible;
            }
        }

        #endregion Private Methods

        public void Dispose()
        {
            CloseAll();
        }
    }
}