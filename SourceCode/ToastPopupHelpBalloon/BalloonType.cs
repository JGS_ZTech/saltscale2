﻿namespace ZTech.Controls
{
    public enum BalloonType
    {
        Help = 0,

        Information = 1,

        Warning = 2
    }
}
