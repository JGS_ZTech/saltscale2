﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using Application = System.Windows.Application;
using Brush = System.Windows.Media.Brush;

namespace ZTech.Controls
{
    public partial class ToastNotification : Window
    {
        #region Members

        private readonly string _name = typeof (ToastNotification).Name;
        private volatile object _lockObject = new object();
        private string _title;

        #endregion Members

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="ToastNotification" /> class.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="text">The text.</param>
        /// <param name="notificationType">Type of the notification.</param>
        public ToastNotification(string title, string text, NotificationType notificationType)
            : this(title, notificationType)
        {
            TextBoxShortDescription.Text = text;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ToastNotification" /> class.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="text">The text.</param>
        /// <param name="hyperlinkText">The hyperlink text.</param>
        /// <param name="notificationType">Type of the notification.</param>
        /// <param name="hyperlinkObjectForRaisedEvent">The hyperlink object for raised event.</param>
        public ToastNotification(string title, string text, string hyperlinkText, NotificationType notificationType,
            object hyperlinkObjectForRaisedEvent = null)
            : this(title, text, notificationType)
        {
            HyperlinkObjectForRaisedEvent = hyperlinkObjectForRaisedEvent;
            SetHyperLinkButton(hyperlinkText);
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ToastNotification" /> class.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="textInlines">The inlines.</param>
        /// <param name="hyperlinkText">The hyperlink text.</param>
        /// <param name="imageSource">The image source.</param>
        /// <param name="hyperlinkObjectForRaisedEvent">The hyperlink object for raised event.</param>
        public ToastNotification(string title, List<Inline> textInlines, string hyperlinkText, ImageSource imageSource,
            object hyperlinkObjectForRaisedEvent = null)
            : this(title)
        {
            imageLeft.Source = imageSource;
            TextBoxShortDescription.Inlines.AddRange(textInlines);
            SetHyperLinkButton(hyperlinkText);
            HyperlinkObjectForRaisedEvent = hyperlinkObjectForRaisedEvent;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ToastNotification" /> class.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="textInlines">The inlines.</param>
        /// <param name="hyperlinkText">The hyperlink text.</param>
        /// <param name="imageSource">The image source.</param>
        /// <param name="hyperlinkObjectForRaisedEvent">The hyperlink object for raised event.</param>
        public ToastNotification(string title, List<Inline> textInlines, string hyperlinkText, Bitmap imageSource,
            object hyperlinkObjectForRaisedEvent = null)
            : this(title, textInlines, hyperlinkText, imageSource.ToBitmapImage(), hyperlinkObjectForRaisedEvent)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ToastNotification" /> class.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="textInlines">The text inlines.</param>
        /// <param name="hyperlinkText">The hyperlink text.</param>
        /// <param name="notificationType">Type of the notification.</param>
        /// <param name="hyperlinkObjectForRaisedEvent">The hyperlink object for raised event.</param>
        public ToastNotification(string title, List<Inline> textInlines, string hyperlinkText,
            NotificationType notificationType, object hyperlinkObjectForRaisedEvent = null)
            : this(title, notificationType)
        {
            HyperlinkObjectForRaisedEvent = hyperlinkObjectForRaisedEvent;
            TextBoxShortDescription.Inlines.AddRange(textInlines);
            SetHyperLinkButton(hyperlinkText);
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ToastNotification" /> class.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="text">The text.</param>
        /// <param name="hyperlinkText">The hyperlink text.</param>
        /// <param name="imageSource">The image source.</param>
        /// <param name="hyperlinkObjectForRaisedEvent">The hyperlink object for raised event.</param>
        public ToastNotification(string title, string text, string hyperlinkText, Bitmap imageSource,
            object hyperlinkObjectForRaisedEvent = null)
            : this(title)
        {
            TextBoxShortDescription.Text = text;
            SetHyperLinkButton(hyperlinkText);
            imageLeft.Source = imageSource.ToBitmapImage();
            HyperlinkObjectForRaisedEvent = hyperlinkObjectForRaisedEvent;
        }


        /// <summary>
        ///     Initializes a new instance of the <see cref="ToastNotification" /> class.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="text">The text.</param>
        /// <param name="imageSource">The image source.</param>
        public ToastNotification(string title, string text, ImageSource imageSource)
            : this(title)
        {
            TextBoxShortDescription.Text = text;
            imageLeft.Source = imageSource;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ToastNotification" /> class.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="text">The text.</param>
        /// <param name="imageSource">The image source.</param>
        public ToastNotification(string title, string text, Bitmap imageSource)
            : this(title, text, imageSource.ToBitmapImage())
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ToastNotification" /> class.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="text">The text.</param>
        /// <param name="hyperlinkText">The hyperlink text.</param>
        /// <param name="imageSource">The image source.</param>
        /// <param name="hyperlinkObjectForRaisedEvent">The hyperlink object for raised event.</param>
        public ToastNotification(string title, string text, string hyperlinkText, ImageSource imageSource,
            object hyperlinkObjectForRaisedEvent = null)
            : this(title)
        {
            TextBoxShortDescription.Text = text;
            HyperlinkObjectForRaisedEvent = hyperlinkObjectForRaisedEvent;
            SetHyperLinkButton(hyperlinkText);
            imageLeft.Source = imageSource;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ToastNotification" /> class.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="notificationType">Type of the notification.</param>
        /// <exception cref="System.ArgumentOutOfRangeException">notificationType</exception>
        private ToastNotification(string title, NotificationType notificationType)
            : this(title)
        {
            switch (notificationType)
            {
                case NotificationType.Error:
                    imageLeft.Source = Properties.Resources.Error.ToBitmapImage();
                    break;

                case NotificationType.Information:
                    imageLeft.Source = Properties.Resources.Information.ToBitmapImage();
                    break;

                case NotificationType.Warning:
                    imageLeft.Source = Properties.Resources.Warning.ToBitmapImage();
                    break;

                default:
                    throw new ArgumentOutOfRangeException("notificationType");
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ToastNotification" /> class.
        /// </summary>
        /// <param name="title">The title.</param>
        private ToastNotification(string title)
        {
            InitializeComponent();
            Application.Current.MainWindow.Closing += MainWindowClosing;

            TextBoxTitle.Text = title;
            this._title = title;
        }

        #endregion Constructors

        #region Public Properties

        /// <summary>
        ///     Gets or sets the color of the font.
        /// </summary>
        /// <value>
        ///     The color of the font.
        /// </value>
        public Brush FontColor
        {
            get { return TextBoxTitle.Foreground; }

            set
            {
                TextBoxTitle.Foreground = value;
                TextBoxShortDescription.Foreground = value;
            }
        }

        /// <summary>
        ///     Gets or sets a brush that describes the border background of a control.
        /// </summary>
        public new Brush BorderBrush
        {
            get { return borderBackground.BorderBrush; }

            set { borderBackground.BorderBrush = value; }
        }

        /// <summary>
        ///     Gets or sets a brush that describes the background of a control.
        /// </summary>
        public new Brush Background
        {
            get { return borderBackground.Background; }

            set { borderBackground.Background = value; }
        }

        /// <summary>
        ///     Gets or sets the hyperlink object for raised event.  This object will be passed back when
        ///     the control raises the HyperlinkClicked event.
        /// </summary>
        /// <value>
        ///     The hyperlink object for raised event.
        /// </value>
        public object HyperlinkObjectForRaisedEvent { get; set; }

        #endregion Public Properties

        #region Events

        /// <summary>
        ///     Occurs when [closed by user].
        /// </summary>
        public event EventHandler<EventArgs> ClosedByUser;

        /// <summary>
        ///     Occurs when [hyperlink clicked].
        /// </summary>
        public event EventHandler<HyperLinkEventArgs> HyperlinkClicked;

        #endregion Events

        #region Public Methods

        /// <summary>
        ///     Opens a window and returns without waiting for the newly opened window to close.
        /// </summary>
        public new void Show()
        {
            IInputElement focusedElement = Keyboard.FocusedElement;

            Topmost = true;
            base.Show();

            Owner = Application.Current.MainWindow;
            Closed += NotificationWindowClosed;
            AdjustWindows();

            if (focusedElement != null)
            {
                // Restore keyboard focus to the original element that had focus. That way if someone
                // was typing into a control we don't steal keyboard focus away from that control.
                focusedElement.Focusable = true;
                Keyboard.Focus(focusedElement);
            }
        }

        /// <summary>
        ///     Shows the dialog.
        /// </summary>
        /// <exception cref="System.NotImplementedException">ShowDialog() is not supported.  Use Show() instead.</exception>
        public new void ShowDialog()
        {
            throw new NotImplementedException("ShowDialog() is not supported.  Use Show() instead.");
        }

        #endregion

        #region Event Handlers

        /// <summary>
        ///     Raises the <see cref="E:ClosedByUser" /> event.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        protected virtual void OnClosedByUser(EventArgs e)
        {
            EventHandler<EventArgs> onClosedByUser = ClosedByUser;
            if (onClosedByUser != null)
            {
                onClosedByUser(this, e);
            }
        }

        /// <summary>
        ///     Raises the <see cref="E:HyperlinkClicked" /> event.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        protected virtual void OnHyperlinkClicked(HyperLinkEventArgs e)
        {
            EventHandler<HyperLinkEventArgs> onHyperlinkClicked = HyperlinkClicked;
            if (onHyperlinkClicked != null)
            {
                onHyperlinkClicked(this, e);
            }
        }

        /// <summary>
        ///     Buttons the view click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs" /> instance containing the event data.</param>
        private void ButtonViewClick(object sender, RoutedEventArgs e)
        {
            OnHyperlinkClicked(new HyperLinkEventArgs {HyperlinkObjectForRaisedEvent = HyperlinkObjectForRaisedEvent});
        }

        /// <summary>
        ///     Doubles the animation completed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void DoubleAnimationCompleted(object sender, EventArgs e)
        {
            if (!IsMouseOver)
            {
                Close();
            }
        }

        /// <summary>
        ///     Images the mouse up.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="MouseButtonEventArgs" /> instance containing the event data.</param>
        private void ImageMouseUp(object sender, MouseButtonEventArgs e)
        {
            OnClosedByUser(new EventArgs());
            Close();
        }

        /// <summary>
        ///     Mains the window closing.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.ComponentModel.CancelEventArgs" /> instance containing the event data.</param>
        private void MainWindowClosing(object sender, CancelEventArgs e)
        {
            foreach (Window window in Application.Current.Windows)
            {
                string windowType = window.GetType().Name;
                if (windowType.Equals(_name))
                {
                    window.Close();
                }
            }
        }

        /// <summary>
        ///     Notifications the window closed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void NotificationWindowClosed(object sender, EventArgs e)
        {
            foreach (Window window in Application.Current.Windows)
            {
                string windowName = window.GetType().Name;

                if (windowName.Equals(_name) && window != this)
                {
                    // Adjust any windows that were above this one to drop down
                    if (window.Top < Top && window.Left == Left)
                    {
                        window.Top = window.Top + ActualHeight;

                        if (!WindowsExistToTheRight(Left))
                        {
                            window.Left = window.Left + ActualWidth;
                        }
                    }
                }
            }

            AdjustWindows();
        }

        #endregion Event Handlers

        #region Private Methods

        /// <summary>
        ///     Dow windows exist to the right.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <returns></returns>
        private bool WindowsExistToTheRight(double left)
        {
            foreach (Window window in Application.Current.Windows)
            {
                string windowName = window.GetType().Name;

                if (windowName.Equals(_name) &&
                    window != this &&
                    left == Screen.PrimaryScreen.WorkingArea.Width - Width)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        ///     Adjusts the windows.
        /// </summary>
        private void AdjustWindows()
        {
            lock (_lockObject)
            {
                Rectangle workingArea = Screen.PrimaryScreen.WorkingArea;

                Left = SystemParameters.WorkArea.Width - ActualWidth;
                double top = SystemParameters.WorkArea.Height - ActualHeight;

                foreach (Window window in Application.Current.Windows)
                {
                    string windowName = window.GetType().Name;

                    if (windowName.Equals(_name) && window != this)
                    {
                        window.Topmost = true;

                        if (Left == window.Left)
                        {
                            top = top - window.ActualHeight;
                        }

                        if (top < 0)
                        {
                            Left = Left - ActualWidth;
                            top = workingArea.Bottom - ActualHeight;
                        }
                    }
                }

                Top = top;
            }
        }

        /// <summary>
        ///     Sets the hyperlink button.
        /// </summary>
        /// <param name="hyperlinkText">The hyperlink text.</param>
        private void SetHyperLinkButton(string hyperlinkText)
        {
            if (!string.IsNullOrWhiteSpace(hyperlinkText))
            {
                buttonView.Content = hyperlinkText;
                buttonView.Visibility = Visibility.Visible;
            }
        }

        #endregion Private Methods
    }
}