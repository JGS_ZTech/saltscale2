﻿using System;

namespace PropertyFilteringLib
{
    public class FilterCriterion
    {
        #region Static Fields

        #region Contains

        public static readonly FilterCriterion Contains = new FilterCriterion(
            "Contains",
            (dataItem, value) => dataItem.ToString().IndexOf(value, StringComparison.OrdinalIgnoreCase) > -1);

        #endregion // Contains

        #region EndsWith

        public static readonly FilterCriterion EndsWith = new FilterCriterion(
            "Ends With",
            (dataItem, value) => dataItem.ToString().EndsWith(value, StringComparison.OrdinalIgnoreCase));

        #endregion // EndsWith

        #region DoNotEndsWith

        public static readonly FilterCriterion DoNotEndsWith = new FilterCriterion(
            "Do Not Ends With",
            (dataItem, value) => !dataItem.ToString().EndsWith(value, StringComparison.OrdinalIgnoreCase));

        #endregion // DoNotEndsWith

        #region IsEqualTo

        public static readonly FilterCriterion IsEqualTo = new FilterCriterion(
            "Is Equal To",
            (dataItem, value) =>
            {
                object convertedValue = value;
                try
                {
                    if (PropertyFilter.IsNumericType(dataItem.GetType()))
                    {
                        dataItem = Convert.ChangeType(dataItem, typeof(double));
                        convertedValue = Convert.ChangeType(value, typeof(double));
                    }
                    else if (dataItem is bool)
                    {
                        convertedValue = Convert.ChangeType(value, typeof(bool));
                    }
                    else if (dataItem is Enum)
                    {
                        dataItem = dataItem.ToString();
                    }

                    return Object.Equals(dataItem, convertedValue);
                }
                catch
                {
                    return false;
                }
            });

        #endregion // IsEqualTo

        #region IsNotEqualTo

        public static readonly FilterCriterion IsNotEqualTo = new FilterCriterion(
            "Is Not Equal To",
            (dataItem, value) =>
            {
                object convertedValue = value;
                try
                {
                    if (PropertyFilter.IsNumericType(dataItem.GetType()))
                    {
                        dataItem = Convert.ChangeType(dataItem, typeof(double));
                        convertedValue = Convert.ChangeType(value, typeof(double));
                    }
                    else if (dataItem is bool)
                    {
                        convertedValue = Convert.ChangeType(value, typeof(bool));
                    }
                    else if (dataItem is Enum)
                    {
                        dataItem = dataItem.ToString();
                    }

                    return !Object.Equals(dataItem, convertedValue);
                }
                catch
                {
                    return true;
                }
            });

        #endregion // IsEqualTo

        #region IsGreaterThan

        public static readonly FilterCriterion IsGreaterThan = new FilterCriterion(
            "Is Greater Than",
            (dataItem, value) =>
            {
                object convertedValue = value;
                try
                {
                    if (PropertyFilter.IsNumericType(dataItem.GetType()))
                    {
                        dataItem = Convert.ChangeType(dataItem, typeof(double));
                        convertedValue = Convert.ChangeType(value, typeof(double));
                    }

                    IComparable comp = dataItem as IComparable;
                    if (comp == null)
                        return false;

                    return comp.CompareTo(convertedValue) > 0;
                }
                catch
                {
                    return false;
                }
            });

        #endregion // IsGreaterThan

        #region IsLessThan

        public static readonly FilterCriterion IsLessThan = new FilterCriterion(
            "Is Less Than",
            (dataItem, value) =>
            {
                object convertedValue = value;
                try
                {
                    if (PropertyFilter.IsNumericType(dataItem.GetType()))
                    {
                        dataItem = Convert.ChangeType(dataItem, typeof(double));
                        convertedValue = Convert.ChangeType(value, typeof(double));
                    }

                    IComparable comp = dataItem as IComparable;
                    if (comp == null)
                        return false;

                    return comp.CompareTo(convertedValue) < 0;
                }
                catch
                {
                    return false;
                }
            });

        #endregion // IsLessThan

        #region StartsWith

        public static readonly FilterCriterion StartsWith = new FilterCriterion(
            "Starts With",
            (dataItem, value) => dataItem.ToString().StartsWith(value, StringComparison.OrdinalIgnoreCase));

        #endregion // StartsWith

        #region DoNotStartsWith

        public static readonly FilterCriterion DoNotStartsWith = new FilterCriterion(
            "Do Not Starts With",
            (dataItem, value) => !dataItem.ToString().StartsWith(value, StringComparison.OrdinalIgnoreCase));

        #endregion // DoNotStartsWith

        #endregion // Static Fields

        #region Instance Properties

        public string DisplayName { get; set; }

        public Func<object, string, bool> IsFilteredIn { get; private set; }

        #endregion // Instance Properties

        #region Public Constructor

        public FilterCriterion(string displayName)
        {
            DisplayName = displayName;
        }

        #endregion

        #region Private Constructor

        FilterCriterion(string displayName, Func<object, string, bool> isFilteredIn)
        {
            this.DisplayName = displayName;
            this.IsFilteredIn = isFilteredIn;
        }

        #endregion // Private Constructor
    }
}