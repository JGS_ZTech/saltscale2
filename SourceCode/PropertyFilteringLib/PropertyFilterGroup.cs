﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows.Markup;

namespace PropertyFilteringLib
{
    [ContentProperty("Filters")]
    public class PropertyFilterGroup
    {
        ICollectionView _dataSourceView;
        ObservableCollection<PropertyFilter> _filters;
        ObservableCollection<PropertyFilter> _filtersApplied;

        public PropertyFilterGroup()
        {
        }

        public ICollectionView DataSourceView
        {
            get { return _dataSourceView; }
            set 
            {
                _dataSourceView = value;

                if (_dataSourceView == null)
                    return;

                _dataSourceView.Filter =  dataItem =>
                {
                    if (RemovedFilter != null)
                    {
                        foreach (PropertyFilter filter in _filtersApplied)

                            if (!filter.IsFilteredIn(dataItem))
                            {
                                if (_filtersApplied.Count > 1 &&
                                    _dataSourceView.Cast<object>().ToList().Find(i => i == dataItem) != null &&
                                    RemovedFilter != filter)
                                    return true;

                                return false;
                            }

                        return true;
                    }
                    else if (LoadedFilter)
                    {
                        foreach (PropertyFilter filter in _filtersApplied)

                            if (!filter.IsFilteredIn(dataItem))
                            {
                                if (_filtersApplied.Count > 1 &&
                                    _dataSourceView.Cast<object>().ToList().Find(i => i == dataItem) != null)
                                    return true;

                                return false;
                            }

                        return true;
                    }
                    else
                    {
                        foreach (PropertyFilter filter in _filters)

                            if (!filter.IsFilteredIn(dataItem))
                            {
                                if (_filtersApplied.Count > 1 &&
                                    _dataSourceView.Cast<object>().ToList().Find(i => i == dataItem) != null)
                                    return true;

                                return false;
                            }

                        return true;
                    }
                };
            }
        }

        public ObservableCollection<PropertyFilter> FiltersApplied
        {
            get { return _filtersApplied ?? (this.FiltersApplied = new ObservableCollection<PropertyFilter>()); }
            set
            {
                if (value == _filtersApplied)
                    return;

                if (_filtersApplied != null)
                    _filtersApplied.CollectionChanged -= this.OnFiltersCollectionChanged;

                _filtersApplied = value;

                if (_filtersApplied != null)
                    _filtersApplied.CollectionChanged += this.OnFiltersCollectionChanged;
            }
        }

        public ObservableCollection<PropertyFilter> Filters
        {
            get { return _filters ?? (this.Filters = new ObservableCollection<PropertyFilter>()); }
            set
            {
                if (value == _filters)
                    return;

                if (_filters != null)
                    _filters.CollectionChanged -= this.OnFiltersCollectionChanged;

                _filters = value;

                if (_filters != null)
                    _filters.CollectionChanged += this.OnFiltersCollectionChanged;

            }
        }

        public PropertyFilter RemovedFilter { get; set; }

        public Boolean LoadedFilter { get; set; }

        void OnFiltersCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null && e.NewItems.Count != 0)
            {
                foreach (PropertyFilter filter in e.NewItems)
                    filter.Group = this;
            }

            this.ApplyFilters();
        }

        internal void ApplyFilters()
        {
            if (_dataSourceView == null)
                return;

            _dataSourceView.Refresh();
        }

        internal void AddFilters(PropertyFilter value)
        {
            var newFilter = new PropertyFilter(value.PropertyName, value.PropertyType, value.PropertyConverter);
            newFilter.DisplayName = value.DisplayName;
            newFilter.ActiveCriterion = value.ActiveCriterion;
            newFilter.Value = value.Value;
            FiltersApplied.Add(newFilter);
            this.ApplyFilters();
        }

        internal void RemoveFilters(PropertyFilter value)
        {
            RemovedFilter = value;
            FiltersApplied.Remove(value);
            this.ApplyFilters();
            RemovedFilter = null;
        }

        public void RefreshFilters()
        {
            if (_dataSourceView == null)
                return;

            LoadedFilter = true;
            _dataSourceView.Refresh();
            LoadedFilter = false;
        }
    }
}