//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class GreatPlain
    {
        public GreatPlain()
        {
            this.Customers = new HashSet<Customer>();
        }
    
        public int GreatPlainID { get; set; }
        public Nullable<int> CustomerID { get; set; }
        public string CustomerCode { get; set; }
    
        public virtual ICollection<Customer> Customers { get; set; }
    }
}
