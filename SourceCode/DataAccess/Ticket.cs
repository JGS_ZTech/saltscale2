//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class Ticket
    {
        public int TicketID { get; set; }
        public int TicketNumber { get; set; }
        public float Quantity { get; set; }
        public bool isManual { get; set; }
        public Nullable<int> TruckID { get; set; }
        public bool isVoid { get; set; }
        public string VoidReason { get; set; }
        public Nullable<bool> isReconciled { get; set; }
        public Nullable<bool> hasBeenExported { get; set; }
        public Nullable<int> CompanyID { get; set; }
        public System.DateTime CreateTime { get; set; }
        public Nullable<System.DateTime> UpdateTime { get; set; }
        public bool IsActive { get; set; }
        public Nullable<int> OrderId { get; set; }
        public string Comment { get; set; }
        public string DataA { get; set; }
        public string DataB { get; set; }
        public string DataC { get; set; }
        public int SeasonId { get; set; }
        public Nullable<int> CashPurchaseId { get; set; }
        public Nullable<decimal> FuelSurcharge { get; set; }
        public Nullable<System.DateTime> DateExported { get; set; }
    
        public virtual CashPurchase CashPurchase { get; set; }
        public virtual Company Company { get; set; }
        public virtual OrderTable OrderTable { get; set; }
        public virtual Season Season { get; set; }
        public virtual Truck Truck { get; set; }
    }
}
