//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class User
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string SignatureFileName { get; set; }
        public string WeighMasterNumber { get; set; }
        public string Signature { get; set; }
        public string WeighMasterName { get; set; }
        public bool IsAuthorizedOrderCreateEditDelete { get; set; }
        public bool IsAuthorizedAdministrator { get; set; }
        public bool IsAuthorizedAccounting { get; set; }
        public bool IsAuthorizedSiteChange { get; set; }
        public bool IsAuthorizedScaleSettings { get; set; }
        public bool IsAuthorizedSeasonChange { get; set; }
        public bool IsAuthorizedCashCustomer { get; set; }
        public bool IsAuthorizedCustomerService { get; set; }
        public bool IsAuthorizedMaterials { get; set; }
        public bool IsAuthorizedReports { get; set; }

        // Create a local Weigh Master Name and Number
        public string WeighMasterNumberSession { get; set; }
        public string WeighMasterNameSession { get; set; }
    }
}
