﻿using System;

namespace Contract.Domain
{
    public interface IDispatcherReport
    {
        string CompanyName { get; set; }
        /// <summary>
        /// aka Entity
        /// </summary>
        string Organization { get; set; }
        /// <summary>
        /// aka Customer Shortname
        /// </summary>
        string CustomerId { get; set; }
        /// <summary>
        /// aka Customer Dispatcher Name
        /// </summary>
        string CustomerName { get; set; }
        string TruckOwner { get; set; }
        string TruckId { get; set; }
        string TicketNumber { get; set; }
        string PoNumber { get; set; }
        int? ConfirmationNumber { get; set; }
        string Material { get; set; }


        bool IsVoid { get; set; }


        int Season { get; set; }
        DateTime OrderEffective { get; set; }
        DateTime? TicketDateTime { get; set; }
        float OrderQuantityTotal { get; set; }
        float TicketQuantityTotal { get; set;}
        //DateTime HeldUntilDate { get; set; }
        float BalanceQuantity { get; set; }
    }
}