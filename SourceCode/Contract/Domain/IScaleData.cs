﻿namespace Contract.Domain
{
    public interface IScaleData
    {
        int Id { get; set; }
        string Site { get; set; }
        string ScaleName { get; set; }
        bool IsScaleEnabled { get; set; }
        int SerialComPort { get; set; }
        int SerialBaud { get; set; }
        string SerialParity { get; set; }
        int SerialDataBits { get; set; }
        int SerialStopBits { get; set; }
        int SerialPortDataReceivedDelayMS { get; set; }
        bool IsIndicatorWeightNegative { get; set; }
        bool IsIndicatorWeightOverload { get; set; }
        bool IsIndicatorWeightUnderload { get; set; }
        bool IsMotionDetectionEnabled { get; set; }
        string MotionDetectionCharacter { get; set; }
        string OutputStreamFormat { get; set; }
        string IndicatorModel { get; set; }
        int ReWeighTimerMS { get; set; }
    }

}