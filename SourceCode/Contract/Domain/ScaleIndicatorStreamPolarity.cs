﻿using System.ComponentModel;

namespace Contract.Domain
{
    /// <summary>
    ///     "space" = Positive
    ///     - = Negative
    ///     ^ = Overload
    ///     ] = Underrange
    /// </summary>
    public enum ScaleIndicatorStreamPolarity
    {
        [Description("Undefined")]
        Undefined = 0,
        /// <summary>
        ///     "space" = Positive
        /// </summary>
        [Description("Positive")] Positive = 32,

        /// <summary>
        ///     - = Negative
        /// </summary>
        [Description("Negative")] Negative = 45,

        /// <summary>
        ///     ^ = Overload
        /// </summary>
        [Description("Overload")] Overload = 94,

        /// <summary>
        ///     ] = Underrange
        /// </summary>
        [Description("Underrange")] Underrange = 93
    }
}