﻿namespace Contract.Domain
{
    public interface IAddress
    {
        string AddressLineOne { get; set; }
        string AddressLineTwo { get; set; }
        string City { get; set; }
        string Comment { get; set; }
        string State { get; set; }
        int AddressId { get; set; }
        bool IsActive { get; set; }
        string Zip { get; set; }
        int SeasonId { get; set; }
    }
}