﻿namespace Contract.Domain
{
    public interface IScaleInfo
    {
        int Id { get; set; }
        string ScaleName { get; set; }
        bool IsScaleEnabled { get; set; }
        int SerialComPort { get; set; }
    }

    public interface IScaleResult
    {
        int Id { get; set; }
        ScaleIndicatorStreamPolarity StreamPolarity { get; set; }
        int? StreamWeight { get; set; }
        ScaleIndicatorStreamUnitOfWeight StreamUnitOfWeight { get; set; }
        ScaleIndicatorStreamWeightGrossNet StreamWeightGrossNet { get; set; }
        ScaleIndicatorStreamMotionStatus StreamMotionStatus { get; set; }
    }
}