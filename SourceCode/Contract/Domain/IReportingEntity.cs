﻿namespace Contract.Domain
{
    public interface IReportingEntity
    {
        int Id { get; set; }
        string Name { get; set; }
        string ShortName { get; set; }
        decimal? SeasonMaterialEstimateTon { get; set; }
    }
    public interface IReportingEntityL2
    {
        int Id { get; set; }
        string Name { get; set; }
        string ShortName { get; set; }
        decimal? SeasonMaterialEstimateTon { get; set; }
    }
    public interface IReportingEntityL3
    {
        int Id { get; set; }
        string Name { get; set; }
        string ShortName { get; set; }
        decimal? SeasonMaterialEstimateTon { get; set; }
    }
}