﻿using System;

namespace Contract.Domain
{
    public interface ICashPurchaseData
    {
        int? CashPurchaseId { get; set; }
        string TruckDescription { get; set; }
        int? MaterialId { get; set; }
        decimal? Quote { get; set; }
        int? CompanyId { get; set; }
        int? LocationId { get; set; }
        int? Tare { get; set; }
        string PaymentForm { get; set; }
        string CheckNumber { get; set; }
        bool TaxExempt { get; set; }
        bool IsComplete { get; set; }
        DateTime CreateTime { get; set; }
        DateTime? UpdateTime { get; set; }
        bool IsActive { get; set; }
        string Comment { get; set; }
        string DataA { get; set; }
        string DataB { get; set; }
        string DataC { get; set; }
        int SeasonId { get; set; }
        decimal? TaxRate { get; set; }
    }
}