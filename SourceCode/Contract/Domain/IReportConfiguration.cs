﻿
namespace Contract.Domain
{
    public interface IReportConfiguration
    {
        int Id { get; set; }
        string Name { get; set; }
        string Configuration { get; set; }
        int SiteId { get; set; }
        int SeasonId { get; set; }
    }
}