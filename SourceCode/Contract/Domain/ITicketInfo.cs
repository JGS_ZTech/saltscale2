﻿using System;

namespace Contract.Domain
{
    public interface ITicketInfo
    {
        int Id { get; set; }
        int TicketNumber { get; set; }
        DateTime CreateTime { get; set; }
        float Tons { get; set; }
        string TruckId { get; set; }
        int? OrderNumber { get; set; }
        int CustomerId { get; set; }
        int? OrderId { get; set; }
        int PurchaseOrderId { get; set; }
        string VoidReason { get; set; }
        bool IsVoid { get; set; }
        int TicketCountPerDay { get; set; }
        int? ConfirmationNumber { get; set; }
    }
}