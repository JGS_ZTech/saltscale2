﻿using System;

namespace Contract.Domain
{
    public interface IOrderSummary
    {
        int OrderId { get;  }
        int BidId { get;  }
        int SiteId { get;  }
        int? OrderNumber { get;  }
        string OrderedBy { get;  }
        bool IsComplete { get;  }
        DateTime? CompleteDate { get;  }
        string Promise { get;  }
        DateTime? PromiseExpiresTime { get;  }
        string SpecialInstructionOverride { get;  }
        DateTime EffectiveTime { get;  }
        DateTime? HeldUntilDate { get;  }
        string Priority { get;  }
        DateTime CreateTime { get;  }
        DateTime? UpdateTime { get;  }
        bool IsActive { get;  }
        string Material { get;  }
        DateTime? LastTicketDate { get;  }
        float Balance { get;  }
        int Quantity { get; }
        string CustomerName { get;  }
        string CustomerCity { get; }
        string CustomerState { get; }
        string CustId { get;  }
        float TodayTons { get; }
        int CustomerId { get; set; }
        string Comment { get; set; }
        int CompanyId { get; set; }
        string CompanyName { get; set; }
        int NumberOfTicket { get; }
        string PoOverride { get; set; }
        int? ConfirmationNumber { get; set; }
        bool IsVoid { get;  }
        bool IsClosed { get; set; }

    }
}