﻿using System;

namespace Contract.Domain
{
    public interface ITruckReport
    {
        
        string TicketNumber { get; set; }
        

        DateTime TicketDateTime { get; set; }
        
        string PoNumber { get; set; }
        string CustomerId { get; set; }
        string CustomerName { get; set; }
        string TruckId { get; set; }
        
        decimal GrossTons { get; }
        decimal TareTons { get; set; }
        decimal NetTons { get; }
        decimal TruckingRate { get; }
        decimal SubTotal { get; }
        decimal FuelSurcharge { get; set; }
        decimal Total { get; }
        string VoidDescription { get; set; }
        string Organization { get; set; }
        string Entity { get; set; }
        string Material { get; set; }
        int Season { get; set; }
        bool IsVoid { get; set; }
        float Quantity { get; set; }
        decimal? Quote { get; set; }
        string ConfNumber { get; set; }
    }
}