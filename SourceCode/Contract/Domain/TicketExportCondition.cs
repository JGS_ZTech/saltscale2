﻿namespace Contract.Domain
{
    public enum TicketExportCondition
    {
        All,
        NotExported,
        Exported,
        ExportedAndVoided
    }
}