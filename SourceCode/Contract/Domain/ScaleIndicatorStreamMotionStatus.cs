﻿using System.ComponentModel;

namespace Contract.Domain
{
    /// <summary>
    ///     0 = Undefined
    ///     "space" = Valid (aka No Motion)
    ///     I = Invalid
    ///     M = Motion
    ///     m = Motion
    ///     O = Over/under range
    ///     o = Overcapacity
    ///     C = Center of Zero
    ///     Z = Center of Zero - Newark Scale COM4 Grey, COM5 Blue
    ///     
    ///     e = Weight not being displayed
    /// </summary>
    public enum ScaleIndicatorStreamMotionStatus
    {
        [Description("Undefined")]
        Undefined = 0,
        NotDisplayed = 101,
        /// <summary>
        ///     I = Invalid
        /// </summary>
        [Description("Invalid")] Invalid = 76,

        /// <summary>
        ///     M = Motion
        /// </summary>
        [Description("Motion")] Motion = 77,
        [Description("Motion")] motion = 109,

        /// <summary>
        ///     "space" = Valid (aka No Motion)
        /// </summary>
        [Description("No Motions")] NoMotion = 32,

        /// <summary>
        ///     O = Over/under range
        /// </summary>
        [Description("Over/Under Range")] OverUnderRange = 79,
        [Description("Overcapacity")] Overcapacity = 111,

        /// <summary>
        ///     C = Center of Zero
        /// </summary>
        [Description("Center of Zero")] CenterOfZeroC = 67,
        [Description("Center of Zero")] CenterOfZeroZ = 90,

        /// <summary>
        ///     B for BZ = Center of Zero
        /// </summary>
        [Description("Below Zero")] BelowZero = 42,

    }
}