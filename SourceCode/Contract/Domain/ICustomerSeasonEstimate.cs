﻿

using System.Collections.ObjectModel;

namespace Contract.Domain
{
    public interface ICustomerSeasonEstimate
    {
        int CustomerId { get; set; }
        int? ReportingEntityId { get; set; }
        string EntityName { get; set; }
        int SeasonId { get; set; }
        string SeasonName { get; set; }
        string CustomerDispatcherName { get; set; }
        string CustomerShortName { get; set; }
        int BidId { get; set; }
        int MaterialId { get; set; }
        string MaterialShortName { get; set; }
        
        decimal Estimate { get; set; }
        decimal Actual { get; set; }
        decimal? Difference { get; }
        decimal? Percentage { get; }
        ICustomerSeasonEstimate TempValues { get; set; }
        void BeginEdit();
        void EndEdit();
        void CancelEdit();
        ObservableCollection<ICustomerSeasonEstimate> CustomerSeasonEstimateHierarchy { get; set; }
    }
}
