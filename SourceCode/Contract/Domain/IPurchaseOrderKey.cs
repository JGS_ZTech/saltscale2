﻿namespace Contract.Domain
{
    public interface IPurchaseOrderKey
    {
        string PurchaseOrderNumber { get; set; }
        int Id { get; set; }
    }
}