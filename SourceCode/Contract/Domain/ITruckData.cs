﻿using System;

namespace Contract.Domain
{
    public interface ITruckData
    {
        int Id { get; set; }
        int? TruckOwnerId { get; set; }
        string ShortName { get; set; }
        string Tag { get; set; }
        string Description { get; set; }
        int? TareInterval { get; set; }
        bool NeedsTareOverride { get; set; }
        int? CubicYards { get; set; }
        string Sticker { get; set; }
        DateTime CreateTime { get; set; }
        DateTime? UpdateTime { get; set; }
        bool IsActive { get; set; }
        string Comment { get; set; }
        string PhoneNumber { get; set; }
        int SeasonId { get; set; }
        int Weight { get; set; }

        DateTime? DateTimeOfWeight { get; set; }
    }
}