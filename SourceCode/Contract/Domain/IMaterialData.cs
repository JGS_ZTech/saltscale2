﻿using System;

namespace Contract.Domain
{
    public interface IMaterialData
    {
        int? Id { get; set; }
        string Description { get; set; }
        string Name { get; set; }
        decimal? PricePerTon { get; set; }
        decimal? PricePerYard { get; set; }
        decimal? PriceEach { get; set; }
        int? MinimumCharge { get; set; }
        DateTime CreateTime { get; set; }
        DateTime? UpdateTime { get; set; }
        bool IsActive { get; set; }
        int SeasonId { get; set; }
    }
}