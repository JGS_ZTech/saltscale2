﻿using System.ComponentModel;

namespace Contract.Domain
{
    /// <summary>
    ///     0 = Undefined
    ///     G = Gross
    ///     g = Gross
    ///     N = Net
    /// </summary>
    public enum ScaleIndicatorStreamWeightGrossNet
    {
        [Description("Undefined")]
        Undefined = 0,
        /// <summary>
        ///     Gross
        /// </summary>
        [Description("Gross")] G = 71,
        [Description("Gross")] g = 103,

        /// <summary>
        ///     Net
        /// </summary>
        [Description("Net")] N = 78
    }
}