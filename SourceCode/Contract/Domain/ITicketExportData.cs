using System;

namespace Contract.Domain
{
    public interface ITicketExportData
    {
        decimal? MaterialCost { get; }
        decimal? SalesTax { get; }
        decimal? Quote { get; set; }
        bool TaxExempt { get; set; }
        decimal? TaxRate { get; set; }
        bool CashTicket { get; set; }
        decimal? TruckingCost { get; set; }
        string TruckId { get; set; }
        string Location { get; set; }
        string Material { get; set; }
        bool IsManual { get; set; }
        float Quantity { get; set; }
        int? AccountingCustomerId { get; set; }
        string AccountingCustomerCode { get; set; }
        int? CustomerId { get; set; }
        string Company { get; set; }
        DateTime ShipDate { get; set; }
        string PurchaseOrder { get; set; }
        int TicketNumber { get; set; }
        string SeasonPrefix { get; set; }
        string VoidReason { get; set; }
        DateTime? VoidDate { get; set; }
        bool Voided { get; set; }
        DateTime? DateExported { get; set; }
        bool? Exported { get; set; }
        string CustomerShortName { get; set; }
        string TruckDescription { get; set; }
        string CashMaterial { get; set; }
    }
}