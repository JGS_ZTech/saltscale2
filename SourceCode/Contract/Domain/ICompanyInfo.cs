﻿namespace Contract.Domain
{
    public interface ICompanyInfo
    {
        string PrinterTicketName { get; set; }
        string Name { get; set; }
        int Id { get; set; }
        string AddressLineOne { get; set; }
        string AddressLineTwo { get; set; }
        string City { get; set; }
        string State { get; set; }
        string Zip { get; set; }
        string PhoneNumberOne { get; set; }
        string PhoneNumberTwo { get; set; }
        string FaxNumber { get; set; }
    }
    
    public interface ICompanyData:ICompanyInfo
    {
        int FirstTicket { get;  }
        int LastTicket { get;  }
        int NextTicket { get;  }
        //string ExportPath  { get;  }
        //string BackupPath { get;  }
        //string ExportFile { get;  }
    }
}