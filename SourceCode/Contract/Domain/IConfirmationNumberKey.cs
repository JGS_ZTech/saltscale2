﻿namespace Contract.Domain
{
    public interface IConfirmationNumberKey
    {
        string ConfirmationNumber { get; set; }
        int Id { get; set; }
    }
}