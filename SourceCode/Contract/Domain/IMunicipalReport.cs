﻿using System;

namespace Contract.Domain
{
    public interface IMunicipalReport
    {
        string TicketNumber { get; set; }
        DateTime TicketDateTime { get; set; }
        string OrderNumber { get; set; }
        bool? OrderCompleted { get; set; }
        string OrderCompletedText { get; }
        string PoNumber { get; set; }
        string CustomerId { get; set; }
        string CustomerName { get; set; }
        string TruckId { get; set; }
        decimal NetTons { get; }
        string Organization { get; set; }
        string Entity { get; set; }
        string Material { get; set; }
        int Season { get; set; }
        bool IsVoid { get; set; }
        float Quantity { get; set; }
        string ConfNumber { get; set; }
    }
}