﻿namespace Contract.Domain
{
    public interface IBidInfo
    {
        int? BidId { get; set; }
        int CustomerId { get; set; }
        int SiteId { get; set; }
        string SiteName { get; set; }
        int MaterialId { get; set; }
        string MaterialShortName { get; set; }
        string MaterialDescription { get; set; }
        decimal? PricePerTon { get; set; }
        decimal? TruckingRate { get; set; }
        bool IsActive { get; set; }
        int SeasonId { get; set; }
        decimal? Toll { get; set; }
        decimal? SeasonMaterialEstimateTon { get; set; }
    }
}
