﻿namespace Contract.Domain
{
    public enum SeasonStep
    {
        Start,
        End,
        Warning,
        FatalException
    }
}