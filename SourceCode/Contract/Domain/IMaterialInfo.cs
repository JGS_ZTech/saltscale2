﻿namespace Contract.Domain
{
    public interface IMaterialInfo
    {
        decimal? PriceEach { get; set; }
        decimal? PricePerTon { get; set; }
        string Description { get; set; }
        string Name { get; set; }
        int Id { get; set; }
    }
}