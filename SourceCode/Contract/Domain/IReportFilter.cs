﻿using System;
using System.ComponentModel;
using System.Collections.Generic;

namespace Contract.Domain
{
    public interface IReportFilter
    {
        ReportDefintionEnum ReportDefinitionEnumValue { get; set; }
        string ReportFileName { get; set; }
        string ReportTitle { get; set; }
        DateTime BeginDateTime { get; set; }
        DateTime EndDateTime { get; set; }
        DetailSummaryEnum DetailSummary { get; set; }
        CustomerTypeEnum CustomerType { get; set; }
        bool IsVoidsIncluded { get; set; }
        int SiteId { get; set; }
        ReportSortByEnum SortBy { get; set; }
        List<ReportFilterGroupApplied> AppliedFilters { get; set; }
    }

    public class ReportFilterGroupApplied
    {
        public string FilterColumn { get; set; }
        public string FilterSQLColumnOperatorValue { get; set; }
        public string FilterValue { get; set; }
    }

    public enum DetailSummaryEnum
    {
        [Description("Detail")]
        Detail = 0,
        [Description("Summary")]
        Summary = 1,
        [Description("Detail And Summary")]
        DetailAndSummary = 2

    }


    public enum CustomerTypeEnum
    {
        [Description("Cash & Non-Cash")]
        Both = 0,
        [Description("Cash")]
        Cash = 1,
        [Description("Non-Cash")]
        NonCash = 2

    }

    public enum ReportDateRangeEnum
    {
        [Description("Custom Date Range")] // Default value if enum is nullable and used in databinding
        DefaultValue,
        [Description("Month to Today")]
        MonthtoToday = 1,
        [Description("Month to Yesterday")]
        MonthtoYesterday = 2,
        [Description("Week to Today")]
        WeektoToday = 3,
        [Description("Week to Yesterday")]
        WeektoYesterday = 4,
        [Description("Season to Today")]
        SeasontoToday = 5,
        [Description("Season to Yesterday")]
        SeasontoYesterday = 6,
        [Description("Today")]
        Today = 7,
        [Description("Yesterday")]
        Yesterday = 8,
        [Description("Last Week")]
        LastWeek = 9,
        [Description("Last Month")]
        LastMonth = 10,
    }

    public enum ReportDefintionEnum
    {
        [Description("Customer Report")]
        CustomerReport = 3,
        [Description("Dispatcher Report")]
        DispatcherReport = 1,
        [Description("Ticket Report")]
        TicketReport = 2,
        [Description("Truck Report")]
        TruckReport = 4,
        [Description("Inventory Report")]
        InventoryReport = 5,
    }

    public enum ReportSortByEnum
    {
        [Description("Company")]
        Company = 1,
        [Description("Customer")]
        Customer = 2,
        [Description("Entity")]
        Entity = 3,
        [Description("Ticket")]
        Ticket = 4,
        [Description("Truck Owner")]
        TruckOwner = 8,
        [Description("Truck")]
        Truck = 5,
        [Description("PO Number")]
        PONumber = 6,
        [Description("Material")]
        Material = 7,
        //[Description("Payment Type")]
        //PaymentType = 9
    }

}
