﻿using System.ComponentModel;

namespace Contract.Domain
{
    /// <summary>
    ///     Character Interpritations: Unit of measurement
    ///     0 = Undefined
    ///     L = pounds
    ///     l = pounds
    ///     K = kilogram
    ///     T = Ton
    ///     G = grams
    ///     O = ounces
    ///     "space" = none
    /// </summary>
    public enum ScaleIndicatorStreamUnitOfWeight
    {
        [Description("Undefined")] Undefined = 0,

        /// <summary>
        ///     Pounds
        /// </summary>
        [Description("Pounds")] L = 76,
        [Description("Pounds")] l = 108,

        /// <summary>
        ///     Kilograms
        /// </summary>
        [Description("Kilograms")] K = 75,

        /// <summary>
        ///     Tons
        /// </summary>
        [Description("Tons")] T = 84,

        /// <summary>
        ///     Grams
        /// </summary>
        [Description("Grams")] G = 71,

        /// <summary>
        ///     Onces
        /// </summary>
        [Description("Onces")] O = 79,

        /// <summary>
        ///     The character is a space - ASCII char(10) - and that is being represented as "None"
        /// </summary>
        [Description("None")] None = 32
    }
}