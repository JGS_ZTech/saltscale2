using System;

namespace Contract.Domain
{
    public interface ICashTicketInfo
    {
        int SeasonId { get; set; }
        int Id { get; set; }
        int TicketNumber { get; set; }
        DateTime CreateTime { get; set; }
        float Tons { get; set; }
        string TruckDescription { get; set; }
        string VoidReason { get; set; }
        bool IsVoid { get; set; }
    }
}