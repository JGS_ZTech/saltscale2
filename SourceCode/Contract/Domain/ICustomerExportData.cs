namespace Contract.Domain
{
    public interface ICustomerExportData
    {
        string State { get; set; }
        string Zip { get; set; }
        string Phone { get; set; }
        string City { get; set; }
        string Address2 { get; set; }
        string Address1 { get; set; }
        string Organization { get; set; }
        string Name { get; set; }
        string DispatcherName { get; set; }
        string CustomerId { get; set; }
    }
}