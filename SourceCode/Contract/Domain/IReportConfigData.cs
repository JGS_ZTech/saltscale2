﻿namespace Contract.Domain
{
    public interface IReportConfigData
    {
        int Id { get; set; }
        string Name { get; set; }
        string Config { get; set; }
    }
}