﻿using System;
using System.Collections.Generic;

namespace Contract.Domain
{
    public interface IContactData
    {
        int? ContactId { get; }
        string Organization { get; set; }
        string Name { get; set; }
        string ContactComment { get; set; }
        string Comment { get; set; }
        IAddressData Address { get; set; }
        List<IPhoneData> Phones { get; set; }
        DateTime CreateTime { get; set; }
        DateTime? UpdateTime { get; set; }
        bool IsActive { get; set; }
        bool IsPrimary { get; set; }
        int SeasonId { get; set; }
    }

    public interface IAddressData
    {
        string AddressLineOne { get; set; }
        string AddressLineTwo { get; set; }
        string City { get; set; }
        string Comment { get; set; }
        string State { get; set; }
        int? AddressId { get; set; }
        string Zip { get; set; }
        DateTime CreateTime { get; set; }
        DateTime? UpdateTime { get; set; }
        bool IsActive { get; set; }
        int SeasonId { get; set; }
    }

    public interface IPhoneData
    {
        string Comment { get; set; }
        bool IsActive { get; set; }
        string NumberType { get; set; }
        string Number { get; set; }
        int? PhoneNumberId { get; set; }
        bool IsPrimary { get; set; }
        DateTime CreateTime { get; set; }
        DateTime? UpdateTime { get; set; }
        int SeasonId { get; set; }
    }

    public interface IContactInfo
    {
        int ContactId { get; }
        string Organization { get; }
        string Name { get; }
        string PhoneNumber { get; }
    }

}