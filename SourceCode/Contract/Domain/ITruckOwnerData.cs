﻿using System;

namespace Contract.Domain
{
    public interface ITruckOwnerData
    {
        int? Id { get; set; }
        string ShortName { get; set; }
        DateTime CreateTime { get; set; }
        DateTime? UpdateTime { get; set; }
        bool IsActive { get; set; }
        string DataA { get; set; }
        string DataB { get; set; }
        string DataC { get; set; }
        int SeasonId { get; set; }
        IContactData Contact { get; set; }
    }
}