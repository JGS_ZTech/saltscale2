﻿using System;

namespace Contract.Domain
{
    public interface ICustomerAddress
    {
        int Id { get; set; }
        string ShortName { get; set; }
        string Name { get; set; }
        string Address { get; }
        string SpecialInstruction { get; set; }
        string Comment { get; set; }
        string PrimaryContactOrganization { get; }
        string PrimaryContactInfo { get; }
        string FullAddress { get; }
        bool IsShippingLocation { get; set; }
        int? ReportingEntityId { get; set; }
        DateTime CreateTime { get; set; }
        DateTime? UpdateTime { get; set; }
        int SeasonId { get; set; }
        IBid Bid { get; set; }
        ICompanyInfo Company { get; set; }
        IContactData Contact { get; set; }
        bool HideDollarAmounts { get; set; }
        decimal? CashMaterialRate { get; set; }
        bool IsTaxExempt { get; set; }
    }
}