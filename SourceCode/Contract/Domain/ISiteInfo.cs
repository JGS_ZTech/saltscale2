﻿namespace Contract.Domain
{
    public interface ISiteInfo
    {
        bool IsOperatingFromThisLocation { get; set; }
        string Name { get; set; }
        int Id { get; set; }
        decimal TaxRate { get; set; }
        int WeightInterval { get; set; }
        decimal DefaultMaterialCost { get; set; }
        int SeasonId { get; set; }
        int? DefaultCompanyId { get; set; }
        int? DefaultCashCustomerCompanyId { get; set; }
        decimal FuelSurcharge { get; set; }
        int ConfNumberNext { get; set; }
        int ConfNumberLast { get; set; }
        int ConfNumberFirst { get; set; }
        decimal POOverLimit { get; set; }
    }

    public interface ISiteConfig
    {
        string Name { get; set; }
        string ConnectionString { get; set; }
        string ConnectionStringDatasource { get; set; }
        string ConnectionStringInitialCatalog { get; set; }
        string ConnectionStringUserId { get; set; }
        string ConnectionStringPassword { get; set; }
        bool ConnectionStringIntegratedSecurity { get; set; }
        bool IsDefault { get; set; }
        bool IsHeadOffice { get; set; }
    }
}