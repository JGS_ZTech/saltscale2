﻿using System.Collections.ObjectModel;

namespace Contract.Domain
{
    public interface IEntitySeasonEstimate
    {
        int CustomerId { get; set; }
        int? ReportingEntityId { get; set; }
        string EntityDispatcherName { get; set; }
        string EntityShortName { get; set; }
        decimal EntitySeasonMaterialEstimateTon { get; set; }
        decimal EntitySeasonMaterialActualTon { get; set; }
        ObservableCollection<ICustomerSeasonEstimate> CustomerSeasonEstimateCollection { get; set; }
    }
}
