﻿namespace Contract.Domain
{
    public interface ICustomerLine
    {
        /// <summary>
        /// AddressLine1 and AddressLine2
        /// </summary>
        string Address { get; }
        string AddressLine1 { get; set; }
        string AddressLine2 { get; set; }
        string City { get; set; }
        string State { get; set; }
        string ShortName { get; set; }
        string Name { get; set; }
        int Id { get; set; }
        int? ReportingEntityId { get; set; }
        string EntityName { get; set; }
        int SeasonId { get; set; }
        string SeasonName { get; set; }
        decimal? CashMaterialRate { get; set; }
        decimal? TruckingRate { get; set; }
        decimal? Toll { get; set; }
        ICompanyInfo Company { get; }

    }
}