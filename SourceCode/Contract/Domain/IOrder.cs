﻿using System;

namespace Contract.Domain
{
    public interface IOrder
    {
        int OrderId { get; set; }
        int BidId { get; set; }
        int SiteId { get; set; }
        int? OrderNumber { get; set; }
        string OrderedBy { get; set; }
        bool IsComplete { get; set; }
        DateTime? CompleteDate { get; set; }
        string Promise { get; set; }
        DateTime? PromiseExpiresTime { get; set; }
        string POOverride { get; set; }
        string SpecialInstructionOverride { get; set; }
        DateTime EffectiveTime { get; set; }
        DateTime? HeldUntilDate { get; set; }
        string Priority { get; set; }
        DateTime CreateTime { get; set; }
        DateTime? UpdateTime { get; set; }
        bool IsActive { get; set; }
        int SeasonId { get; set; }
        string Comment { get; set; }
        ICustomerAddress Customer { get; }
        IBid Bid { get; set; }
        int Quantity { get; set; }
        IMaterialInfo Material { get; set; }
        int? ConfirmationNumber { get; set; }
        bool IsVoid { get; set; }
    }
}