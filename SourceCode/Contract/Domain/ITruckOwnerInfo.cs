﻿namespace Contract.Domain
{
    public interface ITruckOwnerInfo
    {
        int? Id { get; }
        string ShortName { get; }
    }
}