﻿using System.Collections.Generic;

namespace Contract.Domain
{
    public interface IContact
    {
        int ContactId { get; }
        int PhoneId { get;  }
        string Organization { get; set; }
        string Name { get; set; }
        int AddressId { get; set; }
        bool IsActive { get; set; }
        bool IsPrimary { get; set; }
        string ContactComment { get; set; }
        string Comment { get; }
        string AddressLine { get; set; }
        string PhoneNumber { get; set; }
        IAddressData PrimaryAddress { get; }
        List<IPhone> OtherPhones { get; set;  }
    }
}