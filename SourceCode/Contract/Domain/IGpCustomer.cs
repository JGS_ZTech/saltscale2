﻿namespace Contract.Domain
{
    public interface IGpCustomer
    {
        //IEntity....? tbd Entity { get; set; }
        string EntityDispatcherName { get; set; }
        //Customer Customer { get; set; }
        int CustomerId { get; set; }
        string CustomerDispatcherName { get; set; }
        int AccountingCustomerId { get; set; }
        string AccountingCustomerCode { get; set; }
    }
}