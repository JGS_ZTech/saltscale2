﻿using System;

namespace Contract.Domain
{
    public interface ITicketData
    {
        int? TicketId { get; set; }
        int TicketNumber { get; set; }
        float Quantity { get; set; }
        bool IsManual { get; set; }
        int? TruckId { get; set; }
        bool IsVoid { get; set; }
        string VoidReason { get; set; }
        bool? IsReconciled { get; set; }
        bool? HasBeenExported { get; set; }
        int? CompanyId { get; set; }
        DateTime CreateTime { get; set; }
        DateTime? UpdateTime { get; set; }
        bool IsActive { get; set; }
        int? OrderId { get; set; }
        string Comment { get; set; }
        int SeasonId { get; set; }
        
        string TruckShortName { get; set; }
        string TruckTag { get; set; }
        string TruckSticker { get; set; }
        string TruckDescription { get; set; }
        int TruckWeight { get; set; }

        ICashPurchaseData CashData { get; set; }
        string SpecialInstruction { get; set; }

        string CustomerOrganization { get; set; }
        string CustomerContactName { get; set; }
        string CustomerName { get; set; }
        string CustomerShortName { get; set; }
        string CustomerAddress { get; set; }
        string CustomerPhone { get; set; }

        int MaterialId { get; set; }
        IAddressData Address { get; set; }
        string PO { get; set; }
        

        decimal? TruckRate { get; set; }
        int? OrderNumber { get; set; }
        bool HideDollarAmount { get; set; }
        bool IsTareExpired { get; set; }
        decimal FuelSurcharge { get; set; }
        int? ConfirmationNumber { get; set; }


        string DataA { get; set; }
        string DataB { get; set; }
        string DataC { get; set; }
    }
}