﻿namespace Contract.Domain
{
    public interface ITruckInfo
    {
        string OwnerName { get;  }
        int? OwnerId { get;  }
        string Sticker { get;  }
        string Tag { get; }
        string Description { get; }
        string Phone { get; }
        string Name { get; }
        int Id { get;  }
    }
}