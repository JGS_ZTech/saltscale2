﻿namespace Contract.Domain
{
    public interface ICustomerInfo
    {
        string Address { get;  }
        string ShortName { get; }
        string Name { get; }
        int Id { get; }
        string Phone { get; }
        string SpecialInstruction { get;  }
        string FullAddress { get; set; }
        string OrganizationName { get; set; }
        string ContactInfo { get; set; }
        string Comment { get; set; }
        ICompanyInfo Company { get; set; }
    }
}