﻿using System;
using System.ComponentModel;

namespace Contract.Domain
{
    public interface IBid
    {
        int? BidId { get; set; }
        int CustomerId { get; set; }
        int SiteId { get; set; }
        ISiteInfo Site { get; set; }
        int MaterialId { get; set; }
        IMaterialInfo Material { get; set; }
        decimal? PricePerTon { get; set; }
        decimal? PricePerYard { get; set; }
        decimal? TruckingRate { get; set; }
        string POOverride { get; set; }
        DateTime CreateTime { get; set; }
        DateTime? UpdateTime { get; set; }
        bool IsActive { get; set; }
        string Comment { get; set; }
        int SeasonId { get; set; }
        decimal? Toll { get; set; }
        decimal? SeasonMaterialEstimateTon { get; set; }
    }
}