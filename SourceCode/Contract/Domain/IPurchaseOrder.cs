﻿using System;

namespace Contract.Domain
{
    public interface IPurchaseOrder
    {
        int Id { get; set; }
        string PurchaseOrderNumber { get; set; }
        int CustomerId { get; set; }
        int? QuantityOfPO { get; set; }
        float? QuantityDispatched { get; set; }
        float? QuantityOfPOOnOrder { get; set; }
        float? QuantityOfPORemaining { get; set; }
        IMaterialInfo Material { get; set; }
        int MaterialId { get; set; }
        string MaterialDescription { get; set; }
        bool IsComplete { get; set; }
        bool IsHeld { get; set; }
        DateTime? EffectiveDate { get; set; }
        DateTime CreateTime { get; set; }
        DateTime? UpdateTime { get; set; }
        bool IsActive { get; set; }
        string Comment { get; set; }
        int SeasonId { get; set; }
        int CompanyId { get; set; }
        string DataA { get; set; }
        string DataB { get; set; }
        string DataC { get; set; }
        float? BalancePerOrder { get; set; }

    }
}