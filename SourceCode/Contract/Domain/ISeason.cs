﻿using System;

namespace Contract.Domain
{
    public interface ISeason
    {
        string Prefix { get; set; }
        DateTime EndDate { get; set; }
        DateTime StartDate { get; set; }
        int SeasonId { get; set; }
        bool IsActive { get; set; }
    }
}