﻿namespace Contract.Domain
{
    public interface IOrderKey
    {
        int OrderNumber { get; set; }
        int Id { get; set; }
    }
}