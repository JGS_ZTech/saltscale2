﻿namespace Contract.Domain
{
    public interface IAccountingInfo
    {
        int AccountingCustomerId { get; set; }
        string AccountingCustomerCode { get; set; }
    }
}
