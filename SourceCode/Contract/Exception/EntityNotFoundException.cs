﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Runtime.Serialization;

namespace Contract.Exception
{
    [Serializable]
    public class EntityNotFoundException : DataException
    {
        private Type _entityType;
        private object[] _keyValues;
        private ReadOnlyCollection<object> _keyValuesCollection;

        public EntityNotFoundException(string message, Type entityType, params object[] key)
            : base(message)
        {
            _entityType = entityType;
            _keyValues = CloneKey(key);
        }

        public EntityNotFoundException(Type entityType, params object[] key)
            : this(GetMessage(entityType, key), entityType, key)
        {
        }

        public EntityNotFoundException(string message, Type entityType, object[] key, System.Exception innerException)
            : base(message, innerException)
        {
            _entityType = entityType;
            _keyValues = CloneKey(key);
        }

        public EntityNotFoundException(Type entityType, object[] key, System.Exception innerException)
            : this(GetMessage(entityType, key), entityType, key, innerException)
        {
        }

        protected EntityNotFoundException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            info.AddValue(SK.EntityType, _entityType, SK.EntityTypeType);
            info.AddValue(SK.KeyValues, _keyValues, SK.KeyValuesType);
        }

        public Type EntityType
        {
            get { return _entityType; }
        }

        public IReadOnlyList<object> KeyValues
        {
            get { return _keyValuesCollection ?? (_keyValuesCollection = new ReadOnlyCollection<object>(_keyValues)); }
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            _entityType = info.GetValue(SK.EntityType, SK.EntityTypeType) as Type;
            _keyValues = info.GetValue(SK.KeyValues, SK.KeyValuesType) as object[];
        }

        private static string GetMessage(Type entityType, object[] key)
        {
            return string.Concat("Entity '", entityType.FullName, "' with key (", string.Join(", ", key),
                ") was not found.");
        }

        private static object[] CloneKey(object[] key)
        {
            if (key == null) throw new ArgumentNullException("key");
            if (key.Length <= 0) throw new ArgumentException("Empty key is not valid.", "key");
            System.Diagnostics.Contracts.Contract.EndContractBlock();

            var copy = new object[key.Length];
            key.CopyTo(copy, 0);
            return copy;
        }

        private static class SK
        {
            public const string EntityType = "EntityType";

            public const string KeyValues = "KeyValues";
            public static readonly Type EntityTypeType = typeof (Type);
            public static readonly Type KeyValuesType = typeof (object[]);
        }
    }
}