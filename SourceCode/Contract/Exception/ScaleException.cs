﻿using System.IO.Ports;

namespace Contract.Exception
{
    public class ScaleException: System.Exception
    {
        private readonly SerialError _errorCode;

        public ScaleException()
        {
            
        }
        public ScaleException(string message, SerialError errorCode)
            : base(message)
        {
            _errorCode = errorCode;
        }
    }
}