using System;
using System.Data;
using System.Runtime.Serialization;

namespace Contract.Exception
{
    [Serializable]
    public class UniqueConstraintException : DataException
    {
        public UniqueConstraintException() { }
        public UniqueConstraintException(string message) : base(message) { }
        public UniqueConstraintException(string message, System.Exception innerException) : base(message, innerException) { }
        protected UniqueConstraintException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}