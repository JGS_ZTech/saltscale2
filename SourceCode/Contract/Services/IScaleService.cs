﻿using System.Collections.Generic;
using Contract.Domain;

namespace Contract.Services
{
    public interface IScaleService
    {
        IReadOnlyList<IScaleInfo> GetAvailableScales(string site);
        void UpdateScales(IEnumerable<IScaleInfo> scales);
    }

    public interface IScaleManager
    {
        void StartScaling(IScaleInfo scale);
        void StopScaling();
        List<int> GetAvailableComPorts();
        bool IsSelectedScaleComPortExistOnComputer();
        bool IsScaleComPortAccessibleForReading(int serialComPort);
        void Dispose();
    }
}