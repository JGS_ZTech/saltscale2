﻿using System.Collections.Generic;
using Contract.Domain;

namespace Contract.Services
{
    public interface IContactService
    {
        IContactData GetContactData(int contactId, int seasonId);
        int SaveContact(IContactData contactData, int seasonId);
        void DeleteContact(int contactId, int seasonId);
        IContactData GetNewContact(int seasonId);
        IReadOnlyList<IContactInfo> GetFilteredContacts(string filter, int seasonId, bool hideCustomers = false, bool hideTruckOwners = false, bool hideOthers = false);
        bool CanDeleteContact(int contactId, int seasonId);
    }
}