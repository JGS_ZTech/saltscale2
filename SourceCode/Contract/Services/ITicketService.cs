﻿using System;
using System.Collections.Generic;
using Contract.Domain;

namespace Contract.Services
{
    public interface ITicketService
    {
        IReadOnlyList<ITicketInfo> GetTicketsInfo(int seasonId, int? customerId = null, int? orderId = null, bool? cashTransactionTickets = null);
        void VoidTicket(ITicketInfo ticket);
        void VoidTicket(ICashTicketInfo ticket);
        ITicketData GetNewOrderTicket(int? orderId, int truckId, int seasonId);
        ITicketData GetNewCashTicket(int seasonId, string defaultSite, ICashPurchase cashPurchase);
        bool IsTicketExist(int ticketNumber, int? ticketId, int seasonId);
        int SaveOrderTicket(ITicketData ticket);
        int SaveCashTicket(ITicketData ticket, ICashPurchase cashPurchase);
        IReadOnlyList<ICashTicketInfo> GetCashTicketsInfo(int seasonId);
        int GetNextTicketNumber(int companyId);
        List<ITicketExportData> GetDataForExport(string companyName, DateTime fromDate, DateTime toDate, TicketExportCondition condition, string connectionString);
        void MarkAsExported(IEnumerable<int> tickets, string connectionString);
        float GetTonsShippedToday(int seasonId);
    }
}