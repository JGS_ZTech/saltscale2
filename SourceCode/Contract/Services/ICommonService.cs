﻿using System.Collections.Generic;
using Contract.Domain;
using System.Collections.ObjectModel;

namespace Contract.Services
{
    public interface ICommonService
    {
        IReadOnlyList<ISeason> GetSeasons();
        IReadOnlyList<ICompanyInfo> GetCompanies();
        IReadOnlyList<ISiteInfo> GetSites(int seasonId);
        ObservableCollection<IReportingEntity> GetReportingEntities(int season);
        ObservableCollection<IReportingEntityL2> GetReportingEntitiesL2(int season);
        ObservableCollection<IReportingEntityL3> GetReportingEntitiesL3(int season);
        ISeason GetSeasonById(int seasonId);
        string GetConfigItem(string companyName, string itemName);
        bool AuthorizeUser(string password);
        IReadOnlyList<ICompanyData> GetCompaniesData();
        ICompanyData GetCompanyData(int id);
        void SaveCompany(ICompanyData companyData);
        void SaveSite(ISiteInfo site);
        ISiteInfo GetSite(int siteId, int seasonId);
        ISiteInfo GetSite(string siteName, int seasonId);
        void MoveToNewSeason(ISeason selectedSeason, string seasonName);
        bool IsSeasonExist(string seasonName);
        ISeason GetActiveSeason();
        IReadOnlyList<ICompanyInfo> GetCompanies(string connectionString);
        ISeason GetActiveSeason(string connectionString);
    }
}