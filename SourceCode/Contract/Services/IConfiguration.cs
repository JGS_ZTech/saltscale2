﻿using System;
using System.Collections.Generic;
using Contract.Domain;

namespace Contract.Services
{
    public interface IConfiguration
    {
        string SaltScaleLocalComputerSettingsFilePath { get; }
        string saltScaleReplicationHostServer { get; }
        string DefaultScaleNameLocalComputer { get; set;}
        string DefaultPrinterNameLocalComputer { get; set; }
        string DefaultSiteName { get; set; }


        string DefaultScaleName { get; }

        string SelectedScaleName { get; set; }
        ISiteConfig SelectedSite { get; set; }

        ISiteConfig Site { get; }
        bool IsManualMode { get; set; }
        string ConnectionString { get; set; }
        
        List<ISiteConfig> ExistingSites { get; }
        int Season { get; }
        DateTime LastOrderUpdate { get; set; }
        int OrdersRefreshTimeout { get; }

        string DispatchOrderGridSortColumn { get; set; }

        void UpdateDefaultScale(IScaleInfo scale, bool isManual);
        string ConvertConnectionString(string connectionString);
    }
}