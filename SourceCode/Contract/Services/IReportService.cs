﻿using System.Collections.Generic;
using Contract.Domain;

namespace Contract.Services
{
    public interface IReportService
    {
        List<ICustomerReport> GetCustomerReport(int seasonId);

        List<ITruckReport> GetTruckReport(int seasonId);

        List<IMunicipalReport> GetMunicipalReport(int seasonId);

        List<IReportConfiguration> GetReportConfigurationList(int seasonId, int siteId);

        void SaveReportConfiguration(IReportConfiguration selectedReportConfiguration);

        bool DeleteReport(int reportConfigId);
        List<ICustomerExportData> GetCustomerExportData(int seasonId);

    }
}