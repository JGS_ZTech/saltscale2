﻿using System.Collections.Generic;
using Contract.Domain;
using System.Collections.ObjectModel;

namespace Contract.Services
{
    public interface IBidService
    {
        IBid GetBidData(int bidId, int seasonId);
        IBidInfo GetCustomerBidInformation(int seasonId, int customerId);
        ObservableCollection<IBidInfo> GetCustomerBidInformationList(int seasonId, int customerId);
        int SaveBid(ObservableCollection<IBidInfo> bidData, int seasonId);
        string VerifyBidBeforeSave(ObservableCollection<IBidInfo> bidList, int seasonId);
        IBidInfo AddEmptyRow(int companyId, string companyFullName, int siteId, string siteName, int seasonId, int customerId);
        bool IsBidMaterialOrdersComplete(IBidInfo bidInfo);
        bool IsBidMaterialPurchaseOrdersComplete(IBidInfo bidInfo);
        bool IsBidMaterialPurchaseOrdersAndOrderComplete(IBidInfo bidInfo);
        bool DeleteRow(IBidInfo bidInfo);
    }
}
