﻿using System.Collections.Generic;
using DataAccess;

namespace Contract.Services
{
    public interface IUserService
    {
        bool Authenticate(string userName, string password);
        ICollection<User> GetAll();
        User GetByUsername(string username);

        void DefaultUser();
    }
}
