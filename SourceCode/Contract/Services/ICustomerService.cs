﻿using System.Collections.Generic;
using Contract.Domain;
using System.Collections.ObjectModel;

namespace Contract.Services
{
    public interface ICustomerService
    {
        ICustomerAddress GetCustomerAddress(int customerId, int seasonId);
        List<IOrderKey> GetCustomerPendingOrders(int customerId, int seasonId);
        List<IPurchaseOrderKey> GetCustomerPendingPurchaseOrders(int customerId, int seasonId);
        List<IConfirmationNumberKey> GetCustomerConfirmationNumbers(int customerId, int seasonId);
        List<ICustomerLine> GetCustomerList(bool shippingCustomers, bool entityCustomers, int seasonId);
        ICustomerAddress GetNewCustomer(int seasonId, string selectedSiteName);
        bool DeleteCustomer(int customerId);
        List<ICustomerLine> GetFilteredCustomers(string filter, bool shippingCustomers, bool entityCustomers, int seasonId);
        void SaveCustomer(ICustomerAddress customer, int seasonId);
        List<int> GetReportingBranches(int customerId);
        List<int> GetShippingLocations(int season, int customerId);
        ICustomerInfo GetCustomerInfo(int customerId);
        List<ICustomerLine> GetCustomersForSelector(string filter, int seasonId);
        List<IGpCustomer> GetGpCustomers(string connectionString, int seasonId, bool allCustomers = true, bool gpCustomers = false, bool nonGpCustomers = false);
        int AddNewAccountingInfo(string newAccountingCustomerCode, string connectionString);
        void UpdateGpId(IGpCustomer gpCustomer, int seasonId, string connectionString);
        void UpdateGpId(int customerId, int greatPlainId, int seasonId, string connectionString);
        ObservableCollection<ICustomerSeasonEstimate> GetEntityHierarchySeasonEstimate(int seasonId);
        ObservableCollection<ICustomerSeasonEstimate> GetCustomerSeasonEstimate(int seasonId, int? reportingId = null);
        void UpdateSeasonEstimate(ICustomerSeasonEstimate seasonEstimate, int seasonId, string connectionString);
        List<IAccountingInfo> GetAccountingCustomerCodes(string customerCode, string connectionString);
        
    }
}