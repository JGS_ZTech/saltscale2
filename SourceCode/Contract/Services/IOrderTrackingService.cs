﻿namespace Contract.Services
{
    public interface IOrderTrackingService
    {
        void StartTracking();
        void StopTracking();
    }
}