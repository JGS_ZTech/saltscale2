﻿using System.Collections.Generic;
using Contract.Domain;

namespace Contract.Services
{
    public interface IMaterialService
    {
        List<IMaterialInfo> GetMaterials(int seasonId);
        List<IMaterialInfo> GetMaterialsOfCustomer(int seasonId, int customerId);
        IReadOnlyList<IMaterialData> GetMaterialList(int seasonId);
        IMaterialData GetMaterial(int seasonId, int? materialId);
        IMaterialInfo GetMaterial(int seasonId, int materialId);
        IMaterialInfo GetInactiveMaterial(int? materialId);
        void DeleteMaterial(IMaterialData material);
        int SaveMaterial(IMaterialData material);
    }
}