﻿using System.Collections.Generic;
using Contract.Domain;

namespace Contract.Services
{
    public interface IOrderService
    {
        List<IOrderSummary> GetFilteredOrders(string filter, int seasonId);
        List<IOrderSummary> GetFilteredOrders(string filter, int customerId, int seasonId);
        List<IOrderSummary> GetFilteredOrders(string filter, int customerId, int seasonId, string status);
        void SaveOrder(IOrder order);
        void SaveOrder_Reopen(IOrder order);
        bool DeleteOrder(int orderId);
        List<IOrderSummary> GetOrders(int customerId, int seasonId);
        IOrder GetNewOrder(int seasonId, int customerId, string siteName);
        IOrder GetOrder(int seasonId, int orderId);
        bool IsPurchaseOrderQuantityAcceptable(int PurchaseOrderId, string poNumber, int materialId, int orderId, int quantity, int customerId);
        double PurchaseOrderInOverage(int PurchaseOrderId, string poNumber, int materialId, int orderId, int quantity, int customerId);
        double PurchaseOrderOverageValue(int PurchaseOrderId, string poNumber, int materialId, int orderId, int quantity, int customerId);
        bool IsPOValid(string poNumber, int materialId, int orderId, int customerId);
        void CloseCompletedOrders(int seasonId);
        void CloseRemainingOrders(float tons, int seasonId);
        float GetOrderBalance(int orderId, int seasonId);
        float GetOrderRequestedTotal(int orderId, int seasonId);
        float GetSitePOOverLimit(int seasonId);
        List<int?> GetOrderNumbersForConfNumber(int confNumber, int orderId, int seasonId);
    }
}