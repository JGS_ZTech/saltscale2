﻿using System.Collections.Generic;
using Contract.Domain;

namespace Contract.Services
{
    public interface ICashPurchaseService
    {
        ICashPurchase GetCashPurchase(int id, bool isComplete);
        List<ICashPurchase> GetCheckInList(int seasonId);
        List<ICashPurchase> GetReCheckInList(int seasonId, string siteName);
        ICashPurchase GetNewCashPurchase(int seasonId, string siteName);
        void SaveCashPurchase(ICashPurchase cashPurchase);
        bool DeleteCashPurchase(int cashPurchaseId);
    }
}