﻿using System.Collections.Generic;
using Contract.Domain;

namespace Contract.Services
{
    public interface ITruckService
    {
        List<ITruckInfo> GetFilteredTrucks(string filter, int seasonId);
        List<ITruckOwnerInfo> GetFilteredTruckOwners(string filter, int seasonId);
        ITruckData GetNewTruck(int seasonId);
        void SaveTruck(ITruckData truck);
        ITruckData GetTruck(int truckId, int seasonId);
        void DeleteTruck(int truckId);
        ITruckOwnerData GetTruckOwner(int ownerId, int seasonId);
        int SaveTruckOwner(ITruckOwnerData owner, int seasonid);
        void DeleteTruckOwner(int ownerId);
        ITruckOwnerData GetNewTruckOwner(int seasonId);
    }
}