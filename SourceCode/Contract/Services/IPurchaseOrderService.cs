﻿using System.Collections.Generic;
using Contract.Domain;

namespace Contract.Services
{
    public interface IPurchaseOrderService
    {
        IPurchaseOrder GetPurchaseOrder(int id);
        IPurchaseOrder GetPurchaseOrder(string PONumber, int CustomerId);
        List<IPurchaseOrder> GetPurchaseOrderList(int customerId, int seasonId);
        List<IPurchaseOrder> GetFilteredPurchaseOrders(string filter, int customerId, int seasonId);
        List<IPurchaseOrder> GetFilteredPurchaseOrders(string filter, int customerId, int seasonId, string status);
        IPurchaseOrder GetNewPurchaseOrder(int seasonId, int customerId);
        void SavePurchaseOrder(IPurchaseOrder purchaseOrder, int customerId);
        bool DeletePurchaseOrder(int purchaseOrderId);
        decimal GetPurchasedAmount(string purchaseOrderNumber, List<int> shippingLocationIdList);
        decimal GetBalanceOfAllPurchaseOrders(int seasonId);
    }
}