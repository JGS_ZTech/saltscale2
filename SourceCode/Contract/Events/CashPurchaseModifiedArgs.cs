﻿namespace Contract.Events
{
    public class CashPurchaseModifiedArgs : ItemModifiedArgs
    {
        public CashPurchaseModifiedArgs(int cashPurchaseId) : base(cashPurchaseId)
        {
        }
    }
}