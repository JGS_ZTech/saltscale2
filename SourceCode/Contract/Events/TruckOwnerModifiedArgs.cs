﻿namespace Contract.Events
{
    public class TruckOwnerModifiedArgs : ItemModifiedArgs
    {
        public TruckOwnerModifiedArgs(int? ownerId) : base(ownerId)
        {
        }
    }
}