﻿using Contract.Domain;

namespace Contract.Events
{
    public class PhoneModifiedArgs:ItemModifiedArgs
    {
        public PhoneModifiedArgs(int phoneNumberId) : base(phoneNumberId)
        {
        }
    }

    public class ContactPhoneModifiedArgs
    {
        private readonly IPhoneData _phone;

        public ContactPhoneModifiedArgs(IPhoneData phone)

        {
            _phone = phone;
        }

        public IPhoneData Phone
        {
            get { return _phone; }
            
        }
    }
}