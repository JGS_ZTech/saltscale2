﻿namespace Contract.Events
{
    public class TicketAddedVoidedArgs : ItemModifiedArgs
    {
        public TicketAddedVoidedArgs(int? ticketID) : base(ticketID)
        {

        }
    }
}
