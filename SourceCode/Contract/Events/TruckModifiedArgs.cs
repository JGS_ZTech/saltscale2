﻿namespace Contract.Events
{
    public class TruckModifiedArgs : ItemModifiedArgs
    {   
        public TruckModifiedArgs(int? truckId) : base(truckId)
        {
        }
    }
}