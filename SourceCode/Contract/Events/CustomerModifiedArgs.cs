﻿namespace Contract.Events
{
    public class CustomerModifiedArgs : ItemModifiedArgs
    {
        public CustomerModifiedArgs(int customerId) : base(customerId)
        {
        }
    }
}