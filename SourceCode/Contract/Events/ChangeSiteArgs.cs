﻿using Contract.Domain;

namespace Contract.Events
{
    public class ChangeSiteArgs
    {
        public ChangeSiteArgs(ISiteConfig site)
        {
            Site = site;
        }

        public ISiteConfig Site { get; private set; }
    }

    public class AuthArgs
    {
        public AuthArgs()
        {
            
        }
    }
}