﻿namespace Contract.Events
{
    public class ReportConfigurationModifiedArgs : ItemModifiedArgs
    {
        public ReportConfigurationModifiedArgs(int? reportConfiguration)
            : base(reportConfiguration)
        {
        }
    }
}