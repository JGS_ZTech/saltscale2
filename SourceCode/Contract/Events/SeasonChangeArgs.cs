﻿using Contract.Domain;

namespace Contract.Events
{
    public class SeasonChangeArgs
    {
        public SeasonChangeArgs(ISeason season)
        {
            Season = season;
        }

        public ISeason Season { get; private set; }
    }
}