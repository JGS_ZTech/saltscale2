﻿using Contract.Domain;
using Contract.Exception;

namespace Contract.Events
{
    public class ScaleChangedArgs
    {
        public IScaleInfo Scale { get; private set; }

        public ScaleChangedArgs(IScaleInfo scale)
        {
            Scale = scale;
        }
    } 
    
    public class DefaultScaleChangedArgs
    {
        public IScaleInfo Scale { get; private set; }

        public DefaultScaleChangedArgs(IScaleInfo scale)
        {
            Scale = scale;
        }
    }
    
    public class ScaleResultsArgs
    {
        public IScaleResult Result { get; private set; }

        public ScaleResultsArgs(IScaleResult result)
        {
            Result = result;
        }
    }
    public class ScaleExceptionsArgs
    {
        public int ScaleId { get; private set; }
        public string Message   { get; private set; }

        public ScaleExceptionsArgs(ScaleException ex)
        {
            Message = ex.Message;
        }
        
        public ScaleExceptionsArgs(int scaleId, System.Exception ex)
        {
            ScaleId = scaleId;
            Message = ex.Message;
        }
    }
}