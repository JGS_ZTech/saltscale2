﻿using Contract.Domain;

namespace Contract.Events
{
    public class NewSeasonProgressArgs
    {
        public NewSeasonProgressArgs(SeasonStep step, string message)
        {
            Step = step;
            Message = message;
        }

        public SeasonStep Step { get; private set; }

        public string Message { get;private set; }
    }
}