﻿using System.Threading;

namespace Contract.Events
{
    public class OrderModifiedArgs:ItemModifiedArgs
    {
        public OrderModifiedArgs(int? orderId) : base(orderId)
        {
        }
    }
}