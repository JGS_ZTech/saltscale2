﻿namespace Contract.Events
{
    public class PurchaseOrderModifiedArgs : ItemModifiedArgs
    {
        public PurchaseOrderModifiedArgs(int purchaseOrderId)
            : base(purchaseOrderId)
        {
        }
    }
}