namespace Contract.Events
{
    public abstract class ItemModifiedArgs
    {
        protected ItemModifiedArgs(int? itemId)
        {
            ItemId = itemId;
        }

        public int? ItemId { get; private set; }
    }
}