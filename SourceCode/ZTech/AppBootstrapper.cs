using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using BusinessLogic;
using Caliburn.Micro;
using Caliburn.Micro.Logging.NLog;
using Contract.Services;
using ZTech.Helpers;
using ZTech.ViewModels;

namespace ZTech
{
    public class AppBootstrapper : BootstrapperBase
    {
        private SimpleContainer _container;

        public AppBootstrapper()
        {
            Initialize();
            LogManager.GetLog = type => new NLogLogger(type);

            ILog log = LogManager.GetLog(typeof(ShellViewModel));
            log.Info($"{Environment.NewLine}{Environment.NewLine}Salt Scale Opening - {Helpers.ProgramInformationUtility.GetPublishedVersion()}{Environment.NewLine}");
        }

        protected override void Configure()
        {
            _container = new SimpleContainer();

            _container.Singleton<IWindowManager, WindowManager>();
            _container.Singleton<IEventAggregator, EventAggregator>();

            #region ViewModels

            _container.PerRequest<ShellViewModel>();
            _container.PerRequest<DispatcherViewModel>();
            _container.PerRequest<CustomerServiceViewModel>();
            _container.PerRequest<NewOrderViewModel>();
            _container.PerRequest<EditOrderViewModel>();
            _container.PerRequest<CustomerSelectViewModel>();
            _container.PerRequest<CustomerViewModel>();
            _container.PerRequest<IOrderTicketViewModel, OrderTicketViewModel>();
            _container.PerRequest<ScaleWidgetViewModel>();
            _container.PerRequest<NewTruckViewModel>();
            _container.PerRequest<EditTruckViewModel>();
            _container.PerRequest<MaterialViewModel>();
            _container.PerRequest<PurchaseOrderViewModel>();
            //_container.PerRequest<ReportViewerViewModel>();
            _container.PerRequest<NewTruckOwnerViewModel>();
            _container.PerRequest<EditTruckOwnerViewModel>();
            _container.PerRequest<IContactWidgetViewModel, ContactWidgetViewModel>();
            _container.PerRequest<ContactPhoneViewModel>();
            _container.PerRequest<TicketViewerViewModel>();
            _container.PerRequest<VoidReasonViewModel>();
            _container.PerRequest<ITruckSelectViewModel, TruckSelectViewModel>();
            _container.PerRequest<CashCustomerViewModel>();
            _container.PerRequest<CashVoidViewModel>();
            _container.PerRequest<CashPurchaseTicketViewModel>();
            _container.PerRequest<PhoneBookViewModel>();
            _container.PerRequest<LoginViewModel>();
            _container.PerRequest<AdministrationViewModel>();
            _container.PerRequest<ScalesViewModel>();
            //_container.PerRequest<ReportPreViewModel>();
            _container.PerRequest<ChangeSiteViewModel>();
            _container.PerRequest<ChangeSeasonViewModel>();
            _container.PerRequest<ReportGeneratorViewModel>();
            _container.PerRequest<DailyOperationViewModel>();
            _container.PerRequest<TicketPreViewModel>();
            _container.PerRequest<ReportCongfigurationNameViewModel>();
            _container.PerRequest<GPCustomerViewModel>();
            _container.PerRequest<TicketExportViewModel>();
            _container.PerRequest<DailyPoNameViewModel>();
            _container.PerRequest<CustomerExportViewModel>();
            _container.PerRequest<SeasonEstimateViewModel>();

            _container.PerRequest<ReportDisplayViewModel>();

            #endregion

            #region Services

            _container.PerRequest<ICommonService, CommonService>();
            _container.PerRequest<IBidService, BidService>();
            _container.PerRequest<IOrderService, OrderService>();
            _container.PerRequest<ITruckService, TruckService>();
            _container.PerRequest<ICustomerService, CustomerService>();
            _container.PerRequest<IMaterialService, MaterialService>();
            _container.PerRequest<IPurchaseOrderService, PurchaseOrderService>();
            _container.PerRequest<IScaleService, ScaleService>();
            _container.PerRequest<ITicketService, TicketService>();
            _container.PerRequest<ICashPurchaseService, CashPurchaseService>();
            _container.PerRequest<IContactService, ContactService>();
            _container.PerRequest<IReportService, ReportService>();
            _container.PerRequest<IUserService, UserService>();

            _container.PerRequest<IScaleManager, ScaleManager>();


            _container.Singleton<IOrderTrackingService, OrderTrackingService>();

            #endregion

            #region External Config File
            System.Configuration.ExeConfigurationFileMap fileMap = new System.Configuration.ExeConfigurationFileMap();
            fileMap.ExeConfigFilename = @"C:\Eastern Salt\SaltScaleLocal.config";
            //System.Configuration.Configuration config = System.Configuration.ConfigurationManager.OpenMappedExeConfiguration(fileMap, System.Configuration.ConfigurationUserLevel.None);

            //var DefaultScaleName = config.Sections["DefaultScaleName"];

            //ConfigXmlDocument config = new ConfigXmlDocument();
            //config.LoadXml(@"c:\SaltScale\Config.xml");



            #endregion

            #region Config

            var configuration = new Configuration();
            _container.Instance<IConfiguration>(configuration);

            try
            {
                System.Configuration.ExeConfigurationFileMap mapLocalComputer = new System.Configuration.ExeConfigurationFileMap { ExeConfigFilename = configuration.SaltScaleLocalComputerSettingsFilePath };
                System.Configuration.Configuration configLocalComputer = System.Configuration.ConfigurationManager.OpenMappedExeConfiguration(mapLocalComputer, System.Configuration.ConfigurationUserLevel.None);

                //var x = configLocalComputer.GetSection("configurationLocalComputer");

                System.Xml.Linq.XDocument xelement = System.Xml.Linq.XDocument.Load(configuration.SaltScaleLocalComputerSettingsFilePath);

                configuration.DefaultScaleNameLocalComputer = xelement.Descendants("defaultScaleNameLocalComputer").FirstOrDefault().Value;
                configuration.DefaultPrinterNameLocalComputer = xelement.Descendants("defaultPrinterNameLocalComputer").FirstOrDefault().Value;
                 //configuration.DefaultMaterialName = xelement.Descendants("defaultMaterialName").FirstOrDefault().Value;
                configuration.DefaultSiteName = xelement.Descendants("defaultSiteName").FirstOrDefault().Value;

                //System.Xml.XmlDocument xxx = System.Xml.XmlDocument(  //configuration.DefaultPrinterNameLocalComputer = x.SectionInformation.
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            configuration.SelectedScaleName = configuration.DefaultScaleName;
            //configuration.saltScaleReplicationHostServer = configuration.saltScaleReplicationHostServer;

            configuration.SelectedSite = configuration.Site;
            var allsites = configuration.ExistingSites;

            if (configuration.SelectedSite == null && allsites.Count == 1)
            {
                configuration.SelectedSite = allsites[0];
            }
            
            if (configuration.SelectedSite == null || string.IsNullOrWhiteSpace(configuration.SelectedSite.ConnectionString))
                throw new Exception("Site configuration not found.");
            //ToDo Error log

            configuration.ConnectionString = configuration.SelectedSite.ConnectionString;


            //configuration.IsManualMode = configuration.ScaleType.Equals("Manual", StringComparison.InvariantCultureIgnoreCase) || !configuration.SelectedSite.IsDefault;
            
            #endregion

            Application.Current.Properties["IsLogged"] = false;
            Application.Current.Properties["UserName"] = String.Empty;

            #region Binders

            MessageBinder.SpecialValues.Add("$pressedkey", context =>
            {
                var keyArgs = context.EventArgs as KeyEventArgs;

                if (keyArgs != null)
                {
                    return keyArgs.Key;
                }

                return null;
            });

            #endregion
        }

        protected override object GetInstance(Type service, string key)
        {
            object instance = _container.GetInstance(service, key);
            if (instance != null)
                return instance;

            throw new InvalidOperationException("Could not locate any instances.");
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return _container.GetAllInstances(service);
        }

        protected override void BuildUp(object instance)
        {
            _container.BuildUp(instance);
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<ShellViewModel>();
        }

        protected override void OnUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            ILog log = LogManager.GetLog(typeof (ShellViewModel));

            var message = e.Exception.GetAllMessages();

            log.Error(new Exception(message));
            
            Execute.OnUIThreadAsync(ShowMessage);
            base.OnUnhandledException(sender, e);
        }

        private void ShowMessage()
        {
            MessageBox.Show("Unhandled exception has occured in application - addtl logging ToDo ");
            //ToDo Error log
        }
    }
}