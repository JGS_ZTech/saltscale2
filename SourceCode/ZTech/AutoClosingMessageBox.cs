﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Input;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZTech
{
    public class AutoClosingMessageBox
    {
        string _text;
        string _caption;
        System.Windows.MessageBoxButton _MsgBoxButtton;
        System.Windows.MessageBoxImage _MsgBoxImage;
        System.Threading.Timer _timeoutTimer;
        MessageBoxResult _messageBoxResult;
        AutoClosingMessageBox(string text, string caption, System.Windows.MessageBoxButton MsgBoxButtton, System.Windows.MessageBoxImage MsgBoxImage, int timeout)
        {
            _text = text;
            _caption = caption;
            _MsgBoxButtton = MsgBoxButtton;
            _MsgBoxImage = MsgBoxImage;
            _timeoutTimer = new System.Threading.Timer(OnTimerElapsed, null, timeout, System.Threading.Timeout.Infinite);
            using (_timeoutTimer)
                _messageBoxResult = MessageBox.Show(text, caption, MsgBoxButtton, MsgBoxImage);
        }
        public static MessageBoxResult Show(string text, string caption, System.Windows.MessageBoxButton MsgBoxButtton, System.Windows.MessageBoxImage MsgBoxImage, int timeout)
        {
            return new AutoClosingMessageBox(text, caption, MsgBoxButtton, MsgBoxImage, timeout)._messageBoxResult;
        }
        void OnTimerElapsed(object state)
        {
            IntPtr mbWnd = FindWindow("#32770", _caption); // lpClassName is #32770 for MessageBox
            if (mbWnd != IntPtr.Zero)
                SendMessage(mbWnd, WM_CLOSE, IntPtr.Zero, IntPtr.Zero);
            _timeoutTimer.Dispose();
        }
        const int WM_CLOSE = 0x0010;
        [System.Runtime.InteropServices.DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
        [System.Runtime.InteropServices.DllImport("user32.dll", CharSet = System.Runtime.InteropServices.CharSet.Auto)]
        static extern IntPtr SendMessage(IntPtr hWnd, UInt32 Msg, IntPtr wParam, IntPtr lParam);
    }
}
