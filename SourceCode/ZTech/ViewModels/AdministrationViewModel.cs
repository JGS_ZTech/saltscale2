﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Caliburn.Micro;
using Contract.Domain;
using Contract.Events;
using Contract.Services;
using ZTech.Controls;
using ZTech.ViewModels.Abstract;
using Action = System.Action;
using System.Configuration;

namespace ZTech.ViewModels
{
    public class AdministrationViewModel : BaseViewModel, IHandle<NewSeasonProgressArgs>
    {
        private readonly IEventAggregator _eventAggregator;
        private readonly ILog _log;
        private readonly int _seasonId;
        private CompanyItem _companyItem;
        private IReadOnlyList<ICompanyInfo> _companyList;
        private string _companySettingsErrors;
        private bool _enableCompany;
        private List<string> _printerList;
        private CompanyItem _seasonCompanyItem;
        private string _seasonErrors;
        private string _seasonInformation;
        private string _seasonName;
        private List<ISeason> _seasons;
        private ICompanyInfo _selectedCompany;
        private ICompanyInfo _selectedCompanyForSeason;
        private ISeason _selectedSeason;
        private IReadOnlyList<SiteItem> _siteList;
        private readonly string _defaultSite;
        private SiteItem _selectedSite;
        private bool _enableSite;
        private ICompanyInfo _siteDefaultCompany;
        private List<ICompanyInfo> _siteCompanyList;
        private string _siteSettingsErrors;
        private string _seasonLog;
        private bool _isSeasonProgressVisible;
        private bool _isBusy;

        public AdministrationViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            _log = LogManager.GetLog(typeof (AdministrationViewModel));
            _seasonId = (int) Application.Current.Properties["SeasonId"];
            _defaultSite = IoC.Get<IConfiguration>().SelectedSite.Name;
            DisplayName = "Administration";
        }

        public IReadOnlyList<SiteItem> SiteList
        {
            get { return _siteList; }
            set
            {
                if (Equals(value, _siteList)) return;
                _siteList = value;
                NotifyOfPropertyChange(() => SiteList);
            }
        }

        public string CompanySettingsErrors
        {
            get { return _companySettingsErrors; }
            set
            {
                if (value == _companySettingsErrors) return;
                _companySettingsErrors = value;
                NotifyOfPropertyChange(() => CompanySettingsErrors);
            }
        }

        public string SiteSettingsErrors
        {
            get { return _siteSettingsErrors; }
            set
            {
                if (value == _siteSettingsErrors) return;
                _siteSettingsErrors = value;
                NotifyOfPropertyChange(() => SiteSettingsErrors);
            }
        }

       public IReadOnlyList<ICompanyInfo> CompanyList
        {
            get { return _companyList; }
            set
            {
                if (Equals(value, _companyList)) return;
                _companyList = value;
                NotifyOfPropertyChange(() => CompanyList);
            }
        }

        public ICompanyInfo SelectedCompany
        {
            get { return _selectedCompany; }
            set
            {
                if (Equals(value, _selectedCompany)) return;
                _selectedCompany = value;
                NotifyOfPropertyChange(() => SelectedCompany);
                InitializeCompany();
            }
        }

        public CompanyItem CompanyItem
        {
            get { return _companyItem; }
            set
            {
                if (Equals(value, _companyItem)) return;
                _companyItem = value;
                NotifyOfPropertyChange(() => CompanyItem);
            }
        }

        public List<string> PrinterList
        {
            get { return _printerList; }
            set
            {
                if (Equals(value, _printerList)) return;
                _printerList = value;
                NotifyOfPropertyChange(() => PrinterList);
            }
        }

        public bool EnableCompany
        {
            get { return _enableCompany; }
            set
            {
                if (value.Equals(_enableCompany)) return;
                _enableCompany = value;
                NotifyOfPropertyChange(() => EnableCompany);
            }
        }

        public string SeasonInformation
        {
            get { return _seasonInformation; }
            set
            {
                if (value == _seasonInformation) return;
                _seasonInformation = value;
                NotifyOfPropertyChange(() => SeasonInformation);
            }
        }

        public ICompanyInfo SelectedCompanyForSeason
        {
            get { return _selectedCompanyForSeason; }
            set
            {
                if (Equals(value, _selectedCompanyForSeason)) return;
                _selectedCompanyForSeason = value;
                NotifyOfPropertyChange(() => SelectedCompanyForSeason);

                InitializeSeasonCompany();
            }
        }

        public CompanyItem SeasonCompanyItem
        {
            get { return _seasonCompanyItem; }
            set
            {
                if (Equals(value, _seasonCompanyItem)) return;
                _seasonCompanyItem = value;
                NotifyOfPropertyChange(() => SeasonCompanyItem);
            }
        }

        public string SeasonErrors
        {
            get { return _seasonErrors; }
            set
            {
                if (value == _seasonErrors) return;
                _seasonErrors = value;
                NotifyOfPropertyChange(() => SeasonErrors);
            }
        }

        public List<ISeason> Seasons
        {
            get { return _seasons; }
            set
            {
                if (Equals(value, _seasons)) return;
                _seasons = value;
                NotifyOfPropertyChange(() => Seasons);
            }
        }

        public ISeason SelectedSeason
        {
            get { return _selectedSeason; }
            set
            {
                if (Equals(value, _selectedSeason)) return;
                _selectedSeason = value;
                NotifyOfPropertyChange(() => SelectedSeason);
            }
        }

        public string SeasonName
        {
            get { return _seasonName; }
            set
            {
                if (value == _seasonName) return;
                _seasonName = value;
                NotifyOfPropertyChange(() => SeasonName);
            }
        }

        public SiteItem SelectedSite
        {
            get { return _selectedSite; }
            set
            {
                if (Equals(value, _selectedSite)) return;
                _selectedSite = value;
                NotifyOfPropertyChange(() => SelectedSite);
            }
        }

        public bool EnableSite
        {
            get { return _enableSite; }
            set
            {
                if (value.Equals(_enableSite)) return;
                _enableSite = value;
                NotifyOfPropertyChange(() => EnableSite);
            }
        }

        public ICompanyInfo SiteDefaultCompany
        {
            get { return _siteDefaultCompany; }
            set
            {
                if (Equals(value, _siteDefaultCompany)) return;
                _siteDefaultCompany = value;
                NotifyOfPropertyChange(() => SiteDefaultCompany);

                if (SelectedSite != null)
                    SelectedSite.DefaultCompanyId = value.Id;
            }
        }

        public List<ICompanyInfo> SiteCompanyList
        {
            get { return _siteCompanyList; }
            set
            {
                if (Equals(value, _siteCompanyList)) return;
                _siteCompanyList = value;
                NotifyOfPropertyChange(() => SiteCompanyList);
            }
        }

        private void InitializeCompany()
        {
            if (SelectedCompany == null) return;

            CompanySettingsErrors = null;

            ICompanyData data = IoC.Get<ICommonService>().GetCompanyData(SelectedCompany.Id);
            CompanyItem = new CompanyItem(data);
        }


        private void InitializeSeasonCompany()
        {
            if (SelectedCompanyForSeason == null) return;

            SeasonErrors = null;

            ICompanyData data = IoC.Get<ICommonService>().GetCompanyData(SelectedCompanyForSeason.Id);
            SeasonCompanyItem = new CompanyItem(data);
        }

        protected override void OnInitialize()
        {
            base.OnInitialize();
            IsBusy = true;
            var task = new Task(InitTask);
            task.Start();
        }

        private void InitTask()
        {
            try
            {
                InitializePrinterList();

                CompanyList = IoC.Get<ICommonService>().GetCompanies();

                var defaultCompanyItem = new CompanyItem
                {
                    Id = -1,
                    Name = string.Empty
                };
                SiteCompanyList = new List<ICompanyInfo> {defaultCompanyItem};

                SiteCompanyList.AddRange(CompanyList);

                SiteList = IoC.Get<ICommonService>().GetSites(_seasonId).Select(s => new SiteItem(s)).ToList();

                var selectedSite = string.IsNullOrWhiteSpace(_defaultSite)
                    ? SiteList.FirstOrDefault()
                    : SiteList.FirstOrDefault(s => s.Name == _defaultSite);
                if (selectedSite != null)
                    SelectedSite = selectedSite;

                ICompanyInfo company = (selectedSite != null && selectedSite.DefaultCompanyId.HasValue)
                    ? CompanyList.FirstOrDefault(c => c.Id == selectedSite.DefaultCompanyId.Value)
                    : CompanyList.FirstOrDefault();

                if (company != null)
                {
                    SelectedCompany = company;
                    SelectedCompanyForSeason = company;
                }

                var siteCompany = (selectedSite != null && selectedSite.DefaultCompanyId.HasValue)
                    ? CompanyList.FirstOrDefault(c => c.Id == selectedSite.DefaultCompanyId.Value)
                    : defaultCompanyItem;

                SiteDefaultCompany = siteCompany ?? defaultCompanyItem;

                EnableCompany = CompanyList.Count > 1 || SelectedCompany == null;
                EnableSite = SiteList.Count > 1 || SelectedSite == null;

                InitializeSeasons();

                SeasonInformation = "Information will be copied from the selected season to the new season:" +
                                    Environment.NewLine + "Addresses" +
                                    Environment.NewLine + "Contacts" +
                                    Environment.NewLine + "Customers" +
                                    Environment.NewLine + "Entities" +
                                    Environment.NewLine + "Materials" +
                                    Environment.NewLine + "Phone Numbers" +
                                    Environment.NewLine + "Trucks & Weight" +
                                    Environment.NewLine + "Truck Owners";
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(
                            string.Format(
                                "Initialization failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                                ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));

                    TryClose(false);
                };

                action.OnUIThreadAsync();
            }
            finally
            {
                IsBusy = false;
            }
        }

        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                if (value.Equals(_isBusy)) return;
                _isBusy = value;
                NotifyOfPropertyChange(() => IsBusy);
            }
        }

        private void InitializeSeasons()
        {
            IReadOnlyList<ISeason> seasons = IoC.Get<ICommonService>().GetSeasons();
            Seasons = new List<ISeason>(seasons);

            ISeason season = Seasons.FirstOrDefault(s => s.IsActive);
            if (season != null)
                SelectedSeason = season;
        }

        private void InitializePrinterList()
        {
            PrinterSettings.StringCollection installedPrinters = PrinterSettings.InstalledPrinters;

            PrinterList = new List<string>();
            if (installedPrinters.Count > 0)
            {
                foreach (object printer in installedPrinters)
                {
                    PrinterList.Add(printer.ToString());
                }
            }
        }

        public void SaveCompany()
        {
            try
            {
                if (SelectedCompany != null && CompanyItem != null)
                {
                    if (!ValidateCompany())
                        return;

                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show("Are you sure, you want to apply changes to Company configuration?",
                            "Confirmation",
                            MessageBoxButton.YesNo, MessageBoxImage.Question));

                    if (messageBoxResult == MessageBoxResult.No)
                        return;

                    IoC.Get<ICommonService>().SaveCompany(CompanyItem);

                    TryClose(true);
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "Company saving failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };

                action.OnUIThreadAsync();
            }
        }

        public void SaveSite()
        {
            try
            {
                if (SelectedSite != null)
                {
                    if (!ValidateSite())
                        return;

                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show("Are you sure, you want to apply changes to Site configuration?",
                            "Confirmation",
                            MessageBoxButton.YesNo, MessageBoxImage.Question));

                    if (messageBoxResult == MessageBoxResult.No)
                        return;

                    if (SelectedSite.DefaultCompanyId.HasValue && SelectedSite.DefaultCompanyId <= 0)
                        SelectedSite.DefaultCompanyId = null;

                    IoC.Get<ICommonService>().SaveSite(SelectedSite);

                    TryClose(true);
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "Site saving failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };

                action.OnUIThreadAsync();
            }
        }

        public void SaveComputerSettings()
        {
            ExeConfigurationFileMap map = new ExeConfigurationFileMap { ExeConfigFilename = "EXECONFIG_PATH" };
            Configuration config = ConfigurationManager.OpenMappedExeConfiguration(map, ConfigurationUserLevel.None);
        }

        public void DoneSeasonOperations()
        {
            try
            {
                if (!ValidateSeason())
                    return;

                MessageBoxResult messageBoxResult =
                    (MessageBox.Show("Are you sure, you want to create new Season and close previous season?",
                        "Confirmation",
                        MessageBoxButton.YesNo, MessageBoxImage.Question));

                if (messageBoxResult == MessageBoxResult.No)
                    return;

                IsSeasonProgressVisible = true;
                IsBusy = true;
                var cancel = new CancellationToken();
                
                Task.Factory.StartNew(MoveToNewSeasonTask,
                    cancel,
                    TaskCreationOptions.LongRunning,
                    TaskScheduler.Default);
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "Move to new season failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };

                action.OnUIThreadAsync();
            }
        }

        private void MoveToNewSeasonTask()
        {
            try
            {
                IoC.Get<ICommonService>().MoveToNewSeason(SelectedSeason, SeasonName);
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "Move to new season failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));

                    TryClose(false);
                };

                action.OnUIThreadAsync();
            }
            finally
            {
                IsBusy = false;
            }
        }

        public bool IsSeasonProgressVisible
        {
            get { return _isSeasonProgressVisible; }
            set
            {
                if (value.Equals(_isSeasonProgressVisible)) return;
                _isSeasonProgressVisible = value;
                NotifyOfPropertyChange(() => IsSeasonProgressVisible);
            }
        }

        public string SeasonLog
        {
            get { return _seasonLog; }
            set
            {
                if (value == _seasonLog) return;
                _seasonLog = value;
                NotifyOfPropertyChange(() => SeasonLog);
            }
        }

        private bool ValidateCompany()
        {
            CompanySettingsErrors = null;

            var validationResults = new List<ValidationResult>();

            Validator.TryValidateObject(CompanyItem, new ValidationContext(CompanyItem), validationResults);

            if (CompanyItem.FirstTicket < 1)
                validationResults.Add(new ValidationResult("First Ticket # must be started from 1."));

            if (CompanyItem.FirstTicket >= CompanyItem.LastTicket)
                validationResults.Add(new ValidationResult("First Ticket # must be less than Last Ticket #."));

            if (CompanyItem.NextTicket < CompanyItem.FirstTicket || CompanyItem.NextTicket > CompanyItem.LastTicket)
                validationResults.Add(new ValidationResult("Next Ticket # is out of bounds."));

            if (validationResults.Any())
            {
                CompanySettingsErrors = string.Format(
                    "Validation Failed.{0}{1}", Environment.NewLine,
                    string.Join("\n", validationResults.Select(v => v.ErrorMessage)));

                ShowMessage("Validation Errors", CompanySettingsErrors, NotificationType.Warning);

                return false;
            }

            return true;
        }

        private bool ValidateSite()
        {
            SiteSettingsErrors = null;

            var validationResults = new List<ValidationResult>();

            Validator.TryValidateObject(SelectedSite, new ValidationContext(SelectedSite), validationResults);

            if (SelectedSite.TaxRate > 100)
                validationResults.Add(new ValidationResult("Tax must be less than 100."));

            if (SelectedSite.TaxRate < 0)
                validationResults.Add(new ValidationResult("Tax must be greater than 0."));

            //-- 1 = Lowell
            if (SelectedSite.Id != 1)
                if (SelectedSite.ConfNumberFirst == 0)
                {
                    validationResults.Add(new ValidationResult("First Confirmation Number must be greater than 0."));
                }                
                if (SelectedSite.ConfNumberLast == 0)
                {
                    validationResults.Add(new ValidationResult("Last Confirmation Number must be greater than 0."));
                }
                if (SelectedSite.ConfNumberLast <= SelectedSite.ConfNumberFirst)
                {
                    validationResults.Add(new ValidationResult("Last Confirmation Number must be greater than First."));
                }
                if (SelectedSite.ConfNumberNext > SelectedSite.ConfNumberLast 
                    || SelectedSite.ConfNumberFirst > SelectedSite.ConfNumberNext)
                {
                    validationResults.Add(new ValidationResult("Next Confirmation Number is either before the first number or after the last number."));
                }

            if (validationResults.Any())
            {
                SiteSettingsErrors = string.Format(
                    "Validation Failed.{0}{1}", Environment.NewLine,
                    string.Join("\n", validationResults.Select(v => v.ErrorMessage)));

                ShowMessage("Validation Errors", SiteSettingsErrors, NotificationType.Warning);

                return false;
            }

            return true;
        }

        private bool ValidateSeason()
        {
            SeasonErrors = null;

            var validationResults = new List<string>();

            if (SelectedSeason == null)
                validationResults.Add("Season must be selected.");

            if (string.IsNullOrWhiteSpace(SeasonName) || SeasonName.Trim().Length != 2) // 20180713, JGS - It must be 2 digits
                validationResults.Add("Season name length must be the last 2 digits of the ending year of the shipiing season.");

            if (!string.IsNullOrWhiteSpace(SeasonName))
            {
                //var reg = new Regex("^[0-9]{2}-[0-9]{2}");
                var reg = new Regex("^[0-9]{2}"); // 20180713, JGS - We just need the last 2 digits of the shipping year end
                var match = reg.Match(SeasonName);

                if (!match.Success)
                    validationResults.Add("Invalid Season name");

                if (IoC.Get<ICommonService>().IsSeasonExist(SeasonName))
                    validationResults.Add("Season with this name already exist.");
            }

            if (validationResults.Any())
            {
                SeasonErrors = string.Format(
                    "Validation Failed.{0}{1}", Environment.NewLine,
                    string.Join("\n", validationResults));

                ShowMessage("Validation Errors", SeasonErrors, NotificationType.Warning);

                return false;
            }

            return true;
        }

        #region Events

        protected override void OnActivate()
        {
            _eventAggregator.Subscribe(this);
            base.OnActivate();
        }

        protected override void OnDeactivate(bool close)
        {
            _eventAggregator.Unsubscribe(this);
            base.OnDeactivate(close);
        }

        #endregion

        public void Handle(NewSeasonProgressArgs args)
        {
            if (args == null) return;

            switch (args.Step)
            {
                case SeasonStep.Start:
                    SeasonLog = args.Message;
                    break;
                case SeasonStep.End:
                    SeasonLog = SeasonLog + Environment.NewLine + args.Message;
                    _eventAggregator.PublishOnUIThreadAsync(new SeasonChangeArgs(IoC.Get<ICommonService>().GetActiveSeason()));
                    ShowMessage("Information", "Operation Completed.", NotificationType.Information);
                    IsBusy = false;
                    TryClose(true);
                    break;
                case SeasonStep.Warning:
                    SeasonLog = SeasonLog + Environment.NewLine + args.Message;
                    SeasonName = null;
                    InitializeSeasons();
                    _eventAggregator.PublishOnUIThreadAsync(new SeasonChangeArgs(SelectedSeason));
                    IsBusy = false;
                    ShowMessage("Warning", "Operation Completed with warnings.", NotificationType.Warning);
                    break;
                case SeasonStep.FatalException:
                    SeasonLog = SeasonLog + Environment.NewLine + args.Message;
                    InitializeSeasons();
                    _eventAggregator.PublishOnUIThreadAsync(new SeasonChangeArgs(SelectedSeason));
                    ShowMessage("Fatal Exception",
                        "Operation Failed. Please reopen application and try again or contact HelpDesk.",
                        NotificationType.Error);
                    IsBusy = false;
                    TryClose(false);
                     break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }

    #region DTO

    public class CompanyItem : PropertyChangedBase, ICompanyData
    {
        private string _defaultPrinter;
        private int _firstTicket;
        private int _lastTicket;
        private int _nextTicket;

        public CompanyItem()
        {
            
        }
        public CompanyItem(ICompanyData company)
        {
            PrinterTicketName = company.PrinterTicketName;
            Name = company.Name;
            Id = company.Id;
            FirstTicket = company.FirstTicket;
            LastTicket = company.LastTicket;
            NextTicket = company.NextTicket;
            //ExportFile = company.ExportFile;
            //ExportPath = company.ExportPath;
            //BackupPath = company.BackupPath;
        }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Printer is required.")]
        public string PrinterTicketName
        {
            get { return _defaultPrinter; }
            set
            {
                if (value == _defaultPrinter) return;
                _defaultPrinter = value;
                NotifyOfPropertyChange(() => PrinterTicketName);
            }
        }

        public string Name { get; set; }
        public int Id { get; set; }
        public string AddressLineOne { get; set; }
        public string AddressLineTwo { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string PhoneNumberOne { get; set; }
        public string PhoneNumberTwo { get; set; }
        public string FaxNumber { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "First Ticket# is required.")]
        public int FirstTicket
        {
            get { return _firstTicket; }
            set
            {
                if (value == _firstTicket) return;
                _firstTicket = value;
                NotifyOfPropertyChange(() => FirstTicket);
            }
        }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Last Ticket# is required.")]
        public int LastTicket
        {
            get { return _lastTicket; }
            set
            {
                if (value == _lastTicket) return;
                _lastTicket = value;
                NotifyOfPropertyChange(() => LastTicket);
            }
        }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Next Ticket# is required.")]
        public int NextTicket
        {
            get { return _nextTicket; }
            set
            {
                if (value == _nextTicket) return;
                _nextTicket = value;
                NotifyOfPropertyChange(() => NextTicket);
            }
        }

        public string ExportPath { get; set; }
        public string BackupPath { get; set; }
        public string ExportFile { get; set; }
    }

    public class SiteItem : PropertyChangedBase, ISiteInfo
    {
        private decimal _taxRate;
        private int _weightInterval;
        private decimal _defaultMaterialCost;
        private int? _defaultCompanyId;
        private int? _defaultCashCustomerCompanyId;
        private string _fuelSurchargeText;
        private string _taxRateText;

        public SiteItem()
        {
            
        }
        public SiteItem(ISiteInfo siteInfo)
        {
            IsOperatingFromThisLocation = siteInfo.IsOperatingFromThisLocation;
            Name = siteInfo.Name;
            Id = siteInfo.Id;
            WeightInterval = siteInfo.WeightInterval;
            DefaultCompanyId = siteInfo.DefaultCompanyId;
            DefaultCashCustomerCompanyId = siteInfo.DefaultCashCustomerCompanyId;
            DefaultMaterialCost = siteInfo.DefaultMaterialCost;
            SeasonId=siteInfo.SeasonId;
            TaxRate = siteInfo.TaxRate;
            //TaxRateText = TaxRate.HasValue ? TaxRate.Value.ToString("N2") : null;
            FuelSurcharge = siteInfo.FuelSurcharge;
            //FuelSurchargeText = FuelSurcharge.HasValue ? FuelSurcharge.Value.ToString("N2") : null;
            ConfNumberFirst = siteInfo.ConfNumberFirst;
            ConfNumberLast = siteInfo.ConfNumberLast;
            ConfNumberNext = siteInfo.ConfNumberNext;
            POOverLimit = siteInfo.POOverLimit;
        }

        public string TaxRateText
        {
            get { return _taxRateText; }
            set
            {
                if (value == _taxRateText) return;
                _taxRateText = value;
                NotifyOfPropertyChange(() => TaxRateText);

                ///TaxRate = ParseNullDecimalValue(value);
            }
        }

        private decimal? ParseNullDecimalValue(string income)
        {
            if (string.IsNullOrWhiteSpace(income))
                return null;

            income = income.Replace('_', '0');

            decimal result;

            if (decimal.TryParse(income.Trim(), out result))
                return result;

            return null;
        }

        public bool IsOperatingFromThisLocation { get; set; }
        public string Name { get; set; }
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Tax Rate is required.")]
        public decimal TaxRate
        {
            get { return _taxRate; }
            set
            {
                if (value == _taxRate) return;
                _taxRate = value;
                NotifyOfPropertyChange(() => TaxRate);
            }
        }

        public int WeightInterval
        {
            get { return _weightInterval; }
            set
            {
                if (value == _weightInterval) return;
                _weightInterval = value;
                NotifyOfPropertyChange(() => WeightInterval);
            }
        }

        public decimal DefaultMaterialCost
        {
            get { return _defaultMaterialCost; }
            set
            {
                if (value == _defaultMaterialCost) return;
                _defaultMaterialCost = value;
                NotifyOfPropertyChange(() => DefaultMaterialCost);
            }
        }

        public int SeasonId { get; set; }

        public int? DefaultCompanyId
        {
            get { return _defaultCompanyId; }
            set
            {
                if (value == _defaultCompanyId) return;
                _defaultCompanyId = value;
                NotifyOfPropertyChange(() => DefaultCompanyId);
            }
        }

        public int? DefaultCashCustomerCompanyId
        {
            get { return _defaultCashCustomerCompanyId; }
            set
            {
                if (value == _defaultCashCustomerCompanyId) return;
                _defaultCashCustomerCompanyId = value;
                NotifyOfPropertyChange(() => DefaultCashCustomerCompanyId);
            }
        }

        public decimal FuelSurcharge { get; set; }
        public int ConfNumberNext { get; set; }
        public decimal POOverLimit { get; set; }
        public int ConfNumberLast { get; set; }
        public int ConfNumberFirst { get; set; }

        public string FuelSurchargeText
        {
            get { return _fuelSurchargeText; }
            set
            {
                if (value == _fuelSurchargeText) return;
                _fuelSurchargeText = value;
                NotifyOfPropertyChange(() => FuelSurchargeText);

                //FuelSurcharge = ParseNullDecimalValue(value);
            }
        }
    }

    #endregion
}