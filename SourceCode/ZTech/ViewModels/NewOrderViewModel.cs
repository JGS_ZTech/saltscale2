﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Caliburn.Micro;
using Contract.Domain;
using Contract.Events;
using Contract.Exception;
using Contract.Services;
using ZTech.Controls;
using ZTech.ViewModels.Abstract;
using ZTech.ViewModels.ViewObject;
using Action = System.Action;

namespace ZTech.ViewModels
{
    public class NewOrderViewModel : BaseOrderViewModel
    {
        private string _confirmationNumberText;

        public NewOrderViewModel(IEventAggregator eventAggregator) : base(eventAggregator, "New Order Screen")
        {
            _log = LogManager.GetLog(typeof(NewOrderViewModel));
        }

        public int? CustomerId { get; set; }
        public string CompanyNameOfSelectedCustomer { get; set; }

        protected override void InitializeOrder()
        {
            if (!CustomerId.HasValue || CustomerId.Value == 0)
            {
                throw new Exception("Customer not found");
            }

            InitializePo(CustomerId.Value);
            InitializeMaterials(CustomerId.Value); //-- Used for when making a po..?
            InitializeBidInfos(CustomerId.Value);

            Order = IoC.Get<IOrderService>().GetNewOrder(_seasonId, CustomerId.Value, _defaultSite);
            Order.UpdateTime = DateTime.Now;

            SelectedCustomerCompanyName = Order.Customer.Company.Name;


            CustomerAddress = Order.Customer.Name + Environment.NewLine + Order.Customer.Address;

            var defaultPurchaseOrder = PurchaseOrderList.First(p => p.Id <= 0);

            if (string.IsNullOrEmpty(Order.POOverride))
            {
                SelectedPO = defaultPurchaseOrder;
            }
            else
            {
                var purchaseOrder = PurchaseOrderList.FirstOrDefault(p => p.PurchaseOrderNumber == Order.POOverride);
                SelectedPO = purchaseOrder ?? defaultPurchaseOrder; 
            }

            Site = IoC.Get<ICommonService>().GetSite(_defaultSite, _seasonId);
            ConfirmationNumberText = Order.ConfirmationNumber.HasValue
                ? Order.ConfirmationNumber.Value.ToString("000000")
                : null;

        }

        public ISiteInfo Site { get; set; }

        public string ConfirmationNumberText
        {
            get { return _confirmationNumberText; }
            set
            {
                if (value == _confirmationNumberText) return;
                Order.ConfirmationNumber = ParseIntValue(value);
                _confirmationNumberText = value;
                NotifyOfPropertyChange(() => ConfirmationNumberText);
            }
        }

        protected override void SpecificValidation(List<string> errors)
        {
            if (Order.ConfirmationNumber.HasValue && Order.ConfirmationNumber.Value <= 0)
            {
                errors.Add("Confirmation Number must be greater than 0, or be blank");
                return;
            }

            if (Order.ConfirmationNumber.HasValue)
            {
                if ((Site.ConfNumberFirst > Order.ConfirmationNumber.Value)
                    || (Site.ConfNumberLast < Order.ConfirmationNumber.Value))
                {
                    MessageBoxResult result = MessageBox.Show("Are you sure you want to add an Order with Confirmation Number that isn't for this site?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (result == MessageBoxResult.No)
                    {   
                        //errors.Add("Are you sure you want to add an Order with Confirmation Number for another site?");
                    }
                }
                var orders = IoC.Get<IOrderService>().GetOrderNumbersForConfNumber(Order.ConfirmationNumber.Value, Order.OrderId, _seasonId).Where(o => o.HasValue).ToList();
                if (orders.Any())
                {
                    var messageBoxText =
                        string.Format($"Confirmation Number already exist with Order(s){Environment.NewLine}" +
                            "Press 'Ok' if this Order should be associated with the other orders with this Confirmation Number",
                            string.Join(", ", orders));

                    MessageBoxResult result = MessageBox.Show(messageBoxText, "Confirmation", MessageBoxButton.OKCancel,
                        MessageBoxImage.Question);
                    
                    if (result == MessageBoxResult.Cancel)
                    {
                        //errors.Add("Confirmation Number cannot be reused.");
                    }
                }
            }
        }

        public void AddPurchaseOrder()
        {
            var windowManager = IoC.Get<IWindowManager>();

            var poNameViewModel = IoC.Get<DailyPoNameViewModel>();
            var result = windowManager.ShowDialog(poNameViewModel);
            if(!result.HasValue || !result.Value) 
                return;

            var po = new PurchaseOrderDataModel();
            po.Id = PurchaseOrderList.Min(p => p.Id) - 1;
            po.PurchaseOrderNumber = poNameViewModel.PoNumber;

            PurchaseOrderList.Add(po);
            SelectedPO = po;
        }


        // 2018-Jun-22, JGS - For NEW orders set the record to IsVoid = Treue
        public override void Cancel()
        {
            try
            {
                Order.IsVoid = true;
                Order.IsActive = false;
                Order.IsComplete = false;
                IoC.Get<IOrderService>().SaveOrder(Order);
                _eventAggregator.PublishOnCurrentThread(new OrderModifiedArgs(Order.OrderId));
                TryClose(true);
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "The new order record could not be deleted. Please contact your system administrator {0}",
                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };

                action.OnUIThreadAsync();
            }
        }

        // 2018-Jun-22, JGS - For NEW orders Delete and Cancel are the same function
        public override void Delete()
        {
            try
            {
                Order.IsVoid = true;
                Order.IsActive = false;
                Order.IsComplete = false;
                Order.SpecialInstructionOverride = String.Empty;
                IoC.Get<IOrderService>().SaveOrder(Order);
                _eventAggregator.PublishOnCurrentThread(new OrderModifiedArgs(Order.OrderId));
                TryClose(true);
            }
            catch (UniqueConstraintException ex)
            {
                _log.Error(ex);

                Errors = string.Format("The new order record could not be cancelled. Please contact your system administrator {0}", ex.Message);
                ShowMessage("Validation Errors", Errors, NotificationType.Warning);
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "The new order record could not be cancelled. Please contact your system administrator {0}",
                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };

                action.OnUIThreadAsync();
            }
        }

        public override void Done()
        {
            try
            {
                if (!Validate())
                    return;
                if (SelectedPO != null && !string.IsNullOrWhiteSpace(SelectedPO.PurchaseOrderNumber) &&
                    SelectedPO.Id < -1)
                {
                    var msgbox = MessageBox.Show("Are you sure you want to create new Purchase Order? Press 'No' to create Order without Purchase Order.", "Confirmation", MessageBoxButton.YesNo,
                        MessageBoxImage.Question);

                    if (msgbox == MessageBoxResult.Yes)
                    {
                        SelectedPO.EffectiveDate = SelectedPO.UpdateTime = SelectedPO.CreateTime = DateTime.Now;
                        SelectedPO.QuantityOfPO = Order.Quantity;
                        SelectedPO.CustomerId = Order.Customer.Id;
                        SelectedPO.IsActive = true;
                        SelectedPO.SeasonId = _seasonId;
                        SelectedPO.IsComplete = false;
                        SelectedPO.Material = Order.Material;
                        SelectedPO.MaterialId = Order.Material.Id;
                        IoC.Get<IPurchaseOrderService>().SavePurchaseOrder(SelectedPO, Order.Customer.Id);

                        _eventAggregator.PublishOnCurrentThread(new PurchaseOrderModifiedArgs(SelectedPO.Id));
                    }
                    else
                    {
                        Order.POOverride = null;
                    }
                }

                Order.IsActive = true;
                IoC.Get<IOrderService>().SaveOrder(Order);
                _eventAggregator.PublishOnCurrentThread(new OrderModifiedArgs(Order.OrderId));
                
                TryClose(true);
            }
            catch (UniqueConstraintException ex)
            {
                _log.Error(ex);

                Errors = string.Format("Purchase Order saving failed. Data not unique. {0}", ex.Message);
                ShowMessage("Validation Errors", Errors, NotificationType.Warning);
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "Order saving failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };

                action.OnUIThreadAsync();
            }
        }

        public override PurchaseOrderDataModel SelectedPO
        {
            get { return _selectedPo; }
            set
            {
                if (Equals(value, _selectedPo)) return;
                _selectedPo = value;
                NotifyOfPropertyChange(() => SelectedPO);

                if (value == null || value.Id == -1 || string.IsNullOrWhiteSpace(value.PurchaseOrderNumber))
                {
                    ChooseMaterial(Order.Material != null ? Order.Material.Id : Order.Bid.MaterialId);
                    EnableMaterial = true;
                    Order.POOverride = null;
                }
                else if (value.Id < -1)
                {
                    Order.POOverride = value.PurchaseOrderNumber;
                    EnableMaterial = true;
                }
                else
                {
                    ChooseMaterial(value.MaterialId);


                    if (value.MaterialDescription != SelectedBidInfo?.MaterialDescription)
                    {
                        if (SelectedBidInfo != null)
                        {
                            MessageBox.Show($"This PO changed the Order Material from {Environment.NewLine}'{SelectedBidInfo.MaterialDescription}' to '{BidInfoList.Where(m => m.MaterialId == value.MaterialId).FirstOrDefault().MaterialDescription}'", "Material Change", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }

                    Order.POOverride = value.PurchaseOrderNumber;
                    SelectedBidInfo = BidInfoList.Where(m => m.MaterialId == value.MaterialId).FirstOrDefault();

                }
            }
        }

        public string SelectedCustomerCompanyName { get; private set; }
    }
}