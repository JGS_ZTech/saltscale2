﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Caliburn.Micro;
using Contract.Events;
using Contract.Services;
using ZTech.Controls;
using ZTech.ViewModels.Abstract;
using Action = System.Action;

namespace ZTech.ViewModels
{
    public class DailyOperationViewModel : BaseViewModel
    {
        private readonly IEventAggregator _eventAggregator;
        private readonly ILog _log;
        private readonly int _seasonId;
        private bool _closeCompleted;
        private bool _closeRemaining;
        private string _dailyOperationsErrors;
        private bool _isBusy;
        private float _tons;

        public DailyOperationViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            _log = LogManager.GetLog(typeof (DailyOperationViewModel));
            _seasonId = (int) Application.Current.Properties["SeasonId"];
            DisplayName = "Daily Operations";
        }

        public string DailyOperationsErrors
        {
            get { return _dailyOperationsErrors; }
            set
            {
                if (value == _dailyOperationsErrors) return;
                _dailyOperationsErrors = value;
                NotifyOfPropertyChange(() => DailyOperationsErrors);
            }
        }

        public bool CloseCompleted
        {
            get { return _closeCompleted; }
            set
            {
                if (value.Equals(_closeCompleted)) return;
                _closeCompleted = value;
                NotifyOfPropertyChange(() => CloseCompleted);
            }
        }

        public bool CloseRemaining
        {
            get { return _closeRemaining; }
            set
            {
                if (value.Equals(_closeRemaining)) return;
                _closeRemaining = value;
                NotifyOfPropertyChange(() => CloseRemaining);
            }
        }

        public float Tons
        {
            get { return _tons; }
            set
            {
                if (value == _tons) return;
                _tons = value;
                NotifyOfPropertyChange();
            }
        }

        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                if (value.Equals(_isBusy)) return;
                _isBusy = value;
                NotifyOfPropertyChange(() => IsBusy);
            }
        }

        public void KeyHandle(Key key)
        {
            if (key == Key.F2)
            {
                DoneDailyOperations();
            }
        }

        public void DoneDailyOperations()
        {
            try
            {
                if (!ValidateDaily())
                    return;

                IsBusy = true;
                var task = new Task(CloseCompletedTask);
                task.Start();
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "Operation failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };

                action.OnUIThreadAsync();
            }
        }

        private void CloseCompletedTask()
        {
            try
            {
                if (CloseCompleted)
                {
                    MessageBoxResult messageBoxResult = (MessageBox.Show("Are you sure, you want to close all orders marked as complete?",
                            "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question));
                    if (messageBoxResult == MessageBoxResult.Yes)
                    {
                        IoC.Get<IOrderService>().CloseCompletedOrders(_seasonId);
                    }
                }
                if (CloseRemaining)
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show($"Are you sure, you want to close all orders with less than {Tons.ToString("N2")} tons remaining?","Confirmation",
                                MessageBoxButton.YesNo, MessageBoxImage.Question));

                    if (messageBoxResult == MessageBoxResult.Yes)
                    {
                        IoC.Get<IOrderService>().CloseRemainingOrders(Tons, _seasonId);
                    }
                }
                TryClose(true);
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                MessageBoxResult messageBoxResult =
                    (MessageBox.Show(string.Format(
                        "Operation failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                        ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));

                TryClose(false);
            }
            finally
            {
                _eventAggregator.PublishOnUIThreadAsync(new OrderModifiedArgs(null));
                IsBusy = false;
            }
        }

        private bool ValidateDaily()
        {
            DailyOperationsErrors = null;

            if (!CloseCompleted && !CloseRemaining)
            {
                ShowMessage("Information", "No option selected", NotificationType.Information);

                return false;
            }

            if (CloseRemaining && Tons < 0)
            {
                DailyOperationsErrors = "Remaining tons have to be greater or equal 0.";
                ShowMessage("Validation Errors", DailyOperationsErrors, NotificationType.Warning);

                return false;
            }

            return true;
        }

        #region Events

        protected override void OnActivate()
        {
            _eventAggregator.Subscribe(this);
            base.OnActivate();
        }

        protected override void OnDeactivate(bool close)
        {
            _eventAggregator.Unsubscribe(this);
            base.OnDeactivate(close);
        }

        #endregion
    }
}