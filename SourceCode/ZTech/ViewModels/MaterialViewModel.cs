﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Caliburn.Micro;
using Contract.Exception;
using Contract.Services;
using ZTech.Controls;
using ZTech.ViewModels.Abstract;
using ZTech.ViewModels.ViewObject;
using Action = System.Action;

namespace ZTech.ViewModels
{
    public class MaterialViewModel : BaseViewModel
    {
        private readonly ILog _log;
        private readonly int _seasonId;
        private string _errors;
        private IEventAggregator _eventAggregator;
        private List<MaterialDataModel> _materialList;
        private MaterialDataModel _selectedMaterial;

        public MaterialViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            _log = LogManager.GetLog(typeof(MaterialViewModel));
            _seasonId = (int)Application.Current.Properties["SeasonId"];
            DisplayName = "Materials";
        }

        public MaterialDataModel SelectedMaterial
        {
            get { return _selectedMaterial; }
            set
            {
                if (Equals(value, _selectedMaterial)) return;
                _selectedMaterial = value;
                NotifyOfPropertyChange(() => SelectedMaterial);
                Errors = null;
            }
        }

        public List<MaterialDataModel> MaterialList
        {
            get { return _materialList; }
            set
            {
                if (Equals(value, _materialList)) return;
                _materialList = value;
                NotifyOfPropertyChange(() => MaterialList);
            }
        }

        public string Errors
        {
            get { return _errors; }
            set
            {
                if (value == _errors) return;
                _errors = value;
                NotifyOfPropertyChange(() => Errors);
            }
        }

        protected override void OnInitialize()
        {
            base.OnInitialize();
            InitializeMaterials();
        }

        private void InitializeMaterials()
        {
            // 14-Aug-2017, JGS - Added ORDERBY clause to make sure SALT always comes first in the default material list.
            MaterialList =
                IoC.Get<IMaterialService>().GetMaterialList(_seasonId).OrderBy(m => m.Id).Select(m => new MaterialDataModel(m)).ToList();


            MaterialDataModel materialModel = MaterialList.FirstOrDefault();
            if (materialModel != null)
            {
                SelectedMaterial = materialModel;
            }

            // Restore previous sort

        }

        public void NewMaterial()
        {
            SelectedMaterial = new MaterialDataModel();
            SelectedMaterial.UpdateTime = SelectedMaterial.CreateTime = DateTime.Now;
            SelectedMaterial.IsActive = true;
            SelectedMaterial.SeasonId = _seasonId;
        }

        public void DeleteMaterial()
        {
            if (SelectedMaterial == null || !SelectedMaterial.Id.HasValue)
                return;
            Action action = () =>
            {
                MessageBoxResult messageBoxResult =
                    (MessageBox.Show("Are you sure you want to delete this Material?",
                        "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question));

                if (messageBoxResult == MessageBoxResult.Yes)
                    DeleteAction();
            };

            action.OnUIThread();
        }

        private void DeleteAction()
        {
            try
            {
                if (SelectedMaterial == null || !SelectedMaterial.Id.HasValue)
                    return;

                IoC.Get<IMaterialService>().DeleteMaterial(SelectedMaterial);
                SelectedMaterial = null;
                InitializeMaterials();
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                string message = string.Format(
                    "Material deleting failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                    ex.Message);
                ShowMessage("Error", message, NotificationType.Error);
            }
        }

        public void Cancel()
        {
            TryClose(false);
        }

        public void KeyHandle(Key key)
        {
            if (key == Key.F2)
            {
                Done();
            }
        }


        //public System.Windows.Controls.DataGridTextColumn grdcolName { get { return (grdcolName); } }
        //public System.Windows.Controls.DataGridTextColumn grdcolDescription { get { return (grdcolDescription); } }


        public void Done()
        {
            try
            {
                //// Determine column settings
                //string sortName = grdcolName.SortDirection.ToString();
                //string sortDescription = grdcolDescription.SortDirection.ToString();
                //MessageBox.Show(String.Format("Name {0}  Description {1}", sortName, sortName));


                //var isOrderAsc = MaterialList.SequenceEqual(MaterialList.OrderBy(x => x.Name));
                //if (isOrderAsc) MessageBox.Show("Name Ascending");
                //var isOrderDes = MaterialList.SequenceEqual(MaterialList.OrderByDescending(x => x.Name));
                //if (isOrderDes) MessageBox.Show("Name Descending");
                //var isOrderAsc2 = MaterialList.SequenceEqual(MaterialList.OrderBy(x => x.Description));
                //if (isOrderAsc2) MessageBox.Show("Description Ascending");
                //var isOrderDes2 = MaterialList.SequenceEqual(MaterialList.OrderByDescending(x => x.Description));
                //if (isOrderDes2) MessageBox.Show("Description Descending");


                // Process Validation
                if (!Validate())
                    return;

                // Generate the update Date/Tiem 
                SelectedMaterial.UpdateTime = DateTime.Now;

                var id = IoC.Get<IMaterialService>().SaveMaterial(SelectedMaterial);

                InitializeMaterials();

                var material = MaterialList.FirstOrDefault(m => m.Id == id);
                if (material != null)
                    SelectedMaterial = material;

                TryClose(true);
            }
            catch (UniqueConstraintException ex)
            {
                _log.Error(ex);

                Errors = string.Format("Material saving failed. {0}", ex.Message);
                ShowMessage("Validation Errors", Errors, NotificationType.Warning);
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Errors = string.Format(
                    "Material saving failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                    ex.Message);

               ShowMessage("Error", Errors, NotificationType.Error);
            }
        }

        private bool Validate()
        {
            Errors = null;

            ICollection<ValidationResult> validationResults = new Collection<ValidationResult>();
            bool result = Validator.TryValidateObject(SelectedMaterial, new ValidationContext(SelectedMaterial),
                validationResults);

            if (!result || validationResults.Any())
            {
                Errors = string.Format(
                    "Validation Failed.{0}{1}", Environment.NewLine,
                    string.Join("\n", validationResults.Select(v => v.ErrorMessage)));

                ShowMessage("Validation Errors", Errors, NotificationType.Warning);
                
                return false;
            }

            return true;
        }
    }
}