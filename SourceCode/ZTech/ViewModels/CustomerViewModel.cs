﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Caliburn.Micro;
using Contract.Domain;
using Contract.Events;
using Contract.Exception;
using Contract.Services;
using ZTech.Controls;
using ZTech.ViewModels.Abstract;
using ZTech.ViewModels.ViewObject;
using Action = System.Action;
using ValidationResult = System.ComponentModel.DataAnnotations.ValidationResult;
using System.Data.Entity.Validation;

namespace ZTech.ViewModels
{
    public class CustomerViewModel : BaseViewModel, IHandle<PurchaseOrderModifiedArgs>
    {
        private readonly IEventAggregator _eventAggregator;
        private readonly ILog _log;
        private readonly int _seasonId;
        private CustomerDataModel _customer;

        private IBidInfo _bid;

        private IReadOnlyList<IReportingEntity> _reportingEntityList;
        private IReportingEntity _selectedReportingEntity;
        private IReadOnlyList<IReportingEntityL2> _reportingEntityListL2;
        private IReportingEntityL2 _selectedReportingEntityL2;
        private IReadOnlyList<IReportingEntityL3> _reportingEntityListL3;
        private IReportingEntityL3 _selectedReportingEntityL3;
        private IContactWidgetViewModel _contactWidget;
        
        private IReadOnlyList<ISiteInfo> _siteList;
        private ICompanyInfo _selectedCompany;
        private IReadOnlyList<ICompanyInfo> _companyList;
        private List<IMaterialInfo> _materialList;

        private string _errors;
        
        private ISiteConfig _selectedSiteConfig;
        private ISiteInfo _selectedSiteInfo;
        private IMaterialInfo _selectedMaterial;
        private bool _isRateEnabled;
        private bool _isEditButtonEnabled;
        private bool _isDeleteBidEnabled;
        private bool _ResetEntityToNull = false;

        private List<PurchaseOrderDataModel> _poList;
        private PurchaseOrderDataModel _selectedPurchaseOrder;

        private ObservableCollection<IBidInfo> _bidList;
        private IBidInfo _selectedBid;

        private int _materialId;

        public bool IsNew { get; set; }

        public string Errors
        {
            get { return _errors; }
            set
            {
                if (value == _errors) return;
                _errors = value;
                NotifyOfPropertyChange(() => Errors);
            }
        }

        public IContactWidgetViewModel ContactWidget
        {
            get { return _contactWidget; }
            set
            {
                if (Equals(value, _contactWidget)) return;
                _contactWidget = value;
                NotifyOfPropertyChange(() => ContactWidget);
            }
        }


        #region About this customer

        public IReadOnlyList<ICompanyInfo> CompanyList
        {
            get { return _companyList; }
            set
            {
                if (Equals(value, _companyList)) return;
                _companyList = value;
                NotifyOfPropertyChange(() => CompanyList);
            }
        }

        public CustomerDataModel Customer
        {
            get { return _customer; }
            set
            {
                if (Equals(value, _customer)) return;
                _customer = value;
                NotifyOfPropertyChange(() => Customer);
            }
        }

        public int CustomerId { get; set; }

        public IReadOnlyList<IReportingEntity> ReportingEntityList
        {
            get { return _reportingEntityList; }
            set
            {
                if (Equals(value, _reportingEntityList)) return;
                _reportingEntityList = value;
                NotifyOfPropertyChange(() => ReportingEntityList);
            }
        }

        public IReportingEntity SelectedReportingEntity
        {
            get { return _selectedReportingEntity; }
            set
            {
                _selectedReportingEntity = value;
                NotifyOfPropertyChange(() => SelectedReportingEntity);

                // If there is a valid selection then ResetEntityToNull should be false
                if (_selectedReportingEntity != null)
                    ResetEntityToNull = false;
            }
        }

        public IReadOnlyList<IReportingEntityL2> ReportingEntityListL2
        {
            get { return _reportingEntityListL2; }
            set
            {
                if (Equals(value, _reportingEntityListL2)) return;
                _reportingEntityListL2 = value;
                NotifyOfPropertyChange(() => ReportingEntityListL2);
            }
        }

        public IReportingEntityL2 SelectedReportingEntityL2
        {
            get { return _selectedReportingEntityL2; }
            set
            {
                _selectedReportingEntityL2 = value;
                NotifyOfPropertyChange(() => SelectedReportingEntityL2);
            }
        }

        public IReadOnlyList<IReportingEntityL3> ReportingEntityListL3
        {
            get { return _reportingEntityListL3; }
            set
            {
                if (Equals(value, _reportingEntityListL3)) return;
                _reportingEntityListL3 = value;
                NotifyOfPropertyChange(() => ReportingEntityListL3);
            }
        }

        public IReportingEntityL3 SelectedReportingEntityL3
        {
            get { return _selectedReportingEntityL3; }
            set
            {
                _selectedReportingEntityL3 = value;
                NotifyOfPropertyChange(() => SelectedReportingEntityL3);
            }
        }

        private void InitializeCustomer()
        {
            try
            {
                Customer = IsNew
                    ? new CustomerDataModel(IoC.Get<ICustomerService>().GetNewCustomer(_seasonId, _selectedSiteConfig.Name))
                    : new CustomerDataModel(IoC.Get<ICustomerService>().GetCustomerAddress(CustomerId, _seasonId));

                if (Customer.ReportingEntityId.HasValue)
                {
                    if (_reportingEntityList == null) InitializeReportingEntities();
                    SelectedReportingEntity =
                        _reportingEntityList.FirstOrDefault(re => re.Id == Customer.ReportingEntityId);
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    var messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "Customer details screen failed with exception {0}. The error has been logged...",
                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };

                action.OnUIThreadAsync();
            }
        }

        private void InitializeReportingEntities()
        {
            ReportingEntityList = IoC.Get<ICommonService>().GetReportingEntities(_seasonId);
            ReportingEntityListL2 = IoC.Get<ICommonService>().GetReportingEntitiesL2(_seasonId);
            ReportingEntityListL3 = IoC.Get<ICommonService>().GetReportingEntitiesL3(_seasonId);
        }

        #endregion
        
        #region Bid


        public IReadOnlyList<ISiteInfo> SiteList
        {
            get { return _siteList; }
            set
            {
                if (Equals(value, _siteList)) return;
                _siteList = value;
                NotifyOfPropertyChange(() => SiteList);
            }
        }

        public int MaterialId
        {
            get { return _materialId; }
            set
            {
                if (Equals(value, _materialId)) return;
                _materialId = value;
                NotifyOfPropertyChange(() => MaterialId);
            }
        }

        public List<IMaterialInfo> MaterialList
        {
            get { return _materialList; }
            set
            {
                if (Equals(value, _materialList)) return;
                _materialList = value;
                NotifyOfPropertyChange(() => MaterialList);
            }
        }

        public ICompanyInfo SelectedCompany
        {
            get { return _selectedCompany; }
            set
            {
                if (Equals(value, _selectedCompany)) return;
                _selectedCompany = value;
                NotifyOfPropertyChange(() => SelectedCompany);
            }
        }

        public ISiteConfig SelectedSite
        {
            get { return _selectedSiteConfig; }
            set
            {
                if (Equals(value, _selectedSiteConfig)) return;
                _selectedSiteConfig = value;
                NotifyOfPropertyChange(() => SelectedSite);
            }
        }

        public IMaterialInfo SelectedMaterial
        {
            get { return _selectedMaterial; }
            set
            {
                if (Equals(value, _selectedMaterial)) return;
                _selectedMaterial = value;
                NotifyOfPropertyChange(() => SelectedMaterial);
            }
        }

        public IBidInfo Bid
        {
            get { return _bid; }
            set
            {
                if (Equals(value, _bid)) return;
                _bid = value;
                NotifyOfPropertyChange(() => Bid);
            }
        }

        public void NewBidRow(object sender, RoutedEventArgs e)
        {
            _bidList.Add(IoC.Get<IBidService>().AddEmptyRow(
                _selectedCompany.Id,
                _selectedCompany.Name,
                _selectedSiteInfo.Id,
                _selectedSiteConfig.Name,
                _seasonId,
                _customer.Id
                )
                );
        }

        private void InitializeCompanies()
        {
            CompanyList = IoC.Get<ICommonService>().GetCompanies();
        }

        private void InitializeSites()
        {
            SiteList = IoC.Get<ICommonService>().GetSites(_seasonId);
        }

        private void InitializeMaterials()
        {
            MaterialList = IoC.Get<IMaterialService>().GetMaterials(_seasonId);
        }

        private void InitializeBidList()
        {
            try
            {
                BidList = IoC.Get<IBidService>().GetCustomerBidInformationList(_seasonId, _customer.Id);
                
                /*                                  
                var bid = BidList.FirstOrDefault();

                var company = _companyList.FirstOrDefault(c => c.Id == bid.CompanyId);
                if (company != null)
                    SelectedCompany = company;

                CompanyList.Select(x => x.Name);
                */

                /*
                var material = _materialList.FirstOrDefault(m => m.Id == bid.MaterialId);

                if (material == null)
                {
                    var inactiveMaterial = IoC.Get<IMaterialService>().GetInactiveMaterial(bid.MaterialId);
                    if (inactiveMaterial != null)
                    {
                        MaterialList.Add(inactiveMaterial);
                        SelectedMaterial = inactiveMaterial;
                    }
                }
                else
                {
                    SelectedMaterial = material;
                }
                */

                /*
                var bid = Customer.Bid;
                if (bid == null) return;
                var company = _companyList.FirstOrDefault(c => c.Id ==bid.CompanyId);
                if (company != null)
                    SelectedCompany = company;

                var site = _siteList.FirstOrDefault(s => s.Id == bid.SiteId);
                if (site != null)
                    SelectedSite = site;

                var material = _materialList.FirstOrDefault(m => m.Id == bid.MaterialId);

                if (material == null)
                {
                    var inactiveMaterial = IoC.Get<IMaterialService>().GetInactiveMaterial(bid.MaterialId);
                    if (inactiveMaterial != null)
                    {
                        MaterialList.Add(inactiveMaterial);
                        SelectedMaterial = inactiveMaterial;
                    }
                }
                else
                {
                    SelectedMaterial = material;
                }
                */
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    var messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "Customer Dialog failed with exception {0}. Please reopen application and try again or contact HelpDesk.",
                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };

                action.OnUIThreadAsync();
            }
        }

        private void InitializePurchaseOrderList()
        {
            var purchaseOrderList = IoC.Get<IPurchaseOrderService>().GetPurchaseOrderList(Customer.Id, _seasonId)
                .OrderByDescending(x => x.CreateTime)
                .ToList();

            POList = purchaseOrderList.Where(p=>!string.IsNullOrWhiteSpace(p.PurchaseOrderNumber))
                .Select(p => new PurchaseOrderDataModel(p))
                .ToList();

            var po = POList.FirstOrDefault();
            if (po != null)
                SelectedPurchaseOrder = po;
        }

        public List<PurchaseOrderDataModel> POList
        {
            get { return _poList; }
            set
            {
                if (Equals(value, _poList)) return;
                _poList = value;
                NotifyOfPropertyChange(() => POList);
            }
        }

        public ObservableCollection<IBidInfo> BidList
        {
            get { return _bidList; }
            set
            {
                if (Equals(value, _bidList)) return;
                _bidList = value;
                NotifyOfPropertyChange(() => BidList);
            }
        }

        #endregion

        #region Contact Information

        private void InitializeContact()
        {
            ContactWidget = IoC.Get<IContactWidgetViewModel>();
            ContactWidget.ActivateWith(this);
            ContactWidget.SetData(Customer.Contact);
        }

        #endregion

        #region Actions

        public void NewPurchaseOrder()
        {
            var newPurchaseOrderViewModel = IoC.Get<PurchaseOrderViewModel>();
            newPurchaseOrderViewModel.IsNew = true;
            newPurchaseOrderViewModel.CustomerId = Customer.Id;

            var windowManager = IoC.Get<IWindowManager>();
            windowManager.ShowDialog(newPurchaseOrderViewModel);
        }
        public void EditPurchaseOrder()
        {
            if(SelectedPurchaseOrder == null)return;

            var windowManager = IoC.Get<IWindowManager>();
            var editPurchaseOrderViewModel = IoC.Get<PurchaseOrderViewModel>();

            editPurchaseOrderViewModel.PurchaseOrderId = SelectedPurchaseOrder.Id;
            editPurchaseOrderViewModel.CustomerId = Customer.Id;
            windowManager.ShowDialog(editPurchaseOrderViewModel);
        }

        public PurchaseOrderDataModel SelectedPurchaseOrder
        {
            get { return _selectedPurchaseOrder; }
            set
            {
                if (Equals(value, _selectedPurchaseOrder)) return;
                _selectedPurchaseOrder = value;
                NotifyOfPropertyChange(() => SelectedPurchaseOrder);
            }
        }

        public void DeleteBidRow()
        {
            var msgbox = MessageBox.Show($"Are you sure you want to delete this bid?{Environment.NewLine}{Environment.NewLine}Open orders with this material will be checked first and stop if one exists", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);

            if (msgbox == MessageBoxResult.Yes)
            {
                var isBidsCompleted = IoC.Get<IBidService>().IsBidMaterialPurchaseOrdersAndOrderComplete(SelectedBid);
                if (!isBidsCompleted)
                {
                    Action action = () =>
                    {
                        MessageBoxResult messageBoxResult =
                            (MessageBox.Show(
                                string.Format($"The bid material cannot be removed, the customer has an open Order or Purchase Order"), "Open Order/PO with Bid Material", MessageBoxButton.OK));
                    };

                    action.OnUIThreadAsync();
                    return;
                }

                var result = IoC.Get<IBidService>().DeleteRow(SelectedBid);
                SelectedBid = null;
                InitializeBidList();
            }
        }

        public bool IsDeleteBidEnabled
        {
            get { return _isDeleteBidEnabled; }
            set
            {
                if (Equals(value, _isDeleteBidEnabled)) return;
                _isDeleteBidEnabled = value;
                NotifyOfPropertyChange(() => IsDeleteBidEnabled);
            }
        }

        public bool ResetEntityToNull
        {
            get { return _ResetEntityToNull; }
            set
            {
                if (Equals(value, _ResetEntityToNull)) return;
                _ResetEntityToNull = value;
                NotifyOfPropertyChange(() => ResetEntityToNull);

                if (ResetEntityToNull)
                    SelectedReportingEntity = null;
            }
        }

        public IBidInfo SelectedBid
        {
            get { return _selectedBid; }
            set
            {
                if (Equals(value, _selectedBid)) return;
                //ValidateBidMaterialChange(value.MaterialId, _selectedBid.MaterialId);
                _selectedBid = value;
                NotifyOfPropertyChange(() => SelectedBid);

                
                IsDeleteBidEnabled = true;
            }
        }



        public bool ValidateBidMaterialChange()
        {
            var msgbox = MessageBox.Show($"Are you sure you want to change this bid?{Environment.NewLine}{Environment.NewLine}Open orders with this material will be checked first and stop if one exists", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);

            if (msgbox == MessageBoxResult.Yes)
            {
                var isBidsCompleted = IoC.Get<IBidService>().IsBidMaterialPurchaseOrdersAndOrderComplete(SelectedBid);
                if (!isBidsCompleted)
                {
                    Action action = () =>
                    {
                        MessageBoxResult messageBoxResult =
                            (MessageBox.Show(
                                string.Format($"The bid material cannot be changed, the customer has an open Order or Purchase Order"), "Open Order/PO with Bid Material", MessageBoxButton.OK));
                    };

                    action.OnUIThreadAsync();
                    return false;
                }

                SelectedBid = null;
                InitializeBidList();

                return true;
            }
            else
            {
                return false;
            }
        }

        public void Cancel()
        {
            TryClose(false);
        }

        public void EditRate()
        {
            var windowManager = IoC.Get<IWindowManager>();
            var viewModel = IoC.Get<LoginViewModel>();
            var dialogResult = windowManager.ShowDialog(viewModel);
            if (!dialogResult.HasValue || !dialogResult.Value || !viewModel.IsAuthorized) return;

            IsRateEnabled = true;
            IsEditButtonEnabled = false;
        }

        public void SetDefaultRate()
        {
            if (SelectedSite != null)
            {
                Customer.CashMaterialRate = _selectedSiteInfo.DefaultMaterialCost;
            }
            else
            {
                var siteInfo = SiteList.FirstOrDefault(s => s.Name == _selectedSiteConfig.Name);
                if (siteInfo != null)
                    Customer.CashMaterialRate = siteInfo.DefaultMaterialCost;
            }
        }
        public void Handle(PurchaseOrderModifiedArgs args)
        {
            if (args != null)
            {
                InitializePurchaseOrderList();
            }
        }

        public void DataGridBidList_CellEditEnding(object sender, System.Windows.Controls.DataGridCellEditEndingEventArgs e)
        {
            var x = 1;
            x = 1;
        }

        public void ResetEntity()
        {
            ResetEntityToNull = true;
        }

        public void Done()
        {
            try
            {
                foreach (var item in BidList)
                {
                    item.MaterialShortName = IoC.Get<IMaterialService>().GetMaterial(_seasonId, item.MaterialId).Name;
                }


                if (!Validate())
                    return;

                // Process the ResetEntity status
                if (ResetEntityToNull && SelectedReportingEntity == null)
                    Customer.ReportingEntityId = null;
                else
                    Customer.ReportingEntityId = SelectedReportingEntity == null ? new int?() : SelectedReportingEntity.Id;

                Customer.Company = SelectedCompany;

                //Save Customer
                Customer.Contact = ContactWidget.GetData();
                Customer.UpdateTime = DateTime.Now;
                IoC.Get<ICustomerService>().SaveCustomer(Customer, _seasonId);
                _eventAggregator.PublishOnCurrentThread(new CustomerModifiedArgs(Customer.Id));
                IoC.Get<IBidService>().SaveBid(BidList, _seasonId);
                TryClose(true);
            }
            catch (UniqueConstraintException ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult = (MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };

                action.OnUIThreadAsync();
            }
            catch (DbEntityValidationException exEntityValidation)
            {
                _log.Error(exEntityValidation);
                var entityValidationErrorsMessageText = string.Empty;

                foreach (var entityValidation in exEntityValidation.EntityValidationErrors)
                {
                    entityValidationErrorsMessageText += $"Entity type '{entityValidation.Entry.Entity.GetType().Name}' in state '{entityValidation.Entry.State}' has the validation errors...";

                    foreach (var entityValidationError in entityValidation.ValidationErrors)
                    {
                        entityValidationErrorsMessageText += $"{Environment.NewLine}   Property: '{entityValidationError.PropertyName}'    Error: '{entityValidationError.ErrorMessage}'";
                    }

                    entityValidationErrorsMessageText += Environment.NewLine;
                }

                _log.Error(new Exception($"Entity Validation details: {entityValidationErrorsMessageText}"));

                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(string.Format($"Customer saving failed Entity Validation ... {entityValidationErrorsMessageText}{Environment.NewLine}{Environment.NewLine}Please let IT know about this."),
                            "Entity Validation", MessageBoxButton.OK, MessageBoxImage.Error));
                };

                action.OnUIThreadAsync();
            }
            catch (Exception ex)
            {
                var exInnerMessageBoxText = string.Empty;
                _log.Error(ex);
                if (ex.InnerException != null)
                {
                    _log.Error(ex.InnerException);
                    exInnerMessageBoxText = $" and inner exception '{ex.InnerException.Message}'{Environment.NewLine} ";
                }

                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(string.Format($"Customer saving failed with exception '{ex.Message}'{exInnerMessageBoxText}  The changes made were not saved and the error has been logged for IT to review.  Please let IT know about this."),
                            "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };

                action.OnUIThreadAsync();
            }
        }

        
        private bool Validate()
        {
            Errors = null;

            ICollection<ValidationResult> validationResults = new Collection<ValidationResult>();
            bool result = Validator.TryValidateObject(Customer, new ValidationContext(Customer), validationResults);

            var bidVerifyResult = IoC.Get<IBidService>().VerifyBidBeforeSave(BidList, _seasonId);
            if (bidVerifyResult != string.Empty)
                validationResults.Add(new ValidationResult(bidVerifyResult));

            /*
            //Validate Bid Company 
            if (Customer.Bid.Company == null)
            {
                validationResults.Add(new ValidationResult("The Bid Company cannot be left blank."));
            }
            //Validate Bid Site 
            if (Customer.Bid.Site == null)
            {
                validationResults.Add(new ValidationResult("The Bid Site cannot be left blank."));
            }
            //Validate Bid Material 
            if (Customer.Bid.Material == null)
            {
                validationResults.Add(new ValidationResult("The Bid Material cannot be left blank."));
            }
            */
            var messages = validationResults.Union(ContactWidget.Validate()).ToList();

            if (!result || messages.Any())
            {
                Errors = string.Format($"Validation Failed.{Environment.NewLine}{string.Join($"{Environment.NewLine}", messages.Select(v => v.ErrorMessage))}");
                ShowMessage("Validation Errors", Errors, NotificationType.Warning);

                return false;
            }

            return true;
        }

        public void Delete()
        {
            var message = string.Format("Are you sure you want to delete {0} Customer?", Customer.Name);
            MessageBoxResult result = MessageBox.Show(message, "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                if (IoC.Get<ICustomerService>().DeleteCustomer(Customer.Id))
                {
                    _eventAggregator.PublishOnCurrentThread(new CustomerModifiedArgs(Customer.Id));
                    TryClose(true);
                }
            }
        }

        #endregion

        #region Events

        public CustomerViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            _log = LogManager.GetLog(typeof (CustomerViewModel));
            _seasonId = (int) Application.Current.Properties["SeasonId"];
            _selectedSiteConfig = IoC.Get<IConfiguration>().SelectedSite;

            _selectedSiteInfo = IoC.Get<ICommonService>().GetSite(_selectedSiteConfig.Name, _seasonId);
            DisplayName = "Customer Dialog";
        }

        protected override void OnInitialize()
        {
            try
            {
                base.OnInitialize();

                if (_reportingEntityList == null) InitializeReportingEntities();
                if (_companyList == null) InitializeCompanies();
                if (_siteList == null) InitializeSites();
                if (_materialList == null) InitializeMaterials();
                InitializeCustomer();
                InitializeContact();
                InitializeBidList();
                InitializePurchaseOrderList();

                SelectedCompany = CompanyList.Where(c => c.Id == Customer.Company.Id).FirstOrDefault();

                IsRateEnabled = false;
                IsEditButtonEnabled = true;
                if ((bool) Application.Current.Properties["IsLogged"] )
                {
                    IsRateEnabled = true;
                    IsEditButtonEnabled = false;
                }

                _isDeleteBidEnabled = false;

            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(
                            string.Format($"Customer screen (CustomerViewModel) initialization failed with exception '{ex.Message}'. Please reopen application and try again or contact HelpDesk."),
                                "Error", MessageBoxButton.OK, MessageBoxImage.Error));

                    TryClose(false);
                };

                action.OnUIThreadAsync();
            }
        }

        public bool IsEditButtonEnabled
        {
            get { return _isEditButtonEnabled; }
            set
            {
                if (value.Equals(_isEditButtonEnabled)) return;
                _isEditButtonEnabled = value;
                NotifyOfPropertyChange(() => IsEditButtonEnabled);
            }
        }

        public bool IsRateEnabled
        {
            get { return _isRateEnabled; }
            set
            {
                if (value.Equals(_isRateEnabled)) return;
                _isRateEnabled = value;
                NotifyOfPropertyChange(() => IsRateEnabled);
            }
        }

        protected override void OnActivate()
        {
            _eventAggregator.Subscribe(this);
            base.OnActivate();
        }

        protected override void OnDeactivate(bool close)
        {
            _eventAggregator.Unsubscribe(this);
            base.OnDeactivate(close);
        }

        public void KeyHandle(Key key)
        {
            if (key == Key.F2)
            {
                Done();
            }
        }

        #endregion
    }
}