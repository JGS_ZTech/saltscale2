﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Caliburn.Micro;
using Contract.Domain;
using Contract.Services;
using Utility.CSVFile;
using ZTech.Controls;
using ZTech.ViewModels.Abstract;
using ZTech.ViewModels.ViewObject;
using Action = System.Action;
using System.IO;
using System.Windows;

namespace ZTech.ViewModels
{
	public class TicketExportViewModel : BaseViewModel
	{
		private readonly IConfiguration _configuration;
		private readonly IEventAggregator _eventAggregator;
		private readonly ILog _log;
		private bool _allTickets;
		private List<ICompanyInfo> _companyList;
		private bool _exported;
		private bool _exportedVoided;
		private DateTime _fromDate;
		private bool _isEnableCompany;
		private bool _isBusy;
		private bool _notExported;
		private ISeason _season;
		private ICompanyInfo _selectedCompany;
		private ISiteConfig _selectedSite;
		private List<ISiteConfig> _siteList;
		private DateTime _toDate;
		private List<TicketExportModel> _tickets;
		private TicketExportModel _selectedTicket;
		private string _errors;

		private int _numOfTickets;
		private int _numOfCash;
		private int _numOfVoids;

		private List<IAccountingInfo> _accountingCustomerCodeList;
		private IAccountingInfo _selectedAccountingCustomerCode;
		public TicketExportViewModel(IEventAggregator eventAggregator, IConfiguration configuration)
		{
			_eventAggregator = eventAggregator;
			_configuration = configuration;
			_log = LogManager.GetLog(typeof (TicketExportViewModel));
			DisplayName = "Ticket Export";
		}

		public List<ISiteConfig> SiteList
		{
			get { return _siteList; }
			set
			{
				if (Equals(value, _siteList)) return;
				_siteList = value;
				NotifyOfPropertyChange(() => SiteList);
			}
		}

		public ISiteConfig SelectedSite
		{
			get { return _selectedSite; }
			set
			{
				if (Equals(value, _selectedSite)) return;
				_selectedSite = value;
				NotifyOfPropertyChange(() => SelectedSite);
				if (value != null)
				{
					ConnectionString = _configuration.ConvertConnectionString(SelectedSite.ConnectionString);
					InitSeason();
					InitCompany();
				}
			}
		}

		public bool IsEnableCompany
		{
			get { return _isEnableCompany; }
			set
			{
				if (value.Equals(_isEnableCompany)) return;
				_isEnableCompany = value;
				NotifyOfPropertyChange(() => IsEnableCompany);
			}
		}

		public DateTime ToDate
		{
			get { return _toDate; }
			set
			{
				if (value.Equals(_toDate)) return;
				_toDate = value;
				NotifyOfPropertyChange(() => ToDate);
			}
		}

		public DateTime FromDate
		{
			get { return _fromDate; }
			set
			{
				if (value.Equals(_fromDate)) return;
				_fromDate = value;
				NotifyOfPropertyChange(() => FromDate);
			}
		}

		public int NumOfTickets
		{
			get { return _numOfTickets; }
			set
			{
				if (value.Equals(_numOfTickets)) return;
				_numOfTickets = value;
				NotifyOfPropertyChange(() => NumOfTickets);
			}
		}

		public int NumOfCash
		{
			get { return _numOfCash; }
			set
			{
				if (value.Equals(_numOfCash)) return;
				_numOfCash = value;
				NotifyOfPropertyChange(() => NumOfCash);
			}
		}

		public int NumOfVoids
		{
			get { return _numOfVoids; }
			set
			{
				if (value.Equals(_numOfVoids)) return;
				_numOfVoids = value;
				NotifyOfPropertyChange(() => NumOfVoids);
			}
		}

		public string ConnectionString { get; set; }

		public bool AllTickets
		{
			get { return _allTickets; }
			set
			{
				if (value.Equals(_allTickets)) return;
				_allTickets = value;
				NotifyOfPropertyChange(() => AllTickets);
				if (value)
					ExportCondition = TicketExportCondition.All;
			}
		}

		public TicketExportCondition ExportCondition { get; set; }

		public bool NotExported
		{
			get { return _notExported; }
			set
			{
				if (value.Equals(_notExported)) return;
				_notExported = value;
				NotifyOfPropertyChange(() => NotExported);
				if (value)
					ExportCondition = TicketExportCondition.NotExported;
			}
		}

		public bool Exported
		{
			get { return _exported; }
			set
			{
				if (value.Equals(_exported)) return;
				_exported = value;
				NotifyOfPropertyChange(() => Exported);
				if (value)
					ExportCondition = TicketExportCondition.Exported;
			}
		}

		public bool ExportedVoided
		{
			get { return _exportedVoided; }
			set
			{
				if (value.Equals(_exportedVoided)) return;
				_exportedVoided = value;
				NotifyOfPropertyChange(() => ExportedVoided);
				if (value)
					ExportCondition = TicketExportCondition.ExportedAndVoided;
			}
		}

		public bool IsBusy
		{
			get { return _isBusy; }
			set
			{
				if (value.Equals(_isBusy)) return;
				_isBusy = value;
				NotifyOfPropertyChange(() => IsBusy);
			}
		}

		#region Actions

		public List<ICompanyInfo> CompanyList
		{
			get { return _companyList; }
			set
			{
				if (Equals(value, _companyList)) return;
				_companyList = value;
				NotifyOfPropertyChange(() => CompanyList);
			}
		}

		public ICompanyInfo SelectedCompany
		{
			get { return _selectedCompany; }
			set
			{
				if (Equals(value, _selectedCompany)) return;
				_selectedCompany = value;
				NotifyOfPropertyChange(() => SelectedCompany);
			}
		}

		public List<IAccountingInfo> AccountingCustomerCodeList
		{
			get { return _accountingCustomerCodeList; }
			set
			{
				if (Equals(value, _accountingCustomerCodeList)) return;
				_accountingCustomerCodeList = value;
				NotifyOfPropertyChange(() => AccountingCustomerCodeList);
			}
		}

		public IAccountingInfo SelectedAccountingCustomerCode
		{
			get { return _selectedAccountingCustomerCode; }
			set
			{
				if (Equals(value, _selectedAccountingCustomerCode)) return;
				_selectedAccountingCustomerCode = value;
				NotifyOfPropertyChange(() => SelectedAccountingCustomerCode);
			}
		}

		private void FillGrid()
		{
			if (SelectedSite == null
				|| SelectedCompany == null
				|| string.IsNullOrWhiteSpace(SelectedSite.ConnectionString)
				|| (!AllTickets && !Exported && !NotExported && !ExportedVoided))
				return;

			IsBusy = true;
			var task = new Task(GetData);
			task.Start();
		}

		private void GetData()
		{
			try
			{
				Tickets = IoC.Get<ITicketService>()
					.GetDataForExport(SelectedCompany.Name, FromDate, ToDate, ExportCondition,
						ConnectionString).Select(o => new TicketExportModel(o, SelectedSite.Name)).ToList();

				if (Tickets == null)
				{
					Action action = () =>
					{
						MessageBox.Show(string.Format($"Getting the tickets you searched for came back with no results."), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
						TryClose(false);
					};

					action.OnUIThreadAsync();
				}

				SelectedTicket = Tickets.FirstOrDefault();
				//SelectedAccountingCustomerCode = AccountingCustomerCodeList.Where(a => a.CustomerId == Tickets.  a.AccountingCustomerId == Tickets.)
				//SelectedAccountingCustomerCode = Tickets.

				NumOfTickets = Tickets.Count();
				NumOfCash = Tickets.Where(t => t.CashTicket).Count();
				NumOfVoids = Tickets.Where(t => t.Voided).Count();

			}
			catch (Exception ex)
			{
				_log.Error(ex);

                _log.Error(new Exception($"An unexpected error occured while searching for tickets to show.  The search details are...{Environment.NewLine}Site Name: {SelectedSite.Name}{Environment.NewLine}Company: {SelectedCompany.Name}{Environment.NewLine}Date From: {FromDate}{Environment.NewLine}Date To: {ToDate}{Environment.NewLine}Export Condition: {ExportCondition}"));

                Action action = () =>
                {
                    MessageBox.Show(string.Format($"An unexpected error occured while searching for tickets to show.{Environment.NewLine}TicketExportViewModel:GetData"), "Unexpected Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    TryClose(false);
                };

                action.OnUIThreadAsync();
            }
			finally
			{
				IsBusy = false;
			}
		}

		public TicketExportModel SelectedTicket
		{
			get { return _selectedTicket; }
			set
			{
				if (Equals(value, _selectedTicket)) return;
				_selectedTicket = value;
				NotifyOfPropertyChange(() => SelectedTicket);
			}
		}

		public List<TicketExportModel> Tickets
		{
			get { return _tickets; }
			set
			{
				if (Equals(value, _tickets)) return;
				_tickets = value;
				NotifyOfPropertyChange(() => Tickets);
			}
		}

		public void EditItem()
		{
			SaveGpCustomer();
		}

		public void Search()
		{
            if (SelectedSite == null
                || SelectedCompany == null
                || string.IsNullOrWhiteSpace(SelectedSite.ConnectionString)
                || (!AllTickets && !Exported && !NotExported && !ExportedVoided))
            {
                Action action = () =>
                {
                    MessageBox.Show(string.Format($"Please choose what tickets you want to search for"), "Choose Tickets to Search", MessageBoxButton.OK);
                };

                action.OnUIThreadAsync();

                return;
            }

			FillGrid();
		}

        public void KeyHandle(Key key)
        {
            if (key == Key.F2)
            {
                Done();
            }
        }

        public void Done()
        {
            TryClose();
        }

        public void Close()
		{
			TryClose();
		}

		public void Export()
		{
			if (SelectedSite == null
				|| SelectedCompany == null
				|| string.IsNullOrWhiteSpace(SelectedSite.ConnectionString)
				|| (!AllTickets && !Exported && !NotExported && !ExportedVoided))
				return;

			ExportGrid();
		}

		private void ExportGrid()
		{
			if (SelectedSite == null
				|| SelectedCompany == null
				|| string.IsNullOrWhiteSpace(SelectedSite.ConnectionString)
				|| (!AllTickets && !Exported && !NotExported && !ExportedVoided))
				return;

			IsBusy = true;
			var task = new Task(ExportData);
			task.Start();
		}

		private void ExportData()
		{
			try
			{
				Tickets = IoC.Get<ITicketService>()
					.GetDataForExport(SelectedCompany.Name, FromDate, ToDate, ExportCondition,
						ConnectionString).Select(o => new TicketExportModel(o, SelectedSite.Name)).ToList();

				SelectedTicket = Tickets.FirstOrDefault();

				if (Validate())
				{
                    //// Debug - OLD
                    //var serverPath_History = @"C:\Development\Eastern\SaltScale\Repository\DevDynamicsExport";
                    //var serverPath_ToBeIntegrated = @"C:\Development\Eastern\SaltScale\Repository\DevDynamicsExport";
                    //// Debug
                    //var serverPath_History = @"C:\Development\DevDynamicsExport";
                    //var serverPath_ToBeIntegrated = @"C:\Development\DevDynamicsExport";

                    // Production
                    var serverPath_History = @"\\eastern-db1.eastern.com";   // 2018-Jul-10, JGS - eastern-db1
                    var serverPath_ToBeIntegrated = @"\\eastern-db1.eastern.com"; // 2018-Jul-10, JGS - eastern-db1

                    var exportPathToERP_History = serverPath_History + @"\ERP\IM\Data\History";
					var exportPathToERP_ToBeIntegrated = serverPath_ToBeIntegrated + @"\ERP\IM\Data\ToBeIntegrated";
					

					var exportDirPathForCompany = string.Empty;
					var exportDirPathHistoryCompany = string.Empty;
					var exportDirPathToBeIntegratedCompany = string.Empty;
                    var exportFileNameCompanyPrefix = string.Empty;

					if (Directory.Exists(exportPathToERP_History) && Directory.Exists(exportPathToERP_ToBeIntegrated))
					{
						switch (SelectedCompany.Id)
						{
							case 1:
								exportDirPathForCompany = "AS";
                                exportFileNameCompanyPrefix = "AS";
                                break;
							case 2:
								exportDirPathForCompany = "EM";
                                exportFileNameCompanyPrefix = "EMS";
                                break;
							case 3:
								exportDirPathForCompany = "ES";
                                exportFileNameCompanyPrefix = "EMS";
                                exportPathToERP_ToBeIntegrated = serverPath_ToBeIntegrated + @"\ERP\IM\EasternSalt\Data\ToBeIntegrated";
								break;
							case 8:
								exportDirPathForCompany = "GS";
                                exportFileNameCompanyPrefix = "GS";
                                break;
							case 10:
								exportDirPathForCompany = "OP";
                                exportFileNameCompanyPrefix = "OP";
                                break;
							default:
								MessageBox.Show("An export location has not been set in the program for this Company","No Export Location", MessageBoxButton.OK);
								return;
						}
					}
					else
					{
						MessageBox.Show($"An export location has not been created on the Dynamics server.  Please check both folder locations exist: {Environment.NewLine}{Environment.NewLine}{exportPathToERP_History}{Environment.NewLine}{Environment.NewLine}{exportPathToERP_ToBeIntegrated}", "Missing Folder on Dynamics Server", MessageBoxButton.OK);
						return;
					}

					exportDirPathHistoryCompany = exportPathToERP_History + Path.DirectorySeparatorChar + exportDirPathForCompany;
					exportDirPathToBeIntegratedCompany = exportPathToERP_ToBeIntegrated + Path.DirectorySeparatorChar + exportDirPathForCompany;
					   

					if (Directory.Exists(exportDirPathHistoryCompany)
						&& Directory.Exists(exportDirPathToBeIntegratedCompany)
						)
					{
                        // 2018-Jul-10, JGS - Do not allow items without GPIDs to be exported
                        //var ticketsToExport = Tickets.Where(t => !t.Exported.HasValue || !t.Exported.Value).ToList();
                        var ticketsToExport = Tickets.Where(t => !string.IsNullOrWhiteSpace(t.AccountingCustomerCode) && (!t.Exported.HasValue || !t.Exported.Value)).ToList();
                        IoC.Get<ITicketService>().MarkAsExported(ticketsToExport.Select(t => t.TicketNumber), ConnectionString);

                        List<string> fileNameList = new List<string>();
                        fileNameList.Add(string.Format(exportDirPathHistoryCompany + Path.DirectorySeparatorChar + $"Temp{exportFileNameCompanyPrefix}DATA{DateTime.Now.ToString("MMddHHmm")}.csv"));
                        fileNameList.Add(string.Format(exportDirPathToBeIntegratedCompany + Path.DirectorySeparatorChar + $"{exportFileNameCompanyPrefix}DATA{DateTime.Now.ToString("MMddHHmm")}.csv"));

                        var rowCount = 0;
						foreach (var fileToCreate in fileNameList)
						{
							rowCount = 0;
							using (var writer = new CsvFileWriter(fileToCreate))
							{
								writer.Quote = '"';
								writer.Delimiter = ',';

								writer.WriteRow(_header);

								foreach (var item in ticketsToExport)
								{
									writer.WriteRow(item.GetRowDataForExportFile());
									rowCount++;
								}
							}
						}
						// Eastern Salt Co Inc -> Eastern Salt
						// Eastern Minerals... -> Eastern Minerals

                        var exportCompletedMessage = string.Format($"A total of {rowCount} tickets were successfully exported to {Environment.NewLine}{Environment.NewLine}{fileNameList[0].ToString()}{Environment.NewLine}{Environment.NewLine}{fileNameList[1].ToString()}");

                        Action action = () =>
						{
                            MessageBox.Show(exportCompletedMessage, "Accounting Export Completed", MessageBoxButton.OK);
                        };

						action.OnUIThreadAsync();

                        _log.Info($"Account Export completed, message presented to user: '{exportCompletedMessage}'");

                        FillGrid();

                    }
					else
					{
						MessageBox.Show($"The export location for the company '{exportDirPathForCompany}' has not been created on the Dynamics GP server.  Please check if the folder '\\{exportDirPathForCompany}' exists in folder location: {Environment.NewLine}{Environment.NewLine}{exportDirPathHistoryCompany}{Environment.NewLine}{Environment.NewLine}{exportDirPathToBeIntegratedCompany}", "Missing Company Folder on Dynamics Server", MessageBoxButton.OK);
						return;
					}

                    /*
					var dialogResult = DialogResult.Cancel;
					var fileName = string.Format("{0} {1}.csv", DateTime.Now.ToString("yyyMMdd-hhmmss"),
						SelectedSite.Name);
					Action action = () =>
					{
						using (var saveFileService = new SaveFileDialog())
						{
							saveFileService.DefaultExt = "csv";
							saveFileService.Filter = "Comma separated file (*.csv)|.csv";
							saveFileService.OverwritePrompt = true;
							saveFileService.FileName = fileName;
							saveFileService.InitialDirectory =
								Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
							saveFileService.AddExtension = true;
							dialogResult = saveFileService.ShowDialog();
							if (dialogResult == DialogResult.OK)
								fileName = saveFileService.FileName;
						}
					};
					action.OnUIThread();

					if (dialogResult == DialogResult.OK)
					{
						var ticketsToExport = Tickets.Where(t => !t.Exported.HasValue || !t.Exported.Value).ToList();

						using (var writer = new CsvFileWriter(fileName))
						{
							writer.Quote = '"';
							writer.Delimiter = ',';

							writer.WriteRow(_header);

							foreach (var item in ticketsToExport)
							{
								writer.WriteRow(item.GetRowDataForExportFile());
							}
						}

						IoC.Get<ITicketService>()
							.MarkAsExported(ticketsToExport.Select(t => t.TicketNumber), ConnectionString);

						TryClose(true);
					}
					*/
                }
            }
			catch (Exception ex)
			{
				_log.Error(ex);
			}
			finally
			{
				IsBusy = false;
			}
		}

		private bool Validate()
		{
			Errors = null;

            var notExported = Tickets.Any(t => !t.Exported.HasValue || !t.Exported.Value);
            
            if (!notExported)
            {
				Errors = "All tickets already exported";
				ShowMessage("Validation Errors", Errors, NotificationType.Warning);
				return false;
			}

            var ticketMissingAccountingCode = Tickets.Where(t => t.AccountingCustomerCode == string.Empty).Any();
            /*var noGpId =
				Tickets.Any(
					t =>
						(!t.Exported.HasValue || !t.Exported.Value) 
						&& t.CustomerId.HasValue 
						&& t.AccountingCustomerId == 0
						&& !t.CashTicket
						);
            */
			if (ticketMissingAccountingCode)
			{
				Errors = "Accounting Customer Code is missing";
				ShowMessage("Validation Errors", Errors, NotificationType.Warning);
				return false;
			}

			return true;
		}

		public string Errors
		{
			get { return _errors; }
			set
			{
				if (value == _errors) return;
				_errors = value;
				NotifyOfPropertyChange(() => Errors);
			}
		}

		public void OnClose(CancelEventArgs args)
		{
		}

		private void SaveGpCustomer()
		{
			if (SelectedTicket != null && SelectedSite != null && SelectedTicket.CustomerId.HasValue)
			{
				var result = IoC.Get<ICustomerService>().GetAccountingCustomerCodes(SelectedTicket.AccountingCustomerCode, ConnectionString).FirstOrDefault();


				if (result == null || result.AccountingCustomerCode == null)
				{
					SelectedTicket.AccountingCustomerId = IoC.Get<ICustomerService>().AddNewAccountingInfo(SelectedTicket.AccountingCustomerCode, ConnectionString);
				}
                else
                {
                    SelectedTicket.AccountingCustomerId = result.AccountingCustomerId;
                }         

				IoC.Get<ICustomerService>().UpdateGpId(SelectedTicket.CustomerId.Value, SelectedTicket.AccountingCustomerId, _season.SeasonId, ConnectionString);
				/*
				var data = Tickets.Where(t => t.CustomerId == SelectedTicket.CustomerId).ToList();
				foreach (var ticketExportData in data)
				{
					ticketExportData.AccountingCustomerId = SelectedTicket.AccountingCustomerId;
				}
				*/

				Errors = null;
				FillGrid();
			}

		}


#endregion

		private void InitSeason()
		{
			_season = IoC.Get<ICommonService>().GetActiveSeason(ConnectionString);
			FromDate = _season.StartDate;
            //ToDate = FromDate <= _season.EndDate ? _season.EndDate : FromDate.AddYears(1);
            ToDate = DateTime.Today.AddDays(-1).AddHours(23).AddMinutes(59);
        }

		protected override void OnInitialize()
		{
			base.OnInitialize();
			SiteList = _configuration.ExistingSites;
			ISiteConfig site = _configuration.SelectedSite;
			SelectedSite = SiteList.First(s => s.Name == site.Name);

			NumOfTickets = 0;
			NumOfCash = 0;
			NumOfVoids = 0;

			IsEnableCompany = true;
		}

		private void InitCompany()
		{
			var companies = IoC.Get<ICommonService>().GetCompanies(ConnectionString).ToList();
			//var companyItem = new CompanyItem {Id = 9999, Name = "All"};
			//companies.Insert(0, companyItem);
			SelectedCompany = null;
			CompanyList = companies;
			SelectedCompany = CompanyList.FirstOrDefault();
		}

		private readonly string[] _header =
		{
			"Ticket Number",
			"Purchase Order",
			"Ship Date",
			"Company",
			"Customer ID",
			"Great Plain ID",
			"Quantity",
			"Manual Weight",
			"Material",
			"Location",
			"Truck ID",
			"Trucking Cost",
			"Cash Ticket",
			"Material Cost",
			"Sales Tax",
			"Voided",
			"Void Reason",
			"Exported"
		};
	}
}