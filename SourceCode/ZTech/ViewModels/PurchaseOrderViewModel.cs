﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Caliburn.Micro;
using Contract.Domain;
using Contract.Events;
using Contract.Exception;
using Contract.Services;
using ZTech.Controls;
using ZTech.ViewModels.Abstract;
using ZTech.ViewModels.ViewObject;
using Action = System.Action;
using ValidationResult = System.ComponentModel.DataAnnotations.ValidationResult;

namespace ZTech.ViewModels
{
    public class PurchaseOrderViewModel : BaseViewModel
    {
        private readonly IEventAggregator _eventAggregator;
        private readonly ILog _log;
        private readonly int _seasonId;
        //About this Customer Informat
        private ICustomerAddress _customer;
        //PurchaseOrder Information
        private int _purchaseOrderId;
        private PurchaseOrderDataModel _purchaseOrder;
        private IMaterialInfo _selectedMaterial;
        private List<IMaterialInfo> _materialList;
        private string _errors;

        //Model Properties
        public bool IsNew { get; set; }
        public int CustomerId { get; set; }
        public string Errors
        {
            get { return _errors; }
            set
            {
                if (value == _errors) return;
                _errors = value;
                NotifyOfPropertyChange(() => Errors);
            }
        }
        
        #region CustomerInformation

        public ICustomerAddress Customer
        {
            get { return _customer; }
            set
            {
                if (Equals(value, _customer)) return;
                _customer = value;
                NotifyOfPropertyChange();
            }
        }

        public string CustomerAddressInfo
        {
            get
            {
                if (Customer != null)
                {
                    return Customer.PrimaryContactOrganization + Environment.NewLine + Customer.FullAddress;
                }
                return String.Empty;
            }
        }

        private void InitializeCustomer()
        {
            Customer = IoC.Get<ICustomerService>().GetCustomerAddress(CustomerId, _seasonId);
        }

        #endregion

        #region About this Purchase Order

        public PurchaseOrderDataModel PurchaseOrder
        {
            get { return _purchaseOrder; }
            set
            {
                if (Equals(value, _purchaseOrder)) return;
                _purchaseOrder = value;
                NotifyOfPropertyChange(() => PurchaseOrder);
            }
        }

        public int PurchaseOrderId
        {
            get { return _purchaseOrderId; }
            set
            {
                _purchaseOrderId = value;
                InitializePurchaseOrder();
            }
        }

        public List<IMaterialInfo> MaterialList
        {
            get { return _materialList; }
            set
            {
                if (Equals(value, _materialList)) return;
                _materialList = value;
                NotifyOfPropertyChange(() => MaterialList);
            }
        }

        public IMaterialInfo SelectedMaterial
        {
            get { return _selectedMaterial; }
            set
            {
                _selectedMaterial = value;
                NotifyOfPropertyChange(() => SelectedMaterial);

            }
        }

        private void InitializePurchaseOrder()
        {
            try
            {
                InitializeMaterials();

                if (CustomerId != 0)
                {
                    if (!IsNew)
                    {
                        PurchaseOrder = new PurchaseOrderDataModel(IoC.Get<IPurchaseOrderService>().GetPurchaseOrder(PurchaseOrderId));
                        SelectedMaterial = MaterialList.Where(w => w.Id == PurchaseOrder.MaterialId).FirstOrDefault();
                    }
                    else
                    {
                        PurchaseOrder = new PurchaseOrderDataModel(IoC.Get<IPurchaseOrderService>().GetNewPurchaseOrder(_seasonId, CustomerId));
                    }
                }

                



                /*
                if (MaterialList == null) InitializeMaterials();

                var materialInfo = _materialList.FirstOrDefault(m => m.Id == PurchaseOrder.MaterialId);
                if (materialInfo == null)
                {
                    var inactiveMaterial = IoC.Get<IMaterialService>().GetInactiveMaterial(PurchaseOrder.MaterialId);
                    if (inactiveMaterial != null)
                    {
                        MaterialList.Add(inactiveMaterial);
                        SelectedMaterial = inactiveMaterial;
                    }
                }
                else
                {
                    SelectedMaterial = materialInfo;
                }
                */

            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "Purchase Order failed with exception {0}. Please reopen application and try again or contact HelpDesk.",
                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };

                action.OnUIThreadAsync();
            }
        }

        private void InitializeMaterials()
        {
            MaterialList = IoC.Get<IMaterialService>().GetMaterialsOfCustomer(_seasonId, CustomerId);
        }

        #endregion

        #region Actions

        public void Cancel()
        {
            TryClose(false);
        }

        public void Done()
        {
            try
            {
                PurchaseOrder.MaterialId = SelectedMaterial.Id;
                PurchaseOrder.MaterialDescription = SelectedMaterial.Description;

                if (!Validate())
                    return;

                IoC.Get<IPurchaseOrderService>().SavePurchaseOrder(PurchaseOrder, CustomerId);
                _eventAggregator.PublishOnCurrentThread(new PurchaseOrderModifiedArgs(PurchaseOrder.Id));
                TryClose(true);
            }
            catch (UniqueConstraintException ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult = (MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };

                action.OnUIThreadAsync();
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "Purchase Order saving failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };

                action.OnUIThreadAsync();
            }
        }
        /* //--Unused
        public void Report()
        {
            decimal purchased;

            if (!Customer.IsShippingLocation)
            {
                var shippingLocations = IoC.Get<ICustomerService>().GetShippingLocations(_seasonId, Customer.Id);
                purchased = IoC.Get<IPurchaseOrderService>()
                    .GetPurchasedAmount(PurchaseOrder.PurchaseOrderNumber, shippingLocations);
            }
            else
            {
                purchased = IoC.Get<IPurchaseOrderService>()
                    .GetPurchasedAmount(PurchaseOrder.PurchaseOrderNumber, new List<int> { Customer.Id });
            }

            var reportContent = String.Format("{0} has purchased {1}", Customer.Name, purchased.ToString("N2"));

            var newReportViewerViewModel = IoC.Get<ReportViewerViewModel>();
            newReportViewerViewModel.ReportContent = reportContent;

            var windowManager = IoC.Get<IWindowManager>();
            windowManager.ShowDialog(newReportViewerViewModel);
        }
        */

        private bool Validate()
        {
            Errors = null;

            ICollection<ValidationResult> validationResults = new Collection<ValidationResult>();
            bool result = Validator.TryValidateObject(PurchaseOrder, new ValidationContext(PurchaseOrder), validationResults, true);
            if (SelectedMaterial == null)
            {
                validationResults.Add(new ValidationResult("The Material cannot be left blank."));
            }

            if (!result || validationResults.Any())
            {
                Errors = string.Format("Validation Failed.{0}{1}", Environment.NewLine,
                    string.Join("\n", validationResults.Select(v => v.ErrorMessage)));

                ShowMessage("Validation Errors", Errors, NotificationType.Warning);

                return false;
            }

            return true;
        }

        public void Delete()
        {
            var message = string.Format("Are you sure you want to delete {0} Purchase Order", PurchaseOrder.PurchaseOrderNumber);
            MessageBoxResult result = MessageBox.Show(message, "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                if (IoC.Get<IPurchaseOrderService>().DeletePurchaseOrder(PurchaseOrder.Id))
                {
                    _eventAggregator.PublishOnCurrentThread(new PurchaseOrderModifiedArgs(PurchaseOrder.Id));
                    TryClose(true);
                }
            }
        }

        #endregion

        #region Events

        public PurchaseOrderViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            _log = LogManager.GetLog(typeof(PurchaseOrderViewModel));
            _seasonId = (int)Application.Current.Properties["SeasonId"];
            DisplayName = "Purchase Order";
        }

        protected override void OnInitialize()
        {
            try
            {
                base.OnInitialize();
                InitializeCustomer();

                InitializePurchaseOrder();
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(
                            string.Format(
                                "Initialization failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                                ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));

                    TryClose(false);
                };

                action.OnUIThreadAsync();
            }
        }

        protected override void OnActivate()
        {
            try
            {
                _eventAggregator.Subscribe(this);
                base.OnActivate();
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(
                            string.Format(
                                "Initialization failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                                ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));

                    TryClose(false);
                };

                action.OnUIThreadAsync();
            }
        }

        protected override void OnDeactivate(bool close)
        {
            _eventAggregator.Unsubscribe(this);
            base.OnDeactivate(close);
        }

        public void KeyHandle(Key key)
        {
            if (key == Key.F2)
            {
                Done();
            }
        }

        #endregion
    }
}