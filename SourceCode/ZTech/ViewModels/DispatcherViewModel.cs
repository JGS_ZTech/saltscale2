using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Caliburn.Micro;
using Contract.Domain;
using Contract.Events;
using Contract.Services;
using Action = System.Action;
using System.Windows.Controls;
using Syncfusion.UI.Xaml.Grid;
using System.ComponentModel;

namespace ZTech.ViewModels
{
    public class DispatcherViewModel : Screen, IHandle<OrderModifiedArgs>, IHandle<TruckModifiedArgs>
    {
        private readonly IEventAggregator _eventAggregator;
        private readonly IConfiguration _configuration;
        private readonly ILog _log;
        private CustomerItem _customer;
        private IObservableCollection<OrderLine> _orderList;
        private IOrderKey _pendingOrder;
        private List<IOrderKey> _pendingOrders;
        //private IPurchaseOrderKey _pendingPurchaseOrder;
        //private List<IPurchaseOrderKey> _pendingPurchaseOrders;
        //private ScaleWidgetViewModel _scaleWidget;
        private string _searchOrderWord;
        private string _searchTruckWord;
        private int _seasonId;
        private OrderLine _selectedOrder;
        private ITruckInfo _selectedTruck;
        private IObservableCollection<ITruckInfo> _truckList;

        private bool _isVisibleCustomerService;
        private bool _isVisibleOrderCreateEditDelete;

        private GridRowSizingOptions _orderGridRowResizingOptions = new GridRowSizingOptions();
        private double _orderGridQueryRowHeightAutoSize;

        public DispatcherViewModel(IEventAggregator eventAggregator, IConfiguration configuration)
        {
            DisplayName = "";
            _eventAggregator = eventAggregator;
            _configuration = configuration;
            _log = LogManager.GetLog(typeof (DispatcherViewModel));
            _seasonId = (int) Application.Current.Properties["SeasonId"];
            //TODO
            var POBalance = IoC.Get<IPurchaseOrderService>().GetBalanceOfAllPurchaseOrders(_seasonId);
        }

        protected override void OnInitialize()
        {
            base.OnInitialize();

            InitializeGrid();
            //InitializeScale();
            InitializeTruckGrid();

            if (UserAuthorization.UserAuthorizationLoggedIn == null)
            {
                IsVisibleCustomerService = false;
                IsVisibleOrderCreateEditDelete = false;
            }
            else
            {
                IsVisibleCustomerService = UserAuthorization.UserAuthorizationLoggedIn.IsAuthorizedCustomerService;
                IsVisibleOrderCreateEditDelete = UserAuthorization.UserAuthorizationLoggedIn.IsAuthorizedOrderCreateEditDelete;
            }
        }


        #region User Authorized Buttons
        public bool IsVisibleCustomerService
        {
            get { return _isVisibleCustomerService; }
            set
            {
                if (Equals(value, _isVisibleCustomerService)) return;
                _isVisibleCustomerService = value;
                NotifyOfPropertyChange(() => IsVisibleCustomerService);
            }
        }

        public bool IsVisibleOrderCreateEditDelete
        {
            get { return _isVisibleOrderCreateEditDelete; }
            set
            {
                if (Equals(value, _isVisibleOrderCreateEditDelete)) return;
                _isVisibleOrderCreateEditDelete = value;
                NotifyOfPropertyChange(() => IsVisibleOrderCreateEditDelete);
            }
        }
        
        #endregion

        #region Order Grid

        public IObservableCollection<OrderLine> OrderList
        {
            get
            {
                return _orderList;
            }
            set
            {
                _orderList = value;
                NotifyOfPropertyChange(() => OrderList);
            }
        }

        public OrderLine SelectedOrder
        {
            get { return _selectedOrder; }
            set
            {
                _selectedOrder = value;
                NotifyOfPropertyChange(() => SelectedOrder);
                InitializeCustomer();
            }
        }

        public List<IOrderKey> PendingOrders
        {
            get { return _pendingOrders; }
            set
            {
                if (Equals(value, _pendingOrders)) return;
                _pendingOrders = value;
                NotifyOfPropertyChange(() => PendingOrders);
            }
        }

        public IOrderKey SelectedOrderKey
        {
            get { return _pendingOrder; }
            set
            {
                if (Equals(value, _pendingOrder)) return;
                _pendingOrder = value;

                NotifyOfPropertyChange(() => SelectedOrderKey);
                if (_pendingOrder != null)
                {
                    SelectedOrder = OrderList.FirstOrDefault(o => o.OrderId == _pendingOrder.Id);
                }
            }
        }

        public CustomerItem Customer
        {
            get { return _customer; }
            set
            {
                if (Equals(value, _customer)) return;
                _customer = value;
                NotifyOfPropertyChange(() => Customer);
            }
        }

        public string SearchOrderWord
        {
            get { return _searchOrderWord; }
            set
            {
                _searchOrderWord = value;
                NotifyOfPropertyChange(() => SearchOrderWord);
            }
        }
        
       
        private void InitializeCustomer()
        {
            try
            {
                if (SelectedOrder != null)
                {
                    var customer = IoC.Get<ICustomerService>().GetCustomerInfo(SelectedOrder.CustomerId);
                    Customer = new CustomerItem(customer, SelectedOrder.SpecialInstructionOverride);
                    PendingOrders = IoC.Get<ICustomerService>().GetCustomerPendingOrders(SelectedOrder.CustomerId, _seasonId);
                }
                else
                {
                    Customer = new CustomerItem();
                    PendingOrders = new List<IOrderKey>();
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action =
                    () =>
                    {
                        MessageBoxResult messageBoxResult =
                            (MessageBox.Show(string.Format(
                                "Load Customer data failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                                ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                    };

                action.OnUIThreadAsync();
            }
        }

        private void InitializeGrid()
        {
            try
            {
                List<IOrderSummary> orders = IoC.Get<IOrderService>().GetFilteredOrders(SearchOrderWord, _seasonId);

                OrderList = new BindableCollection<OrderLine>(orders.Select(item => new OrderLine(item)));
                /*
                if (Application.Current.Properties["DisptcherOrderColumnSortDirection"].Equals(ListSortDirection.Ascending))
                {
                    OrderList = new BindableCollection<OrderLine>(orders.OrderBy(o => o.GetType().GetProperty(Application.Current.Properties["DisptcherOrderColumnSortName"].ToString()).GetValue(o, null)).Select(item => new OrderLine(item)));
                }
                else
                {
                    OrderList = new BindableCollection<OrderLine>(orders.OrderByDescending(o => o.GetType().GetProperty(Application.Current.Properties["DisptcherOrderColumnSortName"].ToString()).GetValue(o, null)).Select(item => new OrderLine(item)));
                }
                */
                if (OrderList != null)
                {
                    OrderLine orderLine = OrderList.FirstOrDefault();
                    SelectedOrder = orderLine;
                }

                _configuration.LastOrderUpdate = DateTime.Now;

                _eventAggregator.PublishOnCurrentThread(new TicketAddedVoidedArgs(null));

                SelectedOrder = null;

            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action =
                    () =>
                    {
                        MessageBoxResult messageBoxResult =
                            (MessageBox.Show(string.Format(
                                "Load Orders failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                                ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                    };

                action.OnUIThreadAsync();
            }
           
        }

        public void RefreshOrders()
        {
            SearchOrderWord = null;
            InitializeGrid();
        }
        //public void RefreshOrderList()
        //{
        //    SearchOrderWord = null;
        //    InitializeGrid();

        //    _eventAggregator.PublishOnCurrentThread(new TicketAddedVoidedArgs(null));
        //}

        public void RefreshTrucks()
        {
            SearchTruckWord = null;
            InitializeTruckGrid();
        }

        public void SearchOrder()
        {
            InitializeGrid();

            // Set the selected order to the only item in the list
            if (OrderList != null)
            {
                if (OrderList.Count == 1)
                {
                    OrderLine orderLine = OrderList.FirstOrDefault();
                    SelectedOrder = orderLine;
                    //EditOrder();
                }
                else
                {
                    SelectedOrder = null;
                }
            }
            else
            {
                SelectedOrder = null;
            }
        }

        public void ExecuteOrderSearch(Key key)
        {
            if (key == Key.Enter)
            {
                SearchOrder();
            }
        }



        public void ExecuteEditOrder(KeyEventArgs key)
        {
            if (key.Key == Key.Enter)
            {
                EditOrder();
            }
            if (key.Key == Key.F8)
            {
                Ship();
            }
        }

        public void ExecuteEditTruck(Key key)
        {
            if (key == Key.Enter)
            {
                EditTruck();
            }
        }

        #endregion

        #region Truck Grid

        public IObservableCollection<ITruckInfo> TruckList
        {
            get { return _truckList; }
            set
            {
                _truckList = value;
                NotifyOfPropertyChange(() => TruckList);
            }
        }

        public ITruckInfo SelectedTruck
        {
            get { return _selectedTruck; }
            set
            {
                _selectedTruck = value;
                NotifyOfPropertyChange(() => SelectedTruck);
            }
        }

        public string SearchTruckWord
        {
            get { return _searchTruckWord; }
            set
            {
                _searchTruckWord = value;
                NotifyOfPropertyChange(() => SearchTruckWord);
            }
        }

        private void InitializeTruckGrid()
        {
            try
            {
                SelectedTruck = null;

                List<ITruckInfo> trucks = IoC.Get<ITruckService>().GetFilteredTrucks(SearchTruckWord, _seasonId);
                TruckList = new BindableCollection<ITruckInfo>(trucks);
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action =
                    () =>
                    {
                        MessageBoxResult messageBoxResult =
                            (MessageBox.Show(string.Format(
                                "Load Trucks failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                                ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                    };

                action.OnUIThreadAsync();
            }
        }

        public void SearchTruck()
        {           
            InitializeTruckGrid();

            // Set the selected order to the only item in the list
            if (TruckList != null)
            {
                if (TruckList.Count == 1)
                {
                    var truckInfo = TruckList.FirstOrDefault();
                    if (truckInfo != null)
                    {
                        // Select and edit the truck record
                        SelectedTruck = truckInfo;
                        //EditTruck();
                    }
                }
                else
                {
                    SelectedTruck = null;
                }
            }
            else
            {
                SelectedTruck = null;
            }
        }

        public void ExecuteTruckSearch(Key key)
        {
            if (key == Key.Enter)
            {
                SearchTruck();
            }
        }

        #endregion

        #region Scale

        //public ScaleWidgetViewModel ScaleWidget
        //{
        //    get { return _scaleWidget; }
        //    set
        //    {
        //        if (Equals(value, _scaleWidget)) return;
        //        _scaleWidget = value;
        //        NotifyOfPropertyChange(() => ScaleWidget);
        //    }
        //}

        //private void InitializeScale()
        //{
        //    ScaleWidget = IoC.Get<ScaleWidgetViewModel>();
        //    ScaleWidget.ActivateWith(this);
        //}

        #endregion

        #region Actions

        public void NewOrder()
        {
            var customerSelector = IoC.Get<CustomerSelectViewModel>();
            var windowManager = IoC.Get<IWindowManager>();
            bool? result = windowManager.ShowDialog(customerSelector);

            if (!result.HasValue || !result.Value || customerSelector.SelectedCustomer == null) return;

            if (customerSelector.SelectedCustomer.Company.Id == 0)
            {
                _log.Info("Customer " + customerSelector.SelectedCustomer.Name + " is associated to company '" + customerSelector.SelectedCustomer.Company.Name + "'.  No order can be created for it.");
                Action action =
                    () =>
                    {
                        MessageBox.Show("This customer is associated to company '" + customerSelector.SelectedCustomer.Company.Name + "' which means no orders can be placed for it", "Company '" + customerSelector.SelectedCustomer.Company.Name + "'", MessageBoxButton.OK, MessageBoxImage.None);
                    };
                return;
            }

            var newOrderViewModel = IoC.Get<NewOrderViewModel>();
            newOrderViewModel.CustomerId = customerSelector.SelectedCustomer.Id;

            windowManager.ShowDialog(newOrderViewModel);
            customerSelector = null; // 20190227, JGS - Make certain objects opened as windows are set to null
            newOrderViewModel = null; // 20190227, JGS - Make certain objects opened as windows are set to null
            windowManager = null; // 20190301, JGS - Make certain objects opened as windows are set to null
        }

        public void EditOrder()
        {
            // Do note process an order if one has not been selected
            if (SelectedOrder == null) return;

            // Get the current order and POOverride
            int localOrderID = SelectedOrder.OrderId;

            // Create a local order object and statuses
            var _localOrder = IoC.Get<IOrderService>().GetOrder(_seasonId, localOrderID);
            string _localOrderNumber = _localOrder.OrderNumber.ToString();
            bool _localOrderIsActive = _localOrder.IsActive;
            bool _localOrderIsVoid = _localOrder.IsVoid;

            // Do not process deleted orders
            if (_localOrderIsVoid)
            {
                var msgbox = MessageBox.Show(String.Format("Order '{0}' has been voided and cannot be edited. Contact the SaltScale Administrator for more details.", _localOrderNumber),
                            "Cannot Edit Voided Order", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            // Do not process closed orders
            if (!_localOrderIsActive)
            {
                var msgbox = MessageBox.Show(String.Format("Order '{0}' has been closed and cannot be edited. Use the 'Reopen' button in the Customer Service screen to reset its status.", _localOrderNumber),
                            "Cannot Edit Closed Order", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            var windowManager = IoC.Get<IWindowManager>();
            var editOrderViewModel = IoC.Get<EditOrderViewModel>();
            editOrderViewModel.OrderId = SelectedOrder.OrderId;
            windowManager.ShowDialog(editOrderViewModel);
            editOrderViewModel = null; // 20190227, JGS - Make certain objects opened as windows are set to null
            windowManager = null; // 20190301, JGS - Make certain objects opened as windows are set to null
        }

        public void ViewCustomerInfo()
        {
            if (SelectedOrder == null) return;

            var windowManager = IoC.Get<IWindowManager>();
            var customerViewModel = IoC.Get<CustomerViewModel>();

            customerViewModel.CustomerId = SelectedOrder.CustomerId;
            windowManager.ShowDialog(customerViewModel);
            customerViewModel = null; // 20190227, JGS - Make certain objects opened as windows are set to null
            windowManager = null; // 20190301, JGS - Make certain objects opened as windows are set to null
        }

        public void KeyHandle(Key key)
        {
            if (key == Key.F8 && SelectedOrder != null)
            {
                Ship();
            }
        }
        public void Ship()
        {
            //if (IsPrinterExist())
            //if (IoC.Get<ICommonService>().GetCompanyData(SelectedOrder.CompanyId))
            //ToDo check if the printer is in the database.  Do it here before the person can ship to not create a ticket that can't print


            if (SelectedOrder == null) return;           
            var windowManager = IoC.Get<IWindowManager>();

            if (!(bool)Application.Current.Properties["IsLogged"])
            {
                var viewModel = IoC.Get<LoginViewModel>();
                var dialogResult = windowManager.ShowDialog(viewModel);

                if (!dialogResult.HasValue || !dialogResult.Value || !viewModel.IsAuthorized)
                {
                    windowManager = null; // 20190227, JGS - Make certain objects opened as windows are set to null
                    viewModel = null; // 20190227, JGS - Make certain objects opened as windows are set to null
                    return;
                }
            }

            var truck = SelectedTruck;
            if (truck == null)
            {
                var truckSelector = IoC.Get<ITruckSelectViewModel>();
             
                if (SelectedTruck != null)
                    truckSelector.TruckId = SelectedTruck.Id;

                bool? result = windowManager.ShowDialog(truckSelector);

                if (!result.HasValue || !result.Value || truckSelector.SelectedTruck == null)
                {
                    windowManager = null; // 20190225, JGS - Make certain objects opened as windows are set to null
                    truckSelector = null; // 20190225, JGS - Make certain objects opened as windows are set to null
                    return;
                }

                truck = truckSelector.SelectedTruck;
                truckSelector = null;  // 20190225, JGS - Make certain objects opened as windows are set to null
            }

            var shipTicketViewModel = IoC.Get<IOrderTicketViewModel>();

            shipTicketViewModel.TruckId = truck.Id;
            shipTicketViewModel.OrderId = SelectedOrder.OrderId;

            windowManager = null; // 20190225, JGS - Make certain objects opened as windows are set to null
            windowManager = IoC.Get<IWindowManager>();

            windowManager.ShowDialog(shipTicketViewModel);
            windowManager = null; // 20190225, JGS - Make certain objects opened as windows are set to null
            shipTicketViewModel.Deactivate(true); // 20190225, JGS - Make certain objects opened as windows are deactivated
            shipTicketViewModel = null; // 20190225, JGS - Make certain objects opened as windows are set to null
        }

        public void NewTruck()
        {
            var windowManager = IoC.Get<IWindowManager>();

            var newTruckViewModel = IoC.Get<NewTruckViewModel>();

            windowManager.ShowDialog(newTruckViewModel);
            //newTruckViewModel.Deactivate(true);  // 20190225, JGS - Make certain objects opened as windows are deactivated
            newTruckViewModel = null;  // 20190225, JGS - Make certain objects opened as windows are set to null
            windowManager = null; // 20190301, JGS - Make certain objects opened as windows are set to null
        }
        
        public void EditTruck()
        {
            if (SelectedTruck == null) return;
            var windowManager = IoC.Get<IWindowManager>();

            var editTruckViewModel = IoC.Get<EditTruckViewModel>();
            
            editTruckViewModel.TruckId = SelectedTruck.Id;

            windowManager.ShowDialog(editTruckViewModel);
            //editTruckViewModel.Deactivate(true);  // 20190225, JGS - Make certain objects opened as windows are deactivated
            editTruckViewModel = null;  // 20190225, JGS - Make certain objects opened as windows are set to null
            windowManager = null; // 20190301, JGS - Make certain objects opened as windows are set to null
        }

        public void OrderRowSelect()
        {
            Tickets();
        }

        public void Tickets()
        {
            if (SelectedOrder == null) return;

            var newTicketViewerViewModel = IoC.Get<TicketViewerViewModel>();
            newTicketViewerViewModel.CustomerShortName = SelectedOrder.CustId;
            newTicketViewerViewModel.CustomerId = SelectedOrder.CustomerId;
            newTicketViewerViewModel.OrderId = SelectedOrder.OrderId;

            var windowManager = IoC.Get<IWindowManager>();
            windowManager.ShowDialog(newTicketViewerViewModel);
            //newTicketViewerViewModel.Deactivate(true);  // 20190225, JGS - Make certain objects opened as windows are deactivated
            newTicketViewerViewModel = null;  // 20190225, JGS - Make certain objects opened as windows are set to null
            windowManager = null; // 20190301, JGS - Make certain objects opened as windows are set to null
        }

        #endregion

        #region Events


        public void Handle(OrderModifiedArgs args)
        {
            if (args != null)
            {
                var currentOrderId = SelectedOrder != null ? SelectedOrder.OrderId : (int?) null;

                InitializeGrid();
                SelectedOrder = null;

                OrderLine orderLine = null;

                if (args.ItemId.HasValue)
                {
                    orderLine = OrderList.FirstOrDefault(o => o.OrderId == args.ItemId.Value);
                }
                if (orderLine == null && currentOrderId.HasValue)
                {
                    orderLine = OrderList.FirstOrDefault(o => o.OrderId == currentOrderId.Value);
                }
                if (orderLine == null)
                {
                    orderLine = OrderList.FirstOrDefault();
                }
                if (orderLine != null)
                    SelectedOrder = orderLine;
            }
        }

        public void Handle(TruckModifiedArgs args)
        {
            if (args != null)
            {
                SearchTruckWord = null;
                SelectedTruck = null;
                InitializeTruckGrid();

                if (!args.ItemId.HasValue) return;
                
                var truckInfo = TruckList.FirstOrDefault(t => t.Id == args.ItemId);
                
                if (truckInfo != null)
                    SelectedTruck = truckInfo;
            }
        }

        public void OnOrderGridLoaded(Syncfusion.UI.Xaml.Grid.SfDataGrid dataGrid)
        {
            dataGrid.SortColumnDescriptions.Add(new SortColumnDescription() { ColumnName = Application.Current.Properties["DisptcherOrderColumnSortName"].ToString()
                                                                            ,SortDirection = (ListSortDirection.Ascending.ToString() == Application.Current.Properties["DisptcherOrderColumnSortDirection"].ToString()) ? ListSortDirection.Ascending : ListSortDirection.Descending }
            );

            dataGrid.SelectionController = new GridSelectionControllerExt(dataGrid);
        }

        public void OnOrderListSorting(Syncfusion.UI.Xaml.Grid.GridSortColumnsChangingEventArgs e)
        {
            _log.Info($"Sorting order grid by column '{e.AddedItems[0].ColumnName}' in '{e.AddedItems[0].SortDirection}' order");
            Application.Current.Properties["DisptcherOrderColumnSortName"] = e.AddedItems[0].ColumnName;
            Application.Current.Properties["DisptcherOrderColumnSortDirection"] = e.AddedItems[0].SortDirection;
        }


        public void OnQueryRowHeight(Syncfusion.UI.Xaml.Grid.SfDataGrid dataGrid, Syncfusion.UI.Xaml.Grid.QueryRowHeightEventArgs e)
        {
            if (dataGrid.GridColumnSizer.GetAutoRowHeight(e.RowIndex, _orderGridRowResizingOptions, out _orderGridQueryRowHeightAutoSize))
            {
                if (_orderGridQueryRowHeightAutoSize > 24)
                {
                    e.Height = _orderGridQueryRowHeightAutoSize;
                    e.Handled = true;
                }
            }
        }

        protected override void OnActivate()
        {
            _eventAggregator.Subscribe(this);
            base.OnActivate();
        }

        protected override void OnDeactivate(bool close)
        {
            _eventAggregator.Unsubscribe(this);
            base.OnDeactivate(close);
        }

        #endregion

        #region VO

        public class CustomerItem
        {
            public CustomerItem()
            {
            }

            public CustomerItem(ICustomerInfo customer, string specialInstruction)
            {
                Address = customer.Address;
                ShortName = customer.ShortName;
                Name = customer.Name;
                Id = customer.Id;
                Phone = customer.Phone;
                FullAddress = string.Format("{0}{2}{1}{2}Phone: {3}",customer.OrganizationName, customer.FullAddress, Environment.NewLine, Phone);
                Comment = customer.Comment;

                SpecialInstruction = string.IsNullOrWhiteSpace(specialInstruction)
                    ? customer.SpecialInstruction
                    : specialInstruction;
            }

            public string Comment { get; set; }

            public string FullAddress { get; set; }

            public string Phone { get; set; }

            public string Address { get; set; }

            public string SpecialInstruction { get; set; }

            public string ShortName { get; set; }

            public string Name { get; set; }

            public int Id { get; set; }
        }

        public class OrderLine
        {
            public OrderLine()
            {
            }

            public OrderLine(IOrderSummary orderSummary)
            {
                OrderId = orderSummary.OrderId;
                OrderNumber = orderSummary.OrderNumber;
                Date = orderSummary.EffectiveTime;
                CustomerId = orderSummary.CustomerId;
                CustId = orderSummary.CustId;
                Quantity = orderSummary.Quantity;
                Customer = orderSummary.CustomerName;
                CompanyId = orderSummary.CompanyId;
                CompanyName = orderSummary.CompanyName;
                CustomerCity = orderSummary.CustomerCity;
                CustomerState = orderSummary.CustomerState;
                Priority = orderSummary.Priority;
                
                Balance = orderSummary.Balance;
                LastDate = orderSummary.LastTicketDate.HasValue
                    ? orderSummary.LastTicketDate.Value
                    : (DateTime?)null;
                Today = orderSummary.TodayTons;
                PoOverride = orderSummary.PoOverride;
                Comment = orderSummary.Comment;
                SpecialInstructionOverride = orderSummary.SpecialInstructionOverride;
                if (orderSummary.PromiseExpiresTime.HasValue
                    && orderSummary.PromiseExpiresTime.Value.Date >= DateTime.Today
                    && !string.IsNullOrWhiteSpace(orderSummary.Promise)
                    )
                    Promise = orderSummary.Promise;
                Material = orderSummary.Material;
                ConfirmationNumber = orderSummary.ConfirmationNumber;
                IsVoid = orderSummary.IsVoid;
            }

            public int CompanyId { get; set; }
            public object CompanyName { get; set; }
            public string CustomerCity { get; }
            public string CustomerState { get; }
            public string Promise { get; set; }

            public string SpecialInstructionOverride { get; set; }
            public string Comment { get; set; }
       
            public int CustomerId { get; set; }
            public int? OrderNumber { get; set; }
            public int OrderId { get; set; }
            public DateTime Date { get; set; }
            public string CustId { get; set; }
            public int? Quantity { get; set; }
            public string Customer { get; set; }
            public string Priority { get; set; }
            public float Balance { get; set; }
            public DateTime? LastDate { get; set; }
            public float Today { get; set; }
            public string PoOverride { get; set; }
            public string Material { get; set; }
            public int? ConfirmationNumber { get; set; }
            public bool IsVoid { get; set; }
        }

        #endregion
    }

    public class GridSelectionControllerExt : GridSelectionController
    {
        private SfDataGrid grid;

        public GridSelectionControllerExt(SfDataGrid datagrid)
        : base(datagrid)
        {

        }
        
        protected override void ProcessKeyDown(KeyEventArgs args)
        {
            var currentKey = args.Key;
            var arguments = new KeyEventArgs(args.KeyboardDevice, args.InputSource, args.Timestamp, Key.Tab)
            {
                RoutedEvent = args.RoutedEvent
            };
            if (currentKey == Key.Enter)
            {
                base.ProcessKeyDown(arguments);
                args.Handled = arguments.Handled;

                return;
            }
            
            base.ProcessKeyDown(args);
        }
    
    }
}