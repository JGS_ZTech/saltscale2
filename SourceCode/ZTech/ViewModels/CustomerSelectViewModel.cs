﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using Caliburn.Micro;
using Contract.Domain;
using Contract.Services;
using Action = System.Action;
using System.Linq;

namespace ZTech.ViewModels
{
    public interface ICustomerSelectViewModel : IScreen
    {
        int? CustomerId { get; set; }
        ICustomerLine SelectedCustomer { get; set; }
    }

    public class CustomerSelectViewModel : Screen, ICustomerSelectViewModel
    {
        private readonly IEventAggregator _eventAggregator;
        private readonly ILog _log;
        private readonly int _seasonId;
        private bool _canDone;
        //private bool _canSearch;
        private string _filter;
        private ICustomerLine _selectedCustomer;
        private List<ICustomerLine> _customerList;

        public CustomerSelectViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            _log = LogManager.GetLog(typeof (CustomerSelectViewModel));
            _seasonId = (int) Application.Current.Properties["SeasonId"];
            DisplayName = "Select Customer";
        }

        public int? CustomerId { get; set; }

        public string Filter
        {
            get { return _filter; }
            set
            {
                if (value == _filter) return;
                _filter = value;
                NotifyOfPropertyChange(() => Filter);
                Search();
            }
        }

        public ICustomerLine SelectedCustomer
        {
            get { return _selectedCustomer; }
            set
            {
                if (Equals(value, _selectedCustomer)) return;
                _selectedCustomer = value;
                NotifyOfPropertyChange(() => SelectedCustomer);

                CanDone = SelectedCustomer != null;
            }
        }

        public List<ICustomerLine> CustomerList
        {
            get { return _customerList; }
            set
            {
                if (Equals(value, _customerList)) return;
                _customerList = value;
                NotifyOfPropertyChange(() => CustomerList);
            }
        }

        public bool CanDone
        {
            get { return _canDone; }
            set
            {
                if (value.Equals(_canDone)) return;
                _canDone = value;
                NotifyOfPropertyChange(() => CanDone);
            }
        }

        protected override void OnInitialize()
        {
            base.OnInitialize();
            try
            {
                CustomerList = IoC.Get<ICustomerService>().GetCustomersForSelector(string.Empty, _seasonId);
                if (!CustomerId.HasValue)
                    return;

                var selectedCustomerFromList = CustomerList.FirstOrDefault(t => t.Id == CustomerId.Value);
                if (selectedCustomerFromList != null)
                    SelectedCustomer = selectedCustomerFromList;

                /*
                var selectedCustomerCompanyName = IoC.Get<ICustomerService>().GetCustomerBidInformation(_seasonId, SelectedCustomer.BidId);
                if (selectedCustomerCompanyName != null)
                    SelectedCustomerCompanyName = selectedCustomerCompanyName.Company.Name;
                */

            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(
                            string.Format(
                                "Initialization failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                                ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));

                    TryClose(false);
                };

                action.OnUIThreadAsync();
            }
        }

        public void Cancel()
        {
            TryClose(false);
        }

        public void Done()
        {
            if (SelectedCustomer == null)
                return;

            if (SelectedCustomer?.Company.Id == 0)
            {
                _log.Info("Customer " + SelectedCustomer.Name + " is associated to company '" + SelectedCustomer?.Company.Name + "'.  No order can be created for it.");

                Action action =
                    () =>
                    {
                        MessageBox.Show("This customer is associated with company 'Unassigned' which means no orders can be placed for it", "Company 'Unassigned'", MessageBoxButton.OK, MessageBoxImage.None);
                    };
                action.OnUIThreadAsync();

            }
            else
            {
                TryClose(true);
            }
        }

        public void KeyHandle(Key key)
        {
            if (key == Key.F2 && SelectedCustomer != null)
            {
                Done();
            }
        }

        public void ExecuteSearch(Key key)
        {
            if (key == Key.Enter)
            {
                Done();
            }
        }

        public void ExecuteDoneSelectedCustomer(Key key)
        {
            if (key == Key.Enter)
            {
                Done();
            }
        }

        public void Search()
        {
            CustomerList = IoC.Get<ICustomerService>().GetCustomersForSelector(Filter, _seasonId);
            var selected = CustomerId.HasValue ? CustomerList.FirstOrDefault(t => t.Id == CustomerId.Value) : CustomerList.FirstOrDefault();

            if (selected != null)
            {
                SelectedCustomer = selected;
            }
        }

        public void RefreshList()
        {
            CustomerList = IoC.Get<ICustomerService>().GetCustomersForSelector(Filter, _seasonId);
            Filter = null;
        }
    }
}