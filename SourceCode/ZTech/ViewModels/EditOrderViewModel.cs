﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Caliburn.Micro;
using Contract.Events;
using Contract.Services;
using ZTech.ViewModels.Abstract;
using ZTech.ViewModels.ViewObject;
using Action = System.Action;

namespace ZTech.ViewModels
{
    public class EditOrderViewModel : BaseOrderViewModel
    {
        private string _originalPo;
        private bool _voidTicketsOnDelete = false;
        private bool _editOrderAllowed = true;
        private bool _editReopened = false;

        public EditOrderViewModel(IEventAggregator eventAggregator) : base(eventAggregator, "Edit Order Screen")
        {
            _log = LogManager.GetLog(typeof(EditOrderViewModel));
        }

        public int? OrderId { get; set; }

        protected override void InitializeOrder()
        {
            if (!OrderId.HasValue || OrderId.Value == 0)
            {
                throw new Exception("Order not found");
            }

            Order = IoC.Get<IOrderService>().GetOrder(_seasonId, OrderId.Value);
            Order.UpdateTime = DateTime.Now;
            Quantity = Order.Quantity.ToString();
            EditOrderAllowed = !Order.IsVoid;
                        
            InitializePo(Order.Customer.Id);
            InitializeMaterials(Order.Customer.Id);

            CustomerAddress = Order.Customer.Name + Environment.NewLine + Order.Customer.Address;

            var defaultPurchaseOrder = PurchaseOrderList.First(p => p.Id <= 0);

            if (string.IsNullOrEmpty(Order.POOverride))
            {
                SelectedPO = defaultPurchaseOrder;
            }
            else
            {
                var purchaseOrder = PurchaseOrderList.FirstOrDefault(p => p.PurchaseOrderNumber == Order.POOverride);
                SelectedPO = purchaseOrder ?? defaultPurchaseOrder;
            }
            _originalPo = SelectedPO.PurchaseOrderNumber;
        }

        // 2018-Jun-26, JGS - Reset "Active" status for an order
        //public void Reopen()
        //{
        //    // Get the current PO
        //    string poOverride = Order.POOverride.ToString();

        //    // Reset the Order active status
        //    Order.IsActive = true;
        //    Order.IsComplete = false;
        //    IoC.Get<IOrderService>().SaveOrder_Reopen(Order);
        //    _eventAggregator.PublishOnCurrentThread(new OrderModifiedArgs(Order.OrderId));

        //    // Reset the status of the Purchase order, if it is closed
        //    var purchaseOrder = PurchaseOrderList.FirstOrDefault(p => p.PurchaseOrderNumber == Order.POOverride);
        //    if (purchaseOrder != null)
        //    {
        //        purchaseOrder.IsComplete = false;
        //        purchaseOrder.IsActive = true;
        //        IoC.Get<IPurchaseOrderService>().SavePurchaseOrder(purchaseOrder, purchaseOrder.CustomerId);
        //        _eventAggregator.PublishOnCurrentThread(new PurchaseOrderModifiedArgs(purchaseOrder.Id));
        //    }

        //    // Reset the property change for the current order and purchase order
        //    NotifyOfPropertyChange(() => Order);
        //    if (purchaseOrder != null)
        //        NotifyOfPropertyChange(() => purchaseOrder);

        //    // Refresh the POList Table in this form
        //    var purchaseOrderList = IoC.Get<IPurchaseOrderService>().GetPurchaseOrderList(purchaseOrder.CustomerId, _seasonId).Select(p => new PurchaseOrderDataModel(p)).ToList();
        //    POList = purchaseOrderList.OrderByDescending(x => x.PurchaseOrderNumber).ToList();

        //    // Set the reopened status
        //    Reopened = true;
        //}

        // 2018-Jun-22, JGS - For Exiting orders discard changes and close the form
        public override void Cancel()
        {
            // Close the form
            TryClose(false);
        }

        // 2018-Jun-24, JGS - Determine whether to VOID tickets during a delete
        public bool VoidTicketsOnDelete
        {
            get { return _voidTicketsOnDelete; }
            set
            {
                if (value == _voidTicketsOnDelete) return;
                _voidTicketsOnDelete = value;
                NotifyOfPropertyChange(() => VoidTicketsOnDelete);
            }
        }

        // 2018-JUn-28, JGS - Track reopened status
        public bool Reopened
        {
            get { return _editReopened; }
            set
            {
                if (value == _editReopened) return;
                _editReopened = value;
                NotifyOfPropertyChange(() => Reopened);
            }
        }

        // 2018-Jun-24, JGS - Determine whether to VOID tickets during a delete
        public bool EditOrderAllowed
        {
            get { return _editOrderAllowed; }
            set
            {
                if (value == _editOrderAllowed) return;
                _editOrderAllowed = value;
                NotifyOfPropertyChange(() => EditOrderAllowed);
            }
        }



        // 2018-Jun-22, JGS - For Exiting orders set the statuses to Void
        public override void Delete()
        {
            try
            {
                // Void tickets
                if (VoidTicketsOnDelete)
                {
                    // Get a list of tickets asociated with this order and season
                    var TicketList = IoC.Get<ITicketService>().GetTicketsInfo(Order.SeasonId, Order.Bid.CustomerId, Order.OrderId, false);

                    // Set the status and void reason
                    foreach (var localTicket in TicketList)
                    {
                        // Don't overwrite an existing void status/reason/date
                        if (!localTicket.IsVoid)
                        {
                            localTicket.VoidReason = "Order Deleted";
                            IoC.Get<ITicketService>().VoidTicket(localTicket);
                        }
                    }
                }

                // Modify the order status fields and save
                Order.IsVoid = true;
                Order.IsActive = false;
                Order.IsComplete = false;
                IoC.Get<IOrderService>().SaveOrder(Order);
                _eventAggregator.PublishOnCurrentThread(new OrderModifiedArgs(Order.OrderId));

                // Close this form
                TryClose();
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "The new order record could not be deleted. Please contact your system administrator {0}",
                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };

                action.OnUIThreadAsync();
            }
        }

        public void KeyHandle(Key key)
        {
            if (key == Key.F2)
            {
                Done();
            }
        }

        public override void Done()
        {
            try
            {
                if (!Validate())
                    return;

                if (SelectedPO != null && !string.IsNullOrWhiteSpace(SelectedPO.PurchaseOrderNumber) &&
                    SelectedPO.Id < -1)
                {
                    var msgbox = MessageBox.Show("Are you sure you want to create new Purchase Order? Press 'No' to save the Order without changing PO #.", "Confirmation", MessageBoxButton.YesNo,
                        MessageBoxImage.Question);

                    if (msgbox == MessageBoxResult.Yes)
                    {
                        SelectedPO.EffectiveDate = SelectedPO.UpdateTime = SelectedPO.CreateTime = DateTime.Now;
                        SelectedPO.QuantityOfPO = Order.Quantity;
                        SelectedPO.CustomerId = Order.Customer.Id;
                        SelectedPO.IsActive = true;
                        SelectedPO.SeasonId = _seasonId;
                        SelectedPO.IsComplete = false;
                        SelectedPO.IsHeld = false;  // 2019-Mar-17, JGS - Add explicit status assignments for held
                        SelectedPO.Material = Order.Material;
                        SelectedPO.MaterialId = Order.Material.Id;
                        IoC.Get<IPurchaseOrderService>().SavePurchaseOrder(SelectedPO, Order.Customer.Id);
                    }
                    else
                    {
                        Order.POOverride = _originalPo;
                    }
                }

                // 2018-Jun-28, JGS - If the order was reopened then default to active is true
                // 2019-Mar-17, JGS - Add explicit status assignments for void and complete.
                if (Reopened)
                {
                    Order.IsActive = true;
                    Order.IsVoid = false;
                    Order.IsComplete = false;
                    Order.CompleteDate = null;
                }
                else
                    Order.IsActive = Order.CompleteDate == null ? true : false;

                IoC.Get<IOrderService>().SaveOrder(Order);
                _eventAggregator.PublishOnCurrentThread(new OrderModifiedArgs(Order.OrderId));

                TryClose(true);
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "Order saving failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };

                action.OnUIThreadAsync();
            }
        }

         public void AddPurchaseOrder()
        {
            var windowManager = IoC.Get<IWindowManager>();

            var poNameViewModel = IoC.Get<DailyPoNameViewModel>();
            var result = windowManager.ShowDialog(poNameViewModel);
            if (!result.HasValue || !result.Value)
                return;

            var po = new PurchaseOrderDataModel();
            po.Id = PurchaseOrderList.Min(p => p.Id) - 1;
            po.PurchaseOrderNumber = poNameViewModel.PoNumber;

            PurchaseOrderList.Add(po);
            SelectedPO = po;
        }

        public override PurchaseOrderDataModel SelectedPO
        {
            get { return _selectedPo; }
            set
            {
                if (Equals(value, _selectedPo)) return;
         
                if (value == null || value.Id == -1 || string.IsNullOrWhiteSpace(value.PurchaseOrderNumber))
                {
                    ChooseMaterial(Order.Material != null ? Order.Material.Id : Order.Bid.MaterialId);
                    Order.POOverride = null;
                    _selectedPo = value;
                }
                else if (Order.Material != null && value.MaterialId == Order.Material.Id)
                {
                    ChooseMaterial(value.MaterialId);
                    Order.POOverride = value.PurchaseOrderNumber;
                    _selectedPo = value;
                }
                else if (Order.Material == null)
                {
                    ChooseMaterial(value.MaterialId);
                    Order.POOverride = value.PurchaseOrderNumber;
                    _selectedPo = value;
                }
                else if (value.Id < -1)
                {
                    Order.POOverride = value.PurchaseOrderNumber;
                    _selectedPo = value;
                }
                else
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show("Invalid PO# or PO material does not match to selected material.", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning));
                }

                NotifyOfPropertyChange(() => SelectedPO);
            }
        }

        protected override void SpecificValidation(List<string> errors)
        {
            
        }
    }
}