using System;
using System.Collections.Generic;
using System.Linq;
using Caliburn.Micro;
using Contract.Domain;
using Contract.Events;
using Contract.Services;
using ZTech.Helpers;
using Action = System.Action;
using System.Windows;

namespace ZTech.ViewModels
{
    public class ScaleWidgetViewModel : Screen, IHandle<DefaultScaleChangedArgs>
    {
        private readonly IEventAggregator _eventAggregator;
        private readonly ILog _log;
        private bool _forceScaleSelect;
        private BindableCollection<IScaleInfo> _scaleList;
        private IScaleInfo _selectedScale;
        private readonly IScaleManager _scaleManager;
        private string _errors;
        public IReadOnlyList<IScaleInfo> scales;
        public List<int> availableComPorts;

        public ScaleWidgetViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            _log = LogManager.GetLog(typeof(ScaleWidgetViewModel));
            _scaleManager = IoC.Get<IScaleManager>();
        }

        protected override void OnInitialize()
        {
            base.OnInitialize();

            InitializeScale();
        }

        #region Scale

        private void InitializeScale()
        {
            Errors = null;
            // var configuration = IoC.Get<IConfiguration>();
            var configuration = IoC.Get<IConfiguration>();

            // Set manual mode, if specified in the configuration
            if (configuration.SelectedScaleName.Trim().ToUpper() == "MANUAL")
            {
                configuration.IsManualMode = true;
                return;
            }

            // Open the scale if manual mode is NOT selection
            if (!configuration.IsManualMode)
            {
                // IReadOnlyList<IScaleInfo> scales = IoC.Get<IScaleService>().GetAvailableScales(configuration.SelectedSite.Name);
                scales = IoC.Get<IScaleService>().GetAvailableScales(configuration.SelectedSite.Name);
                Scales = new BindableCollection<IScaleInfo>(scales);


                // List<int> availableComPorts = _scaleManager.GetAvailableComPorts().ToList();
                availableComPorts = _scaleManager.GetAvailableComPorts().ToList();

                //-- If the computer has any com ports the scales for this site use
                if (availableComPorts.Any())
                {
                    var scalesAndComsMatch = Scales.Select(s => s.SerialComPort).Where(s => availableComPorts.Contains(s));
                    IScaleInfo selectedScale =
                            Scales.FirstOrDefault(s => s.ScaleName.Equals(configuration.SelectedScaleName,
                            StringComparison.InvariantCultureIgnoreCase));

                    // Setup the default scale
                    if (selectedScale != null)
                    {
                        SelectedScale = selectedScale;
                    }

                    if (SelectedScale == null)
                    {
                        _log.Warn($"OrderTicketViewModel:StartScaleInteraction - Selected Scale is null: '{configuration.SelectedScaleName}'");
                        configuration.IsManualMode = true;

                        // 23-Nov-2018: Not sure if this code really helps
                        Action action = () =>
                        {
                            MessageBox.Show($"Computer specific default scale '{configuration.SelectedScaleName}' is not set to be used{Environment.NewLine}{Environment.NewLine}The scale reading has been set to Manual.", "Scale Indicator Reading", MessageBoxButton.OK, MessageBoxImage.Warning);
                        };
                        action.OnUIThreadAsync();

                        // OR //

                        //MessageBox.Show($"Computer specific default scale '{configuration.SelectedScaleName}' is not set to be used{Environment.NewLine}{Environment.NewLine}The scale reading has been set to Manual.", "Scale Indicator Reading", MessageBoxButton.OK, MessageBoxImage.Warning);

                        // 23-Nov-2018: Not sure if this code really helps
                        return;
                    }

                    if (!SelectedScale.IsScaleEnabled)
                    {
                        _log.Warn($"OrderTicketViewModel:StartScaleInteraction - Selected Scale is not enabled: '{SelectedScale.ScaleName}' with COM port '{SelectedScale.SerialComPort}'");
                        configuration.IsManualMode = true;

                        // 23-Nov-2018: Not sure is this code really helps, Try a random pause followed by a retry to connect to the scale
                        Action action = () =>
                        {
                            MessageBox.Show($"Scale '{SelectedScale.ScaleName}' is not enabled.{Environment.NewLine}{Environment.NewLine}The scale reading has been set to Manual.", "Scale Indicator Reading", MessageBoxButton.OK, MessageBoxImage.Warning);
                        };
                        action.OnUIThreadAsync();

                        return;
                    }

                    if (!_scaleManager.IsScaleComPortAccessibleForReading(SelectedScale.SerialComPort))
                    {
                        // Wait up to 0.5 seconds and try again
                        Random rnd = new Random();
                        System.Threading.Thread.Sleep(rnd.Next(1, 500) + 500);

                        // Null controls
                        configuration = null;
                        scales = null;
                        Scales = null;
                        selectedScale = null;
                        availableComPorts = null;
                        scalesAndComsMatch = null;

                        // Reset 
                        configuration = IoC.Get<IConfiguration>(); // Reload the configuration
                        scales = IoC.Get<IScaleService>().GetAvailableScales(configuration.SelectedSite.Name);  // Refresh the available scales
                        Scales = new BindableCollection<IScaleInfo>(scales);  // Create an new binding to the scales
                        availableComPorts = _scaleManager.GetAvailableComPorts().ToList();
                        scalesAndComsMatch = Scales.Select(s => s.SerialComPort).Where(s => availableComPorts.Contains(s));
                        selectedScale = Scales.FirstOrDefault(s => s.ScaleName.Equals(configuration.SelectedScaleName, StringComparison.InvariantCultureIgnoreCase));

                        // Setup the default scale
                        if (selectedScale != null)
                        {
                            SelectedScale = selectedScale;
                        }

                        if (SelectedScale == null)
                        {
                            _log.Warn($"OrderTicketViewModel:StartScaleInteraction - Selected Scale is null: '{configuration.SelectedScaleName}'");
                            configuration.IsManualMode = true;

                            // 23-Nov-2018: Not sure if this code really helps
                            Action action = () =>
                            {
                                MessageBox.Show($"Computer specific default scale '{configuration.SelectedScaleName}' is not set to be used{Environment.NewLine}{Environment.NewLine}The scale reading has been set to Manual.", "Scale Indicator Reading", MessageBoxButton.OK, MessageBoxImage.Warning);
                            };
                            action.OnUIThreadAsync();

                            // OR //

                            //MessageBox.Show($"Computer specific default scale '{configuration.SelectedScaleName}' is not set to be used{Environment.NewLine}{Environment.NewLine}The scale reading has been set to Manual.", "Scale Indicator Reading", MessageBoxButton.OK, MessageBoxImage.Warning);

                            // 23-Nov-2018: Not sure if this code really helps
                            return;
                        }

                        if (!SelectedScale.IsScaleEnabled)
                        {
                            _log.Warn($"OrderTicketViewModel:StartScaleInteraction - Selected Scale is not enabled: '{SelectedScale.ScaleName}' with COM port '{SelectedScale.SerialComPort}'");
                            configuration.IsManualMode = true;

                            // 23-Nov-2018: Not sure is this code really helps, Try a random pause followed by a retry to connect to the scale
                            Action action = () =>
                            {
                                MessageBox.Show($"Scale '{SelectedScale.ScaleName}' is not enabled.{Environment.NewLine}{Environment.NewLine}The scale reading has been set to Manual.", "Scale Indicator Reading", MessageBoxButton.OK, MessageBoxImage.Warning);
                            };
                            action.OnUIThreadAsync();

                            return;
                        }


                        // If there is still an error then process it
                        if (!_scaleManager.IsScaleComPortAccessibleForReading(SelectedScale.SerialComPort))
                        {
                            _log.Warn($"OrderTicketViewModel:StartScaleInteraction - Selected Scale is not available to be read from: '{SelectedScale.ScaleName}' with COM port '{SelectedScale.SerialComPort}'");
                            configuration.IsManualMode = true;

                            // 23-Nov-2018: Not sure if this code really helps. Try a random pause followed by a retry to connect to the scale
                            Action action = () =>
                            {
                                MessageBox.Show($"Scale '{SelectedScale.ScaleName}' is being blocked by another program (TBD){Environment.NewLine}{Environment.NewLine}The scale reading has been set to Manual.", "Scale Indicator Reading", MessageBoxButton.OK, MessageBoxImage.Warning);
                            };
                            action.OnUIThreadAsync();

                            // Break out of this loop
                            return;
                        }
                        else
                        {
                            // It didn't register an error so proceed
                            _log.Warn($"OrderTicketViewModel:StartScaleInteraction - Selected Scale is now available to be read from: '{SelectedScale.ScaleName}' with COM port '{SelectedScale.SerialComPort}' on 2nd try.");
                            configuration.IsManualMode = false;
                        }
                    }
                }
                else
                {
                    configuration.IsManualMode = true;
                }
            }
        }


        public BindableCollection<IScaleInfo> Scales
        {
            get { return _scaleList; }
            set
            {
                _scaleList = value;
                NotifyOfPropertyChange(() => Scales);
            }
        }

        public IScaleInfo SelectedScale
        {
            get { return _selectedScale; }
            set
            {
                _selectedScale = value;
                var configuration = IoC.Get<IConfiguration>();
                if (!_selectedScale.ScaleName.Equals(configuration.SelectedScaleName, StringComparison.InvariantCultureIgnoreCase))
                {
                    configuration.SelectedScaleName = _selectedScale.ScaleName;
                }

                NotifyOfPropertyChange(() => SelectedScale);
                Errors = null;
                _eventAggregator.PublishOnCurrentThread(new ScaleChangedArgs(value));
            }
        }

        public bool ForceScaleSelect
        {
            get { return _forceScaleSelect; }
            set
            {
                _forceScaleSelect = value;

                NotifyOfPropertyChange(() => ForceScaleSelect);
            }
        }

        public string Errors
        {
            get{  return _errors; }
            set
            {
                if (value == _errors) return;
                _errors = value;
                NotifyOfPropertyChange(() => Errors);
            }
        }

        #endregion

        #region Events

        protected override void OnActivate()
        {
            _eventAggregator.Subscribe(this);
            base.OnActivate();
        }

        protected override void OnDeactivate(bool close)
        {
            _eventAggregator.Unsubscribe(this);
            base.OnDeactivate(close);
        }

        #endregion

        public void Handle(DefaultScaleChangedArgs args)
        {
            if (SelectedScale == null)
            {
                IScaleInfo selectedScale = Scales.FirstOrDefault(s => s.ScaleName.Equals(IoC.Get<IConfiguration>().SelectedScaleName, StringComparison.InvariantCultureIgnoreCase));

                if (selectedScale != null)
                    SelectedScale = selectedScale;
            }
        }
    }
}