﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows;
using Caliburn.Micro;
using Contract.Domain;
using Contract.Events;
using Action = System.Action;

namespace ZTech.ViewModels
{
    public interface IContactWidgetViewModel : IScreen
    {
        void SetData(IContactData contact);
        IContactData GetData();
        ICollection<ValidationResult> Validate();
    }

    public class ContactWidgetViewModel : Screen, IContactWidgetViewModel, IHandle<ContactPhoneModifiedArgs>
    {
        private readonly IEventAggregator _eventAggregator;
        private readonly ILog _log;
        private readonly int _seasonId;
        private AddressModel _address;
        private IContactData _contact;
        private ContactModel _contactData;
        private ObservableCollection<PhoneModel> _otherPhones;
        private PhoneModel _primaryPhone;
        private PhoneModel _selectedPhone;

        public ContactWidgetViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            _log = LogManager.GetLog(typeof (ContactWidgetViewModel));
            _seasonId = (int) Application.Current.Properties["SeasonId"];
        }

        public AddressModel Address
        {
            get { return _address; }
            set
            {
                if (Equals(value, _address)) return;
                _address = value;
                NotifyOfPropertyChange(() => Address);
            }
        }

        public List<PhoneModel> Phones { get; set; }

        public PhoneModel SelectedPhone
        {
            get { return _selectedPhone; }
            set
            {
                if (Equals(value, _selectedPhone)) return;
                _selectedPhone = value;
                NotifyOfPropertyChange(() => SelectedPhone);
            }
        }

        public PhoneModel PrimaryPhone
        {
            get { return _primaryPhone; }
            set
            {
                if (Equals(value, _primaryPhone)) return;
                _primaryPhone = value;
                NotifyOfPropertyChange(() => PrimaryPhone);
            }
        }

        public ObservableCollection<PhoneModel> OtherPhones
        {
            get { return _otherPhones; }
            set
            {
                if (Equals(value, _otherPhones)) return;
                _otherPhones = value;
                NotifyOfPropertyChange(() => OtherPhones);
            }
        }

        public ContactModel ContactData
        {
            get { return _contactData; }
            set
            {
                if (Equals(value, _contactData)) return;
                _contactData = value;
                NotifyOfPropertyChange(() => ContactData);
            }
        }

        public void SetData(IContactData contact)
        {
            _contact = contact;
            Init();
        }

        public IContactData GetData()
        {
            FillContact();
            FillAddress();
            FillPhones();
            return _contact;
        }

        public ICollection<ValidationResult> Validate()
        {
            ICollection<ValidationResult> validationResults = new Collection<ValidationResult>();
            Validator.TryValidateObject(ContactData, new ValidationContext(ContactData), validationResults);
            Validator.TryValidateObject(Address, new ValidationContext(Address), validationResults);
            Validator.TryValidateObject(OtherPhones, new ValidationContext(OtherPhones), validationResults);
            return validationResults;
        }

        private void Init()
        {
            if (_contact == null) return;

            ContactData = new ContactModel(_contact);
            Address = new AddressModel(_contact.Address);
            InitializePhone();
        }

        private void InitializePhone()
        {
            Phones = _contact.Phones.Select(p => new PhoneModel(p)).ToList();
            PhoneModel phone =
                PrimaryPhone =
                    Phones.OrderByDescending(p => p.PhoneNumberId)
                        .FirstOrDefault(p => p.IsPrimary && p.IsActive);

            if (phone == null)
                PrimaryPhone = new PhoneModel();

            OtherPhones =
                new ObservableCollection<PhoneModel>(Phones.Where(p => !p.IsPrimary && p.IsActive).ToList());

            SelectedPhone = null;
        }

        public void ShowMoreNumbers()
        {
        }

        private void FillPhones()
        {
            IPhoneData phone = _contact.Phones.OrderByDescending(p => p.PhoneNumberId)
                .FirstOrDefault(p => p.IsPrimary && p.IsActive);
            if (phone != null)
            {
                phone.UpdateTime = DateTime.Now;
                if (!string.IsNullOrWhiteSpace(PrimaryPhone.Number))
                {
                    phone.Number = PrimaryPhone.Number;
                    if (string.IsNullOrWhiteSpace(phone.NumberType))
                        phone.NumberType = "main";
                }
                else
                    phone.IsActive = false;
            }
            else if (!string.IsNullOrWhiteSpace(PrimaryPhone.Number))
            {
                PrimaryPhone.IsActive = true;
                PrimaryPhone.CreateTime = DateTime.Now;
                PrimaryPhone.UpdateTime = DateTime.Now;
                PrimaryPhone.IsPrimary = true;
                PrimaryPhone.SeasonId = _seasonId;
                PrimaryPhone.NumberType = "main";

                _contact.Phones.Add(PrimaryPhone);
            }

            List<PhoneModel> initPhones = Phones.Where(p => !p.IsPrimary && p.IsActive).ToList();

            IEnumerable<PhoneModel> toAdd = OtherPhones.Where(p => !p.PhoneNumberId.HasValue);
            IEnumerable<int> toUpdate =
                OtherPhones.Where(p => p.PhoneNumberId.HasValue).Select(p => p.PhoneNumberId.Value);
            IEnumerable<int?> toDelete =
                initPhones.Where(p => p.PhoneNumberId.HasValue && !toUpdate.Contains(p.PhoneNumberId.Value))
                    .Select(p => p.PhoneNumberId);
            foreach (var id in toDelete)
            {
                IPhoneData phoneData = _contact.Phones.First(p => p.PhoneNumberId == id);
                phoneData.IsActive = false;
                phoneData.UpdateTime = DateTime.Now;
            }
            foreach (PhoneModel item in toAdd)
            {
                item.UpdateTime = DateTime.Now;
                item.CreateTime = DateTime.Now;
                item.IsActive = true;
                item.IsPrimary = false;
                item.SeasonId = _seasonId;

                _contact.Phones.Add(item);
            }
            foreach (int id in toUpdate)
            {
                IPhoneData phoneData = _contact.Phones.First(p => p.PhoneNumberId == id);
                phoneData.UpdateTime = DateTime.Now;
                phoneData.Number = OtherPhones.First(p => p.PhoneNumberId == id).Number;
                phoneData.NumberType = OtherPhones.First(p => p.PhoneNumberId == id).NumberType;
            }
        }

        private void FillAddress()
        {
            _contact.Address.State = Address.State;
            _contact.Address.City = Address.City;
            _contact.Address.Zip = Address.Zip;
            _contact.Address.UpdateTime = DateTime.Now;
            _contact.Address.AddressLineOne = Address.AddressLineOne;
            _contact.Address.AddressLineTwo = Address.AddressLineTwo;

            _contact.Address.SeasonId = _seasonId;
        }

        private void FillContact()
        {
            _contact.Name = ContactData.Name;
            _contact.Organization = ContactData.Organization;
            _contact.UpdateTime = DateTime.Now;
            _contact.SeasonId = _seasonId;
            _contact.ContactComment = ContactData.ContactComment;
        }

        #region Events

        public void Handle(ContactPhoneModifiedArgs args)
        {
            if (args != null)
            {
                var phoneModel = (PhoneModel) args.Phone;
                OtherPhones.Remove(phoneModel);
                OtherPhones.Add(phoneModel);
                SelectedPhone = phoneModel;
            }
        }

        protected override void OnActivate()
        {
            _eventAggregator.Subscribe(this);
            base.OnActivate();
        }

        protected override void OnDeactivate(bool close)
        {
            _eventAggregator.Unsubscribe(this);
            base.OnDeactivate(close);
        }

        #endregion

        #region PhonePopup

        public void NewPhone()
        {
            try
            {
                var newPhoneViewModel = IoC.Get<ContactPhoneViewModel>();
                newPhoneViewModel.Phone = new PhoneModel
                {
                    IsActive = true,
                    UpdateTime = DateTime.Now,
                    CreateTime = DateTime.Now,
                    SeasonId = _seasonId
                };
                
                var windowManager = IoC.Get<IWindowManager>();
                windowManager.ShowDialog(newPhoneViewModel);
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "New Phone failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };

                action.OnUIThreadAsync();
            }
        }

        public void EditPhone()
        {
            try
            {
                if (SelectedPhone == null) return;
                var newPhoneViewModel = IoC.Get<ContactPhoneViewModel>();
                newPhoneViewModel.Phone = SelectedPhone;

                var windowManager = IoC.Get<IWindowManager>();
                windowManager.ShowDialog(newPhoneViewModel);
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "New Phone failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };

                action.OnUIThreadAsync();
            }
        }

        public void DeletePhone()
        {
            try
            {
                if (SelectedPhone == null) return;
                Action action = () =>
                {
                    var message = string.Format("Are you sure you want to delete {0} Phone Number?", SelectedPhone.Number);
                    MessageBoxResult result = MessageBox.Show(message, "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (result == MessageBoxResult.Yes)
                        DeletePhoneAction();
                };
                action.OnUIThread();
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "New Phone failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };

                action.OnUIThreadAsync();
            }
        }

        private void DeletePhoneAction()
        {
            try
            {
                if (SelectedPhone == null) return;

                OtherPhones.Remove(SelectedPhone);

                SelectedPhone = null;
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "Phone Number deleting failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };

                action.OnUIThreadAsync();
            }
        }

        #endregion

        #region DTO

        public class AddressModel : PropertyChangedBase, IAddressData
        {
            private string _addressLineOne;
            private string _addressLineTwo;
            private string _city;
            private string _comment;
            private string _state;
            private string _zip;
            //private DateTime _createTime;
            //private DateTime? _updateTime;

            public AddressModel()
            {
            }

            public AddressModel(IAddressData address)
            {
                AddressLineOne = address.AddressLineOne;
                AddressLineTwo = address.AddressLineTwo;
                City = address.City;
                Comment = address.Comment;
                State = address.State;
                Zip = address.Zip;
                AddressId = address.AddressId;
                IsActive = address.IsActive;
                CreateTime = address.CreateTime;
                UpdateTime = address.UpdateTime;
                SeasonId = address.SeasonId;
            }

            public string AddressLineOne
            {
                get { return _addressLineOne; }
                set
                {
                    if (value == _addressLineOne) return;
                    _addressLineOne = value;
                    NotifyOfPropertyChange(() => AddressLineOne);
                }
            }

            public string AddressLineTwo
            {
                get { return _addressLineTwo; }
                set
                {
                    if (value == _addressLineTwo) return;
                    _addressLineTwo = value;
                    NotifyOfPropertyChange(() => AddressLineTwo);
                }
            }

            public string City
            {
                get { return _city; }
                set
                {
                    if (value == _city) return;
                    _city = value;
                    NotifyOfPropertyChange(() => City);
                }
            }

            public string Comment
            {
                get { return _comment; }
                set
                {
                    if (value == _comment) return;
                    _comment = value;
                    NotifyOfPropertyChange(() => Comment);
                }
            }

            public string State
            {
                get { return _state; }
                set
                {
                    if (value == _state) return;
                    _state = value;
                    NotifyOfPropertyChange(() => State);
                }
            }

            public int? AddressId { get; set; }

            public string Zip
            {
                get { return _zip; }
                set
                {
                    if (value == _zip) return;
                    _zip = value;
                    NotifyOfPropertyChange(() => Zip);
                }
            }

            public DateTime CreateTime { get; set; }
            public DateTime? UpdateTime { get; set; }
                        public bool IsActive { get; set; }
            public int SeasonId { get; set; }
        }

        public class ContactModel : PropertyChangedBase, IContactData
        {
            private string _contactComment;
            private string _name;
            private string _organization;

            public ContactModel()
            {
            }

            public ContactModel(IContactData contact)
            {
                Comment = contact.Comment;
                ContactComment = contact.ContactComment;
                Name = contact.Name;
                if (!string.IsNullOrWhiteSpace(contact.Organization))
                    Organization = contact.Organization;
                ContactId = contact.ContactId;
                IsActive = contact.IsActive;
                CreateTime = contact.CreateTime;
                UpdateTime = contact.UpdateTime;
                SeasonId = contact.SeasonId;
            }

            public int? ContactId { get; private set; }

            [Required(AllowEmptyStrings = false, ErrorMessage = "The Contact Company Name cannot be left blank.")]
            public string Organization
            {
                get { return _organization; }
                set
                {
                    if (value == _organization) return;
                    _organization = value;
                    NotifyOfPropertyChange(() => Organization);
                }
            }

            public string Name
            {
                get { return _name; }
                set
                {
                    if (value == _name) return;
                    _name = value;
                    NotifyOfPropertyChange(() => Name);
                }
            }

            public string ContactComment
            {
                get { return _contactComment; }
                set
                {
                    if (value == _contactComment) return;
                    _contactComment = value;
                    NotifyOfPropertyChange(() => ContactComment);
                }
            }

            public string Comment { get; set; }
            public string PhoneNumber { get; set; }
            public IAddressData Address { get; set; }
            public List<IPhoneData> Phones { get; set; }
            public DateTime CreateTime { get; set; }
            public DateTime? UpdateTime { get; set; }
            public bool IsActive { get; set; }
            public bool IsPrimary { get; set; }
            public int SeasonId { get; set; }
        }

        public class PhoneModel : PropertyChangedBase, IPhoneData
        {
            private bool _isPrimary;
            private string _number;
            private string _numberType;

            public PhoneModel()
            {
            }

            public PhoneModel(IPhoneData phone)
            {
                Comment = phone.Comment;
                IsActive = phone.IsActive;
                NumberType = phone.NumberType;
                Number = phone.Number;
                PhoneNumberId = phone.PhoneNumberId;
                IsPrimary = phone.IsPrimary;
                CreateTime = phone.CreateTime;
                UpdateTime = phone.UpdateTime;
                //todo add seasonid
            }

            public string Comment { get; set; }
            public bool IsActive { get; set; }


            public string NumberType
            {
                get { return _numberType; }
                set
                {
                    if (value == _numberType) return;
                    _numberType = value;
                    NotifyOfPropertyChange(() => NumberType);
                }
            }

            [Required(AllowEmptyStrings = false, ErrorMessage = "The Phone Number cannot be empty.")]
            public string Number
            {
                get { return _number; }
                set
                {
                    if (value == _number) return;
                    _number = value;
                    NotifyOfPropertyChange(() => Number);
                }
            }

            public int? PhoneNumberId { get; set; }

            public bool IsPrimary
            {
                get { return _isPrimary; }
                set
                {
                    if (value.Equals(_isPrimary)) return;
                    _isPrimary = value;
                    NotifyOfPropertyChange(() => IsPrimary);
                }
            }

            public DateTime CreateTime { get; set; }
            public DateTime? UpdateTime { get; set; }
            public int SeasonId { get; set; }
        }

        #endregion
    }
}