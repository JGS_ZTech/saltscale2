﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Caliburn.Micro;
using Contract.Domain;
using Contract.Services;
using ZTech.Controls;
using ZTech.ViewModels.Abstract;
using Action = System.Action;

namespace ZTech.ViewModels
{
    public class PhoneBookViewModel : BaseViewModel
    {
        private readonly IEventAggregator _eventAggregator;
        private readonly ILog _log;
        private readonly int _seasonId;
        private string _errors;
        private IReadOnlyList<IContactInfo> _contactList;
        private string _filter;
        private IContactInfo _selectedContact;
        private IContactWidgetViewModel _contactWidget;
        private bool _hideCustomers;
        private bool _hideTruckOwners;
        private bool _hideOthers;
        private bool _isBusy;

        public PhoneBookViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            _log = LogManager.GetLog(typeof(PhoneBookViewModel));
            _seasonId = (int) Application.Current.Properties["SeasonId"];
            DisplayName = "Phone Book";
        }

        public string Errors
        {
            get { return _errors; }
            set
            {
                if (value == _errors) return;
                _errors = value;
                NotifyOfPropertyChange(() => Errors);
            }
        }
        public IReadOnlyList<IContactInfo> ContactList
        {
            get { return _contactList; }
            set
            {
                if (Equals(value, _contactList)) return;
                _contactList = value;
                NotifyOfPropertyChange(() => ContactList);
            }
        }

        public string Filter
        {
            get { return _filter; }
            set
            {
                if (value == _filter) return;
                _filter = value;
                NotifyOfPropertyChange(() => Filter);
            }
        }

        public IContactInfo SelectedContact
        {
            get { return _selectedContact; }
            set
            {
                if (Equals(value, _selectedContact)) return;
                _selectedContact = value;
                NotifyOfPropertyChange(() => SelectedContact);
                InitializeContact();
            }
        }

        public bool HideCustomers
        {
            get { return _hideCustomers; }
            set
            {
                if (value.Equals(_hideCustomers)) return;
                _hideCustomers = value;
                NotifyOfPropertyChange(() => HideCustomers);
                RefreshFilters();
            }
        }

        public bool HideTruckOwners
        {
            get { return _hideTruckOwners; }
            set
            {
                if (value.Equals(_hideTruckOwners)) return;
                _hideTruckOwners = value;
                NotifyOfPropertyChange(() => HideTruckOwners);
                RefreshFilters();
            }
        }

        public bool HideOthers
        {
            get { return _hideOthers; }
            set
            {
                if (value.Equals(_hideOthers)) return;
                _hideOthers = value;
                NotifyOfPropertyChange(() => HideOthers);

                RefreshFilters();
            }
        }

        public IContactWidgetViewModel ContactWidget
        {
            get { return _contactWidget; }
            set
            {
                if (Equals(value, _contactWidget)) return;
                _contactWidget = value;
                NotifyOfPropertyChange(() => ContactWidget);
            }
        }
        

        #region Events

        protected override void OnActivate()
        {
            _eventAggregator.Subscribe(this);
            base.OnActivate();
        }

        protected override void OnDeactivate(bool close)
        {
            _eventAggregator.Unsubscribe(this);
            base.OnDeactivate(close);
        }

        #endregion

        private void InitializeContact()
        {
            var contactData = SelectedContact != null
                ? IoC.Get<IContactService>().GetContactData(SelectedContact.ContactId, _seasonId)
                : IoC.Get<IContactService>().GetNewContact(_seasonId);

            if (contactData != null && ContactWidget != null)
            {
                ContactWidget.SetData(contactData);
            }                
        }

        private void InitializeContactWidget()
        {
            if (ContactWidget == null)
            {
                ContactWidget = IoC.Get<IContactWidgetViewModel>();
                ContactWidget.ActivateWith(this);
            }
        }

        protected override void OnInitialize()
        {
            base.OnInitialize();
            IsBusy = true;
            InitializeContactWidget();
            var task = new Task(InitTask);
            task.Start();
        }

        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                if (value.Equals(_isBusy)) return;
                _isBusy = value;
                NotifyOfPropertyChange(() => IsBusy);
            }
        }

        private void InitTask()
        {
            try
            {
                HideCustomers = true;
                HideTruckOwners = true;
                ContactList = IoC.Get<IContactService>().GetFilteredContacts(Filter, _seasonId, true, true, false);
                var contact = ContactList.FirstOrDefault();
                SelectedContact = contact;
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(
                            string.Format(
                                "Initialization failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                                ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));

                    TryClose(false);
                };

                action.OnUIThreadAsync();
            }
            finally
            {
                IsBusy = false;
            }
        }

        public void DeleteContact()
        {
            if(SelectedContact == null)
                return;

            var canDelete = IoC.Get<IContactService>().CanDeleteContact(SelectedContact.ContactId, _seasonId);
            if (!canDelete)
            {
                Action action = () =>
                {
                    var messageBoxResult =
                        (MessageBox.Show(string.Format("You cannot delete primary contact '{0}'.", SelectedContact.Organization),
                        "Warning", MessageBoxButton.OK, MessageBoxImage.Warning));
                };

                action.OnUIThread();
            }
            else
            {
                Action action = () =>
                {
                    var messageBoxResult =
                        (MessageBox.Show("Are you sure you want to delete this Contact?",
                        "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question));

                    if (messageBoxResult == MessageBoxResult.Yes)
                        DeleteAction();
                };

                action.OnUIThread();
            }
            
        }

        private void DeleteAction()
        {
            try
            {
                Errors = null;
                if (SelectedContact == null) return;
                
                IoC.Get<IContactService>().DeleteContact(SelectedContact.ContactId, _seasonId);

                Search();
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                string message = string.Format(
                    "Contact failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                    ex.Message);
                ShowMessage("Error", message, NotificationType.Error);
            }
        }

        public void NewContact()
        {
            Errors = null;

            if (ContactWidget != null)
                ContactWidget.SetData(IoC.Get<IContactService>().GetNewContact(_seasonId));
        }

        public void RefreshFilters()
        {
            Search();
        }

        public void Search()
        {
            IsBusy = true;
            var task = new Task(SearchTask);
            task.Start();
        }

        private void SearchTask()
        {
            try
            {
                Errors = null;
                ContactList = IoC.Get<IContactService>()
                    .GetFilteredContacts(Filter, _seasonId, HideCustomers, HideTruckOwners, HideOthers);

                var contact = ContactList.FirstOrDefault();
                SelectedContact = contact;
            }
            catch (Exception ex)
            {
                _log.Error(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public void ExecuteSearch(Key key)
        {
            if (key == Key.Enter)
            {
                Search();
            }
        }

        public void KeyHandle(Key key)
        {
            if (key == Key.F2)
            {
                Done();
            }
        }

        public void Cancel()
        {
            TryClose(false);
        }
        public void Done()
        {
            try
            {
                if (!Validate())
                    return;

                var contactData = ContactWidget.GetData();
                IoC.Get<IContactService>().SaveContact(contactData, _seasonId);

                TryClose(true);
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "Phone saving failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };

                action.OnUIThreadAsync();
            }
        }

        private bool Validate()
        {
            Errors = null;

            var validationResults = ContactWidget.Validate();

            if (validationResults.Any())
            {
                Errors = string.Format(
                    "Validation Failed.{0}{1}", Environment.NewLine,
                    string.Join("\n", validationResults.Select(v => v.ErrorMessage)));

                ShowMessage("Validation Errors", Errors, NotificationType.Warning);

                return false;
            }

            return true;
        }
    }
}