﻿using System;
using Caliburn.Micro;
using Contract.Domain;

namespace ZTech.ViewModels.ViewObject
{
    public class TicketExportModel : PropertyChangedBase
    {
        private bool _cashTicket;
        private string _company;
        private int? _customerId;
        private string _customerShortName;
        private DateTime? _dateExported;
        private bool? _exported;
        private int _accountingCustomerId;
        private string _accountingCustomerCode;
        private bool _isManual;
        private string _location;
        private string _material;
        private decimal? _materialCost;
        private string _purchaseOrder;
        private float _quantity;
        private decimal? _quote;
        private decimal? _salesTax;
        private DateTime _shipDate;
        private bool _taxExempt;
        private decimal? _taxRate;
        private int _ticketNumber;
        private string _ticketNumberWithSeasonPrefix;
        private string _truckId;
        private decimal? _truckingCost;
        private DateTime? _voidDate;
        private string _voidReason;
        private bool _voided;
        private bool _isAccountingCodeNeeded;
        private bool _isDataGridAccountingCustomerCodeReadOnly;
        public TicketExportModel()
        {
        }

        public TicketExportModel(ITicketExportData data, string location)
        {
            CashTicket = data.CashTicket;
            Company = data.Company;
            CustomerId = data.CustomerId;
            CustomerShortName = data.CustomerShortName;
            DateExported = data.DateExported;
            Exported = data.Exported;
            AccountingCustomerId = data.AccountingCustomerId ?? 0;
            AccountingCustomerCode = data.AccountingCustomerCode;
            IsManual = data.IsManual;
            Location = (location == "Portsmouth") ? "PORTS" : location;
            Material = string.IsNullOrWhiteSpace(data.Material) ? data.CashMaterial : data.Material;
            MaterialCost = data.MaterialCost;
            PurchaseOrder = data.PurchaseOrder;
            Quantity = data.Quantity;
            Quote = data.Quote;
            SalesTax = data.SalesTax;
            ShipDate = data.ShipDate;
            TaxExempt = data.TaxExempt;
            TaxRate = data.TaxRate;
            TicketNumber = data.TicketNumber;
            TicketNumberWithSeasonPrefix = data.SeasonPrefix + "-" + TicketNumber.ToString("D6");
            TruckId = string.IsNullOrWhiteSpace(data.TruckId) ? data.TruckDescription : data.TruckId;
            TruckingCost = data.TruckingCost;
            VoidDate = data.VoidDate;
            VoidReason = data.VoidReason;
            Voided = data.Voided;
        }

        public string[] GetRowDataForExportFile()
        {
            return new[]
            {
                TicketNumberWithSeasonPrefix,
                PurchaseOrder ?? string.Empty,
                ShipDate.ToString("MM/dd/yyyy hh:mm"),
                Company ?? string.Empty,
                CustomerShortName ?? string.Empty,
                AccountingCustomerCode,
                Quantity.ToString("f2"),
                IsManual.ToString(),
                Material ?? string.Empty,
                Location ?? string.Empty,
                TruckId ?? string.Empty,
                TruckingCost.HasValue ? TruckingCost.Value.ToString("f2") : string.Empty,
                CashTicket.ToString(),
                MaterialCost.HasValue ? MaterialCost.Value.ToString("f2") : string.Empty,
                SalesTax.HasValue ? SalesTax.Value.ToString("f2") : string.Empty,
                Voided.ToString(),
                VoidReason ?? string.Empty,
                Exported.HasValue ? Exported.Value.ToString() : string.Empty,
            };
        }

        #region Notify Property Change
        public decimal? MaterialCost
        {
            get { return _materialCost; }
            set
            {
                if (value == _materialCost) return;
                _materialCost = value;
                NotifyOfPropertyChange(() => MaterialCost);
            }
        }

        public decimal? SalesTax
        {
            get { return _salesTax; }
            set
            {
                if (value == _salesTax) return;
                _salesTax = value;
                NotifyOfPropertyChange(() => SalesTax);
            }
        }

        public decimal? Quote
        {
            get { return _quote; }
            set
            {
                if (value == _quote) return;
                _quote = value;
                NotifyOfPropertyChange(() => Quote);
            }
        }

        public bool TaxExempt
        {
            get { return _taxExempt; }
            set
            {
                if (value.Equals(_taxExempt)) return;
                _taxExempt = value;
                NotifyOfPropertyChange(() => TaxExempt);
            }
        }

        public decimal? TaxRate
        {
            get { return _taxRate; }
            set
            {
                if (value == _taxRate) return;
                _taxRate = value;
                NotifyOfPropertyChange(() => TaxRate);
            }
        }

        public bool CashTicket
        {
            get { return _cashTicket; }
            set
            {
                if (value.Equals(_cashTicket)) return;
                _cashTicket = value;
                NotifyOfPropertyChange(() => CashTicket);
            }
        }

        public decimal? TruckingCost
        {
            get { return _truckingCost; }
            set
            {
                if (value == _truckingCost) return;
                _truckingCost = value;
                NotifyOfPropertyChange(() => TruckingCost);
            }
        }

        public string TruckId
        {
            get { return _truckId; }
            set
            {
                if (value == _truckId) return;
                _truckId = value;
                NotifyOfPropertyChange(() => TruckId);
            }
        }

        public string Location
        {
            get { return _location; }
            set
            {
                if (value == _location) return;
                _location = value;
                NotifyOfPropertyChange(() => Location);
            }
        }

        public string Material
        {
            get { return _material; }
            set
            {
                if (value == _material) return;
                _material = value;
                NotifyOfPropertyChange(() => Material);
            }
        }

        public bool IsManual
        {
            get { return _isManual; }
            set
            {
                if (value.Equals(_isManual)) return;
                _isManual = value;
                NotifyOfPropertyChange(() => IsManual);
            }
        }

        public float Quantity
        {
            get { return _quantity; }
            set
            {
                if (value.Equals(_quantity)) return;
                _quantity = value;
                NotifyOfPropertyChange(() => Quantity);
            }
        }

        public int AccountingCustomerId
        {
            get { return _accountingCustomerId; }
            set
            {
                IsAccountingCodeNeeded = CustomerId.HasValue && _accountingCustomerId != 0;
                if (value == _accountingCustomerId) return;
                _accountingCustomerId = value;
                NotifyOfPropertyChange(() => AccountingCustomerId);
            }
        }

        public bool IsDataGridAccountingCustomerCodeReadOnly
        {
            get { return _isDataGridAccountingCustomerCodeReadOnly; }
            set
            {
                if (value.Equals(_isDataGridAccountingCustomerCodeReadOnly)) return;
                _isDataGridAccountingCustomerCodeReadOnly = value;
                NotifyOfPropertyChange(() => IsDataGridAccountingCustomerCodeReadOnly);
            }
        }

        public string AccountingCustomerCode
        {
            get { return _accountingCustomerCode; }
            set
            {
                IsDataGridAccountingCustomerCodeReadOnly = CustomerShortName != "CASH" && CustomerShortName != "VOID1";
                if (IsDataGridAccountingCustomerCodeReadOnly)
                    IsAccountingCodeNeeded = (CustomerId.HasValue && AccountingCustomerId == 0) && IsDataGridAccountingCustomerCodeReadOnly;

                if (value == _accountingCustomerCode) return;
                _accountingCustomerCode = value;
                NotifyOfPropertyChange(() => AccountingCustomerCode);
            }
        }

        public bool IsAccountingCodeNeeded
        {
            get { return _isAccountingCodeNeeded; }
            set
            {
                if (value.Equals(_isAccountingCodeNeeded)) return;
                _isAccountingCodeNeeded = value;
                NotifyOfPropertyChange(() => IsAccountingCodeNeeded);
            }
        }


        public int? CustomerId
        {
            get { return _customerId; }
            set
            {
                if (value == _customerId) return;
                _customerId = value;
                NotifyOfPropertyChange(() => CustomerId);
            }
        }

        public string Company
        {
            get { return _company; }
            set
            {
                if (value == _company) return;
                _company = value;
                NotifyOfPropertyChange(() => Company);
            }
        }

        public DateTime ShipDate
        {
            get { return _shipDate; }
            set
            {
                if (value.Equals(_shipDate)) return;
                _shipDate = value;
                NotifyOfPropertyChange(() => ShipDate);
            }
        }

        public string PurchaseOrder
        {
            get { return _purchaseOrder; }
            set
            {
                if (value == _purchaseOrder) return;
                _purchaseOrder = value;
                NotifyOfPropertyChange(() => PurchaseOrder);
            }
        }

        public int TicketNumber
        {
            get { return _ticketNumber; }
            set
            {
                if (value == _ticketNumber) return;
                _ticketNumber = value;
                NotifyOfPropertyChange(() => TicketNumber);
            }
        }

        public string TicketNumberWithSeasonPrefix
        {
            get { return _ticketNumberWithSeasonPrefix; }
            set
            {
                if (value == _ticketNumberWithSeasonPrefix) return;
                _ticketNumberWithSeasonPrefix = value;
                NotifyOfPropertyChange(() => TicketNumberWithSeasonPrefix);
            }
        }

        public string VoidReason
        {
            get { return _voidReason; }
            set
            {
                if (value == _voidReason) return;
                _voidReason = value;
                NotifyOfPropertyChange(() => VoidReason);
            }
        }

        public DateTime? VoidDate
        {
            get { return _voidDate; }
            set
            {
                if (value.Equals(_voidDate)) return;
                _voidDate = value;
                NotifyOfPropertyChange(() => VoidDate);
            }
        }

        public bool Voided
        {
            get { return _voided; }
            set
            {
                if (value.Equals(_voided)) return;
                _voided = value;
                NotifyOfPropertyChange(() => Voided);
            }
        }

        public DateTime? DateExported
        {
            get { return _dateExported; }
            set
            {
                if (value.Equals(_dateExported)) return;
                _dateExported = value;
                NotifyOfPropertyChange(() => DateExported);
            }
        }

        public bool? Exported
        {
            get { return _exported; }
            set
            {
                if (value.Equals(_exported)) return;
                _exported = value;
                NotifyOfPropertyChange(() => Exported);
            }
        }

        public string CustomerShortName
        {
            get { return _customerShortName; }
            set
            {
                if (value == _customerShortName) return;
                _customerShortName = value;
                NotifyOfPropertyChange(() => CustomerShortName);
            }
        }
        #endregion
    }
}