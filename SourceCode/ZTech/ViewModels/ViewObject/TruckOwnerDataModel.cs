﻿using System;
using System.ComponentModel.DataAnnotations;
using Caliburn.Micro;
using Contract.Domain;

namespace ZTech.ViewModels.ViewObject
{
    public class TruckOwnerDataModel : PropertyChangedBase, ITruckOwnerData
    {
        private string _shortName;
        private string _dataA;
        private string _dataB;
        private string _dataC;

        public TruckOwnerDataModel()
        {
            
        }

        public TruckOwnerDataModel(ITruckOwnerData owner)
        {
            Id = owner.Id;
            ShortName = owner.ShortName;
            DataA = owner.DataA;
            DataB = owner.DataB;
            DataC = owner.DataC;
            IsActive = owner.IsActive;
            SeasonId = owner.SeasonId;
            CreateTime = owner.CreateTime;
            UpdateTime = owner.UpdateTime;
            Contact = owner.Contact;
        }

        public int? Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Owner Id is required")]
        public string ShortName
        {
            get { return _shortName; }
            set
            {
                if (value == _shortName) return;
                _shortName = value;
                NotifyOfPropertyChange(() => ShortName);
            }
        }

        public DateTime CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        public bool IsActive { get; set; }

        public string DataA
        {
            get { return _dataA; }
            set
            {
                if (value == _dataA) return;
                _dataA = value;
                NotifyOfPropertyChange(() => DataA);
            }
        }

        public string DataB
        {
            get { return _dataB; }
            set
            {
                if (value == _dataB) return;
                _dataB = value;
                NotifyOfPropertyChange(() => DataB);
            }
        }

        public string DataC
        {
            get { return _dataC; }
            set
            {
                if (value == _dataC) return;
                _dataC = value;
                NotifyOfPropertyChange(() => DataC);
            }
        }

        public int SeasonId { get; set; }
        public IContactData Contact { get;  set; }
    }

    public class TruckOwnerModel : ITruckOwnerInfo
    {
        public int? Id { get; set; }
        public string ShortName { get; set; }
    }
}