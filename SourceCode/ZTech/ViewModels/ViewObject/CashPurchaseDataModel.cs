using System;
using System.ComponentModel.DataAnnotations;
using Caliburn.Micro;
using Contract.Domain;

namespace ZTech.ViewModels.ViewObject
{
    public class CashPurchaseDataModel : PropertyChangedBase, ICashPurchase
    {
        private string _truckDescription;
        private int? _tare;
        private decimal? _quote;

        public CashPurchaseDataModel(ICashPurchase cashPurchase)
        {
            CashPurchaseId = cashPurchase.CashPurchaseId;
            _truckDescription = cashPurchase.TruckDescription;
            MaterialId = cashPurchase.MaterialId;
            Material = cashPurchase.Material;
            Quote = cashPurchase.Quote;
            CompanyId = cashPurchase.CompanyId;
            Company = cashPurchase.Company;
            LocationId = cashPurchase.LocationId;
            _tare = cashPurchase.Tare;
            PaymentForm = cashPurchase.PaymentForm;
            CheckNumber = cashPurchase.CheckNumber;
            TaxExempt = cashPurchase.TaxExempt;
            IsComplete = cashPurchase.IsComplete;
            CreateTime = cashPurchase.CreateTime;
            UpdateTime = cashPurchase.UpdateTime;
            IsActive = cashPurchase.IsActive;
            Comment = cashPurchase.Comment;
            SeasonId = cashPurchase.SeasonId;
            DataA = cashPurchase.DataA;
            DataB = cashPurchase.DataB;
            DataC = cashPurchase.DataC;
            CheckOutTime = cashPurchase.CheckOutTime;
        }

        public CashPurchaseDataModel()
        {
        }


        public int CashPurchaseId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = @"Truck Description is required")]
        public string TruckDescription
        {
            get { return _truckDescription; }
            set
            {
                if (value == _truckDescription) return;
                _truckDescription = value;
                NotifyOfPropertyChange(() => TruckDescription);
                //Validator.ValidateProperty(value, new ValidationContext(this, null, null) { MemberName = "TruckDescription" });
            }
        }

        public int? MaterialId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = @"Quote is required")]
        [Range(Double.MinValue, Double.MaxValue, ErrorMessage = @"The Quote must be a valid number.")]
        public decimal? Quote
        {
            get { return _quote; }
            set
            {
                if (value == _quote) return;
                _quote = value;
                NotifyOfPropertyChange(() => Quote);
                //Validator.ValidateProperty(value, new ValidationContext(this, null, null) { MemberName = "Quote" });
            }
        }
        
        public int? CompanyId { get; set; }
        
        public int? LocationId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = @"Tare is required")]
        [Range(0, Int32.MaxValue, ErrorMessage = @"The Tare must be a valid number.")]
        public int? Tare
        {
            get { return _tare; }
            set
            {
                if (value == _tare) return;
                _tare = value;
                NotifyOfPropertyChange(() => Tare);
                //Validator.ValidateProperty(value, new ValidationContext(this, null, null) { MemberName = "Tare" });
            }
        }
        
        public string PaymentForm { get; set; }
        
        public string CheckNumber { get; set; }
        
        public bool TaxExempt { get; set; }
        
        public bool IsComplete { get; set; }
        
        public DateTime CreateTime { get; set; }
        
        public DateTime? UpdateTime { get; set; }
        
        public bool IsActive { get; set; }
        
        public string Comment { get; set; }
        
        public string DataA { get; set; }
        
        public string DataB { get; set; }
        
        public string DataC { get; set; }
        
        public int SeasonId { get; set; }
        
        public ISeason Season { get; set; }

        public ICompanyInfo Company { get; set; }

        public IMaterialInfo Material { get; set; }
        public DateTime? CheckOutTime { get; set; }
    }
}