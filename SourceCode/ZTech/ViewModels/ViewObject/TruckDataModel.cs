using System;
using System.ComponentModel.DataAnnotations;
using Caliburn.Micro;
using Contract.Domain;

namespace ZTech.ViewModels.ViewObject
{
    public class TruckDataModel : PropertyChangedBase, ITruckData
    {
        private DateTime _createTime;
        private int? _cubicYards;
        private DateTime? _dateTimeOfWeight;
        private string _description;
        private bool _needsTareOverride;
        private string _phoneNumber;
        private string _shortName;
        private string _sticker;
        private string _tag;
        private int? _tareInterval;
        private DateTime? _updateTime;
        private int _weight;

        public TruckDataModel(ITruckData truck)
        {
            Id = truck.Id;
            TruckOwnerId = truck.TruckOwnerId;
            ShortName = truck.ShortName;
            Tag = truck.Tag;
            Description = truck.Description;
            TareInterval = truck.TareInterval;
            NeedsTareOverride = truck.NeedsTareOverride;
            CubicYards = truck.CubicYards;
            Sticker = truck.Sticker;
            CreateTime = truck.CreateTime;
            UpdateTime = truck.UpdateTime;
            IsActive = truck.IsActive;
            Comment = truck.Comment;
            PhoneNumber = truck.PhoneNumber;
            SeasonId = truck.SeasonId;
            Weight = truck.Weight;
            DateTimeOfWeight = truck.DateTimeOfWeight;
        }

        public TruckDataModel()
        {
        }

        public int Id { get; set; }
        public int? TruckOwnerId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Truck Id is required")]
        public string ShortName
        {
            get { return _shortName; }
            set
            {
                if (value == _shortName) return;
                _shortName = value != null ? value.ToUpper() : null;
                NotifyOfPropertyChange(() => ShortName);
            }
        }

        public string Tag
        {
            get { return _tag; }
            set
            {
                if (value == _tag) return;
                _tag = value != null ? value.ToUpper() : null;
                NotifyOfPropertyChange(() => Tag);
            }
        }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Description is required")]
        public string Description
        {
            get { return _description; }
            set
            {
                if (value == _description) return;
                _description = value;
                NotifyOfPropertyChange(() => Description);
            }
        }

        public int? TareInterval
        {
            get { return _tareInterval; }
            set
            {
                if (value == _tareInterval) return;
                _tareInterval = value;
                NotifyOfPropertyChange(() => TareInterval);
            }
        }

        public bool NeedsTareOverride
        {
            get { return _needsTareOverride; }
            set
            {
                if (value.Equals(_needsTareOverride)) return;
                _needsTareOverride = value;
                NotifyOfPropertyChange(() => NeedsTareOverride);
                if (!value)
                    TareInterval = null;
                else
                {
                    if (!TareInterval.HasValue)
                        TareInterval = 0;
                }
            }
        }

        public int? CubicYards
        {
            get { return _cubicYards; }
            set
            {
                if (value == _cubicYards) return;
                _cubicYards = value;
                NotifyOfPropertyChange(() => CubicYards);
            }
        }

        public string Sticker
        {
            get { return _sticker; }
            set
            {
                if (value == _sticker) return;
                _sticker = value != null ? value.ToUpper() : null;
                NotifyOfPropertyChange(() => Sticker);
            }
        }

        public DateTime CreateTime
        {
            get { return _createTime; }
            set
            {
                if (value.Equals(_createTime)) return;
                _createTime = value;
                NotifyOfPropertyChange(() => CreateTime);
            }
        }

        public DateTime? UpdateTime
        {
            get { return _updateTime; }
            set
            {
                if (value.Equals(_updateTime)) return;
                _updateTime = value;
                NotifyOfPropertyChange(() => UpdateTime);
            }
        }

        public bool IsActive { get; set; }
        public string Comment { get; set; }

        public string PhoneNumber
        {
            get { return _phoneNumber; }
            set
            {
                if (value == _phoneNumber) return;
                _phoneNumber = value;
                NotifyOfPropertyChange(() => PhoneNumber);
            }
        }

        public int SeasonId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Weight is required")]
        public int Weight
        {
            get { return _weight; }
            set
            {
                if (value == _weight) return;
                _weight = value;
                NotifyOfPropertyChange(() => Weight);
            }
        }

        public DateTime? DateTimeOfWeight
        {
            get { return _dateTimeOfWeight; }
            set
            {
                if (value.Equals(_dateTimeOfWeight)) return;
                _dateTimeOfWeight = value;
                NotifyOfPropertyChange(() => DateTimeOfWeight);
            }
        }
    }
}