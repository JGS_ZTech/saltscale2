using System;
using Caliburn.Micro;
using Contract.Domain;

namespace ZTech.ViewModels.ViewObject
{
    public class CashPurchaseTicketDataModel : PropertyChangedBase, ITicketData
    {
        private int _ticketNumber;
        private int _oldTicketNumber;
        private float _oldQuantity;
        private decimal _change;
        private string _checkNumber;
        private decimal _due;
        private bool _isFirstWeightEnabled;
        private bool _isSecondWeightEnabled;
        private bool _isTicketNumberEnabled;
        private decimal _materialCharge;
        private decimal _netWeightTon;
        private decimal _tax;
        private string _tended;
        private string _truckDescription;
        private bool _hideDollarAmount;
        private bool _isOverrideEnabled;
        private decimal _fuelSurcharge;

        public CashPurchaseTicketDataModel(ITicketData cashTicket)
        {
            TicketId = cashTicket.TicketId;
            TicketNumber = cashTicket.TicketNumber;
            OldTicketNumber = cashTicket.TicketNumber;
            Quantity = cashTicket.Quantity;
            IsManual = cashTicket.IsManual;
            TruckId = cashTicket.TruckId;
            IsVoid = cashTicket.IsVoid;
            VoidReason = cashTicket.VoidReason;
            IsReconciled = cashTicket.IsReconciled;
            HasBeenExported = cashTicket.HasBeenExported;
            CompanyId = cashTicket.CompanyId;
            CreateTime = cashTicket.CreateTime;
            UpdateTime = cashTicket.UpdateTime;
            IsActive = cashTicket.IsActive;
            OrderId = cashTicket.OrderId;
            SeasonId = cashTicket.SeasonId;
            Comment = cashTicket.Comment;
            TruckWeight = cashTicket.TruckWeight;
            TruckShortName = cashTicket.TruckShortName;
            TruckTag = cashTicket.TruckTag;
            TruckDescription = cashTicket.TruckDescription;
            MaterialId = cashTicket.MaterialId;
            TruckRate = cashTicket.TruckRate;
            CashData = cashTicket.CashData;
            TaxRate = cashTicket.CashData.TaxRate.HasValue ? cashTicket.CashData.TaxRate.Value : 0m;
            FuelSurcharge = cashTicket.FuelSurcharge;
            DataA = cashTicket.DataA;
            DataB = cashTicket.DataB;
            DataC = cashTicket.DataC;
            ConfirmationNumber = cashTicket.ConfirmationNumber;
            CustomerContactName = cashTicket.CustomerContactName;
        }

        public decimal GetTendedValue(string tended)
        {
            if (string.IsNullOrWhiteSpace(tended))
                return 0m;

            decimal result;

            if (decimal.TryParse(tended.Trim(), out result))
                return result;

            return 0m;
        }

        public CashPurchaseTicketDataModel()
        {
        }

        public int? TicketId { get; set; }

        public int TicketNumber
        {
            get { return _ticketNumber; }
            set
            {
                if (value == _ticketNumber) return;
                _ticketNumber = value;
                NotifyOfPropertyChange(() => TicketNumber);
            }
        }

        public int OldTicketNumber
        {
            get { return _oldTicketNumber; }
            set
            {
                if (value == _oldTicketNumber) return;
                _oldTicketNumber = value;
                NotifyOfPropertyChange(() => OldTicketNumber);
            }
        }

        public float Quantity { get; set; }
        public bool IsManual { get; set; }
        public int? TruckId { get; set; }
        public bool IsVoid { get; set; }
        public string VoidReason { get; set; }
        public bool? IsReconciled { get; set; }
        public bool? HasBeenExported { get; set; }
        public int? CompanyId { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        public bool IsActive { get; set; }
        public int? OrderId { get; set; }
        public string Comment { get; set; }
        public string DataA { get; set; }
        public string DataB { get; set; }
        public string DataC { get; set; }
        public int SeasonId { get; set; }
        public int TruckWeight { get; set; }
        public string TruckShortName { get; set; }
        public string TruckTag { get; set; }
        public string TruckSticker { get; set; }
        public ICashPurchaseData CashData { get; set; }
        public string SpecialInstruction { get; set; }
        public string CustomerAddress { get; set; }
        public string CustomerName { get; set; }
        public int MaterialId { get; set; }
        public IAddressData Address { get; set; }
        public string PO { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerShortName { get; set; }
        public decimal? TruckRate { get; set; }
        public int? OrderNumber { get; set; }
        public float OldQuantity
        {
            get { return _oldQuantity; }
            set
            {
                if (value == _oldQuantity) return;
                _oldQuantity = value;
                NotifyOfPropertyChange(() => OldQuantity);
            }
        }

        public bool IsOverrideEnabled
        {
            get { return _isOverrideEnabled; }
            set
            {
                if (value.Equals(_isOverrideEnabled)) return;
                _isOverrideEnabled = value;
                NotifyOfPropertyChange(() => IsOverrideEnabled);
            }
        }

        public bool IsTicketNumberEnabled
        {
            get { return _isTicketNumberEnabled; }
            set
            {
                if (value.Equals(_isTicketNumberEnabled)) return;
                _isTicketNumberEnabled = value;
                NotifyOfPropertyChange(() => IsTicketNumberEnabled);
            }
        }

        public bool IsSecondWeightEnabled
        {
            get { return _isSecondWeightEnabled; }
            set
            {
                if (value.Equals(_isSecondWeightEnabled)) return;
                _isSecondWeightEnabled = value;
                NotifyOfPropertyChange(() => IsSecondWeightEnabled);
            }
        }

        public bool IsFirstWeightEnabled
        {
            get { return _isFirstWeightEnabled; }
            set
            {
                if (value.Equals(_isFirstWeightEnabled)) return;
                _isFirstWeightEnabled = value;
                NotifyOfPropertyChange(() => IsFirstWeightEnabled);
            }
        }

        public string CheckNumber
        {
            get { return _checkNumber; }
            set
            {
                if (value == _checkNumber) return;
                _checkNumber = value;
                NotifyOfPropertyChange(() => CheckNumber);
            }
        }

        public decimal Change
        {
            get { return _change; }
            set
            {
                if (value == _change) return;
                _change = value;
                NotifyOfPropertyChange(() => Change);
            }
        }

        public string Tended
        {
            get { return _tended; }
            set
            {
                if (value == _tended) return;
                _tended = value;
                NotifyOfPropertyChange(() => Tended);
                var tended = GetTendedValue(value);
                Change = tended - Due;
            }
        }

        public decimal Due
        {
            get { return _due; }
            set
            {
                if (value == _due) return;
                _due = value;
                NotifyOfPropertyChange(() => Due);
            }
        }

        public decimal Tax
        {
            get { return _tax; }
            set
            {
                if (value == _tax) return;
                _tax = value;
                NotifyOfPropertyChange(() => Tax);
            }
        }

        public decimal MaterialCharge
        {
            get { return _materialCharge; }
            set
            {
                if (value == _materialCharge) return;
                _materialCharge = value;
                NotifyOfPropertyChange(() => MaterialCharge);
            }
        }

        public decimal TaxRate { get; set; }

        public string TruckDescription
        {
            get { return _truckDescription; }
            set
            {
                if (value == _truckDescription) return;
                _truckDescription = value;
                NotifyOfPropertyChange(() => TruckDescription);
            }
        }

        public decimal NetWeightTon
        {
            get { return _netWeightTon; }
            set
            {
                if (value == _netWeightTon) return;
                _netWeightTon = value;
                NotifyOfPropertyChange(() => NetWeightTon);
            }
        }

        public bool HideDollarAmount
        {
            get { return _hideDollarAmount; }
            set
            {
                if (value.Equals(_hideDollarAmount)) return;
                _hideDollarAmount = value;
                NotifyOfPropertyChange(() => HideDollarAmount);
            }
        }

        public bool IsTareExpired { get; set; }

        public decimal FuelSurcharge
        {
            get { return _fuelSurcharge; }
            set
            {
                if (value == _fuelSurcharge) return;
                _fuelSurcharge = value;
                NotifyOfPropertyChange(() => FuelSurcharge);
            }
        }

        public int? ConfirmationNumber { get; set; }
        public string CustomerContactName { get; set; }
        public string CustomerOrganization { get; set; }
    }
}