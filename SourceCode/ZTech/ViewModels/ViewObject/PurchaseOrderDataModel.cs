using System;
using System.ComponentModel.DataAnnotations;
using Caliburn.Micro;
using Contract.Domain;

namespace ZTech.ViewModels.ViewObject
{
    public class PurchaseOrderDataModel : PropertyChangedBase, IPurchaseOrder
    {
        private string _purchaseOrderNumber;
        private int? _quantity;
        private DateTime? _effectiveDate;
        private DateTime _createTime;
        private DateTime? _updateTime;
        private string _quantityText;

        public PurchaseOrderDataModel(IPurchaseOrder purchaseOrder)
        {
            Id = purchaseOrder.Id;
            PurchaseOrderNumber = purchaseOrder.PurchaseOrderNumber;
            CustomerId = purchaseOrder.CustomerId;
            QuantityOfPO = purchaseOrder.QuantityOfPO;
            QuantityText = purchaseOrder.QuantityOfPO.HasValue ? purchaseOrder.QuantityOfPO.Value.ToString() : null;
            
            QuantityDispatched = purchaseOrder.QuantityDispatched;
            QuantityOfPOOnOrder = purchaseOrder.QuantityOfPOOnOrder;
            BalancePerOrder = purchaseOrder.BalancePerOrder;
            QuantityOfPORemaining = purchaseOrder.QuantityOfPORemaining;
            MaterialId = purchaseOrder.MaterialId;
            MaterialDescription = purchaseOrder.MaterialDescription;
            //Material = purchaseOrder.Material;
            IsComplete = purchaseOrder.IsComplete;
            IsHeld = purchaseOrder.IsHeld;
            EffectiveDate = purchaseOrder.EffectiveDate;
            CreateTime = purchaseOrder.CreateTime;
            UpdateTime = purchaseOrder.UpdateTime;
            IsActive = purchaseOrder.IsActive;
            Comment = purchaseOrder.Comment;
            SeasonId = purchaseOrder.SeasonId;
            DataA = purchaseOrder.DataA;
            DataB = purchaseOrder.DataB;
            DataC = purchaseOrder.DataC;
        }

        public PurchaseOrderDataModel()
        {
        }

        public int Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = @"The Purchase Order Number cannot be left blank.")]
        public string PurchaseOrderNumber
        {
            get { return _purchaseOrderNumber; }
            set
            {
                if (value == _purchaseOrderNumber) return;
                _purchaseOrderNumber = value;
                NotifyOfPropertyChange(() => PurchaseOrderNumber);
                //Validator.ValidateProperty(value, new ValidationContext(this, null, null) { MemberName = "PurchaseOrderNumber" });
            }
        }
        public int CustomerId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = @"The Quantity cannot be left blank.")]
        [Range(1, Int32.MaxValue, ErrorMessage = @"The Quantity must be a valid number and greater than 0.")]
        public int? QuantityOfPO
        {
            get { return _quantity; }
            set
            {
                if (value == _quantity) return;
                _quantity = value;
                NotifyOfPropertyChange(() => QuantityOfPO);
            }
        }

        public string QuantityText
        {
            get { return _quantityText; }
            set
            {
                if (value == _quantityText) return;
                _quantityText = value;
                NotifyOfPropertyChange(() => QuantityText);
                if (!string.IsNullOrWhiteSpace(_quantityText))
                {
                    int result;
                    if (int.TryParse(_quantityText, out result))
                        QuantityOfPO = result;
                    else
                        QuantityOfPO = null;
                }
                else
                    QuantityOfPO = null;
            }
        }

        public float? QuantityDispatched { get; set; }
        public float? QuantityOfPOOnOrder { get; set; }
        public float? QuantityOfPORemaining { get; set; }
        public int MaterialId { get; set; }
        public string MaterialDescription { get; set; }
        public IMaterialInfo Material { get; set; }
        public bool IsComplete { get; set; }
        public bool IsHeld { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = @"The Effective Date cannot be left blank.")]
        [DataType(DataType.DateTime, ErrorMessage = @"The Effective Date must be a valid date.")]
        public DateTime? EffectiveDate
        {
            get { return _effectiveDate; }
            set
            {
                if (value == _effectiveDate) return;
                _effectiveDate = value;
                NotifyOfPropertyChange(() => EffectiveDate);
                //Validator.ValidateProperty(value, new ValidationContext(this, null, null) { MemberName = "EffectiveDate" });
            }
        }
        public DateTime CreateTime
        {
            get { return _createTime; }
            set
            {
                if (value.Equals(_createTime)) return;
                _createTime = value;
                NotifyOfPropertyChange(() => CreateTime);
            }
        }

        public DateTime? UpdateTime
        {
            get { return _updateTime; }
            set
            {
                if (value.Equals(_updateTime)) return;
                _updateTime = value;
                NotifyOfPropertyChange(() => UpdateTime);
            }
        }

        public bool IsActive { get; set; }
        public string Comment { get; set; }
        public int SeasonId { get; set; }
        public int CompanyId { get; set; }
        public string DataA { get; set; }
        public string DataB { get; set; }
        public string DataC { get; set; }
        public float? BalancePerOrder { get; set; }

        /*
        public float? QuantityDispatched
        {
            get
            {
                if(QuantityOfPO.HasValue && QuantityDispatched.HasValue)
                    return QuantityOfPO.Value - QuantityDispatched.Value;

                return null;
            }
        }
        */
    }
}