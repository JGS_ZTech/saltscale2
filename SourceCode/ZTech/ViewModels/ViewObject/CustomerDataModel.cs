using System;
using System.ComponentModel.DataAnnotations;
using Caliburn.Micro;
using Contract.Domain;

namespace ZTech.ViewModels.ViewObject
{
    public class CustomerDataModel : PropertyChangedBase, ICustomerAddress
    {
        private string _shortName;
        private string _name;
        private DateTime _createTime;
        private DateTime? _updateTime;
        private decimal? _cashMaterialRate;
        private bool _isTaxExempt;
        private bool _hideDollarAmounts;
        private int? _reportingEntityId;
        private bool _isShippingLocation;
        private string _specialInstruction;
        private string _comment;

        public CustomerDataModel(ICustomerAddress customer)
        {
            Id = customer.Id;
            _name = customer.Name;
            _shortName = customer.ShortName;
            Address = customer.Address;
            SpecialInstruction = customer.SpecialInstruction;
            Comment = customer.Comment;
            PrimaryContactOrganization = customer.PrimaryContactOrganization;
            PrimaryContactInfo = customer.PrimaryContactInfo;
            FullAddress = customer.FullAddress;
            IsShippingLocation = customer.IsShippingLocation;
            ReportingEntityId = customer.ReportingEntityId;
            _createTime = customer.CreateTime;
            _updateTime = customer.UpdateTime;
            Contact = customer.Contact;
            SeasonId = customer.SeasonId;
            //Bid = new BidDataModel(customer.Bid);
            HideDollarAmounts = customer.HideDollarAmounts;
            IsTaxExempt = customer.IsTaxExempt;
            CashMaterialRate = customer.CashMaterialRate;
            Company = customer.Company;
        }

        public CustomerDataModel()
        {
        }

        public int Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = @"The Customer ID cannot be left blank.")]
        public string ShortName
        {
            get { return _shortName; }
            set
            {
                if (value == _shortName) return;
                _shortName = value;
                NotifyOfPropertyChange(() => ShortName);
            }
        }
        
        [Required(AllowEmptyStrings = false, ErrorMessage = @"The Dispatcher Name cannot be left blank.")]
        public string Name
        {
            get { return _name; }
            set
            {
                if (value == _name) return;
                _name = value;
                NotifyOfPropertyChange(() => Name);
            }
        }

        public string Address { get; private set; }

        public string SpecialInstruction
        {
            get { return _specialInstruction; }
            set
            {
                if (value == _specialInstruction) return;
                _specialInstruction = value;
                NotifyOfPropertyChange(() => SpecialInstruction);
            }
        }

        public string Comment
        {
            get { return _comment; }
            set
            {
                if (value == _comment) return;
                _comment = value;
                NotifyOfPropertyChange(() => Comment);
            }
        }

        public string PrimaryContactOrganization { get; private set; }

        public string PrimaryContactInfo { get; private set; }

        public string FullAddress { get; private set; }

        public bool IsShippingLocation
        {
            get { return _isShippingLocation; }
            set
            {
                if (value.Equals(_isShippingLocation)) return;
                _isShippingLocation = value;
                NotifyOfPropertyChange(() => IsShippingLocation);
            }
        }

        public int? ReportingEntityId
        {
            get { return _reportingEntityId; }
            set
            {
                if (value == _reportingEntityId) return;
                _reportingEntityId = value;
                NotifyOfPropertyChange(() => ReportingEntityId);
            }
        }

        public DateTime CreateTime
        {
            get { return _createTime; }
            set
            {
                if (value.Equals(_createTime)) return;
                _createTime = value;
                NotifyOfPropertyChange(() => CreateTime);
            }
        }

        public DateTime? UpdateTime
        {
            get { return _updateTime; }
            set
            {
                if (value.Equals(_updateTime)) return;
                _updateTime = value;
                NotifyOfPropertyChange(() => UpdateTime);
            }
        }

        public int SeasonId { get; set; }

        public bool HideDollarAmounts
        {
            get { return _hideDollarAmounts; }
            set
            {
                if (value.Equals(_hideDollarAmounts)) return;
                _hideDollarAmounts = value;
                NotifyOfPropertyChange(() => HideDollarAmounts);
            }
        }

        public decimal? CashMaterialRate
        {
            get { return _cashMaterialRate; }
            set
            {
                if (value == _cashMaterialRate) return;
                _cashMaterialRate = value;
                NotifyOfPropertyChange(() => CashMaterialRate);
            }
        }

        public bool IsTaxExempt
        {
            get { return _isTaxExempt; }
            set
            {
                if (value.Equals(_isTaxExempt)) return;
                _isTaxExempt = value;
                NotifyOfPropertyChange(() => IsTaxExempt);
            }
        }

        public IBid Bid { get; set; }

        public IContactData Contact { get; set; }

        public IBid PrimaryBid { get; set; }

        public ICompanyInfo Company { get; set; }
    }
}