using System;
using System.ComponentModel.DataAnnotations;
using Caliburn.Micro;
using Contract.Domain;

namespace ZTech.ViewModels.ViewObject
{
    public class BidDataModel : PropertyChangedBase, IBid
    {
        private decimal? _truckingRate;
        private decimal? _toll;
        private decimal? _seasonMaterialEstimateTon;
        private string _pOOverride;

        public BidDataModel(IBid bid)
        {
            BidId = bid.BidId;
            CustomerId = bid.CustomerId;
            SiteId = bid.SiteId;
            Site = bid.Site;
            MaterialId = bid.MaterialId;
            Material = bid.Material;
            PricePerTon = bid.PricePerTon;
            PricePerYard = bid.PricePerYard;
            TruckingRate = bid.TruckingRate;
            POOverride = bid.POOverride;
            CreateTime = bid.CreateTime;
            UpdateTime = bid.UpdateTime;
            IsActive = bid.IsActive;
            Comment = bid.Comment;
            SeasonId = bid.SeasonId;
            Toll = bid.Toll;
            SeasonMaterialEstimateTon = bid.SeasonMaterialEstimateTon;
        }

        public BidDataModel()
        {
        }

        public int? BidId { get; set; }
        public int CustomerId { get; set; }
        public int SiteId { get; set; }
        public ISiteInfo Site { get; set; }
        public int MaterialId { get; set; }
        public IMaterialInfo Material { get; set; }
        public decimal? PricePerTon { get; set; }
        public decimal? PricePerYard { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = @"The Trucking Rate cannot be left blank.")]
        [Range(0, Int32.MaxValue, ErrorMessage = @"The Trucking Rate must be a valid number.")]
        public decimal? TruckingRate
        {
            get { return _truckingRate; }
            set
            {
                if (value == _truckingRate) return;
                _truckingRate = value;
                NotifyOfPropertyChange(() => TruckingRate);
                //Validator.ValidateProperty(value, new ValidationContext(this, null, null) { MemberName = "TruckingRate" });
            }
        }
        public string POOverride
        {
            get { return _pOOverride; }
            set
            {
                _pOOverride = value;
                NotifyOfPropertyChange(() => POOverride);
            }
        }
        public DateTime CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        public bool IsActive { get; set; }
        public string Comment { get; set; }
        public int SeasonId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = @"The Toll cannot be left blank.")]
        [Range(Double.MinValue, Double.MaxValue, ErrorMessage = @"The Toll must be a valid number.")]
        public decimal? Toll
        {
            get { return _toll; }
            set
            {
                if (value == _toll) return;
                _toll = value;
                NotifyOfPropertyChange(() => Toll);
                //Validator.ValidateProperty(value, new ValidationContext(this, null, null) { MemberName = "Toll" });
            }
        }

        [Range(Double.MinValue, Double.MaxValue, ErrorMessage = @"The Material Est Tons must be a valid number.")]
        public decimal? SeasonMaterialEstimateTon
        {
            get { return _seasonMaterialEstimateTon; }
            set
            {
                if (value == _seasonMaterialEstimateTon) return;
                _seasonMaterialEstimateTon = value;
                NotifyOfPropertyChange(() => SeasonMaterialEstimateTon);
            }
        }

        public string CompanyFullName { get; set; }
        public string MaterialShortName { get; set; }
    }
}