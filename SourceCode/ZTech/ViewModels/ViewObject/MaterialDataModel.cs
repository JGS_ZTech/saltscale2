using System;
using System.ComponentModel.DataAnnotations;
using Caliburn.Micro;
using Contract.Domain;

namespace ZTech.ViewModels.ViewObject
{
    public class MaterialDataModel : PropertyChangedBase, IMaterialData
    {
        private string _description;
        private string _name;
        private decimal? _pricePerTon;
        private decimal? _pricePerYard;
        private decimal? _priceEach;
        private int? _minimumCharge;
        private DateTime _createTime;
        private DateTime? _updateTime;
        private decimal? _pricePerPound;

        public MaterialDataModel()
        {
           
        }

        public MaterialDataModel(IMaterialData material)
        {
            Id = material.Id;
            Name = material.Name;
            Description = material.Description;
            PricePerTon = material.PricePerTon;
            PricePerYard = material.PricePerYard;
            PriceEach = material.PriceEach;
            MinimumCharge = material.MinimumCharge;
            CreateTime = material.CreateTime;
            UpdateTime = material.UpdateTime;
            IsActive = material.IsActive;
            SeasonId = material.SeasonId;
        }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Description is required")]
        public string Description
        {
            get { return _description; }
            set
            {
                if (value == _description) return;
                _description = value;
                NotifyOfPropertyChange(() => Description);
            }
        }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Material Id is required")]
        public string Name
        {
            get { return _name; }
            set
            {
                if (value == _name) return;
                _name = value;
                NotifyOfPropertyChange(() => Name);
            }
        }

        public int? Id { get; set; }

        public decimal? PricePerTon
        {
            get { return _pricePerTon; }
            set
            {
                if (value == _pricePerTon) return;
                _pricePerTon = value;
                NotifyOfPropertyChange(() => PricePerTon);
                PricePerPound = PricePerTon/2000m;
            }
        }
        public decimal? PricePerPound
        {
            get { return _pricePerPound; }
            set
            {
                if (value == _pricePerPound) return;
                _pricePerPound = value;
                NotifyOfPropertyChange(() => PricePerPound);
            }
        }

        public decimal? PricePerYard
        {
            get { return _pricePerYard; }
            set
            {
                if (value == _pricePerYard) return;
                _pricePerYard = value;
                NotifyOfPropertyChange(() => PricePerYard);
            }
        }

        public decimal? PriceEach
        {
            get { return _priceEach; }
            set
            {
                if (value == _priceEach) return;
                _priceEach = value;
                NotifyOfPropertyChange(() => PriceEach);
            }
        }

        public int? MinimumCharge
        {
            get { return _minimumCharge; }
            set
            {
                if (value == _minimumCharge) return;
                _minimumCharge = value;
                NotifyOfPropertyChange(() => MinimumCharge);
            }
        }

        public DateTime CreateTime
        {
            get { return _createTime; }
            set
            {
                if (value.Equals(_createTime)) return;
                _createTime = value;
                NotifyOfPropertyChange(() => CreateTime);
            }
        }

        public DateTime? UpdateTime
        {
            get { return _updateTime; }
            set
            {
                if (value.Equals(_updateTime)) return;
                _updateTime = value;
                NotifyOfPropertyChange(() => UpdateTime);
            }
        }

        public bool IsActive { get; set; }
        public int SeasonId { get; set; }
    }
}