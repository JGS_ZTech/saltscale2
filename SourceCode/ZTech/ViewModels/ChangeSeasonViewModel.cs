﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Caliburn.Micro;
using Contract.Domain;
using Contract.Events;
using Contract.Services;
using ZTech.Controls;
using ZTech.ViewModels.Abstract;

namespace ZTech.ViewModels
{
    public class ChangeSeasonViewModel : BaseViewModel
    {
        private readonly IEventAggregator _eventAggregator;
        private readonly ILog _log;
        private readonly int _seasonId;
        private string _errors;
        private BindableCollection<ISeason> _seasons;
        private ISeason _selectedSeason;

        public ChangeSeasonViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            _log = LogManager.GetLog(typeof (ChangeSeasonViewModel));
            _seasonId = (int) Application.Current.Properties["SeasonId"];
            DisplayName = "Change Season";
        }

        public ISeason SelectedSeason
        {
            get { return _selectedSeason; }
            set
            {
                if (Equals(value, _selectedSeason)) return;
                _selectedSeason = value;
                NotifyOfPropertyChange(() => SelectedSeason);
            }
        }

        public BindableCollection<ISeason> Seasons
        {
            get { return _seasons; }
            set
            {
                if (Equals(value, _seasons)) return;
                _seasons = value;
                NotifyOfPropertyChange(() => Seasons);
            }
        }

        public string Errors
        {
            get { return _errors; }
            set
            {
                if (value == _errors) return;
                _errors = value;
                NotifyOfPropertyChange(() => Errors);
            }
        }

        #region Actions

        public void Cancel()
        {
            TryClose(false);
        }

        public void KeyHandle(Key key)
        {
            if (key == Key.F2)
            {
                Done();
            }
        }

        public void Done()
        {
            try
            {
                if (!IsValid())
                    return;

                MessageBoxResult messageBoxResult =
                   (MessageBox.Show("Are you sure, you want to change Season?", "Confirmation",
                       MessageBoxButton.YesNo, MessageBoxImage.Question));

                if (messageBoxResult == MessageBoxResult.No)
                    return;

                _eventAggregator.PublishOnUIThreadAsync(new SeasonChangeArgs(SelectedSeason));

                TryClose(true);
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Errors = string.Format(
                    "Operation failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                    ex.Message);

                ShowMessage("Error", Errors, NotificationType.Error);
            }
        }

        private bool IsValid()
        {
            if (SelectedSeason == null)
            {
                Errors = string.Format(
                    "Validation Failed. {0} {1}", Environment.NewLine, "Season cannot be Empty.");

                ShowMessage("Validation Errors", Errors, NotificationType.Warning);
                return false;
            }

            return true;
        }

        #endregion

        protected override void OnInitialize()
        {
            base.OnInitialize();
            IReadOnlyList<ISeason> seasons = IoC.Get<ICommonService>().GetSeasons();
            Seasons = new BindableCollection<ISeason>(seasons);
            SelectedSeason = seasons.FirstOrDefault(s => s.SeasonId == _seasonId);
        }
    }
}