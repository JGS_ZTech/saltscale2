﻿using System;
using System.Linq;
using System.Windows;
using Caliburn.Micro;
using Contract.Domain;
using Contract.Events;
using Contract.Services;
using ZTech.Controls;
using ZTech.ViewModels.Abstract;
using ZTech.ViewModels.ViewObject;

namespace ZTech.ViewModels
{
    public class EditTruckViewModel : BaseTruckViewModel
    {
        public EditTruckViewModel(IEventAggregator eventAggregator): base(eventAggregator, "Edit Truck Screen")
        {
            _log = LogManager.GetLog(typeof (EditTruckViewModel));
        }

        public int TruckId { get; set; }
        private bool _ResetTruckOwnerToNull = false;
        //private ITruckOwnerInfo _selectedTruckOwner;

        public void ResetTruckOwner()
        {
            ResetTruckOwnerToNull = true;
        }

        public bool ResetTruckOwnerToNull
        {
            get { return _ResetTruckOwnerToNull; }
            set
            {
                if (Equals(value, _ResetTruckOwnerToNull)) return;
                _ResetTruckOwnerToNull = value;
                NotifyOfPropertyChange(() => _ResetTruckOwnerToNull);

                if (ResetTruckOwnerToNull)
                {
                    base.SelectedTruckOwner = null;
                }
            }
        }

        protected override void InitializeTruck()
        {
            Truck = new TruckDataModel(IoC.Get<ITruckService>().GetTruck(TruckId, _seasonId));

            if (Truck.TruckOwnerId != null)
            {
                ITruckOwnerInfo owner = TruckOwners.FirstOrDefault(o => o.Id == Truck.TruckOwnerId);
                if (owner != null)
                    SelectedTruckOwner = owner;


                if (Truck.NeedsTareOverride)
                {
                    if (!Truck.TareInterval.HasValue)
                        Truck.TareInterval = 0;

                    var date = (Truck.DateTimeOfWeight.HasValue
                        ? Truck.DateTimeOfWeight.Value.AddDays(Truck.TareInterval.Value)
                        : DateTime.Today).Date;

                    if (date <= DateTime.Today.Date)
                    {
                        Truck.Weight = 0;
                        MessageBox.Show("Truck weight expired. The dispatcher must take a new weight", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        Errors = "Truck weight expired. The dispatcher must take a new weight";
                    }
                }
            }
        }

        public void DeleteTruck()
        {
            if (Truck == null) return;
            System.Action action = () =>
            {
                var messageBoxResult =
                    (MessageBox.Show(string.Format("Are you sure, you want to delete '{0}' truck ?", Truck.ShortName), "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question));

                if (messageBoxResult == MessageBoxResult.Yes)
                    DeleteTruckAction();
            };

            action.OnUIThread();

        }

        private void DeleteTruckAction()
        {
            try
            {
                if (Truck == null) return;

                IoC.Get<ITruckService>().DeleteTruck(Truck.Id);
                
                _eventAggregator.PublishOnCurrentThread(new TruckModifiedArgs(null));

                TryClose(true);

            }
            catch (Exception ex)
            {
                _log.Error(ex);
                System.Action action = () =>
                {
                    var messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "Truck deleting failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };

                action.OnUIThreadAsync();
            }
        }
    }
}