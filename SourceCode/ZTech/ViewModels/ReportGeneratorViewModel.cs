﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Caliburn.Micro;
using Contract.Domain;
using Contract.Events;
using Contract.Exception;
using Contract.Services;
using Microsoft.Reporting.WinForms;
using Newtonsoft.Json;
using ZTech.Controls;
using ZTech.Reports;
using ZTech.ViewModels.Abstract;
using Action = System.Action;
using ValidationResult = System.ComponentModel.DataAnnotations.ValidationResult;
using BusinessLogic;
using ZTech.Helpers;

namespace ZTech.ViewModels
{
    public class ReportGeneratorViewModel : BaseViewModel
    {
        private static ReportGeneratorViewModel _instance = new ReportGeneratorViewModel();
        public static ReportGeneratorViewModel Instance { get { return _instance; } }
        

        public List<ReportFilterGroupApplied> ReportFiltersApplied { get; set; }

        private readonly IEventAggregator _eventAggregator;
        private readonly ILog _log;
        private readonly int _seasonId;
        private readonly int _siteId;
        private ReportDefintionEnum _selectedReport;
        private ReportDateRangeEnum _selectedDateRange;
        private ReportSortByEnum _selectedSortBy;
        private CustomerTypeEnum _selectedCustomerType;
        private DetailSummaryEnum _selectedDetailSummary;
        private IReadOnlyList<ReportDefintionEnum> _reportList;
        private string _errors;
        private ReportDataSource _reportDataSource;
        private ICollectionView _collectionView;
        private DateTime _rangeFromDate;
        private DateTime _rangeToDate;
        private bool _includeVoids;
        private bool _isDateEnabled;
        private int _selectedReportIndex;
        private int _selectedSortByIndex;
        private int _selectedCustomerTypeIndex;
        private int _selectedDetailSummaryIndex;

        public IReportFilter _reportFilter;
        private IReadOnlyList<IReportConfiguration> _reportConfigurationList;
        private IReportConfiguration _selectedReportConfiguration;

        private IReadOnlyList<ICompanyData> _companyList;
        private ICompanyData _selectedCompany;
        public PropertyFilterGroup _propertyFilterGroup;

        private List<ITruckOwnerInfo> _truckOwners;
        private string _truckOwnerFilter;
        private bool _isFilterGroupEnabled;
        private bool _isCustomerTypeEnabled;
        private bool _isVoidsIncludedEnabled;
        private bool _isDetailSummaryEnabled;
        private bool _isSortByEnabled;

        public List<ITruckOwnerInfo> TruckOwners
        {
            get { return _truckOwners; }
            set
            {
                if (Equals(value, _truckOwners)) return;
                _truckOwners = value;
                NotifyOfPropertyChange(() => TruckOwners);
            }
        }

        public string TruckOwnerFilter
        {
            get { return _truckOwnerFilter; }
            set
            {
                if (value == _truckOwnerFilter) return;
                _truckOwnerFilter = value;
                NotifyOfPropertyChange(() => TruckOwnerFilter);
            }
        }

        public string Errors
        {
            get { return _errors; }
            set
            {
                if (value == _errors) return;
                _errors = value;
                NotifyOfPropertyChange(() => Errors);
            }
        }

        #region Company

        public IReadOnlyList<ICompanyData> CompanyList
        {
            get { return _companyList; }
            set
            {
                if (Equals(value, _companyList)) return;
                _companyList = value;
                NotifyOfPropertyChange(() => CompanyList);
            }
        }

        public ICompanyData SelectedCompany
        {
            get { return _selectedCompany; }
            set
            {
                if (Equals(value, _selectedCompany)) return;
                _selectedCompany = value;
                NotifyOfPropertyChange(() => SelectedCompany);
            }
        }

        private void InitializeCompany()
        {
            var commonService = IoC.Get<ICommonService>();
            CompanyList = commonService.GetCompaniesData();
            ICompanyData company = CompanyList.FirstOrDefault();

            if (company != null)
            {
                SelectedCompany = company;
            }
        }

        #endregion

        #region Report Generator

        public DateTime RangeFromDate
        {
            get { return _rangeFromDate; }
            set
            {
                if (Equals(value, _rangeFromDate)) return;
                _rangeFromDate = value;
                NotifyOfPropertyChange(() => RangeFromDate);
            }
        }

        public Boolean IncludeVoids
        {
            get { return _includeVoids; }
            set
            {
                if (Equals(value, _includeVoids)) return;
                _includeVoids = value;
                NotifyOfPropertyChange(() => IncludeVoids);
            }
        }

        public Boolean IsDateEnabled
        {
            get { return _isDateEnabled; }
            set
            {
                if (Equals(value, _isDateEnabled)) return;
                _isDateEnabled = value;
                NotifyOfPropertyChange(() => IsDateEnabled);
            }
        }

        public DateTime RangeToDate
        {
            get { return _rangeToDate; }
            set
            {
                if (Equals(value, _rangeToDate)) return;
                _rangeToDate = value;
                NotifyOfPropertyChange(() => RangeToDate);
            }
        }

        public Boolean IsFilterGroupEnabled
        {
            get { return _isFilterGroupEnabled; }
            set
            {
                if (Equals(value, _isFilterGroupEnabled)) return;
                _isFilterGroupEnabled = value;
                NotifyOfPropertyChange(() => IsFilterGroupEnabled);
            }
        }

        public Boolean IsCustomerTypeEnabled
        {
            get { return _isCustomerTypeEnabled; }
            set
            {
                if (Equals(value, _isCustomerTypeEnabled)) return;
                _isCustomerTypeEnabled = value;
                NotifyOfPropertyChange(() => IsCustomerTypeEnabled);
            }
        }

        public Boolean IsVoidsIncludedEnabled
        {
            get { return _isVoidsIncludedEnabled; }
            set
            {
                if (Equals(value, _isVoidsIncludedEnabled)) return;
                _isVoidsIncludedEnabled = value;
                NotifyOfPropertyChange(() => IsVoidsIncludedEnabled);
            }
        }

        public Boolean IsDetailSummaryEnabled
        {
            get { return _isDetailSummaryEnabled; }
            set
            {
                if (Equals(value, _isDetailSummaryEnabled)) return;
                _isDetailSummaryEnabled = value;
                NotifyOfPropertyChange(() => IsDetailSummaryEnabled);
            }
        }

        public Boolean IsSortByEnabled
        {
            get { return _isSortByEnabled; }
            set
            {
                if (Equals(value, _isSortByEnabled)) return;
                _isSortByEnabled = value;
                NotifyOfPropertyChange(() => IsSortByEnabled);
            }
        }

        public int SelectedReportIndex
        {
            get { return _selectedReportIndex; }
            set
            {
                if (Equals(value, _selectedReportIndex)) return;
                _selectedReportIndex = value;
                NotifyOfPropertyChange(() => SelectedReportIndex);
            }
        }

        public int SelectedSortByIndex
        {
            get { return _selectedSortByIndex; }
            set
            {
                if (Equals(value, _selectedSortByIndex)) return;
                _selectedSortByIndex = value;
                NotifyOfPropertyChange(() => SelectedSortByIndex);
            }
        }

        public int SelectedCustomerTypeIndex
        {
            get { return _selectedCustomerTypeIndex; }
            set
            {
                if (Equals(value, _selectedCustomerTypeIndex)) return;
                _selectedCustomerTypeIndex = value;
                NotifyOfPropertyChange(() => SelectedCustomerTypeIndex);
            }
        }

        public int SelectedDetailSummaryIndex
        {
            get { return _selectedDetailSummaryIndex; }
            set
            {
                if (Equals(value, _selectedDetailSummaryIndex)) return;
                _selectedDetailSummaryIndex = value;
                NotifyOfPropertyChange(() => SelectedDetailSummaryIndex);
            }
        }

        public ReportDefintionEnum SelectedReport
        {
            get { return _selectedReport; }
            set
            {
                if (Equals(value, _selectedReport)) return;
                _selectedReport = value;
                NotifyOfPropertyChange(() => SelectedReport);
                InitializeReport();
            }
        }

        public ReportDateRangeEnum SelectedDateRange
        {
            get { return _selectedDateRange; }
            set
            {
                if (Equals(value, _selectedDateRange)) return;
                _selectedDateRange = value;
                NotifyOfPropertyChange(() => SelectedDateRange);
                SetDateRange();
            }
        }

        public IReadOnlyList<IReportConfiguration> ReportConfigurationList
        {
            get { return _reportConfigurationList; }
            set
            {
                if (Equals(value, _reportConfigurationList)) return;
                _reportConfigurationList = value;
                NotifyOfPropertyChange(() => ReportConfigurationList);
            }
        }

        public IReportConfiguration SelectedReportConfiguration
        {
            get { return _selectedReportConfiguration; }
            set
            {
                if (Equals(value, _selectedReportConfiguration)) return;
                _selectedReportConfiguration = value;
                NotifyOfPropertyChange(() => SelectedReportConfiguration);
                if (SelectedReportConfiguration != null && !String.IsNullOrEmpty(SelectedReportConfiguration.Name))
                {
                    var configuration = JsonConvert.DeserializeObject<ReportConfigurationData>(SelectedReportConfiguration.Configuration, GetJsonSerializerSettings());
                    RangeFromDate = configuration.RangeFromDate;
                    RangeToDate = configuration.RangeToDate;
                    SelectedDateRange = configuration.SelectedDateRange;
                    SelectedReport = configuration.SelectedReport;
                    SelectedSortBy = configuration.SelectedSortBy;
                    SelectedCustomerType = configuration.SelectedCustomerType;
                    SelectedDetailSummary = configuration.SelectedDetailSummary;
                    IncludeVoids = configuration.IncludeVoids;
                    var target = CollectionView.Filter.Target as PropertyFilterGroup ?? new PropertyFilterGroup();
                    var availableCriterias = target.Filters.FirstOrDefault().AvailableCriteria;
                    target.FiltersApplied.Clear();
                    if (configuration.Filters != null)
                    {
                        foreach (var filter in configuration.Filters)
                        {
                            var newFilter = new PropertyFilter(filter.PropertyName, filter.PropertyType, filter.PropertyConverter);
                            newFilter.DisplayName = filter.DisplayName;

                            var filterAppliedActiveCriterion = availableCriterias.FirstOrDefault(x => x.DisplayName == filter.ActiveCriterion.DisplayName);
                            newFilter.ActiveCriterion = filterAppliedActiveCriterion;

                            newFilter.Value = filter.Value;
                            target.FiltersApplied.Add(newFilter);
                        }
                    }
                }
                else
                {
                    InitializeGenerator();
                }
            }
        }

        public ReportSortByEnum SelectedSortBy
        {
            get { return _selectedSortBy; }
            set
            {
                if (Equals(value, _selectedSortBy)) return;
                _selectedSortBy = value;
                NotifyOfPropertyChange(() => SelectedSortBy);
            }
        }

        public CustomerTypeEnum SelectedCustomerType
        {
            get { return _selectedCustomerType; }
            set
            {
                if (Equals(value, _selectedCustomerType)) return;
                _selectedCustomerType = value;
                NotifyOfPropertyChange(() => SelectedCustomerType);
            }
        }

        public DetailSummaryEnum SelectedDetailSummary
        {
            get { return _selectedDetailSummary; }
            set
            {
                if (Equals(value, _selectedDetailSummary)) return;
                _selectedDetailSummary = value;
                NotifyOfPropertyChange(() => SelectedDetailSummary);
            }
        }

        public IReadOnlyList<ReportDefintionEnum> ReportList
        {
            get { return _reportList; }
            set
            {
                if (Equals(value, _reportList)) return;
                _reportList = value;
                NotifyOfPropertyChange(() => ReportList);
            }
        }

        public ReportDataSource ReportDataSource
        {
            get { return _reportDataSource; }
            set
            {
                if (Equals(value, _reportDataSource)) return;
                _reportDataSource = value;
                NotifyOfPropertyChange(() => ReportDataSource);
            }
        }

        public ICollectionView CollectionView
        {
            get { return _collectionView; }
            set
            {
                if (Equals(value, _collectionView)) return;
                _collectionView = value;
                NotifyOfPropertyChange(() => CollectionView);
            }
        }

        #region Events

        public ReportGeneratorViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            _log = LogManager.GetLog(typeof(ReportGeneratorViewModel));
            _seasonId = (int)Application.Current.Properties["SeasonId"];
            var selectedSite = IoC.Get<IConfiguration>().SelectedSite;
            var site = IoC.Get<ICommonService>().GetSite(selectedSite.Name, _seasonId);
            _siteId = site.Id;
            DisplayName = "Reporting";
            RangeFromDate = DateTime.Today;
            RangeToDate = DateTime.Today.AddHours(23).AddMinutes(59).AddSeconds(59);
        }

        public ReportGeneratorViewModel()
        {
        }

        public IReportFilter ReportFilterx { get; set; }

        protected override void OnInitialize()
        {
            InitializeCompany();
            InitializeReportConfigurationList();
            ReportFiltersApplied = new List<ReportFilterGroupApplied>();
            _reportFilter = new ReportFilter();
            _reportFilter.AppliedFilters = new List<ReportFilterGroupApplied>();
            base.OnInitialize();
        }

        protected override void OnActivate()
        {
            _eventAggregator.Subscribe(this);
            base.OnActivate();
        }

        protected override void OnDeactivate(bool close)
        {
            if (ReportGeneratorViewModel.Instance.ReportFiltersApplied != null)
            {
                ReportGeneratorViewModel.Instance.ReportFiltersApplied.Clear();
            }
            _reportFilter = null;

            _eventAggregator.Unsubscribe(this);
            base.OnDeactivate(close);
        }

        #endregion


        public void InitializeReport()
        {
            _reportFilter = null;

            IsSortByEnabled = true;
            switch (SelectedReport)
            {
                case ReportDefintionEnum.DispatcherReport:
                    IsDateEnabled = false;
                    SelectedDateRange = ReportDateRangeEnum.Today;

                    IsFilterGroupEnabled = true;
                    IsCustomerTypeEnabled = false;
                    IsVoidsIncludedEnabled = false;
                    IsDetailSummaryEnabled = true;
                    SelectedDetailSummary = DetailSummaryEnum.Detail;

                    IncludeVoids = false;
                    break;

                case ReportDefintionEnum.CustomerReport:
                    SelectedSortBy = ReportSortByEnum.Customer;
                    IsDateEnabled = true;
                    IsFilterGroupEnabled = true;
                    IsCustomerTypeEnabled = true;
                    IsVoidsIncludedEnabled = true;
                    IsDetailSummaryEnabled = true;
                    SelectedDetailSummary = DetailSummaryEnum.Detail;

                    IncludeVoids = false;
                    break;

                case ReportDefintionEnum.TicketReport:
                    SelectedSortBy = ReportSortByEnum.Ticket;
                    IsDateEnabled = true;
                    IsFilterGroupEnabled = true;
                    IsCustomerTypeEnabled = true;
                    IsVoidsIncludedEnabled = true;
                    IsDetailSummaryEnabled = true;
                    SelectedDetailSummary = DetailSummaryEnum.DetailAndSummary;

                    IncludeVoids = true;
                    break;

                case ReportDefintionEnum.TruckReport:
                    SelectedSortBy = ReportSortByEnum.Truck;
                    IsDateEnabled = true;
                    IsFilterGroupEnabled = true;
                    IsCustomerTypeEnabled = true;
                    IsVoidsIncludedEnabled = true;
                    IsDetailSummaryEnabled = true;
                    SelectedDetailSummary = DetailSummaryEnum.DetailAndSummary;

                    IncludeVoids = true;
                    break;

                case ReportDefintionEnum.InventoryReport:
                    SelectedSortBy = ReportSortByEnum.Material;
                    IsDateEnabled = true;
                    IsFilterGroupEnabled = true;
                    IsCustomerTypeEnabled = false;
                    IsVoidsIncludedEnabled = false;
                    IsDetailSummaryEnabled = false;
                    SelectedDetailSummary = DetailSummaryEnum.Detail;

                    IncludeVoids = false;
                    break;

                default:
                    IsDateEnabled = true;
                    IsFilterGroupEnabled = true;
                    IsCustomerTypeEnabled = true;
                    IsVoidsIncludedEnabled = true;
                    IsDetailSummaryEnabled = true;
                    break;

            }
        }

        public void InitializeGenerator()
        {
            SelectedDateRange = ReportDateRangeEnum.SeasontoToday;
            SelectedReport = 0;
            SelectedReportIndex = -1;
            IncludeVoids = true;
            SelectedCustomerType = CustomerTypeEnum.Both;

            if (CollectionView == null) return;

            var target = CollectionView.Filter.Target as PropertyFilterGroup ?? new PropertyFilterGroup();
            target.FiltersApplied.Clear();
        }

        public void InitializeReportConfigurationList()
        {
            ReportConfigurationList = IoC.Get<IReportService>().GetReportConfigurationList(_seasonId, _siteId);
            if (!_reportConfigurationList.Any()) return;

            var reportConfiguration =
                SelectedReportConfiguration != null ?
                    _reportConfigurationList.FirstOrDefault(x => x.Name == SelectedReportConfiguration.Name) :
                    _reportConfigurationList.FirstOrDefault();

            SelectedReportConfiguration = reportConfiguration;
        }

        public void SetDateRange()
        {
            switch (SelectedDateRange)
            {
                case ReportDateRangeEnum.MonthtoToday:
                    {
                        RangeFromDate = DateTimeDayOfMonthExtensions.FirstDayOfMonth(DateTime.Now);
                        RangeToDate = DateTime.Today.AddHours(23).AddMinutes(59).AddSeconds(59);

                        break;
                    }
                case ReportDateRangeEnum.MonthtoYesterday:
                    {
                        RangeFromDate = DateTimeDayOfMonthExtensions.FirstDayOfMonth(DateTime.Now);
                        RangeToDate = DateTime.Today.AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59);
                        break;
                    }
                case ReportDateRangeEnum.SeasontoToday:
                    {
                        ISeason season = IoC.Get<ICommonService>().GetSeasonById(_seasonId);
                        RangeFromDate = season.StartDate;
                        RangeToDate = DateTime.Today.AddHours(23).AddMinutes(59).AddSeconds(59);
                        break;
                    }
                case ReportDateRangeEnum.SeasontoYesterday:
                    {
                        ISeason season = IoC.Get<ICommonService>().GetSeasonById(_seasonId);
                        RangeFromDate = season.StartDate;
                        RangeToDate = DateTime.Today.AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59);
                        break;
                    }
                case ReportDateRangeEnum.WeektoToday:
                    {
                        RangeFromDate = DateTimeDayOfMonthExtensions.FirstDayOfWeek(DateTime.Now);
                        RangeToDate = DateTime.Today.AddHours(23).AddMinutes(59).AddSeconds(59);
                        break;
                    }
                case ReportDateRangeEnum.WeektoYesterday:
                    {
                        RangeFromDate = DateTimeDayOfMonthExtensions.FirstDayOfWeek(DateTime.Now);
                        RangeToDate = DateTime.Today.AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59);
                        break;
                    }
                case ReportDateRangeEnum.Today:
                    {
                        RangeFromDate = DateTime.Today;
                        RangeToDate = DateTime.Today.AddHours(23).AddMinutes(59).AddSeconds(59);
                        break;
                    }
                case ReportDateRangeEnum.Yesterday:
                    {
                        RangeFromDate = DateTime.Today.AddDays(-1);
                        RangeToDate = DateTime.Today.AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59);
                        break;
                    }

                case ReportDateRangeEnum.LastWeek:
                    {
                        RangeFromDate = DateTimeDayOfMonthExtensions.FirstDayOfWeek(DateTime.Now).AddDays(-7);
                        RangeToDate = DateTimeDayOfMonthExtensions.FirstDayOfWeek(DateTime.Now).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59);
                        break;
                    }

                case ReportDateRangeEnum.LastMonth:
                    {
                        RangeFromDate = DateTimeDayOfMonthExtensions.FirstDayOfMonth(DateTime.Now).AddMonths(-1);
                        RangeToDate = DateTimeDayOfMonthExtensions.LastDayOfMonth(DateTime.Now.AddMonths(-1)).AddHours(23).AddMinutes(59).AddSeconds(59);
                        break;
                    }

                default:
                    {
                        RangeFromDate = DateTime.Today;
                        RangeToDate = DateTime.Today.AddHours(23).AddMinutes(59).AddSeconds(59);
                        IsDateEnabled = true;
                        return;
                    }
            }
        }

        #endregion

        #region Actions


        public void KeyHandle(Key key)
        {
            if (key == Key.F2)
            {
                Done();
            }
        }

        public void Done()
        {
            _reportFilter = null;
            TryClose(false);
        }

        public void Cancel()
        {
            _reportFilter = null;
            TryClose(false);
        }

        public void Reset()
        {
            //ReportGeneratorViewModel.Instance.ReportFiltersApplied.Clear();
            _reportFilter = null;
            Refresh();
        }

        public void Preview()
        {
            try
            {
                GenerateReport();

                /*
                var windowManager = IoC.Get<IWindowManager>();
                var reportDisplayViewer = IoC.Get<ReportDisplayViewModel>();
                reportDisplayViewer.ReportFilterInUse = _reportFilter;
                windowManager.ShowWindow(reportDisplayViewer);
                */

                Reports.ReportDisplayViewer reportDisplayViewer = new Reports.ReportDisplayViewer(_reportFilter);

                reportDisplayViewer.ShowDialog();
                
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                System.Action action = () =>
                {
                    var messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "Preview of report failed with exception: \"{0}\"" + Environment.NewLine + "Inner exception message: \"{1}\"" + Environment.NewLine + "Contact support.  The error has also been logged.",
                            ex.Message, ex.InnerException.Message), "Error", MessageBoxButton.OK));
                };

                action.OnUIThreadAsync();
            }
        }

        public void Print()
        {
            try
            {
                //_reportViewerModel = IoC.Get<ReportViewerViewModel>();
                GenerateReport();
                Reports.ReportDisplayViewer reportDisplayViewer_DirectPrint = new Reports.ReportDisplayViewer(_reportFilter);

                // Print
                reportDisplayViewer_DirectPrint.ReportGenerate_ReportToPrint();
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                System.Action action = () =>
                {
                    var messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "Printing of report failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                            ex.Message), "Error", MessageBoxButton.OK));
                };

                action.OnUIThreadAsync();
            }
        }

        public string ReportFileName { get; set; }

        public void GenerateReport()
        {
            if (!ValidateReport()) return;

            //-- copied from previous GenerateReport
            /////var target = CollectionView.Filter.Target as PropertyFilterGroup ?? new PropertyFilterGroup();

            _reportFilter = new ReportFilter();

            _reportFilter.BeginDateTime = RangeFromDate;
            _reportFilter.EndDateTime = RangeToDate;
            _reportFilter.DetailSummary = (SelectedDetailSummary == 0) ? DetailSummaryEnum.Detail : SelectedDetailSummary;
            _reportFilter.CustomerType = (CustomerTypeEnum)SelectedCustomerType;
            _reportFilter.IsVoidsIncluded = IncludeVoids;
            _reportFilter.SiteId = _siteId;

            _reportFilter.AppliedFilters = ReportGeneratorViewModel.Instance.ReportFiltersApplied;

            // Determine if there are filters to process
            if (_reportFilter.AppliedFilters != null)
            {
                // Loop through the filters and setup a combined OR for multiple instances of the same column
                string tmpColumn;
                string tmpWhere;

                // Loop through the filters and convert them to CONTAINS and NUMERIC comparisions for processing
                for (int i = 0; i < _reportFilter.AppliedFilters.Count; i++)
                {
                    // Get the current column and SQL
                    if (_reportFilter.AppliedFilters[i].FilterSQLColumnOperatorValue.Trim() != "")
                    {
                        // Identify the column type and prep it for processing
                        tmpColumn = _reportFilter.AppliedFilters[i].FilterColumn;

                        // Identify the string content
                        string tmpFilter = _reportFilter.AppliedFilters[i].FilterSQLColumnOperatorValue;
                        int locMaskChar = tmpFilter.IndexOf("%");
                        int locMaskCharStarts = tmpFilter.IndexOf("%'");
                        int locMaskCharEnds = tmpFilter.IndexOf("'%");
                        int locMaskCharLike = tmpFilter.IndexOf("LIKE");

                        // Convert the filter to a SQL compatible string
                        // TICKET Starts With
                        if ((tmpColumn == "TicketNumber") && (locMaskCharStarts > -1))
                        {
                            _reportFilter.AppliedFilters[i].FilterSQLColumnOperatorValue = _reportFilter.AppliedFilters[i].FilterSQLColumnOperatorValue.Replace(" LIKE ", " >= ");
                            _reportFilter.AppliedFilters[i].FilterSQLColumnOperatorValue = _reportFilter.AppliedFilters[i].FilterSQLColumnOperatorValue.Replace("'%", "'");
                            _reportFilter.AppliedFilters[i].FilterSQLColumnOperatorValue = _reportFilter.AppliedFilters[i].FilterSQLColumnOperatorValue.Replace("%'", "'");
                        }
                        // TICKET Ends With
                        if ((tmpColumn == "TicketNumber") && (locMaskCharEnds > -1))
                        {
                            _reportFilter.AppliedFilters[i].FilterSQLColumnOperatorValue = _reportFilter.AppliedFilters[i].FilterSQLColumnOperatorValue.Replace(" LIKE ", " <= ");
                            _reportFilter.AppliedFilters[i].FilterSQLColumnOperatorValue = _reportFilter.AppliedFilters[i].FilterSQLColumnOperatorValue.Replace("'%", "'");
                            _reportFilter.AppliedFilters[i].FilterSQLColumnOperatorValue = _reportFilter.AppliedFilters[i].FilterSQLColumnOperatorValue.Replace("%'", "'");
                        }
                    }
                }

                for (int i = 0; i < _reportFilter.AppliedFilters.Count; i++)
                {
                    // Get the current column and SQL
                    if (_reportFilter.AppliedFilters[i].FilterSQLColumnOperatorValue.Trim() != "")
                    {
                        tmpColumn = _reportFilter.AppliedFilters[i].FilterColumn;
                        tmpWhere = _reportFilter.AppliedFilters[i].FilterSQLColumnOperatorValue.Substring(4, _reportFilter.AppliedFilters[i].FilterSQLColumnOperatorValue.Length - 4);

                        // Loop through the remaining rows and assemble them into the string
                        if (_reportFilter.AppliedFilters.Count > 1)
                        {
                            for (int j = i + 1; j < _reportFilter.AppliedFilters.Count; j++)
                            {
                                if ((_reportFilter.AppliedFilters[j].FilterColumn == tmpColumn) && (_reportFilter.AppliedFilters[j].FilterSQLColumnOperatorValue != ""))
                                {
                                    // If its the TICKET column then determine if it is a range
                                    if ((tmpColumn == "TicketNumber") && (_reportFilter.AppliedFilters[j].FilterSQLColumnOperatorValue.IndexOf("=>") > -1 || _reportFilter.AppliedFilters[j].FilterSQLColumnOperatorValue.IndexOf("<=") > -1))
                                    {
                                        tmpWhere = tmpWhere + " AND " + _reportFilter.AppliedFilters[j].FilterSQLColumnOperatorValue.Substring(4, _reportFilter.AppliedFilters[j].FilterSQLColumnOperatorValue.Length - 4);
                                    }
                                    else
                                    {
                                        tmpWhere = tmpWhere + " OR " + _reportFilter.AppliedFilters[j].FilterSQLColumnOperatorValue.Substring(4, _reportFilter.AppliedFilters[j].FilterSQLColumnOperatorValue.Length - 4);
                                    }
                                    _reportFilter.AppliedFilters[j].FilterSQLColumnOperatorValue = "";
                                }
                            }
                        }

                        // Assign the updated WHERE statement back to the filter
                        _reportFilter.AppliedFilters[i].FilterSQLColumnOperatorValue = " AND (" + tmpWhere + ") ";
                    }
                }
            }


            switch (SelectedReport)
            {
                case ReportDefintionEnum.CustomerReport:
                    _reportFilter.ReportFileName = "ReportCustomer.rpt";
                    _reportFilter.ReportDefinitionEnumValue = SelectedReport;
                    _reportFilter.SortBy = SelectedSortBy;
                    break;

                case ReportDefintionEnum.DispatcherReport:
                    _reportFilter.ReportFileName = "ReportDispatcher.rpt";
                    _reportFilter.ReportDefinitionEnumValue = SelectedReport;
                    break;

                case ReportDefintionEnum.TicketReport:
                    _reportFilter.ReportFileName = "ReportTicket.rpt";
                    _reportFilter.ReportDefinitionEnumValue = SelectedReport;
                    _reportFilter.SortBy = SelectedSortBy;
                    break;

                case ReportDefintionEnum.TruckReport:
                    _reportFilter.ReportFileName = "ReportTruck.rpt";
                    _reportFilter.ReportDefinitionEnumValue = SelectedReport;
                    _reportFilter.SortBy = SelectedSortBy;
                    break;

                case ReportDefintionEnum.InventoryReport:
                    _reportFilter.ReportFileName = "ReportInventory.rpt";
                    _reportFilter.ReportDefinitionEnumValue = SelectedReport;
                    _reportFilter.SortBy = SelectedSortBy;
                    break;

            }
        }

        private static JsonSerializerSettings GetJsonSerializerSettings()
        {
            var jsonSettings = new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor,
                NullValueHandling = NullValueHandling.Include,
                ObjectCreationHandling = ObjectCreationHandling.Auto,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };
            return jsonSettings;
        }

        public void SaveBottom()
        {
            Save();
        }

        public void Save()
        {
            try
            {
                if (!Validate())
                    return;

                if (SelectedReportConfiguration.Id == 0)
                {
                    var windowManager = IoC.Get<IWindowManager>();
                    var reportCongfigurationNameViewModel = IoC.Get<ReportCongfigurationNameViewModel>();
                    var result = windowManager.ShowDialog(reportCongfigurationNameViewModel);
                    if (result.HasValue && result.Value)
                        SelectedReportConfiguration.Name = reportCongfigurationNameViewModel.Name;

                    if (String.IsNullOrWhiteSpace(SelectedReportConfiguration.Name))
                    {
                        ShowMessage("Validation Error", "The Report Configuration Name cannot be left blank.", NotificationType.Warning);
                        SelectedReportConfiguration.Name = string.Empty;
                        return;
                    }
                }

                SelectedReportConfiguration.SiteId = _siteId;
                SelectedReportConfiguration.SeasonId = _seasonId;
                var jsonSettings = GetJsonSerializerSettings();
                var configuration = new ReportConfigurationData
                {
                    RangeFromDate = RangeFromDate,
                    RangeToDate = RangeToDate,
                    SelectedDateRange = SelectedDateRange,
                    SelectedSortBy = SelectedSortBy,
                    SelectedDetailSummary = SelectedDetailSummary,
                    SelectedReport = SelectedReport,
                    SelectedCustomerType = SelectedCustomerType,
                    IncludeVoids = IncludeVoids,
                };

                var target = CollectionView.Filter.Target as PropertyFilterGroup ?? new PropertyFilterGroup();
                // Clean & Save the new Filters
                if (configuration.Filters == null) configuration.Filters = new List<PropertyFilter>();
                configuration.Filters.Clear();
                foreach (var filter in target.FiltersApplied)
                {
                    var filterApplied = new PropertyFilter
                    {
                        DisplayName = filter.DisplayName,
                        DisplayType = filter.DisplayType,
                        PropertyName = filter.PropertyName,
                        PropertyType = filter.PropertyType,
                        PropertyConverter = filter.PropertyConverter,
                        Value = filter.Value,
                    };
                    filterApplied.ActiveCriterion = filterApplied.ActiveCriterion ?? new FilterCriterion(string.Empty);
                    filterApplied.ActiveCriterion.DisplayName = filter.ActiveCriterion == null
                        ? string.Empty
                        : filter.ActiveCriterion.DisplayName;
                    configuration.Filters.Add(filterApplied);
                }
                SelectedReportConfiguration.Configuration = JsonConvert.SerializeObject(configuration, jsonSettings);

                // Save Report Configuration
                IoC.Get<IReportService>().SaveReportConfiguration(SelectedReportConfiguration);
                _eventAggregator.PublishOnCurrentThread(new ReportConfigurationModifiedArgs(SelectedReportConfiguration.Id));
                var selectedReportConfigurationName = SelectedReportConfiguration.Name;
                InitializeReportConfigurationList();
                SelectedReportConfiguration = ReportConfigurationList.FirstOrDefault(x => String.CompareOrdinal(selectedReportConfigurationName, x.Name) == 0);
            }
            catch (UniqueConstraintException ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult = (MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };

                action.OnUIThreadAsync();
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "Report Configuration saving failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };

                action.OnUIThreadAsync();
            }
        }

        public void Delete()
        {
            if (!ValidateReportConfiguration())
                return;

            var message = string.Format("Are you sure you want to delete {0} Report?", SelectedReportConfiguration.Name);
            MessageBoxResult result = MessageBox.Show(message, "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                IoC.Get<IReportService>().DeleteReport(SelectedReportConfiguration.Id);
                InitializeGenerator();
            }
        }

        private bool Validate()
        {
            Errors = null;

            ICollection<ValidationResult> validationResults = new Collection<ValidationResult>();
            bool result = Validator.TryValidateObject(SelectedReportConfiguration, new ValidationContext(SelectedReportConfiguration), validationResults);
            if (SelectedReport == 0)
            {
                validationResults.Add(new ValidationResult("Please select a Type of Report"));
            }

            var messages = validationResults.ToList();

            if (!result || messages.Any())
            {
                Errors = string.Format("Validation Failed.{0}{1}", Environment.NewLine,
                    string.Join("\n", messages.Select(v => v.ErrorMessage)));
                ShowMessage("Validation Errors", Errors, NotificationType.Warning);

                return false;
            }

            return true;
        }

        private bool ValidateReport()
        {
            Errors = null;

            ICollection<ValidationResult> validationResults = new Collection<ValidationResult>();

            //Validate Report Type
            if (SelectedReport == 0)
            {
                validationResults.Add(new ValidationResult("You have to select a valid Report Definition"));
            }

            var messages = validationResults.ToList();

            if (messages.Any())
            {
                Errors = string.Format("Validation Failed.{0}{1}", Environment.NewLine,
                    string.Join("\n", messages.Select(v => v.ErrorMessage)));
                ShowMessage("Validation Errors", Errors, NotificationType.Warning);

                return false;
            }

            return true;
        }

        private bool ValidateReportConfiguration()
        {
            Errors = null;

            ICollection<ValidationResult> validationResults = new Collection<ValidationResult>();
            bool result = true;

            //Validate Report Config
            if (SelectedReportConfiguration == null)
            {
                validationResults.Add(new ValidationResult("Must Select a valid Report Configuration."));
            }

            var messages = validationResults.ToList();

            if (!result || messages.Any())
            {
                Errors = string.Format("Validation Failed.{0}{1}", Environment.NewLine,
                    string.Join("\n", messages.Select(v => v.ErrorMessage)));
                ShowMessage("Validation Errors", Errors, NotificationType.Warning);

                return false;
            }

            return true;
        }

        #endregion

        #region VO

        public class ReportConfigurationData
        {
            public ReportDefintionEnum SelectedReport { get; set; }

            public ReportDateRangeEnum SelectedDateRange { get; set; }

            public ReportSortByEnum SelectedSortBy { get; set; }

            public CustomerTypeEnum SelectedCustomerType { get; set; }
            public DetailSummaryEnum SelectedDetailSummary { get; set; }

            public DateTime RangeFromDate { get; set; }

            public DateTime RangeToDate { get; set; }

            public List<PropertyFilter> Filters { get; set; }

            public bool IncludeVoids { get; set; }
        }

        #endregion
    }
}