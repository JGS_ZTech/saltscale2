﻿using System;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using Caliburn.Micro;
using Contract.Domain;
using Contract.Events;
using Contract.Services;
using ZTech.Helpers;
using System.ComponentModel;

namespace ZTech.ViewModels
{
    public class ShellViewModel : Conductor<object>, IHandle<DefaultScaleChangedArgs>, IHandle<ChangeSiteArgs>, IHandle<SeasonChangeArgs>, IHandle<AuthArgs>, IHandle<TicketAddedVoidedArgs>
    {
        private ISeason _selectedSeason;
        private readonly IEventAggregator _eventAggregator;
        private string _pcName;
        private string _siteName;
        private string _siteConnectionString;
        private string _siteNameDisplay;
        private string _serverName;
        private string _scaleName;
        private string _defaultScaleName;
        private string _defaultPrinterName;
        private string _versionName;
        private string _shortScaleName;
        private string _shortPcName;
        private string _shortVersionName;
        private string _shortServerName;
        private string _shortSiteName;
        private bool _isAuthorized;
        private bool _isDefaultSite;
        private bool _isBusy;
        private ISiteConfig _ownSite;
        private bool _isSiteChangeMenuEnabled;
        private readonly ILog _log;
        private string _tonsShippedToday;
        private string _userName;
        private string _weighMasterName;
        private string _weighMasterNameOverride;
        private string _DefaultSiteColor = "Black";
        public ScaleWidgetViewModel _scaleWidget;

        private IConfiguration _configuration;

        private bool _isVisibleCustomerService;
        private bool _isVisibleCashCustomer;
        private bool _isVisibleMaterials;
        private bool _isVisibleAdministrator;
        private bool _isVisibleAccounting;
        private bool _isVisibleSiteChange;
        private bool _isVisibleScaleSettings;
        private bool _isVisibleReports;
        private bool _isVisibleOrderCreateEditDelete;
        private bool _isVisibleSeasonChange;

        public DateTime appStartDateTime = DateTime.Now;
        public TimeSpan timeRunning;
        public System.Windows.Forms.Timer elapsedTime;
        public string _menuBackgroundColor;

        public ShellViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            _log = LogManager.GetLog(typeof(ShellViewModel));
        }

        protected override void OnInitialize()
        {
            base.OnInitialize();

            var seasons = IoC.Get<ICommonService>().GetSeasons();
            var season = seasons.FirstOrDefault(s => s.IsActive) ??
                         seasons.OrderByDescending(s => s.StartDate).FirstOrDefault();

            if (season == null)
                throw new Exception("No seasons defined.");

            SelectedSeason = season;
            Application.Current.Properties["SeasonId"] = season.SeasonId;

            PcName = Environment.MachineName;
            ShortPcName = Truncate(Environment.MachineName, 8);

            _configuration = IoC.Get<IConfiguration>();

            MenuBackgroundColor = "#FFF6F6F6";
            SiteName = _configuration.SelectedSite.Name;
            SiteConnectionString = _configuration.SelectedSite.ConnectionString;
            ShortSiteName = Truncate(_configuration.SelectedSite.Name, 8);
            IsDefaultSite = _configuration.SelectedSite.IsDefault;

            // Update the caption for the window and the menu panel
            timeRunning = (DateTime.Now.AddSeconds(1) - appStartDateTime);
            if (SiteConnectionString != null)
            {
                if (SiteConnectionString.IndexOf("catalog=repl_") > 1)
                {
                    DisplayName = SiteName + "(Replicated in Lowell)   Running Time : " + timeRunning.ToString(@"hh\:mm\:ss") + ")";
                    SiteNameDisplay = "rpl" + SiteName;
                }
                else
                {
                    DisplayName = SiteName + "(Real-time)   Running Time : " + timeRunning.ToString(@"hh\:mm\:ss") + ")";                   
                    SiteNameDisplay = SiteName;
                }
            }
            else
            {
                DisplayName = SiteName + "(Real-time)   Running Time : " + timeRunning.ToString(@"hh\:mm\:ss") + ")";
                SiteNameDisplay = SiteName;
            }


            var sqlBuilder = new SqlConnectionStringBuilder(_configuration.SelectedSite.ConnectionString);
            ServerName = sqlBuilder.DataSource;
            ShortServerName = Truncate(sqlBuilder.DataSource, 8);

            ScaleName = _configuration.DefaultScaleName;
            DefaultScaleName = ScaleName;
            if (_configuration.IsManualMode || string.IsNullOrWhiteSpace(DefaultScaleName))
                ScaleName = "Manual";

            ShortScaleName = Truncate(ScaleName, 8);

            Assembly assembly = Assembly.GetExecutingAssembly();
            ShortVersionName = VersionName = Helpers.ProgramInformationUtility.GetPublishedVersion();

            IsAuthorized = (bool) Application.Current.Properties["IsLogged"];
            UserName = Application.Current.Properties["UserName"].ToString();
            Application.Current.Properties["DisptcherOrderColumnSortName"] = Application.Current.Properties["DisptcherOrderColumnSortName"] ?? "Customer";
            Application.Current.Properties["DisptcherOrderColumnSortDirection"] = ListSortDirection.Ascending;

            IoC.Get<IUserService>().DefaultUser();
            UserAuthorizationPermissionsCheck();

            OwnSite = _configuration.Site;
            IsSiteChangeMenuEnabled = _configuration.ExistingSites.Count > 1;

            TonsShippedToday = IoC.Get<ITicketService>().GetTonsShippedToday(season.SeasonId).ToString("N2");

            ShowDispatcher();

            // Start the time
            elapsedTime = new System.Windows.Forms.Timer();
            elapsedTime.Enabled = true;
            elapsedTime.Interval = 1000;
            elapsedTime.Tick += new EventHandler(elapsedTime_Tick);
            elapsedTime.Start();

            //// Initialize the scale
            //InitializeScale();
        }


        // Process a tick
        public void elapsedTime_Tick(object sender, EventArgs e)
        {
            // Local variables
            string tempDisplayName;
            int sloc;

            // Update Running Time
            timeRunning = (DateTime.Now.AddSeconds(1) - appStartDateTime);

            // Update the caption running time
            tempDisplayName = DisplayName;
            sloc = tempDisplayName.IndexOf("   Running Time : ");

            // If both locations are greater than 1 repace the time - Default color is "#FFF6F6F6";
            if (sloc > 1)
            {
                DisplayName = tempDisplayName.Substring(0, sloc + 18) + timeRunning.ToString(@"hh\:mm\:ss");
                if (timeRunning.TotalSeconds > 14400)
                {
                    DisplayName = tempDisplayName.Substring(0, sloc + 18) + timeRunning.ToString(@"hh\:mm\:ss") + "  -- Running over 4 hours --";
                    MenuBackgroundColor = "Yellow";
                }
                if (timeRunning.TotalSeconds > 21600)
                {
                    DisplayName = tempDisplayName.Substring(0, sloc + 18) + timeRunning.ToString(@"hh\:mm\:ss") + "  -- Running over 6 hours --";
                    MenuBackgroundColor = "Gold";
                }
                if (timeRunning.TotalSeconds > 28800)
                {
                    if (UserName.Trim().Length > 0)
                        DisplayName = (tempDisplayName.Substring(0, sloc + 18) + timeRunning.ToString(@"hh\:mm\:ss") + "  -- Running over 8 hours " + UserName + "! --");
                    else
                        DisplayName = (tempDisplayName.Substring(0, sloc + 18) + timeRunning.ToString(@"hh\:mm\:ss") + "  -- Running over 8 hours! --");
                    MenuBackgroundColor = "Orange";
                }
                if (timeRunning.TotalSeconds > 36000)
                {
                    if (UserName.Trim().Length > 0)
                        DisplayName = (tempDisplayName.Substring(0, sloc + 18) + timeRunning.ToString(@"hh\:mm\:ss") + "  -- Running over 10 hours " + UserName + "! --");
                    else
                        DisplayName = (tempDisplayName.Substring(0, sloc + 18) + timeRunning.ToString(@"hh\:mm\:ss") + "  -- Running over 10 hours!! --");
                    MenuBackgroundColor = "Red";
                }
            }
        }

        private void UserAuthorizationPermissionsCheck()
        {
            IsVisibleCustomerService = UserAuthorization.UserAuthorizationLoggedIn.IsAuthorizedCustomerService;
            IsVisibleCashCustomer = UserAuthorization.UserAuthorizationLoggedIn.IsAuthorizedCashCustomer;
            IsVisibleMaterials = UserAuthorization.UserAuthorizationLoggedIn.IsAuthorizedMaterials;
            IsVisibleAdministrator = UserAuthorization.UserAuthorizationLoggedIn.IsAuthorizedAdministrator;
            IsVisibleAccounting = UserAuthorization.UserAuthorizationLoggedIn.IsAuthorizedAccounting;
            IsVisibleSiteChange = UserAuthorization.UserAuthorizationLoggedIn.IsAuthorizedSiteChange;
            IsVisibleScaleSettings = UserAuthorization.UserAuthorizationLoggedIn.IsAuthorizedScaleSettings;
            IsVisibleReports = UserAuthorization.UserAuthorizationLoggedIn.IsAuthorizedReports;
            IsVisibleOrderCreateEditDelete = UserAuthorization.UserAuthorizationLoggedIn.IsAuthorizedOrderCreateEditDelete;
        }


        private bool IsNotLoggedInAndNotLowell()
        {
            return !IsAuthorized && _configuration.DefaultSiteName != "Lowell";
        }

        public ScaleWidgetViewModel ScaleWidget
        {
            get { return _scaleWidget; }
            set
            {
                if (Equals(value, _scaleWidget)) return;
                _scaleWidget = value;
                NotifyOfPropertyChange(() => ScaleWidget);
            }
        }

        private void InitializeScale()
        {
            ScaleWidget = IoC.Get<ScaleWidgetViewModel>();
            ScaleWidget.ActivateWith(this);
        }

        public bool IsSiteChangeMenuEnabled
        {
            get { return _isSiteChangeMenuEnabled; }
            set
            {
                if (value.Equals(_isSiteChangeMenuEnabled)) return;
                _isSiteChangeMenuEnabled = value;
                NotifyOfPropertyChange(() => IsSiteChangeMenuEnabled);
            }
        }
        
        public ISiteConfig OwnSite
        {
            get { return _ownSite; }
            set
            {
                if (Equals(value, _ownSite)) return;
                _ownSite = value;
                NotifyOfPropertyChange(() => OwnSite);
            }
        }

        public bool IsAuthorized
        {
            get { return _isAuthorized; }
            set
            {
                if (value.Equals(_isAuthorized)) return;
                _isAuthorized = value;
                NotifyOfPropertyChange(() => IsAuthorized);
            }
        }
        #region Site and Computer info

        public string ShortVersionName
        {
            get { return _shortVersionName; }
            set
            {
                if (value == _shortVersionName) return;
                _shortVersionName = value;
                NotifyOfPropertyChange(() => ShortVersionName);
            }
        }

        public string ShortServerName
        {
            get { return _shortServerName; }
            set
            {
                if (value == _shortServerName) return;
                _shortServerName = value;
                NotifyOfPropertyChange(() => ShortServerName);
            }
        }

        public string ShortSiteName
        {
            get { return _shortSiteName; }
            set
            {
                if (value == _shortSiteName) return;
                _shortSiteName = value;
                NotifyOfPropertyChange(() => ShortSiteName);
            }
        }

        public bool IsDefaultSite
        {
            get { return _isDefaultSite; }
            set
            {
                if (value.Equals(_isDefaultSite)) return;
                _isDefaultSite = value;
                NotifyOfPropertyChange(() => IsDefaultSite);

                if (IsDefaultSite)
                    DefaultSiteColor = "Black";
                else
                    DefaultSiteColor = "Red";
            }
        }

        public string DefaultSiteColor
        {
            get { return _DefaultSiteColor; }
            set
            {
                if (value.Equals(_DefaultSiteColor)) return;
                _DefaultSiteColor = value;
                NotifyOfPropertyChange(() => DefaultSiteColor);
            }
        }

        public string ShortPcName
        {
            get { return _shortPcName; }
            set
            {
                if (value == _shortPcName) return;
                _shortPcName = value;
                NotifyOfPropertyChange(() => ShortPcName);
            }
        }

        public string ShortScaleName
        {
            get { return _shortScaleName; }
            set
            {
                if (value == _shortScaleName) return;
                _shortScaleName = value;
                NotifyOfPropertyChange(() => ShortScaleName);
            }
        }

        public ISeason SelectedSeason
        {
            get { return _selectedSeason; }
            set
            {
                if (Equals(value, _selectedSeason)) return;
                _selectedSeason = value;
                NotifyOfPropertyChange(() => SelectedSeason);
            }
        }

       public string PcName
        {
            get { return _pcName; }
            set
            {
                if (value == _pcName) return;
                _pcName = value;
                NotifyOfPropertyChange(() => PcName);
            }
        }

        public string SiteName
        {
            get { return _siteName; }
            set
            {
                if (value == _siteName) return;
                _siteName = value;
                NotifyOfPropertyChange(() => SiteName);
                
            }
        }

        public string SiteNameDisplay
        {
            get { return _siteNameDisplay; }
            set
            {
                if (value == _siteNameDisplay) return;
                _siteNameDisplay = value;
                NotifyOfPropertyChange(() => SiteNameDisplay);

            }
        }

        public string SiteConnectionString
        {
            get { return _siteConnectionString; }
            set
            {
                if (value == _siteConnectionString) return;
                _siteConnectionString = value;
                NotifyOfPropertyChange(() => SiteConnectionString);

            }
        }

        public string ServerName
        {
            get { return _serverName; }
            set
            {
                if (value == _serverName) return;
                _serverName = value;
                NotifyOfPropertyChange(() => ServerName);
            }
        }

        public string ScaleName
        {
            get { return _scaleName; }
            set
            {
                if (value == _scaleName) return;
                _scaleName = value;
                NotifyOfPropertyChange(() => ScaleName);
            }
        }

        public string DefaultScaleName
        {
            get { return _defaultScaleName; }
            set
            {
                if (value == _defaultScaleName) return;
                _defaultScaleName = value;
                NotifyOfPropertyChange(() => DefaultScaleName);
            }
        }

        public string DefaultPrinterName
        {
            get { return _defaultPrinterName; }
            set
            {
                if (value == _defaultPrinterName) return;
                _defaultPrinterName = value;
                NotifyOfPropertyChange(() => DefaultPrinterName);
            }
        }

        public string VersionName
        {
            get { return _versionName; }
            set
            {
                if (value == _versionName) return;
                _versionName = value;
                NotifyOfPropertyChange(() => VersionName);
            }
        }
#endregion

        public string UserName
        {
            get
            {
                return _userName;
            }
            set
            {
                if (value == _userName) return;
                _userName = value;
                NotifyOfPropertyChange(() => UserName);

                // Store the override value
                Application.Current.Properties["WeighMasterNameOverride"] = value;

                // Default at Login
                _weighMasterName = value;
                NotifyOfPropertyChange(() => WeighMaster);
                _weighMasterNameOverride = value;
                NotifyOfPropertyChange(() => WeighMasterOverride);
            }
        }

        public string WeighMaster
        {
            get
            {
                return _weighMasterName;
            }
            set
            {
                if (value == _weighMasterName) return;
                _weighMasterName = value;
                _weighMasterNameOverride = value;
                NotifyOfPropertyChange(() => WeighMaster);

                // Store the override value
                Application.Current.Properties["WeighMasterNameOverride"] = value;
            }
        }

        public string WeighMasterOverride
        {
            get
            {
                return _weighMasterNameOverride;
            }
            set
            {
                if (value == _weighMasterNameOverride) return;
                _weighMasterNameOverride = value;
                NotifyOfPropertyChange(() => WeighMasterOverride);

                // Store the override value
                Application.Current.Properties["WeighMasterNameOverride"] = value;
            }
        }

        public void ShowDispatcher()
        {
            // Launch as thread
            IsBusy = true;
            var task = new Task(ShowDispatcherTask);
            task.Start();
            //// Don't launch as thread
            //ShowDispatcherTask();
            ////IsBusy = false;
        }

        public void LogIn()
        {
            var windowManager = IoC.Get<IWindowManager>();

            if (!(bool)Application.Current.Properties["IsLogged"])
            {
                var viewModel = IoC.Get<LoginViewModel>();
                var dialogResult = windowManager.ShowDialog(viewModel);
                if (!dialogResult.HasValue || !dialogResult.Value || !viewModel.IsAuthorized) return;
            }
        }

        public void LogOut()
        {
            Application.Current.Properties["UserName"] = String.Empty;
            Application.Current.Properties["IsLogged"] = false;
            IsAuthorized = false;
            UserName = String.Empty;

            IoC.Get<IUserService>().DefaultUser();

            _eventAggregator.PublishOnUIThreadAsync(new AuthArgs());
        }

        private void ShowDispatcherTask()
        {
            try
            {
                var viewModel = IoC.Get<DispatcherViewModel>();
                viewModel.Activated += MainViewModelOnActivated;

                // Update the caption for the window
                timeRunning = (DateTime.Now.AddSeconds(1) - appStartDateTime);
                if (SiteConnectionString.IndexOf("catalog=repl_") > 1)
                {
                    DisplayName = SiteName + "(Replicated in Lowell)   Running Time : " + timeRunning.ToString(@"hh\:mm\:ss") + ")";
                    SiteNameDisplay = "rpl" + SiteName;
                }
                else
                {
                    DisplayName = SiteName + "(Real-time)   Running Time : " + timeRunning.ToString(@"hh\:mm\:ss") + ")";
                    SiteNameDisplay = SiteName;
                }
                ActivateItem(viewModel);
                var trackingService = IoC.Get<IOrderTrackingService>();
                trackingService.StartTracking();

            }
            catch (Exception ex)
            {
                _log.Error(ex);
                IsBusy = false;
                throw;
            }
        }

        private void MainViewModelOnActivated(object sender, ActivationEventArgs activationEventArgs)
        {
            IsBusy = false;
        }

        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                if (value.Equals(_isBusy)) return;
                _isBusy = value;
                NotifyOfPropertyChange(() => IsBusy);
            }
        }

        public void ShowCustomerService()
        {
            // Launch as thread
            var task = new Task(ShowCustomerServiceTask);
            IsBusy = true;
            task.Start();
            //// Don't launch as a thread
            //ShowCustomerServiceTask();
        }

        private void ShowCustomerServiceTask()
        {
            try
            {
                var viewModel = IoC.Get<CustomerServiceViewModel>();
                viewModel.Activated += MainViewModelOnActivated;

                timeRunning = (DateTime.Now.AddSeconds(1) - appStartDateTime);
                if (SiteConnectionString.IndexOf("catalog=repl_") > 1)
                {
                    DisplayName = SiteName + "(Replicated in Lowell)   Running Time : " + timeRunning.ToString(@"hh\:mm\:ss") + ")";
                    SiteNameDisplay = "rpl" + SiteName;
                }
                else
                {
                    DisplayName = SiteName + "(Real-time)   Running Time : " + timeRunning.ToString(@"hh\:mm\:ss") + ")";
                    SiteNameDisplay = SiteName;
                }
                ActivateItem(viewModel);
                
                var trackingService = IoC.Get<IOrderTrackingService>();
                trackingService.StopTracking();
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                IsBusy = false;
                throw;
            }
        }

        public void ShowCashCustomerScreen()
        {
            // Pause for a random number of milliseconds from 500 to 1000.
            Random random = new Random();
            int randomNumber = random.Next(0, 500) + 640;
            System.Threading.Thread.Sleep(randomNumber);

            // Setup Cash Screen
            var windowManager = IoC.Get<IWindowManager>();
            if (IsNotLoggedInAndNotLowell())
            {
                var viewModel = IoC.Get<LoginViewModel>();
                var dialogResult = windowManager.ShowDialog(viewModel);
                if (!dialogResult.HasValue || !dialogResult.Value || !viewModel.IsAuthorized) return;
            }

            var cashCustomerViewModel = IoC.Get<CashCustomerViewModel>();
            windowManager.ShowDialog(cashCustomerViewModel);
            cashCustomerViewModel = null;  // 20190225, JGS - Make certain objects opened as windows are set to null
        }
        public void ShowExportCustomersScreen()
        {
            var windowManager = IoC.Get<IWindowManager>();
            var viewModel = IoC.Get<CustomerExportViewModel>();
            
            windowManager.ShowDialog(viewModel);
            //viewModel.Deactivate();  // 20190225, JGS - Make certain objects opened as windows are deactivated
            viewModel = null;  // 20190225, JGS - Make certain objects opened as windows are set to null
        }

        public void ShowMaterialScreen()
        {
            var windowManager = IoC.Get<IWindowManager>();
            var viewModel = IoC.Get<MaterialViewModel>();

            windowManager.ShowDialog(viewModel);
            //viewModel.Deactivate();  // 20190225, JGS - Make certain objects opened as windows are deactivated
            viewModel = null;  // 20190225, JGS - Make certain objects opened as windows are set to null
        }

        public void ShowPhoneBookScreen()
        {
            var windowManager = IoC.Get<IWindowManager>();
            var viewModel = IoC.Get<PhoneBookViewModel>();

            windowManager.ShowDialog(viewModel);
            //viewModel.Deactivate();  // 20190225, JGS - Make certain objects opened as windows are deactivated
            viewModel = null;  // 20190225, JGS - Make certain objects opened as windows are set to null
        }

        public void ShowAdministrationScreen()
        {
            var windowManager = IoC.Get<IWindowManager>();
            
            if (IsNotLoggedInAndNotLowell())
            {
                var viewModel = IoC.Get<LoginViewModel>();
                var dialogResult = windowManager.ShowDialog(viewModel);

                if (!dialogResult.HasValue || !dialogResult.Value || !viewModel.IsAuthorized) return;
            }

            var adminViewModel = IoC.Get<AdministrationViewModel>();
            windowManager.ShowDialog(adminViewModel);
            //adminViewModel.Deactivate();  // 20190225, JGS - Make certain objects opened as windows are deactivated
            adminViewModel = null;  // 20190225, JGS - Make certain objects opened as windows are set to null
        }

        public void ShowDailyOperationsScreen()
        {
            var windowManager = IoC.Get<IWindowManager>();
            var viewModel = IoC.Get<DailyOperationViewModel>();
            windowManager.ShowDialog(viewModel);
            //viewModel.Deactivate();  // 20190225, JGS - Make certain objects opened as windows are deactivated
            viewModel = null;  // 20190225, JGS - Make certain objects opened as windows are set to null
        }

        public void ShowChangeSiteScreen()
        {
            var windowManager = IoC.Get<IWindowManager>();

            if (IsNotLoggedInAndNotLowell())
            {
                var viewModel = IoC.Get<LoginViewModel>();
                var dialogResult = windowManager.ShowDialog(viewModel);

                if (!dialogResult.HasValue || !dialogResult.Value || !viewModel.IsAuthorized) return;
            }

            var changeSiteViewModel = IoC.Get<ChangeSiteViewModel>();
            windowManager.ShowDialog(changeSiteViewModel);
            //changeSiteViewModel.Deactivate();  // 20190225, JGS - Make certain objects opened as windows are deactivated

            // Get the information for the selected site
            SiteConnectionString  = changeSiteViewModel.SelectedSite.ConnectionString;

            // 20190225, JGS - Make certain objects opened as windows are set to null
            changeSiteViewModel = null;  // 20190225, JGS - Make certain objects opened as windows are set to null

            // Update the caption for the window
            timeRunning = (DateTime.Now.AddSeconds(1) - appStartDateTime);
            if (SiteConnectionString.IndexOf("catalog=repl_") > 1)
            {
                DisplayName = SiteName + "(Replicated in Lowell)   Running Time : " + timeRunning.ToString(@"hh\:mm\:ss") + ")";
                SiteNameDisplay = "rpl" + SiteName;
            }
            else
            {
                DisplayName = SiteName + "(Real-time)   Running Time : " + timeRunning.ToString(@"hh\:mm\:ss") + ")";
                SiteNameDisplay = SiteName;
            }
        }
        public void ShowChangeSeasonScreen()
        {
            //// Do not allow changing of the season
            //var windowManager = IoC.Get<IWindowManager>();
            //if (IsNotLoggedInAndNotLowell())
            //{
            //    var viewModel = IoC.Get<LoginViewModel>();
            //    var dialogResult = windowManager.ShowDialog(viewModel);
            //    if (!dialogResult.HasValue || !dialogResult.Value || !viewModel.IsAuthorized) return;
            //}
        
            //var changeSeasonViewModel = IoC.Get<ChangeSeasonViewModel>();
            //windowManager.ShowDialog(changeSeasonViewModel);
        }

        public void ShowScalesScreen()
        {
            var windowManager = IoC.Get<IWindowManager>();
            var viewModel = IoC.Get<ScalesViewModel>();

            windowManager.ShowDialog(viewModel);
            //viewModel.Deactivate();  // 20190225, JGS - Make certain objects opened as windows are deactivated
            viewModel = null;  // 20190225, JGS - Make certain objects opened as windows are set to null
        }

        public void ShowReportsScreen()
        {
            /*
            if (OwnSite.Name == "Lowell")
            {
                MessageBox.Show("No Orders or Tickets at this location to report on");
            }
            else
            { 
                var windowManager = IoC.Get<IWindowManager>();
                var viewModel = IoC.Get<ReportGeneratorViewModel>();

                windowManager.ShowDialog(viewModel);
            }
            */

            var windowManager = IoC.Get<IWindowManager>();
            var viewModel = IoC.Get<ReportGeneratorViewModel>();
            windowManager.ShowDialog(viewModel);
            //viewModel.Deactivate();  // 20190225, JGS - Make certain objects opened as windows are deactivated
            viewModel = null;  // 20190225, JGS - Make certain objects opened as windows are set to null
        }

        public void ShowGpCustomerScreen()
        {
            var windowManager = IoC.Get<IWindowManager>();
            var viewModel = IoC.Get<GPCustomerViewModel>();
            windowManager.ShowDialog(viewModel);
            //viewModel.Deactivate();  // 20190225, JGS - Make certain objects opened as windows are deactivated
            viewModel = null;  // 20190225, JGS - Make certain objects opened as windows are set to null
        }
        public void ShowTicketExportScreen()
        {
            var windowManager = IoC.Get<IWindowManager>();
            var viewModel = IoC.Get<TicketExportViewModel>();
            windowManager.ShowDialog(viewModel);
            //viewModel.Deactivate();  // 20190225, JGS - Make certain objects opened as windows are deactivated
            viewModel = null;  // 20190225, JGS - Make certain objects opened as windows are set to null
        }

        public void ShowSeasonEstimateScreen()
        {
            var windowManager = IoC.Get<IWindowManager>();
            var viewModel = IoC.Get<SeasonEstimateViewModel>();
            windowManager.ShowDialog(viewModel);
            //viewModel.Deactivate();  // 20190225, JGS - Make certain objects opened as windows are deactivated
            viewModel = null;  // 20190225, JGS - Make certain objects opened as windows are set to null
        }

        public void Exit()
        {
            Application.Current.Shutdown();
        }

        private string Truncate(string item, int length)
        {
            if (item.Length <= length)
                return item;
            return string.Format("{0}...", item.TruncateTo(length));
        }

        public string TonsShippedToday
        {
            get { return _tonsShippedToday; }
            set
            {
                if (value.Equals(_tonsShippedToday)) return;
                _tonsShippedToday = value;
                NotifyOfPropertyChange(() => TonsShippedToday);
            }
        }

        public string MenuBackgroundColor
        {
            get { return _menuBackgroundColor; }
            set
            {
                if (Equals(value, _menuBackgroundColor)) return;
                _menuBackgroundColor = value;
                NotifyOfPropertyChange(() => MenuBackgroundColor);
            }
        }

        #region Navigation Visibility
        public bool IsVisibleCustomerService
        {
            get { return _isVisibleCustomerService; }
            set
            {
                if (Equals(value, _isVisibleCustomerService)) return;
                _isVisibleCustomerService = value;
                NotifyOfPropertyChange(() => IsVisibleCustomerService);
            }
        }

        public bool IsVisibleCashCustomer
        {
            get { return _isVisibleCashCustomer; }
            set
            {
                if (Equals(value, _isVisibleCashCustomer)) return;
                _isVisibleCashCustomer = value;
                NotifyOfPropertyChange(() => IsVisibleCashCustomer);
            }
        }

        public bool IsVisibleMaterials
        {
            get { return _isVisibleMaterials; }
            set
            {
                if (Equals(value, _isVisibleMaterials)) return;
                _isVisibleMaterials = value;
                NotifyOfPropertyChange(() => IsVisibleMaterials);
            }
        }

        public bool IsVisibleAdministrator
        {
            get { return _isVisibleAdministrator; }
            set
            {
                if (Equals(value, _isVisibleAdministrator)) return;
                _isVisibleAdministrator = value;
                NotifyOfPropertyChange(() => IsVisibleAdministrator);
            }
        }

        public bool IsVisibleAccounting
        {
            get { return _isVisibleAccounting; }
            set
            {
                if (Equals(value, _isVisibleAccounting)) return;
                _isVisibleAccounting = value;
                NotifyOfPropertyChange(() => IsVisibleAccounting);
            }
        }

        public bool IsVisibleSiteChange
        {
            get { return _isVisibleSiteChange; }
            set
            {
                if (Equals(value, _isVisibleSiteChange)) return;
                _isVisibleSiteChange = value;
                NotifyOfPropertyChange(() => IsVisibleSiteChange);
            }
        }

        public bool IsVisibleSeasonChange
        {
            get { return _isVisibleSeasonChange; }
            set
            {
                if (Equals(value, _isVisibleSeasonChange)) return;
                _isVisibleSeasonChange = value;
                NotifyOfPropertyChange(() => IsVisibleSeasonChange);
            }
        }

        public bool IsVisibleScaleSettings
        {
            get { return _isVisibleScaleSettings; }
            set
            {
                if (Equals(value, _isVisibleScaleSettings)) return;
                _isVisibleScaleSettings = value;
                NotifyOfPropertyChange(() => IsVisibleScaleSettings);
            }
        }

        public bool IsVisibleReports
        {
            get { return _isVisibleReports; }
            set
            {
                if (Equals(value, _isVisibleReports)) return;
                _isVisibleReports = value;
                NotifyOfPropertyChange(() => IsVisibleReports);
            }
        }

        public bool IsVisibleOrderCreateEditDelete
        {
            get { return _isVisibleOrderCreateEditDelete; }
            set
            {
                if (Equals(value, _isVisibleOrderCreateEditDelete)) return;
                _isVisibleOrderCreateEditDelete = value;
                NotifyOfPropertyChange(() => IsVisibleOrderCreateEditDelete);
            }
        }
        #endregion

        #region Events

        public void Handle(DefaultScaleChangedArgs args)
        {
            if (args != null)
            {
                ScaleName = args.Scale.ScaleName;
                var configuration = IoC.Get<IConfiguration>();
                configuration.SelectedScaleName = args.Scale.ScaleName;
                if (configuration.IsManualMode || !configuration.SelectedSite.IsDefault)
                    ScaleName = "Manual";

                ShortScaleName = Truncate(ScaleName, 8);
            }
        }

        public void Handle(ChangeSiteArgs args)
        {
            if (args != null && args.Site!=null)
            {
                var configuration = IoC.Get<IConfiguration>();
                configuration.SelectedSite = args.Site;
                SiteName = args.Site.Name;
                ShortSiteName = Truncate(args.Site.Name, 8);
                SiteConnectionString = args.Site.ConnectionString;
                IsDefaultSite = args.Site.IsDefault;

                // Update the running time
                timeRunning = (DateTime.Now.AddSeconds(1) - appStartDateTime);

                // Update the caption for the window and the menu panel
                if (SiteConnectionString != null)
                {
                    if (SiteConnectionString.IndexOf("catalog=repl_") > 1)
                    {
                        DisplayName = SiteName + "(Replicated in Lowell)   Running Time : " + timeRunning.ToString(@"hh\:mm\:ss") + ")";
                        SiteNameDisplay = "rpl" + SiteName;
                    }
                    else
                    {
                        DisplayName = SiteName + "(Real-time)   Running Time : " + timeRunning.ToString(@"hh\:mm\:ss") + ")";
                        SiteNameDisplay = SiteName;
                    }
                }
                else
                {
                    DisplayName = SiteName + "(Real-time)   Running Time : " + timeRunning.ToString(@"hh\:mm\:ss") + ")";
                    SiteNameDisplay = SiteName;
                }

                var sqlBuilder = new SqlConnectionStringBuilder(configuration.SelectedSite.ConnectionString);
                ServerName = sqlBuilder.DataSource;
                ShortServerName = Truncate(sqlBuilder.DataSource, 8);

                configuration.ConnectionString = args.Site.ConnectionString;

                var seasons = IoC.Get<ICommonService>().GetSeasons();
                var season = seasons.FirstOrDefault(s => s.IsActive) ??
                             seasons.OrderByDescending(s => s.StartDate).FirstOrDefault();

                if (season == null)
                    throw new Exception("No seasons defined.");

                SelectedSeason = season;
                Application.Current.Properties["SeasonId"] = season.SeasonId;

                ShowDispatcher();
            }
        }

        public void Handle(SeasonChangeArgs args)
        {
            if (args != null && args.Season != null)
            {
                SelectedSeason = args.Season;
                Application.Current.Properties["SeasonId"] = args.Season.SeasonId;
                ShowDispatcher();
            }
        }
        public void Handle(AuthArgs message)
        {
            IsAuthorized = (bool)Application.Current.Properties["IsLogged"];
            UserName = Application.Current.Properties["UserName"].ToString();

            UserAuthorizationPermissionsCheck();
            ShowDispatcher();
        }

        public void Handle(TicketAddedVoidedArgs args)
        {
            TonsShippedToday = IoC.Get<ITicketService>().GetTonsShippedToday(_selectedSeason.SeasonId).ToString("N2");
        }

       protected override void OnActivate()
        {
            _eventAggregator.Subscribe(this);
            base.OnActivate();
        }

        protected override void OnDeactivate(bool close)
        {
            _eventAggregator.Unsubscribe(this);
            base.OnDeactivate(close);
        }

        #endregion
    }
}