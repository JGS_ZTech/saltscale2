﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Input;
using Caliburn.Micro;
using Contract.Events;
using ZTech.Controls;
using ZTech.ViewModels.Abstract;
using Action = System.Action;

namespace ZTech.ViewModels
{
    public class ContactPhoneViewModel : BaseViewModel
    {
        private readonly IEventAggregator _eventAggregator;
        private readonly ILog _log;
        private string _errors;

        private string _number;
        private string _numberType;
        private ContactWidgetViewModel.PhoneModel _phone;

        public ContactPhoneViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            _log = LogManager.GetLog(typeof (ContactPhoneViewModel));
            DisplayName = "Phone Number";
        }

        [Required(AllowEmptyStrings = false, ErrorMessage = "The Phone Type cannot be empty.")]
        public string NumberType
        {
            get { return _numberType; }
            set
            {
                if (value == _numberType) return;
                _numberType = value;
                NotifyOfPropertyChange(() => NumberType);
            }
        }

        [Required(AllowEmptyStrings = false, ErrorMessage = "The Phone Number cannot be empty.")]
        public string Number
        {
            get { return _number; }
            set
            {
                if (value == _number) return;
                _number = value;
                NotifyOfPropertyChange(() => Number);
            }
        }

        #region Phone Number

        public ContactWidgetViewModel.PhoneModel Phone
        {
            get { return _phone; }
            set
            {
                if (Equals(value, _phone)) return;
                _phone = value;
                NotifyOfPropertyChange(() => Phone);
            }
        }

        public string Errors
        {
            get { return _errors; }
            set
            {
                if (value == _errors) return;
                _errors = value;
                NotifyOfPropertyChange(() => Errors);
            }
        }

        #endregion

        #region Actions

        public void Cancel()
        {
            TryClose(false);
        }

        public void KeyHandle(Key key)
        {
            if (key == Key.F2)
            {
                Done();
            }
        }

        public void Done()
        {
            try
            {
                if (!IsValid())
                    return;

                Phone.Number = Number;
                Phone.NumberType = NumberType;

                _eventAggregator.PublishOnCurrentThread(new ContactPhoneModifiedArgs(Phone));

                TryClose(true);
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Errors = string.Format(
                    "Operation failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                    ex.Message);

                ShowMessage("Error", Errors, NotificationType.Error);
            }
        }

        private bool IsValid()
        {
            ICollection<ValidationResult> validationResults = new Collection<ValidationResult>();
            Validator.TryValidateObject(this, new ValidationContext(this), validationResults, true);

            if (validationResults.Any())
            {
                List<string> errors = validationResults.Select(v => v.ErrorMessage).ToList();

                Errors = string.Format(
                    "Validation Failed. {0} {1}", Environment.NewLine,
                    string.Join("; ", errors));

                ShowMessage("Validation Errors", Errors, NotificationType.Warning);

                return false;
            }

            return true;
        }

        #endregion

        protected override void OnInitialize()
        {
            base.OnInitialize();
            Number = Phone.Number;
            NumberType = Phone.NumberType;
        }
    }
}