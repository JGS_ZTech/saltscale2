﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Caliburn.Micro;
using Contract.Domain;
using Contract.Events;
using Contract.Services;
using ZTech.Controls;
using ZTech.Reports;
using ZTech.ViewModels.Abstract;
using Action = System.Action;

namespace ZTech.ViewModels
{
    public class ScalesViewModel : BaseViewModel, IHandle<ScaleResultsArgs>, IHandle<ScaleExceptionsArgs>
    {
        private readonly IEventAggregator _eventAggregator;
        private readonly ILog _log;
        private readonly int _seasonId;
        private List<ScaleData> _scaleList;
        private List<int> _serialComPorts;
        private string _errors;
        private bool _isManual;
        private bool _isBusy;

        public ScalesViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            _log = LogManager.GetLog(typeof(ScalesViewModel));
            _seasonId = (int)Application.Current.Properties["SeasonId"];
            DisplayName = "Scales";
        }

        protected override void OnInitialize()
        {
            base.OnInitialize();
            IsBusy = true;
            var task = new Task(InitTask);
            task.Start();
        }

        private void InitTask()
        {
            try
            {
                var configuration = IoC.Get<IConfiguration>();
                var scales = IoC.Get<IScaleService>()
                    .GetAvailableScales(configuration.SelectedSite.Name);
                ScaleList = scales
                        .Select(s => new ScaleData(s))
                        .ToList();

                var selected = configuration.SelectedScaleName;

                var ports = IoC.Get<IScaleManager>().GetAvailableComPorts();
                if (!ports.Any())
                {
                    ports = ScaleList.Select(s => s.SerialComPort).Distinct().ToList();
                }

                SerialComPorts = ports;

                foreach (var scaleData in ScaleList)
                {
                   if (scaleData.ScaleName.Equals(selected, StringComparison.InvariantCultureIgnoreCase))
                        scaleData.IsDefault = true;

                    scaleData.StartTestScalingEvent+=ScaleDataOnStartTestScaling;
                }
                IsManual = configuration.IsManualMode;
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(
                            string.Format(
                                "Initialization failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                                ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));

                    TryClose(false);
                };

                action.OnUIThreadAsync();
            }
            finally
            {
                IsBusy = false;
            }
        }

        private void ScaleDataOnStartTestScaling(object sender, EventArgs eventArgs)
        {
            var item = sender as ScaleData;
            if (item == null || !item.IsTestMode) return;

            var toStop = ScaleList.Where(s => s.IsTestMode && s.Id != item.Id).ToList();
            foreach (var scaleData in toStop)
            {
                scaleData.IsTestMode = false;
            }
        }

        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                if (value.Equals(_isBusy)) return;
                _isBusy = value;
                NotifyOfPropertyChange(() => IsBusy);
            }
        }

        public bool IsManual
        {
            get { return _isManual; }
            set
            {
                if (value.Equals(_isManual)) return;
                _isManual = value;
                NotifyOfPropertyChange(() => IsManual);
            }
        }
        public List<int> SerialComPorts
        {
            get { return _serialComPorts; }
            set
            {
                if (Equals(value, _serialComPorts)) return;
                _serialComPorts = value;
                NotifyOfPropertyChange(() => SerialComPorts);
            }
        }

        public List<ScaleData> ScaleList
        {
            get { return _scaleList; }
            set
            {
                if (Equals(value, _scaleList)) return;
                _scaleList = value;
                NotifyOfPropertyChange(() => ScaleList);
            }
        }

        public string Errors
        {
            get { return _errors; }
            set
            {
                if (value == _errors) return;
                _errors = value;
                NotifyOfPropertyChange(() => Errors);
            }
        }

        public void KeyHandle(Key key)
        {
            if (key == Key.F2)
            {
                Done();
            }
        }

        public void Cancel()
        {
            TryClose(true);
        }
        public void Done()
        {
            try
            {
                if (!Validate())
                    return;

                IoC.Get<IScaleService>().UpdateScales(ScaleList);

                var scaleData = ScaleList.First(s => s.IsDefault);
                IoC.Get<IConfiguration>().UpdateDefaultScale(scaleData, IsManual);

                _eventAggregator.PublishOnCurrentThread(new DefaultScaleChangedArgs(scaleData));

                TryClose(true);
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "Scales saving failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };

                action.OnUIThreadAsync();
            }
        }

        private bool Validate()
        {
            Errors = null;

            var validationResults = new List<string>();
            if (!ScaleList.Any(s => s.IsDefault))
                validationResults.Add("Default scale should be selected.");

            if (validationResults.Any())
            {
                Errors = string.Format(
                    "Validation Failed.{0}{1}", Environment.NewLine,
                    string.Join("\n", validationResults));

                ShowMessage("Validation Errors", Errors, NotificationType.Warning);

                return false;
            }

            return true;
        }


        #region Events

        protected override void OnActivate()
        {
            _eventAggregator.Subscribe(this);
            base.OnActivate();
        }

        protected override void OnDeactivate(bool close)
        {
            foreach (var scaleData in ScaleList)
            {
                scaleData.StopScaling();
            }

            _eventAggregator.Unsubscribe(this);
            base.OnDeactivate(close);
        }

        #endregion

        public void Handle(ScaleResultsArgs args)
        {
            var scaleData = ScaleList.FirstOrDefault(s => s.Id == args.Result.Id);
            if (scaleData != null)
            {
                scaleData.Error = null;

                scaleData.StreamUnitOfWeight = args.Result.StreamUnitOfWeight;
                scaleData.StreamMotionStatus = args.Result.StreamMotionStatus;
                scaleData.StreamPolarity = args.Result.StreamPolarity;
                scaleData.StreamWeightGrossNet = args.Result.StreamWeightGrossNet;
                scaleData.StreamWeight = args.Result.StreamWeight.HasValue && args.Result.StreamWeight.Value > 0
                    ? args.Result.StreamWeight.Value.ToString("N0")
                    : "-";
            }
        }

        public void Handle(ScaleExceptionsArgs args)
        {
            var scaleData = ScaleList.FirstOrDefault(s => s.Id == args.ScaleId);
            if (scaleData != null)
            {
                scaleData.StreamWeight = "-";
                scaleData.Error = args.Message;
            }
        }
    }

    #region DTO

    public class ScaleData : PropertyChangedBase, IScaleInfo
    {
        private string _scaleName;
        private bool _isScaleEnabled;
        private int _serialComPort;
        private string _streamWeight;
        private bool _isDefault;
        private string _error;

        private ScaleIndicatorStreamPolarity? _streamPolarity;
        private ScaleIndicatorStreamUnitOfWeight? _streamUnitOfWeight;
        private ScaleIndicatorStreamWeightGrossNet? _streamWeightGrossNet;
        private ScaleIndicatorStreamMotionStatus? _streamMotionStatus;
        private readonly IScaleManager _scaleManager;
        private bool _isComPortEnabled;
        private bool _isTestMode;
        private string _motionStatus;
        private string _unitOfWeight;
        private string _weightType;
        private string _polarity;
        public event EventHandler StartTestScalingEvent;

        public ScaleData(IScaleInfo scaleInfo)
        {
            _scaleManager = IoC.Get<IScaleManager>();
            Id = scaleInfo.Id;
            ScaleName = scaleInfo.ScaleName;
            SerialComPort = scaleInfo.SerialComPort;
            StreamWeight = "-";
            IsScaleEnabled = scaleInfo.IsScaleEnabled;
            IsComPortEnabled = true;
        }

        public int Id { get; set; }

        public string ScaleName
        {
            get { return _scaleName; }
            set
            {
                if (value == _scaleName) return;
                _scaleName = value;
                NotifyOfPropertyChange(() => ScaleName);
            }
        }

        public bool IsDefault
        {
            get { return _isDefault; }
            set
            {
                if (value.Equals(_isDefault)) return;
                _isDefault = value;
                NotifyOfPropertyChange(() => IsDefault);
            }
        }

        public bool IsTestMode
        {
            get { return _isTestMode; }
            set
            {
                if (value.Equals(_isTestMode)) return;
                _isTestMode = value;

                if (StartTestScalingEvent != null)
                    StartTestScalingEvent(this, new EventArgs());

                try
                {
                    Error = null;
                    if (_isTestMode)
                        _scaleManager.StartScaling(this);
                    else
                        _scaleManager.StopScaling();
                }
                catch (Exception ex)
                {
                    _isTestMode = false;
                    Error = ex.Message;
                }
                IsComPortEnabled = !_isTestMode;

                NotifyOfPropertyChange(() => IsTestMode);
            }
        }

        public bool IsScaleEnabled
        {
            get { return _isScaleEnabled; }
            set
            {
                if (value.Equals(_isScaleEnabled)) return;
                _isScaleEnabled = value;
                NotifyOfPropertyChange(() => IsScaleEnabled);

            }
        }

        public bool IsComPortEnabled
        {
            get { return _isComPortEnabled; }
            set
            {
                if (value.Equals(_isComPortEnabled)) return;
                _isComPortEnabled = value;
                NotifyOfPropertyChange(() => IsComPortEnabled);
            }
        }

        public int SerialComPort
        {
            get { return _serialComPort; }
            set
            {
                if (value == _serialComPort) return;
                _serialComPort = value;
                NotifyOfPropertyChange(() => SerialComPort);
            }
        }

        public string StreamWeight
        {
            get { return _streamWeight; }
            set
            {
                if (value == _streamWeight) return;
                _streamWeight = value;
                NotifyOfPropertyChange(() => StreamWeight);
            }
        }


        public ScaleIndicatorStreamPolarity? StreamPolarity
        {
            get { return _streamPolarity; }
            set
            {
                if (value == _streamPolarity)
                    return;
                
                _streamPolarity = value;
                if (!value.HasValue || value.Value == 0)
                    _streamPolarity = null;
                
                NotifyOfPropertyChange(() => StreamPolarity);
                Polarity = _streamPolarity.HasValue ? _streamPolarity.Value.GetDescription() : string.Empty;
            }
        }

        public string Polarity
        {
            get { return _polarity; }
            set
            {
                if (value == _polarity) return;
                _polarity = value;
                NotifyOfPropertyChange(() => Polarity);
            }
        }

        public ScaleIndicatorStreamUnitOfWeight? StreamUnitOfWeight
        {
            get { return _streamUnitOfWeight; }
            set
            {
                if (value == _streamUnitOfWeight)
                    return;
                
                _streamUnitOfWeight = value;
                if (!value.HasValue || value.Value == 0)
                    _streamUnitOfWeight = null;

                NotifyOfPropertyChange(() => StreamUnitOfWeight);
                UnitOfWeight = _streamUnitOfWeight.HasValue ? _streamUnitOfWeight.Value.GetDescription() : string.Empty;
            }
        }

        public string UnitOfWeight
        {
            get { return _unitOfWeight; }
            set
            {
                if (value == _unitOfWeight) return;
                _unitOfWeight = value;
                NotifyOfPropertyChange(() => UnitOfWeight);
            }
        }

        public ScaleIndicatorStreamWeightGrossNet? StreamWeightGrossNet
        {
            get { return _streamWeightGrossNet; }
            set
            {
                if (value == _streamWeightGrossNet)
                    return;
               
                _streamWeightGrossNet = value;
                if (!value.HasValue || value.Value == 0)
                    _streamWeightGrossNet = null;

                NotifyOfPropertyChange(() => StreamWeightGrossNet);
                WeightType = _streamWeightGrossNet.HasValue ? _streamWeightGrossNet.Value.GetDescription() : string.Empty;
            }
        }

        public string WeightType
        {
            get { return _weightType; }
            set
            {
                if (value == _weightType) return;
                _weightType = value;
                NotifyOfPropertyChange(() => WeightType);
            }
        }

        public ScaleIndicatorStreamMotionStatus? StreamMotionStatus
        {
            get { return _streamMotionStatus; }
            set
            {
                if (value == _streamMotionStatus)
                    return;

                _streamMotionStatus = value;
                if (!value.HasValue || value.Value == 0)
                    _streamMotionStatus = null;
                
                NotifyOfPropertyChange(() => StreamMotionStatus);
                MotionStatus = _streamMotionStatus.HasValue ? _streamMotionStatus.Value.GetDescription() : string.Empty;
            }
        }

        public string MotionStatus
        {
            get { return _motionStatus; }
            set
            {
                if (value == _motionStatus) return;
                _motionStatus = value;
                NotifyOfPropertyChange(() => MotionStatus);
            }
        }

        public string Error
        {
            get { return _error; }
            set
            {
                if (value == _error) return;
                _error = value;
                NotifyOfPropertyChange();
            }
        }

        public void StopScaling()
        {
            _scaleManager.StopScaling();
        }

    }
    #endregion
}