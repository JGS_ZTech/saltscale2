﻿using Caliburn.Micro;
using Contract.Services;
using ZTech.ViewModels.Abstract;
using ZTech.ViewModels.ViewObject;

namespace ZTech.ViewModels
{
    public class NewTruckViewModel : BaseTruckViewModel
    {
        public NewTruckViewModel(IEventAggregator eventAggregator) : base(eventAggregator, "New Truck Screen")
        {
            _log = LogManager.GetLog(typeof (NewTruckViewModel));
        }

        protected override void InitializeTruck()
        {
            Truck = new TruckDataModel(IoC.Get<ITruckService>().GetNewTruck(_seasonId));
        }
    }
}