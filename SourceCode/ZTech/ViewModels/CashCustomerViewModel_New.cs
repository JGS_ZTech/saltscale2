﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Input;

using Caliburn.Micro;
using Contract.Domain;
using Contract.Events;
using Contract.Exception;
using Contract.Services;
using ZTech.Controls;
using ZTech.Helpers;
using ZTech.Reports;
using ZTech.ViewModels.Abstract;
using ZTech.ViewModels.ViewObject;
using Action = System.Action;
using ValidationResult = System.ComponentModel.DataAnnotations.ValidationResult;
using System.IO;
using CrystalDecisions.ReportAppServer.Controllers;
using CrystalDecisions.Shared;
using System.Diagnostics;

namespace ZTech.ViewModels
{
    public class CashCustomerViewModel : BaseViewModel, IHandle<CashPurchaseModifiedArgs>, IHandle<ScaleChangedArgs>, IHandle<ScaleResultsArgs>, IHandle<ScaleExceptionsArgs>
    {
        private readonly IEventAggregator _eventAggregator;
        private readonly ILog _log;
        private readonly int _seasonId;
        private string _defaultSite = IoC.Get<IConfiguration>().SelectedSite.Name;
        private string _errors;
        //Cash Customer Information
        private CashPurchaseDataModel _cashPurchase;
        private CashPurchaseDataModel _selectedCheckIn;
        private List<CashPurchaseDataModel> _checkInList;
        private CashPurchaseDataModel _selectedReCheckIn;
        private List<CashPurchaseDataModel> _reCheckInList;
        //Material Information
        private IReadOnlyList<IMaterialInfo> _materialList;
        private IMaterialInfo _selectedMaterial;
        //Company Information
        private IReadOnlyList<ICompanyInfo> _companyList;
        private ICompanyInfo _selectedCompany;
        //Scale
        private ScaleWidgetViewModel _scaleWidget;
        private bool _isNewPurchase;
        private bool _isReCheckInSelected;
        private bool _isCheckInSelected = true;
        private bool _isCheckOutSelected = true;
        private bool _IsCheckInEnabled = true;
        private bool _isTruckSelected;
        private bool _isTruckWeightEnabled = true;
        private bool _isCancelTicketEnabled = false;
        private bool _isTrucksDescriptionFocused;
        private readonly IScaleManager _scaleManager = IoC.Get<IScaleManager>();
        private bool _isBusy;
        private bool _isWeightOverrideEnabled;

        //Model Properties
        public IScaleInfo SelectedScale { get; set; }

        // TICKET View Vairables
        private Color _checkBackground;
        private bool _cash;
        private bool _enableTended;
        private bool _enableCheckNumber;
        private bool _creditCard;
        private bool _check;
        private bool _isExempt;
        private decimal _cashRate;
        private int _firstWeight;
        private int _secondWeight;
        private int _netWeight;
        //Cash Ticket Information
        private CashPurchaseTicketDataModel _cashTicket;
        private IReadOnlyList<ICompanyData> _companyListData;
        private ICompanyData _selectedCompanyData;
        //Scale Information
        private string _taxRateLabel;
        private decimal _fuelSurcharge;
        private string _secondWeightText;
        private bool _isPrintingEnabled;
        private string _checkNumber;
        private string _ticketNumber;

        private bool _isPanelReCheckInEnabled = true;
        private bool _isPanelDoneEnabled = true;
        private bool _isPanelCheckOutEnabled = false;

        private CrystalDecisions.CrystalReports.Engine.ReportDocument _crystalReportDocument;

        public Boolean IsConsolidated = true;

        #region Cash Customer


        public CashPurchaseDataModel CashPurchase
        {
            get { return _cashPurchase; }
            set
            {
                if (Equals(value, _cashPurchase)) return;
                _cashPurchase = value;
                NotifyOfPropertyChange(() => CashPurchase);
                Errors = null;
            }
        }

        public CashPurchaseDataModel SelectedCheckIn
        {
            get { return _selectedCheckIn; }
            set
            {
                if (Equals(value, _selectedCheckIn)) return;
                _selectedCheckIn = value;
                NotifyOfPropertyChange(() => SelectedCheckIn);
                Errors = null;
                LoadCheckInCashCustomer();
            }
        }

        public List<CashPurchaseDataModel> CheckInList
        {
            get { return _checkInList; }
            set
            {
                if (Equals(value, _checkInList)) return;
                _checkInList = value;
                NotifyOfPropertyChange(() => CheckInList);
            }
        }

        public CashPurchaseDataModel SelectedReCheckIn
        {
            get { return _selectedReCheckIn; }
            set
            {
                if (Equals(value, _selectedReCheckIn)) return;
                _selectedReCheckIn = value;
                NotifyOfPropertyChange(() => SelectedReCheckIn);
                Errors = null;
                LoadReCheckInCashCustomer();
            }
        }

        public List<CashPurchaseDataModel> ReCheckInList
        {
            get { return _reCheckInList; }
            set
            {
                if (Equals(value, _reCheckInList)) return;
                _reCheckInList = value;
                NotifyOfPropertyChange(() => ReCheckInList);
            }
        }

        public string Errors
        {
            get { return _errors; }
            set
            {
                if (value == _errors) return;
                _errors = value;
                NotifyOfPropertyChange(() => Errors);
            }
        }

        private void InitializeCashCustomer()
        {
            try
            {
                CashPurchase = new CashPurchaseDataModel(IoC.Get<ICashPurchaseService>().GetNewCashPurchase(_seasonId, _defaultSite)); 
                List<CashPurchaseDataModel> tempCheckInList = IoC.Get<ICashPurchaseService>().GetCheckInList(_seasonId).Select(cp => new CashPurchaseDataModel(cp)).ToList();
                CheckInList = tempCheckInList.OrderByDescending(x => x.UpdateTime).ToList();
                List<CashPurchaseDataModel> tempReCheckInList = IoC.Get<ICashPurchaseService>().GetReCheckInList(_seasonId, _defaultSite).Select(cp => new CashPurchaseDataModel(cp)).ToList();
                ReCheckInList = tempReCheckInList.OrderByDescending(x => x.UpdateTime).ToList();
                IsNewPurchase = true;
                IsReCheckInSelected = false;
                IsCheckInSelected = false;
                IsTruckSelected = false;
            }
            catch (Exception ex)
            {
                var exInnerMessageBoxText = string.Empty;
                _log.Error(ex);
                if (ex.InnerException != null)
                {
                    _log.Error(ex.InnerException);
                    exInnerMessageBoxText = $" and inner exception '{ex.InnerException.Message}'{Environment.NewLine} ";
                }

                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show($"Cash Customer Initilization failed with exception '{ex.Message}'{exInnerMessageBoxText}.  Please let IT know this occured.", "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };
                action.OnUIThreadAsync();

                //// 20181218, JGS - Minimize thread processing for error messages
                //MessageBox.Show($"Cash Customer Initilization failed with exception '{ex.Message}'{exInnerMessageBoxText}.  Please let IT know this occured.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);               
            }
        }


        #region Properties
        public bool IsTruckSelected
        {
            get { return _isTruckSelected; }
            set
            {
                if (value.Equals(_isTruckSelected)) return;
                _isTruckSelected = value;
                NotifyOfPropertyChange(() => IsTruckSelected);
            }
        }

        public bool IsCheckInSelected
        {
            get { return _isCheckInSelected; }
            set
            {
                if (value.Equals(_isCheckInSelected)) return;
                _isCheckInSelected = value;
                NotifyOfPropertyChange(() => IsCheckInSelected);
            }
        }

        public bool IsCheckOutSelected
        {
            get { return _isCheckOutSelected; }
            set
            {
                if (value.Equals(_isCheckOutSelected)) return;
                _isCheckOutSelected = value;
                NotifyOfPropertyChange(() => IsCheckOutSelected);
            }
        }

        public bool IsCheckInEnabled
        {
            get { return _IsCheckInEnabled; }
            set
            {
                if (value.Equals(_IsCheckInEnabled)) return;
                _IsCheckInEnabled = value;
                NotifyOfPropertyChange(() => IsCheckInEnabled);
            }
        }

        public bool IsReCheckInSelected
        {
            get { return _isReCheckInSelected; }
            set
            {
                if (value.Equals(_isReCheckInSelected)) return;
                _isReCheckInSelected = value;
                NotifyOfPropertyChange(() => IsReCheckInSelected);
            }
        }

        public bool IsNewPurchase
        {
            get { return _isNewPurchase; }
            set
            {
                if (value.Equals(_isNewPurchase)) return;
                _isNewPurchase = value;
                NotifyOfPropertyChange(() => IsNewPurchase);
            }
        }

        public bool IsWeightOverrideEnabled
        {
            get { return _isWeightOverrideEnabled; }
            set
            {
                if (value.Equals(_isWeightOverrideEnabled)) return;
                _isWeightOverrideEnabled = value;
                NotifyOfPropertyChange(() => IsWeightOverrideEnabled);
            }
        }

        #endregion

        private void LoadCheckInCashCustomer()
        {
            try
            {
                if (_selectedCheckIn == null) return;

                CashPurchase = new CashPurchaseDataModel(IoC.Get<ICashPurchaseService>().GetCashPurchase(SelectedCheckIn.CashPurchaseId, false));
                SelectedMaterial = MaterialList.FirstOrDefault(m => m.Id == CashPurchase.Material.Id);
                SelectedCompany = CompanyList.FirstOrDefault(c => c.Id == CashPurchase.Company.Id);

                if (_scaleManager != null)
                {
                    StopScaleInteraction();
                    if (ScaleWidget != null)
                        ScaleWidget.Errors = null;
                }

                // It is a new purchase so lets process as such 
                IsNewPurchase = true;  // 2019-Jan-07, JGS - Originally "false" so I am resetting to "true"
                IsCheckInSelected = true;
                IsReCheckInSelected = false;
                SelectedReCheckIn = null;
                IsTruckSelected = true;
            }
            catch (Exception ex)
            {
                _log.Error(ex);

                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "Cash Customer failed with exception {0}. Please reopen application and try again or contact HelpDesk.",
                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };
                action.OnUIThreadAsync();

                //// 20181218, JGS - Minimize thread processing for error messages
                //MessageBox.Show(string.Format(
                //            "Cash Customer failed with exception {0}. Please reopen application and try again or contact HelpDesk.",
                //            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void LoadReCheckInCashCustomer()
        {
            try
            {
                if (_selectedReCheckIn == null) return;
                
                CashPurchase = new CashPurchaseDataModel(IoC.Get<ICashPurchaseService>().GetCashPurchase(SelectedReCheckIn.CashPurchaseId, true))
                {
                    PaymentForm = String.Empty,
                    CheckNumber = String.Empty,
                    IsComplete = false
                };

                SelectedMaterial = MaterialList.FirstOrDefault(m => m.Id == CashPurchase.Material.Id);
                SelectedCompany = CompanyList.FirstOrDefault(c => c.Id == CashPurchase.Company.Id);
                if (_scaleManager != null)
                {
                    StopScaleInteraction();
                    if (ScaleWidget != null)
                        ScaleWidget.Errors = null;
                }
                IsCheckInSelected = false;
                IsReCheckInSelected = true;
                IsNewPurchase = false;
                SelectedCheckIn = null;
                IsTruckSelected = true;
            }
            catch (Exception ex)
            {
                _log.Error(ex);

                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "Cash Customer failed with exception {0}. Please reopen application and try again or contact HelpDesk.",
                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };
                action.OnUIThreadAsync();

                //// 20181218, JGS - Minimize thread processing for error messages
                //MessageBox.Show(string.Format(
                //                            "Cash Customer failed with exception {0}. Please reopen application and try again or contact HelpDesk.",
                //                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        #endregion

        #region Material

        public IMaterialInfo SelectedMaterial
        {
            get { return _selectedMaterial; }
            set
            {
                if (Equals(value, _selectedMaterial)) return;
                _selectedMaterial = value;
                NotifyOfPropertyChange(() => SelectedMaterial);

                if (_selectedMaterial != null && _selectedMaterial.PricePerTon.HasValue)
                    CashPurchase.Quote = _selectedMaterial.PricePerTon.Value;
                else
                    CashPurchase.Quote = 0;
            }
        }

        public IReadOnlyList<IMaterialInfo> MaterialList
        {
            get { return _materialList; }
            set
            {
                if (Equals(value, _materialList)) return;
                _materialList = value;
                NotifyOfPropertyChange(() => MaterialList);
            }
        }

        private void InitializeMaterial()
        {
            try
            {
                // Cash Purchase Material
                MaterialList = IoC.Get<IMaterialService>().GetMaterials(_seasonId);

                // Default material to Configuration, SALT, ROAD SALT or FirstOrDefault
                var materialModel = MaterialList.FirstOrDefault(m => m.Name.ToString().ToUpper().Trim() == "SALT");
                if (materialModel == null)
                    materialModel = MaterialList.FirstOrDefault(m => m.Name.ToString().ToUpper().Trim() == "ROAD SALT");
                if (materialModel == null)
                    materialModel = MaterialList.FirstOrDefault();

                // If no material is found then abort
                if (materialModel == null) return;

                CashPurchase.Material = materialModel;
                SelectedMaterial = materialModel;
                CashPurchase.Quote = materialModel.PricePerTon;
            }
            catch (Exception ex)
            {
                _log.Error(ex);

                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "Cash Customer failed with exception {0}. Please reopen application and try again or contact HelpDesk.",
                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };
                action.OnUIThreadAsync();

                //// 20181218, JGS - Minimize thread processing for error messages
                //MessageBox.Show(string.Format(
                //                            "Cash Customer failed with exception {0}. Please reopen application and try again or contact HelpDesk.",
                //                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        #endregion

        #region Company

        public ICompanyInfo SelectedCompany
        {
            get { return _selectedCompany; }
            set
            {
                if (Equals(value, _selectedCompany)) return;
                _selectedCompany = value;
                NotifyOfPropertyChange(() => SelectedCompany);
                if (_selectedCompany == null) return;
                CashPurchase.Company = _selectedCompany;
                CashPurchase.CompanyId = _selectedCompany.Id;
            }
        }

        public IReadOnlyList<ICompanyInfo> CompanyList
        {
            get { return _companyList; }
            set
            {
                if (Equals(value, _companyList)) return;
                _companyList = value;
                NotifyOfPropertyChange(() => CompanyList);
            }
        }

        private void InitializeCompany()
        {
            var commonService = IoC.Get<ICommonService>();
            CompanyList = commonService.GetCompanies();
            var selectedSite = IoC.Get<ICommonService>().GetSite(_defaultSite, _seasonId);
            var companyModel = CompanyList.Where(s => s.Id == selectedSite.DefaultCashCustomerCompanyId).FirstOrDefault();

            CashPurchase.Company = companyModel;
            SelectedCompany = companyModel;

            //try
            //{
            //    var selectedSite = IoC.Get<ICommonService>().GetSite(_defaultSite, _seasonId);
            //    CompanyList = IoC.Get<ICommonService>().GetCompanies();
            //    var companyModel = CompanyList.Where(s => s.Id == selectedSite.DefaultCashCustomerCompanyId).FirstOrDefault();


            //    CashPurchase.Company = companyModel;
            //    SelectedCompany = companyModel;
            //}
            //catch (Exception ex)
            //{
            //    _log.Error(ex);

            //    Action action = () =>
            //    {
            //        MessageBoxResult messageBoxResult =
            //            (MessageBox.Show(string.Format(
            //                "Cash Customer failed with exception {0}. Please reopen application and try again or contact HelpDesk.",
            //                ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
            //    };
            //    action.OnUIThreadAsync();

            //    //// 20181218, JGS - Minimize thread processing for error messages
            //    //MessageBox.Show(string.Format(
            //    //                            "Cash Customer failed with exception {0}. Please reopen application and try again or contact HelpDesk.",
            //    //                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            //}
        }

        #endregion

        #region Scale


        public ScaleWidgetViewModel ScaleWidget
        {
            get { return _scaleWidget; }
            set
            {
                if (Equals(value, _scaleWidget)) return;
                _scaleWidget = value;
                NotifyOfPropertyChange(() => ScaleWidget);
            }
        }

        private void InitializeScale()
        {         
            //// 2019-Jan-07, JGS - Reload scale widget
            //if (ScaleWidget == null)
            //{
            //    ScaleWidget = IoC.Get<ScaleWidgetViewModel>();
            //    ScaleWidget.ConductWith(this);
            //}

            if (IoC.Get<IConfiguration>().IsManualMode)
            {
                ManualScaling();
            }
            else
            {
                StartScaleInteraction();
            }
        }

        private void ManualScaling()
        {
            IsWeightOverrideEnabled = true;
            if (CashTicket != null)
            {
                CashTicket.IsFirstWeightEnabled = true;
                CashTicket.IsSecondWeightEnabled = true;
                CashTicket.IsManual = true;
            }

            if (ScaleWidget != null)
                ScaleWidget.Errors = null;

            if (_scaleManager != null)
                StopScaleInteraction();
        }
        
        
        private void StartScaleInteraction()
        {
            try
            {
                SelectedScale = ScaleWidget.SelectedScale;
                _scaleManager.StartScaling(SelectedScale);
            }
            catch (Exception ex)
            {
                ManualScaling();
                Errors = $"Start Scale Interaction failed with exception: {ex.Message}";
                ShowMessage("Scale Error", Errors, NotificationType.Warning);
                _log.Error(ex);
            }
        }

        private void StopScaleInteraction()
        {
            try
            {
                SelectedScale = ScaleWidget.SelectedScale;
                _scaleManager.StopScaling();
            }
            catch (Exception ex)
            {
                ManualScaling();
                Errors = $"Stop Scale Interaction failed with exception: {ex.Message}";
                ShowMessage("Scale Error", Errors, NotificationType.Warning);
                _log.Error(ex);
            }
        }

        private void ResetToNewPurchase()
        {
            // Reset view models
            InitializeCashCustomer();
            InitializeMaterial();
            
            // Reset status
            SelectedCheckIn = null;
            SelectedReCheckIn = null;
            IsNewPurchase = true;
            IsCheckInSelected = false;
            IsReCheckInSelected = false;
            IsTruckSelected = false;
            IsCheckOutSelected = true;
            IsTruckWeightEnabled = true;

            // Reset tickt payment Values
            Cash = true;
            Check = false;
            CreditCard = false;
            CheckNumber = "";
            

            // Activate/Deactivate Buttons
            IsPanelCheckOutEnabled = false;
            IsPanelDoneEnabled = true;
            IsPanelReCheckInEnabled = true;
            IsCancelTicketEnabled = false;

            //if (ScaleWidget == null)
            //{
            //    ScaleWidget = IoC.Get<ScaleWidgetViewModel>();
            //    ScaleWidget.ConductWith(this);
            //}
        }

        #endregion

        #region Actions

        public void KeyHandle(Key key)
        {
            if (key == Key.F2)
            {
                Done();
            }
        }

        public void Done()
        {
            //if (_scaleManager != null)
            //{
            //    StopScaleInteraction();
            //    if (ScaleWidget != null)
            //        ScaleWidget.Errors = null;
            //    ScaleWidget = null;
            //}
            TryClose(false);
        }

        //public void Cancel()
        //{
        //    // Turn off Check-In Fields and form
        //    IsCheckInSelected = false;

        //    // Activate and Populate CHECK-OUT for the selected Cash Purchase
        //    IsCheckOutSelected = true;
        //    IsCheckInEnabled = true;
        //    IsTruckWeightEnabled = true;

        //    // Activate/Deactivate Buttons
        //    IsPanelCheckOutEnabled = false;
        //    IsPanelDoneEnabled = true;
        //    IsPanelReCheckInEnabled = true;

        //    // Revert to the CheckIn mode
        //    ResetToNewPurchase();
        //}

        public void Reset()
        {
            ResetToNewPurchase();
            StartScaleInteraction();
        }

        public void CheckIn()
        {
            try
            {
                // If this is a NOT new purchase or the current values are invalid to do not allow check-in
                if (!IsNewPurchase || !Validate())
                    return;

                CashPurchase.Material = SelectedMaterial;
                CashPurchase.Material.Id = SelectedMaterial.Id;
                CashPurchase.Company = SelectedCompany;
                CashPurchase.Company.Id = SelectedCompany.Id;
                IoC.Get<ICashPurchaseService>().SaveCashPurchase(CashPurchase);

                // Refresh the screen
                ResetToNewPurchase();
                //if (!IoC.Get<IConfiguration>().IsManualMode)
                //    StartScaleInteraction();
            }
            catch (UniqueConstraintException ex)
            {
                _log.Error(ex);

                Action action = () =>
                {
                    MessageBoxResult messageBoxResult = (MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };
                action.OnUIThreadAsync();

                //// 20181218, JGS - Minimize thread processing for error messages
                //MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                // Refresh the screen
                ResetToNewPurchase();
                //if (!IoC.Get<IConfiguration>().IsManualMode)
                //    StartScaleInteraction();
            }
            catch (Exception ex)
            {
                _log.Error(ex);

                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "Cash Customer saving failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };
                action.OnUIThreadAsync();

                // 20181218, JGS - Minimize thread processing for error messages
                MessageBox.Show(string.Format(
                            "Cash Customer saving failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public void ReCheckIn()
        {
            try
            {
                if (!IsReCheckInSelected || !Validate())
                    return;

                IoC.Get<ICashPurchaseService>().SaveCashPurchase(CashPurchase);
                ResetToNewPurchase();               
            }
            catch (UniqueConstraintException ex)
            {
                _log.Error(ex);

                Action action = () =>
                {
                    MessageBoxResult messageBoxResult = (MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };
                action.OnUIThreadAsync();

                //// 20181218, JGS - Minimize thread processing for error messages
                //MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception ex)
            {
                _log.Error(ex);

                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "Cash Customer saving failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };
                action.OnUIThreadAsync();

                //// 20181218, JGS - Minimize thread processing for error messages
                //MessageBox.Show(string.Format(
                //                            "Cash Customer saving failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                //                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public void CancelTicket()
        {
            // Reset Check In/Out Sections
            IsCheckOutSelected = true;
            IsCheckInEnabled = true;
            IsCancelTicketEnabled = false;
            if (CashTicket != null)
            {
                CashTicket.IsFirstWeightEnabled = true;
                CashTicket.IsSecondWeightEnabled = false;
                CashTicket.IsManual = false;
            }

            // Turn-On Check-In
            IsCheckInSelected = true;

            // Reset to New Purchase
            DeleteTicketInfo();
            ResetToNewPurchase();
        }

        public void ResetTicket()
        {
            // Reset Check In/Out Sections
            IsCheckOutSelected = true;
            IsCheckInEnabled = true;
            if (CashTicket != null)
            {
                CashTicket.IsFirstWeightEnabled = false;
                CashTicket.IsSecondWeightEnabled = false;
                CashTicket.IsManual = false;
            }

            // Turn-On Check-In
            IsCheckInSelected = true;

            // Reset to New Purchase
            ResetToNewPurchase();
        }

        public void Deactivate()
        {
            try
            {
                // Reset status to remove the item fromthe list
                CashPurchase.IsActive = false;
                CashPurchase.IsComplete = true;

                IoC.Get<ICashPurchaseService>().SaveCashPurchase(CashPurchase);
                ResetToNewPurchase();
            }
            catch (UniqueConstraintException ex)
            {
                _log.Error(ex);

                Action action = () =>
                {
                    MessageBoxResult messageBoxResult = (MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };
                action.OnUIThreadAsync();

                //// 20181218, JGS - Minimize thread processing for error messages
                //MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception ex)
            {
                _log.Error(ex);

                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "Cash Customer deactivation failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };
                action.OnUIThreadAsync();

                //// 20181218, JGS - Minimize thread processing for error messages
                //MessageBox.Show(string.Format(
                //            "Cash Customer deactivation failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                //            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public void VoidCashTix()
        {
            var cashVoidViewModel = IoC.Get<CashVoidViewModel>();

            var windowManager = IoC.Get<IWindowManager>();
            windowManager.ShowDialog(cashVoidViewModel);
        }

        public void CheckOut()
        {
            // Make sure a check-in is NOT selected
            if (SelectedCheckIn == null) return;

            // Turn off Check-In Fields and form
            IsCheckInSelected = true;
            IsCheckInEnabled = true;
            IsTruckWeightEnabled = false;

            // Activate/Deactivate Buttons
            IsPanelCheckOutEnabled = true;
            IsPanelDoneEnabled = false;
            IsPanelReCheckInEnabled = false;
            IsCancelTicketEnabled = true;

            // Restart Scale interaction
            StartScaleInteraction();

            // Activate and Populate CHECK-OUT for the selected Cash Purchase
            IsCheckOutSelected = true;
            InitializeTicket();
        }

        public void Delete()
        {
            try
            {
                if (_selectedCheckIn == null) return;

                var message = string.Format("Are you sure you want to delete {0} Cash Purchase?", SelectedCheckIn.TruckDescription);
                MessageBoxResult result = MessageBox.Show(message, "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    var ItRan = IoC.Get<ICashPurchaseService>().DeleteCashPurchase(SelectedCheckIn.CashPurchaseId);
                    ResetToNewPurchase();
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex);

                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "Cash Customer failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };
                action.OnUIThreadAsync();
            }
        }

        public void WeightOverride()
        {
            ManualScaling();
        }
        public void WeightOverrideTicket()
        {
            ManualScaling();
        }

        private bool Validate()
        {
            Errors = null;

            ICollection<ValidationResult> validationResults = new Collection<ValidationResult>();
            bool result = Validator.TryValidateObject(CashPurchase, new ValidationContext(CashPurchase), validationResults, true);

            if (CashPurchase.Tare.HasValue && CashPurchase.Tare.Value <= 0)
                validationResults.Add(new ValidationResult("The Tare must be greater than 0 lbs.")); 
            
            if (CashPurchase.Tare.HasValue && CashPurchase.Tare.Value > 150000) //--ToDo
                validationResults.Add(new ValidationResult("The Tare must be less than 150000 lbs."));

            if (!result || validationResults.Any())
            {
                Errors = string.Format("Validation Failed.{0}{1}", Environment.NewLine,
                    string.Join("\n", validationResults.Select(v => v.ErrorMessage)));

                ShowMessage("Validation Errors", Errors, NotificationType.Warning);

                return false;
            }

            return true;
        }


        public void PrintTicket()
        {
            IsPrintingEnabled = false;

            var stopWatch = System.Diagnostics.Stopwatch.StartNew();

            // Ticket: 179943 - Pause for a random number of milliseconds from 0 to 250.
            Random random = new Random();
            int randomNumber = random.Next(0, 250);
            System.Threading.Thread.Sleep(randomNumber);

            try
            {
                Debug.WriteLine("Cash Print Button Click - Begin");
                _log.Info($"Cash Print Button Click - Begin");

                // Stop the scale reading
                if (_scaleManager != null)
                {
                    StopScaleInteraction();
                    if (ScaleWidget != null)
                        ScaleWidget.Errors = null;
                }

                if (!ValidateTicket())
                {
                    if (!IoC.Get<IConfiguration>().IsManualMode && !CashTicket.IsManual)
                    {
                        StartScaleInteraction();
                    }

                    return;
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "Ticket saving failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };

                action.OnUIThread();
                return;
            }



            try
            {
                var selectedSite = IoC.Get<IConfiguration>().SelectedSite;

                if (selectedSite.IsDefault || selectedSite.Name == "Dev")
                {
                    lock (this.CashTicket)
                    {
                        // Make sure to change purchase 1st amount if its different
                        if (CashPurchase.Tare.ToString() != FirstWeight.ToString())
                        {
                            CashPurchase.Tare = FirstWeight;
                        }
                        SaveTicket();
                        GeneratePrint();
                        Print();

                        // If you get to this point the ticket is being printed
                        //MessageBoxResult messageBoxResult0 =
                        //    (MessageBox.Show(string.Format(
                        //        "Ticket printing ... ",
                        //        ""), "Status", MessageBoxButton.OK, MessageBoxImage.Information));

                        //MessageBoxResult messageBoxResult0 = AutoClosingMessageBox.Show("Ticket printing ... ", "Status", MessageBoxButton.OK, MessageBoxImage.Information, 5000);


                        // What happens if we don't aggregate a cash "Order" ID - Nothing
                        //_eventAggregator.PublishOnCurrentThread(new OrderModifiedArgs(CashTicket.OrderId));

                        _eventAggregator.PublishOnCurrentThread(new CashPurchaseModifiedArgs(CashPurchase.CashPurchaseId));
                        _eventAggregator.PublishOnCurrentThread(new TicketAddedVoidedArgs(CashTicket.TicketId));

                        // Try keeping the screen open and reset to New Purchase
                        DeleteTicketInfo();
                        ResetToNewPurchase();
                        StartScaleInteraction();
                        return;

                        // Done();
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult1 =
                        (MessageBox.Show(string.Format(
                            "Loc 1: Ticket printing failed with exception \"{0}\". Please check the printer and try again or contact HelpDesk.",
                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };

                action.OnUIThread();

                MessageBoxResult messageBoxResult2 =
                    (MessageBox.Show(string.Format(
                        "Loc 2: Ticket printing failed with exception \"{0}\". Please check the printer and try again or contact HelpDesk.",
                        ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));

                // Try keeping the screen open and reset to New Purchase
                DeleteTicketInfo();
                ResetToNewPurchase();
                StartScaleInteraction();
                return;

                // Close the form
                //TryClose(true);

            }
            finally
            {
                _log.Info($"Print Button Click - End");
                stopWatch.Stop();
                _log.Info($"Print Button Click Time to Process - {stopWatch.ElapsedMilliseconds}ms");

                //MessageBoxResult messageBoxResult3 = AutoClosingMessageBox.Show("Loc 3: Ticket printing ... ", "Status", MessageBoxButton.OK, MessageBoxImage.Information, 5000);


                //// Try keeping the screen open and reset to New Purchase
                //DeleteTicketInfo();
                //ResetToNewPurchase();
                //StartScaleInteraction();

                // Close the form
                //TryClose(true);
            }
        }

        public void CanPreviewTicket()
        {
            if (CashTicket != null)
            {
                IsPrintingEnabled = FirstWeight > 0 && SecondWeight > FirstWeight
                                                && (Cash || Check || CreditCard)
                                                && CashTicket.TicketNumber > 0
                                                && SelectedMaterial != null;
            }

            if (Check && string.IsNullOrWhiteSpace(CheckNumber))
            {
                IsPrintingEnabled = false;
            }
        }

        public void PreviewTicket()
        {
            try
            {
                if (_scaleManager != null)
                {
                    StopScaleInteraction();
                    if (ScaleWidget != null)
                        ScaleWidget.Errors = null;
                }

                if (!ValidateTicket())
                {
                    if (!IoC.Get<IConfiguration>().IsManualMode && !CashTicket.IsManual)
                        StartScaleInteraction();

                    return;
                }

                // Generate ticket and preview
                GeneratePrint();
                Preview();
            }
            catch (Exception ex)
            {
                _log.Error(ex);

                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "Ticket saving/printing failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };
                action.OnUIThreadAsync();

                //// 20181218, JGS - Minimize thread processing for error messages
                //MessageBox.Show(string.Format(
                //                            "Ticket saving/printing failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                //                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void GeneratePrint()
        {
            var stopWatch = System.Diagnostics.Stopwatch.StartNew();

            _log.Info($"Generate Cash Print - Begin");

            // Truck Description / Customer Organziation
            var customerOrganization = CashTicket.TruckDescription.Substring(0, Math.Min(45, CashTicket.TruckDescription.Length));


            // Ticket Number
            ISeason season = IoC.Get<ICommonService>().GetSeasonById(_seasonId);
            string seasonprefix = string.Empty;
            if (season != null)
            {
                seasonprefix = season.Prefix.Split(new[] { '-' }, StringSplitOptions.RemoveEmptyEntries)[0];
            }
            //string ticketNum = string.Format("{0}-{1}", seasonprefix, Ticket.TicketNumber.ToString("000000"));
            string ticketNum = string.Format("{0}", CashTicket.TicketNumber.ToString("000000"));

            //Company
            var companyId = CashTicket.CompanyId == null ? -1 : SelectedCompany.Id;
            var company = IoC.Get<ICommonService>().GetCompanyData(companyId);
            var companyName = string.Empty;
            var companyDivision = string.Empty;
            var companyAddress = string.Empty;
            var companyPhoneNumber1 = string.Empty;
            var companyPhoneNumber2 = string.Empty;
            if (company != null)
            {
                companyName = company.Name;
                companyDivision = (company.AddressLineOne != string.Empty || company.AddressLineOne != null) ? company.AddressLineOne : string.Empty;
                if (companyDivision.IndexOf("Division", 0) > 0)
                    companyName = String.Format("{0}\n{1}", company.Name, companyDivision);

                //companyAddress = $"{company.AddressLineOne}, {company.City}, {company.State} {company.Zip}";
                companyAddress = String.Format("{0}{1}\n{2}, {3}  {4}",
                    (company.AddressLineOne.IndexOf("Division", 0) == -1) ? company.AddressLineOne + "  " : string.Empty,
                    (company.AddressLineTwo != string.Empty || company.AddressLineTwo != null) ? company.AddressLineTwo + " " : string.Empty,
                    company.City,
                    company.State,
                    company.Zip);


                companyPhoneNumber1 = (company.PhoneNumberOne != string.Empty && company.PhoneNumberOne != null) ? "  Phone: " + company.PhoneNumberOne : string.Empty;
                companyPhoneNumber2 = (company.PhoneNumberTwo != string.Empty && company.PhoneNumberTwo != null) ? "  Phone: " + company.PhoneNumberTwo : string.Empty;
            }

            // Cash Rate
            string customerRate = (Math.Floor(CashTicket.MaterialCharge * 100m)).ToString("0000");

            // Gross, Tare, Net -- Value is in lbs and should be rounded to a whole number
            //-- Done so the weight read at the screen when printing is the same that is saved in the database
            string tare = CashTicket.CashData.Tare.Value.ToString("N0");
            string gross = CashTicket.TruckWeight.ToString("N0");
            string net = CashTicket.Quantity.ToString("N0");
            string tons = CashTicket.NetWeightTon.ToString("N2");

            // Material Cost, Sales Tax, Total
            var materialCost = string.Empty;
            var salesTax = string.Empty;
            var total = string.Empty;
            string paymentType = string.Empty;

            var isHideDollarAmountToTicketTruck = false;
            var isHideDollarAmountToTicketOffice = false;
            var isHideDollarAmountToTicketCustomer = false;
            var isHideDollarAmountToTicketDispatch = false;

            materialCost = CashTicket.MaterialCharge.ToString("C");
            salesTax = CashTicket.Tax.ToString("C");
            total = CashTicket.Due.ToString("C");           
            if (Cash)
                paymentType = "Cash";
            if (Check)
                paymentType = $"Check {CheckNumber}";
            if (CreditCard)
                paymentType = "Credit Card";


            if (CashTicket.HideDollarAmount)
            {
                isHideDollarAmountToTicketTruck = true;
                isHideDollarAmountToTicketOffice = true;
                isHideDollarAmountToTicketCustomer = true;
            }

            var manuallyText = CashTicket.IsManual ? "Manually Key Weight " : string.Empty;

            // WEIGHT MASTER
            var weighMasterNameNumber = IoC.Get<IUserService>().GetByUsername(Application.Current.Properties["UserName"].ToString());

            string _applicationPathReports = AppDomain.CurrentDomain.BaseDirectory + "Reports" + Path.DirectorySeparatorChar;

            _log.Info($"Generate Cash Print - End");
            stopWatch.Stop();
            _log.Info($"Generate Print Time to Process - {stopWatch.ElapsedMilliseconds}ms");

            stopWatch.Restart();

            #region Crystal Ticket
            _log.Info($"Generate Print Cash Customer - Begin");

            _crystalReportDocument = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
            _crystalReportDocument.Load(_applicationPathReports + "Ticket.rpt", CrystalDecisions.Shared.OpenReportMethod.OpenReportByTempCopy);


            CrystalDecisions.CrystalReports.Engine.Database crDatabase;
            CrystalDecisions.CrystalReports.Engine.Tables crTables;

            CrystalDecisions.Shared.ConnectionInfo crConnectioninfo = new CrystalDecisions.Shared.ConnectionInfo();

            _crystalReportDocument.DataSourceConnections.ToString();

            crDatabase = _crystalReportDocument.Database;
            crTables = crDatabase.Tables;

            _crystalReportDocument.SetParameterValue("CompanyName", companyName);
            _crystalReportDocument.SetParameterValue("CompanyPhoneAddress", companyAddress + companyPhoneNumber1 + companyPhoneNumber2);
            _crystalReportDocument.SetParameterValue("CustomerOrganization", customerOrganization);
            _crystalReportDocument.SetParameterValue("Address", string.Empty);
            _crystalReportDocument.SetParameterValue("SpecialInstructions", CashTicket.SpecialInstruction ?? string.Empty);
            _crystalReportDocument.SetParameterValue("TicketNumber", ticketNum);
            _crystalReportDocument.SetParameterValue("Date", CashTicket.CreateTime.ToString("MM/dd/yy HH:mm"));
            _crystalReportDocument.SetParameterValue("ConfirmationNumber", string.Empty);
            _crystalReportDocument.SetParameterValue("PONumber", string.Empty);
            _crystalReportDocument.SetParameterValue("CustomerIdWithRate", paymentType + ((paymentType == "Check") ? '#' + CheckNumber : string.Empty));
            _crystalReportDocument.SetParameterValue("ManuallyText", manuallyText);
            _crystalReportDocument.SetParameterValue("WeighMasterNameNumber", string.Format($"{weighMasterNameNumber.WeighMasterNumber}   {weighMasterNameNumber.WeighMasterName}"));

            // Reset the Weigh Master name the specified name
            string tmpWM = "*" + weighMasterNameNumber.UserName.ToString().Trim().ToUpper() + "*";
            string tmpWMO = "*" + Application.Current.Properties["WeighMasterNameOverride"].ToString().Trim().ToUpper() + "*";
            if (tmpWM != tmpWMO)
            {
                _crystalReportDocument.SetParameterValue("WeighMasterNameNumber", string.Format($"{Application.Current.Properties["WeighMasterNameOverride"].ToString()}"));
            }

            _crystalReportDocument.SetParameterValue("MaterialDescription", SelectedMaterial.Description ?? string.Empty);


            _crystalReportDocument.SetParameterValue("Gross", gross);
            _crystalReportDocument.SetParameterValue("Tare", tare);
            _crystalReportDocument.SetParameterValue("Net", net);
            _crystalReportDocument.SetParameterValue("Tons", tons);

            _crystalReportDocument.SetParameterValue("MaterialCost", materialCost);
            _crystalReportDocument.SetParameterValue("SalesTax", salesTax);
            _crystalReportDocument.SetParameterValue("Total", total);
            //_crystalReportDocument.SetParameterValue("Tended", CashTicket.Tended);  // String.Format("{0.00}", CashTicket.Tended));
            _crystalReportDocument.SetParameterValue("PaymentType", paymentType);
            _crystalReportDocument.SetParameterValue("Change", CashTicket.Change.ToString());

            _crystalReportDocument.SetParameterValue("IsHideDollarAmountToTicketTruck", isHideDollarAmountToTicketTruck);
            _crystalReportDocument.SetParameterValue("IsHideDollarAmountToTicketOffice", isHideDollarAmountToTicketOffice);
            _crystalReportDocument.SetParameterValue("IsHideDollarAmountToTicketCustomer", isHideDollarAmountToTicketCustomer);
            _crystalReportDocument.SetParameterValue("IsHideDollarAmountToTicketDispatch", isHideDollarAmountToTicketDispatch);

            _crystalReportDocument.SetParameterValue("TruckName", string.Empty);
            _crystalReportDocument.SetParameterValue("TruckPlate", string.Empty);
            _crystalReportDocument.SetParameterValue("TruckTag", string.Empty);

            // Set the Weigh Master signature to the specified image or not
            if (tmpWM != tmpWMO)
            {
                _crystalReportDocument.SetParameterValue("SignatureImageFilePath", _applicationPathReports + "Signature" + Path.DirectorySeparatorChar + "Blank.bmp");
            }
            else
            {
                // Load the signature file
                if (weighMasterNameNumber.SignatureFileName != "")
                    _crystalReportDocument.SetParameterValue("SignatureImageFilePath", _applicationPathReports + "Signature" + Path.DirectorySeparatorChar + weighMasterNameNumber.SignatureFileName + ".bmp");
                else
                    _crystalReportDocument.SetParameterValue("SignatureImageFilePath", _applicationPathReports + "Signature" + Path.DirectorySeparatorChar + weighMasterNameNumber.UserName + ".bmp");
            }

            _log.Info($"Generate Print Cash Customer - End");

            stopWatch.Stop();
            _log.Info($"Generate Print Ticket in Crystal Time to Process - {stopWatch.ElapsedMilliseconds}ms");

            #endregion

        }

        private void Print()
        {
            var stopWatch = System.Diagnostics.Stopwatch.StartNew();

            _log.Info($"Printing of Ticket Cash Customer - Begin");

            //-- Production
            try
            {
                PrintReportOptions crystalPrintReportOptions = new PrintReportOptions();
                crystalPrintReportOptions.PrinterName = IoC.Get<IConfiguration>().DefaultPrinterNameLocalComputer;
                crystalPrintReportOptions.RemoveAllPrinterPageRanges();
                crystalPrintReportOptions.AddPrinterPageRange(1, 1);
                _crystalReportDocument.ReportClientDocument.PrintOutputController.PrintReport(crystalPrintReportOptions);
            } catch (Exception ex)
            {
                _log.Error(ex);
                MessageBoxResult messageBoxResult4 = AutoClosingMessageBox.Show("Loc 4: Ticket printing error: " + ex.Message.ToString(), "Status", MessageBoxButton.OK, MessageBoxImage.Information, 5000);
            }

            _log.Info($"Printing of Ticket Cash Customer - End");
            stopWatch.Stop();
            _log.Info($"Printing of Ticket Cash Customer Time to Process - {stopWatch.ElapsedMilliseconds}ms");

        }

        private void Preview()
        {
            //var windowManager = IoC.Get<IWindowManager>();
            //_printModel.DefaultPrinter = SelectedCompany.PrinterTicketName ?? string.Empty;
            //_printModel.IsPrintActive = false;
            //windowManager.ShowDialog(_printModel);
        }

        private void SaveTicket()
        {
            var debugTextSavingTicketBegin = "Saving Ticket - Begin";
            Debug.WriteLine(debugTextSavingTicketBegin);
            _log.Info(debugTextSavingTicketBegin);



            CashTicket.UpdateTime = DateTime.Now;
            CashTicket.Quantity = NetWeight;

            CashTicket.CashData.Tare = FirstWeight;
            CashTicket.TruckWeight = SecondWeight;
            CashTicket.CashData.TaxExempt = IsExempt;
            if (Check)
            {
                CashTicket.CashData.PaymentForm = "Check";
                CashTicket.CashData.CheckNumber = CashTicket.CheckNumber;
            }
            else if (CreditCard)
            {
                CashTicket.CashData.PaymentForm = "CC";    // "Credit Card";
            }
            else if (Cash)
            {
                CashTicket.CashData.PaymentForm = "Cash";
            }

            CashTicket.CashData.Quote = CashRate;
            CashTicket.TicketId = IoC.Get<ITicketService>().SaveCashTicket(CashTicket, CashPurchase);

            // Write amounts
            CashTicket.DataA = CashTicket.Due.ToString() + "," + CashTicket.Tended.ToString() + "," + CashTicket.Change.ToString();

            var debugTextSavingTicketEnd = "Saving Cash Ticket - End";
            Debug.WriteLine(debugTextSavingTicketEnd);
            Debug.WriteLine($"Saved Cash ticket '{CashTicket.TicketNumber}'");

            _log.Info(debugTextSavingTicketEnd);
            _log.Info($"Saved Cash ticket '{CashTicket.TicketNumber}'");
        }

        private bool ValidateTicket()  // Validate Ticket
        {
            Errors = null;
            var str = new List<string>();
            if (FirstWeight <= 0)       // Originally <= 0
                str.Add("First Weight must be more than 0.");

            if (SecondWeight <= 0)      // Originally <= 0
                str.Add("Second Weight must be more than 0.");
            else if (SecondWeight <= FirstWeight && FirstWeight > 0)
                str.Add("Second Weight must be more than First Weight.");

            if (SecondWeight > 1500000)
                str.Add("Second Weight must be less than 1500000 lbs.");

            if (NetWeight < 0)      // Originally <= 0
                str.Add("Net Weight must be more than 0.");

            if (!CreditCard && !Cash && !Check)
                str.Add("You must select a payment type.");

            if (Check && string.IsNullOrWhiteSpace(CashTicket.CheckNumber))
                str.Add("You must enter the check number.");

            var tended = CashTicket.GetTendedValue(CashTicket.Tended);
            //if (Cash && CashTicket.Due > tended)
            //    str.Add("Tended must be equal or greater than Amount Due.");

            if (CashTicket.TicketNumber <= 0)
            {
                var messageValidateTicketNumberCurrentAtOrLessThanZero = $"The new ticket number should not be lower than the current ticket number of '{CashTicket.TicketNumber}'";
                str.Add(messageValidateTicketNumberCurrentAtOrLessThanZero);
                _log.Info(messageValidateTicketNumberCurrentAtOrLessThanZero);
            }

            bool isTicketExist = IoC.Get<ITicketService>().IsTicketExist(CashTicket.TicketNumber, CashTicket.TicketId, _seasonId);
            int nextTicketNumber = IoC.Get<ITicketService>().GetNextTicketNumber(CashTicket.CompanyId.Value);

            if (isTicketExist)
            {
                string messageValidateTicketNumberExistsCurrentNumberNextNumber = $"The ticket number '{CashTicket.TicketNumber}' already exists.  Changing number to next in sequence: '{nextTicketNumber}'";
                str.Add(messageValidateTicketNumberExistsCurrentNumberNextNumber);
                TicketNumber = nextTicketNumber.ToString();
                CashTicket.OldTicketNumber = nextTicketNumber;

                _log.Info(messageValidateTicketNumberExistsCurrentNumberNextNumber);
            }

            if (CashTicket.OldTicketNumber != CashTicket.TicketNumber)
            {
                if (CashTicket.OldTicketNumber > CashTicket.TicketNumber && nextTicketNumber <= 0)
                {
                    string messageValidateTicketNumberLower = "The new ticket number should not be lower than the current ticket number.";
                    str.Add(messageValidateTicketNumberLower);
                    _log.Info(messageValidateTicketNumberLower);
                }

                if (nextTicketNumber > 0 && nextTicketNumber > CashTicket.TicketNumber)
                {
                    string messageValidateTicketNumberHigher = "The new ticket number should not be higher than the current ticket number.";
                    str.Add(messageValidateTicketNumberHigher);
                    _log.Info(messageValidateTicketNumberHigher);
                }
            }

            if (SelectedCompanyData == null
                || SelectedCompanyData.FirstTicket > CashTicket.TicketNumber
                || SelectedCompanyData.LastTicket < CashTicket.TicketNumber)
            {
                string messageValidateTicketNumberOutsideOfDesignatedRange = $"The Ticket number is outside the company range of ticket numbers ({SelectedCompanyData.FirstTicket} - {SelectedCompanyData.LastTicket})";
                str.Add(messageValidateTicketNumberOutsideOfDesignatedRange);
                _log.Info(messageValidateTicketNumberOutsideOfDesignatedRange);
            }

            if (CashRate <= 0)
            {
                string messageValidatePricePerTonGreaterThanZero = "Price per ton must be greater than 0.";
                str.Add(messageValidatePricePerTonGreaterThanZero);
                _log.Info(messageValidatePricePerTonGreaterThanZero);
            }

            if (SelectedMaterial == null)
            {
                string messageValidateSelectMaterial = "Material must be selected.";
                str.Add(messageValidateSelectMaterial);
                _log.Info(messageValidateSelectMaterial);
            }

            if (str.Any())
            {
                Errors = string.Format(
                    "Validation Failed.{0}{1}", Environment.NewLine,
                    string.Join("\n", str));

                ShowMessage("Validation Errors", Errors, NotificationType.Warning);
                return false;
            }

            return true;
        }


        #endregion

        #region Events

        public CashCustomerViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            _log = LogManager.GetLog(typeof(CashCustomerViewModel));
            _seasonId = (int)Application.Current.Properties["SeasonId"];
            _scaleManager = IoC.Get<IScaleManager>();
            DisplayName = "Cash Purchase: Truck Registration and Ticket Generator";
        }

        
        public bool IsTrucksDescriptionFocused
        {
            get { return _isTrucksDescriptionFocused; }
            set
            {
                if (value.Equals(_isTrucksDescriptionFocused)) return;
                _isTrucksDescriptionFocused = value;
                NotifyOfPropertyChange(() => IsTrucksDescriptionFocused);
            }
        }

        protected override void OnInitialize()
        {
            try
            {
                base.OnInitialize();
                ScaleWidget = IoC.Get<ScaleWidgetViewModel>();
                ScaleWidget.ActivateWith(this);
                IsBusy = true;
                var task = new Task(InitTask);
                task.Start();
            }
            catch (Exception ex)
            {
                _log.Error(ex);

                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(
                            string.Format(
                                "Initialization failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                                ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                    TryClose(false);
                };
                action.OnUIThreadAsync();
            }
        }

        private void InitTask()
        { 
            try
            {
                // Set default statuses
                IsWeightOverrideEnabled = false;

                // Initialize the task      
                InitializeCashCustomer();
                InitializeCompany();
                InitializeMaterial();

                // Verify the ttruck
                if (!IsTrucksDescriptionFocused)
                {
                    IsTrucksDescriptionFocused = true;
                }

                // Wait to initialize the scale
                InitializeScale();
            }
            catch (Exception ex)
            {
                var exInnerMessageBoxText = string.Empty;

                _log.Error(ex);
                if (ex.InnerException != null)
                {
                    _log.Error(ex.InnerException);
                    exInnerMessageBoxText = $" and inner exception '{ex.InnerException.Message}'{Environment.NewLine} ";
                }

                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(
                            string.Format(
                                "Initialization failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                                ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                    TryClose(false);
                };
                action.OnUIThreadAsync();

            }
            finally
            {
                IsBusy = false;
            }
        }

        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                if (value.Equals(_isBusy)) return;
                _isBusy = value;
                NotifyOfPropertyChange(() => IsBusy);
            }
        }

        //public ISiteInfo Site { get; set; }

        protected override void OnActivate()
        {
            _eventAggregator.Subscribe(this);
            base.OnActivate();
        }

        protected override void OnDeactivate(bool close)
        {
            if (_scaleManager != null)
                StopScaleInteraction();

            _eventAggregator.Unsubscribe(this);

            if (_crystalReportDocument != null)
            {
                _crystalReportDocument.Close();
                _crystalReportDocument.Dispose();
            }

            base.OnDeactivate(close);
        }

        public void Handle(ScaleChangedArgs args)
        {
            var _wasPrintingEnabled = false;

            if (args != null)
            {
                if (IsPrintingEnabled)
                {
                    IsPrintingEnabled = false;
                    _wasPrintingEnabled = true;
                }

                if (_scaleManager != null && CashPurchase != null && !IsWeightOverrideEnabled && !Equals(SelectedScale, args.Scale))
                {
                    SelectedScale = args.Scale;
                    StopScaleInteraction();
                    StartScaleInteraction();
                }

                if (_wasPrintingEnabled)
                {
                    IsPrintingEnabled = true;
                }
            }
        }

        public void Handle(ScaleResultsArgs args)
        {
            try
            {
                // 2018-Dec-07 - Is Check-In or Check-Out is enabled
                if (SelectedCheckIn != null && SelectedReCheckIn != null)
                    return;

                if ((IsCheckInSelected == false) && (IsCheckOutSelected == false))
                    return;

                switch (args.Result.StreamPolarity)
                {
                    case ScaleIndicatorStreamPolarity.Negative:
                        //UI label to have text "Negative"
                        ScaleWidget.Errors = args.Result.StreamPolarity.GetDescription();
                        break;

                    case ScaleIndicatorStreamPolarity.Overload:
                        //UI label to have text "Overload"
                        ScaleWidget.Errors = args.Result.StreamPolarity.GetDescription();
                        break;

                    case ScaleIndicatorStreamPolarity.Underrange:
                        //UI label to have text "Underrange"
                        ScaleWidget.Errors = args.Result.StreamPolarity.GetDescription();
                        break;
                    case ScaleIndicatorStreamPolarity.Positive:
                        CashPurchase.Tare =
                            args.Result.StreamWeight.HasValue
                                ? args.Result.StreamWeight.Value.GetWeightInPounds(args.Result.StreamUnitOfWeight)
                                : 0;

                        // 2018-Dec-06, JGS: Add direct input to Ticket SecondWeight parameters
                        SecondWeight =
                            args.Result.StreamWeight.HasValue
                                ? args.Result.StreamWeight.Value.GetWeightInPounds(args.Result.StreamUnitOfWeight)
                                : 0;
                        SecondWeightText =
                            (args.Result.StreamWeight.HasValue
                                ? args.Result.StreamWeight.Value.GetWeightInPounds(args.Result.StreamUnitOfWeight)
                                : 0).ToString("N0");
                        // 2018-Dec-06, JGS: Add direct input to Ticket FirstWeight and SecondWeight parameters

                        ScaleWidget.Errors = null;
                        break;
                    case ScaleIndicatorStreamPolarity.Undefined:
                    default:
                        ScaleWidget.Errors = ScaleIndicatorStreamPolarity.Undefined.GetDescription();
                        break;
                }
            }
            catch (Exception ex)
            {
                var x = ex.Message;
            }
        }
        public void Handle(ScaleExceptionsArgs message)
        {
            //ManualScaling();
            //ShowMessage("Scale Errors", string.Format("Scale unavailable this time with exception: '{0}'", message.Message), NotificationType.Warning);
            _log.Warn($"Scale Handler: {message.Message}  Scale Id: {message.ScaleId}");
        }

        #endregion

        #region Properties
        public Color CheckBackground
        {
            get { return _checkBackground; }
            set
            {
                if (value.Equals(_checkBackground)) return;
                _checkBackground = value;
                NotifyOfPropertyChange(() => CheckBackground);
            }
        }

        public bool Cash
        {
            get { return _cash; }
            set
            {
                if (value.Equals(_cash)) return;
                _cash = value;
                NotifyOfPropertyChange(() => Cash);
                if (value)
                    InitCashOption();
                ReCalculate();
                CanPreviewTicket();
            }
        }

        public bool EnableTended
        {
            get { return _enableTended; }
            set
            {
                if (value.Equals(_enableTended)) return;
                _enableTended = value;
                NotifyOfPropertyChange(() => EnableTended);
            }
        }

        public bool EnableCheckNumber
        {
            get { return _enableCheckNumber; }
            set
            {
                if (value.Equals(_enableCheckNumber)) return;
                _enableCheckNumber = value;
                NotifyOfPropertyChange(() => EnableCheckNumber);
            }
        }

        public bool CreditCard
        {
            get { return _creditCard; }
            set
            {
                if (value.Equals(_creditCard)) return;
                _creditCard = value;
                NotifyOfPropertyChange(() => CreditCard);
                if (value)
                    InitCreditCardOption();
                ReCalculate();
                CanPreviewTicket();
            }
        }

        public bool Check
        {
            get { return _check; }
            set
            {
                if (value.Equals(_check)) return;
                _check = value;
                NotifyOfPropertyChange(() => Check);
                if (value)
                    InitCheckOption();
                ReCalculate();
                CanPreviewTicket();
            }
        }
        public bool IsExempt
        {
            get { return _isExempt; }
            set
            {
                if (value.Equals(_isExempt)) return;
                _isExempt = value;
                NotifyOfPropertyChange(() => IsExempt);
                ReCalculate();
            }
        }

        public decimal CashRate
        {
            get { return _cashRate; }
            set
            {
                if (value == _cashRate) return;
                _cashRate = value;
                NotifyOfPropertyChange(() => CashRate);
                ReCalculate();
            }
        }

        public int FirstWeight
        {
            get { return _firstWeight; }
            set
            {
                if (value == _firstWeight) return;
                _firstWeight = value;
                NotifyOfPropertyChange(() => FirstWeight);
                CalculateNet();
                ReCalculate();
            }
        }

        public int SecondWeight
        {
            get { return _secondWeight; }
            set
            {
                if (value == _secondWeight) return;
                _secondWeight = value;
                NotifyOfPropertyChange(() => SecondWeight);
                CalculateNet();
                ReCalculate();
                CanPreviewTicket();
            }
        }

        public string SecondWeightText
        {
            get { return _secondWeightText; }
            set
            {
                if (value == _secondWeightText) return;
                var secondWeight = ParseIntValue(value);
                SecondWeight = secondWeight ?? 0;
                _secondWeightText = secondWeight.HasValue ? secondWeight.Value.ToString("N0") : null;
                NotifyOfPropertyChange(() => SecondWeightText);
                CalculateNet();
                ReCalculate();
                CanPreviewTicket();
            }
        }

        public int NetWeight
        {
            get { return _netWeight; }
            set
            {
                if (value == _netWeight) return;
                _netWeight = value;
                NotifyOfPropertyChange(() => NetWeight);
                CalculateNet();
                ReCalculate();
            }
        }

        public decimal FuelSurcharge
        {
            get { return _fuelSurcharge; }
            set
            {
                if (value == _fuelSurcharge) return;
                _fuelSurcharge = value;
                NotifyOfPropertyChange(() => FuelSurcharge);

                //CashTicket.FuelSurcharge = ParseNullDecimalValue(value);
                CashTicket.FuelSurcharge = _fuelSurcharge;
            }
        }

        // _isTruckWeightEnabled
        public bool IsTruckWeightEnabled
        {
            get { return _isTruckWeightEnabled; }
            set
            {
                if (value.Equals(_isTruckWeightEnabled)) return;
                _isTruckWeightEnabled = value;
                NotifyOfPropertyChange(() => IsTruckWeightEnabled);
            }
        }

        // _isCancelTicketEnabled
        public bool IsCancelTicketEnabled
        {
            get { return _isCancelTicketEnabled; }
            set
            {
                if (value.Equals(_isCancelTicketEnabled)) return;
                _isCancelTicketEnabled = value;
                NotifyOfPropertyChange(() => IsCancelTicketEnabled);
            }
        }

        public bool IsPrintingEnabled
        {
            get { return _isPrintingEnabled; }
            set
            {
                if (value.Equals(_isPrintingEnabled)) return;
                _isPrintingEnabled = value;
                NotifyOfPropertyChange(() => IsPrintingEnabled);
            }
        }

        //private bool _isPanelReCheckInEnabled = true;
        public bool IsPanelReCheckInEnabled
        {
            get { return _isPanelReCheckInEnabled; }
            set
            {
                if (value.Equals(_isPanelReCheckInEnabled)) return;
                _isPanelReCheckInEnabled = value;
                NotifyOfPropertyChange(() => IsPanelReCheckInEnabled);
            }
        }

        //private bool _isPanelDoneEnabled = true;
        public bool IsPanelDoneEnabled
        {
            get { return _isPanelDoneEnabled; }
            set
            {
                if (value.Equals(_isPanelDoneEnabled)) return;
                _isPanelDoneEnabled = value;
                NotifyOfPropertyChange(() => IsPanelDoneEnabled);
            }
        }

        //private bool _isPanelCheckOutEnabled = true;
        public bool IsPanelCheckOutEnabled
        {
            get { return _isPanelCheckOutEnabled; }
            set
            {
                if (value.Equals(_isPanelCheckOutEnabled)) return;
                _isPanelCheckOutEnabled = value;
                NotifyOfPropertyChange(() => IsPanelCheckOutEnabled);
            }
        }

        #endregion

        #region Cash Ticket Preview

        // Redundant to Errors() for the CashCustomerViewModel
        //public string Errors
        //{
        //    get { return _errors; }
        //    set
        //    {
        //        if (value == _errors) return;
        //        _errors = value;
        //        NotifyOfPropertyChange(() => Errors);
        //    }
        //}

        // Redundant to CashPurchase() for the CashCustomerViewModel
        //public CashPurchaseDataModel CashPurchase
        //{
        //    get { return _cashPurchase; }
        //    set
        //    {
        //        if (Equals(value, _cashPurchase)) return;
        //        _cashPurchase = value;
        //        NotifyOfPropertyChange(() => CashPurchase);
        //        Errors = null;
        //    }
        //}

        public CashPurchaseTicketDataModel CashTicket
        {
            get { return _cashTicket; }
            set
            {
                if (Equals(value, _cashTicket)) return;
                _cashTicket = value;
                NotifyOfPropertyChange(() => CashTicket);
                Errors = null;
            }
        }
        public string CheckNumber
        {
            get { return _checkNumber; }
            set
            {
                if (value == _checkNumber) return;
                _checkNumber = value;
                NotifyOfPropertyChange(() => CheckNumber);
                if (CashTicket != null)
                    CashTicket.CheckNumber = _checkNumber;
                CanPreviewTicket();
            }
        }
        public string TicketNumber
        {
            get { return _ticketNumber; }
            set
            {
                if (value == _ticketNumber) return;
                _ticketNumber = value;
                NotifyOfPropertyChange(() => TicketNumber);

                var result = ParseIntValue(_ticketNumber);

                CashTicket.TicketNumber = result.HasValue ? result.Value : 0;
                CanPreviewTicket();
            }
        }
        private void InitCashOption()
        {
            EnableTended = true;
            EnableCheckNumber = false;
            if (CashTicket != null)
            {
                CashTicket.CheckNumber = string.Empty;
                CheckBackground = Colors.White;
                CashTicket.Tended = CashTicket.Due.ToString("N2");  //0.ToString("N2");
                ReCalculate();
                CanPreviewTicket();
            }
        }

        private void InitCreditCardOption()
        {
            EnableTended = false;
            EnableCheckNumber = false;
            if (CashTicket != null)
            {
                CashTicket.CheckNumber = string.Empty;
                CheckBackground = Colors.White;
                CashTicket.Tended = CashTicket.Due.ToString("N2");
                ReCalculate();
                CanPreviewTicket();
            }
        }

        private void InitCheckOption()
        {
            EnableTended = false;
            EnableCheckNumber = true;
            if (CashTicket != null)
            {
                CheckBackground = Colors.LightYellow;
                CashTicket.Tended = CashTicket.Due.ToString("N2");
                ReCalculate();
                CanPreviewTicket();
            }
        }


        private void ReCalculate()
        {
            //CashTicket.MaterialCharge = TruncateDecimals(CashRate * (NetWeight / 2000m));
            //CashTicket.Tax = TruncateDecimals(CashTicket.TaxRate * CashTicket.MaterialCharge / 100m);
            if (CashTicket != null)
            {
                CashTicket.MaterialCharge = (NetWeight > 0) ? (CashRate * CashTicket.NetWeightTon) : 0;
                CashTicket.Tax = CashTicket.TaxRate == 0 ? 0 : (CashTicket.TaxRate * CashTicket.MaterialCharge);

                if (IsExempt)
                    CashTicket.Tax = 0m;

                CashTicket.Due = TruncateDecimals(CashTicket.MaterialCharge + CashTicket.Tax);

                if (!Check && !CreditCard && !Cash)
                    CashTicket.Tended = 0.ToString("N2");
                if (Check || CreditCard)
                    CashTicket.Tended = CashTicket.Due.ToString("N2");
            }
        }

        private void CalculateNet()
        {
            NetWeight = SecondWeight - FirstWeight;
            if (CashTicket != null)
            {
                CashTicket.NetWeightTon = TruncateDecimals(NetWeight / 2000m);
            }
        }

        private void DeleteTicketInfo()
        {
            //// Initial Setup
            //CashPurchase.TruckDescription = "";
            //CashPurchase.Tare = 0;
            //FirstWeight = 0;
            //SecondWeight = 0;

            if (CashTicket != null)
            {
                // Reset all controls to null
                CashTicket.TruckDescription = null;
                TicketNumber = null;
                FirstWeight = 0;
                SecondWeight = 0;
                SecondWeightText = "0";
                NetWeight = 0;
                CashTicket.NetWeightTon = 0;
                CashRate = 0;
                CashTicket.MaterialCharge = 0;
                IsExempt = false;
                CashTicket.Tax = 0;
                CashTicket.Due = 0;
                CashTicket.Tended = "0";
                CashTicket.CheckNumber = "";
                CashTicket.Change = 0;

                // Reset Status
                CashTicket.IsFirstWeightEnabled = true;
                CashTicket.IsSecondWeightEnabled = false;

                // Set payment default type
                Cash = true;

                // Recalculate the total
                ReCalculate();

                // Delete the ticket
                CashTicket = null;
            }
        }

        private void InitializeTicket()
        {
            InitializeNewTicket();
        }

        private void InitializeNewTicket()
        {
            // Reset and create the cash ticket
            CashTicket = null;
            CashTicket = new CashPurchaseTicketDataModel(IoC.Get<ITicketService>().GetNewCashTicket(_seasonId, _defaultSite, CashPurchase))
            {
                TruckDescription = CashPurchase.TruckDescription
            };

            // Ticket Manual Scaling
            CashTicket.IsFirstWeightEnabled = false;
            CashTicket.IsSecondWeightEnabled = true;

            TicketNumber = CashTicket.TicketNumber.ToString();
            FirstWeight = CashPurchase.Tare != null ? CashPurchase.Tare.Value : 0;
            //var secondWeight = (int)(CashTicket.Quantity * 2000);
            //SecondWeightText = secondWeight == 0 ? null : secondWeight.ToString("N0");
            CalculateNet();

            IMaterialInfo materialInfo = MaterialList.FirstOrDefault(m => m.Id == CashPurchase.Material.Id);
            SelectedMaterial = materialInfo;
            //if (materialInfo == null)
            //{
            //    var inactiveMaterial = IoC.Get<IMaterialService>().GetInactiveMaterial(CashPurchase.MaterialId);
            //    if (inactiveMaterial != null)
            //    {
            //        MaterialList.Add(inactiveMaterial);
            //        SelectedMaterial = inactiveMaterial;
            //    }
            //}
            //else
            //{
            //    SelectedMaterial = materialInfo;
            //}

            CompanyListData = IoC.Get<ICommonService>().GetCompaniesData();
            ICompanyData company = CompanyListData.FirstOrDefault(m => m.Id == CashPurchase.CompanyId);

            if (company != null)
            {
                SelectedCompanyData = company;
                TicketNumber = company.NextTicket.ToString();
                CashTicket.OldTicketNumber = company.NextTicket;
            }

            //TODO CHECK CASH RATE
            CashRate = CashPurchase.Quote != null ? CashPurchase.Quote.Value : 0;
            IsExempt = CashPurchase.TaxExempt;

            TaxRateLabel = $"Tax ({(CashTicket.TaxRate * 100).ToString("N2")}%)";

            // Set payment default type
            Cash = true;

            // Recalculate the total
            ReCalculate();

            //FuelSurcharge = CashTicket.FuelSurcharge.HasValue ? CashTicket.FuelSurcharge.Value.ToString("N2") : null;
            //FuelSurcharge = _fuelSurcharge;
        }

        public string TaxRateLabel
        {
            get { return _taxRateLabel; }
            set
            {
                if (value == _taxRateLabel) return;
                _taxRateLabel = value;
                NotifyOfPropertyChange(() => TaxRateLabel);
            }
        }

        private static decimal TruncateDecimals(decimal value)
        {
            //return decimal.Truncate(value*100)/100;
            return decimal.Round(value, 2);
        }

        private decimal? ParseNullDecimalValue(string income)
        {
            if (string.IsNullOrWhiteSpace(income))
                return null;

            income = income.Replace('_', '0');

            decimal result;

            if (decimal.TryParse(income.Trim(), out result))
                return result;

            return null;
        }

        private int? ParseIntValue(string income)
        {
            if (string.IsNullOrWhiteSpace(income))
                return null;

            income = income.Replace(",", string.Empty);
            income = income.Replace("'", string.Empty);
            income = income.Replace(".", string.Empty);

            int result;

            if (int.TryParse(income.Trim(), out result))
                return result;

            return null;
        }

        #endregion

        #region Ticket Company
        public IReadOnlyList<ICompanyData> CompanyListData
        {
            get { return _companyListData; }
            set
            {
                if (Equals(value, _companyListData)) return;
                _companyListData = value;
                NotifyOfPropertyChange(() => CompanyListData);
            }
        }

        public ICompanyData SelectedCompanyData
        {
            get { return _selectedCompanyData; }
            set
            {
                if (Equals(value, _selectedCompanyData)) return;
                _selectedCompanyData = value;
                NotifyOfPropertyChange(() => SelectedCompanyData);
                TicketNumber = SelectedCompanyData.NextTicket.ToString();
            }
        }

        #endregion

        #region Ticket Material


        #endregion


        public void Handle(CashPurchaseModifiedArgs args)
        {
            if (args != null)
            {
                InitializeCashCustomer();
            }
        }
    }
}