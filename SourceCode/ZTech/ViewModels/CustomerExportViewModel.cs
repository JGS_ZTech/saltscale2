﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.IO;
using System.Windows;
using System.Windows.Forms.Integration;
using Caliburn.Micro;
using Contract.Services;
using Microsoft.Reporting.WinForms;
using ZTech.Reports;
using ZTech.Helpers;
using Screen = Caliburn.Micro.Screen;

namespace ZTech.ViewModels
{
    public class CustomerExportViewModel : Screen
    {
        private readonly IEventAggregator _eventAggregator;
        private readonly ILog _log;
        private WindowsFormsHost _viewer;
        private readonly int _seasonId;
        private ReportDataSource ReportDataSource { get; set; }

        public CustomerExportViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            _log = LogManager.GetLog(typeof(CustomerExportViewModel));
            _seasonId = (int)Application.Current.Properties["SeasonId"];
            DisplayName = "Customer Export";
        }

        protected override void OnInitialize()
        {
            var windowsFormsHost = new WindowsFormsHost();
            var reportViewer = new ReportViewer();
            windowsFormsHost.Child = reportViewer;
            Viewer = windowsFormsHost;

            var assemblyPath = System.Reflection.Assembly.GetExecutingAssembly().Location;

            var path = Path.Combine(Path.GetDirectoryName(assemblyPath), "Reports", "CustomerExport.rdlc");

            reportViewer.LocalReport.ReportPath = path;

            ReportDataSource = new ReportDataSource
            {
                Name = "CustomerExportDataSet",
                Value = IoC.Get<IReportService>().GetCustomerExportData(_seasonId)
            };
            reportViewer.LocalReport.DataSources.Add(ReportDataSource);
            reportViewer.RefreshReport();

            base.OnInitialize();
        }

        public WindowsFormsHost Viewer
        {
            get { return _viewer; }
            set
            {
                if (Equals(value, _viewer)) return;
                _viewer = value;
                NotifyOfPropertyChange(() => Viewer);
            }
        }
    }
}