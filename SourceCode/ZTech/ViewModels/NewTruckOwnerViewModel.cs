﻿using Caliburn.Micro;
using Contract.Domain;
using Contract.Services;
using ZTech.ViewModels.Abstract;
using ZTech.ViewModels.ViewObject;

namespace ZTech.ViewModels
{
    public class NewTruckOwnerViewModel : BaseTruckOwnerViewModel
    {
        public NewTruckOwnerViewModel(IEventAggregator eventAggregator)
            : base(eventAggregator, "New Truck Owner Screen")
        {
            _log = LogManager.GetLog(typeof (NewTruckOwnerViewModel));
        }

        protected override void InitializeTruckOwner()
        {
            ITruckOwnerData newOwner = IoC.Get<ITruckService>().GetNewTruckOwner(_seasonId);
            Owner = new TruckOwnerDataModel(newOwner);
        }
    }
}