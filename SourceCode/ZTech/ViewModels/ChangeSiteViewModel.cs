﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Caliburn.Micro;
using Contract.Domain;
using Contract.Events;
using Contract.Services;
using ZTech.Controls;
using ZTech.ViewModels.Abstract;

namespace ZTech.ViewModels
{
    public class ChangeSiteViewModel : BaseViewModel
    {
        private readonly IConfiguration _configuration;
        private readonly IEventAggregator _eventAggregator;
        private readonly ILog _log;
        private string _defaultSiteName;
        private Boolean _useLowellData = false;
        private Boolean _optionLowellData = false;
        private string _useLowellForegroundColor = "White";
        private int _UseLowellFontHeight = 0;
        private string _errors;
        private ISiteConfig _selectedSite;
        private List<ISiteConfig> _siteList;
        private string _selectedSiteName;
        private List<string> _SitesNamesList;
        private string _selectedSeasonName;
        private List<string> _SeasonsNamesList;


        public ChangeSiteViewModel(IEventAggregator eventAggregator, IConfiguration configuration)
        {
            _eventAggregator = eventAggregator;
            _configuration = configuration;
            _log = LogManager.GetLog(typeof(ChangeSiteViewModel));
            DisplayName = "Change Site";
        }

        public List<ISiteConfig> SiteList
        {
            get { return _siteList; }
            set
            {
                if (Equals(value, _siteList)) return;
                _siteList = value;
                NotifyOfPropertyChange(() => SiteList);
            }
        }

        public List<string> SitesNamesList
        {
            get { return _SitesNamesList; }
            set
            {
                if (Equals(value, _SitesNamesList)) return;
                _SitesNamesList = value;
                NotifyOfPropertyChange(() => SitesNamesList);
            }
        }

        public List<string> SeasonsNamesList
        {
            get { return _SeasonsNamesList; }
            set
            {
                if (Equals(value, _SeasonsNamesList)) return;
                _SeasonsNamesList = value;
                NotifyOfPropertyChange(() => SeasonsNamesList);
            }
        }

        public ISiteConfig SelectedSite
        {
            get { return _selectedSite; }
            set
            {
                if (Equals(value, _selectedSite)) return;
                _selectedSite = value;
                NotifyOfPropertyChange(() => SelectedSite);
            }
        }

        public string SelectedSiteName
        {
            get { return _selectedSiteName; }
            set
            {
                if (Equals(value, _selectedSiteName)) return;
                _selectedSiteName = value;
                NotifyOfPropertyChange(() => SelectedSiteName);

                //// Limit the SeasonNameList
                //ResetSeasonaNameList(_selectedSiteName);
                //NotifyOfPropertyChange(() => SeasonsNamesList);

                // Set the default for the selected season
                SelectedSeasonName = "Current";
            }
        }

        public string DefaultSiteName
        {
            get { return _defaultSiteName; }
            set
            {
                if (Equals(value, _defaultSiteName)) return;
                _defaultSiteName = value;
                NotifyOfPropertyChange(() => DefaultSiteName);
            }
        }

        public Boolean UseLowellData
        {
            get { return _useLowellData; }
            set
            {
                if (Equals(value, _useLowellData)) return;
                _useLowellData = value;
                NotifyOfPropertyChange(() => UseLowellData);
            }
        }

        public Boolean OptionLowellData
        {
            get { return _optionLowellData; }
            set
            {
                if (Equals(value, _optionLowellData)) return;
                _optionLowellData = value;
                NotifyOfPropertyChange(() => OptionLowellData);
            }
        }

        public string UseLowellForegroundColor
        {
            get { return _useLowellForegroundColor; }
            set
            {
                if (Equals(value, _useLowellForegroundColor)) return;
                _useLowellForegroundColor = value;
                NotifyOfPropertyChange(() => UseLowellForegroundColor);
            }
        }

        public int UseLowellFontHeight
        {
            get { return _UseLowellFontHeight; }
            set
            {
                if (Equals(value, _UseLowellFontHeight)) return;
                _UseLowellFontHeight = value;
                NotifyOfPropertyChange(() => UseLowellFontHeight);
            }
        }


        public string SelectedSeasonName
        {
            get { return _selectedSeasonName; }
            set
            {
                if (Equals(value, _selectedSeasonName)) return;
                _selectedSeasonName = value;
                NotifyOfPropertyChange(() => SelectedSeasonName);
            }
        }

        public string Errors
        {
            get { return _errors; }
            set
            {
                if (value == _errors) return;
                _errors = value;
                NotifyOfPropertyChange(() => Errors);
            }
        }

        #region Actions

        public void Cancel()
        {
            TryClose(false);
        }

        public void KeyHandle(Key key)
        {
            if (key == Key.F2)
            {
                Done();
            }
        }

        public void Done()
        {
            // Replications location
            string lowellSource = _configuration.saltScaleReplicationHostServer; //"Hephaestus";   saltScaleReplicationHostServer

            try
            {
                // Get the current Site/Season selections and make a Selected Site
                Boolean foundit = false;
                string localSiteFull = SelectedSiteName;
                if (_selectedSeasonName != "Current")
                    localSiteFull = SelectedSeasonName + "_" + localSiteFull;
                foreach (ISiteConfig localSite in SiteList)
                {
                    // If the Site and Season match then reset the "SelectedSite" 
                    if (localSite.Name.ToString().ToUpper().Trim() == localSiteFull.ToUpper().Trim())
                    {
                        SelectedSite = localSite;
                        foundit = true;

                        // Post Process
                        if (_selectedSeasonName == "Current" && OptionLowellData == true && UseLowellData == true)
                        {
                            // Update link with the 
                            string localConnection = SelectedSite.ConnectionString.ToString();
                            string strFirst = "data source=";
                            int locFirst = localConnection.IndexOf(strFirst) + 12;
                            string strSecond = ";initial catalog=";
                            int locSecond = localConnection.IndexOf(strSecond) - 1;

                            localConnection = localConnection.Substring(0, locFirst) + lowellSource + ";initial catalog=repl_" + localConnection.Substring(locSecond + 18, localConnection.Length - locSecond - 18);
                            SelectedSite.ConnectionString = localConnection;
                        }
                    }
                }
                // If REPL data is selectied and 
                if (_selectedSeasonName != "Current" && OptionLowellData == true && UseLowellData == true)
                {
                    foundit = false;
                }
                if (!foundit)
                {
                    MessageBoxResult notificationBoxResult =
                      (MessageBox.Show("The Site and Season combination you selected cannot be located. Please try again.", "Site/Season Not Found",
                          MessageBoxButton.OK, MessageBoxImage.Information));
                    return;
                }

                if (!IsValid())
                    return;

                MessageBoxResult messageBoxResult =
                  (MessageBox.Show("Are you sure, you want to change Site?", "Confirmation",
                      MessageBoxButton.YesNo, MessageBoxImage.Question));

                if (messageBoxResult == MessageBoxResult.No)
                {
                    return;
                }

                _eventAggregator.PublishOnUIThreadAsync(new ChangeSiteArgs(SelectedSite));

                TryClose(true);
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Errors = string.Format(
                    "Operation failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                    ex.Message);

                ShowMessage("Error", Errors, NotificationType.Error);
            }
        }

        private bool IsValid()
        {
            if (SelectedSite == null)
            {
                Errors = string.Format(
                    "Validation Failed. {0} {1}", Environment.NewLine, "Site cannot be Empty.");

                ShowMessage("Validation Errors", Errors, NotificationType.Warning);
                return false;
            }

            return true;
        }

        #endregion

        protected override void OnInitialize()
        {
            base.OnInitialize();
            SiteList = _configuration.ExistingSites;
            ISiteConfig site = _configuration.SelectedSite;

            // Get the default site name
            string siteDefault = _configuration.DefaultSiteName;

            // Get the default site name and set "Change Site" defaults
            UseLowellData = false;
            OptionLowellData = false;
            UseLowellForegroundColor = "White";
            UseLowellFontHeight = 0;
            if (siteDefault.ToUpper().Trim().ToString() == "LOWELL")
            {
                UseLowellData = true;
                OptionLowellData = true;
                UseLowellForegroundColor = "Black";
                UseLowellFontHeight = 32;
            }

            // Break down lists by site and year
            string localSiteName;
            string localSeasonName;
            string localItem;
            int localItemPosition;
            foreach (ISiteConfig localSite in SiteList)
            {
                // Extract the site name
                localItem = localSite.Name.ToString();

                // Parse the name and year
                localItemPosition = localItem.IndexOf("_") - 1;
                if (localItemPosition == 3)
                {
                    // Name and String
                    localSiteName = localItem.Substring(localItemPosition + 2, localItem.Length - localItemPosition - 2);
                    localSeasonName = localItem.Substring(0, localItemPosition + 1);
                }
                else
                {
                    // Assign the name and current season
                    localSiteName = localItem;
                    localSeasonName = "Current";
                }

                // Add the site name to the name list
                if (SitesNamesList == null)
                {
                    SitesNamesList = new List<string>();
                    SitesNamesList.Add(localSiteName);
                }
                else
                {
                    // Is the item already in the list?
                    Boolean foundIt = false;
                    foreach (string localSiteNameList in SitesNamesList)
                    {
                        if (localSiteName == localSiteNameList)
                        {
                            foundIt = true;
                        }
                    }

                    // Add it to the SITE list if not found
                    if (!foundIt)
                    {
                        SitesNamesList.Add(localSiteName);
                    }

                }

                // Add the site year to the year list?
                if (SeasonsNamesList == null)
                {
                    SeasonsNamesList = new List<string>();
                    SeasonsNamesList.Add("Current");
                }
                else
                {
                    // Is the item already in the list
                    Boolean foundIt = false;
                    foreach (string localSeasonNameList in SeasonsNamesList)
                    {
                        if (localSeasonName == localSeasonNameList)
                        {
                            foundIt = true;
                        }
                    }

                    // Add it to the YEAR list if not found
                    if (!foundIt)
                    {
                        SeasonsNamesList.Add(localSeasonName);
                    }
                }
            }

            // Set the default Site
            SelectedSite = SiteList.First(s => s.Name == site.Name);

            // Extract the site name and year
            localItem = SelectedSite.Name.ToString();

            // Parse the name and year
            localItemPosition = localItem.IndexOf("_") - 1;
            if (localItemPosition == 3)
            {
                // Name and String
                localSiteName = localItem.Substring(localItemPosition + 2, localItem.Length - localItemPosition - 2);
                localSeasonName = localItem.Substring(0, localItemPosition + 1);
            }
            else
            {
                // Assign the name and current season
                localSiteName = localItem;
                localSeasonName = "Current";
            }

            SelectedSiteName = SitesNamesList.First(s => s.ToString() == localSiteName);
            SelectedSeasonName = SeasonsNamesList.First(s => s.ToString() == localSeasonName);
        }

        protected void ResetSeasonaNameList(string newSiteName)
        {
            // Break down the SeasonNameList
            SeasonsNamesList = new List<string>();
            SeasonsNamesList.Add("Current");

            // Break down lists by site and year
            string localSiteName = "";
            string localSeasonName = "";
            string localItem;
            int localItemPosition;
            foreach (ISiteConfig localSite in SiteList)
            {
                // Extract the site name
                localItem = localSite.Name.ToString();

                // Parse the name and year
                localItemPosition = localItem.IndexOf("_") - 1;
                if (localItemPosition == 3)
                {
                    // Name and String
                    localSiteName = localItem.Substring(localItemPosition + 2, localItem.Length - localItemPosition - 2);
                    localSeasonName = localItem.Substring(0, localItemPosition + 1);
                }
                else
                {
                    // Name and String
                    localSiteName = localItem;
                    localSeasonName = "Current";
                }

                // Locate the site
                if ((localSiteName == newSiteName) && (localSeasonName != "") && (localSeasonName != "Current"))
                {
                    // Add the year to the season list
                    SeasonsNamesList.Add(localSeasonName);
                }
            }

            // Set the default season name to Current
            SelectedSeasonName = "Current";
        }
    }
}