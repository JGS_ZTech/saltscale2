﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;
using Caliburn.Micro;
using Contract.Domain;
using Contract.Events;
using Contract.Services;
using ZTech.Controls;
using ZTech.Helpers;
using ZTech.Reports;
using ZTech.ViewModels.Abstract;
using ZTech.ViewModels.ViewObject;
using Action = System.Action;
using System.IO;
using CrystalDecisions.ReportAppServer.Controllers;
using CrystalDecisions.Shared;
using System.Diagnostics;

namespace ZTech.ViewModels
{
    public interface ICashPurchaseTicketViewModel : IScreen
    {

    }

    public class CashPurchaseTicketViewModel : BaseViewModel, IHandle<ScaleChangedArgs>, IHandle<ScaleResultsArgs>, IHandle<ScaleExceptionsArgs>
    {
        private readonly IEventAggregator _eventAggregator;
        private readonly ILog _log;
        private string _errors;
        private readonly int _seasonId;
        private Color _checkBackground;
        private bool _cash;
        private bool _enableTended;
        private bool _enableCheckNumber;
        private bool _creditCard;
        private bool _check;
        private bool _isExempt;
        private decimal _cashRate;
        private int _firstWeight;
        private int _secondWeight;
        private int _netWeight;
        //private TicketPreViewModel _printModel;
        //Cash Ticket Information
        private CashPurchaseTicketDataModel _cashTicket;
        //Cash Customer Information
        private CashPurchaseDataModel _cashPurchase;
        //Company Information
        private IReadOnlyList<ICompanyData> _companyList;
        private ICompanyData _selectedCompany;
        //Material Information
        private List<IMaterialInfo> _materialList;
        private IMaterialInfo _selectedMaterial;
        //Scale Information
        private ScaleWidgetViewModel _scaleWidget;
        private string _defaultSite;
        private string _taxRateLabel;
        private decimal _fuelSurcharge;
        private readonly IScaleManager _scaleManager;
        private bool _isBusy;
        private string _secondWeightText;
        private bool _isPrintingEnabled;
        private string _checkNumber;
        private string _ticketNumber;

        private CrystalDecisions.CrystalReports.Engine.ReportDocument _crystalReportDocument;

        #region Properties
        public Color CheckBackground
        {
            get { return _checkBackground; }
            set
            {
                if (value.Equals(_checkBackground)) return;
                _checkBackground = value;
                NotifyOfPropertyChange(() => CheckBackground);
            }
        }

        public bool Cash
        {
            get { return _cash; }
            set
            {
                if (value.Equals(_cash)) return;
                _cash = value;
                NotifyOfPropertyChange(() => Cash);
                if (value)
                    InitCashOption();
                ReCalculate();
                CanPreviewTicket();
            }
        }

        public bool EnableTended
        {
            get { return _enableTended; }
            set
            {
                if (value.Equals(_enableTended)) return;
                _enableTended = value;
                NotifyOfPropertyChange(() => EnableTended);
            }
        }

        public bool EnableCheckNumber
        {
            get { return _enableCheckNumber; }
            set
            {
                if (value.Equals(_enableCheckNumber)) return;
                _enableCheckNumber = value;
                NotifyOfPropertyChange(() => EnableCheckNumber);
            }
        }

        public bool CreditCard
        {
            get { return _creditCard; }
            set
            {
                if (value.Equals(_creditCard)) return;
                _creditCard = value;
                NotifyOfPropertyChange(() => CreditCard);
                if (value)
                    InitCreditCardOption();
                ReCalculate();
                CanPreviewTicket();
            }
        }

        public bool Check
        {
            get { return _check; }
            set
            {
                if (value.Equals(_check)) return;
                _check = value;
                NotifyOfPropertyChange(() => Check);
                if (value)
                    InitCheckOption();
                ReCalculate();
                CanPreviewTicket();
            }
        }
        public bool IsExempt
        {
            get { return _isExempt; }
            set
            {
                if (value.Equals(_isExempt)) return;
                _isExempt = value;
                NotifyOfPropertyChange(() => IsExempt);
                ReCalculate();
            }
        }

        public decimal CashRate
        {
            get { return _cashRate; }
            set
            {
                if (value == _cashRate) return;
                _cashRate = value;
                NotifyOfPropertyChange(() => CashRate);
                ReCalculate();
            }
        }

        public int FirstWeight
        {
            get { return _firstWeight; }
            set
            {
                if (value == _firstWeight) return;
                _firstWeight = value;
                NotifyOfPropertyChange(() => FirstWeight);
                CalculateNet();
                ReCalculate();
            }
        }

        public int SecondWeight
        {
            get { return _secondWeight; }
            set
            {
                if (value == _secondWeight) return;
                _secondWeight = value;
                NotifyOfPropertyChange(() => SecondWeight);
                CalculateNet();
                ReCalculate();
                CanPreviewTicket();
            }
        }

        public string SecondWeightText
        {
            get { return _secondWeightText; }
            set
            {
                if (value == _secondWeightText) return;
                var secondWeight = ParseIntValue(value);
                SecondWeight = secondWeight ?? 0;
                _secondWeightText = secondWeight.HasValue ? secondWeight.Value.ToString("N0") : null;
                NotifyOfPropertyChange(() => SecondWeightText);
            }
        }

        public int NetWeight
        {
            get { return _netWeight; }
            set
            {
                if (value == _netWeight) return;
                _netWeight = value;
                NotifyOfPropertyChange(() => NetWeight);
                CalculateNet();
                ReCalculate();
            }
        }

        public decimal FuelSurcharge
        {
            get { return _fuelSurcharge; }
            set
            {
                if (value == _fuelSurcharge) return;
                _fuelSurcharge = value;
                NotifyOfPropertyChange(() => FuelSurcharge);

                //CashTicket.FuelSurcharge = ParseNullDecimalValue(value);
                CashTicket.FuelSurcharge = _fuelSurcharge;
            }
        }

        public bool IsPrintingEnabled
        {
            get { return _isPrintingEnabled; }
            set
            {
                if (value.Equals(_isPrintingEnabled)) return;
                _isPrintingEnabled = value;
                NotifyOfPropertyChange(() => IsPrintingEnabled);
            }
        }

        #endregion

        #region Cash Ticket Preview

        public string Errors
        {
            get { return _errors; }
            set
            {
                if (value == _errors) return;
                _errors = value;
                NotifyOfPropertyChange(() => Errors);
            }
        }

        public CashPurchaseDataModel CashPurchase
        {
            get { return _cashPurchase; }
            set
            {
                if (Equals(value, _cashPurchase)) return;
                _cashPurchase = value;
                NotifyOfPropertyChange(() => CashPurchase);
                Errors = null;
            }
        }

        public CashPurchaseTicketDataModel CashTicket
        {
            get { return _cashTicket; }
            set
            {
                if (Equals(value, _cashTicket)) return;
                _cashTicket = value;
                NotifyOfPropertyChange(() => CashTicket);
                Errors = null;
            }
        }
        public string CheckNumber
        {
            get { return _checkNumber; }
            set
            {
                if (value == _checkNumber) return;
                _checkNumber = value;
                NotifyOfPropertyChange(() => CheckNumber);
                if (CashTicket != null)
                    CashTicket.CheckNumber = _checkNumber;
                CanPreviewTicket();
            }
        }
        public string TicketNumber
        {
            get { return _ticketNumber; }
            set
            {
                if (value == _ticketNumber) return;
                _ticketNumber = value;
                NotifyOfPropertyChange(() => TicketNumber);
                
                var result = ParseIntValue(_ticketNumber);
                
                CashTicket.TicketNumber = result.HasValue ? result.Value : 0;
                CanPreviewTicket();
            }
        }
        private void InitCashOption()
        {
            EnableTended = true;
            EnableCheckNumber = false;
            CashTicket.CheckNumber = string.Empty;
            CheckBackground = Colors.White;
            CashTicket.Tended = 0.ToString("N2");
            ReCalculate();
            CanPreviewTicket();
        }

        private void InitCreditCardOption()
        {
            EnableTended = false;
            EnableCheckNumber = false;
            CashTicket.CheckNumber = string.Empty;
            CheckBackground = Colors.White;
            CashTicket.Tended = CashTicket.Due.ToString("N2");
            ReCalculate();
            CanPreviewTicket();
        }

        private void InitCheckOption()
        {
            EnableTended = false;
            EnableCheckNumber = true;
            CheckBackground = Colors.LightYellow;
            CashTicket.Tended = CashTicket.Due.ToString("N2");
            ReCalculate();
            CanPreviewTicket();
        }


        private void ReCalculate()
        {
            //CashTicket.MaterialCharge = TruncateDecimals(CashRate * (NetWeight / 2000m));
            //CashTicket.Tax = TruncateDecimals(CashTicket.TaxRate * CashTicket.MaterialCharge / 100m);
            if (CashTicket != null)
            {
                CashTicket.MaterialCharge = (NetWeight > 0) ? (CashRate * CashTicket.NetWeightTon) : 0;
                CashTicket.Tax = CashTicket.TaxRate == 0 ? 0 : (CashTicket.TaxRate * CashTicket.MaterialCharge);

                if (IsExempt)
                    CashTicket.Tax = 0m;

                CashTicket.Due = TruncateDecimals(CashTicket.MaterialCharge + CashTicket.Tax);

                if (!Check && !CreditCard && !Cash)
                    CashTicket.Tended = 0.ToString("N2");
                if (Check || CreditCard)
                    CashTicket.Tended = CashTicket.Due.ToString("N2");
            }
        }

        private void CalculateNet()
        {
            NetWeight = SecondWeight - FirstWeight;
            if (CashTicket != null)
            {
                CashTicket.NetWeightTon = TruncateDecimals(NetWeight / 2000m);
            }
        }

        private void InitializeTicket()
        {
            InitializeNewTicket();
        }

        private void InitializeNewTicket()
        {
            CashTicket = new CashPurchaseTicketDataModel(IoC.Get<ITicketService>().GetNewCashTicket(_seasonId, _defaultSite, CashPurchase))
            {
                TruckDescription = CashPurchase.TruckDescription
            };

            TicketNumber = CashTicket.TicketNumber.ToString();
            FirstWeight = CashPurchase.Tare != null ? CashPurchase.Tare.Value : 0;
            var secondWeight = (int)(CashTicket.Quantity * 2000);
            SecondWeightText = secondWeight == 0 ? null : secondWeight.ToString("N0");
            CalculateNet();

            IMaterialInfo materialInfo = MaterialList.FirstOrDefault(m => m.Id == CashPurchase.MaterialId);

            if (materialInfo == null)
            {
                var inactiveMaterial = IoC.Get<IMaterialService>().GetInactiveMaterial(CashPurchase.MaterialId);
                if (inactiveMaterial != null)
                {
                    MaterialList.Add(inactiveMaterial);
                    SelectedMaterial = inactiveMaterial;
                }
            }
            else
            {
                SelectedMaterial = materialInfo;
            }

            ICompanyData company = CompanyList.FirstOrDefault(m => m.Id == CashPurchase.CompanyId);

            if (company != null)
            {
                SelectedCompany = company;
                TicketNumber = company.NextTicket.ToString();
                CashTicket.OldTicketNumber = company.NextTicket;
            }

            //TODO CHECK CASH RATE
            CashRate = CashPurchase.Quote != null ? CashPurchase.Quote.Value : 0;
            IsExempt = CashPurchase.TaxExempt;

            TaxRateLabel = $"Tax ({(CashTicket.TaxRate * 100).ToString("N2")}%)";

            // Set payment default type
            Cash = true;

            ReCalculate();
            //FuelSurcharge = CashTicket.FuelSurcharge.HasValue ? CashTicket.FuelSurcharge.Value.ToString("N2") : null;
            //FuelSurcharge = _fuelSurcharge;
            
        }

        public string TaxRateLabel
        {
            get { return _taxRateLabel; }
            set
            {
                if (value == _taxRateLabel) return;
                _taxRateLabel = value;
                NotifyOfPropertyChange(() => TaxRateLabel);
            }
        }

        private static decimal TruncateDecimals(decimal value)
        {
            //return decimal.Truncate(value*100)/100;
            return decimal.Round(value, 2);
        }

        private decimal? ParseNullDecimalValue(string income)
        {
            if (string.IsNullOrWhiteSpace(income))
                return null;

            income = income.Replace('_', '0');

            decimal result;

            if (decimal.TryParse(income.Trim(), out result))
                return result;

            return null;
        }
        
        private int? ParseIntValue(string income)
        {
            if (string.IsNullOrWhiteSpace(income))
                return null;

            income = income.Replace(",", string.Empty);
            income = income.Replace("'", string.Empty);
            income = income.Replace(".", string.Empty);

            int result;

            if (int.TryParse(income.Trim(), out result))
                return result;

            return null;
        }

        #endregion

        #region Company

        public IReadOnlyList<ICompanyData> CompanyList
        {
            get { return _companyList; }
            set
            {
                if (Equals(value, _companyList)) return;
                _companyList = value;
                NotifyOfPropertyChange(() => CompanyList);
            }
        }

        public ICompanyData SelectedCompany
        {
            get { return _selectedCompany; }
            set
            {
                if (Equals(value, _selectedCompany)) return;
                _selectedCompany = value;
                NotifyOfPropertyChange(() => SelectedCompany);
                TicketNumber = SelectedCompany.NextTicket.ToString();
            }
        }

        private void InitializeCompany()
        {
            var commonService = IoC.Get<ICommonService>();
            CompanyList = commonService.GetCompaniesData();
        }

        #endregion

        #region Material

        public List<IMaterialInfo> MaterialList
        {
            get { return _materialList; }
            set
            {
                if (Equals(value, _materialList)) return;
                _materialList = value;
                NotifyOfPropertyChange(() => MaterialList);

                
            }
        }

        public IMaterialInfo SelectedMaterial
        {
            get { return _selectedMaterial; }
            set
            {
                if (Equals(value, _selectedMaterial)) return;
                _selectedMaterial = value;
                NotifyOfPropertyChange(() => SelectedMaterial);

                if (_selectedMaterial != null && _selectedMaterial.PricePerTon.HasValue)
                    CashRate = _selectedMaterial.PricePerTon.Value;
                else if (CashTicket.CashData.Quote.HasValue) 
                    CashRate = CashTicket.CashData.Quote.Value;
            }
        }

        private void InitializeMaterial()
        {
            MaterialList = IoC.Get<IMaterialService>().GetMaterials(_seasonId);
        }

        #endregion

        #region Scale

        public IScaleInfo SelectedScale { get; set; }

        public ScaleWidgetViewModel ScaleWidget
        {
            get { return _scaleWidget; }
            set
            {
                if (Equals(value, _scaleWidget)) return;
                _scaleWidget = value;
                NotifyOfPropertyChange(() => ScaleWidget);
            }
        }

        private void InitializeScale()
        {
            CashTicket.IsSecondWeightEnabled = false;

            // 2018-Nov-15 - Reinitialize Scale for second reading
            ScaleWidget = IoC.Get<ScaleWidgetViewModel>();
            ScaleWidget.ActivateWith(this);
            // 2018-Nov-15

            // Check scale "manual" mode
            if (IoC.Get<IConfiguration>().IsManualMode)
            {
                ManualScaling();
            }
            else
            {
                StartScaleInteraction();
            }
        }

        private void StartScaleInteraction()
        {
            try
            {
                // 07-Jan-2019, JGS - Reload the widget if it is null
                if (ScaleWidget == null)
                {
                    ScaleWidget = IoC.Get<ScaleWidgetViewModel>();
                    ScaleWidget.ConductWith(this);
                }

                // 15-Nov-2018, JGS - Original: CashPurchaseTicketViewModel code restored
                if (ScaleWidget.SelectedScale != null && ScaleWidget.SelectedScale.IsScaleEnabled)
                {
                    if (SelectedScale == null)
                        SelectedScale = ScaleWidget.SelectedScale;

                    if (_scaleManager != null)
                        _scaleManager.StartScaling(SelectedScale);
                }
                else
                {
                    ManualScaling();
                }
            }
            catch (Exception ex)
            {
                ManualScaling();
                Errors = $"Scale Integration failed with exception: {ex.Message}";
                ShowMessage("Scale Error", Errors, NotificationType.Warning);
                _log.Error(ex);
            }
        }

        #endregion

        #region Events

        public CashPurchaseTicketViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            _log = LogManager.GetLog(typeof(CashPurchaseTicketViewModel));
            _seasonId = (int)Application.Current.Properties["SeasonId"];
            _defaultSite = IoC.Get<IConfiguration>().SelectedSite.Name;
            _scaleManager = IoC.Get<IScaleManager>();
            DisplayName = "Cash Customer Ticket Preview";
        }

        protected override void OnInitialize()
        {
            // 2018-Nov-15, JGS - Original CashOrderTicketViewModel position restored
            base.OnInitialize();
            try
            {
                ScaleWidget = IoC.Get<ScaleWidgetViewModel>();
                ScaleWidget.ConductWith(this);
                IsBusy = true;
                var task = new Task(InitTask);
                task.Start();
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(
                            string.Format(
                                "Initialization failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                                ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));

                    TryClose(false);
                };

                action.OnUIThreadAsync();
            }
            //finally
            //{
            //    // Test the scale
            //    if (ScaleWidget == null)
            //    {
            //        // Try again
            //        base.OnInitialize();
            //        try
            //        {
            //            ScaleWidget = IoC.Get<ScaleWidgetViewModel>();
            //            ScaleWidget.ConductWith(this);
            //            IsBusy = true;
            //            var task = new Task(InitTask);
            //            task.Start();
            //        }
            //        catch (Exception ex)
            //        {
            //            _log.Error(ex);
            //            Action action = () =>
            //            {
            //                MessageBoxResult messageBoxResult =
            //                    (MessageBox.Show(
            //                        string.Format(
            //                            "Initialization failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
            //                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));

            //                TryClose(false);
            //            };

            //            action.OnUIThreadAsync();
            //        }
            //    }
            //}
        }

        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                if (value.Equals(_isBusy)) return;
                _isBusy = value;
                NotifyOfPropertyChange(() => IsBusy);
            }
        }

        private void InitTask()
        {
            try
            {
                EnableCheckNumber = false;
                InitializeCompany();
                InitializeMaterial();
                InitializeTicket();
                CashTicket.IsOverrideEnabled = false;
                CashTicket.IsTicketNumberEnabled = false;
                CashTicket.IsManual = false;

                InitializeScale();

            }
            catch (Exception ex)
            {
                var exInnerMessageBoxText = string.Empty;
                _log.Error(ex);
                if (ex.InnerException != null)
                {
                    _log.Error(ex.InnerException);
                    exInnerMessageBoxText = $" and inner exception '{ex.InnerException.Message}'{Environment.NewLine} ";
                }

                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                                           (MessageBox.Show($"New Ticket Number Initialization (GetNewCashTicket) failed with exception '{ex.Message}'{exInnerMessageBoxText}", "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };

                action.OnUIThreadAsync();
            }
            finally
            {
                IsBusy = false;
            }
        }

        public void Handle(ScaleChangedArgs args)
        {
            // 2018-Nov-15, JGS - Original CasePurchaseTicketViewModel restored
            if (args != null)
            {
                if (_scaleManager != null && CashTicket != null && !CashTicket.IsManual)
                {
                    SelectedScale = args.Scale;

                    _scaleManager.StopScaling();
                    //_scaleManager.Dispose();
                    _scaleManager.StartScaling(SelectedScale);
                }
            }
        }

        protected override void OnActivate()
        {
            _eventAggregator.Subscribe(this);
            base.OnActivate();
        }

        protected override void OnDeactivate(bool close)
        {
            if (_scaleManager != null)
                _scaleManager.StopScaling();

            // 2018-Jun-13, JGS - From OrderTicketViewModel 
            // Housekeeping - Dispose of the crystal reports object
            if (_crystalReportDocument != null)
            {
                _crystalReportDocument.Close();
                _crystalReportDocument.Dispose();
            }
            // 2018-Jun-13, JGS - From OrderTicketViewModel 

            _eventAggregator.Unsubscribe(this);
            base.OnDeactivate(close);
        }

        #endregion

        #region Actions

        public void Cancel()
        {
            _scaleManager.StopScaling();
            TryClose(false);
        }

        public void Override()
        {
            ManualScaling();
            CashTicket.IsTicketNumberEnabled = true;

            if (_scaleManager != null)
                _scaleManager.StopScaling();
        }

        public void ManualScaling()
        {
            CashTicket.IsFirstWeightEnabled = true;
            CashTicket.IsSecondWeightEnabled = true;
            CashTicket.IsManual = true;

            if (ScaleWidget != null)
                ScaleWidget.Errors = null;

            if (_scaleManager != null)
                _scaleManager.StopScaling();
        }

        public void PrintTicketX()
        {
            try
            {
                IsBusy = true;
                //var doDoPrintTicket = new Task(DoPrintTicket);
                //doDoPrintTicket.Start();
            }
            finally
            {
                IsBusy = true;
            }
        }

        public void PrintTicket()
        {
            IsPrintingEnabled = false;

            var stopWatch = System.Diagnostics.Stopwatch.StartNew();

            try
            {
                Debug.WriteLine("Cash Print Button Click - Begin");
                _log.Info($"Cash Print Button Click - Begin");

                // Stop the scale reading
                if (_scaleManager != null)
                {
                    _scaleManager.StopScaling();
                }

                if (!Validate())
                {
                    if (!IoC.Get<IConfiguration>().IsManualMode && !CashTicket.IsManual)
                    {
                        StartScaleInteraction();
                    }

                    return;
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "Ticket saving failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };

                action.OnUIThread();
                return;
            }



            try
            {
                var selectedSite = IoC.Get<IConfiguration>().SelectedSite;
                if (selectedSite.IsDefault)
                {
                    lock (this.CashTicket)
                    {
                        SaveTicket();
                        GeneratePrint();
                        Print();
                    }

                    _eventAggregator.PublishOnCurrentThread(new OrderModifiedArgs(CashTicket.OrderId));
                    _eventAggregator.PublishOnCurrentThread(new CashPurchaseModifiedArgs(CashPurchase.CashPurchaseId));
                    _eventAggregator.PublishOnCurrentThread(new TicketAddedVoidedArgs(CashTicket.TicketId));
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "Ticket printing failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };

                action.OnUIThread();
            }
            finally
            {
                _log.Info($"Print Button Click - End");
                stopWatch.Stop();
                _log.Info($"Print Button Click Time to Process - {stopWatch.ElapsedMilliseconds}ms");

                // Make sure to kill the print thread
                Action action = () =>
                {
                };
                action.OnUIThread();

                // 19-Nove-2018, JGS - Close this form but not the mail Cash Customer Form. There may be additional trucks setup to go. The user needs to close the CashCustomerView explicitly
                TryClose(true);
            }

        }


        public void CanPreviewTicket()
        {
            IsPrintingEnabled = FirstWeight > 0 && SecondWeight > FirstWeight
                                                && (Cash || Check || CreditCard)
                                                && CashTicket.TicketNumber > 0
                                                && SelectedMaterial != null;

            if (Check && string.IsNullOrWhiteSpace(CheckNumber))
            {
                IsPrintingEnabled = false;
            }
        }

        public void PreviewTicket()
        {
            try
            {
                if (_scaleManager != null)
                    _scaleManager.StopScaling();

                if (!Validate())
                {
                    if (!IoC.Get<IConfiguration>().IsManualMode && !CashTicket.IsManual)
                        StartScaleInteraction();

                    return;
                }

                // Generate ticket and preview
                GeneratePrint();
                Preview();
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "Ticket saving/printing failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };

                action.OnUIThreadAsync();
            }
        }

        private void GeneratePrint()
        {

            var stopWatch = System.Diagnostics.Stopwatch.StartNew();

            _log.Info($"Generate Cash Print - Begin");

            // Truck Description / Customer Organziation
            var customerOrganization = CashTicket.TruckDescription.Substring(0, Math.Min(45, CashTicket.TruckDescription.Length));

  
            // Ticket Number
            ISeason season = IoC.Get<ICommonService>().GetSeasonById(_seasonId);
            string seasonprefix = string.Empty;
            if (season != null)
            {
                seasonprefix = season.Prefix.Split(new[] { '-' }, StringSplitOptions.RemoveEmptyEntries)[0];
            }
            //string ticketNum = string.Format("{0}-{1}", seasonprefix, Ticket.TicketNumber.ToString("000000"));
            string ticketNum = string.Format("{0}", CashTicket.TicketNumber.ToString("000000"));

            //Company
            var companyId = CashTicket.CompanyId == null ? -1 : SelectedCompany.Id;
            var company = IoC.Get<ICommonService>().GetCompanyData(companyId);
            var companyName = string.Empty;
            var companyAddress = string.Empty;
            var companyPhoneNumber1 = string.Empty;
            var companyPhoneNumber2 = string.Empty;
            if (company != null)
            {
                companyName = company.Name;
                companyAddress = String.Format("{0}{1} {2}, {3} {4}",
                    company.AddressLineOne,
                    (company.AddressLineTwo != string.Empty || company.AddressLineTwo != null) ? " " + company.AddressLineTwo + "," : string.Empty,
                    company.City,
                    company.State,
                    company.Zip);
                companyPhoneNumber1 = (company.PhoneNumberOne != string.Empty) ? " Phone: " + company.PhoneNumberOne : string.Empty;
            }

         
            // Cash Rate
            string customerRate = (Math.Floor(CashTicket.MaterialCharge*100m)).ToString("0000");

            // Gross, Tare, Net -- Value is in lbs and should be rounded to a whole number
            //-- Done so the weight read at the screen when printing is the same that is saved in the database
            string tare = CashTicket.CashData.Tare.Value.ToString("N0");
            string gross = CashTicket.TruckWeight.ToString("N0");
            string net = CashTicket.Quantity.ToString("N0");
            string tons = CashTicket.NetWeightTon.ToString("N2");

            // Material Cost, Sales Tax, Total
            var materialCost = string.Empty;
            var salesTax = string.Empty;
            var total = string.Empty;
            string paymentType = string.Empty;

            var isHideDollarAmountToTicketTruck = false;
            var isHideDollarAmountToTicketOffice = false;
            var isHideDollarAmountToTicketCustomer = false;
            var isHideDollarAmountToTicketDispatch = false;


            materialCost = CashTicket.MaterialCharge.ToString("C");
            salesTax = CashTicket.Tax.ToString("C");
            total = CashTicket.Due.ToString("C");
            if (Cash)
                paymentType = "Cash";
            if (Check)
                paymentType = $"Check {CheckNumber}";
            if (CreditCard)
                paymentType = "Credit Card";


            if (CashTicket.HideDollarAmount)
            {
                isHideDollarAmountToTicketTruck = true;
                isHideDollarAmountToTicketOffice = true;
                isHideDollarAmountToTicketCustomer = true;
            }

            var manuallyText = CashTicket.IsManual ? "Manually Key Weight " : string.Empty;

            var weighMasterNameNumber = IoC.Get<IUserService>().GetByUsername(Application.Current.Properties["UserName"].ToString());

            string _applicationPathReports = AppDomain.CurrentDomain.BaseDirectory + "Reports" + Path.DirectorySeparatorChar;

            _log.Info($"Generate Cash Print - End");
            stopWatch.Stop();
            _log.Info($"Generate Print Time to Process - {stopWatch.ElapsedMilliseconds}ms");

            stopWatch.Restart();


            #region Crystal Ticket
            _log.Info($"Generate Print Cash Customer - Begin");

            _crystalReportDocument = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
            _crystalReportDocument.Load(_applicationPathReports + "Ticket.rpt", CrystalDecisions.Shared.OpenReportMethod.OpenReportByTempCopy);


            CrystalDecisions.CrystalReports.Engine.Database crDatabase;
            CrystalDecisions.CrystalReports.Engine.Tables crTables;

            CrystalDecisions.Shared.ConnectionInfo crConnectioninfo = new CrystalDecisions.Shared.ConnectionInfo();

            _crystalReportDocument.DataSourceConnections.ToString();

            crDatabase = _crystalReportDocument.Database;
            crTables = crDatabase.Tables;

            _crystalReportDocument.SetParameterValue("CompanyName", companyName);
            _crystalReportDocument.SetParameterValue("CompanyPhoneAddress", companyAddress + companyPhoneNumber1);
            _crystalReportDocument.SetParameterValue("CustomerOrganization", customerOrganization);
            _crystalReportDocument.SetParameterValue("Address", string.Empty);
            _crystalReportDocument.SetParameterValue("SpecialInstructions", CashTicket.SpecialInstruction ?? string.Empty);
            _crystalReportDocument.SetParameterValue("TicketNumber", ticketNum);
            _crystalReportDocument.SetParameterValue("Date", CashTicket.CreateTime.ToString("MM/dd/yy HH:mm"));
            _crystalReportDocument.SetParameterValue("ConfirmationNumber", string.Empty);
            _crystalReportDocument.SetParameterValue("PONumber", string.Empty);
            _crystalReportDocument.SetParameterValue("CustomerIdWithRate", paymentType + ((paymentType == "Check") ? '#' + CheckNumber : string.Empty));
            _crystalReportDocument.SetParameterValue("ManuallyText", manuallyText);
            _crystalReportDocument.SetParameterValue("WeighMasterNameNumber", string.Format($"{weighMasterNameNumber.WeighMasterNumber}   {weighMasterNameNumber.WeighMasterName}"));
            _crystalReportDocument.SetParameterValue("MaterialDescription", SelectedMaterial.Description ?? string.Empty);


            _crystalReportDocument.SetParameterValue("Gross", gross);
            _crystalReportDocument.SetParameterValue("Tare", tare);
            _crystalReportDocument.SetParameterValue("Net", net);
            _crystalReportDocument.SetParameterValue("Tons", tons);

            _crystalReportDocument.SetParameterValue("MaterialCost", materialCost);
            _crystalReportDocument.SetParameterValue("SalesTax", salesTax);
            _crystalReportDocument.SetParameterValue("Total", total);
            //_crystalReportDocument.SetParameterValue("Tended", CashTicket.Tended);  // String.Format("{0.00}", CashTicket.Tended));
            _crystalReportDocument.SetParameterValue("PaymentType", paymentType);
            _crystalReportDocument.SetParameterValue("Change", CashTicket.Change.ToString());

            _crystalReportDocument.SetParameterValue("IsHideDollarAmountToTicketTruck", isHideDollarAmountToTicketTruck);
            _crystalReportDocument.SetParameterValue("IsHideDollarAmountToTicketOffice", isHideDollarAmountToTicketOffice);
            _crystalReportDocument.SetParameterValue("IsHideDollarAmountToTicketCustomer", isHideDollarAmountToTicketCustomer);
            _crystalReportDocument.SetParameterValue("IsHideDollarAmountToTicketDispatch", isHideDollarAmountToTicketDispatch);

            _crystalReportDocument.SetParameterValue("TruckName", string.Empty);
            _crystalReportDocument.SetParameterValue("TruckPlate", string.Empty);
            _crystalReportDocument.SetParameterValue("TruckTag", string.Empty);

            _crystalReportDocument.SetParameterValue("SignatureImageFilePath", _applicationPathReports + "Signature" + Path.DirectorySeparatorChar + weighMasterNameNumber.UserName + ".bmp");

            _log.Info($"Generate Print Cash Customer - End");

            stopWatch.Stop();
            _log.Info($"Generate Print Ticket in Crystal Time to Process - {stopWatch.ElapsedMilliseconds}ms");

            #endregion

        }

        private void Print()
        {
            var stopWatch = System.Diagnostics.Stopwatch.StartNew();

            //-- Crystal
            _log.Info($"Printing of Ticket Cash Customer - Begin");
            //_crystalReportDocument.PrintOptions.PrinterName = "TICKET";
            //_crystalReportDocument.PrintToPrinter(1, false, 1, 1);

            PrintReportOptions crystalPrintReportOptions = new PrintReportOptions();
            crystalPrintReportOptions.PrinterName = IoC.Get<IConfiguration>().DefaultPrinterNameLocalComputer;
            crystalPrintReportOptions.RemoveAllPrinterPageRanges();
            crystalPrintReportOptions.AddPrinterPageRange(1, 1);
            _crystalReportDocument.ReportClientDocument.PrintOutputController.PrintReport(crystalPrintReportOptions);

            _log.Info($"Printing of Ticket Cash Customer - End");

            stopWatch.Stop();
            _log.Info($"Printing of Ticket Cash Customer Time to Process - {stopWatch.ElapsedMilliseconds}ms");

            // Kill the scale thread on this window
            Action action = () =>
            {
            };
            action.OnUIThread();
        }

        private void Preview()
        {
            //var windowManager = IoC.Get<IWindowManager>();
            //_printModel.DefaultPrinter = SelectedCompany.PrinterTicketName ?? string.Empty;
            //_printModel.IsPrintActive = false;
            //windowManager.ShowDialog(_printModel);
            ////_printModel = null;  // 20190225, JGS - Make certain objects opened as windows are set to null
        }

        private void SaveTicket()
        {
            var debugTextSavingTicketBegin = "Saving Ticket - Begin";
            Debug.WriteLine(debugTextSavingTicketBegin);
            _log.Info(debugTextSavingTicketBegin);


            CashTicket.UpdateTime = DateTime.Now;
            CashTicket.Quantity = NetWeight;

            CashTicket.CashData.Tare = FirstWeight;
            CashTicket.TruckWeight = SecondWeight;
            CashTicket.CashData.TaxExempt = IsExempt;
            if (Check)
            {
                CashTicket.CashData.PaymentForm = "Check";
                CashTicket.CashData.CheckNumber = CashTicket.CheckNumber;
            }
            else if (CreditCard)
            {
                CashTicket.CashData.PaymentForm = "Credit Card";
            }
            else if (Cash)
            {
                CashTicket.CashData.PaymentForm = "Cash";
            }

            CashTicket.CashData.Quote = CashRate;
            CashTicket.TicketId = IoC.Get<ITicketService>().SaveCashTicket(CashTicket, CashPurchase);

            CashTicket.DataA = CashTicket.Due.ToString() + "," + CashTicket.Tended.ToString() + "," + CashTicket.Change.ToString();

            var debugTextSavingTicketEnd = "Saving Cash Ticket - End";
            Debug.WriteLine(debugTextSavingTicketEnd);
            Debug.WriteLine($"Saved Cash ticket '{CashTicket.TicketNumber}'");

            _log.Info(debugTextSavingTicketEnd);
            _log.Info($"Saved Cash ticket '{CashTicket.TicketNumber}'");
        }

        private bool Validate()
        {
            Errors = null;
            var str = new List<string>();
            if (FirstWeight <= 0)       // Originally <= 0
                str.Add("First Weight must be more than 0.");

            if (SecondWeight <= 0)      // Originally <= 0
                str.Add("Second Weight must be more than 0.");
            else if (SecondWeight <= FirstWeight && FirstWeight > 0)
                str.Add("Second Weight must be more than First Weight.");

            if (SecondWeight > 1500000)
                str.Add("Second Weight must be less than 1500000 lbs.");

            if (NetWeight <= 0)      // Originally <= 0
                str.Add("Net Weight must be more than 0.");

            if (!CreditCard && !Cash && !Check)
                str.Add("You must select a payment type.");

            if (Check && string.IsNullOrWhiteSpace(CashTicket.CheckNumber))
                str.Add("You must enter the check number.");

            var tended = CashTicket.GetTendedValue(CashTicket.Tended);
            //if (Cash && CashTicket.Due > tended)
            //    str.Add("Tended must be equal or greater than Amount Due.");

            if (CashTicket.TicketNumber <= 0)
            {
                var messageValidateTicketNumberCurrentAtOrLessThanZero = $"The new ticket number should not be lower than the current ticket number of '{CashTicket.TicketNumber}'";
                str.Add(messageValidateTicketNumberCurrentAtOrLessThanZero);
                _log.Info(messageValidateTicketNumberCurrentAtOrLessThanZero);
            }


            int nextTicketNumber = IoC.Get<ITicketService>().GetNextTicketNumber(CashTicket.CompanyId.Value);

            bool isTicketExist = IoC.Get<ITicketService>().IsTicketExist(CashTicket.TicketNumber, CashTicket.TicketId, _seasonId);

            

            if (isTicketExist)
            {
                string messageValidateTicketNumberExistsCurrentNumberNextNumber = $"The ticket number '{CashTicket.TicketNumber}' already exists.  Changing number to next in sequence: '{nextTicketNumber}'";
                str.Add(messageValidateTicketNumberExistsCurrentNumberNextNumber);

                TicketNumber = nextTicketNumber.ToString();
                CashTicket.OldTicketNumber = nextTicketNumber;

                _log.Info(messageValidateTicketNumberExistsCurrentNumberNextNumber);
            }

            if (CashTicket.OldTicketNumber != CashTicket.TicketNumber)
            {
                if (CashTicket.OldTicketNumber > CashTicket.TicketNumber && nextTicketNumber <= 0)
                {
                    string messageValidateTicketNumberLower = "The new ticket number should not be lower than the current ticket number.";
                    str.Add(messageValidateTicketNumberLower);
                    _log.Info(messageValidateTicketNumberLower);
                }

                if (nextTicketNumber > 0 && nextTicketNumber > CashTicket.TicketNumber)
                {
                    string messageValidateTicketNumberHigher = "The new ticket number should not be higher than the current ticket number.";
                    str.Add(messageValidateTicketNumberHigher);
                    _log.Info(messageValidateTicketNumberHigher);
                }
            }

            if (SelectedCompany == null
                || SelectedCompany.FirstTicket > CashTicket.TicketNumber
                || SelectedCompany.LastTicket < CashTicket.TicketNumber
                )
            {
                string messageValidateTicketNumberOutsideOfDesignatedRange = $"The Ticket number is outside the company range of ticket numbers ({SelectedCompany.FirstTicket} - {SelectedCompany.LastTicket})";
                str.Add(messageValidateTicketNumberOutsideOfDesignatedRange);
                _log.Info(messageValidateTicketNumberOutsideOfDesignatedRange);
            }

            if (CashRate <= 0)
            {
                string messageValidatePricePerTonGreaterThanZero = "Price per ton must be greater than 0.";
                str.Add(messageValidatePricePerTonGreaterThanZero);
                _log.Info(messageValidatePricePerTonGreaterThanZero);
            }

            if (SelectedMaterial == null)
            {
                string messageValidateSelectMaterial = "Material must be selected.";
                str.Add(messageValidateSelectMaterial);
                _log.Info(messageValidateSelectMaterial);
            }

            if (str.Any())
            {
                Errors = string.Format(
                    "Validation Failed.{0}{1}", Environment.NewLine,
                    string.Join("\n", str));

                ShowMessage("Validation Errors", Errors, NotificationType.Warning);
                return false;
            }

            return true;
        }

        #endregion

        public void Handle(ScaleResultsArgs args)
        {
            switch (args.Result.StreamPolarity)
            {
                case ScaleIndicatorStreamPolarity.Negative:
                    //UI label to have text "Negative"
                    ScaleWidget.Errors = args.Result.StreamPolarity.GetDescription();
                    break;

                case ScaleIndicatorStreamPolarity.Overload:
                    //UI label to have text "Overload"
                    ScaleWidget.Errors = args.Result.StreamPolarity.GetDescription();
                    break;

                case ScaleIndicatorStreamPolarity.Underrange:
                    //UI label to have text "Underrange"
                    ScaleWidget.Errors = args.Result.StreamPolarity.GetDescription();
                    break;
                case ScaleIndicatorStreamPolarity.Positive:
                    SecondWeightText =
                        (args.Result.StreamWeight.HasValue
                            ? args.Result.StreamWeight.Value.GetWeightInPounds(args.Result.StreamUnitOfWeight)
                            : 0).ToString("N0");
                    ScaleWidget.Errors =null;
                    break;
                case ScaleIndicatorStreamPolarity.Undefined:
                default:
                    ScaleWidget.Errors = ScaleIndicatorStreamPolarity.Undefined.GetDescription();
                    break;
            }
        }

        private void ScaleWidgetActivated(object sender, ActivationEventArgs args)
        {
            if (args.WasInitialized)
            {
                if (ScaleWidget.SelectedScale != null
                    && ScaleWidget.SelectedScale.IsScaleEnabled)
                {
                    SelectedScale = ScaleWidget.SelectedScale;
                    return;
                }
            }
            if (ScaleWidget != null)
            {
                ScaleWidget.Errors = null;
            }
        }

        public void Handle(ScaleExceptionsArgs message)
        {
            //ManualScaling();
            //ShowMessage("Scale Errors", string.Format($"Scale unavailable this time with exception: '{message.Message}'. Put weight manually or contact HelpDesk."), NotificationType.Warning);
            _log.Warn($"Scale Handler: {message.Message}  Scale Id: {message.ScaleId}");

        }
    }
}