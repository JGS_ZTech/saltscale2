﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Caliburn.Micro;
using Contract.Domain;
using Contract.Exception;
using Contract.Services;
using ZTech.Controls;
using ZTech.ViewModels.Abstract;
using Action = System.Action;
using Contract.Events;

namespace ZTech.ViewModels
{
    public class CashVoidViewModel : BaseViewModel
    {
        private readonly IEventAggregator _eventAggregator;
        private readonly ILog _log;
        private readonly int _seasonId;
        private ICashTicketInfo _selectedTicket;
        private IReadOnlyList<ICashTicketInfo> _ticketList;
        private string _errors;

        public string Errors
        {
            get { return _errors; }
            set
            {
                if (value == _errors) return;
                _errors = value;
                NotifyOfPropertyChange(() => Errors);
            }
        }

        #region Void Reason

        public ICashTicketInfo SelectedTicket
        {
            get { return _selectedTicket; }
            set
            {
                if (Equals(value, _selectedTicket)) return;
                _selectedTicket = value;
                NotifyOfPropertyChange(() => SelectedTicket);
            }
        }

        public IReadOnlyList<ICashTicketInfo> TicketList
        {
            get { return _ticketList; }
            set
            {
                if (Equals(value, _ticketList)) return;
                _ticketList = value;
                NotifyOfPropertyChange(() => TicketList);
            }
        }

        private void InitializeTicketList()
        {
            TicketList = IoC.Get<ITicketService>().GetCashTicketsInfo(_seasonId);
            if (TicketList != null && TicketList.Any())
            {
                SelectedTicket = TicketList.FirstOrDefault();
            }
        }

        #endregion

        #region Actions

        public void KeyHandle(Key key)
        {
            if (key == Key.F2)
            {
                Void();
            }
        }

        public void Done()
        {
            Void();
        }


        public void Cancel()
        {
            TryClose(false);
        }

        public void Void()
        {
            try
            {
                if (!Validate())
                {
                    return;
                }

                IoC.Get<ITicketService>().VoidTicket(SelectedTicket);
                _eventAggregator.PublishOnCurrentThread(new TicketAddedVoidedArgs(null));

                TryClose(true);
            }
            catch (UniqueConstraintException ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };

                action.OnUIThreadAsync();
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                System.Action action = () =>
                {
                    var messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "Customer saving failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };

                action.OnUIThreadAsync();
            }

        }

        private bool Validate()
        {
            Errors = null;

            ICollection<ValidationResult> validationResults = new Collection<ValidationResult>();
            // Validate Ticket is selected
            if (SelectedTicket == null)
            {
                validationResults.Add(new ValidationResult("There is not Ticket selected."));
            }

            // Validate Void Reason
            if (SelectedTicket != null && string.IsNullOrEmpty(SelectedTicket.VoidReason))
            {
                validationResults.Add(new ValidationResult("The Void Reason cannot be left blank."));
            }

            if (validationResults.Any())
            {
                Errors = string.Format("Validation Failed.{0}{1}", Environment.NewLine,
                    string.Join("\n", validationResults.Select(v => v.ErrorMessage)));

                ShowMessage("Validation Errors", Errors, NotificationType.Warning);

                return false;
            }

            return true;
        }

        #endregion

        #region Events

        public CashVoidViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            _log = LogManager.GetLog(typeof(CashVoidViewModel));
            _seasonId = (int)Application.Current.Properties["SeasonId"];
            DisplayName = "Cash Void";
        }

        protected override void OnInitialize()
        {
            try
            {
                base.OnInitialize();

                InitializeTicketList();
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(
                            string.Format(
                                "Initialization failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                                ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));

                    TryClose(false);
                };

                action.OnUIThreadAsync();
            }
        }

        protected override void OnActivate()
        {
            _eventAggregator.Subscribe(this);
            base.OnActivate();
        }

        protected override void OnDeactivate(bool close)
        {
            _eventAggregator.Unsubscribe(this);
            base.OnDeactivate(close);
        }

        #endregion
    }
}