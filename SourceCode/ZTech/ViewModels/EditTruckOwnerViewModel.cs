﻿using System;
using System.Windows;
using Caliburn.Micro;
using Contract.Events;
using Contract.Services;
using System.Windows.Input;
using ZTech.Controls;
using ZTech.ViewModels.Abstract;
using ZTech.ViewModels.ViewObject;

namespace ZTech.ViewModels
{
    public class EditTruckOwnerViewModel : BaseTruckOwnerViewModel
    {
        public EditTruckOwnerViewModel(IEventAggregator eventAggregator)
            : base(eventAggregator, "Edit Truck Owner Screen")
        {
            _log = LogManager.GetLog(typeof (EditTruckOwnerViewModel));
        }

        public int OwnerId { get; set; }

        protected override void InitializeTruckOwner()
        {
            Owner = new TruckOwnerDataModel(IoC.Get<ITruckService>().GetTruckOwner(OwnerId, _seasonId));
        }

        public void Delete()
        {
            if (Owner == null || !Owner.Id.HasValue) return;
            System.Action action = () =>
            {
                MessageBoxResult messageBoxResult =
                    (MessageBox.Show("Are you sure you want to delete this Truck Owner?",
                        "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question));

                if (messageBoxResult == MessageBoxResult.Yes)
                    DeleteOwnerAction();
            };

            action.OnUIThread();
        }

        private void DeleteOwnerAction()
        {
            try
            {
                if (Owner == null || !Owner.Id.HasValue) return;

                IoC.Get<ITruckService>().DeleteTruckOwner(Owner.Id.Value);
                _eventAggregator.PublishOnCurrentThread(new TruckOwnerModifiedArgs((int?)null));

                TryClose(true);
                }
            catch (Exception ex)
            {
                _log.Error(ex);
                string message = string.Format(
                    "Truck Owner deleting failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                    ex.Message);
                ShowMessage("Error", message, NotificationType.Error);
            }
        }
    }
}