﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Input;
using Caliburn.Micro;
using ZTech.Controls;
using ZTech.ViewModels.Abstract;

namespace ZTech.ViewModels
{
    public class ReportCongfigurationNameViewModel : BaseViewModel
    {
        private readonly IEventAggregator _eventAggregator;
        private string _name;
        private string _errors;
        
        public string Name
        {
            get { return _name; }
            set
            {
                if (Equals(value, _name)) return;
                _name = value;
                NotifyOfPropertyChange(() => Name);
            }
        }
        public string Errors
        {
            get { return _errors; }
            set
            {
                if (value == _errors) return;
                _errors = value;
                NotifyOfPropertyChange(() => Errors);
            }
        }

        #region Actions

        public void Cancel()
        {
            TryClose(false);
        }

        public void KeyHandle(Key key)
        {
            if (key == Key.F2)
            {
                Done();
            }
        }

        public void Done()
        {
            if (!IsValid())
                return;

            TryClose(true);
        }

        private bool IsValid()
        {
            ICollection<ValidationResult> validationResults = new Collection<ValidationResult>();

            //Validate Report Configuration Name
            if (string.IsNullOrEmpty(Name))
            {
                validationResults.Add(new ValidationResult("The Report Configuration Name cannot be left blank."));
            }

            if (validationResults.Any())
            {
                List<string> errors = validationResults.Select(v => v.ErrorMessage).ToList();
                var messageBoxText = string.Format(
                    "Validation Failed. {0} {1}", Environment.NewLine,
                    string.Join("; ", errors));
                ShowMessage("Validation Errors", messageBoxText, NotificationType.Warning);

                return false;
            }

            return true;
        }

        #endregion

        #region Events

        public ReportCongfigurationNameViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            DisplayName = "Report Configuration Name";
        }

        protected override void OnActivate()
        {
            _eventAggregator.Subscribe(this);
            base.OnActivate();
        }

        protected override void OnDeactivate(bool close)
        {
            _eventAggregator.Unsubscribe(this);
            base.OnDeactivate(close);
        }

        #endregion
    }
}