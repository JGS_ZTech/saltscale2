﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Caliburn.Micro;
using Contract.Events;
using Contract.Services;
using ZTech.Controls;
using ZTech.ViewModels.Abstract;
using Action = System.Action;

namespace ZTech.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        private readonly IEventAggregator _eventAggregator;
        private readonly ILog _log;
        private string _errors;

        public LoginViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            _log = LogManager.GetLog(typeof (LoginViewModel));
            DisplayName = "Login";
        }

        protected override void OnInitialize()
        {
            base.OnInitialize();
            IsAuthorized = false;

            /*
#if DEBUG
            UserName = "Kenny";
            Password = "Kenny";
            Done();
#endif
        */
        }
        
        public bool IsAuthorized { get; private set; }

        public string Errors
        {
            get { return _errors; }
            set
            {
                if (value == _errors) return;
                _errors = value;
                NotifyOfPropertyChange(() => Errors);
            }
        }
        
        private string Password { get; set; }

        public string UserName { get; set; }


        #region Actions

        public void PasswordChanged(Control control)
        {
            if (!(control is PasswordBox)) return;
            Password = (control as PasswordBox).Password;
        }

        public void Cancel()
        {
            IsAuthorized = false;
            TryClose(false);
        }

        public void KeyHandle(Key key)
        {
            if (key == Key.F2)
            {
                Done();
            }
        }

        public void Done()
        {
            try
            {
                if (!IsValid())
                {
                    return;
                }

                var user = IoC.Get<IUserService>().GetByUsername(UserName);
                if (user != null && user.Password == this.Password.ToLower())
                {
                    IsAuthorized = true;

                    Application.Current.Properties["IsLogged"] = true;
                    Application.Current.Properties["UserName"] = user.UserName;

                    UserAuthorization.UserAuthorizationLoggedIn = user;

                    _eventAggregator.PublishOnUIThreadAsync(new AuthArgs());
                }
                else
                {
                    IsAuthorized = false;

                    Errors = $"Authorization Failed{Environment.NewLine}User Name and/or Password are incorrect.";

                    _log.Info($"Login failed for user '{UserName}'");

                    ShowMessage("Login Failed", Errors, NotificationType.Warning);
                    return;
                }

                TryClose(true);
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Errors = $"Operation failed with exception '{ex.Message}'  Please infom IT";

                ShowMessage("Error", Errors, NotificationType.Error);
            }
        }

        private bool IsValid()
        {
            if (string.IsNullOrWhiteSpace(UserName))
            {
                Errors = $"Validation Failed{Environment.NewLine}User Name cannot be empty";

                ShowMessage("Validation Errors", Errors, NotificationType.Warning);
                return false;
            }

            if (string.IsNullOrWhiteSpace(Password))
            {
                Errors = $"Validation Failed{Environment.NewLine}Password cannot be empty";
                
                ShowMessage("Validation Errors", Errors, NotificationType.Warning);
                return false;
            }

            return true;
        }


        #endregion

    }
}