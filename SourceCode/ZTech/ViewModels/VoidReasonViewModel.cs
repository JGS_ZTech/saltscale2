﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Caliburn.Micro;
using Contract.Domain;
using Contract.Events;
using Contract.Exception;
using Contract.Services;
using ZTech.Controls;
using ZTech.ViewModels.Abstract;
using Action = System.Action;

namespace ZTech.ViewModels
{
    public class VoidReasonViewModel: BaseViewModel
    {
        private readonly IEventAggregator _eventAggregator;
        private ILog _log;
        private ITicketInfo _ticket;
        protected readonly int _seasonId;

        #region Void Reason

        public ITicketInfo Ticket
        {
            get { return _ticket; }
            set
            {
                if (Equals(value, _ticket)) return;
                _ticket = value;
                NotifyOfPropertyChange(() => Ticket);
            }
        }

        #endregion

        #region Actions

        public void Cancel()
        {
            TryClose(false);
        }

        public void KeyHandle(Key key)
        {
            if (key == Key.F2)
            {
                Done();
            }
        }

        public void Done()
        {
            try
            {
                if (!IsValid())
                    return;

                IoC.Get<ITicketService>().VoidTicket(Ticket);
                IoC.Get<ITicketService>().GetTonsShippedToday(_seasonId);
                TryClose(true);
            }
            catch (UniqueConstraintException ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };

                action.OnUIThreadAsync();
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                System.Action action = () =>
                {
                    var messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "Voiding of ticket failed with exception \"{0}\"",
                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };

                action.OnUIThreadAsync();
            }

        }

        private bool IsValid()
        {
            ICollection<ValidationResult> validationResults = new Collection<ValidationResult>();
            bool result = true;

            //Validate Bid Company 
            if (string.IsNullOrEmpty(Ticket.VoidReason))
            {
                validationResults.Add(new ValidationResult("The Void Reason cannot be left blank."));
            }

            if (!result || validationResults.Any())
            {
                List<string> errors = validationResults.Select(v => v.ErrorMessage).ToList();
                var messageBoxText = string.Format(
                    "Validation Failed. {0} {1}", Environment.NewLine,
                    string.Join("; ", errors));
                ShowMessage("Validation Errors", messageBoxText, NotificationType.Warning);

                return false;
            }

            return true;
        }

        #endregion

        #region Events

        public VoidReasonViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            _log = LogManager.GetLog(typeof(VoidReasonViewModel));
            _seasonId = (int)Application.Current.Properties["SeasonId"];
            DisplayName = "Void Reason";
        }

        protected override void OnActivate()
        {
            _eventAggregator.Subscribe(this);
            base.OnActivate();
        }

        protected override void OnDeactivate(bool close)
        {
            _eventAggregator.Unsubscribe(this);
            base.OnDeactivate(close);
        }

        #endregion

        #region VO

        #endregion
    }
}