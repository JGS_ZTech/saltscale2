﻿using CrystalDecisions.Shared;
using CrystalDecisions.ReportAppServer.Controllers;
using CrystalDecisions.ReportAppServer.DataDefModel;
using CrystalDecisions.ReportAppServer.ReportDefModel;
using System;
using System.IO;
using System.Windows;
using Caliburn.Micro;
using Contract.Domain;
using Contract.Services;
using CrystalDecisions.ReportAppServer.ClientDoc;
using CrystalDecisions.CrystalReports.Engine;
using ZTech.ViewModels.Abstract;
using ZTech.Reports;
using System.Windows.Input;

namespace ZTech.ViewModels
{
    public class ReportDisplayViewModel : BaseViewModel
    {
        private readonly IEventAggregator _eventAggregator;
        private CrystalDecisions.ReportAppServer.ClientDoc.ISCDReportClientDocument rptClientDoc;
        private readonly ILog _log;
        private ISiteConfig _selectedSite;
        private SAPBusinessObjects.WPF.Viewer.CrystalReportsViewer _rptViewer;

        public IReportFilter ReportFilterInUse { get; set; }

        public ReportDisplayViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            //-- Needed per SAP forum posting because of the WPF control of the viewer
            System.Diagnostics.PresentationTraceSources.DataBindingSource.Switch.Level = System.Diagnostics.SourceLevels.Critical;
            //InitializeComponent();
            _selectedSite = IoC.Get<IConfiguration>().SelectedSite;
            _log = LogManager.GetLog(typeof(ReportDisplayViewModel));
        }



        #region Events

        protected override void OnInitialize()
        {
            base.OnInitialize();
            try
            {
                ReportGenerate();
            }
            catch (Exception ex)
            {
                var exInnerMessageBoxText = string.Empty;
                _log.Error(ex);
                if (ex.InnerException != null)
                {
                    _log.Error(ex.InnerException);
                    exInnerMessageBoxText = $" and inner exception '{ex.InnerException.Message}'{Environment.NewLine} ";
                }

                System.Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(
                            string.Format(
                                "Initialization failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                                ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));

                    TryClose(false);
                };

            }
           
        }

        protected override void OnActivate()
        {
            _eventAggregator.Subscribe(this);
            base.OnActivate();
        }

        protected override void OnDeactivate(bool close)
        {
            _eventAggregator.Unsubscribe(this);
            base.OnDeactivate(close);
        }

        #endregion

        #region Actions

        public SAPBusinessObjects.WPF.Viewer.CrystalReportsViewer RptViewer
        {
            get { return _rptViewer; }
            set
            {
                if (value.Equals(_rptViewer)) return;
                _rptViewer = value;
                NotifyOfPropertyChange(() => RptViewer);
            }
        }

        public void PrintReport()
        {
            try
            {
                // Print
                Reports.ReportDisplayViewer reportDisplayViewer_DirectPrint = new Reports.ReportDisplayViewer(ReportFilterInUse);
                reportDisplayViewer_DirectPrint.ReportGenerate_ReportToPrint();
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                System.Action action = () =>
                {
                    var messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "Printing of report failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                            ex.Message), "Error", MessageBoxButton.OK));
                };

                action.OnUIThreadAsync();
            }
        }


        public void KeyHandle(Key key)
        {
            if (key == Key.F2)
            {
                Done();
            }
        }

        public void Done()
        {
            TryClose(false);
        }

        public void Close()
        {
            TryClose(false);
        }
        #endregion



        public void ReportGenerate()
        {
            string ServerName = _selectedSite.ConnectionStringDatasource;
            string DatabaseName = _selectedSite.ConnectionStringInitialCatalog;
            string DatabaseUser = _selectedSite.ConnectionStringUserId;
            string DatabasePassword = _selectedSite.ConnectionStringPassword;
            bool DatabaseIntegratedSecurity = _selectedSite.ConnectionStringIntegratedSecurity;

            string ReportName = ReportFilterInUse.ReportFileName;

            _log.Info($"Report Display Viewer-Pre CR code: Server '{ServerName}'  Database '{DatabaseName}'  Report '{ReportName}'");

            #region Generate Report - Change DataSource

            bool IsLoggedOn = false;

            CrystalDecisions.CrystalReports.Engine.ReportObjects crReportObjects;
            CrystalDecisions.CrystalReports.Engine.SubreportObject crSubreportObject;
            CrystalDecisions.CrystalReports.Engine.ReportDocument crSubreportDocument;
            CrystalDecisions.CrystalReports.Engine.Database crDatabase;
            CrystalDecisions.CrystalReports.Engine.Tables crTables;
            TableLogOnInfo crTableLogOnInfo;

            //-- used in one line
            CrystalDecisions.CrystalReports.Engine.ReportDocument rpt = new CrystalDecisions.CrystalReports.Engine.ReportDocument();

            rpt.Load(Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), "Reports", ReportName), OpenReportMethod.OpenReportByTempCopy);

            CrystalDecisions.Shared.ConnectionInfo crConnectioninfo = new CrystalDecisions.Shared.ConnectionInfo();

            rpt.DataSourceConnections.ToString();

            //set up the database and tables objects to refer to the current report
            crDatabase = rpt.Database;
            crTables = crDatabase.Tables;

            crConnectioninfo.ServerName = ServerName;
            crConnectioninfo.DatabaseName = DatabaseName;
            crConnectioninfo.UserID = DatabaseUser;
            crConnectioninfo.Password = DatabasePassword;
            crConnectioninfo.IntegratedSecurity = DatabaseIntegratedSecurity;

            int tableIndex = 0;
            bool mainSecureDB;


            bool ConWorks = false;

            // get the DB name from the report
            CrystalDecisions.Shared.NameValuePair2 nvp2 = (NameValuePair2)rpt.Database.Tables[0].LogOnInfo.ConnectionInfo.Attributes.Collection[1];

            //loop through all the tables and pass in the connection info
            foreach (CrystalDecisions.CrystalReports.Engine.Table crTable in crTables)
            {
                mainSecureDB = rpt.Database.Tables[tableIndex].LogOnInfo.ConnectionInfo.IntegratedSecurity;
                string mainTableName = crTable.Name.ToString();
                tableIndex++;

                //pass the necessary parameters to the connectionInfo object
                crConnectioninfo.ServerName = ServerName;
                if (!mainSecureDB)
                {
                    crConnectioninfo.ServerName = ServerName;
                    crConnectioninfo.DatabaseName = DatabaseName;
                    crConnectioninfo.UserID = DatabaseUser;
                    crConnectioninfo.Password = DatabasePassword;
                }
                else
                {
                    crConnectioninfo.IntegratedSecurity = true;
                }

                crTableLogOnInfo = crTable.LogOnInfo;
                crTableLogOnInfo.ConnectionInfo = crConnectioninfo;

                try
                {
                    crTable.ApplyLogOnInfo(crTableLogOnInfo);
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("ApplyLogOnInfo failed: " + ex.ToString());
                    _log.Error(ex);
                }

                try
                {
                    if (crTable.TestConnectivity())
                        ConWorks = true;
                    else
                        ConWorks = false;
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("Test Connectivity failed: " + ex.ToString());
                    _log.Error(ex);
                }

            }

            #region Logs into tables
            if (ConWorks)
            {
                if (rptClientDoc != null)
                {
                    GroupPath gp = new GroupPath();
                    string tmp = String.Empty;
                    try
                    {
                        rptClientDoc.RowsetController.GetSQLStatement(gp, out tmp);
                        System.Diagnostics.Debug.WriteLine("Sql Statement: " + tmp);
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "Fetching SQL statements is not supported for this report.")
                        {
                            CrystalDecisions.ReportAppServer.Controllers.DatabaseController databaseController = rpt.ReportClientDocument.DatabaseController;
                            ISCRTable oldTable = (ISCRTable)databaseController.Database.Tables[0];

                            if (nvp2.Name.ToString() == crConnectioninfo.DatabaseName.ToString())
                            {
                                if (((dynamic)oldTable).CommandText.ToString() != null)
                                {
                                    System.Diagnostics.Debug.WriteLine("Main Report Command: " + ((dynamic)oldTable).CommandText.ToString());
                                }
                            }
                            else
                                System.Diagnostics.Debug.WriteLine("Wrong log on Server Name: Database: " + nvp2.Value.ToString() + " does not exist");
                        }
                        else
                        {
                            System.Diagnostics.Debug.WriteLine("ERROR: " + ex.Message);
                        }
                    }
                }

                //set the crSections object to the current report's sections
                CrystalDecisions.CrystalReports.Engine.Sections crSections = rpt.ReportDefinition.Sections;
                int flcnt = 0;
                bool SecureDB;

                //loop through all the sections to find all the report objects
                foreach (CrystalDecisions.CrystalReports.Engine.Section crSection in crSections)
                {
                    crReportObjects = crSection.ReportObjects;
                    //loop through all the report objects to find all the subreports
                    foreach (CrystalDecisions.CrystalReports.Engine.ReportObject crReportObject in crReportObjects)
                    {
                        if (crReportObject.Kind == ReportObjectKind.SubreportObject)
                        {
                            try
                            {
                                ++flcnt;

                                //you will need to typecast the reportobject to a subreport 
                                //object once you find it
                                crSubreportObject = (CrystalDecisions.CrystalReports.Engine.SubreportObject)crReportObject;
                                string mysubname = crSubreportObject.SubreportName.ToString();
                                //mysubname = " ";
                                //open the subreport object
                                crSubreportDocument = crSubreportObject.OpenSubreport(crSubreportObject.SubreportName);

                                CrystalDecisions.CrystalReports.Engine.Database crSubDatabase;
                                CrystalDecisions.CrystalReports.Engine.Tables crSubTables;
                                TableLogOnInfo crTableSubLogOnInfo;

                                CrystalDecisions.Shared.ConnectionInfo crSubConnectioninfo = new CrystalDecisions.Shared.ConnectionInfo();

                                //set the database and tables objects to work with the subreport
                                crSubDatabase = crSubreportDocument.Database;
                                //crSubTables = crSubTables.Tables;
                                tableIndex = 0;

                                // get the DB name from the report
                                CrystalDecisions.Shared.NameValuePair2 nvp2Sub = (NameValuePair2)crSubreportDocument.Database.Tables[tableIndex].LogOnInfo.ConnectionInfo.Attributes.Collection[1];

                                //btnSQLStatement.Text = "Data Sources are the not the same: Database: " + nvp2.Value.ToString();

                                //loop through all the tables in the subreport and 
                                //set up the connection info and apply it to the tables
                                foreach (CrystalDecisions.CrystalReports.Engine.Table crTable in crTables)
                                {   // check if integrated security is enabled
                                    SecureDB = crSubreportDocument.Database.Tables[tableIndex].LogOnInfo.ConnectionInfo.IntegratedSecurity;
                                    string TableName = crTable.Name.ToString();
                                    tableIndex++;
                                    crConnectioninfo.ServerName = ServerName;
                                    if (!SecureDB)
                                    {
                                        crConnectioninfo.DatabaseName = DatabaseName;
                                        crConnectioninfo.UserID = DatabaseUser;
                                        crConnectioninfo.Password = DatabasePassword;
                                    }
                                    else
                                        crConnectioninfo.IntegratedSecurity = true;

                                    crTableLogOnInfo = crTable.LogOnInfo;
                                    crTableLogOnInfo.ConnectionInfo = crConnectioninfo;
                                    crTable.ApplyLogOnInfo(crTableLogOnInfo);

                                    // Get subreport SQl 
                                    foreach (String resultField in rptClientDoc.SubreportController.GetSubreportNames())
                                    {
                                        SubreportController subreportController = rptClientDoc.SubreportController;
                                        SubreportClientDocument subreportClientDocument = subreportController.GetSubreport(resultField);
                                        subreportClientDocument.DatabaseController.LogonEx(crConnectioninfo.ServerName, crConnectioninfo.DatabaseName, crConnectioninfo.UserID, crConnectioninfo.Password);

                                        try
                                        {
                                            CrystalDecisions.ReportAppServer.ReportDefModel.ReportObjects rptObjs;
                                            rptObjs = rptClientDoc.ReportDefController.ReportObjectController.GetAllReportObjects();

                                            foreach (CrystalDecisions.ReportAppServer.ReportDefModel.ReportObject rptObj1 in rptObjs)
                                            {
                                                switch (rptObj1.Kind)
                                                {
                                                    case CrReportObjectKindEnum.crReportObjectKindSubreport:
                                                        CrystalDecisions.ReportAppServer.ReportDefModel.SubreportObject subObj1;
                                                        subObj1 = (CrystalDecisions.ReportAppServer.ReportDefModel.SubreportObject)rptObj1;

                                                        GroupPath gp1 = new GroupPath();
                                                        gp1.FromString("");
                                                        string sql = String.Empty;
                                                        subreportClientDocument.RowsetController.GetSQLStatement(gp1, out sql);

                                                        System.Diagnostics.Debug.WriteLine("Subreport: " + resultField.ToString() + Environment.NewLine + sql);
                                                        break;
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            _log.Error(ex);
                                            {
                                                if (nvp2Sub.Name.ToString() == crConnectioninfo.DatabaseName.ToString())
                                                {
                                                    if (ex.Message == "Fetching SQL statements is not supported for this report.")
                                                    {
                                                        CrystalDecisions.ReportAppServer.Controllers.DatabaseController databaseController = rpt.ReportClientDocument.SubreportController.GetSubreport(resultField.ToString()).DatabaseController;
                                                        ISCRTable oldTable = (ISCRTable)databaseController.Database.Tables[0];
                                                        System.Diagnostics.Debug.WriteLine("Subreport: " + resultField.ToString() + " - Command: " + ((dynamic)oldTable).CommandText.ToString());
                                                    }
                                                    else
                                                    {
                                                        System.Diagnostics.Debug.WriteLine("Logon Subreport: " + resultField.ToString() + " - ERROR: " + ex.Message);
                                                    }
                                                }
                                                else
                                                    System.Diagnostics.Debug.WriteLine("Wrong Subreport log on Server Name or Database: " + nvp2Sub.Value.ToString() + " does not exist or getting SQL from subreport not supported");
                                            }
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                _log.Error(ex);
                                {
                                    if (ex.Message == "Fetching SQL statements is not supported for this report.")
                                    {
                                        System.Diagnostics.Debug.WriteLine("ERROR: " + ex.Message);
                                    }
                                    else
                                    {
                                        //btnSQLStatement.Text += "ERROR: " + ex.Message;
                                        //btnSQLStatement.AppendText("\n");
                                    }
                                }
                            }
                        }
                    }
                }


                //if (btrVerifyDatabase.Checked)
                //--??   rpt.VerifyDatabase();
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("No data source or failed to log on");
            }
            #endregion

            #endregion

            IsLoggedOn = true;

            //--?? rpt.VerifyDatabase();

            #region Crystal Parameters
            //-- Applicable to all reports
            rpt.SetParameterValue("SiteId", ReportFilterInUse.SiteId);
            rpt.SetParameterValue("SeasonId", (int)Application.Current.Properties["SeasonId"]);
            rpt.SetParameterValue("DetailAndOrSummary", ReportFilterInUse.DetailSummary.GetDescription());


            //-- Applicable to specific reports
            switch (ReportFilterInUse.ReportDefinitionEnumValue)
            {
                case ReportDefintionEnum.DispatcherReport:

                    break;

                case ReportDefintionEnum.CustomerReport:
                case ReportDefintionEnum.TicketReport:
                case ReportDefintionEnum.TruckReport:
                case ReportDefintionEnum.InventoryReport:

                    ParameterDiscreteValue crParameterDiscreteValue_IsCash = new ParameterDiscreteValue();
                    ParameterFieldDefinition crParameterfieldDefinition_IsCash = rpt.DataDefinition.ParameterFields["IsCash"];
                    ParameterValues crParameterValues_IsCash = crParameterfieldDefinition_IsCash.CurrentValues;


                    rpt.SetParameterValue("TicketDateTimeFrom", ReportFilterInUse.BeginDateTime);
                    rpt.SetParameterValue("TicketDateTimeTo", ReportFilterInUse.EndDateTime);

                    rpt.SetParameterValue("SortedBy", ReportFilterInUse.SortBy.GetDescription());

                    switch (ReportFilterInUse.CustomerType)
                    {
                        case CustomerTypeEnum.NonCash:
                            crParameterDiscreteValue_IsCash.Value = 0;
                            crParameterValues_IsCash.Add(crParameterDiscreteValue_IsCash);

                            crParameterfieldDefinition_IsCash.ApplyCurrentValues(crParameterValues_IsCash);
                            break;
                        case CustomerTypeEnum.Cash:
                            crParameterDiscreteValue_IsCash.Value = 1;
                            crParameterValues_IsCash.Add(crParameterDiscreteValue_IsCash);

                            crParameterfieldDefinition_IsCash.ApplyCurrentValues(crParameterValues_IsCash);
                            break;
                        default:
                            crParameterDiscreteValue_IsCash.Value = 0;

                            crParameterValues_IsCash.Add(crParameterDiscreteValue_IsCash);

                            crParameterDiscreteValue_IsCash = new ParameterDiscreteValue();
                            crParameterDiscreteValue_IsCash.Value = 1;
                            crParameterValues_IsCash.Add(crParameterDiscreteValue_IsCash);

                            crParameterfieldDefinition_IsCash.ApplyCurrentValues(crParameterValues_IsCash);
                            break;
                    }


                    ParameterDiscreteValue crParameterDiscreteValue_IncludeVoids = new ParameterDiscreteValue();
                    ParameterFieldDefinition crParameterfieldDefinition_IncludeVoids = rpt.DataDefinition.ParameterFields["VoidTypes"];
                    ParameterValues crParameterValues_IncludeVoids = crParameterfieldDefinition_IncludeVoids.CurrentValues;

                    crParameterDiscreteValue_IncludeVoids.Value = 0;

                    crParameterValues_IncludeVoids.Add(crParameterDiscreteValue_IncludeVoids);

                    if (ReportFilterInUse.IsVoidsIncluded)
                    {
                        crParameterDiscreteValue_IncludeVoids = new ParameterDiscreteValue();
                        crParameterDiscreteValue_IncludeVoids.Value = 1;
                        crParameterValues_IncludeVoids.Add(crParameterDiscreteValue_IncludeVoids);
                    }
                    crParameterfieldDefinition_IncludeVoids.ApplyCurrentValues(crParameterValues_IncludeVoids);

                    //rpt.SetParameterValue("IsVoidedTicketsIncluded", ReportFilterInUse.IsVoidsIncluded);


                    var sqlWhereFilter = string.Empty;
                    if (ReportFilterInUse.AppliedFilters != null)
                    {
                        foreach (var filter in ReportFilterInUse.AppliedFilters)
                        {
                            sqlWhereFilter += filter.FilterSQLColumnOperatorValue;
                        }
                    }
                    rpt.SetParameterValue("SqlWhereFilter", sqlWhereFilter);


                    break;
            }
            #endregion

            _log.Info($"Report Display Viewer-Post CR code: Server '{crConnectioninfo.ServerName}'  Database '{crConnectioninfo.DatabaseName}'  Report '{rpt.ReportClientDocument.ReportDocument.Name}'");

            try
            {
                RptViewer = new SAPBusinessObjects.WPF.Viewer.CrystalReportsViewer();
                RptViewer.ViewerCore.ReportSource = rpt;
            }
            catch (Exception ex)
            {
                var x = ex.Message;
            }
        }


        private void Viewer_Loaded(object sender, RoutedEventArgs e)
        {
            //Viewer.Owner = Window.GetWindow(this);

            ReportGenerate();
        }


        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //Viewer.Dispose();
            //Close();
            //e.Cancel = true;
        }
    }
}