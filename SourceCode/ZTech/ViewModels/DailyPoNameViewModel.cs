﻿using System;
using System.Windows.Input;
using Caliburn.Micro;
using ZTech.Controls;
using ZTech.ViewModels.Abstract;

namespace ZTech.ViewModels
{
    public class DailyPoNameViewModel : BaseViewModel
    {
        private readonly IEventAggregator _eventAggregator;
        private readonly ILog _log;
        private string _errors;
        private string _poNumber;

        public DailyPoNameViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            _log = LogManager.GetLog(typeof(DailyPoNameViewModel));
            DisplayName = "Create Daily P.O.";
        }

        public string Errors
        {
            get { return _errors; }
            set
            {
                if (value == _errors) return;
                _errors = value;
                NotifyOfPropertyChange(() => Errors);
            }
        }

        #region Actions

        public string PoNumber
        {
            get { return _poNumber; }
            set
            {
                if (value == _poNumber) return;
                _poNumber = value;
                NotifyOfPropertyChange(() => PoNumber);
            }
        }

        public void Cancel()
        {
            TryClose(false);
        }

        public void KeyHandle(Key key)
        {
            if (key == Key.F2)
            {
                Done();
            }
        }

        public void Done()
        {
            try
            {
                if (!IsValid())
                    return;

                TryClose(true);
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Errors = string.Format(
                    "Operation failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                    ex.Message);

                ShowMessage("Error", Errors, NotificationType.Error);
            }
        }

        private bool IsValid()
        {
            if (string.IsNullOrWhiteSpace(PoNumber))
            {
                Errors = string.Format(
                    "Validation Failed. {0} {1}", Environment.NewLine, "Purchase Order nubmer cannot be Empty.");

                ShowMessage("Validation Errors", Errors, NotificationType.Warning);
                return false;
            }

            return true;
        }

        #endregion
    }
}