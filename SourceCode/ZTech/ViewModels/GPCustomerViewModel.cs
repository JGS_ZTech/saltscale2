﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Caliburn.Micro;
using Contract.Domain;
using Contract.Services;
using ZTech.Controls;
using ZTech.ViewModels.Abstract;

namespace ZTech.ViewModels
{
    public class GPCustomerViewModel : BaseViewModel
    {
        private readonly IConfiguration _configuration;
        private readonly IEventAggregator _eventAggregator;
        private readonly ILog _log;
        private ISiteConfig _selectedSite;
        private List<ISiteConfig> _siteList;
        private bool _allCustomers;
        private bool _gpCustomers;
        private bool _nonGpCustomers;
        private bool _isBusy;
        private ISeason _season;
        private List<IGpCustomer> _customers;
        private IGpCustomer _selectedCustomer;

        public GPCustomerViewModel(IEventAggregator eventAggregator, IConfiguration configuration)
        {
            _eventAggregator = eventAggregator;
            _configuration = configuration;
            _log = LogManager.GetLog(typeof(GPCustomerViewModel));
            DisplayName = @"Great Plains Customers";
        }

        public List<ISiteConfig> SiteList
        {
            get { return _siteList; }
            set
            {
                if (Equals(value, _siteList)) return;
                _siteList = value;
                NotifyOfPropertyChange(() => SiteList);
            }
        }

        public void EditItem()
        {
            SaveGpCustomer();
        }

        public ISiteConfig SelectedSite
        {
            get { return _selectedSite; }
            set
            {
                if (Equals(value, _selectedSite)) return;
                _selectedSite = value;
                NotifyOfPropertyChange(() => SelectedSite);
                if (value != null)
                {
                    ConnectionString = _configuration.ConvertConnectionString(SelectedSite.ConnectionString);
                    InitSeason();
                    FillGrid();
                }
            }
        }

        private void InitSeason()
        {
            _season = IoC.Get<ICommonService>().GetActiveSeason(ConnectionString);
        }

        public string ConnectionString { get; set; }

        public bool AllCustomers
        {
            get { return _allCustomers; }
            set
            {
                if (value.Equals(_allCustomers)) return;
                _allCustomers = value;
                NotifyOfPropertyChange(() => AllCustomers);
                if (value)
                    FillGrid();
            }
        }

        public bool GPCustomers
        {
            get { return _gpCustomers; }
            set
            {
                if (value.Equals(_gpCustomers)) return;
                _gpCustomers = value;
                NotifyOfPropertyChange(() => GPCustomers);
                if (value)
                    FillGrid();
            }
        }

        public bool NonGPCustomers
        {
            get { return _nonGpCustomers; }
            set
            {
                if (value.Equals(_nonGpCustomers)) return;
                _nonGpCustomers = value;
                NotifyOfPropertyChange(() => NonGPCustomers);
                if (value)
                    FillGrid();
            }
        }

        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                if (value.Equals(_isBusy)) return;
                _isBusy = value;
                NotifyOfPropertyChange(() => IsBusy);
            }
        }

        #region Actions

        private void FillGrid()
        {
            if(SelectedSite == null || string.IsNullOrWhiteSpace(SelectedSite.ConnectionString) || (!AllCustomers && !GPCustomers && !NonGPCustomers))
                return;

            IsBusy = true;
            var task = new Task(GetCustomers);
            task.Start();
        }

        private void GetCustomers()
        {
            try
            {
                Customers = IoC.Get<ICustomerService>().GetGpCustomers(ConnectionString, _season.SeasonId, AllCustomers, GPCustomers, NonGPCustomers);
                SelectedCustomer = Customers.FirstOrDefault();
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                ShowMessage("Error", string.Format(
                    "Initialization failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                    ex.Message), NotificationType.Error);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public IGpCustomer SelectedCustomer
        {
            get { return _selectedCustomer; }
            set
            {
                if (Equals(value, _selectedCustomer)) return;
                _selectedCustomer = value;
                NotifyOfPropertyChange(() => SelectedCustomer);
            }
        }

        public List<IGpCustomer> Customers
        {
            get { return _customers; }
            set
            {
                if (Equals(value, _customers)) return;
                _customers = value;
                NotifyOfPropertyChange(() => Customers);
            }
        }

        public void KeyHandle(Key key)
        {
            if (key == Key.F2)
            {
                Close();
            }
        }

        public void Close()
        {
            TryClose(false);
        }

        public void OnClose(CancelEventArgs args)
        {
        }

        private void SaveGpCustomer()
        {
            if (SelectedCustomer != null && SelectedSite != null)
            {
                IoC.Get<ICustomerService>().UpdateGpId(SelectedCustomer, _season.SeasonId, ConnectionString);
            }
        }

        #endregion
        protected override void OnInitialize()
        {
            base.OnInitialize();
            SiteList = _configuration.ExistingSites;
            ISiteConfig site = _configuration.SelectedSite;
            SelectedSite = SiteList.First(s => s.Name == site.Name);
        }

    }
}