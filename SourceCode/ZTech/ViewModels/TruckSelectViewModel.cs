﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Caliburn.Micro;
using Contract.Domain;
using Contract.Services;
using Action = System.Action;

namespace ZTech.ViewModels
{
    public interface ITruckSelectViewModel: IScreen
    {
        int? TruckId { get; set; }
        ITruckInfo SelectedTruck { get; set; }
    }

    public class TruckSelectViewModel : Screen, ITruckSelectViewModel
    {
        private readonly IEventAggregator _eventAggregator;
        private readonly ILog _log;
        private readonly int _seasonId;
        private bool _canDone;
        private bool _canSearch;
        private string _filter;
        private ITruckInfo _selectedTruck;
        private List<ITruckInfo> _truckList;

        public TruckSelectViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            _log = LogManager.GetLog(typeof (TruckSelectViewModel));
            _seasonId = (int) Application.Current.Properties["SeasonId"];
            DisplayName = "Truck selector";
        }

        public int? TruckId { get; set; }

        public string Filter
        {
            get { return _filter; }
            set
            {
                if (value == _filter) return;
                _filter = value;
                NotifyOfPropertyChange(() => Filter);
                Search();
            }
        }

        public ITruckInfo SelectedTruck
        {
            get { return _selectedTruck; }
            set
            {
                if (Equals(value, _selectedTruck)) return;
                _selectedTruck = value;
                NotifyOfPropertyChange(() => SelectedTruck);

                CanDone = SelectedTruck != null;
            }
        }

        public List<ITruckInfo> TruckList
        {
            get { return _truckList; }
            set
            {
                if (Equals(value, _truckList)) return;
                _truckList = value;
                NotifyOfPropertyChange(() => TruckList);
            }
        }

        public bool CanDone
        {
            get { return _canDone; }
            set
            {
                if (value.Equals(_canDone)) return;
                _canDone = value;
                NotifyOfPropertyChange(() => CanDone);
            }
        }

        protected override void OnInitialize()
        {
            base.OnInitialize();
            try
            {
                TruckList = IoC.Get<ITruckService>().GetFilteredTrucks(string.Empty, _seasonId);
                if (!TruckId.HasValue)
                    return;

                var selected = TruckList.FirstOrDefault(t => t.Id == TruckId.Value);
                if (selected != null)
                    SelectedTruck = selected;
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(
                            string.Format(
                                "Initialization failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                                ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));

                    TryClose(false);
                };

                action.OnUIThreadAsync();
            }
        }

        public void Cancel()
        {
            TryClose(false);
        }

        public void Done()
        {
            if (SelectedTruck == null)
                return;

            TryClose(true);
        }

        public void KeyHandle(Key key)
        {
            // Process F2
            if (key == Key.F2 && SelectedTruck != null)
            {
                Done();
            }
            else
            {
                // Process Default
                if (key == Key.Enter && SelectedTruck != null)
                {
                    TryClose(true);
                }
            }
        }

        public void ExecuteSelect(Key key)
        {
            if (key == Key.Enter && SelectedTruck != null)
            {
                TryClose(true);
            }
        }
        public void Search()
        {
            TruckList = IoC.Get<ITruckService>().GetFilteredTrucks(Filter, _seasonId);
            var selected = TruckId.HasValue ? TruckList.FirstOrDefault(t => t.Id == TruckId.Value) : TruckList.FirstOrDefault();
            
            if (selected != null)
                SelectedTruck = selected;
        }

        public void RefreshList()
        {
            TruckList = IoC.Get<ITruckService>().GetFilteredTrucks(string.Empty, _seasonId);
            Filter = null;
        }
    }
}