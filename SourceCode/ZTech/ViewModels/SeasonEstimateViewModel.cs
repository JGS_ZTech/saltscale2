﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Caliburn.Micro;
using Contract.Domain;
using Contract.Services;
using ZTech.Controls;
using ZTech.ViewModels.Abstract;
using System.Windows.Input;
using Syncfusion.UI.Xaml.Grid.Converter;
using Syncfusion.XlsIO;
using Syncfusion.Data;
using System.Collections.ObjectModel;
using Microsoft.Win32;
using System.IO;

namespace ZTech.ViewModels
{
    public class SeasonEstimateViewModel : BaseViewModel
    {
        private readonly IConfiguration _configuration;
        private readonly IEventAggregator _eventAggregator;
        private readonly ILog _log;
        private bool _isBusy;
        private ISeason _season;
        private ObservableCollection<ICustomerSeasonEstimate> _allCustomers;
        private ObservableCollection<ICustomerSeasonEstimate> _filteredCustomers;
        private ICustomerSeasonEstimate _selectedCustomer;
        private string _customerSearchTerm;
        private string _entitySearchTerm;

        private Syncfusion.UI.Xaml.Grid.SfDataGrid _dataGridToExport;

        public string ConnectionString { get; set; }

        public SeasonEstimateViewModel(IEventAggregator eventAggregator, IConfiguration configuration)
        {
            _eventAggregator = eventAggregator;
            _configuration = configuration;
            _log = LogManager.GetLog(typeof(SeasonEstimateViewModel));
            DisplayName = @"Season Estimate";
        }

        #region Properties

        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                if (value.Equals(_isBusy)) return;
                _isBusy = value;
                NotifyOfPropertyChange(() => IsBusy);
            }
        }

        public ICustomerSeasonEstimate SelectedCustomer
        {
            get { return _selectedCustomer; }
            set
            {
                if (Equals(value, _selectedCustomer)) return;
                _selectedCustomer = value;
                NotifyOfPropertyChange(() => SelectedCustomer);
            }
        }

        public ObservableCollection<ICustomerSeasonEstimate> AllCustomers
        {
            get { return _allCustomers; }
            set
            {
                if (Equals(value, _allCustomers)) return;
                _allCustomers = value;
                NotifyOfPropertyChange(() => AllCustomers);
            }
        }

        public ObservableCollection<ICustomerSeasonEstimate> FilteredCustomers
        {
            get { return _filteredCustomers; }
            set
            {
                if (Equals(value, _filteredCustomers)) return;
                _filteredCustomers = value;
                NotifyOfPropertyChange(() => FilteredCustomers);
            }
        }

        public string CustomerSearchTerm
        {
            get
            {
                return _customerSearchTerm;
            }
            set
            {
                if (Equals(value, _customerSearchTerm)) return;
                _customerSearchTerm = value;
                NotifyOfPropertyChange(() => CustomerSearchTerm);

                FilteredCustomers = new ObservableCollection<ICustomerSeasonEstimate>(AllCustomers.Where(x => x.CustomerDispatcherName.StartsWith(_customerSearchTerm, StringComparison.OrdinalIgnoreCase)));
            }
        }

        public string EntitySearchTerm
        {
            get
            {
                return _entitySearchTerm;
            }
            set
            {
                if (Equals(value, _entitySearchTerm)) return;
                _entitySearchTerm = value;
                NotifyOfPropertyChange(() => EntitySearchTerm);

                FilteredCustomers = new ObservableCollection<ICustomerSeasonEstimate>(AllCustomers.Where(x => x.EntityName.StartsWith(_entitySearchTerm, StringComparison.OrdinalIgnoreCase)));
            }
        }

        #endregion

        protected override void OnInitialize()
        {
            base.OnInitialize();

            var SiteList = _configuration.ExistingSites;
            ISiteConfig site = _configuration.SelectedSite;
            var SelectedSite = SiteList.First(s => s.Name == site.Name);

            ConnectionString = _configuration.ConvertConnectionString(SelectedSite.ConnectionString);
            InitSeason();
            FillGrid();
        }


        private void InitSeason()
        {
            _season = IoC.Get<ICommonService>().GetActiveSeason(ConnectionString);
        }


        #region Actions

        private void FillGrid()
        {
            IsBusy = true;
            var task = new Task(GetCustomers);
            task.Start();
        }

        private void GetCustomers()
        {
            try
            {
                AllCustomers = IoC.Get<ICustomerService>().GetCustomerSeasonEstimate(_season.SeasonId);
                FilteredCustomers = AllCustomers;
                SelectedCustomer = AllCustomers.FirstOrDefault();
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                ShowMessage("Error", $"Initialization failed with exception '{ex.Message}'. Please reopen application and try again or contact HelpDesk.", NotificationType.Error);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public void OnCurrentCellBeginEdit(Syncfusion.UI.Xaml.Grid.CurrentCellBeginEditEventArgs e)
        {
            SelectedCustomer.BeginEdit();
        }

        public void OnCurrentCellValidating(Syncfusion.UI.Xaml.Grid.CurrentCellValidatingEventArgs e)
        {
            if (e.OldValue.ToString() != e.NewValue.ToString())
            {
                var message = $"Customer {SelectedCustomer.CustomerDispatcherName} with material {SelectedCustomer.MaterialShortName} already has an estimate of {SelectedCustomer.Estimate}.{Environment.NewLine + Environment.NewLine}Are you sure you want to change it to {e.NewValue.ToString()}?";
                var msgbox = MessageBox.Show(message, "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);

                if (msgbox == MessageBoxResult.Yes)
                {
                    SelectedCustomer.EndEdit();
                    SelectedCustomer.Estimate = Convert.ToDecimal(e.NewValue.ToString());
                    SaveSeasonEstimate();
                    //FillGrid();
                }
            }
            else
            {
                SelectedCustomer.CancelEdit();
            }
        }

        public void OnCustomerEstimateItemsSourceChanged(Syncfusion.UI.Xaml.Grid.SfDataGrid dataGrid)
        {
            _dataGridToExport = dataGrid;
        }

        public void ExportToExcel()
        {
            var options = new ExcelExportingOptions();
            options.ExcelVersion = ExcelVersion.Excel2016;
            var excelEngine = _dataGridToExport.ExportToExcel(_dataGridToExport.View, options);
            var workBook = excelEngine.Excel.Workbooks[0];
            workBook.Worksheets[0].UsedRange.AutofitColumns();
            workBook.Worksheets[0].UsedRange.AutofitRows();

            //workBook.SaveAs($@"C:\SaltScale\Export\Season Estimate {DateTime.Now.ToString("yyyy-MM-dd HHmm")}.xlsx");

            SaveFileDialog sfd = new SaveFileDialog
            {
                FilterIndex = 2,
                Filter = "Excel 2013 File(*.xlsx)|*.xlsx|Excel 2016 File(*.xlsx)|*.xlsx",
                FileName = $"Season Estimate {DateTime.Now.ToString("yyyy-MM-dd HHmm")}.xlsx"
            };

            if (sfd.ShowDialog() == true)
            {
                using (Stream stream = sfd.OpenFile())
                {
                    if (sfd.FilterIndex == 3)
                        workBook.Version = ExcelVersion.Excel2013;
                    else
                        workBook.Version = ExcelVersion.Excel2016;

                    workBook.SaveAs(stream);
                }

                //-- Confirmation to open the file
                if (MessageBox.Show("Do you want to open the saved file?", "Excel Workbook has been created",
                                    MessageBoxButton.YesNo, MessageBoxImage.Information) == MessageBoxResult.Yes)
                {
                    //-- Launch Excel
                    System.Diagnostics.Process.Start(sfd.FileName);
                }
            }
        }

        public void KeyHandle(Key key)
        {
            if (key == Key.F2)
            {
                Done();
            }
        }

        public void Done()
        {
            TryClose(false);
        }

        public void Close()
        {
            TryClose(false);
        }

        public void OnClose(CancelEventArgs args)
        {
        }

        private void SaveSeasonEstimate()
        {
            if (SelectedCustomer != null)
            {
                IoC.Get<ICustomerService>().UpdateSeasonEstimate(SelectedCustomer, _season.SeasonId, ConnectionString);
            }
        }

        #endregion


        public void CustomerGrid_ValidationError(object sender, ValidationErrorEventArgs e)
        {
            _log.Warn($"Invalid Estimate value entered. BidID: '{0}', Value: '{this.SelectedCustomer.Estimate}'");

            this.SelectedCustomer.Estimate = 0;
        }
    }
}