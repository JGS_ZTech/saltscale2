﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Caliburn.Micro;
using Contract.Events;
using Contract.Exception;
using Contract.Services;
using ZTech.Controls;
using ZTech.ViewModels.ViewObject;
using Action = System.Action;

namespace ZTech.ViewModels.Abstract
{
    public abstract class BaseTruckOwnerViewModel : BaseViewModel
    {
        protected readonly IEventAggregator _eventAggregator;
        protected ILog _log;
        protected readonly int _seasonId;
        private IContactWidgetViewModel _contactWidget;
        private string _errors;
        private TruckOwnerDataModel _owner;


        public BaseTruckOwnerViewModel(IEventAggregator eventAggregator, string displayName)
        {
            _eventAggregator = eventAggregator;
            _seasonId = (int)Application.Current.Properties["SeasonId"];
            DisplayName = displayName;
        }


        public string Errors
        {
            get { return _errors; }
            set
            {
                if (value == _errors) return;
                _errors = value;
                NotifyOfPropertyChange(() => Errors);
            }
        }

        public IContactWidgetViewModel ContactWidget
        {
            get { return _contactWidget; }
            set
            {
                if (Equals(value, _contactWidget)) return;
                _contactWidget = value;
                NotifyOfPropertyChange(() => ContactWidget);
            }
        }

        public TruckOwnerDataModel Owner
        {
            get { return _owner; }
            set
            {
                if (Equals(value, _owner)) return;
                _owner = value;
                NotifyOfPropertyChange(() => Owner);
            }
        }

        protected override void OnInitialize()
        {
            base.OnInitialize();
            try
            {
                InitializeTruckOwner();
                InitializeContact();
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(
                            string.Format(
                                "Initialization failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                                ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));

                    TryClose(false);
                };

                action.OnUIThreadAsync();
            }
        }

        protected abstract void InitializeTruckOwner();

        private void InitializeContact()
        {
            ContactWidget = IoC.Get<IContactWidgetViewModel>();
            ContactWidget.ActivateWith(this);
            ContactWidget.SetData(Owner.Contact);
        }

        public void KeyHandle(Key key)
        {
            if (key == Key.F2)
            {
                Done();
            }
        }

        public void Cancel()
        {
            TryClose(false);
        }

        private bool Validate()
        {
            Errors = null;

            ICollection<ValidationResult> validationResults = new Collection<ValidationResult>();

            bool result = Validator.TryValidateObject(Owner, new ValidationContext(Owner), validationResults);

            List<ValidationResult> messages = validationResults.Union(ContactWidget.Validate()).ToList();

            if (!result || messages.Any())
            {
                Errors = string.Format(
                    "Validation Failed.{0}{1}", Environment.NewLine,
                    string.Join("\n", messages.Select(v => v.ErrorMessage)));
                ShowMessage("Validation Errors", Errors, NotificationType.Warning);

                return false;
            }

            return true;
        }

        public void Done()
        {
            try
            {
                if (!Validate())
                    return;
                
                Owner.Contact = ContactWidget.GetData();

                //-- IsPrimary is set explicitly here because a truck owner will have one contact.
                //-- Not UI checkbox to indicate otherwise and a primary is needed for reporting
                Owner.Contact.IsPrimary = true;

                Owner.UpdateTime = DateTime.Now;
                int ownerId = IoC.Get<ITruckService>().SaveTruckOwner(Owner, _seasonId);

                _eventAggregator.PublishOnCurrentThread(new TruckOwnerModifiedArgs(ownerId));

                TryClose(true);
            }
            catch (UniqueConstraintException ex)
            {
                _log.Error(ex);
                Errors = string.Format("Truck Owner saving failed. {0}", ex.Message);
                ShowMessage("Validation Errors", Errors, NotificationType.Warning);
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Errors = string.Format(
                    "Truck Owner saving failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                    ex.Message);

                ShowMessage("Error", Errors, NotificationType.Error);
            }
        }
    }
}