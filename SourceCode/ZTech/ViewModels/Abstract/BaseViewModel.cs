﻿using System;
using Caliburn.Micro;
using ZTech.Controls;
using Action = System.Action;

namespace ZTech.ViewModels.Abstract
{
    public abstract class BaseViewModel : Screen
    {
        protected void ShowMessage(string title, string message, NotificationType messageType)
        {
            Action action = () =>
            {
                using (var notification = new ToastPopUp(title, message, "Ok", messageType))
                {
                    notification.HyperlinkClicked+=NotificationOnHyperlinkClicked;
                    notification.Show();
                }
            };
            action.OnUIThread();
        }

        private void NotificationOnHyperlinkClicked(object sender, HyperLinkEventArgs args  )
        {
            var form = sender as ToastPopUp;

            if(form !=null)
                form.Close();
        }
    }
}