﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Caliburn.Micro;
using Contract.Domain;
using Contract.Events;
using Contract.Services;
using ZTech.Controls;
using ZTech.Helpers;
using ZTech.ViewModels.ViewObject;
using Action = System.Action;
using System.Collections.ObjectModel;

namespace ZTech.ViewModels.Abstract
{
    public abstract class BaseOrderViewModel : BaseViewModel
    {
        protected readonly string _defaultSite;
        protected readonly IEventAggregator _eventAggregator;
        protected ILog _log;
        protected readonly int _seasonId;
        private IReadOnlyList<ICompanyInfo> _companyList;
        private string _customerAddress;
        private string _errors;
        private List<IMaterialInfo> _materialList;
        private ObservableCollection<IBidInfo> _bidInfoList;
        private IOrder _order;
        private IMaterialInfo _selectedMaterial;
        private IBidInfo _selectedBidInfo;
        private IReadOnlyList<ISiteInfo> _siteList;
        private List<PurchaseOrderDataModel> _purchaseOrderList;
        protected PurchaseOrderDataModel _selectedPo;
        private bool _enableMaterial;
        private List<PurchaseOrderDataModel> _poList;
        private string _quantity;

        protected BaseOrderViewModel(IEventAggregator eventAggregator, string displayName)
        {
            _eventAggregator = eventAggregator;
            _log = LogManager.GetLog(typeof(BaseOrderViewModel));
            _seasonId = (int)Application.Current.Properties["SeasonId"];
            _defaultSite = IoC.Get<IConfiguration>().SelectedSite.Name;
            DisplayName = displayName;
        }

        public IOrder Order
        {
            get { return _order; }
            set
            {
                if (Equals(value, _order)) return;
                _order = value;
                NotifyOfPropertyChange(() => Order);
            }
        }

        public string Quantity
        {
            get { return _quantity; }
            set
            {
                if (value == _quantity) return;
                _quantity = value;
                NotifyOfPropertyChange(() => Quantity);
                if (!string.IsNullOrWhiteSpace(_quantity) && Order != null)
                {
                    int result;
                    if (int.TryParse(_quantity, out result))
                        Order.Quantity = result;
                }
                    
            }
        }

        public ObservableCollection<IBidInfo> BidInfoList
        {
            get { return _bidInfoList; }
            set
            {
                if (Equals(value, _bidInfoList)) return;
                _bidInfoList = value;
                NotifyOfPropertyChange(() => BidInfoList);
            }
        }

        public List<IMaterialInfo> MaterialList
        {
            get { return _materialList; }
            set
            {
                if (Equals(value, _materialList)) return;
                _materialList = value;
                NotifyOfPropertyChange(() => MaterialList);
            }
        }

        public IReadOnlyList<ISiteInfo> SiteList
        {
            get { return _siteList; }
            set
            {
                if (Equals(value, _siteList)) return;
                _siteList = value;
                NotifyOfPropertyChange(() => SiteList);
            }
        }

        public IReadOnlyList<ICompanyInfo> CompanyList
        {
            get { return _companyList; }
            set
            {
                if (Equals(value, _companyList)) return;
                _companyList = value;
                NotifyOfPropertyChange(() => CompanyList);
            }
        }

        public string CustomerAddress
        {
            get { return _customerAddress; }
            set
            {
                if (value == _customerAddress) return;
                _customerAddress = value;
                NotifyOfPropertyChange(() => CustomerAddress);
            }
        }

        public IBidInfo SelectedBidInfo
        {
            get { return _selectedBidInfo; }
            set
            {
                if (Equals(value, _selectedBidInfo)) return;
                _selectedBidInfo = value;
                NotifyOfPropertyChange(() => SelectedBidInfo);
                Order.Bid = IoC.Get<IBidService>().GetBidData(value.BidId ?? 0, _seasonId);
                Order.BidId = value.BidId ?? 0;
                Order.Material = IoC.Get<IMaterialService>().GetMaterial(_seasonId, value.MaterialId);
            }
        }

        public IMaterialInfo SelectedMaterial
        {
            get { return _selectedMaterial; }
            set
            {
                if (Equals(value, _selectedMaterial)) return;
                _selectedMaterial = value;
                NotifyOfPropertyChange(() => SelectedMaterial);
                Order.Material = value;
            }
        }

        public string Errors
        {
            get { return _errors; }
            set
            {
                if (value == _errors) return;
                _errors = value;
                NotifyOfPropertyChange(() => Errors);
            }
        }

        protected override void OnInitialize()
        {
            base.OnInitialize();
            try
            {
                var commonService = IoC.Get<ICommonService>();
                CompanyList = commonService.GetCompanies();
                SiteList = commonService.GetSites(_seasonId);
                InitializeOrder();
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    var messageBoxResult =
                        (MessageBox.Show(
                            string.Format(
                                "Initialization failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                                ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                    TryClose(false);
                };

                action.OnUIThreadAsync();
            }
        }

        protected void InitializeBidInfos(int customerId)
        {
            BidInfoList = IoC.Get<IBidService>().GetCustomerBidInformationList(_seasonId, customerId);
        }

        protected void InitializeMaterials(int customerId)
        {
            MaterialList = IoC.Get<IMaterialService>().GetMaterialsOfCustomer(_seasonId, customerId);
        }

        protected void InitializePo(int customerId)
        {
            var purchaseOrderList = IoC.Get<IPurchaseOrderService>().GetPurchaseOrderList(customerId, _seasonId).Select(p=>new PurchaseOrderDataModel(p)).ToList();
            PurchaseOrderList = new List<PurchaseOrderDataModel>();
            PurchaseOrderList.Add(new PurchaseOrderDataModel
            {
                Id = -1,
                PurchaseOrderNumber = string.Empty
            });

            PurchaseOrderList.AddRange(purchaseOrderList.OrderByDescending(x => x.CreateTime).ToList());
            POList = purchaseOrderList.OrderByDescending(x => x.PurchaseOrderNumber).ToList();
        }

        public List<PurchaseOrderDataModel> POList
        {
            get { return _poList; }
            set
            {
                if (Equals(value, _poList)) return;
                _poList = value;
                NotifyOfPropertyChange(() => POList);
            }
        }

                

        public bool EnableMaterial
        {
            get { return _enableMaterial; }
            set
            {
                if (value.Equals(_enableMaterial)) return;
                _enableMaterial = value;
                NotifyOfPropertyChange(() => EnableMaterial);
            }
        }

        public List<PurchaseOrderDataModel> PurchaseOrderList
        {
            get { return _purchaseOrderList; }
            set
            {
                if (Equals(value, _purchaseOrderList)) return;
                _purchaseOrderList = value;
                NotifyOfPropertyChange(() => PurchaseOrderList);
            }
        }

        protected abstract void InitializeOrder();
        public abstract void Done();
        public abstract void Cancel();
        public abstract void Delete();

        public void KeyHandle(Key key)
        {
            if (key == Key.F2)
            {
                Done();
            }
        }


        // 2018-Jun-22, JGS - Make this an abstract function to be implemented differently for New (Delete) and Existing (Discard) orders
        //public void Cancel()
        //{
        //    // Try to close the screen
        //    TryClose(false);
        //}

        //// 2018-Jun-22, JGS - Make this an abstract function to be implemented for Existing (Discard) orders
        //public void Delete()
        //{

        //    var msgbox = MessageBox.Show("Are you sure you want to delete this order?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);

        //    if (msgbox == MessageBoxResult.Yes)
        //    {
        //        if (IoC.Get<IOrderService>().DeleteOrder(Order.OrderId))
        //        {
        //            _eventAggregator.PublishOnCurrentThread(new OrderModifiedArgs(Order.OrderId));
        //            TryClose(true);
        //        }
        //        else
        //        {
        //            _log.Warn($"Order Deletion wasn't successful. Order # {Order.OrderNumber},  Order Id {Order.OrderId}");
        //            Action action =
        //                () =>
        //                {
        //                    MessageBox.Show($"Order number {Order.OrderNumber} (Order Id: {Order.OrderId}) wasn't able to be removed.  Please pass this onto IT to look into", "Order Deletion Failed", MessageBoxButton.OK, MessageBoxImage.None);
        //                };
        //            action.OnUIThreadAsync();
        //        }
        //    }
        //}

        protected abstract void SpecificValidation(List<string> errors);

        protected bool Validate()
        {
            Errors = null;
            var str = new List<string>();

            if (Order.Quantity <= 0)
                str.Add("The quantity must be more than 0");

            if (Order.Quantity >= 1000000)
                str.Add("The quantity must be a number less than 1 Million");

            if (SelectedMaterial == null)
                str.Add("Material must be selected");

            // Processs Overlimit
            if (Order.SiteId > 0
                && SelectedMaterial != null 
                && SelectedPO != null 
                && SelectedPO.Id > 0
                && !IoC.Get<IOrderService>().IsPurchaseOrderQuantityAcceptable(SelectedPO.Id, Order.POOverride.Trim(), SelectedMaterial.Id, Order.OrderId, Order.Quantity, Order.Customer.Id))
            {
                str.Add("Purchase Order balance isn't big enough to include this order");
            }

            if (!string.IsNullOrWhiteSpace(Order.POOverride) 
                && SelectedMaterial != null
                && SelectedPO != null
                && SelectedPO.Id > 0
                && !IoC.Get<IOrderService>().IsPOValid(Order.POOverride.Trim(), SelectedMaterial.Id,Order.OrderId, Order.Customer.Id))
            {
                str.Add("Invalid PO # or selected material does not match to PO material.");
            }

            SpecificValidation(str);

            if (str.Any())
            {
                Errors = $"Validation Failed{Environment.NewLine}{Environment.NewLine}{string.Join("\n", str)}";
                ShowMessage("Validation Errors", Errors, NotificationType.Warning);

                return false;
            }

            // 2018-Oct-03, JGS - Determine if the value is in the "overage" range
            if (Order.SiteId > 0
                && SelectedMaterial != null
                && SelectedPO != null
                && SelectedPO.Id > 0)
            {
                double PurchaseOrderInOverage = IoC.Get<IOrderService>().PurchaseOrderInOverage(SelectedPO.Id, Order.POOverride.Trim(), SelectedMaterial.Id, Order.OrderId, Order.Quantity, Order.Customer.Id);
                double PurchaseOrderOverageValue = IoC.Get<IOrderService>().PurchaseOrderOverageValue(SelectedPO.Id, Order.POOverride.Trim(), SelectedMaterial.Id, Order.OrderId, Order.Quantity, Order.Customer.Id);
                if (PurchaseOrderInOverage > 0)
                {
                    string strWarning = String.Format("Including this change, the total of orders ({0:0} tons) for this PO will be greater than the original {1:0} tons and less than the allowed overage of {2:0} tons. Update the PO quantity to a minimum of {3:0} tons.", PurchaseOrderInOverage, SelectedPO.QuantityOfPO, PurchaseOrderOverageValue, (PurchaseOrderInOverage + 1).ToString("#"));
                    MessageBox.Show(strWarning, "Update Purchase Order: " + Order.POOverride.Trim(), MessageBoxButton.OK, MessageBoxImage.Exclamation);

                    // Allow the user to proceed
                    return true;
                }
            }

            return true;
        }
        protected int? ParseIntValue(string income)
        {
            if (string.IsNullOrWhiteSpace(income))
                return null;

            income = income.Replace(",", string.Empty);
            income = income.Replace("'", string.Empty);
            income = income.Replace(".", string.Empty);

            int result;

            if (int.TryParse(income.Trim(), out result))
                return result;

            return null;
        }

        public abstract PurchaseOrderDataModel SelectedPO { get; set; }


        protected void ChooseBid()
        {
            //GetCustomerBidInformationList
        }

        protected void ChooseMaterial(int materialId)
        {
            IMaterialInfo materialInfo = MaterialList.FirstOrDefault(m => m.Id == materialId);
            if (materialInfo == null)
            {
                var inactiveMaterial = IoC.Get<IMaterialService>().GetInactiveMaterial(materialId);
                if (inactiveMaterial != null)
                {
                    MaterialList.Add(inactiveMaterial);
                    SelectedMaterial = inactiveMaterial;
                    Order.Material = SelectedMaterial;
                }
            }
            else
            {
                SelectedMaterial = materialInfo;
                Order.Material = materialInfo;

            }
        }
    }
}