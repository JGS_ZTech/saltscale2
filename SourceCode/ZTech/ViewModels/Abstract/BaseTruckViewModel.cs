﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Caliburn.Micro;
using Contract.Domain;
using Contract.Events;
using Contract.Exception;
using Contract.Services;
using ZTech.Controls;
using ZTech.Helpers;
using ZTech.Reports;
using ZTech.ViewModels.ViewObject;
using Action = System.Action;
using Syncfusion.Windows.Tools.Controls;

namespace ZTech.ViewModels.Abstract
{
    public abstract class BaseTruckViewModel : BaseViewModel, IHandle<TruckOwnerModifiedArgs>, IHandle<ScaleResultsArgs>, IHandle<ScaleExceptionsArgs>, IHandle<ScaleChangedArgs>
    {
        protected readonly IEventAggregator _eventAggregator;
        protected readonly int _seasonId;
        private string _errors;
        private bool _isBusy;
        private bool _isScaleEnabled;
        protected ILog _log;
        private ScaleWidgetViewModel _scaleWidget;
        private ITruckOwnerInfo _selectedTruckOwner;
        private TruckDataModel _truck;
        private string _truckOwnerFilter;
        private List<ITruckOwnerInfo> _truckOwners;
        private bool _isManual;
        private readonly IScaleManager _scaleManager;
        private bool _isScalingStarted;
        private string _weightButtonText;
        private AutoComplete _truckOwnersAutoComplete;

        public BaseTruckViewModel(IEventAggregator eventAggregator, string displayName)
        {
            _eventAggregator = eventAggregator;
            _log = LogManager.GetLog(typeof (BaseTruckViewModel));
            _seasonId = (int) Application.Current.Properties["SeasonId"];
            _scaleManager = IoC.Get<IScaleManager>();
            DisplayName = displayName;
        }

        #region Properties

        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                if (value.Equals(_isBusy)) return;
                _isBusy = value;
                NotifyOfPropertyChange(() => IsBusy);
            }
        }

        public TruckDataModel Truck
        {
            get { return _truck; }
            set
            {
                if (Equals(value, _truck)) return;
                _truck = value;
                NotifyOfPropertyChange(() => Truck);
            }
        }

        public List<ITruckOwnerInfo> TruckOwners
        {
            get { return _truckOwners; }
            set
            {
                if (Equals(value, _truckOwners)) return;
                _truckOwners = value;
                NotifyOfPropertyChange(() => TruckOwners);
            }
        }
        public bool IsManual
        {
            get { return _isManual; }
            set
            {
                if (value.Equals(_isManual)) return;
                _isManual = value;
                NotifyOfPropertyChange(() => IsManual);
            }
        }

        public ITruckOwnerInfo SelectedTruckOwner
        {
            get { return _selectedTruckOwner; }
            set
            {
                if (Equals(value, _selectedTruckOwner)) return;
                _selectedTruckOwner = value;
                NotifyOfPropertyChange(() => SelectedTruckOwner);
            }
        }

        public string TruckOwnerFilter
        {
            get { return _truckOwnerFilter; }
            set
            {
                if (value == _truckOwnerFilter) return;
                _truckOwnerFilter = value;
                NotifyOfPropertyChange(() => TruckOwnerFilter);
            }
        }

        public string Errors
        {
            get { return _errors; }
            set
            {
                if (value == _errors) return;
                _errors = value;
                NotifyOfPropertyChange(() => Errors);
            }
        }

        public bool IsScaleEnabled
        {
            get { return _isScaleEnabled; }
            set
            {
                if (value.Equals(_isScaleEnabled)) return;
                _isScaleEnabled = value;
                NotifyOfPropertyChange(() => IsScaleEnabled);
            }
        }

        public string WeightButtonText
        {
            get { return _weightButtonText; }
            set
            {
                if (value.Equals(_weightButtonText)) return;
                _weightButtonText = value;
                NotifyOfPropertyChange(() => WeightButtonText);
            }
        }


        //public AutoComplete TruckOwnersAutoComplete
        //{
        //    get { return _truckOwnersAutoComplete; }
        //    set
        //    {
        //        if (value.Equals(_truckOwnersAutoComplete)) return;
        //        _truckOwnersAutoComplete = value;
        //        NotifyOfPropertyChange(() => TruckOwnersAutoComplete);
        //    }
        //}

        #endregion

        protected override void OnInitialize()
        {
            
            try
            {
                base.OnInitialize();
                InitializeScale();
                InitializeTruckOwners();
                InitializeTruck();
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(
                            string.Format(
                                "Initialization failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                                ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));

                    TryClose(false);
                };

                action.OnUIThreadAsync();
            }
            finally
            {
                IsBusy = false;
            }

        }

        #region Scale

        public ScaleWidgetViewModel ScaleWidget
        {
            get { return _scaleWidget; }
            set
            {
                if (Equals(value, _scaleWidget)) return;
                _scaleWidget = value;
                NotifyOfPropertyChange(() => ScaleWidget);
            }
        }

        private void InitializeScale()
        {
            ScaleWidget = IoC.Get<ScaleWidgetViewModel>();
            ScaleWidget.ConductWith(this);

            IsManual = false;

            if (IoC.Get<IConfiguration>().IsManualMode)
            {
                IsScaleEnabled = false;
                IsManual = true;
                ManualScalingForShippment();
            }
            else
            {
                IsScaleEnabled = true;
                ScaleWidget.Activated += ScaleWidgetActivated;
            }
        }

        private void ManualScalingForShippment()
        {
            if (ScaleWidget != null)
                ScaleWidget.Errors = null;

            if (_scaleManager != null)
                _scaleManager.StopScaling();
        }

        private void StartScaleInteraction()
        {
            try
            {
                if (ScaleWidget.SelectedScale != null)
                {
                    SelectedScale = ScaleWidget.SelectedScale;
                    _scaleManager.StartScaling(SelectedScale);
                }
            }
            catch (Exception ex)
            {
                ManualScalingForShippment();
                Errors = $"Start Scale Integration failed with exception: {ex.Message}";
                ShowMessage("Scale Error", Errors, NotificationType.Warning);
                _log.Error(ex);
            }
        }

        public void Handle(ScaleResultsArgs args)
        {
            switch (args.Result.StreamPolarity)
            {
                case ScaleIndicatorStreamPolarity.Negative:
                    //UI label to have text "Negative"
                    ScaleWidget.Errors = args.Result.StreamPolarity.GetDescription();
                    break;

                case ScaleIndicatorStreamPolarity.Overload:
                    //UI label to have text "Overload"
                    ScaleWidget.Errors = args.Result.StreamPolarity.GetDescription();
                    break;

                case ScaleIndicatorStreamPolarity.Underrange:
                    //UI label to have text "Underrange"
                    ScaleWidget.Errors = args.Result.StreamPolarity.GetDescription();
                    break;
                case ScaleIndicatorStreamPolarity.Positive:
                    Truck.Weight =
                        args.Result.StreamWeight.HasValue
                            ? args.Result.StreamWeight.Value.GetWeightInPounds(args.Result.StreamUnitOfWeight)
                            : 0;
                    ScaleWidget.Errors = null;
                    break;
                case ScaleIndicatorStreamPolarity.Undefined:
                default:
                    ScaleWidget.Errors = ScaleIndicatorStreamPolarity.Undefined.GetDescription();
                    //ToDo: Log
                    break;
            }
        }
        
        private void ScaleWidgetActivated(object sender, ActivationEventArgs args)
        {
            if (args.WasInitialized)
            {
                if (ScaleWidget.SelectedScale != null
                    && ScaleWidget.SelectedScale.IsScaleEnabled)
                {
                    IsScaleEnabled = true;
                    SelectedScale = ScaleWidget.SelectedScale;
                    return;
                }
            }
            if (ScaleWidget != null)
            {
                ScaleWidget.Errors = null;
                IsScaleEnabled = false;
            }
        }
        

        public IScaleInfo SelectedScale { get; set; }

        #endregion

        protected abstract void InitializeTruck();

        private void InitializeTruckOwners()
        {
            var emptyOwnerInstance = new TruckOwnerModel();
            List<ITruckOwnerInfo> result = IoC.Get<ITruckService>().GetFilteredTruckOwners(TruckOwnerFilter, _seasonId);

            TruckOwners = result;
        }

        public void SearchTruckOwner()
        {
            InitializeTruckOwners();
            SelectedTruckOwner = null;
        }

        public void ExecuteOwnerSearch(Key key)
        {
            if (key == Key.Enter)
            {
                SearchTruckOwner();
            }
        }

        public void ExecuteEditOwner(Key key)
        {
            if (key == Key.Enter)
            {
                EditOwner();
            }
        }

        public void KeyHandle(Key key)
        {
            if (key == Key.F2)
            {
                Done();
            }
        }

        public void Cancel()
        {
            _scaleManager.StopScaling();
            _eventAggregator.PublishOnCurrentThread(new TruckModifiedArgs(Truck.Id));
            TryClose(false);
        }

        private bool Validate()
        {
            Errors = null;

            ICollection<ValidationResult> validationResults = new Collection<ValidationResult>();
            bool result = Validator.TryValidateObject(Truck, new ValidationContext(Truck), validationResults);
            if (Truck.Weight <= 0)
            {
                validationResults.Add(new ValidationResult("Weight should be > 0"));
            }

            if (Truck.NeedsTareOverride && (!Truck.TareInterval.HasValue || Truck.TareInterval <= 0))
            {
                validationResults.Add(new ValidationResult("Tare interval should be > 0"));
            }

            if (!result || validationResults.Any())
            {
                Errors = string.Format(
                    "Validation Failed.{0}{1}", Environment.NewLine,
                    string.Join("\n", validationResults.Select(v => v.ErrorMessage)));

                ShowMessage("Validation Errors", Errors, NotificationType.Warning);

                return false;
            }

            return true;
        }

        public void ChangeWeight()
        {
            try
            {
                if (ScaleWidget.SelectedScale == null || IsManual) return;

                if (!_isScalingStarted)
                {
                    _scaleManager.StartScaling(SelectedScale);
                    _isScalingStarted = true;
                    WeightButtonText = "Stop Weight";
                }
                else
                {
                    _scaleManager.StopScaling();
                    _isScalingStarted = false;
                    WeightButtonText = "Start Weight";
                }

            }
            catch (Exception ex)
            {
                IsManual = true;
                IsScaleEnabled = false;
                
                if (ScaleWidget != null)
                    ScaleWidget.Errors = null;
                
                Errors = string.Format("Scale unavailable this time with exception: '{0}'. Put weight manually or contact HelpDesk.", ex.GetAllMessages());
                ShowMessage("Scale Errors", Errors, NotificationType.Warning);
            }
        }

        public void NewOwner()
        {
            var windowManager = IoC.Get<IWindowManager>();
            var ownerViewModel = IoC.Get<NewTruckOwnerViewModel>();
            windowManager.ShowDialog(ownerViewModel);
        }

        public void EditOwner()
        {
            if (SelectedTruckOwner == null || !SelectedTruckOwner.Id.HasValue) return;

            var windowManager = IoC.Get<IWindowManager>();
            var ownerViewModel = IoC.Get<EditTruckOwnerViewModel>();
            ownerViewModel.OwnerId = SelectedTruckOwner.Id.Value;
            windowManager.ShowDialog(ownerViewModel);
        }

       public void Done()
        {
            try
            {

                //if(_scaleManager != null && _isScalingStarted)
                //    _scaleManager.StopScaling();

                if (SelectedTruckOwner != null && Truck.TruckOwnerId.HasValue && Truck.TruckOwnerId != SelectedTruckOwner.Id)
                {
                    var currentOwner = TruckOwners.Single(t => t.Id == Truck.TruckOwnerId.Value);
                    var message = $"Are you sure you want to change Truck Ownership from '{currentOwner.ShortName}' to '{SelectedTruckOwner.ShortName}'?";
                    var msgbox = MessageBox.Show(message, "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);

                    if (msgbox == MessageBoxResult.No)
                    {
                        return;
                    }
                }

                if (!Validate())
                {
                    //if(_scaleManager !=null && _isScalingStarted)
                    //    _scaleManager.StartScaling(SelectedScale);

                    return;
                }

                // 2018-11-03 - JGS - Allow the truck owner to be set to null with addition of ELSE clause below
                if (SelectedTruckOwner != null)
                    Truck.TruckOwnerId = SelectedTruckOwner.Id;
                else
                    Truck.TruckOwnerId = null; 

                if (!Truck.DateTimeOfWeight.HasValue)
                    Truck.DateTimeOfWeight = DateTime.Now;

                Truck.UpdateTime = DateTime.Now;

                IoC.Get<ITruckService>().SaveTruck(Truck);
                _eventAggregator.PublishOnCurrentThread(new TruckModifiedArgs(Truck.Id));

                _scaleManager.StopScaling();

                TryClose(true);
            }
            catch (UniqueConstraintException ex)
            {
                _log.Error(ex);

                Errors = string.Format("Truck saving failed. Data not unique. {0}", ex.Message);
                ShowMessage("Validation Errors", Errors, NotificationType.Warning);
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Errors = string.Format(
                    "Truck saving failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                    ex.Message);
                ShowMessage("Validation Errors", Errors, NotificationType.Error);
            }
        }



        #region Events

        public void Handle(TruckOwnerModifiedArgs args)
        {
            if (args != null)
            {
                InitializeTruckOwners();

                if (args.ItemId.HasValue)
                {
                    var owner = TruckOwners.FirstOrDefault(o => o.Id == args.ItemId);
                    if (owner != null)
                    {
                        SelectedTruckOwner = owner;
                        return;
                    }
                }

                if (Truck.TruckOwnerId.HasValue)
                {
                    var owner = TruckOwners.FirstOrDefault(o => o.Id == Truck.TruckOwnerId.Value);
                    if (owner != null)
                    {
                        SelectedTruckOwner = owner;
                        return;
                    }
                }

                SelectedTruckOwner = TruckOwners.FirstOrDefault();
            }
        }

        protected override void OnActivate()
        {
            _eventAggregator.Subscribe(this);
            base.OnActivate();
        }

        protected override void OnDeactivate(bool close)
        {
            ////_scaleManager.StopScaling();
            _eventAggregator.Unsubscribe(this);
            base.OnDeactivate(close);
        }

        #endregion

        public void Handle(ScaleExceptionsArgs message)
        {
            /*
            IsManual = true;
            IsScaleEnabled = false;
            
            if (ScaleWidget != null)
                ScaleWidget.Errors = null;

            Errors = string.Format("Scale unavailable this time with exception: '{0}'. Put weight manually or contact HelpDesk.", message.Message);
            ShowMessage("Scale Errors", Errors, NotificationType.Warning);
            */
            _log.Warn($"Scale Handler: {message.Message}  Scale Id: {message.ScaleId}");
        }

        
        public void Handle(ScaleChangedArgs args)
        {
            if (args != null)
            {
                
                if (_scaleManager != null && !IsManual && _isScalingStarted)
                {
                    SelectedScale = args.Scale;

                    _scaleManager.StopScaling();
                    _scaleManager.StartScaling(SelectedScale);
                }
            }
        }
    }
}