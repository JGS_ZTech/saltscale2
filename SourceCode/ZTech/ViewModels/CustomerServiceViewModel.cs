using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Caliburn.Micro;
using Contract.Domain;
using Contract.Events;
using Contract.Services;
using Action = System.Action;

namespace ZTech.ViewModels
{
    public class CustomerServiceViewModel : Screen, IHandle<CustomerModifiedArgs>, IHandle<PurchaseOrderModifiedArgs>, IHandle<OrderModifiedArgs>
    {
        //TODO Event to Phone (screen is not created)

        private readonly IEventAggregator _eventAggregator;
        private readonly ILog _log;
        private IObservableCollection<CustomerLine> _customerList;
        private List<ICustomerLine> _customerAddressList;
        private CustomerLine _selectedCustomer;
        private string _searchCustomerWord;
        private bool _entityCustomersIsChecked;
        private bool _shippingCustomersIsChecked;
        private bool _allCustomersIsChecked;
        private static int _seasonId;
        private IObservableCollection<IOrderSummary> _orderList;
        private IOrderSummary _selectedOrder;
        private string _searchOrderWord;
        private bool _isFromOrderGrid;
        private IObservableCollection<IPurchaseOrder> _purchaseOrderList;
        private IPurchaseOrder _selectedPurchaseOrder;
        private string _searchPurchaseOrderWord;
        private CustomerItem customer;
        private bool _isNewOrderNewPurchaseOrderButtonsEnabled;

        // Orders Filter control
        private bool _showOpenOrders = false;
        private bool _showCompletedOrders = false;
        private bool _showClosedOrders = false;
        private bool _showDeletedOrders = false;
        private bool _showAllOrders = false;

        private bool _showOpenPurchaseOrders = false;
        private bool _showCompletePurchaseOrders = false;
        private bool _showAllPurchaseOrders = false;

        public CustomerServiceViewModel(IEventAggregator eventAggregator)
        {
            DisplayName = "Customer Service";
            _eventAggregator = eventAggregator;
            _log = LogManager.GetLog(typeof (CustomerServiceViewModel));
            _seasonId = (int) Application.Current.Properties["SeasonId"];
        }

        protected override void OnInitialize()
        {
            base.OnInitialize();
            ShippingCustomersIsChecked = true;
            InitializeCustomerGrid();

            // Set Order display control and initialize the grid
            ShowOpenOrders = true;
            InitializeOrderGrid();
            ShowOpenPurchaseOrders = true;
            InitializePurchaseOrderGrid();
        }

        private void InitSelectedCustomer()
        {
            if (SelectedCustomer == null) return;
                Customer = new CustomerItem(IoC.Get<ICustomerService>().GetCustomerInfo(SelectedCustomer.CustomerId));
        }

        public CustomerItem Customer
        {
            get { return customer; }
            set
            {
                if (Equals(value, customer)) return;
                customer = value;
                NotifyOfPropertyChange(() => Customer);
            }
        }

        #region Customer Grid

        public IObservableCollection<CustomerLine> CustomerList
        {
            get { return _customerList; }
            set
            {
                if (Equals(value, _customerList)) return;
                _customerList = value;
                NotifyOfPropertyChange(() => CustomerList);
            }
        }

        public CustomerLine SelectedCustomer
        {
            get { return _selectedCustomer; }
            set
            {
                _selectedCustomer = value;
                NotifyOfPropertyChange(() => SelectedCustomer);
                InitializePurchaseOrderGrid();
                InitSelectedCustomer();
                InitializeNewOrderNewPurchaseOrderButtons();
                if (!_isFromOrderGrid)
                {
                    InitializeOrderGrid();
                }
            }
        }

        public string SearchCustomerWord
        {
            get { return _searchCustomerWord; }
            set
            {
                _searchCustomerWord = value;
                NotifyOfPropertyChange(() => SearchCustomerWord);
            }
        }

        public bool AllCustomersIsChecked
        {
            get { return _allCustomersIsChecked; }
            set
            {
                _allCustomersIsChecked = value;
                NotifyOfPropertyChange(() => AllCustomersIsChecked);
            }

        }

        public bool ShippingCustomersIsChecked
        {
            get { return _shippingCustomersIsChecked; }
            set
            {
                _shippingCustomersIsChecked = value;
                NotifyOfPropertyChange(() => ShippingCustomersIsChecked);
            }

        }

        public bool EntityCustomersIsChecked
        {
            get { return _entityCustomersIsChecked; }
            set
            {
                _entityCustomersIsChecked = value;
                NotifyOfPropertyChange(() => EntityCustomersIsChecked);
            }
        }


        private void InitializeCustomerGrid()
        {
            _customerAddressList = String.IsNullOrEmpty(SearchCustomerWord)
                ? IoC.Get<ICustomerService>().GetCustomerList(ShippingCustomersIsChecked, EntityCustomersIsChecked, _seasonId)
                : IoC.Get<ICustomerService>().GetFilteredCustomers(SearchCustomerWord, ShippingCustomersIsChecked, EntityCustomersIsChecked, _seasonId);

            CustomerList = new BindableCollection<CustomerLine>(_customerAddressList.Select(item => new CustomerLine(item)));
            CustomerLine customer = CustomerList.FirstOrDefault();
            if (customer != null)
                SelectedCustomer = customer;
        }

        public void SearchCustomer()
        {
            if (string.IsNullOrWhiteSpace(SearchCustomerWord))
            {
                InitializeCustomerGrid();
                return;
            }

            var customers = IoC.Get<ICustomerService>()
                .GetFilteredCustomers(SearchCustomerWord, ShippingCustomersIsChecked, EntityCustomersIsChecked, _seasonId);

            CustomerList = new BindableCollection<CustomerLine>(customers.Select(item => new CustomerLine(item)));
            CustomerLine customer = CustomerList.FirstOrDefault();
            if (customer != null)
                SelectedCustomer = customer;
        }

        public void RefreshCustomer()
        {
            SearchCustomerWord = String.Empty;
            InitializeCustomerGrid();
            InitializePurchaseOrderGrid();
        }

        public void ShippingCustomers()
        {
            InitializeCustomerGrid();
            InitializePurchaseOrderGrid();
        }

        public void AllCustomers()
        {
            InitializeCustomerGrid();
            InitializePurchaseOrderGrid();
        }

        public void EntityCustomers()
        {
            InitializeCustomerGrid();
            InitializePurchaseOrderGrid();
        }

        public void ExecuteCustomerSearch(Key key)
        {
            if (key == Key.Enter)
            {
                SearchCustomer();
            }
        }

        public class CustomerLine
        {
            public CustomerLine()
            {
            }

            public CustomerLine(ICustomerLine customerLine)
            {
                CustomerId = customerLine.Id;
                ShortName = customerLine.ShortName;
                Name = customerLine.Name;
                EntityName = customerLine.EntityName;
                AddressLine1 = customerLine.AddressLine1;
                City = customerLine.City;
                State = customerLine.State;
                Company = customerLine.Company;

                //TODO Rework for multiple materials
                //CashMaterialRate = customerLine.CashMaterialRate;
                //TruckingRate = customerLine.TruckingRate;
                //Toll = customerLine.Toll;
                CashMaterialRate = 0;
                TruckingRate = 0;
                Toll = 0;
            }

            public int CustomerId { get; set; }
            public string ShortName { get; set; }
            public string Name { get; set; }
            public string AddressLine1 { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            int? ReportingEntityId { get; set; }
            public string EntityName { get; set; }
            public int SeasonId { get; set; }
            public string SeasonName { get; set; }
            public decimal? CashMaterialRate{ get; set; }
            public decimal? TruckingRate{ get; set; }
            public decimal? Toll{ get; set; }
            public ICompanyInfo Company { get; set; }
        }

        public class CustomerItem
        {
            public CustomerItem()
            {
            }

            public CustomerItem(ICustomerInfo customer)
            {
                CustomerId = customer.Id;
                ShortName = customer.ShortName;
                Name = customer.Name;
                Address = customer.Address;
                FullAddress = customer.FullAddress;
                SpecialInstruction = customer.SpecialInstruction;
                PrimaryContactOrganization = customer.OrganizationName;
                PrimaryContactInfo = customer.ContactInfo;
                EntityAddress = String.Format($"{PrimaryContactOrganization}{Environment.NewLine}{FullAddress}");
                Phone = customer.Phone;
            }

            public string Phone { get; set; }

            public int CustomerId { get; set; }
            public string ShortName { get; set; }
            public string Name { get; set; }
            public string Address { get; set; }
            public string SpecialInstruction { get; set; }
            public string PrimaryContactOrganization { get; set; }
            public string PrimaryContactInfo { get; set; }
            public string EntityAddress { get; set; }
            public object FullAddress { get; set; }
        }

        #endregion

        #region Order Grid

        public IObservableCollection<IOrderSummary> OrderList
        {
            get { return _orderList; }
            set
            {
                _orderList = value;
                NotifyOfPropertyChange(() => OrderList);
            }
        }

        public IOrderSummary SelectedOrder
        {
            get { return _selectedOrder; }
            set
            {
                _selectedOrder = value;
                NotifyOfPropertyChange(() => SelectedOrder);
                SelectCustomer();
            }
        }

        private void SelectCustomer()
        {
            if (SelectedOrder != null 
                && SelectedCustomer != null 
                && SelectedOrder.CustomerId != SelectedCustomer.CustomerId
                )
            {
                _isFromOrderGrid = true;
                SelectedCustomer = _customerList.FirstOrDefault(x => x.CustomerId == SelectedOrder.CustomerId);
                _isFromOrderGrid = false;
            }

            if (SelectedCustomer?.Company.Id == 0)
            {
                _log.Info("Customer " + SelectedCustomer.Name + " is associated to company '" + SelectedCustomer.Company.Name + "'.  No order can be created for it.");
                Action action =
                    () =>
                    {
                        MessageBox.Show("This customer is associated with company '" + SelectedCustomer.Company.Name + "' which means no orders can be placed for it", "Company 'Unassigned'", MessageBoxButton.OK, MessageBoxImage.None);
                    };
                action.OnUIThreadAsync();
            }

        }

        public Boolean IsNewOrderNewPurchaseOrderButtonsEnabled
        {
            get { return _isNewOrderNewPurchaseOrderButtonsEnabled; }
            set
            {
                if (Equals(value, _isNewOrderNewPurchaseOrderButtonsEnabled)) return;
                _isNewOrderNewPurchaseOrderButtonsEnabled = value;
                NotifyOfPropertyChange(() => IsNewOrderNewPurchaseOrderButtonsEnabled);
            }
        }

        public string SearchOrderWord
        {
            get { return _searchOrderWord; }
            set
            {
                _searchOrderWord = value;
                NotifyOfPropertyChange(() => SearchOrderWord);
            }
        }

        private void InitializeOrderGrid()
        {
            //// Set the default objects and order list
            //var orders = new List<IOrderSummary>();
            //if (SelectedCustomer != null)
            //    //orders = IoC.Get<IOrderService>().GetOrders(SelectedCustomer.CustomerId, _seasonId);
            //    orders = IoC.Get<IOrderService>().GetFilteredOrders(SearchOrderWord, SelectedCustomer.CustomerId, _seasonId, orderStatus);

            //OrderList = new BindableCollection<IOrderSummary>(orders);
            //var order = OrderList.FirstOrDefault();
            //if (order != null)
            //    SelectedOrder = order;

            // Control values
            string SearchOrderWord = string.Empty;
            string orderStatus = "open";
            if (ShowAllOrders) orderStatus = "all";
            if (ShowOpenOrders) orderStatus = "open";
            if (ShowCompletedOrders) orderStatus = "completed";
            if (ShowClosedOrders) orderStatus = "closed";
            if (ShowDeletedOrders) orderStatus = "deleted";

            // Set the default objects and order list
            var orders = new List<IOrderSummary>();
            if (SelectedCustomer != null)
                //orders = IoC.Get<IOrderService>().GetOrders(SelectedCustomer.CustomerId, _seasonId);
                orders = IoC.Get<IOrderService>().GetFilteredOrders(SearchOrderWord, SelectedCustomer.CustomerId, _seasonId, orderStatus);

            // Generate the order list
            OrderList = new BindableCollection<IOrderSummary>(orders);
            var order = OrderList.FirstOrDefault();
            if (order != null)
                SelectedOrder = order;

            // Assign the selected customer
            SelectCustomer();
        }

        public void SearchOrder()
        {
            if (string.IsNullOrWhiteSpace(SearchOrderWord) || SelectedCustomer == null)
            {
                InitializeOrderGrid();
                return;
            }

            List<IOrderSummary> orders = IoC.Get<IOrderService>()
                .GetFilteredOrders(SearchOrderWord, SelectedCustomer.CustomerId, _seasonId);
            OrderList = new BindableCollection<IOrderSummary>(orders);

            var orderSummary = OrderList.FirstOrDefault();
            if (orderSummary != null)
                SelectedOrder = orderSummary;

            SelectCustomer();
        }

        public bool ShowOpenOrders
        {
            get { return _showOpenOrders; }
            set
            {
                if (value == _showOpenOrders) return;
                _showOpenOrders = value;
                NotifyOfPropertyChange(() => ShowOpenOrders);

                // Reset the grid
                InitializeOrderGrid();
            }
        }

        public bool ShowCompletedOrders
        {
            get { return _showCompletedOrders; }
            set
            {
                if (value == _showCompletedOrders) return;
                _showCompletedOrders = value;
                NotifyOfPropertyChange(() => ShowCompletedOrders);

                // Reset the grid
                InitializeOrderGrid();
            }
        }

        public bool ShowClosedOrders
        {
            get { return _showClosedOrders; }
            set
            {
                if (value == _showClosedOrders) return;
                _showClosedOrders = value;
                NotifyOfPropertyChange(() => ShowClosedOrders);

                // Reset the grid
                InitializeOrderGrid();
            }
        }

        public bool ShowDeletedOrders
        {
            get { return _showDeletedOrders; }
            set
            {
                if (value == _showDeletedOrders) return;
                _showDeletedOrders = value;
                NotifyOfPropertyChange(() => ShowDeletedOrders);

                // Reset the grid
                InitializeOrderGrid();
            }
        }

        public bool ShowAllOrders
        {
            get { return _showAllOrders; }
            set
            {
                if (value == _showAllOrders) return;
                _showAllOrders = value;
                NotifyOfPropertyChange(() => ShowAllOrders);

                // Reset the grid
                InitializeOrderGrid();
            }
        }

        public bool ShowOpenPurchaseOrders
        {
            get { return _showOpenPurchaseOrders; }
            set
            {
                if (value == _showOpenPurchaseOrders) return;
                _showOpenPurchaseOrders = value;
                NotifyOfPropertyChange(() => ShowOpenPurchaseOrders);

                // Reset the grid
                InitializePurchaseOrderGrid();
            }
        }

        public bool ShowCompletePurchaseOrders
        {
            get { return _showCompletePurchaseOrders; }
            set
            {
                if (value == _showCompletePurchaseOrders) return;
                _showCompletePurchaseOrders = value;
                NotifyOfPropertyChange(() => ShowCompletePurchaseOrders);

                // Reset the grid
                InitializePurchaseOrderGrid();
            }
        }

        public bool ShowAllPurchaseOrders
        {
            get { return _showAllPurchaseOrders; }
            set
            {
                if (value == _showAllPurchaseOrders) return;
                _showAllPurchaseOrders = value;
                NotifyOfPropertyChange(() => ShowAllPurchaseOrders);

                // Reset the grid
                InitializePurchaseOrderGrid();
            }
        }

        public void RefreshOrder()
        {
            SearchOrderWord = string.Empty;
            InitializeOrderGrid();
        }

        public void ExecuteOrderSearch(Key key)
        {
            if (key == Key.Enter)
            {
                SearchOrder();
            }
        }

        #endregion

        #region Purchase Orders Grid

        public IObservableCollection<IPurchaseOrder> PurchaseOrderList
        {
            get { return _purchaseOrderList; }
            set
            {
                _purchaseOrderList = value;
                NotifyOfPropertyChange(() => PurchaseOrderList);
            }
        }

        public IPurchaseOrder SelectedPurchaseOrder
        {
            get { return _selectedPurchaseOrder; }
            set
            {
                _selectedPurchaseOrder = value;
                NotifyOfPropertyChange(() => SelectedPurchaseOrder);
            }
        }

        public string SearchPurchaseOrderWord
        {
            get { return _searchPurchaseOrderWord; }
            set
            {
                _searchPurchaseOrderWord = value;
                NotifyOfPropertyChange(() => SearchPurchaseOrderWord);
            }
        }

        private void InitializePurchaseOrderGrid()
        {
            // Control values
            string SearchOrderWord = string.Empty;
            string poStatus = "open";
            if (ShowAllPurchaseOrders) poStatus = "all";
            if (ShowOpenPurchaseOrders) poStatus = "open";
            if (ShowCompletePurchaseOrders) poStatus = "completed";

            var puchaseOrders = new List<IPurchaseOrder>();
            if (SelectedCustomer != null)
                //puchaseOrders = IoC.Get<IPurchaseOrderService>().GetPurchaseOrderList(SelectedCustomer.CustomerId, _seasonId);
                puchaseOrders = IoC.Get<IPurchaseOrderService>().GetFilteredPurchaseOrders(SearchOrderWord, SelectedCustomer.CustomerId, _seasonId, poStatus);

            PurchaseOrderList = new BindableCollection<IPurchaseOrder>(puchaseOrders);
            var purchaseOrder = PurchaseOrderList.FirstOrDefault();
            if (purchaseOrder != null)
                SelectedPurchaseOrder = purchaseOrder;
        }



        private void InitializeNewOrderNewPurchaseOrderButtons()
        {
            IsNewOrderNewPurchaseOrderButtonsEnabled = (SelectedCustomer != null && SelectedCustomer.Company.Id != 0) ? true : false;
        }

        public void SearchPurchaseOrder()
        {
            if (string.IsNullOrWhiteSpace(SearchPurchaseOrderWord) || SelectedCustomer == null)
            {
                InitializePurchaseOrderGrid();
                return;
            }

            List<IPurchaseOrder> purchaseOrders =
                IoC.Get<IPurchaseOrderService>()
                    .GetFilteredPurchaseOrders(SearchPurchaseOrderWord, SelectedCustomer.CustomerId, _seasonId);

            PurchaseOrderList = new BindableCollection<IPurchaseOrder>(purchaseOrders);
            var purchaseOrder = PurchaseOrderList.FirstOrDefault();
            if (purchaseOrder != null)
                SelectedPurchaseOrder = purchaseOrder;
        }

        public void RefreshPurchaseOrder()
        {
            SearchPurchaseOrderWord = string.Empty;
            InitializePurchaseOrderGrid();
        }

        public void ExecutePurchaseOrderSearch(Key key)
        {
            if (key == Key.Enter)
            {
                SearchPurchaseOrder();
            }
        }

        #endregion

        #region Actions

        public void NewCustomer()
        {
            var newCustomerViewModel = IoC.Get<CustomerViewModel>();
            newCustomerViewModel.IsNew = true;

            var windowManager = IoC.Get<IWindowManager>();
            windowManager.ShowDialog(newCustomerViewModel);
        }

        public void EditCustomer()
        {
            if (SelectedCustomer == null) return;

            if (CustomerList == null || CustomerList.Count == 0) return;

            var newCustomerViewModel = IoC.Get<CustomerViewModel>();
            newCustomerViewModel.CustomerId = SelectedCustomer.CustomerId;

            var windowManager = IoC.Get<IWindowManager>();
            windowManager.ShowDialog(newCustomerViewModel);

            // Reset order and PO displays
            ShowOpenOrders = true;
            ShowOpenPurchaseOrders = true;
        }

        public void NewOrder()
        {
            if (SelectedCustomer == null) return;

            var newOrderViewModel = IoC.Get<NewOrderViewModel>();
            newOrderViewModel.CustomerId = SelectedCustomer.CustomerId;

            var windowManager = IoC.Get<IWindowManager>();
            windowManager.ShowDialog(newOrderViewModel);
        }

        public void ExecuteOrderEdit(Key key)
        {
            if (key == Key.Enter)
            {
                EditOrder();
            }
        }

        public void EditOrder()
        {
            // Do note process an order if one has not been selected
            if (SelectedCustomer == null || SelectedOrder == null) return;
            if (OrderList == null || OrderList.Count == 0) return;

            // Get the current order and POOverride
            int localOrderID = SelectedOrder.OrderId;
            int localCustomerId = SelectedCustomer.CustomerId;

            // Create a local order object and statuses
            var _localOrder = IoC.Get<IOrderService>().GetOrder(_seasonId, localOrderID);
            string _localOrderNumber = _localOrder.OrderNumber.ToString();
            bool _localOrderIsActive = _localOrder.IsActive;
            bool _localOrderIsVoid = _localOrder.IsVoid;

            // Do not process deleted orders
            if (_localOrderIsVoid)
            {
                var msgbox = MessageBox.Show(String.Format("Order '{0}' has been voided and cannot be edited. Contact the SaltScale Administrator for more details.", _localOrderNumber), 
                            "Cannot Edit Voided Order", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            // Do not process closed orders
            if (!_localOrderIsActive)
            {
                var msgbox = MessageBox.Show(String.Format("Order '{0}' has been closed and cannot be edited. Use the 'Reopen' button to reset its status.", _localOrderNumber),
                            "Cannot Edit Closed Order", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            var windowManager = IoC.Get<IWindowManager>();
            var editOrderViewModel = IoC.Get<EditOrderViewModel>();
            editOrderViewModel.OrderId = SelectedOrder.OrderId;
            var result = windowManager.ShowDialog(editOrderViewModel);
        }

        public void CustomerRowSelect()
        {
            TicketsCustomer();
        }
        public void TicketsCustomer()
        {
            if (SelectedCustomer == null) return;

            var newTicketViewerViewModel = IoC.Get<TicketViewerViewModel>();
            newTicketViewerViewModel.CustomerShortName = SelectedCustomer.ShortName;
            newTicketViewerViewModel.CustomerId = SelectedCustomer.CustomerId;

            var windowManager = IoC.Get<IWindowManager>();
            windowManager.ShowDialog(newTicketViewerViewModel);
        }

        public void NewPurchaseOrder()
        {
            if (SelectedCustomer == null) return;

            var newPurchaseOrderViewModel = IoC.Get<PurchaseOrderViewModel>();
            newPurchaseOrderViewModel.IsNew = true;
            newPurchaseOrderViewModel.CustomerId = SelectedCustomer.CustomerId;

            var windowManager = IoC.Get<IWindowManager>();
            windowManager.ShowDialog(newPurchaseOrderViewModel);
        }

        public void ExecuteEditPo(Key key)
        {
            if (key == Key.Enter)
            {
                EditPurchaseOrder();
            }
        }

        public void EditPurchaseOrder()
        {
            if (SelectedCustomer == null || SelectedPurchaseOrder == null) return;

            if (PurchaseOrderList == null || PurchaseOrderList.Count == 0) return;

            var windowManager = IoC.Get<IWindowManager>();

            var editPurchaseOrderViewModel = IoC.Get<PurchaseOrderViewModel>();

            editPurchaseOrderViewModel.PurchaseOrderId = SelectedPurchaseOrder.Id;
            editPurchaseOrderViewModel.CustomerId = SelectedCustomer.CustomerId;
            windowManager.ShowDialog(editPurchaseOrderViewModel);

            // Refresh the Order and PurchaseOrderLists
            ShowOpenOrders = true;
            InitializeOrderGrid();
            ShowOpenPurchaseOrders = true;
            InitializePurchaseOrderGrid();
        }

        public void ReopenOrder()
        {
            // Maker sure an order is sewlected
            if (SelectedCustomer == null) return;

            // Get the current order and POOverride
            int localOrderID = SelectedOrder.OrderId;
            int localCustomerId = SelectedCustomer.CustomerId;

            // Create a local order object
            var localOrder = IoC.Get<IOrderService>().GetOrder(_seasonId, localOrderID);

            // Update order and related PO statuses
            if (localOrder != null)
            {
                // Reset the Order active status
                localOrder.IsActive = true;
                localOrder.IsComplete = false;
                localOrder.IsVoid = false;
                localOrder.CompleteDate = null;
                localOrder.UpdateTime = DateTime.Now;
                IoC.Get<IOrderService>().SaveOrder_Reopen(localOrder);
                _eventAggregator.PublishOnCurrentThread(new OrderModifiedArgs(localOrder.OrderId));

                // Create a local purchase order object
                var localPurchaseOrder = IoC.Get<IPurchaseOrderService>().GetPurchaseOrder(localOrder.POOverride, localCustomerId);

                // Reset the status of the Purchase order, if it is closed
                if (localPurchaseOrder != null)
                {
                    localPurchaseOrder.IsComplete = false;
                    localPurchaseOrder.IsActive = true;
                    localPurchaseOrder.IsHeld = false;
                    localPurchaseOrder.QuantityOfPO = 2 * localPurchaseOrder.QuantityOfPO;
                    localPurchaseOrder.UpdateTime = DateTime.Now;
                    IoC.Get<IPurchaseOrderService>().SavePurchaseOrder(localPurchaseOrder, localCustomerId);
                    _eventAggregator.PublishOnCurrentThread(new PurchaseOrderModifiedArgs(localPurchaseOrder.Id));
                }

                // Refresh the order and puchase order grids
                ShowOpenOrders = true;
                InitializeOrderGrid();
                ShowOpenPurchaseOrders = true;
                InitializePurchaseOrderGrid();
            }
        }

        // RefreshPurchaseOrder
        public void ReopenPurchaseOrder()
        {
            // Maker sure an order and PO are  selected
            if (SelectedCustomer == null || SelectedPurchaseOrder == null) return;
            if (PurchaseOrderList == null || PurchaseOrderList.Count == 0) return;

            int localPurchaseOrderID = SelectedPurchaseOrder.Id;
            int localCustomerID = SelectedCustomer.CustomerId;

            // Create a local purchase order object 
            var localPurchaseOrder = IoC.Get<IPurchaseOrderService>().GetPurchaseOrder(localPurchaseOrderID);

            // Reset the status and double the quantity of the original purchase order
            if (localPurchaseOrder != null)
            {
                //// Get the site multiplier for orders
                //float tempFactor = IoC.Get<IOrderService>().GetSitePOOverLimit(_seasonId);

                localPurchaseOrder.IsComplete = false;
                localPurchaseOrder.IsHeld = false;
                localPurchaseOrder.IsActive = true;
                localPurchaseOrder.QuantityOfPO = 2 * localPurchaseOrder.QuantityOfPO;
                localPurchaseOrder.UpdateTime = DateTime.Now;
                IoC.Get<IPurchaseOrderService>().SavePurchaseOrder(localPurchaseOrder, localCustomerID);
                _eventAggregator.PublishOnCurrentThread(new PurchaseOrderModifiedArgs(localPurchaseOrder.Id));
            }

            // Refresh the Order and PurchaseOrderLists
            ShowOpenOrders = true;
            InitializeOrderGrid();
            ShowOpenPurchaseOrders = true;
            InitializePurchaseOrderGrid();
        }

        public void OrderRowSelect()
        {
            OrderTickets();
        }

        public void OrderTickets()
        {
            if (SelectedOrder == null) return;

            var newTicketViewerViewModel = IoC.Get<TicketViewerViewModel>();
            newTicketViewerViewModel.CustomerShortName = SelectedOrder.CustId;
            newTicketViewerViewModel.CustomerId = SelectedOrder.CustomerId;
            newTicketViewerViewModel.OrderId = SelectedOrder.OrderId;

            var windowManager = IoC.Get<IWindowManager>();
            windowManager.ShowDialog(newTicketViewerViewModel);
        }

        #endregion

        #region Events

        public void Handle(CustomerModifiedArgs args)
        {
            if (args != null)
            {
                InitializeCustomerGrid();

                var selectedCustomer = CustomerList.FirstOrDefault(c => c.CustomerId == args.ItemId);

                if (selectedCustomer != null)
                    SelectedCustomer = selectedCustomer;
            }
        }

        public void Handle(PurchaseOrderModifiedArgs args)
        {
            if (args != null)
            {
                InitializePurchaseOrderGrid();

                var selectedPurchaseOrder = PurchaseOrderList.FirstOrDefault(c => c.Id == args.ItemId);

                if (selectedPurchaseOrder != null)
                    SelectedPurchaseOrder = selectedPurchaseOrder;
            }
        }

        public void Handle(OrderModifiedArgs args)
        {
            if (args != null)
            {
                InitializeOrderGrid();

                IOrderSummary order = OrderList.FirstOrDefault(o => o.OrderId == args.ItemId) ??
                                      OrderList.FirstOrDefault();

                if (order != null)
                    SelectedOrder = order;
            }
        }

        public void ExecuteEditCustomer(Key key)
        {
            if (key == Key.Enter)
            {
                EditCustomer();
            }
        }

        protected override void OnActivate()
        {
            _eventAggregator.Subscribe(this);
            base.OnActivate();
        }

        protected override void OnDeactivate(bool close)
        {
            _eventAggregator.Unsubscribe(this);
            base.OnDeactivate(close);
        }

        #endregion
    }
}