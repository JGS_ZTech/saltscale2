﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.IO;
using System.Windows.Forms.Integration;
using Caliburn.Micro;
using Microsoft.Reporting.WinForms;
using ZTech.Reports;
using ZTech.Helpers;
using Screen = Caliburn.Micro.Screen;
using Action = System.Action;
using System.Windows;

namespace ZTech.ViewModels
{
    public class TicketPreViewModel : Screen
    {
        private readonly IEventAggregator _eventAggregator;
        private readonly ILog _log;
        private WindowsFormsHost _viewer;
        private List<ReportParameter> _parameterList { get; set; }
        
        public bool IsPrintActive { get; set; }
        public TicketTypeEnum ReportType { get; set; }
        public ReportDataSource ReportDataSource { get; set; }

        public TicketPreViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            _log = LogManager.GetLog(typeof(TicketPreViewModel));
            DisplayName = "Reports Preview";
            _parameterList = new List<ReportParameter>();
        }

        protected override void OnInitialize()
        {
            PrinterSettings ps = new PrinterSettings();
            ps.PrinterName = DefaultPrinter;
            ps.DefaultPageSettings.Margins = new Margins(0, 0, 0, 0);
            //ps.DefaultPageSettings.PaperSource = ps.PaperSources[1]; //ToDo will get an out of index error, need to check for if "Tray 2" on those laser printers is present.
            ps.DefaultPageSettings.PaperSize = new PaperSize("Legal", 850, 1400);
            ps.PrintRange = PrintRange.SomePages;
            ps.FromPage = 1;
            ps.ToPage = 1;


            var windowsFormsHost = new WindowsFormsHost();
            var reportViewer = new ReportViewer();
            windowsFormsHost.Child = reportViewer;
            Viewer = windowsFormsHost;

            var assemblyPath = System.Reflection.Assembly.GetExecutingAssembly().Location;

            var reportName = String.Format("{0}{1}", ReportType, ".rdlc");

            var path = Path.Combine(Path.GetDirectoryName(assemblyPath), "Reports", reportName);

            SetTicketSettings();
            reportViewer.LocalReport.ReportPath = path;
            reportViewer.LocalReport.EnableExternalImages = true;

            if (_parameterList.Count > 0)
            {
                reportViewer.LocalReport.SetParameters(_parameterList);
            }

            if (IsPrinterExist())
            {
                reportViewer.PrinterSettings.PrinterName = DefaultPrinter;
            }
            else
            {
                DefaultPrinter = null;
            }
            reportViewer.ShowPrintButton = IsPrintActive;
            reportViewer.RefreshReport();

            base.OnInitialize();
        }

        private bool IsPrinterExist()
        {
            if (string.IsNullOrWhiteSpace(DefaultPrinter))
                return false;

            PrinterSettings.StringCollection installedPrinters = PrinterSettings.InstalledPrinters;

            if (installedPrinters.Count <= 0) return false;
            foreach (object printer in installedPrinters)
            {
                if (printer.ToString().Contains(DefaultPrinter))
                    return true;
            }

            return false;
        }

        public string DefaultPrinter { get; set; }

        public WindowsFormsHost Viewer
        {
            get { return _viewer; }
            set
            {
                if (Equals(value, _viewer)) return;
                _viewer = value;
                NotifyOfPropertyChange(() => Viewer);
            }
        }

        public void AddParameter(string name, string value, bool visible = true)
        {
            _parameterList.Add(new ReportParameter(name, value, visible));
        }

        public void AddParameter(string name, string[] values, bool visible = true)
        {
            _parameterList.Add(new ReportParameter(name, values, visible));
        }

        public void PrintTicket()
        {
            // Ticket: 179943 - Pause for a random number of milliseconds from 0 to 250.
            Random random = new Random();
            int randomNumber = random.Next(0, 250);
            System.Threading.Thread.Sleep(randomNumber);

            OnInitialize();
            

            PrinterSettings ps = new PrinterSettings();
            ps.PrinterName = DefaultPrinter;
            ps.DefaultPageSettings.Margins = new Margins(0, 0, 0, 0);
            if (ps.PaperSources.Count > 0)
            {
                ps.DefaultPageSettings.PaperSource = ps.PaperSources[1];  //ToDo will get an out of index error, need to check for if "Tray 2" on those laser printers is present.
            }
            ps.DefaultPageSettings.PaperSize = new PaperSize("Legal", 850, 1400);
            ps.PrintRange = PrintRange.SomePages;
            ps.FromPage = 1;
            ps.ToPage = 1;

            /*
            for (int i = 0; i < ps.PaperSources.Count; i++)
            {
                Console.WriteLine(ps.PaperSources[i].SourceName);
                //printDocument.Print();
            }
            */

            var reportView = ((ReportViewer)Viewer.Child);

            reportView.PrinterSettings = ps;
            //reportView.PrinterSettings.DefaultPageSettings.Margins = new Margins(0, 0, 0, 0);
            //reportView.PrinterSettings.DefaultPageSettings.PaperSize = new PaperSize("Legal", 850, 1400);
            //reportView.PrinterSettings.DefaultPageSettings.PaperSource.SourceName = "Rear Tray";
            //reportView.PrinterSettings.DefaultPageSettings.PaperSource.SourceName = ps.PaperSources[1].SourceName; // "First Available Tray"


            var autoPrintTicket = new ReportPrintDocument(reportView, reportView.LocalReport);
            autoPrintTicket.PrinterSettings = ps;

            //autoPrintTicket.PrinterSettings.DefaultPageSettings.Margins = new Margins(0, 0, 0, 0);
            //autoPrintTicket.PrinterSettings.DefaultPageSettings.PaperSize = new PaperSize("Legal", 850, 1400);
            //autoPrintTicket.PrinterSettings.DefaultPageSettings.PaperSource.SourceName = ps.PaperSources[1].SourceName; // "First Available Tray"

            if (IsPrinterExist())
            {
                //autoPrintTicket.PrinterSettings.PrinterName = DefaultPrinter;
                //reportView.Print();

                try
                {
                    autoPrintTicket.Print();
                } catch(Exception ex)
               { 
                    _log.Error(ex);
                    Action action = () =>
                    {
                        MessageBoxResult messageBoxResult =
                            (MessageBox.Show(
                                "Error sending print document to windows control: " +
                                ex.Message + Environment.NewLine + ex.StackTrace + Environment.NewLine + ex.InnerException.Message + Environment.NewLine + ex.InnerException.StackTrace, "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                    };

                    action.OnUIThreadAsync();
                }
            }
            else
            {
                throw new Exception("Invalid printer configuration.");
            }
        }

        public void SetTicketSettings()
        {
            var reportView = ((ReportViewer) Viewer.Child);

            reportView.PrinterSettings.PrinterName = DefaultPrinter;

            reportView.PrinterSettings.DefaultPageSettings.Margins = new Margins(0, 0, 0, 0);
//            reportView.PrinterSettings.DefaultPageSettings.PaperSource = reportView.PrinterSettings.PaperSources[1];  //ToDo will get an out of index error, need to check for if "Tray 2" on those laser printers is present.
            reportView.PrinterSettings.DefaultPageSettings.PaperSize = new PaperSize("Legal", 850, 1400);
            reportView.PrinterSettings.PrintRange = PrintRange.SomePages;
            reportView.PrinterSettings.FromPage = 1;
            reportView.PrinterSettings.ToPage = 1;
        }
    }
}