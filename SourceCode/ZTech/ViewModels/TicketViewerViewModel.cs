﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Caliburn.Micro;
using Contract.Domain;
using Contract.Events;
using Contract.Services;
using Action = System.Action;

namespace ZTech.ViewModels
{
    public class TicketViewerViewModel : Screen
    {
        private readonly IEventAggregator _eventAggregator;
        private readonly ILog _log;
        private readonly int _seasonId;
        private string _customerShortName;
        private bool _isVoidEnabled;
        private ITicketInfo _selectedTicket;
        private IReadOnlyList<ITicketInfo> _ticketList;
        private int _ticketsToday;
        private int _totalTickets;
        private float _totalTons;
        private int _ticketsTodayForSelectedOrder;
        private int _totalTicketsForSelectedOrder;
        private float _totalTonsForSelectedOrder;


        private int _ticketsSelectedDayForSelectedOrder;
        private int _totalTicketsForSelectedOrderSelectedDay;
        private float _totalTonsForSelectedOrderSelectedDay;

        private bool _isOrderEnabled;

        private bool _isVisibleCustomerService;
        private bool _isVisibleOrderCreateEditDelete;

        public int? CustomerId { get; set; }

        public int? OrderId { get; set; }

        public string CustomerShortName
        {
            get { return _customerShortName; }
            set
            {
                _customerShortName = value;
            }
        }

        public IReadOnlyList<ITicketInfo> TicketList
        {
            get { return _ticketList; }
            set
            {
                _ticketList = value;
                NotifyOfPropertyChange(() => TicketList);
                
                InitializeTicketCountPerDay();
                InitializeTotal();
            }
        }

        public ITicketInfo SelectedTicket
        {
            get { return _selectedTicket; }
            set
            {
                IsVoidEnabled = value != null;
                IsOrderEnabled = value != null || OrderId.HasValue;
                if (Equals(value, _selectedTicket)) return;
                _selectedTicket = value;
                NotifyOfPropertyChange(() => SelectedTicket);
                InitializeTicketCountPerDay();
            }
        }

        public bool IsOrderEnabled
        {
            get { return _isOrderEnabled; }
            set
            {
                if (value.Equals(_isOrderEnabled)) return;
                _isOrderEnabled = value;
                NotifyOfPropertyChange(() => IsOrderEnabled);
            }
        }

        public bool IsVoidEnabled
        {
            get { return _isVoidEnabled; }
            set
            {
                if (value.Equals(_isVoidEnabled)) return;
                _isVoidEnabled = value;
                NotifyOfPropertyChange(() => IsVoidEnabled);
            }
        }

        #region User Authorized Buttons
        public bool IsVisibleCustomerService
        {
            get { return _isVisibleCustomerService; }
            set
            {
                if (Equals(value, _isVisibleCustomerService)) return;
                _isVisibleCustomerService = value;
                NotifyOfPropertyChange(() => IsVisibleCustomerService);
            }
        }

        public bool IsVisibleOrderCreateEditDelete
        {
            get { return _isVisibleOrderCreateEditDelete; }
            set
            {
                if (Equals(value, _isVisibleOrderCreateEditDelete)) return;
                _isVisibleOrderCreateEditDelete = value;
                NotifyOfPropertyChange(() => IsVisibleOrderCreateEditDelete);
            }
        }

        #endregion

        #region All Orders in grid
        public float TotalTons
        {
            get { return _totalTons; }
            set
            {
                if (value.Equals(_totalTons)) return;
                _totalTons = value;
                NotifyOfPropertyChange(() => TotalTons);
            }
        }

        public int TotalTickets
        {
            get { return _totalTickets; }
            set
            {
                if (value == _totalTickets) return;
                _totalTickets = value;
                NotifyOfPropertyChange(() => TotalTickets);
            }
        }

        public int TicketsToday
        {
            get { return _ticketsToday; }
            set
            {
                if (value == _ticketsToday) return;
                _ticketsToday = value;
                NotifyOfPropertyChange(() => TicketsToday);
            }
        }
        #endregion

        #region All Orders in grid for selected order
        public float TotalTonsForSelectedOrder
        {
            get { return _totalTonsForSelectedOrder; }
            set
            {
                if (value.Equals(_totalTonsForSelectedOrder)) return;
                _totalTonsForSelectedOrder = value;
                NotifyOfPropertyChange(() => TotalTonsForSelectedOrder);
            }
        }

        public int TotalTicketsForSelectedOrder
        {
            get { return _totalTicketsForSelectedOrder; }
            set
            {
                if (value == _totalTicketsForSelectedOrder) return;
                _totalTicketsForSelectedOrder = value;
                NotifyOfPropertyChange(() => TotalTicketsForSelectedOrder);
            }
        }

        public int TicketsTodayForSelectedOrder
        {
            get { return _ticketsTodayForSelectedOrder; }
            set
            {
                if (value == _ticketsTodayForSelectedOrder) return;
                _ticketsTodayForSelectedOrder = value;
                NotifyOfPropertyChange(() => TicketsTodayForSelectedOrder);
            }
        }
        #endregion


        #region Count for Selected Order and Selected Date
        public int TicketsSelectedDayForSelectedOrder
        {
            get { return _ticketsSelectedDayForSelectedOrder; }
            set
            {
                if (value.Equals(_ticketsSelectedDayForSelectedOrder)) return;
                _ticketsSelectedDayForSelectedOrder = value;
                NotifyOfPropertyChange(() => TicketsSelectedDayForSelectedOrder);
            }
        }

        public int TotalTicketsForSelectedOrderSelectedDay
        {
            get { return _totalTicketsForSelectedOrderSelectedDay; }
            set
            {
                if (value == _totalTicketsForSelectedOrderSelectedDay) return;
                _totalTicketsForSelectedOrderSelectedDay = value;
                NotifyOfPropertyChange(() => TotalTicketsForSelectedOrderSelectedDay);
            }
        }

        public float TotalTonsForSelectedOrderSelectedDay
        {
            get { return _totalTonsForSelectedOrderSelectedDay; }
            set
            {
                if (value == _totalTonsForSelectedOrderSelectedDay) return;
                _totalTonsForSelectedOrderSelectedDay = value;
                NotifyOfPropertyChange(() => TotalTonsForSelectedOrderSelectedDay);
            }
        }
        #endregion

        private void InitializeTicketsGrid()
        {
            try
            {
                SelectedTicket = null;

                IReadOnlyList<ITicketInfo> tickets = IoC.Get<ITicketService>().GetTicketsInfo(_seasonId, CustomerId, OrderId);
                TicketList = new BindableCollection<ITicketInfo>(tickets);
                InitializeTotal();
                InitializeTicketCountPerDay();

                if (UserAuthorization.UserAuthorizationLoggedIn == null)
                {
                    IsVisibleCustomerService = false;
                    IsVisibleOrderCreateEditDelete = false;
                }
                else
                {
                    IsVisibleCustomerService = UserAuthorization.UserAuthorizationLoggedIn.IsAuthorizedCustomerService;
                    IsVisibleOrderCreateEditDelete = UserAuthorization.UserAuthorizationLoggedIn.IsAuthorizedOrderCreateEditDelete;
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action =
                    () =>
                    {
                        MessageBoxResult messageBoxResult =
                            (MessageBox.Show(string.Format(
                                "Loading of Tickets failed, please let support know. Exception messages"
                                + Environment.NewLine +
                                "Execption Message: \"{0}\""
                                + Environment.NewLine +
                                "Inner Execption Message: \"{1}\"",
                                ex.Message, ex.InnerException.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                    };

                action.OnUIThreadAsync();
            }
        }

        private void InitializeTotal()
        {
            TotalTons = 0;
            if (TicketList == null || !TicketList.Any()) return;

            TotalTons = TicketList.Sum(ticket => ticket.Tons);
            TotalTickets = TicketList.Count;
            TicketsToday = TicketList.Count(t => t.CreateTime.Date == DateTime.Today);
        }

        private void InitializeTicketCountPerDay()
        {
            int ticketCount = 0;
            var previousDate = new DateTime();

            if (TicketList != null && TicketList.Any())
            {
                foreach (ITicketInfo ticket in TicketList.OrderBy(x => x.OrderNumber))
                {
                    if (ticket.CreateTime.Date != previousDate.Date)
                    {
                        ticketCount = 1;
                        ticket.TicketCountPerDay = ticketCount;
                    }
                    else
                    {
                        ticketCount += 1;
                        ticket.TicketCountPerDay = ticketCount;
                    }
                    previousDate = ticket.CreateTime;
                }
            }

            if (SelectedTicket != null)
            {
                TicketsTodayForSelectedOrder = TicketList.Where(w => w.OrderId == _selectedTicket.OrderId).Count(t => t.CreateTime.Date == DateTime.Today);
                TotalTicketsForSelectedOrder = TicketList.Where(w => w.OrderId == _selectedTicket.OrderId).Count();
                TotalTonsForSelectedOrder = TicketList.Where(w => w.OrderId == _selectedTicket.OrderId).Sum(orderTicket => orderTicket.Tons);

                
                TicketsSelectedDayForSelectedOrder = TicketList.Where(w => w.OrderId == _selectedTicket.OrderId).Count(t => t.CreateTime.Date == _selectedTicket.CreateTime.Date);
                TotalTicketsForSelectedOrderSelectedDay = TicketList.Where(w => w.OrderId == _selectedTicket.OrderId).Count(t => t.CreateTime.Date == _selectedTicket.CreateTime.Date);
                TotalTonsForSelectedOrderSelectedDay = TicketList.Where(w => w.OrderId == _selectedTicket.OrderId && w.CreateTime.Date == _selectedTicket.CreateTime.Date).Sum(orderTicket => orderTicket.Tons);
            }
        }

        #region Actions

        public void Customer()
        {
            if (SelectedTicket == null && !CustomerId.HasValue) return;

            var newCustomerViewModel = IoC.Get<CustomerViewModel>();

            newCustomerViewModel.CustomerId = CustomerId.HasValue ? CustomerId.Value : SelectedTicket.CustomerId;

            var windowManager = IoC.Get<IWindowManager>();
            windowManager.ShowDialog(newCustomerViewModel);
        }

        public void Order()
        {
            if (SelectedTicket == null && !OrderId.HasValue) return;
            
            var windowManager = IoC.Get<IWindowManager>();

            var editOrderViewModel = IoC.Get<EditOrderViewModel>();
            editOrderViewModel.OrderId = OrderId.HasValue ? OrderId.Value : SelectedTicket.OrderId;
            windowManager.ShowDialog(editOrderViewModel);
        }

        public void Void()
        {
            if (SelectedTicket == null) return;

            var windowManager = IoC.Get<IWindowManager>();

            var voidReasonViewModel = IoC.Get<VoidReasonViewModel>();
            voidReasonViewModel.Ticket = SelectedTicket;
            windowManager.ShowDialog(voidReasonViewModel);
            InitializeTicketsGrid();

            _eventAggregator.PublishOnCurrentThread(new TicketAddedVoidedArgs(null));
        }

        public void KeyHandle(Key key)
        {
            if (key == Key.F2)
            {
                Done();
            }
        }

        public void Done()
        {
            try
            {
                if (SelectedTicket != null || CustomerId.HasValue)
                    _eventAggregator.PublishOnCurrentThread(
                        new CustomerModifiedArgs(CustomerId.HasValue ? CustomerId.Value : SelectedTicket.CustomerId));

                if (OrderId.HasValue)
                    _eventAggregator.PublishOnCurrentThread(new OrderModifiedArgs(OrderId.Value));

                TryClose(true);
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "Customer saving failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };

                action.OnUIThreadAsync();
            }
        }

        #endregion

        #region Events

        public TicketViewerViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            _log = LogManager.GetLog(typeof (TicketViewerViewModel));
            _seasonId = (int) Application.Current.Properties["SeasonId"];
            DisplayName = "Tickets For";
        }

        protected override void OnInitialize()
        {
            base.OnInitialize();

            InitializeTicketsGrid();

            // Set the starting display values for the window header
            DisplayName = String.Format("Tickets for {0}", CustomerShortName);
            string localOrderNumber = "NA";

            if (OrderId.HasValue)
            {
                localOrderNumber = IoC.Get<IOrderService>().GetOrder(_seasonId, OrderId.Value).OrderNumber.ToString();
                DisplayName = String.Format("{0} Order #: {1}   (Internal Order ID: {2})", DisplayName, localOrderNumber, OrderId.Value);

                NotifyOfPropertyChange(() => DisplayName);
            }
        }

        protected override void OnActivate()
        {
            _eventAggregator.Subscribe(this);
            base.OnActivate();
        }

        protected override void OnDeactivate(bool close)
        {
            _eventAggregator.Unsubscribe(this);
            base.OnDeactivate(close);
        }

        #endregion
    }
}