﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Caliburn.Micro;
using Contract.Domain;
using Contract.Events;
using Contract.Services;
using ZTech.Controls;
using ZTech.Helpers;
using ZTech.Reports;
using ZTech.ViewModels.Abstract;
using Action = System.Action;
using System.Windows.Input;
using System.IO;
using CrystalDecisions.ReportAppServer.Controllers;
using CrystalDecisions.Shared;
using System.Diagnostics;

namespace ZTech.ViewModels
{
    public interface IOrderTicketViewModel : IScreen
    {
        int TruckId { get; set; }
        int? OrderId { get; set; }
    }

    public class OrderTicketViewModel : BaseViewModel, IHandle<ScaleChangedArgs>, IOrderTicketViewModel, IHandle<ScaleResultsArgs>, IHandle<ScaleExceptionsArgs>
    {
        private readonly IEventAggregator _eventAggregator;
        private readonly ILog _log;
        private readonly IScaleManager _scaleManager;
        private readonly int _seasonId;
        private float _balanceToShip;
        private float _balanceToShipBefore;
        private float _TotalToShip;
        private float _POOverLimit;
        private string _POOverLimitText;
        private bool _cash;
        private decimal _cashRate;
        private decimal _change;
        private bool _check;
        private bool _billedFromLowell;
        private Color _checkBackground;
        private string _checkNumber;
        private IReadOnlyList<ICompanyData> _companyList;
        private string _confirmationNumber;
        private bool _creditCard;
        private string _customerAddress;
        private string _customerOrganization;
        private decimal _due;
        private bool _enableCheckNumber;
        private bool _enableTended;
        private string _errors;
        private int _firstWeight;
        private string _firstWeightText;
        private decimal _fuelSurcharge;
        private bool _hideDollarAmount;
        private bool _isBusy;
        //private bool _isCashReadonly;
        private bool _isExempt;
        private bool _isFirstWeightEnabled;
        private bool _isOrderExceed;
        private bool _isTotalOrderExceed;
        //private bool _isSecondWeightOverWeight;
        private bool _isPrintingEnabled;
        //private bool? _isScaleEnabled;
        private bool _isSecondWeightEnabled;
        private bool _isTareExpired;
        private bool _isTicketNumberOverrideEnabled;
        private string _ticketNumberOverrideText;
        private decimal _materialCharge;
        private List<IMaterialInfo> _materialList;
        private int _netWeight;
        private decimal _netWeightTon;
        private string _orderExceedToolTip;
        private string _orderTotalExceedToolTip;
        private string _secondWeightOverWeightToolTip;
        //private TicketPreViewModel _printModel;
        private ScaleWidgetViewModel _scaleWidget;
        private int _secondWeight;
        private string _secondWeightText;
        private ICompanyData _selectedCompany;
        private IMaterialInfo _selectedMaterial;
        private bool _showCashNotification;
        private bool _showCashSection;
        private string _specialInstruction;
        private string _tareExpiredToolTip;

        private ISiteConfig _selectedSite;

        private decimal _tax;
        private string _taxRateLabel;
        private decimal? _tended;
        private bool _isAmountTendedFocused;
        //private string _ticketNumber;
        private string _truckDescription;

        private CrystalDecisions.CrystalReports.Engine.ReportDocument _crystalReportDocument;

        public OrderTicketViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            _log = LogManager.GetLog(typeof(OrderTicketViewModel));
            _seasonId = (int)Application.Current.Properties["SeasonId"];
            _scaleManager = IoC.Get<IScaleManager>();
            DisplayName = "Ticket Preview";
        }

        #region Properties 

        public bool ShowCashNotification
        {
            get { return _showCashNotification; }
            set
            {
                if (value.Equals(_showCashNotification)) return;
                _showCashNotification = value;
                NotifyOfPropertyChange(() => ShowCashNotification);
            }
        }

        public bool ShowCashSection
        {
            get { return _showCashSection; }
            set
            {
                if (value.Equals(_showCashSection)) return;
                _showCashSection = value;
                NotifyOfPropertyChange(() => ShowCashSection);
            }
        }

        public decimal TaxRate { get; set; }

        private ITicketData Ticket { get; set; }

        public string Errors
        {
            get { return _errors; }
            set
            {
                if (value == _errors) return;
                _errors = value;
                NotifyOfPropertyChange(() => Errors);
                //ToDo: Error Log?
            }
        }

        public IScaleInfo SelectedScale { get; set; }

        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                if (value.Equals(_isBusy)) return;
                _isBusy = value;
                NotifyOfPropertyChange(() => IsBusy);
            }
        }

        public string ConfirmationNumber
        {
            get { return _confirmationNumber; }
            set
            {
                if (value == _confirmationNumber) return;
                _confirmationNumber = value;
                NotifyOfPropertyChange(() => ConfirmationNumber);
            }
        }

        public decimal FuelSurcharge
        {
            get { return _fuelSurcharge; }
            set
            {
                if (value == _fuelSurcharge) return;
                _fuelSurcharge = value;
                NotifyOfPropertyChange(() => FuelSurcharge);

                //Ticket.FuelSurcharge = ParseNullDecimalValue(value);
                Ticket.FuelSurcharge = value;
            }
        }

        public string TaxRateLabel
        {
            get { return _taxRateLabel; }
            set
            {
                if (value == _taxRateLabel) return;
                _taxRateLabel = value;
                NotifyOfPropertyChange(() => TaxRateLabel);
            }
        }

        public bool IsTareExpired
        {
            get { return _isTareExpired; }
            set
            {
                if (value.Equals(_isTareExpired)) return;
                _isTareExpired = value;
                NotifyOfPropertyChange(() => IsTareExpired);
            }
        }

        public string TareExpiredToolTip
        {
            get { return _tareExpiredToolTip; }
            set
            {
                if (value == _tareExpiredToolTip) return;
                _tareExpiredToolTip = value;
                NotifyOfPropertyChange(() => TareExpiredToolTip);
            }
        }

        public bool HideDollarAmount
        {
            get { return _hideDollarAmount; }
            set
            {
                if (value.Equals(_hideDollarAmount)) return;
                _hideDollarAmount = value;
                NotifyOfPropertyChange(() => HideDollarAmount);
            }
        }

        public float BalanceToShipBefore
        {
            get { return _balanceToShipBefore; }
            set
            {
                if (value.Equals(_balanceToShipBefore)) return;
                _balanceToShipBefore = value;
                NotifyOfPropertyChange(() => BalanceToShipBefore);
            }
        }

        public float BalanceToShip
        {
            get { return _balanceToShip; }
            set
            {
                if (value.Equals(_balanceToShip)) return;
                _balanceToShip = value;
                NotifyOfPropertyChange(() => BalanceToShip);
            }
        }

        public float TotalToShip
        {
            get { return _TotalToShip; }
            set
            {
                if (value.Equals(_TotalToShip)) return;
                _TotalToShip = value;
                NotifyOfPropertyChange(() => TotalToShip);
            }
        }

        public float POOverLimit
        {
            get { return _POOverLimit; }
            set
            {
                if (value.Equals(_POOverLimit)) return;
                _POOverLimit = value;
                NotifyOfPropertyChange(() => POOverLimit);
            }
        }

        public string POOverLimitText
        {
            get { return _POOverLimitText; }
            set
            {
                if (value.Equals(_POOverLimitText)) return;
                _POOverLimitText = value;
                NotifyOfPropertyChange(() => POOverLimitText);
            }
        }

        public string OrderExceedToolTip
        {
            get { return _orderExceedToolTip; }
            set
            {
                if (value == _orderExceedToolTip) return;
                _orderExceedToolTip = value;
                NotifyOfPropertyChange(() => OrderExceedToolTip);
            }
        }

        public string OrderTotalExceedToolTip
        {
            get { return _orderTotalExceedToolTip; }
            set
            {
                if (value == _orderTotalExceedToolTip) return;
                _orderTotalExceedToolTip = value;
                NotifyOfPropertyChange(() => _orderTotalExceedToolTip);
            }
        }

        public string SecondWeightOverWeightToolTip
        {
            get { return _secondWeightOverWeightToolTip; }
            set
            {
                if (value == _secondWeightOverWeightToolTip) return;
                _secondWeightOverWeightToolTip = value;
                NotifyOfPropertyChange(() => SecondWeightOverWeightToolTip);
            }
        }

        public bool IsOrderExceed
        {
            get { return _isOrderExceed; }
            set
            {
                if (value.Equals(_isOrderExceed)) return;
                _isOrderExceed = value;
                NotifyOfPropertyChange(() => IsOrderExceed);
            }
        }

        public bool IsTotalOrderExceed
        {
            get { return _isTotalOrderExceed; }
            set
            {
                if (value.Equals(_isTotalOrderExceed)) return;
                _isTotalOrderExceed = value;
                NotifyOfPropertyChange(() => _isTotalOrderExceed);
            }
        }

        public bool IsSecondWeightOverWeight
        {
            get { return _isOrderExceed; }
            set
            {
                if (value.Equals(_isOrderExceed)) return;
                _isOrderExceed = value;
                NotifyOfPropertyChange(() => IsSecondWeightOverWeight);
            }
        }

        public bool IsPrintingEnabled
        {
            get { return _isPrintingEnabled; }
            set
            {
                if (value.Equals(_isPrintingEnabled)) return;
                _isPrintingEnabled = value;
                NotifyOfPropertyChange(() => IsPrintingEnabled);
            }
        }

        public bool EnableCheckNumber
        {
            get { return _enableCheckNumber; }
            set
            {
                if (value.Equals(_enableCheckNumber)) return;
                _enableCheckNumber = value;
                NotifyOfPropertyChange(() => EnableCheckNumber);
            }
        }

        #endregion

        #region Scale Properties

        public bool IsTicketNumberOverrideEnabled
        {
            get { return _isTicketNumberOverrideEnabled; }
            set
            {
                if (value.Equals(_isTicketNumberOverrideEnabled)) return;
                _isTicketNumberOverrideEnabled = value;
                NotifyOfPropertyChange(() => IsTicketNumberOverrideEnabled);
            }
        }

        public string TicketNumberOverrideText
        {
            get { return _ticketNumberOverrideText; }
            set
            {
                if (value.Equals(_ticketNumberOverrideText)) return;
                _ticketNumberOverrideText = value;
                NotifyOfPropertyChange(() => TicketNumberOverrideText);
                CanPreviewTicket();
            }
        }


        public bool IsSecondWeightEnabled
        {
            get { return _isSecondWeightEnabled; }
            set
            {
                if (value.Equals(_isSecondWeightEnabled)) return;
                _isSecondWeightEnabled = value;
                NotifyOfPropertyChange(() => IsSecondWeightEnabled);
            }
        }

        public bool IsFirstWeightEnabled
        {
            get { return _isFirstWeightEnabled; }
            set
            {
                if (value.Equals(_isFirstWeightEnabled)) return;
                _isFirstWeightEnabled = value;
                NotifyOfPropertyChange(() => IsFirstWeightEnabled);
            }
        }

        public ScaleWidgetViewModel ScaleWidget
        {
            get { return _scaleWidget; }
            set
            {
                if (Equals(value, _scaleWidget)) return;
                _scaleWidget = value;
                NotifyOfPropertyChange(() => ScaleWidget);
            }
        }

        public int TruckId { get; set; }
        public int? OrderId { get; set; }

        private void InitializeScale()
        {
            IsSecondWeightEnabled = false;

            if (IoC.Get<IConfiguration>().IsManualMode)
            {
                ManualScalingForShippment();
            }
            else
            {
                StartScaleInteraction();
            }
        }

        public void Override()
        {
            ManualScalingForShippment();
            IsTicketNumberOverrideEnabled = true;
            CanPreviewTicket();
        }

        #endregion

        private void StartScaleInteraction()
        {
            try
            {
                SelectedScale = ScaleWidget.SelectedScale;
                _scaleManager.StartScaling(SelectedScale);
            }
            catch (Exception ex)
            {
                ManualScalingForShippment();
                Errors = $"Start Scale Integration failed with exception: {ex.Message}";
                ShowMessage("Scale Error", Errors, NotificationType.Warning);
                _log.Error(ex);
            }
        }

        private void ManualScalingForShippment()
        {
            IsSecondWeightEnabled = true;
            Ticket.IsManual = true;

            if (ScaleWidget != null)
                ScaleWidget.Errors = null;

            if (_scaleManager != null)
                _scaleManager.StopScaling();
        }


        #region VO

        public Color CheckBackground
        {
            get { return _checkBackground; }
            set
            {
                if (value.Equals(_checkBackground)) return;
                _checkBackground = value;
                NotifyOfPropertyChange(() => CheckBackground);
            }
        }

        public bool Cash
        {
            get { return _cash; }
            set
            {
                if (value.Equals(_cash)) return;
                _cash = value;
                NotifyOfPropertyChange(() => Cash);
                if (value)
                    InitCashOption();
                CanPreviewTicket();
            }
        }

        public bool EnableTended
        {
            get { return _enableTended; }
            set
            {
                if (value.Equals(_enableTended)) return;
                _enableTended = value;
                NotifyOfPropertyChange(() => EnableTended);
            }
        }

        public bool IsAmountTendedFocused
        {
            get { return _isAmountTendedFocused; }
            set
            {
                if (value.Equals(_isAmountTendedFocused)) return;
                _isAmountTendedFocused = value;
                NotifyOfPropertyChange(() => IsAmountTendedFocused);
            }
        }

        public bool CreditCard
        {
            get { return _creditCard; }
            set
            {
                if (value.Equals(_creditCard)) return;
                _creditCard = value;
                NotifyOfPropertyChange(() => CreditCard);
                if (value)
                    InitCreditCardOption();
                CanPreviewTicket();
            }
        }

        public bool BilledFromLowell
        {
            get { return _billedFromLowell; }
            set
            {
                if (value.Equals(_billedFromLowell)) return;
                _billedFromLowell = value;
                NotifyOfPropertyChange(() => BilledFromLowell);
                if (value)
                    InitBilledFromLowellOption();
                CanPreviewTicket();
            }
        }

        public bool Check
        {
            get { return _check; }
            set
            {
                if (value.Equals(_check)) return;
                _check = value;
                NotifyOfPropertyChange(() => Check);
                if (value)
                    InitCheckOption();
                CanPreviewTicket();
            }
        }

        public string CheckNumber
        {
            get { return _checkNumber; }
            set
            {
                if (value == _checkNumber) return;
                _checkNumber = value;
                NotifyOfPropertyChange(() => CheckNumber);
                CanPreviewTicket();
            }
        }

        public decimal Change
        {
            get { return _change; }
            set
            {
                if (value == _change) return;
                _change = value;
                NotifyOfPropertyChange(() => Change);
            }
        }

        public bool IsExempt
        {
            get { return _isExempt; }
            set
            {
                if (value.Equals(_isExempt)) return;
                _isExempt = value;
                NotifyOfPropertyChange(() => IsExempt);
                ReCalculateCash();
            }
        }

        public decimal? Tended
        {
            get { return _tended; }
            set
            {
                if (value == _tended) return;
                _tended = value;
                NotifyOfPropertyChange(() => Tended);
                //decimal tended = ParseDecimalValue(value);
                Change = (value == null) ? 0 : Convert.ToDecimal(value) - Due;
            }
        }

        public decimal Due
        {
            get { return _due; }
            set
            {
                if (value == _due) return;
                _due = value;
                NotifyOfPropertyChange(() => Due);
            }
        }

        public decimal Tax
        {
            get { return _tax; }
            set
            {
                if (value == _tax) return;
                _tax = value * 100;
                NotifyOfPropertyChange(() => Tax);
            }
        }

        public decimal MaterialCharge
        {
            get { return _materialCharge; }
            set
            {
                if (value == _materialCharge) return;
                _materialCharge = value;
                NotifyOfPropertyChange(() => MaterialCharge);
            }
        }

        public decimal CashRate
        {
            get { return _cashRate; }
            set
            {
                if (value == _cashRate) return;
                _cashRate = value;
                NotifyOfPropertyChange(() => CashRate);
                ReCalculateCash();
            }
        }

        public string CustomerAddress
        {
            get { return _customerAddress; }
            set
            {
                if (value == _customerAddress) return;
                _customerAddress = value;
                NotifyOfPropertyChange(() => CustomerAddress);
            }
        }

        public string CustomerOrganization
        {
            get { return _customerOrganization; }
            set
            {
                if (value == _customerOrganization) return;
                _customerOrganization = value;
                NotifyOfPropertyChange(() => CustomerOrganization);
            }
        }

        public string SpecialInstruction
        {
            get { return _specialInstruction; }
            set
            {
                if (value == _specialInstruction) return;
                _specialInstruction = value;
                NotifyOfPropertyChange(() => SpecialInstruction);
            }
        }

        public List<IMaterialInfo> MaterialList
        {
            get { return _materialList; }
            set
            {
                if (Equals(value, _materialList)) return;
                _materialList = value;
                NotifyOfPropertyChange(() => MaterialList);
            }
        }

        public IMaterialInfo SelectedMaterial
        {
            get { return _selectedMaterial; }
            set
            {
                if (Equals(value, _selectedMaterial)) return;
                _selectedMaterial = value;
                NotifyOfPropertyChange(() => SelectedMaterial);
            }
        }

        public IReadOnlyList<ICompanyData> CompanyList
        {
            get { return _companyList; }
            set
            {
                if (Equals(value, _companyList)) return;
                _companyList = value;
                NotifyOfPropertyChange(() => CompanyList);
            }
        }

        public ICompanyData SelectedCompany
        {
            get { return _selectedCompany; }
            set
            {
                if (Equals(value, _selectedCompany)) return;
                _selectedCompany = value;
                NotifyOfPropertyChange(() => SelectedCompany);
                TicketNumberOverrideText = SelectedCompany.NextTicket.ToString();
            }
        }


        public string TruckDescription
        {
            get { return _truckDescription; }
            set
            {
                if (value == _truckDescription) return;
                _truckDescription = value;
                NotifyOfPropertyChange(() => TruckDescription);
            }
        }

        public int FirstWeight
        {
            get { return _firstWeight; }
            set
            {
                if (value == _firstWeight) return;
                _firstWeight = value;
                NotifyOfPropertyChange(() => FirstWeight);
                CalculateNet();
            }
        }

        public string FirstWeightText
        {
            get { return _firstWeightText; }
            set
            {
                if (value == _firstWeightText) return;
                int? firstWeight = ParseIntValue(value);
                FirstWeight = firstWeight ?? 0;
                _firstWeightText = firstWeight.HasValue ? firstWeight.Value.ToString("N0") : null;
                NotifyOfPropertyChange(() => FirstWeightText);
            }
        }

        public int SecondWeight
        {
            get { return _secondWeight; }
            set
            {
                if (value == _secondWeight) return;
                _secondWeight = value;
                NotifyOfPropertyChange(() => SecondWeight);
                CalculateNet();
                CanPreviewTicket();
            }
        }

        public string SecondWeightText
        {
            get { return _secondWeightText; }
            set
            {
                if (value == _secondWeightText) return;
                int? secondWeight = ParseIntValue(value);
                SecondWeight = secondWeight ?? 0;
                _secondWeightText = secondWeight.HasValue ? secondWeight.Value.ToString("N0") : null;
                NotifyOfPropertyChange(() => SecondWeightText);
            }
        }

        public int NetWeight
        {
            get { return _netWeight; }
            set
            {
                if (value == _netWeight) return;
                _netWeight = value;
                NotifyOfPropertyChange(() => NetWeight);
                ReCalculateCash();
            }
        }

        public decimal NetWeightTon
        {
            get { return _netWeightTon; }
            set
            {
                if (value == _netWeightTon) return;
                _netWeightTon = value;
                NotifyOfPropertyChange(() => NetWeightTon);
            }
        }

        private decimal ParseDecimalValue(string income)
        {
            if (string.IsNullOrWhiteSpace(income))
                return 0m;

            decimal result;

            if (decimal.TryParse(income.Trim(), out result))
                return result;

            return 0m;
        }

        private decimal? ParseNullDecimalValue(string income)
        {
            if (string.IsNullOrWhiteSpace(income))
                return null;

            income = income.Replace('_', '0');

            decimal result;

            if (decimal.TryParse(income.Trim(), out result))
                return result;

            return null;
        }

        private int? ParseIntValue(string income)
        {
            if (string.IsNullOrWhiteSpace(income))
                return null;

            income = income.Replace(",", string.Empty);
            income = income.Replace("'", string.Empty);
            income = income.Replace(".", string.Empty);

            int result;

            if (int.TryParse(income.Trim(), out result))
                return result;

            return null;
        }

        #endregion

        #region Events

        public void Handle(ScaleChangedArgs args)
        {
            var _wasPrintingEnabled = false;

            if (args != null)
            {
                if (IsPrintingEnabled)
                {
                    IsPrintingEnabled = false;
                    _wasPrintingEnabled = true;
                }

                if (_scaleManager != null && Ticket != null && !Ticket.IsManual && !Equals(SelectedScale, args.Scale))
                {
                    SelectedScale = args.Scale;

                    _scaleManager.StopScaling();
                    _scaleManager.StartScaling(SelectedScale);
                }

                if (_wasPrintingEnabled)
                {
                    IsPrintingEnabled = true;
                }
            }
        }

        protected override void OnActivate()
        {
            _eventAggregator.Subscribe(this);
            base.OnActivate();
        }

        protected override void OnDeactivate(bool close)
        {
            if (_scaleManager != null)
                _scaleManager.StopScaling();

            _eventAggregator.Unsubscribe(this);

            if (_crystalReportDocument != null)
            {
                _crystalReportDocument.Close();
                _crystalReportDocument.Dispose();
            }

            base.OnDeactivate(close);
        }

        #endregion

        public void Handle(ScaleExceptionsArgs message)
        {
            //ManualScalingForShippment();
            //ToDo: Error Log
            _log.Warn($"Scale Handler: {message.Message}  Scale Id: {message.ScaleId}");
            //            ShowMessage("Scale Errors", $"Scale unavailable this time with exception: '{message.Message}'. Need to scale manually for this ticket or Cancel and come back to this Ship window", NotificationType.Warning);
        }

        public void Handle(ScaleResultsArgs args)
        {
            switch (args.Result.StreamPolarity)
            {
                case ScaleIndicatorStreamPolarity.Negative:
                    //UI label to have text "Negative"
                    ScaleWidget.Errors = args.Result.StreamPolarity.GetDescription();
                    break;

                case ScaleIndicatorStreamPolarity.Overload:
                    //UI label to have text "Overload"
                    ScaleWidget.Errors = args.Result.StreamPolarity.GetDescription();
                    break;

                case ScaleIndicatorStreamPolarity.Underrange:
                    //UI label to have text "Underrange"
                    ScaleWidget.Errors = args.Result.StreamPolarity.GetDescription();
                    break;
                case ScaleIndicatorStreamPolarity.Positive:
                    SecondWeightText =
                        (args.Result.StreamWeight.HasValue
                            ? args.Result.StreamWeight.Value.GetWeightInPounds(args.Result.StreamUnitOfWeight)
                            : 0).ToString("N0");
                    ScaleWidget.Errors = null;
                    break;
                case ScaleIndicatorStreamPolarity.Undefined:
                default:
                    ScaleWidget.Errors = ScaleIndicatorStreamPolarity.Undefined.GetDescription();
                    //ToDo: Log
                    break;
            }
        }

        protected override void OnInitialize()
        {
            try
            {
                base.OnInitialize();
                ScaleWidget = IoC.Get<ScaleWidgetViewModel>();
                ScaleWidget.ConductWith(this);
                // Start as thread
                IsBusy = true;
                var task = new Task(InitTask);
                task.Start();
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(
                            string.Format(
                                "Initialization of scale failed with exception \"{0}\"",
                                ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));

                    TryClose(false);
                };

                action.OnUIThreadAsync();
            }
        }

        private void InitTask()
        {
            try
            {
                // Initialize the task     
                IsTicketNumberOverrideEnabled = false;
                EnableCheckNumber = false;
                EnableTended = false;
                InitializeCompany();
                InitializeMaterial();
                InitializeTicket();
                InitializeScale();


                _selectedSite = IoC.Get<IConfiguration>().SelectedSite;
            }
            catch (Exception ex)
            {
                var exInnerMessageBoxText = string.Empty;

                _log.Error(ex);
                if (ex.InnerException != null)
                {
                    _log.Error(ex.InnerException);
                    exInnerMessageBoxText = $" and inner exception '{ex.InnerException.Message}'{Environment.NewLine} ";
                }

                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(
                            string.Format($"Order Ticket Preview (OrderTicketViewModel) initialization failed with exception '{ex.Message}'{exInnerMessageBoxText}", "Ticket Preview Error", MessageBoxButton.OK, MessageBoxImage.Error)));

                    TryClose(false);
                };

                action.OnUIThreadAsync();
            }
            finally
            {
                IsBusy = false;
            }
        }

        private void InitializeMaterial()
        {
            MaterialList = IoC.Get<IMaterialService>().GetMaterials(_seasonId);
        }

        private void InitializeCompany()
        {
            var commonService = IoC.Get<ICommonService>();
            CompanyList = commonService.GetCompaniesData();
        }

        private void InitializeTicket()
        {
            Ticket = IoC.Get<ITicketService>().GetNewOrderTicket(OrderId, TruckId, _seasonId);
            ConfirmationNumber = Ticket.ConfirmationNumber == null ? "000000" : Ticket.ConfirmationNumber.Value.ToString("000000");
            if (ConfirmationNumber == "000000")
                _log.Warn($"OrderTicketViewModel:InitializeTicket() ConfirmationNumber is null.  Order Num: {Ticket.OrderNumber}  Ticket Num: {Ticket.TicketNumber}");

            TicketNumberOverrideText = Ticket.TicketNumber.ToString();
            TruckDescription = string.Format($"{Ticket.TruckShortName}: {Ticket.TruckTag} {Ticket.TruckSticker} {Ticket.TruckDescription}");

            FirstWeightText = Ticket.TruckWeight.ToString("N0");
            if (Ticket.IsTareExpired)
            {
                IsTareExpired = true;
                IsFirstWeightEnabled = true;
                TareExpiredToolTip = "Truck weight expired. The dispatcher must take a new weight";
                MessageBox.Show(TareExpiredToolTip, "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                Errors = TareExpiredToolTip;
            }
            var secondWeight = (int)(Ticket.Quantity * 2000);
            SecondWeightText = secondWeight == 0 ? null : secondWeight.ToString("N0");

            CalculateNet();

            IMaterialInfo materialInfo = MaterialList.FirstOrDefault(m => m.Id == Ticket.MaterialId);
            if (materialInfo == null)
            {
                IMaterialInfo inactiveMaterial = IoC.Get<IMaterialService>().GetInactiveMaterial(Ticket.MaterialId);
                if (inactiveMaterial != null)
                {
                    MaterialList.Add(inactiveMaterial);
                    SelectedMaterial = inactiveMaterial;
                }
            }
            else
            {
                SelectedMaterial = materialInfo;
            }

            ICompanyData company = CompanyList.FirstOrDefault(m => m.Id == Ticket.CompanyId);
            if (company != null)
            {
                SelectedCompany = company;
                Ticket.TicketNumber = company.NextTicket;
                TicketNumberOverrideText = Ticket.TicketNumber.ToString();
            }

            SpecialInstruction = Ticket.SpecialInstruction;
            CustomerOrganization = Ticket.CustomerOrganization;
            CustomerAddress = Ticket.CustomerAddress;
            HideDollarAmount = Ticket.HideDollarAmount;

            // Get the limit for being over the Bid/Order limit
            POOverLimit = IoC.Get<IOrderService>().GetSitePOOverLimit(_seasonId);

            if (Ticket.CashData == null || !Ticket.CashData.Quote.HasValue || Ticket.CashData.Quote.Value == 0)
            {
                ShowCashSection = false;
                ShowCashNotification = true;
                return;
            }

            ShowCashSection = true;
            ShowCashNotification = false;
            IsExempt = Ticket.CashData.TaxExempt;
            TaxRate = Ticket.CashData.TaxRate.HasValue ? Ticket.CashData.TaxRate.Value : 0m;
            TaxRateLabel = string.Format($"Tax ({(TaxRate * 100).ToString("N2")}%)");
            CashRate = Ticket.CashData.Quote.Value;

            ReCalculateCash();

            //Change = Due;
        }

        private void ReCalculateCash()
        {
            if (!ShowCashSection)
                return;

            MaterialCharge = TruncateDecimals(CashRate * (NetWeight / 2000m));
            Tax = (TaxRate * MaterialCharge) / 100m;

            if (IsExempt)
                Tax = 0m;

            Due = TruncateDecimals(Tax + MaterialCharge);
            if (!Check && !CreditCard && !Cash)
                Tended = 0;
            if (Check || CreditCard || BilledFromLowell)
                Tended = Due;

            Change = (Tended == 0 || Tended == null) ? Due : Convert.ToDecimal(Tended) - Due;

            // Recalculate the Net weights
            CalculateNet();
        }

        private void CalculateNet()
        {
            // Calculate new Net weights
            IsOrderExceed = false;
            OrderExceedToolTip = string.Empty;
            NetWeight = SecondWeight - FirstWeight;
            NetWeightTon = TruncateDecimals(NetWeight / 2000m);

            // Update balance values
            BalanceToShipBefore = IoC.Get<IOrderService>().GetOrderBalance(OrderId.Value, _seasonId);
            BalanceToShip = BalanceToShipBefore - ((float)NetWeightTon < 0 ? 0 : (float)NetWeightTon);
            TotalToShip = IoC.Get<IOrderService>().GetOrderRequestedTotal(OrderId.Value, _seasonId);
            POOverLimit = IoC.Get<IOrderService>().GetSitePOOverLimit(_seasonId);
            if (POOverLimit > 1)
                POOverLimitText = String.Format("{0:0} up to {1:0}", TotalToShip, POOverLimit * TotalToShip);
            else
                POOverLimitText = String.Format("{0:0}", TotalToShip);

            // 2018-Jun-13, JGS - Process overweight for order/bid
            if (NetWeightTon > 0 && (BalanceToShip - (double)NetWeightTon) < 0)
            {
                // If over the alotted 
                if (((double)(TotalToShip * POOverLimit) - (double)NetWeightTon) < 0)
                {
                    IsOrderExceed = true;
                    IsTotalOrderExceed = true;
                    OrderExceedToolTip = "The amount of material loaded on a truck is more than the amount in the order and more than the bid overage limit.";
                    _log.Info($"The amount of material loaded on a truck is more than the amount in the order and more than the bid overage limit.{Environment.NewLine}Order Total: {TotalToShip} upto {POOverLimit}  Order Balance: {BalanceToShip}   Net: {NetWeightTon}");
                }
                else
                {
                    IsOrderExceed = true;
                    OrderExceedToolTip = "The amount of material loaded on a truck is more than the amount in the order.";
                    _log.Info($"The amount of material loaded on a truck is more than the amount in the order.{Environment.NewLine}Order Total: {TotalToShip}   Order Balance: {BalanceToShip}   Net: {NetWeightTon}");
                }
            }

            if (SecondWeight >= 103940)
            {
                IsSecondWeightOverWeight = true;
                SecondWeightOverWeightToolTip = "Truck is overweight";
                _log.Info($"Truck is overweight:  Tare: {FirstWeight}  Net: {NetWeightTon}");
            }
            else
            {
                IsSecondWeightOverWeight = false;
            }
        }

        public void Cancel()
        {
            TryClose(false);
        }

        public void PrintTicket()
        {
            IsPrintingEnabled = false;

            var stopWatch = System.Diagnostics.Stopwatch.StartNew();

            // Ticket: 179943 - Pause for a random number of milliseconds from 0 to 250.
            Random random = new Random();
            int randomNumber = random.Next(0, 250);
            System.Threading.Thread.Sleep(randomNumber);

            try
            {
                Debug.WriteLine("Print Button Click - Begin");
                _log.Info($"Print Button Click - Begin");

                if (_scaleManager != null)
                {
                    _scaleManager.StopScaling();
                }

                if (!Validate())
                {
                    if (!IoC.Get<IConfiguration>().IsManualMode && !Ticket.IsManual)
                    {
                        StartScaleInteraction();
                    }

                    return;
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "Ticket saving failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };

                action.OnUIThread();
                return;
            }


            try
            {
                
                if (_selectedSite.IsDefault || _selectedSite.Name == "Dev")
                {
                    lock (this.Ticket)
                    {
                        SaveTicket();
                        GeneratePrint();
                        Print();

                        _eventAggregator.PublishOnCurrentThread(new OrderModifiedArgs(Ticket.OrderId));
                        _eventAggregator.PublishOnUIThreadAsync(new TruckModifiedArgs(null));
                        _eventAggregator.PublishOnCurrentThread(new TicketAddedVoidedArgs(Ticket.TicketId));
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "Ticket printing failed with exception when printing a ticket: \"{0}\".", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };

                action.OnUIThread();
            }
            finally
            {
                _log.Info($"Print Button Click - End");
                stopWatch.Stop();
                _log.Info($"Print Button Click Time to Process - {stopWatch.ElapsedMilliseconds}ms");

                // Close the form
                TryClose(true);
            }
        }

        public void CanPreviewTicket()
        {
            int number = ParseIntValue(TicketNumberOverrideText) ?? 0;
            IsPrintingEnabled = FirstWeight >= 0
                                && SecondWeight > FirstWeight
                                && number > 0;

            if (IsPrintingEnabled && ShowCashSection)
            {
                IsPrintingEnabled = Check || CreditCard || Cash || BilledFromLowell;

                if (Check && string.IsNullOrWhiteSpace(CheckNumber))
                {
                    IsPrintingEnabled = false;
                }
            }
            #region Site specific weight limit checks
            if (_selectedSite != null)
            {
                switch (_selectedSite.Name)
                {
                    case "StatenIsland":
                        _log.Info($"CanPreviewTicket Site Specific Weight Limit Checks - Site: {_selectedSite.Name} - Begin");
                        if (SecondWeight >= 124000 && !IsTicketNumberOverrideEnabled)
                        {
                            IsPrintingEnabled = false;
                            _log.Info($"CanPreviewTicket Site Specific Weight Limit Checks - Site: {_selectedSite.Name} - Print button disabled{Environment.NewLine}Second Weight: {SecondWeight}, Is Ticket Override Enabled: {IsTicketNumberOverrideEnabled}");
                        }
                        _log.Info($"CanPreviewTicket Site Specific Weight Limit Checks - Site: {_selectedSite.Name} - End");
                        break;
                }
            }
            #endregion
            
        }

        public void PreviewTicket()
        {
            try
            {
                if (_scaleManager != null)
                    _scaleManager.StopScaling();

                if (!Validate())
                {
                    if (!IoC.Get<IConfiguration>().IsManualMode)
                        StartScaleInteraction();

                    return;
                }
                int number = ParseIntValue(TicketNumberOverrideText) ?? 0;

                Ticket.TicketNumber = number;
                GeneratePrint();
                Preview();
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Action action = () =>
                {
                    MessageBoxResult messageBoxResult =
                        (MessageBox.Show(string.Format(
                            "Ticket saving/printing failed with exception \"{0}\". Please reopen application and try again or contact HelpDesk.",
                            ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                };

                action.OnUIThreadAsync();
            }
        }

        private void SaveTicket()
        {
            var debugTextSavingTicketBegin = "Saving Ticket - Begin";
            Debug.WriteLine(debugTextSavingTicketBegin);
            _log.Info(debugTextSavingTicketBegin);

            Ticket.TicketNumber = ParseIntValue(TicketNumberOverrideText) ?? 0;
            Ticket.UpdateTime = DateTime.Now;
            Ticket.Quantity = NetWeight;
            if (ShowCashSection)
            {
                if (!BilledFromLowell)
                {
                    Ticket.CashData.Tare = Ticket.TruckWeight;
                    Ticket.CashData.TaxExempt = IsExempt;
                    if (Check)
                    {
                        Ticket.CashData.PaymentForm = "Check";
                        Ticket.CashData.CheckNumber = CheckNumber;
                    }
                    else if (CreditCard)
                    {
                        Ticket.CashData.PaymentForm = "CC";
                    }
                    else if (Cash)
                    {
                        Ticket.CashData.PaymentForm = "Cash";
                    }
                }
            }

            // Write amounts
            Ticket.DataA = Due.ToString() + "," + Tended.ToString() + "," + Change.ToString();

            var debugTextSavingTicketEnd = "Saving Ticket - End";
            Debug.WriteLine(debugTextSavingTicketEnd);
            Debug.WriteLine($"Saved ticket '{Ticket.TicketNumber}'");

            _log.Info(debugTextSavingTicketEnd);
            _log.Info($"Saved ticket '{Ticket.TicketNumber}'");

            _log.Info($"Before Ticket Saved: '{Ticket.TicketNumber}'");
            Ticket.TicketId = IoC.Get<ITicketService>().SaveOrderTicket(Ticket);
            _log.Info($"After Ticket Saved: '{Ticket.TicketNumber}'");

        }

        private void GeneratePrint()
        {
            var stopWatch = System.Diagnostics.Stopwatch.StartNew();

            _log.Info($"Generate Print - Begin");

            // Customer Name
            //string customerOrganization = Ticket.CustomerOrganization.Substring(0, Math.Min(45, Ticket.CustomerOrganization.Length));
            string customerOrganization = Ticket.CustomerOrganization.Substring(0, Math.Min(45, Ticket.CustomerOrganization.Length));
            if (customerOrganization.Length > 45)
                customerOrganization = customerOrganization.Substring(0, 45);

            // Address
            string contactName = (Ticket.CustomerContactName == null) ? string.Empty : Ticket.CustomerContactName.Substring(0, Math.Min(45, Ticket.CustomerContactName.Length));
            string ad1 = Ticket.Address.AddressLineOne.Substring(0, Math.Min(45, Ticket.Address.AddressLineOne.Length));
            string ad2 = Ticket.Address.AddressLineTwo.Substring(0, Math.Min(45, Ticket.Address.AddressLineTwo.Length));
            if (ad2 != string.Empty)
            {
                ad1 += Environment.NewLine + ad2;
            }

            var zipCodeCheck = Ticket.Address.Zip.Trim() != string.Empty ? ", " + Ticket.Address.Zip : string.Empty;
            string csz = string.Format($"{Ticket.Address.City.Trim()} {Ticket.Address.State.Trim()}{zipCodeCheck}");

            string address = String.Format($"{ad1}{Environment.NewLine}{csz}{Environment.NewLine}{Ticket.CustomerPhone}");

            // Ticket Number
            ISeason season = IoC.Get<ICommonService>().GetSeasonById(_seasonId);
            string seasonprefix = string.Empty;
            if (season != null)
            {
                seasonprefix = season.Prefix.Split(new[] { '-' }, StringSplitOptions.RemoveEmptyEntries)[0];
            }

            string ticketNum = string.Format("{0}", Ticket.TicketNumber.ToString("000000"));

            //Company
            var companyId = Ticket.CompanyId == null ? -1 : Ticket.CompanyId.Value;
            var company = IoC.Get<ICommonService>().GetCompanyData(companyId);
            var companyName = string.Empty;
            var companyDivision = string.Empty;
            var companyAddress = string.Empty;
            var companyPhoneNumber1 = string.Empty;
            var companyPhoneNumber2 = string.Empty;
            if (company != null)
            {
                companyName = company.Name;
                companyDivision = (company.AddressLineOne != string.Empty || company.AddressLineOne != null) ? company.AddressLineOne: string.Empty;
                if (companyDivision.IndexOf("Division", 0) > 0)
                    companyName = String.Format("{0}\n{1}", company.Name, companyDivision);

                //companyAddress = $"{company.AddressLineOne}, {company.City}, {company.State} {company.Zip}";
                companyAddress = String.Format("{0}{1}\n{2}, {3}  {4}",
                    (company.AddressLineOne.IndexOf("Division", 0) == -1) ? company.AddressLineOne + "  ": string.Empty,
                    (company.AddressLineTwo != string.Empty || company.AddressLineTwo != null) ? company.AddressLineTwo + " " : string.Empty,
                    company.City,
                    company.State,
                    company.Zip);


                companyPhoneNumber1 = (company.PhoneNumberOne != string.Empty && company.PhoneNumberOne != null) ? "  Phone: " + company.PhoneNumberOne : string.Empty;
                companyPhoneNumber2 = (company.PhoneNumberTwo != string.Empty && company.PhoneNumberTwo != null) ? "  Phone: " + company.PhoneNumberTwo : string.Empty;
            }

            //PONumber
            string ponumber = Ticket.PO != null ? Ticket.PO : " -  ";

            // Cash Rate
            string truckRate = (Math.Floor(Ticket.TruckRate.Value * 100m)).ToString("0000");

            // Gross, Tare, Net -- Value is in lbs and should be rounded to a whole number
            //-- Done so the weight read at the screen when printing is the same that is saved in the database

            string tare = Ticket.TruckWeight.ToString("N0");
            string gross = (Ticket.Quantity + Ticket.TruckWeight).ToString("N0");
            string net = Ticket.Quantity.ToString("N0");
            string tons = (Ticket.Quantity / 2000).ToString("N2");

            // Material Cost, Sales Tax, Total
            string materialCost = string.Empty;
            string salesTax = string.Empty;
            string total = string.Empty;
            string paymentType = string.Empty;

            var isHideDollarAmountToTicketTruck = false;
            var isHideDollarAmountToTicketOffice = false;
            var isHideDollarAmountToTicketCustomer = false;
            var isHideDollarAmountToTicketDispatch = false;

            if (ShowCashSection && !BilledFromLowell)
            {
                materialCost = MaterialCharge.ToString("C");
                salesTax = Tax.ToString("C");
                total = Due.ToString("C");
                if (Cash)
                    paymentType = "Cash";
                if (Check)
                    paymentType = $"Check {CheckNumber}";
                if (CreditCard)
                    paymentType = "Credit Card";
            }

            if (HideDollarAmount)
            {
                isHideDollarAmountToTicketTruck = true;
                isHideDollarAmountToTicketOffice = true;
                isHideDollarAmountToTicketCustomer = true;
            }

            string ordnum = Ticket.OrderNumber.HasValue ? Ticket.OrderNumber.Value.ToString() : string.Empty;


            //-- Tag = Plate
            string truckName = $"Truck: {Ticket.TruckShortName}";
            string truckPlate = $"Plate: {(string.IsNullOrWhiteSpace(Ticket.TruckTag) ? " - " : Ticket.TruckTag)}";
            string truckTag = $"Tag: {(string.IsNullOrWhiteSpace(Ticket.TruckSticker) ? " - " : Ticket.TruckSticker)}";

            string balance = BalanceToShip.ToString("N2");

            string confirmationNumber = Ticket.ConfirmationNumber.HasValue
                ? Ticket.ConfirmationNumber.Value.ToString("000000")
                : string.Empty;

            string manuallyText = Ticket.IsManual ? "Manually Key Weight " : string.Empty;

            // WEIGHT MASTER
            var weighMasterNameNumber = IoC.Get<IUserService>().GetByUsername(Application.Current.Properties["UserName"].ToString());

            string _applicationPathReports = AppDomain.CurrentDomain.BaseDirectory + "Reports" + Path.DirectorySeparatorChar;

            _log.Info($"Generate Print - End");
            stopWatch.Stop();
            _log.Info($"Generate Print Time to Process - {stopWatch.ElapsedMilliseconds}ms");

            stopWatch.Restart();

            #region Crystal Ticket
            _log.Info($"Generate Print Ticket in Crystal - Begin");

            _crystalReportDocument = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
            _crystalReportDocument.Load(_applicationPathReports + "Ticket.rpt", CrystalDecisions.Shared.OpenReportMethod.OpenReportByTempCopy);

            CrystalDecisions.CrystalReports.Engine.Database crDatabase;
            CrystalDecisions.CrystalReports.Engine.Tables crTables;

            CrystalDecisions.Shared.ConnectionInfo crConnectioninfo = new CrystalDecisions.Shared.ConnectionInfo();

            _crystalReportDocument.DataSourceConnections.ToString();

            crDatabase = _crystalReportDocument.Database;
            crTables = crDatabase.Tables;

            _crystalReportDocument.SetParameterValue("CompanyName", companyName);
            _crystalReportDocument.SetParameterValue("CompanyPhoneAddress", companyAddress + companyPhoneNumber1 + companyPhoneNumber2);
            _crystalReportDocument.SetParameterValue("CustomerOrganization", customerOrganization);
            _crystalReportDocument.SetParameterValue("Address", address);
            _crystalReportDocument.SetParameterValue("SpecialInstructions", SpecialInstruction ?? string.Empty);
            _crystalReportDocument.SetParameterValue("TicketNumber", ticketNum);
            _crystalReportDocument.SetParameterValue("Date", Ticket.CreateTime.ToString("MM/dd/yy HH:mm"));
            _crystalReportDocument.SetParameterValue("ConfirmationNumber", confirmationNumber);
            _crystalReportDocument.SetParameterValue("PONumber", ponumber);
            _crystalReportDocument.SetParameterValue("OrderNumber", ordnum);
            _crystalReportDocument.SetParameterValue("CustomerIdWithRate", Ticket.CustomerShortName + '#' + truckRate);
            _crystalReportDocument.SetParameterValue("ManuallyText", manuallyText);
            _crystalReportDocument.SetParameterValue("WeighMasterNameNumber", string.Format($"{weighMasterNameNumber.WeighMasterNumber}   {weighMasterNameNumber.WeighMasterName}"));

            // Reset the Weigh Master name the specified name
            string tmpWM = "*" + weighMasterNameNumber.UserName.ToString().Trim().ToUpper() + "*";
            string tmpWMO = "*" + Application.Current.Properties["WeighMasterNameOverride"].ToString().Trim().ToUpper() + "*";
            if (tmpWM != tmpWMO)
            {
                _crystalReportDocument.SetParameterValue("WeighMasterNameNumber", string.Format($"{Application.Current.Properties["WeighMasterNameOverride"].ToString()}"));
            }

            _crystalReportDocument.SetParameterValue("MaterialDescription", SelectedMaterial.Description ?? string.Empty);


            _crystalReportDocument.SetParameterValue("Gross", gross);
            _crystalReportDocument.SetParameterValue("Tare", tare);
            _crystalReportDocument.SetParameterValue("Net", net);
            _crystalReportDocument.SetParameterValue("Tons", tons);

            _crystalReportDocument.SetParameterValue("MaterialCost", materialCost);
            _crystalReportDocument.SetParameterValue("SalesTax", salesTax);
            _crystalReportDocument.SetParameterValue("Total", total);
            //_crystalReportDocument.SetParameterValue("Tended", Tended);
            _crystalReportDocument.SetParameterValue("PaymentType", paymentType);
            _crystalReportDocument.SetParameterValue("Change", Change.ToString());

            _crystalReportDocument.SetParameterValue("IsHideDollarAmountToTicketTruck", isHideDollarAmountToTicketTruck);
            _crystalReportDocument.SetParameterValue("IsHideDollarAmountToTicketOffice", isHideDollarAmountToTicketOffice);
            _crystalReportDocument.SetParameterValue("IsHideDollarAmountToTicketCustomer", isHideDollarAmountToTicketCustomer);
            _crystalReportDocument.SetParameterValue("IsHideDollarAmountToTicketDispatch", isHideDollarAmountToTicketDispatch);

            _crystalReportDocument.SetParameterValue("TruckName", truckName);
            _crystalReportDocument.SetParameterValue("TruckPlate", truckPlate);
            _crystalReportDocument.SetParameterValue("TruckTag", truckTag);

            // Set the Weigh Master signature to the specified image or not
            if (tmpWM != tmpWMO)
            {
                _crystalReportDocument.SetParameterValue("SignatureImageFilePath", _applicationPathReports + "Signature" + Path.DirectorySeparatorChar + "Blank.bmp");
            }
            else
            {
                // Load the signature file
                if (weighMasterNameNumber.SignatureFileName != "")
                    _crystalReportDocument.SetParameterValue("SignatureImageFilePath", _applicationPathReports + "Signature" + Path.DirectorySeparatorChar + weighMasterNameNumber.SignatureFileName + ".bmp");
                else
                    _crystalReportDocument.SetParameterValue("SignatureImageFilePath", _applicationPathReports + "Signature" + Path.DirectorySeparatorChar + weighMasterNameNumber.UserName + ".bmp");
            }

            _log.Info($"Generate Print Ticket in Crystal - End");
            stopWatch.Stop();
            _log.Info($"Generate Print Ticket in Crystal Time to Process - {stopWatch.ElapsedMilliseconds}ms");

            #endregion
        }

        private void Print()
        {
            var stopWatch = System.Diagnostics.Stopwatch.StartNew();
            
            _log.Info($"Printing of Ticket - Begin");

            //-- Production
            try
            {
                PrintReportOptions crystalPrintReportOptions = new PrintReportOptions();
                crystalPrintReportOptions.PrinterName = IoC.Get<IConfiguration>().DefaultPrinterNameLocalComputer;
                crystalPrintReportOptions.RemoveAllPrinterPageRanges();
                crystalPrintReportOptions.AddPrinterPageRange(1, 1);
                _crystalReportDocument.ReportClientDocument.PrintOutputController.PrintReport(crystalPrintReportOptions);
            } catch (Exception ex)
            {
                _log.Error(ex);
            }

            _log.Info($"Printing of Ticket - End");
            stopWatch.Stop();
            _log.Info($"Printing of Ticket Time to Process - {stopWatch.ElapsedMilliseconds}ms");
        }

        private void Preview()
        {
            //var windowManager = IoC.Get<IWindowManager>();
            //_printModel.DefaultPrinter = SelectedCompany.PrinterTicketName ?? string.Empty;
            //_printModel.IsPrintActive = false;
            //windowManager.ShowDialog(_printModel);
            ////_printModel = null;  // 20190225, JGS - Make certain objects opened as windows are set to null
        }

        private bool Validate()
        {
            Errors = null;
            var str = new List<string>();
            if (FirstWeight <= 0)
                str.Add("First Weight must be more than 0.");

            if (SecondWeight <= 0)
                str.Add("Second Weight must be more than 0.");
            else if (SecondWeight <= FirstWeight && FirstWeight > 0)
                str.Add("Second Weight must be more than First Weight.");

            if (SecondWeight > 150000)
                str.Add("Second Weight must be less than 150000 lbs.");

            if (NetWeight < 0)
                str.Add("Net Weight must be more than 0");

            if (ShowCashSection)
            {
                if (!CreditCard && !Cash && !Check && !BilledFromLowell)
                    str.Add("You must select a payment type.");
                if (Check && string.IsNullOrWhiteSpace(CheckNumber))
                    str.Add("You must enter the check number.");
                decimal tended = (Tended == null) ? 0 : ParseDecimalValue(Tended.ToString());
                if (Cash && Due > Tended)
                    str.Add("Tended must be equal or greater than Due.");
            }

            //ToDo Review
            int ticketNumberCurrent = ParseIntValue(TicketNumberOverrideText) ?? 0;

            if (ticketNumberCurrent <= 0)
            {
                var messageValidateTicketNumberCurrentAtOrLessThanZero = $"The new ticket number should not be lower than the current ticket number of '{ticketNumberCurrent}'";
                str.Add(messageValidateTicketNumberCurrentAtOrLessThanZero);
                _log.Info(messageValidateTicketNumberCurrentAtOrLessThanZero);
            }

            bool isTicketExist = IoC.Get<ITicketService>().IsTicketExist(ticketNumberCurrent, Ticket.TicketId, _seasonId);
            int nextTicketNumber = IoC.Get<ITicketService>().GetNextTicketNumber(Ticket.CompanyId.Value);

            if (isTicketExist)
            {
                string messageValidateTicketNumberExistsCurrentNumberNextNumber = $"The ticket number '{ticketNumberCurrent}' already exists.  Changing number to next in sequence: '{nextTicketNumber}'";
                str.Add(messageValidateTicketNumberExistsCurrentNumberNextNumber);
                ticketNumberCurrent = nextTicketNumber;
                Ticket.TicketNumber = nextTicketNumber;

                _log.Info(messageValidateTicketNumberExistsCurrentNumberNextNumber);
            }

            if (ticketNumberCurrent != Ticket.TicketNumber)
            {
                if (ticketNumberCurrent < Ticket.TicketNumber && nextTicketNumber <= 0)
                {
                    string messageValidateTicketNumberLower = "The new ticket number should not be lower than the current ticket number.";
                    str.Add(messageValidateTicketNumberLower);
                    _log.Info(messageValidateTicketNumberLower);
                }

                if (nextTicketNumber > 0 && nextTicketNumber > ticketNumberCurrent)
                {
                    string messageValidateTicketNumberHigher = "The new ticket number should not be higher than the current ticket number.";
                    str.Add(messageValidateTicketNumberHigher);
                    _log.Info(messageValidateTicketNumberHigher);
                }
            }

            if (SelectedCompany == null
                || SelectedCompany.FirstTicket > ticketNumberCurrent
                || SelectedCompany.LastTicket < ticketNumberCurrent)
            {
                string messageValidateTicketNumberOutsideOfDesignatedRange = $"The Ticket number is outside the company range of ticket numbers ({SelectedCompany.FirstTicket} - {SelectedCompany.LastTicket})";
                str.Add(messageValidateTicketNumberOutsideOfDesignatedRange);
                _log.Info(messageValidateTicketNumberOutsideOfDesignatedRange);

            }
            TicketNumberOverrideText = ticketNumberCurrent.ToString();
            if (str.Any())
            {
                Errors = string.Format(
                    "Validation Failed.{0}{1}", Environment.NewLine,
                    string.Join("\n", str));

                ShowMessage("Validation Errors", Errors, NotificationType.Warning);

                return false;
            }

            return true;
        }



        private void InitCashOption()
        {
            EnableTended = true;
            EnableCheckNumber = false;
            CheckBackground = Colors.White;
            CheckNumber = string.Empty;
            //Tended = 0;//.ToString("N2");
            Tended = null;
            IsAmountTendedFocused = true;
            ReCalculateCash();
        }

        private void InitCreditCardOption()
        {
            EnableTended = false;
            EnableCheckNumber = false;
            CheckBackground = Colors.White;
            CheckNumber = string.Empty;
            Tended = Due;//.ToString("N2");
        }

        private void InitCheckOption()
        {
            EnableTended = false;
            EnableCheckNumber = true;
            CheckBackground = Colors.LightYellow;
            Tended = Due;//.ToString("N2");
        }

        private void InitBilledFromLowellOption()
        {
            EnableTended = false;
            EnableCheckNumber = false;
            CheckBackground = Colors.White;
            Tended = Due;
        }

        private static decimal TruncateDecimals(decimal value)
        {
            // return decimal.Truncate(value * 100) / 100;
            return decimal.Round(value, 2);
        }

        public void KeyHandle(Key key)
        {
            if (key == Key.Enter && IsPrintingEnabled == true)
            {
                PrintTicket();
            }
        }
    }
}