﻿#pragma checksum "..\..\..\..\Views\ShellView.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "4A6EDA60082429B35C3EC5A1DFC1EBC64BC7EE7A"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Caliburn.Micro;
using RootLibrary.WPF.Localization;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using Xceed.Wpf.Toolkit;
using Xceed.Wpf.Toolkit.Chromes;
using Xceed.Wpf.Toolkit.Core.Converters;
using Xceed.Wpf.Toolkit.Core.Input;
using Xceed.Wpf.Toolkit.Core.Media;
using Xceed.Wpf.Toolkit.Core.Utilities;
using Xceed.Wpf.Toolkit.Panels;
using Xceed.Wpf.Toolkit.Primitives;
using Xceed.Wpf.Toolkit.PropertyGrid;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;
using Xceed.Wpf.Toolkit.PropertyGrid.Commands;
using Xceed.Wpf.Toolkit.PropertyGrid.Converters;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;
using Xceed.Wpf.Toolkit.Zoombox;
using ZTech.Helpers;


namespace ZTech.Views {
    
    
    /// <summary>
    /// ShellView
    /// </summary>
    public partial class ShellView : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 24 "..\..\..\..\Views\ShellView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem Dispatcher;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\..\..\Views\ShellView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem CustomerService;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\..\..\Views\ShellView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem CashCustomer;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\..\..\Views\ShellView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem Reports;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\..\..\Views\ShellView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem _SeasonEstimate;
        
        #line default
        #line hidden
        
        
        #line 57 "..\..\..\..\Views\ShellView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem Materials;
        
        #line default
        #line hidden
        
        
        #line 63 "..\..\..\..\Views\ShellView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem Phonebook;
        
        #line default
        #line hidden
        
        
        #line 70 "..\..\..\..\Views\ShellView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem DailyOperations;
        
        #line default
        #line hidden
        
        
        #line 79 "..\..\..\..\Views\ShellView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem Scales;
        
        #line default
        #line hidden
        
        
        #line 85 "..\..\..\..\Views\ShellView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem ChangeSite;
        
        #line default
        #line hidden
        
        
        #line 92 "..\..\..\..\Views\ShellView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem ChangeSeason;
        
        #line default
        #line hidden
        
        
        #line 99 "..\..\..\..\Views\ShellView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem GreatPlainsOption;
        
        #line default
        #line hidden
        
        
        #line 102 "..\..\..\..\Views\ShellView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem TicketExport;
        
        #line default
        #line hidden
        
        
        #line 106 "..\..\..\..\Views\ShellView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem SeasonEstimate;
        
        #line default
        #line hidden
        
        
        #line 112 "..\..\..\..\Views\ShellView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem Administration;
        
        #line default
        #line hidden
        
        
        #line 118 "..\..\..\..\Views\ShellView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem ExportCustomers;
        
        #line default
        #line hidden
        
        
        #line 125 "..\..\..\..\Views\ShellView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem Exit;
        
        #line default
        #line hidden
        
        
        #line 151 "..\..\..\..\Views\ShellView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label TonsShippedToday;
        
        #line default
        #line hidden
        
        
        #line 173 "..\..\..\..\Views\ShellView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtWeighMaster;
        
        #line default
        #line hidden
        
        
        #line 176 "..\..\..\..\Views\ShellView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button LogOut;
        
        #line default
        #line hidden
        
        
        #line 184 "..\..\..\..\Views\ShellView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button LogIn;
        
        #line default
        #line hidden
        
        
        #line 212 "..\..\..\..\Views\ShellView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtShortScaleName;
        
        #line default
        #line hidden
        
        
        #line 215 "..\..\..\..\Views\ShellView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtSelectedSeason_Prefix;
        
        #line default
        #line hidden
        
        
        #line 219 "..\..\..\..\Views\ShellView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtShortPcName;
        
        #line default
        #line hidden
        
        
        #line 222 "..\..\..\..\Views\ShellView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtShortSiteName;
        
        #line default
        #line hidden
        
        
        #line 226 "..\..\..\..\Views\ShellView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtShortServerName;
        
        #line default
        #line hidden
        
        
        #line 231 "..\..\..\..\Views\ShellView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtShortVersionName;
        
        #line default
        #line hidden
        
        
        #line 235 "..\..\..\..\Views\ShellView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ContentControl ActiveItem;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/ZTech;component/views/shellview.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\Views\ShellView.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.Dispatcher = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 2:
            this.CustomerService = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 3:
            this.CashCustomer = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 4:
            this.Reports = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 5:
            this._SeasonEstimate = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 6:
            this.Materials = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 7:
            this.Phonebook = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 8:
            this.DailyOperations = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 9:
            this.Scales = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 10:
            this.ChangeSite = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 11:
            this.ChangeSeason = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 12:
            this.GreatPlainsOption = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 13:
            this.TicketExport = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 14:
            this.SeasonEstimate = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 15:
            this.Administration = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 16:
            this.ExportCustomers = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 17:
            this.Exit = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 18:
            this.TonsShippedToday = ((System.Windows.Controls.Label)(target));
            return;
            case 19:
            this.txtWeighMaster = ((System.Windows.Controls.TextBox)(target));
            return;
            case 20:
            this.LogOut = ((System.Windows.Controls.Button)(target));
            return;
            case 21:
            this.LogIn = ((System.Windows.Controls.Button)(target));
            return;
            case 22:
            this.txtShortScaleName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 23:
            this.txtSelectedSeason_Prefix = ((System.Windows.Controls.TextBox)(target));
            return;
            case 24:
            this.txtShortPcName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 25:
            this.txtShortSiteName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 26:
            this.txtShortServerName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 27:
            this.txtShortVersionName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 28:
            this.ActiveItem = ((System.Windows.Controls.ContentControl)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

