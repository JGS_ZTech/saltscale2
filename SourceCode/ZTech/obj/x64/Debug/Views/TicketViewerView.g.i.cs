﻿#pragma checksum "..\..\..\..\Views\TicketViewerView.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "38A1B20068DA55A20603F2076A74E267"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Caliburn.Micro;
using RootLibrary.WPF.Localization;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using ZTech.Helpers;
using ZTech.ViewModels;


namespace ZTech.Views {
    
    
    /// <summary>
    /// TicketViewerView
    /// </summary>
    public partial class TicketViewerView : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 33 "..\..\..\..\Views\TicketViewerView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TicketsToday;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\..\..\Views\TicketViewerView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TotalTickets;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\..\..\Views\TicketViewerView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TotalTons;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\..\..\Views\TicketViewerView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TicketsTodayForSelectedOrder;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\..\..\Views\TicketViewerView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TotalTicketsForSelectedOrder;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\..\..\Views\TicketViewerView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TotalTonsForSelectedOrder;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\..\..\Views\TicketViewerView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TicketsSelectedDayForSelectedOrder;
        
        #line default
        #line hidden
        
        
        #line 70 "..\..\..\..\Views\TicketViewerView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TotalTicketsForSelectedOrderSelectedDay;
        
        #line default
        #line hidden
        
        
        #line 79 "..\..\..\..\Views\TicketViewerView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TotalTonsForSelectedOrderSelectedDay;
        
        #line default
        #line hidden
        
        
        #line 87 "..\..\..\..\Views\TicketViewerView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid TicketList;
        
        #line default
        #line hidden
        
        
        #line 148 "..\..\..\..\Views\TicketViewerView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Done;
        
        #line default
        #line hidden
        
        
        #line 152 "..\..\..\..\Views\TicketViewerView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Order;
        
        #line default
        #line hidden
        
        
        #line 165 "..\..\..\..\Views\TicketViewerView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Customer;
        
        #line default
        #line hidden
        
        
        #line 171 "..\..\..\..\Views\TicketViewerView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Void;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/ZTech;component/views/ticketviewerview.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\Views\TicketViewerView.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.TicketsToday = ((System.Windows.Controls.TextBox)(target));
            return;
            case 2:
            this.TotalTickets = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.TotalTons = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.TicketsTodayForSelectedOrder = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.TotalTicketsForSelectedOrder = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.TotalTonsForSelectedOrder = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.TicketsSelectedDayForSelectedOrder = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.TotalTicketsForSelectedOrderSelectedDay = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.TotalTonsForSelectedOrderSelectedDay = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.TicketList = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 11:
            this.Done = ((System.Windows.Controls.Button)(target));
            return;
            case 12:
            this.Order = ((System.Windows.Controls.Button)(target));
            return;
            case 13:
            this.Customer = ((System.Windows.Controls.Button)(target));
            return;
            case 14:
            this.Void = ((System.Windows.Controls.Button)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

