﻿#pragma checksum "..\..\..\Views\CustomerServiceView.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "7CB2704410AB8BCACD01791C0ACBE8C2"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Caliburn.Micro;
using RootLibrary.WPF.Localization;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using ZTech.Helpers;


namespace ZTech.Views {
    
    
    /// <summary>
    /// CustomerServiceView
    /// </summary>
    public partial class CustomerServiceView : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 25 "..\..\..\Views\CustomerServiceView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid CustomerList;
        
        #line default
        #line hidden
        
        
        #line 77 "..\..\..\Views\CustomerServiceView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton EntityCustomers;
        
        #line default
        #line hidden
        
        
        #line 78 "..\..\..\Views\CustomerServiceView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton ShippingCustomers;
        
        #line default
        #line hidden
        
        
        #line 79 "..\..\..\Views\CustomerServiceView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton AllCustomers;
        
        #line default
        #line hidden
        
        
        #line 80 "..\..\..\Views\CustomerServiceView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button TicketsCustomer;
        
        #line default
        #line hidden
        
        
        #line 81 "..\..\..\Views\CustomerServiceView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button NewCustomer;
        
        #line default
        #line hidden
        
        
        #line 82 "..\..\..\Views\CustomerServiceView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button EditCustomer;
        
        #line default
        #line hidden
        
        
        #line 83 "..\..\..\Views\CustomerServiceView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox SearchCustomerWord;
        
        #line default
        #line hidden
        
        
        #line 89 "..\..\..\Views\CustomerServiceView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button SearchCustomer;
        
        #line default
        #line hidden
        
        
        #line 90 "..\..\..\Views\CustomerServiceView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button RefreshCustomer;
        
        #line default
        #line hidden
        
        
        #line 95 "..\..\..\Views\CustomerServiceView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DockPanel OrdersDockPanel;
        
        #line default
        #line hidden
        
        
        #line 111 "..\..\..\Views\CustomerServiceView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid OrderList;
        
        #line default
        #line hidden
        
        
        #line 178 "..\..\..\Views\CustomerServiceView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button OrderTickets;
        
        #line default
        #line hidden
        
        
        #line 179 "..\..\..\Views\CustomerServiceView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button NewOrder;
        
        #line default
        #line hidden
        
        
        #line 184 "..\..\..\Views\CustomerServiceView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button EditOrder;
        
        #line default
        #line hidden
        
        
        #line 185 "..\..\..\Views\CustomerServiceView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox SearchOrderWord;
        
        #line default
        #line hidden
        
        
        #line 187 "..\..\..\Views\CustomerServiceView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button SearchOrder;
        
        #line default
        #line hidden
        
        
        #line 188 "..\..\..\Views\CustomerServiceView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button RefreshOrder;
        
        #line default
        #line hidden
        
        
        #line 215 "..\..\..\Views\CustomerServiceView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Customer_EntityAddress;
        
        #line default
        #line hidden
        
        
        #line 217 "..\..\..\Views\CustomerServiceView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Customer_PrimaryContactInfo;
        
        #line default
        #line hidden
        
        
        #line 220 "..\..\..\Views\CustomerServiceView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid PurchaseOrderList;
        
        #line default
        #line hidden
        
        
        #line 289 "..\..\..\Views\CustomerServiceView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button NewPurchaseOrder;
        
        #line default
        #line hidden
        
        
        #line 293 "..\..\..\Views\CustomerServiceView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button EditPurchaseOrder;
        
        #line default
        #line hidden
        
        
        #line 294 "..\..\..\Views\CustomerServiceView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox SearchPurchaseOrderWord;
        
        #line default
        #line hidden
        
        
        #line 295 "..\..\..\Views\CustomerServiceView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button SearchPurchaseOrder;
        
        #line default
        #line hidden
        
        
        #line 296 "..\..\..\Views\CustomerServiceView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button RefreshPurchaseOrder;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/ZTech;component/views/customerserviceview.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Views\CustomerServiceView.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.CustomerList = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 2:
            this.EntityCustomers = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 3:
            this.ShippingCustomers = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 4:
            this.AllCustomers = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 5:
            this.TicketsCustomer = ((System.Windows.Controls.Button)(target));
            return;
            case 6:
            this.NewCustomer = ((System.Windows.Controls.Button)(target));
            return;
            case 7:
            this.EditCustomer = ((System.Windows.Controls.Button)(target));
            return;
            case 8:
            this.SearchCustomerWord = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.SearchCustomer = ((System.Windows.Controls.Button)(target));
            return;
            case 10:
            this.RefreshCustomer = ((System.Windows.Controls.Button)(target));
            return;
            case 11:
            this.OrdersDockPanel = ((System.Windows.Controls.DockPanel)(target));
            return;
            case 12:
            this.OrderList = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 13:
            this.OrderTickets = ((System.Windows.Controls.Button)(target));
            return;
            case 14:
            this.NewOrder = ((System.Windows.Controls.Button)(target));
            return;
            case 15:
            this.EditOrder = ((System.Windows.Controls.Button)(target));
            return;
            case 16:
            this.SearchOrderWord = ((System.Windows.Controls.TextBox)(target));
            return;
            case 17:
            this.SearchOrder = ((System.Windows.Controls.Button)(target));
            return;
            case 18:
            this.RefreshOrder = ((System.Windows.Controls.Button)(target));
            return;
            case 19:
            this.Customer_EntityAddress = ((System.Windows.Controls.TextBox)(target));
            return;
            case 20:
            this.Customer_PrimaryContactInfo = ((System.Windows.Controls.TextBox)(target));
            return;
            case 21:
            this.PurchaseOrderList = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 22:
            this.NewPurchaseOrder = ((System.Windows.Controls.Button)(target));
            return;
            case 23:
            this.EditPurchaseOrder = ((System.Windows.Controls.Button)(target));
            return;
            case 24:
            this.SearchPurchaseOrderWord = ((System.Windows.Controls.TextBox)(target));
            return;
            case 25:
            this.SearchPurchaseOrder = ((System.Windows.Controls.Button)(target));
            return;
            case 26:
            this.RefreshPurchaseOrder = ((System.Windows.Controls.Button)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

