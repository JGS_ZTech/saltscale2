﻿#pragma checksum "..\..\..\Views\EditTruckView.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "6DB7C8EDD110B804DACFA968F283E5C3"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Caliburn.Micro;
using RootLibrary.WPF.Localization;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using Xceed.Wpf.Toolkit;
using Xceed.Wpf.Toolkit.Chromes;
using Xceed.Wpf.Toolkit.Core.Converters;
using Xceed.Wpf.Toolkit.Core.Input;
using Xceed.Wpf.Toolkit.Core.Media;
using Xceed.Wpf.Toolkit.Core.Utilities;
using Xceed.Wpf.Toolkit.Panels;
using Xceed.Wpf.Toolkit.Primitives;
using Xceed.Wpf.Toolkit.PropertyGrid;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;
using Xceed.Wpf.Toolkit.PropertyGrid.Commands;
using Xceed.Wpf.Toolkit.PropertyGrid.Converters;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;
using Xceed.Wpf.Toolkit.Zoombox;
using ZTech.Controls;
using ZTech.Helpers;


namespace ZTech.Views {
    
    
    /// <summary>
    /// EditTruckView
    /// </summary>
    public partial class EditTruckView : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 57 "..\..\..\Views\EditTruckView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal ZTech.Helpers.AutoCompleteComboBox TruckOwners;
        
        #line default
        #line hidden
        
        
        #line 77 "..\..\..\Views\EditTruckView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button NewOwner;
        
        #line default
        #line hidden
        
        
        #line 78 "..\..\..\Views\EditTruckView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button EditOwner;
        
        #line default
        #line hidden
        
        
        #line 108 "..\..\..\Views\EditTruckView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Truck_ShortName;
        
        #line default
        #line hidden
        
        
        #line 110 "..\..\..\Views\EditTruckView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Truck_Sticker;
        
        #line default
        #line hidden
        
        
        #line 112 "..\..\..\Views\EditTruckView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Truck_Tag;
        
        #line default
        #line hidden
        
        
        #line 114 "..\..\..\Views\EditTruckView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Truck_Description;
        
        #line default
        #line hidden
        
        
        #line 118 "..\..\..\Views\EditTruckView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Truck_Weight;
        
        #line default
        #line hidden
        
        
        #line 124 "..\..\..\Views\EditTruckView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ChangeWeight;
        
        #line default
        #line hidden
        
        
        #line 127 "..\..\..\Views\EditTruckView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Truck_CubicYards;
        
        #line default
        #line hidden
        
        
        #line 132 "..\..\..\Views\EditTruckView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox Truck_NeedsTareOverride;
        
        #line default
        #line hidden
        
        
        #line 135 "..\..\..\Views\EditTruckView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Truck_TareInterval;
        
        #line default
        #line hidden
        
        
        #line 143 "..\..\..\Views\EditTruckView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox DateTimeOfWeight;
        
        #line default
        #line hidden
        
        
        #line 146 "..\..\..\Views\EditTruckView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Truck_PhoneNumber;
        
        #line default
        #line hidden
        
        
        #line 157 "..\..\..\Views\EditTruckView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Done;
        
        #line default
        #line hidden
        
        
        #line 158 "..\..\..\Views\EditTruckView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button DeleteTruck;
        
        #line default
        #line hidden
        
        
        #line 159 "..\..\..\Views\EditTruckView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Cancel;
        
        #line default
        #line hidden
        
        
        #line 161 "..\..\..\Views\EditTruckView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox CreatedDate;
        
        #line default
        #line hidden
        
        
        #line 163 "..\..\..\Views\EditTruckView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox UpdatedDate;
        
        #line default
        #line hidden
        
        
        #line 165 "..\..\..\Views\EditTruckView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ContentControl ScaleWidget;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/ZTech;component/views/edittruckview.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Views\EditTruckView.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.TruckOwners = ((ZTech.Helpers.AutoCompleteComboBox)(target));
            return;
            case 2:
            this.NewOwner = ((System.Windows.Controls.Button)(target));
            return;
            case 3:
            this.EditOwner = ((System.Windows.Controls.Button)(target));
            return;
            case 4:
            this.Truck_ShortName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.Truck_Sticker = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.Truck_Tag = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.Truck_Description = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.Truck_Weight = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.ChangeWeight = ((System.Windows.Controls.Button)(target));
            return;
            case 10:
            this.Truck_CubicYards = ((System.Windows.Controls.TextBox)(target));
            return;
            case 11:
            this.Truck_NeedsTareOverride = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 12:
            this.Truck_TareInterval = ((System.Windows.Controls.TextBox)(target));
            return;
            case 13:
            this.DateTimeOfWeight = ((System.Windows.Controls.TextBox)(target));
            return;
            case 14:
            this.Truck_PhoneNumber = ((System.Windows.Controls.TextBox)(target));
            return;
            case 15:
            this.Done = ((System.Windows.Controls.Button)(target));
            return;
            case 16:
            this.DeleteTruck = ((System.Windows.Controls.Button)(target));
            return;
            case 17:
            this.Cancel = ((System.Windows.Controls.Button)(target));
            return;
            case 18:
            this.CreatedDate = ((System.Windows.Controls.TextBox)(target));
            return;
            case 19:
            this.UpdatedDate = ((System.Windows.Controls.TextBox)(target));
            return;
            case 20:
            this.ScaleWidget = ((System.Windows.Controls.ContentControl)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

