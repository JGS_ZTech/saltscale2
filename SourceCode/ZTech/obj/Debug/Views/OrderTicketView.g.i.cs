﻿#pragma checksum "..\..\..\Views\OrderTicketView.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "36F0DB96D56A84F3502388457442E9CA"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Caliburn.Micro;
using RootLibrary.WPF.Localization;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using Xceed.Wpf.Toolkit;
using Xceed.Wpf.Toolkit.Chromes;
using Xceed.Wpf.Toolkit.Core.Converters;
using Xceed.Wpf.Toolkit.Core.Input;
using Xceed.Wpf.Toolkit.Core.Media;
using Xceed.Wpf.Toolkit.Core.Utilities;
using Xceed.Wpf.Toolkit.Panels;
using Xceed.Wpf.Toolkit.Primitives;
using Xceed.Wpf.Toolkit.PropertyGrid;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;
using Xceed.Wpf.Toolkit.PropertyGrid.Commands;
using Xceed.Wpf.Toolkit.PropertyGrid.Converters;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;
using Xceed.Wpf.Toolkit.Zoombox;
using ZTech.Controls;
using ZTech.Helpers;


namespace ZTech.Views {
    
    
    /// <summary>
    /// OrderTicketView
    /// </summary>
    public partial class OrderTicketView : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 53 "..\..\..\Views\OrderTicketView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TruckDescription;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\..\Views\OrderTicketView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox FirstWeight;
        
        #line default
        #line hidden
        
        
        #line 75 "..\..\..\Views\OrderTicketView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox SecondWeight;
        
        #line default
        #line hidden
        
        
        #line 96 "..\..\..\Views\OrderTicketView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox NetWeight;
        
        #line default
        #line hidden
        
        
        #line 100 "..\..\..\Views\OrderTicketView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox NetWeightTon;
        
        #line default
        #line hidden
        
        
        #line 106 "..\..\..\Views\OrderTicketView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox MaterialList;
        
        #line default
        #line hidden
        
        
        #line 109 "..\..\..\Views\OrderTicketView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock ConfirmationNumber;
        
        #line default
        #line hidden
        
        
        #line 117 "..\..\..\Views\OrderTicketView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox BalanceToShip;
        
        #line default
        #line hidden
        
        
        #line 129 "..\..\..\Views\OrderTicketView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox CustomerOrganization;
        
        #line default
        #line hidden
        
        
        #line 130 "..\..\..\Views\OrderTicketView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox CustomerAddress;
        
        #line default
        #line hidden
        
        
        #line 134 "..\..\..\Views\OrderTicketView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox SpecialInstruction;
        
        #line default
        #line hidden
        
        
        #line 168 "..\..\..\Views\OrderTicketView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox CashRate;
        
        #line default
        #line hidden
        
        
        #line 178 "..\..\..\Views\OrderTicketView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox MaterialCharge;
        
        #line default
        #line hidden
        
        
        #line 183 "..\..\..\Views\OrderTicketView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox IsExempt;
        
        #line default
        #line hidden
        
        
        #line 184 "..\..\..\Views\OrderTicketView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock TaxRateLabel;
        
        #line default
        #line hidden
        
        
        #line 186 "..\..\..\Views\OrderTicketView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Tax;
        
        #line default
        #line hidden
        
        
        #line 192 "..\..\..\Views\OrderTicketView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Due;
        
        #line default
        #line hidden
        
        
        #line 197 "..\..\..\Views\OrderTicketView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Tended;
        
        #line default
        #line hidden
        
        
        #line 208 "..\..\..\Views\OrderTicketView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton Cash;
        
        #line default
        #line hidden
        
        
        #line 209 "..\..\..\Views\OrderTicketView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton CreditCard;
        
        #line default
        #line hidden
        
        
        #line 210 "..\..\..\Views\OrderTicketView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton Check;
        
        #line default
        #line hidden
        
        
        #line 212 "..\..\..\Views\OrderTicketView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox CheckNumber;
        
        #line default
        #line hidden
        
        
        #line 219 "..\..\..\Views\OrderTicketView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Change;
        
        #line default
        #line hidden
        
        
        #line 220 "..\..\..\Views\OrderTicketView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox HideDollarAmount;
        
        #line default
        #line hidden
        
        
        #line 227 "..\..\..\Views\OrderTicketView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ContentControl ScaleWidget;
        
        #line default
        #line hidden
        
        
        #line 238 "..\..\..\Views\OrderTicketView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button PrintTicket;
        
        #line default
        #line hidden
        
        
        #line 239 "..\..\..\Views\OrderTicketView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button PreviewTicket;
        
        #line default
        #line hidden
        
        
        #line 240 "..\..\..\Views\OrderTicketView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Override;
        
        #line default
        #line hidden
        
        
        #line 242 "..\..\..\Views\OrderTicketView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TicketNumber;
        
        #line default
        #line hidden
        
        
        #line 249 "..\..\..\Views\OrderTicketView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox CompanyList;
        
        #line default
        #line hidden
        
        
        #line 251 "..\..\..\Views\OrderTicketView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Cancel;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/ZTech;component/views/orderticketview.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Views\OrderTicketView.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.TruckDescription = ((System.Windows.Controls.TextBox)(target));
            return;
            case 2:
            this.FirstWeight = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.SecondWeight = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.NetWeight = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.NetWeightTon = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.MaterialList = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 7:
            this.ConfirmationNumber = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 8:
            this.BalanceToShip = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.CustomerOrganization = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.CustomerAddress = ((System.Windows.Controls.TextBox)(target));
            return;
            case 11:
            this.SpecialInstruction = ((System.Windows.Controls.TextBox)(target));
            return;
            case 12:
            this.CashRate = ((System.Windows.Controls.TextBox)(target));
            return;
            case 13:
            this.MaterialCharge = ((System.Windows.Controls.TextBox)(target));
            return;
            case 14:
            this.IsExempt = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 15:
            this.TaxRateLabel = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 16:
            this.Tax = ((System.Windows.Controls.TextBox)(target));
            return;
            case 17:
            this.Due = ((System.Windows.Controls.TextBox)(target));
            return;
            case 18:
            this.Tended = ((System.Windows.Controls.TextBox)(target));
            return;
            case 19:
            this.Cash = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 20:
            this.CreditCard = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 21:
            this.Check = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 22:
            this.CheckNumber = ((System.Windows.Controls.TextBox)(target));
            return;
            case 23:
            this.Change = ((System.Windows.Controls.TextBox)(target));
            return;
            case 24:
            this.HideDollarAmount = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 25:
            this.ScaleWidget = ((System.Windows.Controls.ContentControl)(target));
            return;
            case 26:
            this.PrintTicket = ((System.Windows.Controls.Button)(target));
            return;
            case 27:
            this.PreviewTicket = ((System.Windows.Controls.Button)(target));
            return;
            case 28:
            this.Override = ((System.Windows.Controls.Button)(target));
            return;
            case 29:
            this.TicketNumber = ((System.Windows.Controls.TextBox)(target));
            return;
            case 30:
            this.CompanyList = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 31:
            this.Cancel = ((System.Windows.Controls.Button)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

