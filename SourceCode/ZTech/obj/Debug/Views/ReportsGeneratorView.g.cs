﻿#pragma checksum "..\..\..\Views\ReportsGeneratorView.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "FE07382AFF902C53130E419224D82F8C"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Caliburn.Micro;
using Contract.Domain;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using Xceed.Wpf.Toolkit;
using Xceed.Wpf.Toolkit.Chromes;
using Xceed.Wpf.Toolkit.Core.Converters;
using Xceed.Wpf.Toolkit.Core.Input;
using Xceed.Wpf.Toolkit.Core.Media;
using Xceed.Wpf.Toolkit.Core.Utilities;
using Xceed.Wpf.Toolkit.Panels;
using Xceed.Wpf.Toolkit.Primitives;
using Xceed.Wpf.Toolkit.PropertyGrid;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;
using Xceed.Wpf.Toolkit.PropertyGrid.Commands;
using Xceed.Wpf.Toolkit.PropertyGrid.Converters;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;
using Xceed.Wpf.Toolkit.Zoombox;
using ZTech.Controls;
using ZTech.Helpers;
using ZTech.Reports;
using ZTech.ViewModels;


namespace ZTech.Views {
    
    
    /// <summary>
    /// ReportGeneratorView
    /// </summary>
    public partial class ReportGeneratorView : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 89 "..\..\..\Views\ReportsGeneratorView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.WatermarkComboBox ReportTypeList;
        
        #line default
        #line hidden
        
        
        #line 101 "..\..\..\Views\ReportsGeneratorView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox ReportConfigurationList;
        
        #line default
        #line hidden
        
        
        #line 109 "..\..\..\Views\ReportsGeneratorView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Save;
        
        #line default
        #line hidden
        
        
        #line 130 "..\..\..\Views\ReportsGeneratorView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.WatermarkComboBox DateRangeList;
        
        #line default
        #line hidden
        
        
        #line 143 "..\..\..\Views\ReportsGeneratorView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.DateTimePicker RangeFromDate;
        
        #line default
        #line hidden
        
        
        #line 151 "..\..\..\Views\ReportsGeneratorView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.DateTimePicker RangeToDate;
        
        #line default
        #line hidden
        
        
        #line 266 "..\..\..\Views\ReportsGeneratorView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.WatermarkComboBox CustomerTypeList;
        
        #line default
        #line hidden
        
        
        #line 289 "..\..\..\Views\ReportsGeneratorView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox IsVoidsIncluded;
        
        #line default
        #line hidden
        
        
        #line 325 "..\..\..\Views\ReportsGeneratorView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.WatermarkComboBox DetailSummaryList;
        
        #line default
        #line hidden
        
        
        #line 342 "..\..\..\Views\ReportsGeneratorView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.WatermarkComboBox ReportSortByList;
        
        #line default
        #line hidden
        
        
        #line 368 "..\..\..\Views\ReportsGeneratorView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Print;
        
        #line default
        #line hidden
        
        
        #line 374 "..\..\..\Views\ReportsGeneratorView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Preview;
        
        #line default
        #line hidden
        
        
        #line 381 "..\..\..\Views\ReportsGeneratorView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Cancel;
        
        #line default
        #line hidden
        
        
        #line 382 "..\..\..\Views\ReportsGeneratorView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button SaveBottom;
        
        #line default
        #line hidden
        
        
        #line 383 "..\..\..\Views\ReportsGeneratorView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Delete;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/ZTech;component/views/reportsgeneratorview.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Views\ReportsGeneratorView.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.ReportTypeList = ((Xceed.Wpf.Toolkit.WatermarkComboBox)(target));
            return;
            case 2:
            this.ReportConfigurationList = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 3:
            this.Save = ((System.Windows.Controls.Button)(target));
            return;
            case 4:
            this.DateRangeList = ((Xceed.Wpf.Toolkit.WatermarkComboBox)(target));
            return;
            case 5:
            this.RangeFromDate = ((Xceed.Wpf.Toolkit.DateTimePicker)(target));
            return;
            case 6:
            this.RangeToDate = ((Xceed.Wpf.Toolkit.DateTimePicker)(target));
            return;
            case 7:
            this.CustomerTypeList = ((Xceed.Wpf.Toolkit.WatermarkComboBox)(target));
            return;
            case 8:
            this.IsVoidsIncluded = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 9:
            this.DetailSummaryList = ((Xceed.Wpf.Toolkit.WatermarkComboBox)(target));
            return;
            case 10:
            this.ReportSortByList = ((Xceed.Wpf.Toolkit.WatermarkComboBox)(target));
            return;
            case 11:
            this.Print = ((System.Windows.Controls.Button)(target));
            return;
            case 12:
            this.Preview = ((System.Windows.Controls.Button)(target));
            return;
            case 13:
            this.Cancel = ((System.Windows.Controls.Button)(target));
            return;
            case 14:
            this.SaveBottom = ((System.Windows.Controls.Button)(target));
            return;
            case 15:
            this.Delete = ((System.Windows.Controls.Button)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

