﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Data;
using Contract.Domain;

namespace ZTech.Reports
{

    public class CustomerTypeList
    {
        public CustomerTypeEnum CustomerTypeEnum { get; set; }
    }

    public class CustomerTypeEnumToString : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (string.IsNullOrEmpty(value.ToString()))  // This is for databinding
                return null;
            return (StringToEnum<CustomerTypeEnum>(value.ToString())).GetDescription(); // <-- The extention method
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (string.IsNullOrEmpty(value.ToString())) // This is for databinding
                return null;
            return StringToEnum<CustomerTypeEnum>(value.ToString());
        }

        public static T StringToEnum<T>(string name)
        {
            return (T)Enum.Parse(typeof(T), name);
        }

        #endregion
    }
}

