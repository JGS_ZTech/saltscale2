﻿using System.ComponentModel;
using System.Windows;

namespace ZTech.Reports
{
    public partial class PropertyFilterGroupView
    {
        public PropertyFilterGroupView()
        {
            InitializeComponent();
        }

        public ICollectionView CollectionView
        {
            get { return (ICollectionView)GetValue(CollectionViewProperty); }
            set { SetValue(CollectionViewProperty, value); }
        }

        public static readonly DependencyProperty CollectionViewProperty =
            DependencyProperty.Register(
            "CollectionView",
            typeof(ICollectionView),
            typeof(PropertyFilterGroupView),
            new UIPropertyMetadata(null, ApplyCollectionViewToGroup));

        public PropertyFilterGroup FilterGroup
        {
            get { return (PropertyFilterGroup)GetValue(FilterGroupProperty); }
            set { SetValue(FilterGroupProperty, value); }
        }

        public static readonly DependencyProperty FilterGroupProperty =
            DependencyProperty.Register(
            "FilterGroup",
            typeof(PropertyFilterGroup),
            typeof(PropertyFilterGroupView),
            new UIPropertyMetadata(null, ApplyCollectionViewToGroup));

        static void ApplyCollectionViewToGroup(DependencyObject depObj, DependencyPropertyChangedEventArgs e)
        {
            PropertyFilterGroupView view = depObj as PropertyFilterGroupView;
            if (view == null || view.FilterGroup == null || view.CollectionView == null)
                return;

            view.FilterGroup.DataSourceView = view.CollectionView;
        }

    }
}