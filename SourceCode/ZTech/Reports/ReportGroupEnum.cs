﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Data;
using Contract.Domain;

namespace ZTech.Reports
{
    public class ReportSortByEnumList
    {
        public ReportSortByEnum SortByEnum { get; set; }
    }

    public class ReportSortByEnumToString : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (string.IsNullOrEmpty(value.ToString()))  // This is for databinding
                return null;
            return (StringToEnum<ReportSortByEnum>(value.ToString())).GetDescription(); // <-- The extention method
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (string.IsNullOrEmpty(value.ToString())) // This is for databinding
                return null;
            return StringToEnum<ReportSortByEnum>(value.ToString());
        }

        public static T StringToEnum<T>(string name)
        {
            return (T)Enum.Parse(typeof(T), name);
        }

        #endregion
    }
}

