﻿using System.Windows;
using Caliburn.Micro;

namespace ZTech
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            ConventionManager.ApplyValidation = (binding, viewModelType, property) =>
            {
                binding.ValidatesOnExceptions = true;
            };
        }
    }
}