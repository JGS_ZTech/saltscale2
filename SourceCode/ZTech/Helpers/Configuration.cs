﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Windows;
using Contract.Domain;
using Contract.Services;

namespace ZTech.Helpers
{
    public class Configuration : IConfiguration
    {
        private string _connectionString;

        private SaltScaleConfigurationSection _config
        {
            get { return (SaltScaleConfigurationSection) ConfigurationManager.GetSection("saltScaleConfiguration"); }
        }

        public string SaltScaleLocalComputerSettingsFilePath
        {
            get { return _config.LocalComputerSettingsFilePath; }
        }
        public string saltScaleReplicationHostServer
        {
            get { return _config.LocalComputerSettingsReplicationHostServer; }
        }
        public string DefaultScaleNameLocalComputer { get; set; }
        public string DefaultPrinterNameLocalComputer { get; set; }
        //public string DefaultMaterialName { get; set; }
        public string DefaultSiteName { get; set; }
        //LocalComputerSettingsFilePath

        public Configuration()
        {
        }
        
        public string DefaultScaleName
        {
            //get { return _config.DefaultScaleName; }
            get
            {
                return (string.IsNullOrWhiteSpace(this.DefaultScaleNameLocalComputer)) ? _config.DefaultScaleName : this.DefaultScaleNameLocalComputer;
            }
        }

        public string SelectedScaleName { get; set; }
        public ISiteConfig SelectedSite { get; set; }

        public int OrdersRefreshTimeout
        {
            get { return _config.OrdersRefreshTimeout; }
        }
        public bool IsManualMode { get; set; }

        private class SiteConf : ISiteConfig
        {
            public string Name { get; set; }
            public string ConnectionString { get; set; }
            public string ConnectionStringDatasource { get; set; }
            public string ConnectionStringInitialCatalog { get; set; }
            public string ConnectionStringUserId { get; set; }
            public string ConnectionStringPassword { get; set; }
            public bool ConnectionStringIntegratedSecurity { get; set; }
            public bool IsDefault { get; set; }
            public bool IsHeadOffice { get; set; }
        }

        public ISiteConfig Site
        {
            get
            {
                for (int i = 0; i < _config.SiteConfig.Count; i++)
                {
                    if (_config.SiteConfig[i].Name.Trim() == this.DefaultSiteName.Trim())
                    {
                        var sqlConnectionStringBuilder = new SqlConnectionStringBuilder();

                        if (_config.SiteConfig[i].ConnectionString != null)
                            sqlConnectionStringBuilder = new SqlConnectionStringBuilder(_config.SiteConfig[i].ConnectionString);

                        return new SiteConf()
                        {
                            Name = _config.SiteConfig[i].Name,
                            ConnectionString = _config.SiteConfig[i].ConnectionString,
                            ConnectionStringDatasource = sqlConnectionStringBuilder.DataSource,
                            ConnectionStringInitialCatalog = sqlConnectionStringBuilder.InitialCatalog,
                            ConnectionStringUserId = sqlConnectionStringBuilder.UserID,
                            ConnectionStringPassword = sqlConnectionStringBuilder.Password,
                            ConnectionStringIntegratedSecurity = sqlConnectionStringBuilder.IntegratedSecurity,
                            IsDefault = true,
                            IsHeadOffice = _config.SiteConfig[i].IsHeadOffice,
                        };
                    }
                }

                return null;
            }
        }

        public string ConnectionString
        {
            get { return _connectionString; }
            set
            {
                _connectionString = ConvertConnectionString(value); 
            }
        }

        public List<ISiteConfig> ExistingSites
        {
            get
            {
                var result = new List<ISiteConfig>();
                var sqlConnectionStringBuilder = new SqlConnectionStringBuilder();

                for (int i = 0; i < _config.SiteConfig.Count; i++)
                {
                    if (string.IsNullOrWhiteSpace(_config.SiteConfig[i].Name))
                        continue;

                    if (_config.SiteConfig[i].ConnectionString != null)
                        sqlConnectionStringBuilder = new SqlConnectionStringBuilder(_config.SiteConfig[i].ConnectionString);


                    result.Add(new SiteConf
                    {
                        Name = _config.SiteConfig[i].Name,
                        ConnectionString = _config.SiteConfig[i].ConnectionString,
                        ConnectionStringDatasource = sqlConnectionStringBuilder.DataSource,
                        ConnectionStringInitialCatalog = sqlConnectionStringBuilder.InitialCatalog,
                        ConnectionStringUserId = sqlConnectionStringBuilder.UserID,
                        ConnectionStringPassword = sqlConnectionStringBuilder.Password,
                        ConnectionStringIntegratedSecurity = sqlConnectionStringBuilder.IntegratedSecurity,
                        //IsDefault = _config.SiteConfig[i].IsDefault,
                        IsDefault = (_config.SiteConfig[i].Name.Trim() == this.DefaultSiteName.Trim()),
                        IsHeadOffice = _config.SiteConfig[i].IsHeadOffice,
                    });
                }

                return result;
            }
        }

        public int Season { get { return (int) Application.Current.Properties["SeasonId"]; } }
        public DateTime LastOrderUpdate { get; set; }

        public string DispatchOrderGridSortColumn { get; set; }

        public string ConvertConnectionString(string connectionString)
        {
            const string appName = "EntityFramework";
            const string providerName = "System.Data.SqlClient";

            var sqlBuilder = new SqlConnectionStringBuilder(connectionString);
            sqlBuilder.ApplicationName = appName;

            var efBuilder = new EntityConnectionStringBuilder();
            efBuilder.Metadata = "res://*/DataModel.csdl|res://*/DataModel.ssdl|res://*/DataModel.msl";
            efBuilder.Provider = providerName;
            efBuilder.ProviderConnectionString = sqlBuilder.ConnectionString;

            return efBuilder.ConnectionString;
        }

        public void UpdateDefaultScale(IScaleInfo scale, bool isManual)
        {
            System.Configuration.Configuration cfg = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            var section = cfg.GetSection("saltScaleConfiguration") as SaltScaleConfigurationSection;
            if (section != null)
            {
                section.DefaultScaleName = scale.ScaleName;
                //section.ScaleType = isManual ? "Manual" : "Standard";
                section.SectionInformation.ForceSave=true;
                cfg.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("saltScaleConfiguration");
            }

            IsManualMode = isManual;

            SelectedScaleName = scale.ScaleName;
        }


    }

    public enum ScaleType
    {
        Indicator,
        Manual
    }
    
}