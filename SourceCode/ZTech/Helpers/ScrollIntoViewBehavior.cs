﻿using System;
using System.Windows.Controls;
using System.Windows.Interactivity;

namespace ZTech.Helpers
{
    public class ScrollIntoViewBehavior : Behavior<DataGrid>
    {
        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.SelectionChanged += AssociatedObject_SelectionChanged;
        }
        void AssociatedObject_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender is DataGrid)
            {
                var grid = (sender as DataGrid);
                if (grid.SelectedItem != null)
                {
                    grid.Dispatcher.BeginInvoke(new Action(delegate
                    {
                        grid.UpdateLayout();
                        if (grid.SelectedItem != null) grid.ScrollIntoView(grid.SelectedItem, null);
                    }));
                }
            }
        }
        protected override void OnDetaching()
        {
            base.OnDetaching();
            AssociatedObject.SelectionChanged -=
                AssociatedObject_SelectionChanged;
        }
    }

    public class ScrollIntoViewBehaviorSfDataGrid : Behavior<Syncfusion.UI.Xaml.Grid.SfDataGrid>
    {
        void AssociatedObject_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender is Syncfusion.UI.Xaml.Grid.SfDataGrid)
            {
                var grid = (sender as Syncfusion.UI.Xaml.Grid.SfDataGrid);
                if (grid.SelectedItem != null)
                {
                    grid.Dispatcher.BeginInvoke(new Action(delegate
                    {
                        grid.UpdateLayout();
                        Syncfusion.UI.Xaml.ScrollAxis.RowColumnIndex x = new Syncfusion.UI.Xaml.ScrollAxis.RowColumnIndex();
                        x.RowIndex = grid.SelectedIndex;

                        if (grid.SelectedItem != null) grid.ScrollInView(x);
                    }));
                }
            }
        }

    }

    public class ScrollIntoListBehavior : Behavior<ListBox>
    {
        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.SelectionChanged += AssociatedObject_SelectionChanged;
        }
        void AssociatedObject_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender is ListBox)
            {
                var grid = (sender as ListBox);
                if (grid.SelectedItem != null)
                {
                    grid.Dispatcher.BeginInvoke(new Action(delegate
                    {
                        grid.UpdateLayout();
                        grid.ScrollIntoView(grid.SelectedItem);
                    }));
                }
            }
        }
        protected override void OnDetaching()
        {
            base.OnDetaching();
            AssociatedObject.SelectionChanged -=
                AssociatedObject_SelectionChanged;
        }
    }
}