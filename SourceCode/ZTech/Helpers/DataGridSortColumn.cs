﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Interactivity;

namespace ZTech.Helpers
{
    public class DataGridSortColumn : Behavior<Syncfusion.UI.Xaml.Grid.SfDataGrid>
    {

        public DataGridSortColumn(object sender)
        {
            if (sender is Syncfusion.UI.Xaml.Grid.SfDataGrid)
            {
                var grid = (sender as Syncfusion.UI.Xaml.Grid.SfDataGrid);
                if (grid.SelectedItem != null)
                {
                    grid.Dispatcher.BeginInvoke(new Action(delegate
                    {
                        grid.UpdateLayout();
                        Syncfusion.UI.Xaml.ScrollAxis.RowColumnIndex x = new Syncfusion.UI.Xaml.ScrollAxis.RowColumnIndex();
                        x.RowIndex = grid.SelectedIndex;

                        if (grid.SelectedItem != null) grid.ScrollInView(x);
                    }));
                }
            }
        }

    }
}
