﻿using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;

namespace ZTech.Helpers
{
    public class IntegerTextBoxBehavior : Behavior<TextBox>
    {
        /// <summary>
        ///     Called when attached to a TextBox.
        /// </summary>
        protected override void OnAttached()
        {
            AssociatedObject.PreviewTextInput += OnPreviewTextInput;
            DataObject.AddPastingHandler(AssociatedObject, OnClipboardPaste);
        }

        /// <summary>
        ///     This method handles paste and drag/drop events onto the TextBox.  It restricts the character
        ///     set and ensures we have consistent behavior.
        /// </summary>
        /// <param name="sender">TextBox sender</param>
        /// <param name="e">EventArgs</param>
        private void OnClipboardPaste(object sender, DataObjectPastingEventArgs e)
        {
            var text = e.SourceDataObject.GetData(e.FormatToApply) as string;

            if (!string.IsNullOrEmpty(text) && !Validate(text))
                e.CancelCommand();
        }

        /// <summary>
        ///     This checks if the resulting string will match the regex expression
        /// </summary>
        private void OnPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!Validate(e.Text))
                e.Handled = true;
        }


        private bool Validate(string newContent)
        {
            string testString;

            // replace selection with new text.
            if (!string.IsNullOrEmpty(AssociatedObject.SelectedText))
            {
                string pre = AssociatedObject.Text.Substring(0, AssociatedObject.SelectionStart);
                string after =
                    AssociatedObject.Text.Substring(AssociatedObject.SelectionStart + AssociatedObject.SelectionLength,
                        AssociatedObject.Text.Length -
                        (AssociatedObject.SelectionStart + AssociatedObject.SelectionLength));
                testString = pre + newContent + after;
            }
            else
            {
                string pre = AssociatedObject.Text.Substring(0, AssociatedObject.CaretIndex);
                string after = AssociatedObject.Text.Substring(AssociatedObject.CaretIndex,
                    AssociatedObject.Text.Length - AssociatedObject.CaretIndex);
                testString = pre + newContent + after;
            }

            var regExpr = new Regex(@"^(\d*)$");
            if (regExpr.IsMatch(testString))
                return true;

            return false;
        }
    } 
    public class IntegerWithCommasTextBoxBehavior : Behavior<TextBox>
    {
        /// <summary>
        ///     Called when attached to a TextBox.
        /// </summary>
        protected override void OnAttached()
        {
            AssociatedObject.PreviewTextInput += OnPreviewTextInput;
            DataObject.AddPastingHandler(AssociatedObject, OnClipboardPaste);
            
        }

        /// <summary>
        ///     This method handles paste and drag/drop events onto the TextBox.  It restricts the character
        ///     set and ensures we have consistent behavior.
        /// </summary>
        /// <param name="sender">TextBox sender</param>
        /// <param name="e">EventArgs</param>
        private void OnClipboardPaste(object sender, DataObjectPastingEventArgs e)
        {
            var text = e.SourceDataObject.GetData(e.FormatToApply) as string;

            if (!string.IsNullOrEmpty(text) && !Validate(text))
                e.CancelCommand();
        }

        /// <summary>
        ///     This checks if the resulting string will match the regex expression
        /// </summary>
        private void OnPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!Validate(e.Text))
                e.Handled = true;
        }


        private bool Validate(string newContent)
        {
            string testString;

            // replace selection with new text.
            if (!string.IsNullOrEmpty(AssociatedObject.SelectedText))
            {
                string pre = AssociatedObject.Text.Substring(0, AssociatedObject.SelectionStart);
                string after =
                    AssociatedObject.Text.Substring(AssociatedObject.SelectionStart + AssociatedObject.SelectionLength,
                        AssociatedObject.Text.Length -
                        (AssociatedObject.SelectionStart + AssociatedObject.SelectionLength));
                testString = pre + newContent + after;
            }
            else
            {
                string pre = AssociatedObject.Text.Substring(0, AssociatedObject.CaretIndex);
                string after = AssociatedObject.Text.Substring(AssociatedObject.CaretIndex,
                    AssociatedObject.Text.Length - AssociatedObject.CaretIndex);
                testString = pre + newContent + after;
            }

            testString = testString.Replace(",", string.Empty);

            var regExpr = new Regex(@"^(\d*)$");
            if (regExpr.IsMatch(testString))
                return true;

            return false;
        }
    }
}