﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;
using System.Windows.Media;
using Color = System.Drawing.Color;

namespace ZTech.Helpers
{
    public class InvertedBoolToVisibiltyConverter : MarkupExtension, IValueConverter
    {
        public InvertedBoolToVisibiltyConverter()
        {

        }
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var result = value as bool?;
            return (result.HasValue && result.Value)
                ? Visibility.Collapsed
                : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    } 

    public class BoolToVisibiltyConverter : MarkupExtension, IValueConverter
    {
        public BoolToVisibiltyConverter()
        {
            
        }
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var result = value as bool?;
            return (result.HasValue && result.Value)
                ? Visibility.Visible
                : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    } 

    public class StringToVisibiltyConverter : MarkupExtension, IValueConverter
    {
        public StringToVisibiltyConverter()
        {
            
        }
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return string.IsNullOrEmpty(value as string)
                ? Visibility.Collapsed
                : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }

    public class ColorToBackgroundConverter : MarkupExtension, IValueConverter
    {
        public ColorToBackgroundConverter()
        {

        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var color = (System.Windows.Media.Color)value;
            return new SolidColorBrush(color);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }

    public class FloatToBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            float result; 

            float.TryParse(value.ToString(), out result);
            
            if(result < 0)
                return Brushes.Red;

            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }

    public class IsEqualOrLessThanConverter : IValueConverter
    {
        public static readonly IValueConverter Instance = new IsEqualOrLessThanConverter();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            decimal intValue = decimal.Parse(value.ToString());
            decimal compareToValue = decimal.Parse(parameter.ToString());

            return intValue <= compareToValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}


