﻿using System;
using System.Globalization;
using System.Reflection;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;

namespace ZTech.Helpers
{
    public class DataGridRowHeight : MarkupExtension, IValueConverter
    {
        public DataGridRowHeight()
        {
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {

            var input = (value as Syncfusion.UI.Xaml.Grid.SfDataGrid);

            if (input == null)
            {
                return 30;
            }
            else
            {
                return 55;
            }

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}