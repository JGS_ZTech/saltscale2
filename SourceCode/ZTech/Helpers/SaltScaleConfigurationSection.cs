using System;
using System.Configuration;

namespace ZTech.Helpers
{
    public class SaltScaleConfigurationSection : ConfigurationSection
    {

        [ConfigurationProperty("saltScaleLocalComputerSettingsFilePath", IsRequired = true)]
        public string LocalComputerSettingsFilePath
        {
            get { return (string)this["saltScaleLocalComputerSettingsFilePath"]; }
        }

        [ConfigurationProperty("saltScaleReplicationHostServer", IsRequired = true)]
        public string LocalComputerSettingsReplicationHostServer
        {
            get { return (string)this["saltScaleReplicationHostServer"]; }
        }

        [ConfigurationProperty("defaultScaleName", IsRequired = false)]
        [StringValidator(InvalidCharacters = "~!@#$%^&*()[]{}/;'\"|\\", MinLength = 0, MaxLength = 60)]
        public string DefaultScaleName
        {
            get { return (string) this["defaultScaleName"]; }
            set { this["defaultScaleName"] = value; }
        }

        [ConfigurationProperty("ordersRefreshTimeout", DefaultValue = 120, IsRequired = false)]
        public int OrdersRefreshTimeout
        {
            get { return (int)this["ordersRefreshTimeout"]; }
            set { this["ordersRefreshTimeout"] = value; }
        }

        [ConfigurationProperty("sites", IsDefaultCollection = false)]
        [ConfigurationCollection(typeof (SiteConfigCollection),
            AddItemName = "add",
            ClearItemsName = "clear",
            RemoveItemName = "remove")]
        public SiteConfigCollection SiteConfig
        {
            get
            {
                SiteConfigCollection siteConfigCollection = (SiteConfigCollection) base["sites"];

                return siteConfigCollection;
            }
        }

        [ConfigurationProperty("defaultScaleNameLocalComputer", IsRequired = false)]
        public string DefaultScaleNameLocalComputer
        {
            get { return (string)this["defaultScaleNameLocalComputer"]; }
            set { this["defaultScaleNameLocalComputer"] = value; }
        }

        [ConfigurationProperty("defaultPrinterNameLocalComputer", IsRequired = false)]
        public string DefaultPrinterNameLocalComputer
        {
            get { return (string)this["defaultPrinterNameLocalComputer"]; }
            set { this["defaultPrinterNameLocalComputer"] = value; }
        }


        public class SiteConfigCollection : ConfigurationElementCollection
        {
            public SiteConfigCollection()
            {
                SiteConfigElement site = (SiteConfigElement) CreateNewElement();
                Add(site);
            }

            public override ConfigurationElementCollectionType CollectionType
            {
                get { return ConfigurationElementCollectionType.AddRemoveClearMap; }
            }

            protected override ConfigurationElement CreateNewElement()
            {
                return new SiteConfigElement();
            }

            protected override Object GetElementKey(ConfigurationElement element)
            {
                return ((SiteConfigElement) element).Name;
            }

            public SiteConfigElement this[int index]
            {
                get { return (SiteConfigElement) BaseGet(index); }
                set
                {
                    if (BaseGet(index) != null)
                    {
                        BaseRemoveAt(index);
                    }
                    BaseAdd(index, value);
                }
            }

            public new SiteConfigElement this[string name]
            {
                get { return (SiteConfigElement) BaseGet(name); }
            }

            public int IndexOf(SiteConfigElement site)
            {
                return BaseIndexOf(site);
            }

            public void Add(SiteConfigElement site)
            {
                BaseAdd(site);
            }

            protected override void BaseAdd(ConfigurationElement element)
            {
                BaseAdd(element, false);
            }

            public void Remove(SiteConfigElement site)
            {
                if (BaseIndexOf(site) >= 0)
                    BaseRemove(site.Name);
            }

            public void RemoveAt(int index)
            {
                BaseRemoveAt(index);
            }

            public void Remove(string name)
            {
                BaseRemove(name);
            }

            public void Clear()
            {
                BaseClear();
            }
        }

        public class SiteConfigElement : ConfigurationElement
        {
            public SiteConfigElement(String name, String connectionString, bool defaultSite, bool isHeadOffice)
            {
                Name = name;
                ConnectionString = connectionString;
                IsDefault = defaultSite;
                IsHeadOffice = isHeadOffice;
            }

            public SiteConfigElement()
            {
            }

            [ConfigurationProperty("name", IsRequired = true, IsKey = true)]
            public string Name
            {
                get { return (string) this["name"]; }
                set { this["name"] = value; }
            }

            [ConfigurationProperty("connectionString")]
            public string ConnectionString
            {
                get { return (string) this["connectionString"]; }
                set { this["connectionString"] = value; }
            }

            [ConfigurationProperty("isDefault")]
            public bool IsDefault
            {
                get { return (bool) this["isDefault"]; }
                set { this["isDefault"] = value; }
            }

            [ConfigurationProperty("isHeadOffice")]
            public bool IsHeadOffice
            {
                get { return (bool) this["isHeadOffice"]; }
                set { this["isHeadOffice"] = value; }
            }
        }
    }
    

    
}