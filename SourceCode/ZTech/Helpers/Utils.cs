﻿using System;
using System.Collections.Generic;
using System.Linq;
using Contract.Domain;

namespace ZTech.Helpers
{
    public static class StringUtility
    {
        public static string TruncateTo(this string text, int value)
        {
            return string.IsNullOrEmpty(text) ? string.Empty : text.Substring(0, Math.Min(text.Length, value));
        }
    }

    public static class WeightUtility
    {
        public static int GetWeightInPounds(this int weight, ScaleIndicatorStreamUnitOfWeight unitOfWeight)
        {
            int result;

            switch (unitOfWeight)
            {
                case ScaleIndicatorStreamUnitOfWeight.K:
                    result=(int) (weight * 2.20462);
                    break;
                case ScaleIndicatorStreamUnitOfWeight.T:
                    result = weight*2000;
                    break;
                case ScaleIndicatorStreamUnitOfWeight.G:
                    result=(int) (weight * 0.00220462);
                    break;
                case ScaleIndicatorStreamUnitOfWeight.O:
                    result = (int) (weight * 0.0625);
                    break;
                case ScaleIndicatorStreamUnitOfWeight.None:
                case ScaleIndicatorStreamUnitOfWeight.L:
                default:
                    result = weight;
                    break;
            }

            return result;
        }
    }

    public static class ExceptionUtility
    {
        public static IEnumerable<TSource> FromHierarchy<TSource>(
            this TSource source,
            Func<TSource, TSource> nextItem,
            Func<TSource, bool> canContinue)
        {
            for (var current = source; canContinue(current); current = nextItem(current))
            {
                yield return current;
            }
        }

        public static IEnumerable<TSource> FromHierarchy<TSource>(
            this TSource source,
            Func<TSource, TSource> nextItem)
            where TSource : class
        {
            return FromHierarchy(source, nextItem, s => s != null);
        }

        public static string GetAllMessages(this Exception exception)
        {
            var messages = exception.FromHierarchy(ex => ex.InnerException)
                .Select(ex => ex.Message);
            return String.Join(Environment.NewLine, messages);
        }
    }

    public static class ProgramInformationUtility
    {
        public static string GetPublishedVersion()
        {
            if (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed)
            {
                return System.Deployment.Application.ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString();
            }
            return "0.0.0.0";
        }
    }

}