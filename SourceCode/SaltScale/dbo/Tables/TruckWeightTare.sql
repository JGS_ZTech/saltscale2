﻿CREATE TABLE [dbo].[TruckWeightTare] (
    [TruckWeightTare]  INT           IDENTITY (1, 1) NOT NULL,
    [DateTimeOfWeight] DATETIME      NULL,
    [TareWeight]       INT           NOT NULL,
    [TruckId]          INT           NOT NULL,
    [CreateTime]       DATETIME      CONSTRAINT [DF_TruckWeightTare_CreateTime] DEFAULT (getdate()) NOT NULL,
    [UpdateTime]       DATETIME      NULL,
    [IsActive]         BIT           CONSTRAINT [DF_TruckWeightTare_IsActive] DEFAULT ((1)) NOT NULL,
    [Comment]          VARCHAR (255) NULL,
    [DataA]            VARCHAR (255) NULL,
    [DataB]            VARCHAR (255) NULL,
    [DataC]            VARCHAR (255) NULL,
    CONSTRAINT [PK_TruckWeightTare] PRIMARY KEY CLUSTERED ([TruckWeightTare] ASC),
    CONSTRAINT [FK_TruckWeightTare_Truck] FOREIGN KEY ([TruckId]) REFERENCES [dbo].[Truck] ([TruckID])
);

