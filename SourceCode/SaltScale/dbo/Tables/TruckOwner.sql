﻿CREATE TABLE [dbo].[TruckOwner] (
    [TruckOwnerID] INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ShortName]    VARCHAR (10)  NOT NULL,
    [CreateTime]   DATETIME      CONSTRAINT [DF_TruckOwner_CreateTime] DEFAULT (getdate()) NOT NULL,
    [UpdateTime]   DATETIME      NULL,
    [IsActive]     BIT           CONSTRAINT [DF_TruckOwner_IsActive] DEFAULT ((1)) NOT NULL,
    [Comment]      VARCHAR (255) NULL,
    [DataA]        VARCHAR (255) NULL,
    [DataB]        VARCHAR (255) NULL,
    [DataC]        VARCHAR (255) NULL,
    [SeasonId]     INT           NOT NULL,
    CONSTRAINT [PK_TruckOwner] PRIMARY KEY CLUSTERED ([TruckOwnerID] ASC),
    CONSTRAINT [FK_TruckOwner_Season] FOREIGN KEY ([SeasonId]) REFERENCES [dbo].[Season] ([SeasonId])
);

