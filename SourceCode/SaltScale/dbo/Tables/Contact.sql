﻿CREATE TABLE [dbo].[Contact] (
    [ContactID]      INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Organization]   VARCHAR (255)  NULL,
    [Name]           VARCHAR (255)  NULL,
    [AddressID]      INT            NOT NULL,
    [isPrimary]      BIT            NOT NULL,
    [ContactComment] VARCHAR (2000) NULL,
    [CreateTime]     DATETIME       CONSTRAINT [DF_Contact_CreateTime] DEFAULT (getdate()) NOT NULL,
    [UpdateTime]     DATETIME       NULL,
    [IsActive]       BIT            CONSTRAINT [DF_Contact_IsActive] DEFAULT ((1)) NOT NULL,
    [Comment]        VARCHAR (255)  NULL,
    [DataA]          VARCHAR (255)  NULL,
    [DataB]          VARCHAR (255)  NULL,
    [DataC]          VARCHAR (255)  NULL,
    [SeasonId]       INT            NOT NULL,
    CONSTRAINT [PK_Contact] PRIMARY KEY CLUSTERED ([ContactID] ASC),
    CONSTRAINT [FK_Contact_Address] FOREIGN KEY ([AddressID]) REFERENCES [dbo].[Address] ([AddressID]),
    CONSTRAINT [FK_Contact_Season] FOREIGN KEY ([SeasonId]) REFERENCES [dbo].[Season] ([SeasonId])
);

