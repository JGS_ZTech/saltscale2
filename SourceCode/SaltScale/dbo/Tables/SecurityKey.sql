﻿CREATE TABLE [dbo].[SecurityKey] (
    [Id]          INT           IDENTITY (1, 1) NOT NULL,
    [SecurityKey] NVARCHAR (20) NOT NULL,
    CONSTRAINT [PK_SecurityKey] PRIMARY KEY CLUSTERED ([Id] ASC)
);

