﻿CREATE TABLE [dbo].[Season] (
    [SeasonId]  INT         IDENTITY (1, 1) NOT NULL,
    [Prefix]    VARCHAR (5) NOT NULL,
    [StartDate] DATE        NOT NULL,
    [EndDate]   DATE        NOT NULL,
    [IsActive]  BIT         NOT NULL,
    CONSTRAINT [PK_Season] PRIMARY KEY CLUSTERED ([SeasonId] ASC)
);

