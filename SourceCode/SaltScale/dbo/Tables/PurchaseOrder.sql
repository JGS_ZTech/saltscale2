﻿CREATE TABLE [dbo].[PurchaseOrder] (
    [PurchaseOrderID]     INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [CustomerID]          INT           NOT NULL,
    [PurchaseOrderNumber] VARCHAR (80)  NOT NULL,
    [Quantity]            INT           NOT NULL,
    [MaterialID]          INT           NOT NULL,
    [isComplete]          BIT           NOT NULL,
    [isHeld]              BIT           NOT NULL,
    [effectiveDate]       DATETIME      NOT NULL,
    [CreateTime]          DATETIME      CONSTRAINT [DF_PurchaseOrder_CreateTime] DEFAULT (getdate()) NOT NULL,
    [UpdateTime]          DATETIME      NULL,
    [IsActive]            BIT           CONSTRAINT [DF_PurchaseOrder_IsActive] DEFAULT ((1)) NOT NULL,
    [Comment]             VARCHAR (255) NULL,
    [DataA]               VARCHAR (255) NULL,
    [DataB]               VARCHAR (255) NULL,
    [DataC]               VARCHAR (255) NULL,
    [SeasonId]            INT           NOT NULL,
    CONSTRAINT [PK_PurchaseOrder] PRIMARY KEY CLUSTERED ([PurchaseOrderID] ASC),
    CONSTRAINT [FK_PurchaseOrder_Customer] FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[Customer] ([CustomerID]),
    CONSTRAINT [FK_PurchaseOrder_Material] FOREIGN KEY ([MaterialID]) REFERENCES [dbo].[Material] ([MaterialID]),
    CONSTRAINT [FK_PurchaseOrder_Season] FOREIGN KEY ([SeasonId]) REFERENCES [dbo].[Season] ([SeasonId])
);

