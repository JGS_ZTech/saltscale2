﻿CREATE TABLE [dbo].[TempNotExported] (
    [TicketID] INT NOT NULL,
    CONSTRAINT [PK_TempNotExporteda] PRIMARY KEY CLUSTERED ([TicketID] ASC)
);

