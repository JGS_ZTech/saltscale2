﻿CREATE TABLE [dbo].[TruckOwnerContact] (
    [TruckOwnerContactID] INT IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ContactID]           INT NULL,
    [TruckOwnerID]        INT NOT NULL,
    CONSTRAINT [PK_TruckOwnerContact] PRIMARY KEY CLUSTERED ([TruckOwnerContactID] ASC),
    CONSTRAINT [FK_TruckOwnerContact_Contact] FOREIGN KEY ([ContactID]) REFERENCES [dbo].[Contact] ([ContactID]),
    CONSTRAINT [FK_TruckOwnerContact_TruckOwner] FOREIGN KEY ([TruckOwnerID]) REFERENCES [dbo].[TruckOwner] ([TruckOwnerID])
);

