﻿CREATE TABLE [dbo].[Customize] (
    [CustomizeID] INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [TableName]   VARCHAR (50) NULL,
    [DataA]       VARCHAR (50) NULL,
    [DataB]       VARCHAR (50) NULL,
    [DataC]       VARCHAR (50) NULL,
    CONSTRAINT [PK_Customize] PRIMARY KEY CLUSTERED ([CustomizeID] ASC)
);

