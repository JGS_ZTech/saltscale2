﻿CREATE TABLE [dbo].[Address] (
    [AddressID]      INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [AddressLineOne] VARCHAR (80)  NULL,
    [AddressLineTwo] VARCHAR (80)  NULL,
    [City]           VARCHAR (80)  NULL,
    [State]          VARCHAR (2)   NULL,
    [Zip]            VARCHAR (10)  NULL,
    [CreateTime]     DATETIME      CONSTRAINT [DF_Address_CreateTime] DEFAULT (getdate()) NOT NULL,
    [UpdateTime]     DATETIME      NULL,
    [IsActive]       BIT           CONSTRAINT [DF_Address_IsActive] DEFAULT ((1)) NOT NULL,
    [Comment]        VARCHAR (255) NULL,
    [DataA]          VARCHAR (255) NULL,
    [DataB]          VARCHAR (255) NULL,
    [DataC]          VARCHAR (255) NULL,
    [SeasonId]       INT           NOT NULL,
    CONSTRAINT [PK_Address] PRIMARY KEY CLUSTERED ([AddressID] ASC),
    CONSTRAINT [FK_Address_Season] FOREIGN KEY ([SeasonId]) REFERENCES [dbo].[Season] ([SeasonId])
);

