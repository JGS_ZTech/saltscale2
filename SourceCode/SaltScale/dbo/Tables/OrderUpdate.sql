﻿CREATE TABLE [dbo].[OrderUpdate] (
    [OrderUpdateID] INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [OrderID]       INT           NOT NULL,
    [Quantity]      INT           NOT NULL,
    [CreateTime]    DATETIME      CONSTRAINT [DF_OrderUpdate_CreateTime] DEFAULT (getdate()) NOT NULL,
    [UpdateTime]    DATETIME      NULL,
    [IsActive]      BIT           CONSTRAINT [DF_OrderUpdate_IsActive] DEFAULT ((1)) NOT NULL,
    [Comment]       VARCHAR (255) NULL,
    [DataA]         VARCHAR (255) NULL,
    [DataB]         VARCHAR (255) NULL,
    [DataC]         VARCHAR (255) NULL,
    CONSTRAINT [PK_OrderUpdate] PRIMARY KEY CLUSTERED ([OrderUpdateID] ASC),
    CONSTRAINT [FK_OrderUpdate_OrderTable] FOREIGN KEY ([OrderID]) REFERENCES [dbo].[OrderTable] ([OrderID])
);

