﻿CREATE TABLE [dbo].[ticketProps] (
    [LastID]      INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Name]        VARCHAR (50) NULL,
    [ValueNumber] INT          NULL,
    CONSTRAINT [PK_ticketProps] PRIMARY KEY CLUSTERED ([LastID] ASC)
);

