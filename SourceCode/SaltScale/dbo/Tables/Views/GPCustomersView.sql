﻿
CREATE VIEW [dbo].[GPCustomersView]
AS
SELECT DISTINCT 
                         dbo.Customer.CustomerID, Customer_Entity.ShortName AS Entity, Customer_Entity.DispatcherName AS EntityDispatcherName, 
                         dbo.Customer.ShortName AS Customer, dbo.Customer.DispatcherName AS CustomerDispatchName, dbo.Customer.GPID, dbo.Customer.SeasonId
FROM            dbo.Customer LEFT OUTER JOIN
                         dbo.Customer AS Customer_Entity ON dbo.Customer.ReportingEntityID = Customer_Entity.CustomerID

