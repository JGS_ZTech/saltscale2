﻿CREATE TABLE [dbo].[Material] (
    [MaterialID]    INT             IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ShortName]     VARCHAR (10)    NULL,
    [Description]   VARCHAR (255)   NULL,
    [PricePerTon]   DECIMAL (18, 2) NULL,
    [PricePerYard]  DECIMAL (18, 2) NULL,
    [PriceEach]     DECIMAL (18, 2) NULL,
    [MinimumCharge] INT             NULL,
    [CreateTime]    DATETIME        CONSTRAINT [DF_Material_CreateTime] DEFAULT (getdate()) NOT NULL,
    [UpdateTime]    DATETIME        NULL,
    [IsActive]      BIT             CONSTRAINT [DF_Material_IsActive] DEFAULT ((1)) NOT NULL,
    [Comment]       VARCHAR (255)   NULL,
    [DataA]         VARCHAR (255)   NULL,
    [DataB]         VARCHAR (255)   NULL,
    [DataC]         VARCHAR (255)   NULL,
    [SeasonId]      INT             NOT NULL,
    CONSTRAINT [PK_Material] PRIMARY KEY CLUSTERED ([MaterialID] ASC),
    CONSTRAINT [FK_Material_Season] FOREIGN KEY ([SeasonId]) REFERENCES [dbo].[Season] ([SeasonId])
);

