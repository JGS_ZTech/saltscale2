﻿CREATE TABLE [dbo].[GreatPlains] (
    [GreatPlainID] INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [CustomerID]   INT          NULL,
    [GPID]         VARCHAR (50) NULL,
    CONSTRAINT [PK_GreatPlains] PRIMARY KEY CLUSTERED ([GreatPlainID] ASC)
);

