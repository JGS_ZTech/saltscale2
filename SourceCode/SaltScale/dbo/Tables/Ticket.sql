﻿CREATE TABLE [dbo].[Ticket] (
    [TicketID]        INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [TicketNumber]    INT            NOT NULL,
    [Quantity]        REAL           NOT NULL,
    [isManual]        BIT            NOT NULL,
    [TruckID]         INT            NULL,
    [isVoid]          BIT            NOT NULL,
    [VoidReason]      VARCHAR (255)  NULL,
    [isReconciled]    BIT            NULL,
    [hasBeenExported] BIT            NULL,
    [CompanyID]       INT            NULL,
    [CreateTime]      DATETIME       CONSTRAINT [DF_Ticket_CreateTime] DEFAULT (getdate()) NOT NULL,
    [UpdateTime]      DATETIME       NULL,
    [IsActive]        BIT            CONSTRAINT [DF_Ticket_IsActive] DEFAULT ((1)) NOT NULL,
    [OrderId]         INT            NULL,
    [Comment]         VARCHAR (255)  NULL,
    [DataA]           VARCHAR (255)  NULL,
    [DataB]           VARCHAR (255)  NULL,
    [DataC]           VARCHAR (255)  NULL,
    [SeasonId]        INT            NOT NULL,
    [CashPurchaseId]  INT            NULL,
    [FuelSurcharge]   DECIMAL (3, 2) NULL,
    [DateExported]    DATETIME       NULL,
    CONSTRAINT [PK_Ticket] PRIMARY KEY CLUSTERED ([TicketID] ASC),
    CONSTRAINT [FK_Ticket_CashPurchase] FOREIGN KEY ([CashPurchaseId]) REFERENCES [dbo].[CashPurchase] ([cashPurchaseID]),
    CONSTRAINT [FK_Ticket_Company] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[Company] ([CompanyId]),
    CONSTRAINT [FK_Ticket_OrderTable] FOREIGN KEY ([OrderId]) REFERENCES [dbo].[OrderTable] ([OrderID]),
    CONSTRAINT [FK_Ticket_Season] FOREIGN KEY ([SeasonId]) REFERENCES [dbo].[Season] ([SeasonId]),
    CONSTRAINT [FK_Ticket_Truck] FOREIGN KEY ([TruckID]) REFERENCES [dbo].[Truck] ([TruckID])
);

