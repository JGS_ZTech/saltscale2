﻿CREATE TABLE [dbo].[Customer] (
    [CustomerID]          INT             IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ReportingEntityID]   INT             NULL,
    [DispatcherName]      VARCHAR (80)    NULL,
    [ShortName]           VARCHAR (10)    NOT NULL,
    [isShippingLocation]  BIT             NOT NULL,
    [specialInstructions] VARCHAR (2000)  NULL,
    [CreateTime]          DATETIME        CONSTRAINT [DF_Customer_CreateTime] DEFAULT (getdate()) NOT NULL,
    [UpdateTime]          DATETIME        NULL,
    [IsActive]            BIT             CONSTRAINT [DF_Customer_IsActive] DEFAULT ((1)) NOT NULL,
    [Comment]             VARCHAR (255)   NULL,
    [DataA]               VARCHAR (255)   NULL,
    [DataB]               VARCHAR (255)   NULL,
    [DataC]               VARCHAR (255)   NULL,
    [SeasonId]            INT             NOT NULL,
    [HideDollarAmounts]   BIT             DEFAULT ((0)) NOT NULL,
    [IsTaxExempt]         BIT             DEFAULT ((0)) NOT NULL,
    [CashMaterialRate]    DECIMAL (18, 2) NULL,
    [GPID]                VARCHAR (50)    NULL,
    CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED ([CustomerID] ASC),
    CONSTRAINT [FK_Customer_Season] FOREIGN KEY ([SeasonId]) REFERENCES [dbo].[Season] ([SeasonId])
);

