﻿CREATE TABLE [dbo].[Site] (
    [SiteId]                      INT             NOT NULL,
    [SiteName]                    VARCHAR (25)    NOT NULL,
    [IsOperatingFromThisLocation] BIT             NOT NULL,
    [SeasonId]                    INT             NOT NULL,
    [MaterialCost]                DECIMAL (18, 2) NULL,
    [WeightInterval]              INT             NULL,
    [TaxRate]                     DECIMAL (4, 2)  NULL,
    [DefaultCompanyId]            INT             NULL,
    [FuelSurcharge]               DECIMAL (3, 2)  NULL,
    [ConfNumberFirst]             INT             NULL,
    [ConfNumberLast]              INT             NULL,
    [ConfNumberNext]              INT             NULL,
    CONSTRAINT [PK_Site] PRIMARY KEY CLUSTERED ([SiteId] ASC),
    CONSTRAINT [FK_Site_Company] FOREIGN KEY ([DefaultCompanyId]) REFERENCES [dbo].[Company] ([CompanyId]),
    CONSTRAINT [FK_Site_Season] FOREIGN KEY ([SeasonId]) REFERENCES [dbo].[Season] ([SeasonId])
);

