﻿CREATE TABLE [dbo].[Company] (
    [CompanyId]   INT          NOT NULL,
    [FullName]    VARCHAR (50) NOT NULL,
    [PrinterPort] VARCHAR (50) NOT NULL,
    [AddressLineOne] VARCHAR(80) NULL, 
    [AddressLineTwo] VARCHAR(80) NULL, 
    [City] VARCHAR(80) NULL, 
    [State] VARCHAR(2) NULL, 
    [Zip] VARCHAR(10) NULL, 
    [PhoneNumberOne] VARCHAR(20) NULL, 
    [PhoneNumberTwo] VARCHAR(20) NULL, 
    [FaxNumber] VARCHAR(20) NULL, 
    CONSTRAINT [PK_Company] PRIMARY KEY CLUSTERED ([CompanyId] ASC)
);

