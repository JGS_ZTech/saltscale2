﻿CREATE TABLE [dbo].[ReportConfiguration] (
    [ID]            INT            IDENTITY (1, 1) NOT NULL,
    [Name]          VARCHAR (50)   NOT NULL,
    [Configuration] NVARCHAR (MAX) NULL,
    [SiteId]        INT            NOT NULL,
    [SeasonId]      INT            NOT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    FOREIGN KEY ([SeasonId]) REFERENCES [dbo].[Season] ([SeasonId]),
    FOREIGN KEY ([SiteId]) REFERENCES [dbo].[Site] ([SiteId])
);

