﻿CREATE TABLE [dbo].[configuration] (
    [configurationID] INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [name]            VARCHAR (50) NULL,
    [valuestring]     VARCHAR (50) NULL,
    CONSTRAINT [PK_configuration] PRIMARY KEY CLUSTERED ([configurationID] ASC)
);

