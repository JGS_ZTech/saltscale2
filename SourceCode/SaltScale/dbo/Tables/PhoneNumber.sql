﻿CREATE TABLE [dbo].[PhoneNumber] (
    [PhoneNumberID] INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ContactID]     INT           NOT NULL,
    [NumberType]    VARCHAR (80)  NULL,
    [PhoneNumber]   VARCHAR (50)  NULL,
    [isPrimary]     BIT           NOT NULL,
    [CreateTime]    DATETIME      CONSTRAINT [DF_PhoneNumber_CreateTime] DEFAULT (getdate()) NOT NULL,
    [UpdateTime]    DATETIME      NULL,
    [IsActive]      BIT           CONSTRAINT [DF_PhoneNumber_IsActive] DEFAULT ((1)) NOT NULL,
    [Comment]       VARCHAR (255) NULL,
    [DataA]         VARCHAR (255) NULL,
    [DataB]         VARCHAR (255) NULL,
    [DataC]         VARCHAR (255) NULL,
    [SeasonId]      INT           NOT NULL,
    CONSTRAINT [PK_PhoneNumber] PRIMARY KEY CLUSTERED ([PhoneNumberID] ASC),
    CONSTRAINT [FK_PhoneNumber_Contact] FOREIGN KEY ([ContactID]) REFERENCES [dbo].[Contact] ([ContactID]),
    CONSTRAINT [FK_PhoneNumber_Season] FOREIGN KEY ([SeasonId]) REFERENCES [dbo].[Season] ([SeasonId])
);

