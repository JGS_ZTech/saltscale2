﻿CREATE TABLE [dbo].[Synchronize] (
    [synchID]        INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [localCommonID]  INT          NULL,
    [remoteCommonID] INT          NULL,
    [remoteLocation] VARCHAR (50) NULL,
    CONSTRAINT [PK_Synchronize] PRIMARY KEY CLUSTERED ([synchID] ASC)
);

