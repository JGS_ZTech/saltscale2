﻿CREATE TABLE [dbo].[Truck] (
    [TruckID]           INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [TruckOwnerID]      INT           NULL,
    [ShortName]         VARCHAR (10)  NOT NULL,
    [Tag]               VARCHAR (80)  NULL,
    [Description]       VARCHAR (255) NULL,
    [TareInterval]      INT           NULL,
    [NeedsTareOverride] BIT           NOT NULL,
    [CubicYards]        INT           NULL,
    [Sticker]           VARCHAR (5)   NULL,
    [CreateTime]        DATETIME      CONSTRAINT [DF_Truck_CreateTime] DEFAULT (getdate()) NOT NULL,
    [UpdateTime]        DATETIME      NULL,
    [IsActive]          BIT           CONSTRAINT [DF_Truck_IsActive] DEFAULT ((1)) NOT NULL,
    [Comment]           VARCHAR (255) NULL,
    [DataA]             VARCHAR (255) NULL,
    [DataB]             VARCHAR (255) NULL,
    [DataC]             VARCHAR (255) NULL,
    [SeasonId]          INT           NOT NULL,
    CONSTRAINT [PK_Truck] PRIMARY KEY CLUSTERED ([TruckID] ASC),
    CONSTRAINT [FK_Truck_Season] FOREIGN KEY ([SeasonId]) REFERENCES [dbo].[Season] ([SeasonId]),
    CONSTRAINT [FK_Truck_TruckOwner] FOREIGN KEY ([TruckOwnerID]) REFERENCES [dbo].[TruckOwner] ([TruckOwnerID])
);

