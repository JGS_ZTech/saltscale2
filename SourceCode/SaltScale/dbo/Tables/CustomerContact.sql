﻿CREATE TABLE [dbo].[CustomerContact] (
    [CustomerContactID] INT IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ContactID]         INT NOT NULL,
    [CustomerID]        INT NOT NULL,
    CONSTRAINT [PK_CustomerContact] PRIMARY KEY CLUSTERED ([CustomerContactID] ASC),
    CONSTRAINT [FK_CustomerContact_Contact] FOREIGN KEY ([ContactID]) REFERENCES [dbo].[Contact] ([ContactID]),
    CONSTRAINT [FK_CustomerContact_Customer] FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[Customer] ([CustomerID])
);

