﻿

CREATE PROCEDURE [dbo].[sp_MoveToNewSeason] 
	@oldId int, @newSeasonName varchar(5)
AS
BEGIN
	SET NOCOUNT ON;

begin tran

declare @newSeasonId int;

update Season set IsActive=0,EndDate=GETDATE() where IsActive=1;

insert into Season (Prefix, StartDate, EndDate, IsActive)
values(
@newSeasonName, GETDATE(), DATEADD(year, 1, getdate()), 1);

select @newSeasonId = SeasonId  from Season where Prefix=@newSeasonName;

--material

INSERT INTO [Material]
           ([ShortName]
           ,[Description]
           ,[PricePerTon]
           ,[PricePerYard]
           ,[PriceEach]
           ,[MinimumCharge]
           ,[CreateTime]
           ,[UpdateTime]
           ,[IsActive]
           ,[Comment]
           ,[DataA]
           ,[DataB]
           ,[DataC]
           ,[SeasonId])
    SELECT [ShortName]
           ,[Description]
           ,[PricePerTon]
           ,[PricePerYard]
           ,[PriceEach]
           ,[MinimumCharge]
           ,GETDATE()
		   , GETDATE()
		   , 1
           ,[Comment]
           ,[DataA]
           ,[DataB]
           ,c1.MaterialID
           ,@newSeasonId
		   FROM Material c1 where c1.IsActive =1 and c1.SeasonId=@oldId;

--Site

INSERT INTO [Site]
           (SiteId,
		   [SiteName]
           ,[IsOperatingFromThisLocation]
           ,[SeasonId]
           ,[MaterialCost]
           ,[WeightInterval]
           ,[TaxRate]
           ,[DefaultCompanyId]
	   ,[FuelSurcharge]
	   ,[ConfNumberFirst]
           ,[ConfNumberLast]
           ,[ConfNumberNext]
)
   select (select max(SiteId)+1 from Site) ,SiteName, IsOperatingFromThisLocation, @newSeasonId, MaterialCost, WeightInterval, 
TaxRate, DefaultCompanyId, FuelSurcharge, ConfNumberFirst,ConfNumberLast,ConfNumberFirst from [Site] where SeasonId=@oldId;

--contacts
INSERT INTO Contact (Organization, Name, AddressID, isPrimary, ContactComment, CreateTime, UpdateTime, IsActive, Comment, DataA, DataB, DataC, SeasonId)
	SELECT c1.Organization, c1.Name, c1.AddressID, c1.isPrimary, c1.ContactComment, GETDATE(), GETDATE(), 1, c1.Comment, c1.DataA, c1.DataB, c1.ContactID, @newSeasonId 
	FROM Contact c1 where c1.IsActive =1 and c1.SeasonId=@oldId;

INSERT INTO [Address] (AddressLineOne, AddressLineTwo, City, [State], Zip, CreateTime, UpdateTime, IsActive, Comment, DataA, DataB, DataC, SeasonId)
	SELECT AddressLineOne, AddressLineTwo, City, [State], Zip, GETDATE(), GETDATE(), 1, a.Comment, a.DataA, a.DataB, a.AddressID, @newSeasonId 
	FROM [Address] A inner join Contact c on c.AddressID=a.AddressID where a.IsActive=1 and a.SeasonId=@oldId and c.SeasonId = @oldId and c.IsActive=1;

UPDATE T SET T.AddressID = A.AddressID FROM Contact T INNER JOIN [Address] A ON A.DataC is not null and A.DataC<>'' and T.AddressID = A.DataC and T.SeasonId =@newSeasonId;

INSERT INTO PhoneNumber(NumberType, PhoneNumber, isPrimary, CreateTime, UpdateTime, IsActive, Comment, DataA, DataB, DataC, SeasonId, ContactID)
	SELECT NumberType, PhoneNumber, p.isPrimary, GETDATE(), GETDATE(), 1, p.Comment, p.DataA, p.DataB, p.DataC, @newSeasonId, c.ContactID
	FROM PhoneNumber p INNER JOIN Contact c ON p.ContactID = c.DataC and c.SeasonId = @newSeasonId
	where p.IsActive=1 and p.SeasonId=@oldId and c.IsActive=1 and p.PhoneNumber is not null and p.PhoneNumber <>'';
--customer

INSERT INTO Customer
           ([ReportingEntityID]
           ,[DispatcherName]
           ,[ShortName]
           ,[isShippingLocation]
           ,[specialInstructions]
		   ,[Comment]
           ,[CreateTime]
           ,[UpdateTime]
           ,[IsActive]
           ,[DataA]
           ,[DataB]
           ,[DataC]
           ,[SeasonId]
           ,[HideDollarAmounts]
           ,[IsTaxExempt]
           ,[CashMaterialRate]
		   ,[GPID])
     select [ReportingEntityID],[DispatcherName],[ShortName],[isShippingLocation],[specialInstructions],[Comment], GETDATE(), GETDATE(), 1, c1.CustomerID, DataB, [DataC],@newSeasonId, [HideDollarAmounts],[IsTaxExempt],[CashMaterialRate], [GPID]
	 FROM Customer c1 where c1.IsActive =1 and c1.SeasonId=@oldId;

INSERT INTO [dbo].[CustomerContact]
           ([ContactID]
           ,[CustomerID])
     SELECT con.ContactID, c.CustomerID FROM [CustomerContact] cc inner join Customer c on cc.CustomerID = c.DataA and c.DataA is not null and c.DataA <>'' and c.SeasonId = @newSeasonId inner join Contact con on cc.ContactID =con.DataC and con.DataC is not null and con.DataC <>'' and con.SeasonId=@newSeasonId;

--bid

INSERT INTO [dbo].[Bid]
           ([CustomerID]
           ,[SiteId]
           ,[MaterialID]
           ,[PricePerTon]
           ,[PricePerYard]
           ,[TruckingRate]
           ,[POOverride]
           ,[CreateTime]
           ,[UpdateTime]
           ,[IsActive]
           ,[Comment]
           ,[DataA]
           ,[DataB]
           ,[DataC]
           ,[SeasonId]
           ,[Toll])
     select c.CustomerID
           ,[SiteId]
           ,[MaterialID]
           ,b.[PricePerTon]
           ,b.[PricePerYard]
           ,b.[TruckingRate]
           ,b.[POOverride]
            ,GETDATE(),
			GETDATE(),
			1
			,b.[Comment]
           ,b.[DataA]
           ,b.[DataB]
           ,b.[DataC]
           ,@newSeasonId
           ,b.[Toll]
		   from Bid b inner join Customer c on b.CustomerID = c.DataA and c.DataA is not null and c.DataA <>'' and c.SeasonId = @newSeasonId  
		   where b.IsActive=1 and b.SeasonId=@oldId

    update b set b.MaterialID= m.MaterialID
		   FROM Bid b inner join Material m on b.MaterialID = m.DataC and m.DataC is not null and m.DataC <>''
		   WHERE b.SeasonId =  @newSeasonId

	update b set b.SiteId = m.new
		   FROM Bid b inner join (select s.SiteId old, s1.SiteId new from Site s inner join Site s1 on s.SiteName = s1.SiteName and s.SeasonId=@oldId and s1.SeasonId=@newSeasonId) m on b.SiteId = m.old
		   WHERE b.SeasonId =  @newSeasonId
--truck owner

INSERT INTO [dbo].[TruckOwner]
           ([ShortName]
           ,[CreateTime]
           ,[UpdateTime]
           ,[IsActive]
           ,[Comment]
           ,[DataA]
           ,[DataB]
           ,[DataC]
           ,[SeasonId])
select ShortName, GETDATE(), GETDATE(),1, Comment, DataA, DataB, TruckOwnerID, @newSeasonId from TruckOwner where SeasonId = @oldId and IsActive=1

INSERT INTO [dbo].TruckOwnerContact
           ([ContactID]
           ,TruckOwnerID)
     SELECT con.ContactID, c.TruckOwnerID FROM TruckOwnerContact cc 
	 inner join TruckOwner c on cc.TruckOwnerID = c.DataC and c.DataC is not null and c.DataC <>'' and c.SeasonId = @newSeasonId 
	 inner join Contact con on cc.ContactID =con.DataC and con.DataC is not null and con.DataC <>'' and con.SeasonId=@newSeasonId;

--truck

INSERT INTO [dbo].[Truck]
           ([TruckOwnerID]
           ,[ShortName]
           ,[Tag]
           ,[Description]
           ,[TareInterval]
           ,[NeedsTareOverride]
           ,[CubicYards]
           ,[Sticker]
           ,[CreateTime]
           ,[UpdateTime]
           ,[IsActive]
           ,[Comment]
           ,[DataA]
           ,[DataB]
           ,[DataC]
           ,[SeasonId])
select tw.[TruckOwnerID]
           ,t.[ShortName]
           ,[Tag]
           ,[Description]
           ,[TareInterval]
           ,[NeedsTareOverride]
           ,[CubicYards]
           ,[Sticker]
           ,GETDATE()
           ,GETDATE()
           ,1
           ,t.[Comment]
           ,t.[DataA]
           ,t.[DataB]
           ,TruckID
           ,@newSeasonId
		   from Truck t 
		   left join TruckOwner tw on t.TruckOwnerID = tw.DataC and tw.DataC is not null and tw.DataC <>'' and tw.SeasonId = @newSeasonId 
		   where t.IsActive =1 and t.SeasonId = @oldId

INSERT INTO [dbo].[TruckWeightTare]
           ([DateTimeOfWeight]
           ,[TareWeight]
           ,[TruckId]
           ,[CreateTime]
           ,[UpdateTime]
           ,[IsActive]
           ,[Comment]
           ,[DataA]
           ,[DataB]
           ,[DataC])
select [DateTimeOfWeight]
           ,[TareWeight]
           ,t.[TruckId]
           ,GETDATE()
           ,GETDATE()
           ,1
           ,tw.[Comment]
           ,tw.[DataA]
           ,tw.[DataB]
           ,tw.[DataC]
		   from TruckWeightTare tw 
		   inner join Truck t on tw.TruckId = t.DataC and t.DataC is not null and t.DataC <>'' and t.SeasonId = @newSeasonId 
		   where tw.IsActive =1

--ticketprops
update t set t.ValueNumber = r.newVal
from ticketProps t
inner join
(select LastID, ValueNumber, (select ValueNumber from ticketProps where Name=k.f) newVal from ticketProps tp
inner join (select 'FirstTicket '+ FullName f, 'NextTicket '+ FullName l from Company) k ON tp.Name = k.l) r 
on t.LastID=r.LastID;

commit tran
END


