BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.[User] ADD
	IsAuthorizedOrderCreateEditDelete bit NOT NULL CONSTRAINT DF_User_IsAuthorizedOrderCreateEditDelete DEFAULT 0,
	IsAuthorizedAdministrator bit NOT NULL CONSTRAINT DF_User_IsAuthorizedAdministrator DEFAULT 0,
	IsAuthorizedAccounting bit NOT NULL CONSTRAINT DF_User_IsAuthorizedAccounting DEFAULT 0,
	IsAuthorizedSiteChange bit NOT NULL CONSTRAINT DF_User_IsAuthorizedSiteChange DEFAULT 0,
	IsAuthorizedScaleSettings bit NOT NULL CONSTRAINT DF_User_IsAuthorizedScaleSettings DEFAULT 0,
	IsAuthorizedSeasonChange bit NOT NULL CONSTRAINT DF_User_IsAuthorizedSeasonChange DEFAULT 0,
	IsAuthorizedCashCustomer bit NOT NULL CONSTRAINT DF_User_IsAuthorizedCashCustomer DEFAULT 0,
	IsAuthorizedCustomerService bit NOT NULL CONSTRAINT DF_User_IsAuthorizedCustomerService DEFAULT 0,
	IsAuthorizedMaterials bit NOT NULL CONSTRAINT DF_User_IsAuthorizedMaterials DEFAULT 0,
	IsAuthorizedReports bit NOT NULL CONSTRAINT DF_User_IsAuthorizedReports DEFAULT 0
GO
ALTER TABLE dbo.[User] SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
