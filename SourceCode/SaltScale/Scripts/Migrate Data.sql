/*
USE [DatabaseSaltScale];
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	

	/* -- Site Information
		Name			SiteId	TaxRate		CompanyID	FullName			PrinterPort
		Lowell			1					
		Chelsea			2		6.25		3			Eastern Salt		SaltTix
											4			Eastern Minerals	MineralsTix
		Newark			3					6			Atlantic Salt		TICKET
		Staten Island	4					6			Atlantic Salt		ticket
		?Portland		
		Rukert			7					3			Eastern Salt		Ticket
		Portsmouth		8
		Claymont		9
	*/

	/*
		SaltScaleAdminChelsea
		SaltScaleChelsea

		SaltScaleAdminNewark
		SaltScaleNewark	

		SaltScaleAdminPortland
		SaltScalePortland

		SaltScaleAdminStatenIsland
		SaltScaleStatenIsland

		SaltScaleAdminRukert
		SaltScaleRukert
	*/


	DECLARE @SeasonID int = 1
	DECLARE @Site varchar(25) = 'Chelsea'
	DECLARE @SiteId int = 2
	DECLARE @SiteCashTaxRate decimal(3,2) = '6.25'

/*
	--// FK constraints will fail if Season and Site are not populated with data before running this.	
	SET IDENTITY_INSERT [Season] ON
	INSERT INTO dbo.Season (SeasonID, Prefix, StartDate, EndDate, IsActive)
	VALUES (@SeasonID, '16-17','2016-09-12','2016-8-31',1)
	SET IDENTITY_INSERT [Season] OFF

	--INSERT INTO dbo.[Site] ([SiteId],[SiteName],[IsOperatingFromThisLocation],[SeasonId],[MaterialCost],[WeightInterval],[TaxRate],[DefaultCompanyId])
	--SELECT [SiteId],[SiteName],[IsOperatingFromThisLocation],[SeasonId],[MaterialCost],[WeightInterval],[TaxRate],[DefaultCompanyId]
	--FROM SaltScaleDev.dbo.[Site]
	
	INSERT INTO dbo.[Site] ([SiteId],[SiteName],[IsOperatingFromThisLocation],[SeasonId],[MaterialCost],[WeightInterval],[TaxRate],[DefaultCompanyId])
		VALUES				(@SiteId,@Site,1,@SeasonID,0.00, 10, @SiteCashTaxRate,null)


*/
/*
	Find / Replace for the following Source databases:
		FIND:		SaltScaleChelseaV1
		REPLACE:	{Source DB}

		FIND:		SaltScaleChelseaAdminV1
		REPLACE:	{Source Admin DB}

		FIND:		GreatPlainsTableName
		REPLACE:	{Name of Great Plains Table}
*/



-------------------------------------------------------------------------------------------------------------------


--// Disable all check constraints during import.  They will be enabled and checked when complete.
EXEC sp_msforeachtable 'ALTER TABLE ? NOCHECK CONSTRAINT all'


--// Address
SET IDENTITY_INSERT [Address] ON

INSERT INTO dbo.Address (
	[AddressID]
	,[AddressLineOne]
	,[AddressLineTwo]
	,[City]
	,[State]
	,[Zip]
	,[CreateTime]
	,[UpdateTime]
	,[IsActive]
	,[Comment]
	,[DataA]
	,[DataB]
	,[DataC]
	,[SeasonId]
)
SELECT 
	v1.[AddressID]
	,v1.[AddressLineOne]
	,v1.[AddressLineTwo]
	,v1.[City]
	,v1.[State]
	,v1.[Zip]
	,c1.[CreateTime]
	,c1.[UpdateTime]
	,c1.[isActive]
	,c1.[Comment]
	,c1.[DataA]
	,c1.[DataB]
	,c1.[DataC]
	,@SeasonID
FROM SaltScaleChelseaV1.dbo.Address v1 
	LEFT OUTER JOIN SaltScaleChelseaV1.dbo.CommonData c1 ON v1.CommonID = c1.CommonID

SET IDENTITY_INSERT [Address] OFF
DBCC CHECKIDENT('dbo.Address')
	
	
--// Bid
SET IDENTITY_INSERT [Bid] ON

INSERT INTO dbo.Bid(
	[BidID]
	,[CustomerID]
	,[CompanyID]
	,[SiteId]
	,[MaterialID]
	,[PricePerTon]
	,[PricePerYard]
	,[TruckingRate]
	,[POOverride]
	,[CreateTime]
	,[UpdateTime]
	,[IsActive]
	,[Comment]
	,[DataA]
	,[DataB]
	,[DataC]
	,[SeasonId]
	,[Toll]
	)
SELECT 
	v1.[BidID]
	,v1.[CustomerID]
	,v1.[CompanyID]
	,v1.[LocationID]
	,v1.[MaterialID]
	,v1.[PricePerTon]
	,v1.[PricePerYard]
	,CONVERT(DECIMAL(18,2), v1.[TruckingRate]) / 100 --// Last two digits are cents, being converted to decimal
	,v1.[POOverride]
	,c1.[CreateTime]
	,c1.[UpdateTime]
	,c1.[isActive]
	,c1.[Comment]
	,c1.[DataA]
	,c1.[DataB]
	,c1.[DataC]
	,@SeasonID
	,0 --// Toll
FROM SaltScaleChelseaV1.dbo.Bid v1 
	LEFT OUTER JOIN SaltScaleChelseaV1.dbo.CommonData c1 ON v1.CommonID = c1.CommonID

SET IDENTITY_INSERT [Bid] OFF
DBCC CHECKIDENT('dbo.Bid')

	
--// CashPurchase
SET IDENTITY_INSERT [CashPurchase] ON

INSERT INTO dbo.CashPurchase (
	[cashPurchaseID]
	,[truckDescription]
	,[materialID]
	,[quote]
	,[companyID]
	,[locationID]
	,[tare]
	,[paymentForm]
	,[checkNumber]
	,[taxExempt]
	,[isComplete]
	,[CreateTime]
	,[UpdateTime]
	,[IsActive]
	,[Comment]
	,[DataA]
	,[DataB]
	,[DataC]
	,[SeasonId]
	,[TaxRate]
)
SELECT
	v1.[cashPurchaseID]
	,v1.[truckDescription]
	,v1.[materialID]
	,CONVERT(DECIMAL(18,2), v1.[quote]) / 100 --// Last two digits are cents, being converted to decimal
	,v1.[companyID]
	,v1.[locationID]
	,v1.[tare]
	,v1.[paymentForm]
	,v1.[checkNumber]
	,v1.[taxExempt]
	,v1.[isComplete]
	,c1.[CreateTime]
	,c1.[UpdateTime]
	,c1.[isActive]
	,c1.[Comment]
	,c1.[DataA]
	,c1.[DataB]
	,c1.[DataC]
	,@SeasonID
	,0 --// TaxRate
FROM SaltScaleChelseaV1.dbo.CashPurchase v1 
	LEFT OUTER JOIN SaltScaleChelseaV1.dbo.CommonData c1 ON v1.CommonID = c1.CommonID

SET IDENTITY_INSERT [CashPurchase] OFF
DBCC CHECKIDENT('dbo.CashPurchase')


--// Company
INSERT INTO dbo.Company (
	[CompanyId]
	,[FullName]
	,[PrinterPort]
	,[AddressLineOne]
	,[AddressLineTwo]
	,[City]
	,[State]
	,[Zip]
	,[PhoneNumberOne]
	,[PhoneNumberTwo]
	,[FaxNumber]
	,[SeasonMaterialEstimateTon]
)
SELECT 
	[CompanyID]
	,[FullName]
	,COALESCE([PrinterPort], '''')
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL --//PhoneNumberOne
	,NULL
	,NULL
	,NULL
FROM SaltScaleChelseaAdminV1.dbo.Company


--// Contact
SET IDENTITY_INSERT [Contact] ON

INSERT INTO dbo.Contact (
	[ContactID]
	,[Organization]
	,[Name]
	,[AddressID]
	,[isPrimary]
	,[ContactComment]
	,[CreateTime]
	,[UpdateTime]
	,[IsActive]
	,[Comment]
	,[DataA]
	,[DataB]
	,[DataC]
	,[SeasonId]
)
SELECT 
	v1.[ContactID]
	,v1.[Organization]
	,v1.[Name]
	,v1.[AddressID]
	,v1.[isPrimary]
	,v1.[ContactComment]
	,c1.[CreateTime]
	,c1.[UpdateTime]
	,c1.[isActive]
	,c1.[Comment]
	,c1.[DataA]
	,c1.[DataB]
	,c1.[DataC]
	,@SeasonID
FROM SaltScaleChelseaV1.dbo.Contact v1 
	LEFT OUTER JOIN SaltScaleChelseaV1.dbo.CommonData c1 ON v1.CommonID = c1.CommonID
	
SET IDENTITY_INSERT [Contact] OFF
DBCC CHECKIDENT('dbo.Contact')


--// Customer
SET IDENTITY_INSERT [Customer] ON

INSERT INTO dbo.Customer (
	[CustomerID]
	,[ReportingEntityID]
	,[DispatcherName]
	,[ShortName]
	,[isShippingLocation]
	,[specialInstructions]
	,[CreateTime]
	,[UpdateTime]
	,[IsActive]
	,[Comment]
	,[DataA]
	,[DataB]
	,[DataC]
	,[SeasonId]
	,[HideDollarAmounts]
	,[IsTaxExempt]
	,[CashMaterialRate]
)
SELECT 
	v1.[CustomerID]
	,v1.[ReportingEntityID]
	,v1.[DispatcherName]
	,v1.[ShortName]
	,v1.[isShippingLocation]
	,v1.[specialInstructions]
	,c1.[CreateTime]
	,c1.[UpdateTime]
	,c1.[isActive]
	,c1.[Comment]
	,c1.[DataA]
	,c1.[DataB]
	,c1.[DataC]
	,@SeasonID
	,0  --// HideDollarAmounts
	,0  --// IsTaxExcempt
	,0  --// CashMaterialRate
	--// GPID
FROM SaltScaleChelseaV1.dbo.Customer v1 
	LEFT OUTER JOIN SaltScaleChelseaV1.dbo.CommonData c1 ON v1.CommonID = c1.CommonID
	
SET IDENTITY_INSERT [Customer] OFF
DBCC CHECKIDENT('dbo.Customer')


--// CustomerContact
SET IDENTITY_INSERT [CustomerContact] ON

INSERT INTO dbo.CustomerContact (
	[CustomerContactID]
	,[ContactID]
	,[CustomerID]
)
SELECT 
	v1.CustomerContactID
	,v1.ContactID
	,v1.CustomerID
FROM SaltScaleChelseaV1.dbo.CustomerContact v1
	
SET IDENTITY_INSERT [CustomerContact] OFF
DBCC CHECKIDENT('dbo.CustomerContact')


--// Customize
SET IDENTITY_INSERT [Customize] ON

INSERT INTO dbo.Customize(
	[CustomizeID]
	,[TableName]
	,[DataA]
	,[DataB]
	,[DataC]
)
SELECT
	v1.[CustomizeID]
	,v1.[TableName]
	,v1.[DataA]
	,v1.[DataB]
	,v1.[DataC]
FROM SaltScaleChelseaV1.dbo.Customize v1
	
SET IDENTITY_INSERT [Customize] OFF
DBCC CHECKIDENT('dbo.Customize')
	
	
--// GreatPlains
SET IDENTITY_INSERT [GreatPlains] ON

INSERT INTO dbo.GreatPlains (
	[GreatPlainID]
	,[CustomerID]
	,[GPID]
)
SELECT
	v1.[GreatPlainID]
	,v1.[CustomerID]
	,v1.[GPID]
FROM SaltScaleAdminLowell.dbo.GreatPlainsChelsea v1
	
SET IDENTITY_INSERT [GreatPlains] OFF
DBCC CHECKIDENT('dbo.GreatPlains')


--// Material
SET IDENTITY_INSERT Material ON

INSERT INTO dbo.Material (
	[MaterialID]
	,[ShortName]
	,[Description]
	,[PricePerTon]
	,[PricePerYard]
	,[PriceEach]
	,[MinimumCharge]
	,[CreateTime]
	,[UpdateTime]
	,[IsActive]
	,[Comment]
	,[DataA]
	,[DataB]
	,[DataC]
	,[SeasonId]
)
SELECT
	v1.[MaterialID]
	,v1.[ShortName]
	,v1.[Description]
	,CONVERT(DECIMAL(18,2), v1.[PricePerTon]) / 100 --// Last two digits are cents, being converted to decimal
	,CONVERT(DECIMAL(18,2), v1.[PricePerYard]) / 100 --// Last two digits are cents, being converted to decimal
	,CONVERT(DECIMAL(18,2), v1.[PriceEach]) / 100 --// Last two digits are cents, being converted to decimal
	,v1.[MinimumCharge]
	,c1.[CreateTime]
	,c1.[UpdateTime]
	,c1.[isActive]
	,c1.[Comment]
	,c1.[DataA]
	,c1.[DataB]
	,c1.[DataC]
	,@SeasonID
FROM SaltScaleChelseaV1.dbo.Material v1
	LEFT OUTER JOIN SaltScaleChelseaV1.dbo. CommonData c1 ON v1.CommonID = c1.CommonID
	
SET IDENTITY_INSERT Material OFF
DBCC CHECKIDENT('dbo.Material')


--// OrderTable
SET IDENTITY_INSERT OrderTable ON

INSERT INTO dbo.OrderTable (
	[OrderID]
	,[BidID]
	,[SiteID]
	,[OrderNumber]
	,[OrderedBy]
	,[isComplete]
	,[CompleteDate]
	,[Promise]
	,[PromiseExpiresTime]
	,[POOverride]
	,[SpecialInstructionOverride]
	,[EffectiveTime]
	,[HeldUntilDate]
	,[Priority]
	,[CreateTime]
	,[UpdateTime]
	,[IsActive]
	,[SeasonId]
	,[Comment]
	,[DataA]
	,[DataB]
	,[DataC]
	,[MaterialId]
)
SELECT
	v1.[OrderID]
	,v1.[BidID]
	,v1.[LocationID]
	,v1.[OrderNumber]
	,v1.[OrderedBy]
	,v1.[isComplete]
	,v1.[CompleteDate]
	,v1.[Promise]
	,v1.[PromiseExpiresTime]
	,v1.[POOverride]
	,v1.[SpecialInstructionOverride]
	,v1.[EffectiveTime]
	,v1.[HeldUntilDate]
	,v1.[Priority]
	,c1.[CreateTime]
	,c1.[UpdateTime]
	,c1.[isActive]
	,@SeasonID
	,c1.[Comment]
	,c1.[DataA]
	,c1.[DataB]
	,c1.[DataC]
	,b1.MaterialID
	--// ConfirmationNumber
FROM SaltScaleChelseaV1.dbo.OrderTable v1
	LEFT OUTER JOIN SaltScaleChelseaV1.dbo.CommonData c1 ON v1.CommonID = c1.CommonID
	LEFT OUTER JOIN SaltScaleChelseaV1.dbo.Bid b1 ON b1.BidID = v1.BidID
	
SET IDENTITY_INSERT OrderTable OFF
DBCC CHECKIDENT('dbo.OrderTable')


--// OrderUpdate
SET IDENTITY_INSERT OrderUpdate ON

INSERT INTO dbo.OrderUpdate (
	[OrderUpdateID]
	,[OrderID]
	,[Quantity]
	,[CreateTime]
	,[UpdateTime]
	,[IsActive]
	,[Comment]
	,[DataA]
	,[DataB]
	,[DataC]
)
SELECT
	v1.[OrderUpdateID]
	,v1.[OrderID]
	,v1.[Quantity]
	,c1.[CreateTime]
	,c1.[UpdateTime]
	,c1.[isActive]
	,c1.[Comment]
	,c1.[DataA]
	,c1.[DataB]
	,c1.[DataC]
FROM SaltScaleChelseaV1.dbo.OrderUpdate v1
	LEFT OUTER JOIN SaltScaleChelseaV1.dbo.CommonData c1 ON v1.CommonID = c1.CommonID
	
SET IDENTITY_INSERT OrderUpdate OFF
DBCC CHECKIDENT('dbo.OrderUpdate')


--// PhoneNumber
SET IDENTITY_INSERT PhoneNumber ON

INSERT INTO dbo.PhoneNumber(
	[PhoneNumberID]
	,[ContactID]
	,[NumberType]
	,[PhoneNumber]
	,[isPrimary]
	,[CreateTime]
	,[UpdateTime]
	,[IsActive]
	,[Comment]
	,[DataA]
	,[DataB]
	,[DataC]
	,[SeasonId]
)
SELECT
	v1.[PhoneNumberID]
	,v1.[ContactID]
	,v1.[NumberType]
	,v1.[PhoneNumber]
	,v1.[isPrimary]
	,c1.[CreateTime]
	,c1.[UpdateTime]
	,c1.[isActive]
	,c1.[Comment]
	,c1.[DataA]
	,c1.[DataB]
	,c1.[DataC]
	,@SeasonID
FROM SaltScaleChelseaV1.dbo.PhoneNumber v1
	LEFT OUTER JOIN SaltScaleChelseaV1.dbo.CommonData c1 ON v1.CommonID = c1.CommonID
	
SET IDENTITY_INSERT PhoneNumber OFF
DBCC CHECKIDENT('dbo.PhoneNumber')


--// PurchaseOrder
SET IDENTITY_INSERT PurchaseOrder ON

INSERT INTO dbo.PurchaseOrder (
	[PurchaseOrderID]
	,[CustomerID]
	,[PurchaseOrderNumber]
	,[Quantity]
	,[MaterialID]
	,[isComplete]
	,[isHeld]
	,[effectiveDate]
	,[CreateTime]
	,[UpdateTime]
	,[IsActive]
	,[Comment]
	,[DataA]
	,[DataB]
	,[DataC]
	,[SeasonId]
)
SELECT
	v1.[PurchaseOrderID]
	,v1.[CustomerID]
	,v1.[PurchaseOrderNumber]
	,v1.[Quantity]
	,v1.[MaterialID]
	,v1.[isComplete]
	,v1.[isHeld]
	,v1.[effectiveDate]
	,c1.[CreateTime]
	,c1.[UpdateTime]
	,c1.[isActive]
	,c1.[Comment]
	,c1.[DataA]
	,c1.[DataB]
	,c1.[DataC]
	,@SeasonID
FROM SaltScaleChelseaV1.dbo.PurchaseOrder v1
	LEFT OUTER JOIN SaltScaleChelseaV1.dbo.CommonData c1 ON v1.CommonID = c1.CommonID
	
SET IDENTITY_INSERT PurchaseOrder OFF
DBCC CHECKIDENT('dbo.PurchaseOrder')

--// Ticket
SET IDENTITY_INSERT Ticket ON

INSERT INTO dbo.Ticket (
	[TicketID]
	,[TicketNumber]
	,[Quantity]
	,[isManual]
	,[TruckID]
	,[isVoid]
	,[VoidReason]
	,[isReconciled]
	,[hasBeenExported]
	,[CompanyID]
	,[CreateTime]
	,[UpdateTime]
	,[IsActive]
	,[OrderId]
	,[Comment]
	,[DataA]
	,[DataB]
	,[DataC]
	,[SeasonId]
	,[CashPurchaseId]
)
SELECT 
	v1.[TicketID]
	,v1.[TicketNumber]
	,v1.[Quantity]
	,v1.[isManual]
	,v1.[TruckID]
	,v1.[isVoid]
	,v1.[VoidReason]
	,v1.[isReconciled]
	,v1.[hasBeenExported]
	,v1.[CompanyID]
	,c1.[CreateTime]
	,c1.[UpdateTime]
	,c1.[isActive]
	,o1.OrderID
	,c1.[Comment]
	,c1.[DataA]
	,c1.[DataB]
	,c1.[DataC]
	,@SeasonID
	,t1.cashpurchaseID
FROM SaltScaleChelseaV1.dbo.Ticket v1
	LEFT OUTER JOIN SaltScaleChelseaV1.dbo.CommonData c1 ON v1.CommonID = c1.CommonID
	LEFT OUTER JOIN SaltScaleChelseaV1.dbo.OrderTicket o1 ON v1.TicketID = o1.TicketID
	LEFT OUTER JOIN SaltScaleChelseaV1.dbo.CashTransaction t1 ON v1.TicketID = t1.ticketId
		
SET IDENTITY_INSERT Ticket OFF
DBCC CHECKIDENT('dbo.Ticket')
		

--// ticketProps
SET IDENTITY_INSERT ticketProps ON

INSERT INTO dbo.ticketProps (
	[LastID]
	,[Name]
	,[ValueNumber]
)
SELECT
	v1.[LastID]
	,v1.[Name]
	,v1.[ValueNumber]
FROM SaltScaleChelseaV1.dbo.ticketProps v1
	
SET IDENTITY_INSERT ticketProps OFF
DBCC CHECKIDENT('dbo.ticketProps')

--// Truck
SET IDENTITY_INSERT Truck ON

INSERT INTO dbo.Truck (
	[TruckID]
	,[TruckOwnerID]
	,[ShortName]
	,[Sticker]		--// License Plate
	,[Description]
	,[TareInterval]
	,[NeedsTareOverride]
	,[CubicYards]
	,[Tag]		--// Sticker
	,[CreateTime]
	,[UpdateTime]
	,[IsActive]
	,[Comment]
	,[DataA]
	,[DataB]
	,[DataC]
	,[SeasonId]
)
SELECT
	v1.[TruckID]
	,v1.[TruckOwnerID]
	,v1.[ShortName]
	,''--v1.[Tag]
	,v1.[Description]
	,v1.[TareInterval]
	,v1.[NeedsTareOverride]
	,v1.[CubicYards]
	,'' --// Tag
	,c1.[CreateTime]
	,c1.[UpdateTime]
	,c1.[isActive]
	,c1.[Comment]
	,c1.[DataA]
	,c1.[DataB]
	,c1.[DataC]
	,@SeasonID
FROM SaltScaleChelseaV1.dbo.Truck v1
	LEFT OUTER JOIN SaltScaleChelseaV1.dbo.CommonData c1 ON v1.CommonID = c1.CommonID
	
SET IDENTITY_INSERT Truck OFF
DBCC CHECKIDENT('dbo.Truck')


--// TruckOwner
SET IDENTITY_INSERT TruckOwner ON

INSERT INTO dbo.TruckOwner (
	[TruckOwnerID]
	,[ShortName]
	,[CreateTime]
	,[UpdateTime]
	,[IsActive]
	,[Comment]
	,[DataA]
	,[DataB]
	,[DataC]
	,[SeasonId]
)
SELECT
	v1.[TruckOwnerID]
	,v1.[ShortName]
	,c1.[CreateTime]
	,c1.[UpdateTime]
	,c1.[isActive]
	,c1.[Comment]
	,c1.[DataA]
	,c1.[DataB]
	,c1.[DataC]
	,@SeasonID
FROM SaltScaleChelseaV1.dbo.TruckOwner v1
	LEFT OUTER JOIN SaltScaleChelseaV1.dbo.CommonData c1 ON v1.CommonID = c1.CommonID
	
SET IDENTITY_INSERT TruckOwner OFF
DBCC CHECKIDENT('dbo.TruckOwner')
      

--// TruckOwnerContact
SET IDENTITY_INSERT TruckOwnerContact ON

INSERT INTO dbo.TruckOwnerContact (
	[TruckOwnerContactID]
	,[ContactID]
	,[TruckOwnerID]
)
SELECT
	v1.[TruckOwnerContactID]
	,v1.[ContactID]
	,v1.[TruckOwnerID]
FROM SaltScaleChelseaV1.dbo.TruckOwnerContact v1
	
SET IDENTITY_INSERT TruckOwnerContact OFF
DBCC CHECKIDENT('dbo.TruckOwnerContact')


--// TruckWeightTare
SET IDENTITY_INSERT TruckWeightTare ON

INSERT INTO dbo.TruckWeightTare (
	[TruckWeightTare]
	,[DateTimeOfWeight]
	,[TareWeight]
	,[TruckId]
	,[CreateTime]
	,[UpdateTime]
	,[IsActive]
	,[Comment]
	,[DataA]
	,[DataB]
	,[DataC]
)
SELECT
	v1.[TruckWeightID]
	,c1.[UpdateTime]
	,v1.[EmptyWeight]
	,v1.[TruckID]
	,c1.[CreateTime]
	,c1.[UpdateTime]
	,c1.[isActive]
	,c1.[Comment]
	,c1.[DataA]
	,c1.[DataB]
	,c1.[DataC]
FROM SaltScaleChelseaV1.dbo.TruckWeight v1
	LEFT OUTER JOIN SaltScaleChelseaV1.dbo.CommonData c1 ON v1.CommonID = c1.CommonID
	
SET IDENTITY_INSERT TruckWeightTare OFF
DBCC CHECKIDENT('dbo.TruckWeightTare')


--// Enable all of the constraints and verify existing data
exec sp_msforeachtable 'ALTER TABLE ? WITH CHECK CHECK CONSTRAINT all'

*/