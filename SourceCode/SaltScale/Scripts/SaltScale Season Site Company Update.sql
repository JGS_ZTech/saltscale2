﻿/*
SET IDENTITY_INSERT dbo.season ON;
insert into season (seasonid, prefix, StartDate, EndDate, IsActive) values (1, '17', '9/1/2016', '8/31/2017', 1)
SET IDENTITY_INSERT dbo.season OFF;

SET IDENTITY_INSERT dbo.greatplains ON;
insert into greatplains (greatplainid, customerId, gpid) values (0, NULL, '')
SET IDENTITY_INSERT dbo.greatplains OFF;

update Customer
set CompanyId = 10
where CompanyId = 0

----------

update address
set SeasonId = 1
where SeasonId = 4

update bid
set SeasonId = 1
where SeasonId = 4

update CashPurchase
set SeasonId = 1
where SeasonId = 4

update Contact
set SeasonId = 1
where SeasonId = 4

update Customer
set SeasonId = 1
where SeasonId = 4

update Material
set SeasonId = 1
where SeasonId = 4

update OrderTable
set SeasonId = 1
where SeasonId = 4

update PhoneNumber
set SeasonId = 1
where SeasonId = 4

update PurchaseOrder
set SeasonId = 1
where SeasonId = 4

update Site
set SeasonId = 1
where SeasonId = 4

update Truck
set SeasonId = 1
where SeasonId = 4

update TruckOwner
set SeasonId = 1
where SeasonId = 4


*/


update customer
set gpid = 0
where gpid is null


update site
set WeightInterval = 0
where WeightInterval is null

update site
set FuelSurcharge = 0
where FuelSurcharge is null

update site
set MaterialCost = 0
where MaterialCost is null

update site
set TaxRate = 0
where TaxRate is null