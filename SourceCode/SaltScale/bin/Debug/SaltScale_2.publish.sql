﻿/*
Deployment script for SaltScaleChelsea_New

This code was generated by a tool.
Changes to this file may cause incorrect behavior and will be lost if
the code is regenerated.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "SaltScaleChelsea_New"
:setvar DefaultFilePrefix "SaltScaleChelsea_New"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\"
:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\"

GO
:on error exit
GO
/*
Detect SQLCMD mode and disable script execution if SQLCMD mode is not supported.
To re-enable the script after enabling SQLCMD mode, execute the following:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'SQLCMD mode must be enabled to successfully execute this script.';
        SET NOEXEC ON;
    END


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET AUTO_UPDATE_STATISTICS_ASYNC ON 
            WITH ROLLBACK IMMEDIATE;
    END


GO
USE [$(DatabaseName)];


GO
/*
 Pre-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be executed before the build script.	
 Use SQLCMD syntax to include a file in the pre-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the pre-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

GO

GO
PRINT N'Altering [dbo].[Company]...';


GO
ALTER TABLE [dbo].[Company]
    ADD [AddressLineOne] VARCHAR (80) NULL,
        [AddressLineTwo] VARCHAR (80) NULL,
        [City]           VARCHAR (80) NULL,
        [State]          VARCHAR (2)  NULL,
        [Zip]            VARCHAR (10) NULL,
        [PhoneNumberOne] VARCHAR (20) NULL,
        [PhoneNumberTwo] VARCHAR (20) NULL,
        [FaxNumber]      VARCHAR (20) NULL;


GO
PRINT N'Creating [dbo].[User]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
CREATE TABLE [dbo].[User] (
    [Id]                INT           IDENTITY (1, 1) NOT NULL,
    [UserName]          VARCHAR (50)  NOT NULL,
    [Password]          VARCHAR (25)  NOT NULL,
    [SignatureFileName] NVARCHAR (50) NULL,
    [WeighMasterNumber] VARCHAR (50)  NULL,
    CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED ([Id] ASC) ON [PRIMARY]
) ON [PRIMARY];


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
PRINT N'Refreshing [dbo].[sp_MoveToNewSeason]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[sp_MoveToNewSeason]';


GO
PRINT N'Update complete.';


GO
