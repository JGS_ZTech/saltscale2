﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestStack.White;
using TestStack.White.Factory;
using TestStack.White.ScreenObjects;
using UnitTests.Pages;

namespace UnitTests
{
    [TestClass]
    public class LoginTests
    {
        private Application _application;

        [TestInitialize]
        public void TestInitialize()
        {
            _application = Application.Launch(@"C:\depot\Eastern\ZTech\ZTech\bin\Debug\ZTech.exe");
        }

        [TestMethod]
        public void LoginValid()
        {
            ScreenRepository screenRepository = new ScreenRepository(_application.ApplicationSession);
            var loginForm = screenRepository.Get<LoginScreen>(LoginScreen.Title, InitializeOption.NoCache);
            
            loginForm.FillForm("Dispatcher", "dispatcher");
            loginForm.PressLogin();
            
            var dispatcheForm = screenRepository.Get<DispatcherScreen>("Dispatcher", InitializeOption.NoCache);
          
            Assert.IsNotNull(dispatcheForm);        
        }
        
        [TestCleanup]
        public void TestCleanup()
        {
            _application.Kill();
        }

    }
}
