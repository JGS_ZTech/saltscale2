﻿using TestStack.White.ScreenObjects;
using TestStack.White.UIItems.WindowItems;

namespace UnitTests.Pages
{
    public partial class DispatcherScreen : AppScreen
    {
        public DispatcherScreen(Window window, ScreenRepository screenRepository) : base(window, screenRepository)
        {
        }
    }
}