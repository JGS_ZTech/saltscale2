namespace Utility.CSVFile
{
    public enum EmptyLineBehavior
    {
        NoColumns,
        EmptyColumn,
        Ignore,
        EndOfFile,
    }
}