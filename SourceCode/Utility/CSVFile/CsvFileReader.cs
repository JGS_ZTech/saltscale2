using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace Utility.CSVFile
{
    public class CsvFileReader : CsvFileCommon, IDisposable
    {
        private readonly StreamReader reader;
        private string currLine;
        private int currPos;
        private readonly EmptyLineBehavior emptyLineBehavior;

        public bool EndOfFile
        {
            get { return reader == null || reader.EndOfStream; }
        }

        public CsvFileReader(Stream stream,
                             EmptyLineBehavior emptyLineBehavior = EmptyLineBehavior.NoColumns)
        {
            reader = new StreamReader(stream);
            this.emptyLineBehavior = emptyLineBehavior;
        }

        public CsvFileReader(string path,
                             EmptyLineBehavior emptyLineBehavior = EmptyLineBehavior.NoColumns)
        {
            reader = new StreamReader(path);
            this.emptyLineBehavior = emptyLineBehavior;
        }

        public bool ReadRow(List<string> columns)
        {
            if (columns == null)
                throw new ArgumentNullException("columns");

            ReadNextLine:
            currLine = reader.ReadLine();
            currPos = 0;
            if (currLine == null)
                return false;
            if (currLine.Length == 0)
            {
                switch (emptyLineBehavior)
                {
                    case EmptyLineBehavior.NoColumns:
                        columns.Clear();
                        return true;
                    case EmptyLineBehavior.Ignore:
                        goto ReadNextLine;
                    case EmptyLineBehavior.EndOfFile:
                        return false;
                }
            }

            int numColumns = 0;
            while (true)
            {
                string column;
                if (currPos < currLine.Length && currLine[currPos] == Quote)
                    column = ReadQuotedColumn();
                else
                    column = ReadUnquotedColumn();
                if (numColumns < columns.Count)
                    columns[numColumns] = column;
                else
                    columns.Add(column);
                numColumns++;
                if (currLine == null || currPos == currLine.Length)
                    break;
                Debug.Assert(currLine[currPos] == Delimiter);
                currPos++;
            }
            if (numColumns < columns.Count)
                columns.RemoveRange(numColumns, columns.Count - numColumns);
            return true;
        }

        private string ReadQuotedColumn()
        {
            Debug.Assert(currPos < currLine.Length && currLine[currPos] == Quote);
            currPos++;

            var builder = new StringBuilder();
            while (true)
            {
                while (currPos == currLine.Length)
                {
                    currLine = reader.ReadLine();
                    currPos = 0;
                    if (currLine == null)
                        return builder.ToString();
                    builder.Append(Environment.NewLine);
                }

                if (currLine[currPos] == Quote)
                {
                    int nextPos = (currPos + 1);
                    if (nextPos < currLine.Length && currLine[nextPos] == Quote)
                        currPos++;
                    else
                        break; 
                }
                builder.Append(currLine[currPos++]);
            }

            if (currPos < currLine.Length)
            {
                Debug.Assert(currLine[currPos] == Quote);
                currPos++;
                builder.Append(ReadUnquotedColumn());
            }
            return builder.ToString();
        }

        private string ReadUnquotedColumn()
        {
            int startPos = currPos;
            currPos = currLine.IndexOf(Delimiter, currPos);
            if (currPos == -1)
                currPos = currLine.Length;
            if (currPos > startPos)
                return currLine.Substring(startPos, currPos - startPos);
            return String.Empty;
        }

        public void Dispose()
        {
            reader.Dispose();
        }
    }
}