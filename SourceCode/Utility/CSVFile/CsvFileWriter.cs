﻿using System;
using System.IO;
using System.Text;

namespace Utility.CSVFile
{
    public class CsvFileWriter : CsvFileCommon, IDisposable
    {
        private readonly StreamWriter writer;
        private string oneQuote;
        private string twoQuotes;
        private string quotedFormat;

        public CsvFileWriter(Stream stream)
        {
            writer = new StreamWriter(stream);
        }

        public CsvFileWriter(string path)
        {
            writer = new StreamWriter(path, true);
        }

        public void WriteRow(string[] columns)
        {
            if (columns == null)
                throw new ArgumentNullException("columns");

            if (oneQuote == null || oneQuote[0] != Quote)
            {
                oneQuote = String.Format("{0}", Quote);
                twoQuotes = String.Format("{0}{0}", Quote);
                quotedFormat = String.Format("{0}{{0}}{0}", Quote);
            }

            var record = new StringBuilder();

            for (int i = 0; i < columns.Length; i++)
            {
                if (i > 0) record.Append(Delimiter);

                if (columns[i] == null) columns[i] = string.Empty;

                if (columns[i].IndexOfAny(SpecialChars) == -1)
                {
                    if (AlwaysQuoted)
                        record.Append(string.Format(quotedFormat, columns[i].Replace(oneQuote, twoQuotes)));
                    else
                        record.Append(columns[i]);
                }
                else
                    record.Append(string.Format(quotedFormat, columns[i].Replace(oneQuote, twoQuotes)));
            }

            writer.WriteLine(string.Concat(record));
        }

        public void Dispose()
        {
            writer.Dispose();
        }
    }

}